<?php
    /*
    |--------------------------------------------------------------------------
    | Impostazioni regionali e settings riguardanti le date, la lingua e la valuta
    |--------------------------------------------------------------------------
    */
    return  [
        'currency_character' => env('DATES_CURRENCY_LOCALE_CURRENCY_CHARACTER', '€'),
        'currency_html_entity' => env('DATES_CURRENCY_LOCALE_CURRENCY_HTML_ENTITY', '&euro;'),
        'local_date_format_full_carbon' => env('DATES_CURRENCY_LOCALE_DATE_FORMAT_FULL_CARBON', 'Y-m-d'),
        'local_date_format_full_momentjs' => env('DATES_CURRENCY_LOCALE_DATE_FORMAT_FULL_MOMENTJS', 'YYYY-MM-DD'),
        'local_date_format_short_carbon' => env('DATES_CURRENCY_LOCALE_DATE_FORMAT_SHORT_CARBON', 'm-d'),
        'local_date_format_short_momentjs' => env('DATES_CURRENCY_LOCALE_DATE_FORMAT_SHORT_MOMENTJS', 'MM-DD'),
        'local_datetime_format_full_carbon' => env('DATES_CURRENCY_LOCALE_DATETIME_FORMAT_FULL_CARBON', 'Y-m-d H:i:s'),
        'local_datetime_format_full_momentjs' => env('DATES_CURRENCY_LOCALE_DATETIME_FORMAT_FULL_MOMENTJS', 'YYYY-MM-DD HH:mm:ss'),
        'local_datetime_format_short_carbon' => env('DATES_CURRENCY_LOCALE_DATETIME_FORMAT_SHORT_CARBON', 'm-d H:i'),
        'local_datetime_format_short_momentjs' => env('DATES_CURRENCY_LOCALE_DATETIME_FORMAT_SHORT_MOMENTJS', 'MM-DD HH:mm'),
        'local_time_format_full_carbon' => env('DATES_CURRENCY_LOCALE_TIME_FORMAT_FULL_CARBON', 'H:i:s'),
        'local_time_format_full_momentjs' => env('DATES_CURRENCY_LOCALE_TIME_FORMAT_FULL_MOMENTJS', 'HH:mm:ss'),
        'local_time_format_short_carbon' => env('DATES_CURRENCY_LOCALE_TIME_FORMAT_SHORT_CARBON', 'H:i'),
        'local_time_format_short_momentjs' => env('DATES_CURRENCY_LOCALE_TIME_FORMAT_SHORT_MOMENTJS', 'HH:mm'),
        'local_timezone' => env('DATES_CURRENCY_LOCALE_LOCAL_TIMEZONE', 'UTC'),
        'max_utc_timestamp' => env('DATES_CURRENCY_LOCALE_MAX_UTC_TIMESTAMP', '2050-12-31 23:59:59'),
        'min_utc_timestamp' => env('DATES_CURRENCY_LOCALE_MIN_UTC_TIMESTAMP', '2000-01-01 00:00:00'), 
        'default_report_interval_timespan' =>  env('DATES_CURRENCY_LOCALE_DEFAULT_REPORT_INTERVAL_TIMESPAN', '1 month'),
    ];
