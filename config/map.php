<?php
    /*
    |--------------------------------------------------------------------------
    | Impostazioni relative alla mappa
    |--------------------------------------------------------------------------
    */
    return  [
        'divider' => env('MAP_DIVIDER', 15),                            // le coordinate delle coloninne provenienti da SmartView corrispondono a quelle adatte al file immagine della mappa di SmartCloud. E' possibile usare questo valore per adattarle: $coordinata_smartcloud = $coordinata_smartview / config('map.divider');
        'x_coord_offset_px' => env('MAP_X_COORD_OFFSET_PX', 0),
        'y_coord_offset_px' => env('MAP_Y_COORD_OFFSET_PX', 48),
    ];
