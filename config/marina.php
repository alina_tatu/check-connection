<?php
    /*
    |--------------------------------------------------------------------------
    | Impostazioni generiche del marina
    |--------------------------------------------------------------------------
    */
    return  [
        'admin_home_view' => env('MARINA_ADMIN_HOME_VIEW', 'plant_status'),
        
        // permette di usare il form consumption_reports per richiedere un grafico di consumo con date e campionamento personalizzati. E' (almeno per ora) una feature opzionale perchè è stata richiesta da Lantau Yacht Club a giugno 2021, ed è stato inviato preventivo che non è ancora stato accettato. L'ho implementata per semplificare il funzionamento del form e facilitarne la manutenzione:
        'allow_requests_for_consumption_charts_with_custom_dates_and_sampling' => env('MARINA_ALLOW_REQUESTS_FOR_CONSUMPTION_CHARTS_WITH_CUSTOM_DATES_AND_SAMPLING', false),
        
        'alphanumeric_berth_names' => env('MARINA_ALPHANUMERIC_BERTH_NAMES', false),

        // orari ai quali girerà il job che elimina due volte al giorno le snapshot inutili dal db (fuso orario locale):
        'delete_unnecessary_socket_snapshots_daily_hour_1' => env('MARINA_DELETE_UNNECESSARY_SOCKET_SNAPSHOTS_DAILY_HOUR_1', 5),
        'delete_unnecessary_socket_snapshots_daily_hour_2' => env('MARINA_DELETE_UNNECESSARY_SOCKET_SNAPSHOTS_DAILY_HOUR_2', 21),

        'always_dispatch_socket_snapshots_synchronously' => env('MARINA_ALWAYS_DISPATCH_SOCKET_SNAPSHOTS_SYNCHRONOUSLY', true),     // se settato a true, il supervisore dovrà sempre attendere la risposta del cloud quando invia le snapshots in modalità sync. Al contrario - quando settato a false - SmartCloud risponderà a SmartView immediatamente dopo la validazione dei dati, mettendo in queue le operazioni da fare successivamente (come la generazione del SubscriptionSnapshot, la verifica e la notifica degli allarmi, etc.)

        'interval_between_two_identical_snapshot_for_same_socket_minutes' => env('MARINA_INTERVAL_BETWEEN_TWO_IDENTICAL_SNAPSHOT_FOR_SAME_SOCKETS_MINUTES', 60),
        'manage_customers_and_subscriptions_via_web' => env('MARINA_MANAGE_CUSTOMERS_AND_SUBSCRIPTIONS_VIA_WEB', false),
        'max_power_consumption_value_for_alarm_threshold' => env('MARINA_MAX_SOCKET_CREDIT_FOR_THRESHOLDS', 3000),              // valore massimo settabile per le soglie di allarme consumo energia sulla presa
        'max_socket_credit_value_for_alarm_threshold' => env('MARINA_MAX_SOCKET_CREDIT_FOR_THRESHOLDS', 999999.99),             // valore massimo settabile per le soglie di allarme credito sulla presa. Per default lo metto al massimo valore scrivibile sul db.
        'max_sockets_per_serial_line' => env('MARINA_MAX_SOCKETS_PER_SERIAL_LINE', 400),
        'max_sockets_per_pedestal' => env('MARINA_MAX_SOCKETS_PER_PEDESTAL', 4),
        'max_water_tap_time_alarm_minimum_consumption_in_liters_per_minute_for_water_tap_to_be_considered_open' => env('MARINA_MIN_LITERS_PER_MINUTE_BEFORE_CONSIDERING_TAP_OPEN_FOR_WATER_TIME_ALARM', 60),
        'max_water_tap_time_alarm_minimum_consumption_in_liters_between_snapshots_to_see_growth_in_consumption' => env('MARINA_MIN_LITERS_BETWEEN_SNAPSHOTS_TO_SEE_CONSUMPTION_GROWTH_FOR_WATER_TIME_ALARM', 5),
        'max_water_tap_time_alarm_minutes_value_for_alarm_threshold' => env('MARINA_MAX_MINUTES_FOR_WATER_TIME_ALARM', 1440),
        'max_water_tap_time_alarm_threshold_for_admin_users_minutes' => env('MARINA_MAX_MINUTES_FOR_WATER_TIME_ALARM_FOR_ADMIN_USERS', null),
        'name' => env('MARINA_NAME', 'Plus test plant'),
        'notifications_refresh_interval_seconds' => env('MARINA_NOTIFICATIONS_REFRESH_INTERVAL_SECONDS', 60),                   // intervallo di tempo per l'update delle notifiche sulla navbar e sulla view notifications (secondi),
        'power_alarms_minutes' => env('MARINA_INTERVAL_BETWEEN_TWO_SNAPSHOTS_TO_CHECK_FOR_POWER_CONSUMPTION_ALARMS_MINUTES', 60),
        'standard_users_can_access_consumption_reports_page' => env('MARINA_STANDARD_USERS_CAN_ACCESS_CONSUMPTION_REPORTS_PAGE', true),
        'show_socket_disconnection_alerts_on_charts' => env('MARINA_SHOW_SOCKET_DISCONNECTION_ALERTS_ON_CHARTS', true),
        'add_logo_on_pdf_consumption_reports' => env('MARINA_ADD_LOGO_ON_PDF_CONSUMPTION_REPORTS', false),
    ];
