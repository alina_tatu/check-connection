<?php

namespace Tests\Feature;

use Tests\TestCase;
//use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;
use App\Berth;
use App\User;
use App\BaseSocketSnapshot;
use App\SocketSnapshot;
use App\SocketLastSnapshot;
use App\Subscription;
use App\SubscriptionSnapshot;
use App\SubscriptionAlarmThresholdsRecord;
use App\Transponder;
use App\SubscriptionCostsRecord;

class SubscriptionSnapshotsAreCalculatedCorrectlyTest extends TestCase {
    //use DatabaseTransactions;     // TODO: scommentarlo?

    private $created_berths, $created_users, $created_transponders, $created_subscriptions;

    public function setUp(): void {
        $this->created_berths = $this->created_users = $this->created_transponders = $this->created_subscriptions = collect();
        parent::setUp();
    }

    public function teardown(): void {        
        SocketLastSnapshot::whereIn('socket_id', $this->created_berths->pluck('id'))->delete();
        SocketSnapshot::whereIn('socket_id', $this->created_berths->pluck('id'))->delete();
        SubscriptionSnapshot::whereIn('subscription_id', $this->created_subscriptions->pluck('id'))->delete();
        SubscriptionCostsRecord::whereIn('subscription_id', $this->created_subscriptions->pluck('id'))->delete();
        SubscriptionAlarmThresholdsRecord::whereIn('subscription_id', $this->created_subscriptions->pluck('id'))->delete();
        Subscription::whereIn('id', $this->created_subscriptions->pluck('id'))->delete();
        Transponder::whereIn('id', $this->created_transponders->pluck('id'))->delete();
        User::whereIn('id', $this->created_users->pluck('id'))->delete();
        Berth::whereIn('id', $this->created_berths->pluck('id'))->delete();        
        parent::tearDown();
    }

    public function test_consumption_on_one_socket_only_from_zero_counters_is_calculated_correctly() {
        $socket_id = 364319;
        $pontoon_name = "Z1 Right";
        $berth_name = 319;
        $berth_description = $pontoon_name." ".$berth_name;
        $berth = Berth::firstOrCreate([
            'id' => $socket_id,
            'pontoon_name' => $pontoon_name,
            'berth_name' => $berth_name,
            'description' => $berth_description,
            'socket_type' => Berth::DEFAULT_SOCKET_TYPE,
        ]);
        $berth->calculate_and_set_pedestal_id();
        $berth->save();
        $this->created_berths->push($berth);
        $user_1 = User::firstOrCreate([
            'last_name' => 'Pallino',
            'first_name' => 'Pinco',
            'token_for_actions_on_sockets' => User::generate_token_for_actions_on_sockets(),
        ]);
        $this->created_users->push($user_1);
        $user_2 = User::firstOrCreate([
            'last_name' => 'Semproni',
            'first_name' => 'Sempronio',
            'token_for_actions_on_sockets' => User::generate_token_for_actions_on_sockets(),
        ]);
        $this->created_users->push($user_2);
        $transponder_1 = Transponder::firstOrCreate([
            'code' => '0773B135',
            'used_for_socket_subscriptions' => false,
        ]);
        $this->created_transponders->push($transponder_1);
        $transponder_2 = Transponder::firstOrCreate([
            'code' => 'ABCDEF12',
            'used_for_socket_subscriptions' => false,
        ]);
        $this->created_transponders->push($transponder_2);
        $subscription_1 = Subscription::firstOrCreate([
            'user_id' => $user_1->get_id(),
            'transponder_id' => $transponder_1->get_code(),
            'socket_id' => null,
            'begin_dt' => Carbon::yesterday(),
            'end_dt' => null,
        ]);
        $this->created_subscriptions->push($subscription_1);
        $subscription_2 = Subscription::firstOrCreate([
            'user_id' => $user_2->get_id(),
            'transponder_id' => $transponder_2->get_code(),
            'socket_id' => null,
            'begin_dt' => Carbon::yesterday()->subDays(3),
            'end_dt' => null,
        ]);
        $this->created_subscriptions->push($subscription_2);
        $subscription_1_costs = SubscriptionCostsRecord::firstOrCreate([
            'subscription_id' => $subscription_1->get_id(),
            'power_cost_per_kwh' => 0.35,
            'water_cost_per_m3' => 7,
            'begin_dt' => $subscription_1->get_begin_timestamp(),
            'end_dt' => null,
        ]);
        $subscription_2_costs = SubscriptionCostsRecord::firstOrCreate([
            'subscription_id' => $subscription_2->get_id(),
            'power_cost_per_kwh' => 1.27,
            'water_cost_per_m3' => 8.5,
            'begin_dt' => $subscription_2->get_begin_timestamp(),
            'end_dt' => null,
        ]);
        // creo il primo snapshot: la presa comunica ma è vergine e spenta, transponder id nullo:
        $timestamp = Carbon::now()
                           ->subMinutes(1);
        $link_status = BaseSocketSnapshot::LINK_STATUS_CONNECTED;
        $on_off_status = BaseSocketSnapshot::SOCKET_STATUS_OFF;
        $transponder_id = BaseSocketSnapshot::TRANSPONDER_ID_VALUE_RECEIVED_FROM_SMARTVIEW_WHEN_TRANSPONDER_IS_NULL;
        $total_power_cnt = 0;
        $total_water_cnt = 0;
        $postpaid_power_cnt = 0; 
        $postpaid_water_cnt = 0;
        $residual_credit = 0;
        $epower_id = 1234567890;
        $instant_power_consumption = 0 * pow(10, 5);
        $alarms = null;
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($socket_id,
                                                                               $timestamp->timestamp,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_id,
                                                                               $total_power_cnt,  
                                                                               $postpaid_power_cnt,
                                                                               $total_water_cnt,  
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $epower_id,
                                                                               $instant_power_consumption,
                                                                               $alarms);
        $this->get($socket_snapshot_url);

        // secondo snapshot, 30 secondi prima di now(): la presa è appena stata accesa dal transponder_1, credito residuo: 10, ancora nessuno consumo:
        $timestamp = Carbon::now()
                           ->subSeconds(30);
        $on_off_status = BaseSocketSnapshot::SOCKET_STATUS_ON;
        $residual_credit = 10;
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($socket_id,
                                                                               $timestamp->timestamp,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_1->get_code(),
                                                                               $total_power_cnt,  
                                                                               $postpaid_power_cnt,
                                                                               $total_water_cnt,  
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $epower_id,
                                                                               $instant_power_consumption,
                                                                               $alarms);
        $this->get($socket_snapshot_url);

        $this->assertDatabaseHas('subscription_snapshots', [
            'subscription_id' => $subscription_1->get_id(),
            'timestamp' => $timestamp->format((new BaseSocketSnapshot())->dateFormat),
            'socket_id' => $socket_id,
            'subscription_total_power_consumption' => 0,
            'subscription_total_water_consumption' => 0,
            'subscription_total_credit_consumption' => 0,
            'subscription_power_consumption_on_socket' => 0,
            'subscription_water_consumption_on_socket' => 0,
            'subscription_credit_consumption_on_socket' => 0,
        ]);
        // terzo snapshot: timestamp: now(), la presa sta consumando sempre con transponder_1: ha usato 1 di credito ed accumulato un consumo di energia = 1, acqua = 2 (per comodità incremento sia i totali che i postpaid)
        $timestamp = Carbon::now();
        $total_power_cnt = $postpaid_power_cnt = 1;
        $total_water_cnt = $postpaid_water_cnt = 2;
        $residual_credit = 9;
        $instant_power_consumption = 250 * pow(10, 5);
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($socket_id,
                                                                               $timestamp->timestamp,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_1->get_code(),
                                                                               $total_power_cnt,  
                                                                               $postpaid_power_cnt,
                                                                               $total_water_cnt,  
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $epower_id,
                                                                               $instant_power_consumption,
                                                                               $alarms);
        $this->get($socket_snapshot_url);
        
        $this->assertDatabaseHas('subscription_snapshots', [
            'subscription_id' => $subscription_1->get_id(),
            'timestamp' => $timestamp->format((new BaseSocketSnapshot())->dateFormat),
            'socket_id' => $socket_id,
            'subscription_total_power_consumption' => 1,
            'subscription_total_water_consumption' => 2,
            'subscription_total_credit_consumption' => 1,
            'subscription_power_consumption_on_socket' => 1,
            'subscription_water_consumption_on_socket' => 2,
            'subscription_credit_consumption_on_socket' => 1,
        ]);

        // quarto snapshot: timestamp: now() + 1 minuto, la presa sta consumando sempre con transponder_1: ha usato altre 2 unità di credito ed accumulato un consumo di energia +3, acqua +5 (per comodità incremento sia i totali che i postpaid)
        $timestamp = Carbon::now()->addMinutes(1);
        $total_power_cnt = $postpaid_power_cnt += 3;
        $total_water_cnt = $postpaid_water_cnt += 5;
        $residual_credit -= 2;
        $instant_power_consumption = 470 * pow(10, 5);
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($socket_id,
                                                                               $timestamp->timestamp,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_1->get_code(),
                                                                               $total_power_cnt,  
                                                                               $postpaid_power_cnt,
                                                                               $total_water_cnt,  
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $epower_id,
                                                                               $instant_power_consumption,
                                                                               $alarms);
        $this->get($socket_snapshot_url);

        //  verifico che il subscription_snapshot relativo al socket_snapshot appena inviato venga scritto correttamente:
        $this->assertDatabaseHas('subscription_snapshots', [
            'subscription_id' => $subscription_1->get_id(),
            'timestamp' => $timestamp->format((new BaseSocketSnapshot())->dateFormat),
            'socket_id' => $socket_id,
            'subscription_total_power_consumption' => 4,
            'subscription_total_water_consumption' => 7,
            'subscription_total_credit_consumption' => 3,
            'subscription_power_consumption_on_socket' => 4,
            'subscription_water_consumption_on_socket' => 7,
            'subscription_credit_consumption_on_socket' => 3,
        ]);

        // quinto snapshot: timestamp: now() + 2 minuti: la presa ha sempre transponder_1 attivo, ha gli stessi contatori e lo stesso credito dello snapshot precedente, ma viene spenta
        $timestamp = Carbon::now()->addMinutes(2);
        $on_off_status = BaseSocketSnapshot::SOCKET_STATUS_OFF;
        $instant_power_consumption = 0;
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($socket_id,
                                                                               $timestamp->timestamp,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_1->get_code(),
                                                                               $total_power_cnt,  
                                                                               $postpaid_power_cnt,
                                                                               $total_water_cnt,  
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $epower_id,
                                                                               $instant_power_consumption,
                                                                               $alarms);
        $this->get($socket_snapshot_url);

        //  verifico che il subscription_snapshot relativo al socket_snapshot appena inviato venga scritto correttamente:
        $this->assertDatabaseHas('subscription_snapshots', [
            'subscription_id' => $subscription_1->get_id(),
            'timestamp' => $timestamp->format((new BaseSocketSnapshot())->dateFormat),
            'socket_id' => $socket_id,
            'subscription_total_power_consumption' => 4,
            'subscription_total_water_consumption' => 7,
            'subscription_total_credit_consumption' => 3,
            'subscription_power_consumption_on_socket' => 4,
            'subscription_water_consumption_on_socket' => 7,
            'subscription_credit_consumption_on_socket' => 3,
        ]);

        // sesto snapshot: timestamp: now() + 4 minuti: la presa viene accesa con transponder_2 attivo, ha 5 di credito e gli stessi contatori di prima
        $timestamp = Carbon::now()->addMinutes(4);
        $on_off_status = BaseSocketSnapshot::SOCKET_STATUS_ON;
        $instant_power_consumption = 20;
        $residual_credit = 5;
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($socket_id,
                                                                               $timestamp->timestamp,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_2->get_code(),
                                                                               $total_power_cnt,  
                                                                               $postpaid_power_cnt,
                                                                               $total_water_cnt,  
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $epower_id,
                                                                               $instant_power_consumption,
                                                                               $alarms);
        $this->get($socket_snapshot_url);

        //  verifico che il subscription_snapshot relativo al socket_snapshot appena inviato venga scritto correttamente:
        $this->assertDatabaseHas('subscription_snapshots', [
            'subscription_id' => $subscription_2->get_id(),
            'timestamp' => $timestamp->format((new BaseSocketSnapshot())->dateFormat),
            'socket_id' => $socket_id,
            'subscription_total_power_consumption' => 0,
            'subscription_total_water_consumption' => 0,
            'subscription_total_credit_consumption' => 0,
            'subscription_power_consumption_on_socket' => 0,
            'subscription_water_consumption_on_socket' => 0,
            'subscription_credit_consumption_on_socket' => 0,
        ]);

        // settimo snapshot: timestamp: now() + 6 minuti: la presa sta consumando con transponder_2 attivo, ha consumato 1 di credito, 4 di energia e 1.5 di acqua
        $timestamp = Carbon::now()->addMinutes(6);
        $on_off_status = BaseSocketSnapshot::SOCKET_STATUS_ON;
        $instant_power_consumption = 20;
        $residual_credit = 4;
        $total_power_cnt = $postpaid_power_cnt += 4;
        $total_water_cnt = $postpaid_water_cnt += 1.5;
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($socket_id,
                                                                               $timestamp->timestamp,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_2->get_code(),
                                                                               $total_power_cnt,  
                                                                               $postpaid_power_cnt,
                                                                               $total_water_cnt,  
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $epower_id,
                                                                               $instant_power_consumption,
                                                                               $alarms);
        $this->get($socket_snapshot_url);

        //  verifico che il subscription_snapshot relativo al socket_snapshot appena inviato venga scritto correttamente:
        $this->assertDatabaseHas('subscription_snapshots', [
            'subscription_id' => $subscription_2->get_id(),
            'timestamp' => $timestamp->format((new BaseSocketSnapshot())->dateFormat),
            'socket_id' => $socket_id,
            'subscription_total_power_consumption' => 4,
            'subscription_total_water_consumption' => 1.5,
            'subscription_total_credit_consumption' => 1,
            'subscription_power_consumption_on_socket' => 4,
            'subscription_water_consumption_on_socket' => 1.5,
            'subscription_credit_consumption_on_socket' => 1,
        ]);


        // TODO: far usare ancora la presa dal primo transponder e verificare i nuovi subscription_snapshots, poi far usare due prese contemporaneamente dallo stesso transponder. Aggiungere poi altri test ancora?
    }
}
