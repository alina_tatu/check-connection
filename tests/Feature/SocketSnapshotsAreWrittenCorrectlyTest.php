<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;
use App\BaseSocketSnapshot;

class SocketSnapshotsAreWrittenCorrectlyTest extends TestCase {
    use DatabaseTransactions;        // questo trait elimina automaticamente i dati scritti sul db durante i test, dopo averli eseguiti

    public function test_socket_snapshot_call_returns_http_ok_code() {
        $socket_id = 401;
        $timestamp = Carbon::now();
        $link_status = BaseSocketSnapshot::LINK_STATUS_CONNECTED;
        $on_off_status = BaseSocketSnapshot::SOCKET_STATUS_OFF;
        $transponder_id = BaseSocketSnapshot::TRANSPONDER_ID_VALUE_RECEIVED_FROM_SMARTVIEW_WHEN_TRANSPONDER_IS_NULL;
        $total_power_cnt = $total_water_cnt = $postpaid_power_cnt = $postpaid_water_cnt = $residual_credit = 0;
        $epower_id = 123456789;
        $instant_power_consumption = 0;
        $alarms = null;
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($socket_id,
                                                                               $timestamp->timestamp,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_id,
                                                                               $total_power_cnt,  
                                                                               $postpaid_power_cnt,
                                                                               $total_water_cnt,  
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $epower_id,
                                                                               $instant_power_consumption,
                                                                               $alarms);
        $response = $this->get($socket_snapshot_url);
        $response->assertStatus(202);
    }

    public function test_socket_snapshot_values_are_written_correctly() {
        $socket_id = 402;
        $timestamp = Carbon::now();
        $link_status = BaseSocketSnapshot::LINK_STATUS_CONNECTED;
        $on_off_status = BaseSocketSnapshot::SOCKET_STATUS_ON;
        $transponder_id = "12345678";
        $total_power_cnt = 2;
        $total_water_cnt = 1;
        $postpaid_power_cnt = 0.8; 
        $postpaid_water_cnt = 0.5;
        $residual_credit = 10;
        $epower_id = 123456789;
        $instant_power_consumption = 25 * pow(10, 5);
        $alarms = null;
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($socket_id,
                                                                               $timestamp->timestamp,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_id,
                                                                               $total_power_cnt,  
                                                                               $postpaid_power_cnt,
                                                                               $total_water_cnt,  
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $epower_id,
                                                                               $instant_power_consumption,
                                                                               $alarms);
        $this->get($socket_snapshot_url);
        $this->assertDatabaseHas('socket_snapshots', [
            'socket_id' => $socket_id,
            'timestamp' => $timestamp->format((new BaseSocketSnapshot())->dateFormat),
            'link_status' => $link_status,
            'on_off_status' => $on_off_status,
            'transponder_id' => $transponder_id,
            'total_power_cnt' => $total_power_cnt,
            'postpaid_power_cnt' => $postpaid_power_cnt,
            'total_water_cnt' => $total_water_cnt,
            'postpaid_water_cnt' => $postpaid_water_cnt,
            'residual_credit' => $residual_credit,
            'epower_id' => $epower_id,
            'instant_power_consumption' => $instant_power_consumption * pow(10, -5),
        ]);
    }
}
