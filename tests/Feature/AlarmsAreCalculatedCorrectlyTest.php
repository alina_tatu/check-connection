<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use App\Berth;
use App\BaseSocketSnapshot;
use App\SocketSnapshot;

class AlarmsAreCalculatedCorrectlyTest extends TestCase {
    use DatabaseTransactions;
    
    private $current_max_water_tap_time_alarm_threshold_for_admin_value;

    public function test_max_water_tap_time_alarm_for_admins() {
        // setto momentaneamente una soglia di 60 minuti per MARINA_MAX_MINUTES_FOR_WATER_TIME_ALARM_FOR_ADMIN_USERS:
        // TODO: ho provato a modificare la voce in .env con putenv() non funziona. Affinchè il test non fallisca, al momento setto esplicitamente una soglia per l'allarme che sia coerente con le snapshot inviate:
        $this->current_max_water_tap_time_alarm_threshold_for_admin_value = config('marina.max_water_tap_time_alarm_threshold_for_admin_users_minutes');
        putenv("MARINA_MAX_MINUTES_FOR_WATER_TIME_ALARM_FOR_ADMIN_USERS=60");

        $berth = Berth::firstOrCreate([
            'id' => 364319, 
            'pontoon_name' => "Z1 Right",
            'berth_name' => "319",
            'description' => "Z1 Right 319",
            'socket_type' => Berth::DEFAULT_SOCKET_TYPE,
        ]);
        $berth->calculate_and_set_pedestal_id();
        $berth->save();

        // creo il primo snapshot: timestamp: 2h fa, la presa comunica ma è vergine e spenta, transponder_id nullo:
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($berth->id,
                                                                               Carbon::now()->subMinutes(120)->timestamp,
                                                                               BaseSocketSnapshot::LINK_STATUS_CONNECTED,
                                                                               BaseSocketSnapshot::SOCKET_STATUS_OFF,
                                                                               BaseSocketSnapshot::TRANSPONDER_ID_VALUE_RECEIVED_FROM_SMARTVIEW_WHEN_TRANSPONDER_IS_NULL,
                                                                               0,  
                                                                               0,
                                                                               0,
                                                                               0,
                                                                               0,
                                                                               1234567890,
                                                                               0,
                                                                               null);
        $this->get($socket_snapshot_url);

        // creo il secondo snapshot: timestamp: 1h59' fa, la presa è appena stata accesa dal transponder con id "ABCDEFGH", contatori ancora a zero:
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($berth->id,
                                                                               Carbon::now()->subMinutes(119)->timestamp,
                                                                               BaseSocketSnapshot::LINK_STATUS_CONNECTED,
                                                                               BaseSocketSnapshot::SOCKET_STATUS_ON,
                                                                               "ABCDEFGH",
                                                                               0,  
                                                                               0,
                                                                               0,  
                                                                               0,
                                                                               20,
                                                                               1234567890,
                                                                               100,
                                                                               null);
        $this->get($socket_snapshot_url);

        // creo il secondo snapshot: timestamp: 1h58' fa, la presa sta consumando acqua con il transponder con id "ABCDEFGH":
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($berth->id,
                                                                               Carbon::now()->subMinutes(118)->timestamp,
                                                                               BaseSocketSnapshot::LINK_STATUS_CONNECTED,
                                                                               BaseSocketSnapshot::SOCKET_STATUS_ON,
                                                                               "ABCDEFGH",
                                                                               0,  
                                                                               0,
                                                                               2000,  
                                                                               2000,
                                                                               18,
                                                                               1234567890,
                                                                               110,
                                                                               null);
        $this->get($socket_snapshot_url);

        // altro snapshot: timestamp: 1h49' fa, la presa sta ancora consumando acqua con il transponder con id "ABCDEFGH":
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($berth->id,
                                                                               Carbon::now()->subMinutes(109)->timestamp,
                                                                               BaseSocketSnapshot::LINK_STATUS_CONNECTED,
                                                                               BaseSocketSnapshot::SOCKET_STATUS_ON,
                                                                               "ABCDEFGH",
                                                                               0,  
                                                                               0,
                                                                               400000,  
                                                                               400000,
                                                                               15,
                                                                               1234567890,
                                                                               107,
                                                                               null);
        $this->get($socket_snapshot_url);

        // altro snapshot: timestamp: 1h fa, la presa sta ancora consumando acqua con il transponder con id "ABCDEFGH":
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($berth->id,
                                                                               Carbon::now()->subMinutes(60)->timestamp,
                                                                               BaseSocketSnapshot::LINK_STATUS_CONNECTED,
                                                                               BaseSocketSnapshot::SOCKET_STATUS_ON,
                                                                               "ABCDEFGH",
                                                                               0,  
                                                                               0,
                                                                               12000000,  
                                                                               12000000,
                                                                               10,
                                                                               1234567890,
                                                                               126,
                                                                               null);
        $this->get($socket_snapshot_url);

         // altro snapshot: timestamp: 59 minuti fa, la presa sta ancora consumando acqua con il transponder con id "ABCDEFGH":
         $timestamp = Carbon::now()->subMinutes(59);
         $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($berth->id,
                                                                                $timestamp->timestamp,
                                                                                BaseSocketSnapshot::LINK_STATUS_CONNECTED,
                                                                                BaseSocketSnapshot::SOCKET_STATUS_ON,
                                                                                "ABCDEFGH",
                                                                                0,  
                                                                                0,
                                                                                140000000,  
                                                                                140000000,
                                                                                9,
                                                                                1234567890,
                                                                                136,
                                                                                null);
        $this->get($socket_snapshot_url);

        $this->assertDatabaseHas('socket_snapshots', [
            'socket_id' => $berth->get_id(),
            'timestamp' => $timestamp->format((new BaseSocketSnapshot())->dateFormat),
            'transponder_id' => 'ABCDEFGH',
            'total_power_cnt' => 0,
            'postpaid_power_cnt' => 0,
            'total_water_cnt' => 140000000,
            'postpaid_water_cnt' => 140000000,
            'epower_id' => 1234567890,
        ]);

        $last_socket_snapshot_inserted = SocketSnapshot::where('socket_id', $berth->get_id())
                                                       ->where('timestamp', $timestamp->format((new BaseSocketSnapshot())->dateFormat))
                                                       ->where('transponder_id', 'ABCDEFGH')
                                                       ->where('total_power_cnt', 0)
                                                       ->where('postpaid_power_cnt', 0)
                                                       ->where('total_water_cnt', 140000000)
                                                       ->where('postpaid_water_cnt', 140000000)
                                                       ->where('epower_id', 1234567890)
                                                       ->orderBy('timestamp', 'desc')
                                                       ->orderBy('id', 'desc')
                                                       ->first();

        // TODO: il campo allarmi risulta essere null e l'assert seguente fallisce:
        $this->assertTrue(str_contains(strval($last_socket_snapshot_inserted->alarms), BaseSocketSnapshot::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_ADMINS));


        // altro snapshot: timestamp: 20 minuti fa, la presa è in allarme comunicazione:
        $timestamp = Carbon::now()->subMinutes(20);
        $socket_snapshot_url = BaseSocketSnapshot::compose_socket_snapshot_url($berth->id,
                                                                               $timestamp->timestamp,
                                                                               BaseSocketSnapshot::LINK_STATUS_NOT_CONNECTED,
                                                                               BaseSocketSnapshot::SOCKET_STATUS_NOT_CONNECTED,
                                                                               BaseSocketSnapshot::TRANSPONDER_ID_VALUE_RECEIVED_FROM_SMARTVIEW_WHEN_TRANSPONDER_IS_NULL,
                                                                               0,  
                                                                               0,
                                                                               0,  
                                                                               0,
                                                                               0,
                                                                               0,
                                                                               0,
                                                                               null);
        $this->get($socket_snapshot_url);

        // verifico se il flag di mancata comunicazione nel campo alarms viene settato:
        $this->assertDatabaseHas('socket_snapshots', [
            'socket_id' => $berth->get_id(),
            'timestamp' => $timestamp->format((new BaseSocketSnapshot())->dateFormat),
            'transponder_id' => BaseSocketSnapshot::TRANSPONDER_ID_VALUE_RECEIVED_FROM_SMARTVIEW_WHEN_TRANSPONDER_IS_NULL,
            'link_status' => BaseSocketSnapshot::LINK_STATUS_NOT_CONNECTED,
            'on_off_status' => BaseSocketSnapshot::SOCKET_STATUS_NOT_CONNECTED,
            'total_power_cnt' => 0,
            'postpaid_power_cnt' => 0,
            'total_water_cnt' => 0,
            'postpaid_water_cnt' => 0,
            'epower_id' => 0,
            'alarms' => BaseSocketSnapshot::ALARMS_FIELD_FLAG_NOT_CONNECTED
        ]);


        // TODO: devo anche verificare che gli snapshot inseriti prima dell'ultimo non abbiano l'allarme attivo


        // ripristino la soglia precedente per per MARINA_MAX_MINUTES_FOR_WATER_TIME_ALARM_FOR_ADMIN_USERS:
        // TODO: putenv non funziona, non mi è chiaro come mai: il valore della variabile d'ambiente non viene modificato. Affinchè il test non fallisca, al momento occorre settare esplicitamente una soglia per l'allarme che sia coerente con le snapshot inviate.
        putenv("MARINA_MAX_MINUTES_FOR_WATER_TIME_ALARM_FOR_ADMIN_USERS=".$this->current_max_water_tap_time_alarm_threshold_for_admin_value);
    }

    // TODO: testare gli altri allarmi
    
}
