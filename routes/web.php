<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    use Illuminate\Support\Facades\Route;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Controllers\Auth\EmailChangeController;
    use App\Http\Controllers\ConsumptionReportsController;

    Route::get('users/verify_email', [EmailChangeController::class, 'verify'])->name('user.email-change-verify');

    Auth::routes();

    Route::prefix('api/v1')->group(function () {
	// dato che queste routes sono in web.php, sono già protette per default dal middleware VerifyCsrfToken. Non ho bisogno di aggiungerlo:
        Route::post('socket/{socket_id}/switch_off', ['uses' => 'BerthController@schedule_action_on_socket_ajax',
                                                      'socket_id' => 'socket_id'])
             ->name('turn_off_socket');

        Route::post('socket/{socket_id}/reset', ['uses' => 'BerthController@schedule_action_on_socket_ajax',
                                                 'socket_id' => 'socket_id'])
             ->name('reset_socket');
        /*
        Route::post('socket/{socket_id}/switch_on', ['uses' => 'BerthController@schedule_action_on_socket_ajax',
                                                     'socket_id' => 'socket_id'])
             ->name('turn_on_socket');   // TODO: da abilitare quando si potranno accendere le prese da remoto
        */

        // TODO: devo mettere le due routes delle notifiche dentro routes/api.php? se lo faccio, devo pre-autenticarle con Passport o Sanctum. EDIT: in teoria non ne ho bisogno: dovrebbero essere già protette dal csrf token dato che sono in web.php. Ma se le invio da postman senza token sembrano funzionare lo stesso, a differenza delle routes di reset/switch off delle prese. Perchè?
        
        Route::get('users/{username}/unread_notifications_count', 'UserController@get_unread_notifications_count_via_ajax')
             ->name('get_unread_notifications_count_for_logged_user_via_ajax');

        Route::get('users/{username}/unread_notifications', 'UserController@get_notifications_via_ajax')
             ->name('get_notifications_for_logged_user_via_ajax');
    });

    

    /* routes per le API v0 (vecchie select.php e insert.php da SmartCloud 1.x, ancora in uso da SmartView e TagManager). NOTA: ho provato a spostarle in web/api.php ma non funzionano. */
    // TODO: le chiamate risultano parecchio lente: quasi 7 secondi per scrivere un socketsnapshot + subscriptionsnapshot quando entrambe queste tabelle sono vuote!! però anche solamente fermandomi all'inizio dell'insert_router con un die(), impiego quasi 1.7 secondi (poco più di 1.4 se lancio il caching delle views, della configurazione e delle routes). Come mai? Posso velocizzare in qualche modo?
    Route::get('/ws/insert', 'APIv0Controller@insert_router');
    Route::get('/ws/select', 'APIv0Controller@select_router');

    Route::group(['middleware' => 'auth'], function () {
    // routes filtrate dal middleware auth. L'utente non può accedervi se non è loggato:

        Route::get('password/expired', 'Auth\ExpiredPasswordController@expired')
             ->name('password.expired');
            
        Route::post('password/post_expired', 'Auth\ExpiredPasswordController@postExpired')
             ->name('password.post_expired');

        Route::group(['middleware' => 'EnsurePasswordIsNotExpired'], function () {

            Route::get('/home', 'PagesController@index')->name('home');
            Route::get('/', 'PagesController@index');
            Route::get('/my_subscriptions', 'UserController@my_subscriptions')->name('my_subscriptions');   // stato delle prese sottoscritte per gli utenti che le hanno
            Route::any('/subscriptions/{subscription_id}/alarm_thresholds', ['uses' => 'SubscriptionController@open_alarm_thresholds_page',
                                                                             'as' => 'open_alarm_thresholds_page']);
            Route::post('/subscriptions/{subscription_id}/write_alarm_thresholds', ['uses' => 'SubscriptionController@write_alarm_thresholds',
                                                                                    'as' => 'write_alarm_thresholds']);
            
            // TODO: bisognerebbe unificare le routes di calcolo dei consumi per i contratti e per i posti barca sotto lo stesso prefisso (siano esse report pdf/xls, pagine web, grafici, etc): /subscriptions/{subscription_id}/consumption/{berth_id}/{begin_epoch_timestamp}/{end_epoch_timestamp/{type=[pdf|xls|chart|web]}. In questo modo potrei anche fare ordine dentro a questo file raggruppandole sotto lo stesso route prefix
            
            // route per il report file del consumo di un contratto tra due timestamp personalizzati:
            // TODO: per il momento la metto come <any> per poterla testare, ma una volta ultimati i form che richiedono il report (che devono usare post), permettere solo il method post:
            Route::any('/subscriptions/{subscription_id}/consumption/{berth_id}/{begin_epoch_timestamp}/{end_epoch_timestamp}/report_file/{file_type}', 'SubscriptionController@get_consumption_report_file')
                ->name('get_subscription_consumption_on_berth_report_file');

            // route per la generazione di un grafico dei consumi di un contratto su un determinato posto barca in periodo personalizzato:
            // TODO: per il momento la metto come <any> per poterla testare, ma una volta ultimati i form che richiedono il grafico (che devono usare post), permettere solo il method post. EDIT: il problema è che arrivo alla route usando un redirect, e non sono ancora riuscito a redirectare passando i dati in post anzichè in get. Di conseguenza, se vincolo questa route ad essere chiamata in post, laravel dà exception perchè il redirect la chiama in get:
            Route::any('/subscriptions/{subscription_id}/{berth_id}/{begin_timestamp}/{end_timestamp}/{subject}/{sampling_interval_argument}/chart', 'SubscriptionController@get_consumption_chart')
                 ->name('get_subscription_consumption_chart_on_berth');

            // route per il check del consumo di una subscription da parte di un admin o dell'intestatario:
            Route::post('/subscriptions/{subscription_id}/consumption/summary', 'SubscriptionController@check_consumption')->name('subscription.check_consumption');

            Route::get('/notifications', 'UserController@open_notifications_page')->name('notifications_page');
            Route::get('/settings', 'UserController@open_user_settings_page')->name('open_user_settings_page');
            Route::post('/users/{user_id}/update_settings', ['uses' => 'UserController@set_user_settings_and_redirect', 'as' => 'set_user_settings']);

            Route::get('/consumption_reports', 'ConsumptionReportsController@selectReportSubjectTypeStep')
                 ->name('open_consumption_reports_page');

            Route::post('/consumption_reports', 'ConsumptionReportsController@handleFormSubmit')
                 ->name('handle_consumption_reports_form_submit');

            Route::group(['middleware' => 'redirect_to_home_if_logged_user_is_not_viewer_admin_full_admin_or_supervisor'], function () {
            // routes filtrate dal middleware redirect_to_home_if_logged_user_is_not_viewer_admin_full_admin_or_supervisor. L'utente non può accedervi se non è un viewer_admin, un full_admin o supervisore:
                Route::get('/plant_status', ['uses' => 'PlantController@plant_status_page', 'page' => 'list'])->name('plant_status');
                
                Route::get('/plant_map', ['uses' => 'PlantController@plant_map_page', 'page' => 'map'])->name('plant_map');
                
                Route::get('/users', 'UserController@index')->name('user_list');
                Route::get('/users/{user_id}/edit', 'UserController@edit')->name('edit_user');
                Route::get('/subscriptions', 'SubscriptionController@index')->name('subscription_list');
                Route::get('/berths', 'BerthController@index')->name('berth_list');

                // route per la generazione di un grafico consumi di un posto barca in periodo personalizzato:
                // TODO: per il momento la metto come <any> per poterla testare, ma una volta ultimati i form che richiedono il grafico (che devono usare post), permettere solo il method post EDIT: il problema è che arrivo alla route usando un redirect, e non sono ancora riuscito a redirectare passando i dati in post anzichè in get. Di conseguenza, se vincolo questa route ad essere chiamata in post, laravel dà exception perchè il redirect la chiama in get:
                Route::any('/berth_consumption/{berth_id}/{begin_timestamp}/{end_timestamp}/{subject}/{sampling_interval_argument}/chart', 'BerthController@get_consumption_chart')
                     ->name('get_berth_consumption_chart');

                // route per il report file del consumo di un posto barca tra due timestamp personalizzati:
                // TODO: per il momento la metto come <any> per poterla testare, ma una volta ultimati i form che richiedono il report (che devono usare post), permettere solo il method post EDIT: il problema è che arrivo alla route usando un redirect, e non sono ancora riuscito a redirectare passando i dati in post anzichè in get. Di conseguenza, se vincolo questa route ad essere chiamata in post, laravel dà exception perchè il redirect la chiama in get:
                Route::any('/berth_consumption/{berth_id}/{begin_epoch_timestamp}/{end_epoch_timestamp}/report_file/{file_type}', 'BerthController@get_consumption_report_file')->name('get_berth_consumption_report_file');

                // route per il report file del consumo dell'intero marina tra due timestamp personalizzati:
                // TODO: per il momento la metto come <any> per poterla testare, ma una volta ultimati i form che richiedono il report (che devono usare post), permettere solo il method post EDIT: il problema è che arrivo alla route usando un redirect, e non sono ancora riuscito a redirectare passando i dati in post anzichè in get. Di conseguenza, se vincolo questa route ad essere chiamata in post, laravel dà exception perchè il redirect la chiama in get:
                Route::any('/plant_consumption/{begin_epoch_timestamp}/{end_epoch_timestamp}/report_file/{file_type}', 'PlantController@get_consumption_report_file')->name('get_plant_consumption_report_file');

                // route per il report file del consumo di un singolo pontile tra due timestamp personalizzati:
                // TODO: per il momento la metto come <any> per poterla testare, ma una volta ultimati i form che richiedono il report (che devono usare post), permettere solo il method post EDIT: il problema è che arrivo alla route usando un redirect, e non sono ancora riuscito a redirectare passando i dati in post anzichè in get. Di conseguenza, se vincolo questa route ad essere chiamata in post, laravel dà exception perchè il redirect la chiama in get:
                Route::any('/pontoon_consumption/{pontoon_name}/{begin_epoch_timestamp}/{end_epoch_timestamp}/report_file/{file_type}', 'PontoonController@get_consumption_report_file')->name('get_pontoon_consumption_report_file');

                Route::get('/refunds_log', 'PlantController@open_refunds_log_page')->name('open_refunds_log_page');
            });

            Route::group(['middleware' => 'redirect_to_home_if_logged_user_is_not_full_admin_or_supervisor'], function () {
            // routes filtrate dal middleware redirect_to_home_if_logged_user_is_not_full_admin_or_supervisor. L'utente non può accedervi se non è un full_admin o supervisore:
                Route::get('/users/create', 'UserController@create')->name('create_user');
                Route::post('/users/store', 'UserController@store')->name('store_user');
                Route::post('/users/{user_id}/update', 'UserController@update')->name('update_user');
                Route::post('/users/{user_id}/enable', 'UserController@enable')->name('enable_user');
                Route::post('/users/{user_id}/disable', 'UserController@disable')->name('disable_user');

                /* metto :any() perchè
                    1. se supporto solo GET - quando si verifica un errore di validazione e il redirect mi rimanda indietro - vedo il token nell'url
                    2. se supporto solo POST - quando si verifica un errore di validazione e il redirect mi rimanda indietro - non posso tornare indietro perchè il redirect è una chiamata GET e non è supportata:
                */
                Route::any('/berths/{berth_id}/new_subscription', 'SubscriptionController@open_new_subscription_on_berth_form')->name('new_berth_subscription');

                Route::post('/subscriptions/create', 'SubscriptionController@create_new_subscription_on_berth')->name('create_new_subscription_on_berth');

                Route::post('/subscriptions/{subscription_id}/update_notes', ['as' => 'subscription.update_notes',
                                                                              'uses' => 'SubscriptionController@update_notes']);

                Route::post('/subscriptions/{subscription_id}/close', ['as' => 'subscription.close',
                                                                       'uses' => 'SubscriptionController@close']);

                Route::post('/subscriptions/{subscription_id}/add_payment', ['uses' => 'SubscriptionConsumptionPaymentsController@create',
                                                                             'as' => 'payment.create']);

                Route::post('/payments/{payment_id}/delete', ['as' => 'payment.delete',
                                                              'uses' => 'SubscriptionConsumptionPaymentsController@delete']);
                
                Route::get('/transponders/select_new_transponder_to_create_socket_subscriptions', 'TransponderController@select_new_transponder_to_create_socket_subscriptions')->name('select_new_transponder_to_create_socket_subscriptions');

                Route::post('/transponders/enable_to_socket_subscriptions_activation', 'TransponderController@enable_to_socket_subscriptions_activation')->name('enable_transponder_to_create_socket_subscriptions');
            });

            Route::group(['middleware' => 'redirect_to_home_if_logged_user_is_not_supervisor'], function () {
                // routes filtrate dal middleware redirect_to_home_if_logged_user_is_not_supervisor. L'utente non può accedervi se non è supervisore:
                Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');          // route per il log viewer TODO: tenerla o rimuoverla una volta raggiunto un certo livello di sicurezza sugli errori?
            });
        });
    });
