<?php
    use App\Http\Controllers\SubscriptionController;
    use App\Subscription;
    use App\User;
    use App\Berth;
?>
@extends('layouts.app_menu')

@section('custom_js')
    <script type='text/javascript' src="{{ URL::asset('js/subscriptions_consumption.js') }}"></script>
@endsection

@section('content')
    <div id='content' class='container mt-5 pt-5'>
        @include('fragments.flash_message')
        @if($epower_id_has_changed_during_interval === true)
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>
                    {{ ucfirst(__('subscriptions/consumption.epower_on_berth_has_been_changed_during_period_message')) }}
                </strong>
            </div>
        @endif
        <form id='data' class='mb-4 mx-1' method='post'>
        @csrf
            <div class='mb-5'>
                <input type='hidden' name='subscription_id' value='{{ $subscription->get_id() }}'/>
                <?php
                    $user = $subscription->user;
                    if(($user) && ($user->count() > 0)) {
                        $user_full_name = $user->get_full_name();
                        $boat = $user->boat;
                    } else {
                        $user_full_name = ucfirst(__('common.unknown_user'));
                        $boat = null;
                    }
                    $subscription_begin_timestamp_to_display = $subscription->get_begin_timestamp()
                                                                            ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                            ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'));
                    $subscription_end_timestamp_to_display = '-';
                    if(!$subscription->is_ongoing()) {
                        $subscription_end_timestamp_to_display = $subscription->get_end_timestamp()
                                                                              ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                              ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'));
                    }
                    $update_notes_btt_confirmation_request = ucfirst(__('subscriptions/consumption.update_subscription_notes_btt_confirmation_request_caption'));
                    $end_subscription_btt_confirmation_request = ucfirst(__('subscriptions/consumption.end_subscription_btt_confirmation_request_caption'));
                ?>
                <div class='form-group row'>
                    <label for='user_name' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                        {{ ucfirst(__('subscriptions/consumption.user_name_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10 col-lg-8'>
                        <?php // TODO: aggiungere lo username in uno span azzurro, come sulle altre views? ?>
                        <input type='text' readonly class='form-control-plaintext' id='user_name' value='{{ $user_full_name }}'/>
                    </div>
                </div>
                @if(($user) && ($user->count() > 0))
                    <?php $boat = $user->boat; ?>
                    @if(($boat) && ($boat->count() > 0))
                        <div class='form-group row'>
                            <label for='boat_name' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                                {{ ucfirst(__('subscriptions/consumption.boat_name_field_label_caption')) }}
                            </label>
                            <div class='col-sm-10 col-lg-8'>
                                <input type='text' readonly class='form-control-plaintext' id='boat_name' value='{{ $boat->get_name() }}'/>
                            </div>
                        </div>
                    @endif
                @endif
                @if($subscription->is_bound_to_socket())
                    <?php
                        $berth = $subscription->berth;
                        if(($berth) && ($berth->count() > 0)) {
                            $berth_name = $berth->get_description();
                        } else {
                            $berth_name = ucfirst(__('common.unknown_berth'));        // TODO: evidenziarlo in qualche modo?
                        }
                    ?>
                    <div class='form-group row'>
                        <label for='berth_name' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                            {{ ucfirst(__('subscriptions/consumption.berth_name_field_label_caption')) }}
                        </label>
                        <div class='col-sm-10 col-lg-8'>
                            <?php // TODO: formattare come sulle altre views, ovvero in uno span azzurro? ?>
                            <input type='text' readonly class='form-control-plaintext' id='berth_name' value='{{ $berth_name }}'/>
                        </div>
                    </div>
                    @if((($berth) && ($berth->count() > 0)) && 
                        (($berth_socket_type = $berth->get_socket_type()) !== Berth::SOCKET_TYPE_NOT_DEFINED))
                        <div class='form-group row'>
                            <label for='socket_type' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                                {{ ucfirst(__('subscriptions/consumption.socket_type_field_label_caption')) }}
                            </label>
                            <div class='col-sm-10 col-lg-8'>
                                <input type='text' readonly class='form-control-plaintext' id='socket_type'
                                    value='{{ $berth_socket_type }}'/>
                            </div>
                        </div>
                    @endif
                @endif
                <div class='form-group row'>
                    <?php
                        $transponder_id = "-";
                        if(!is_null($subscription->transponder) && ($subscription->transponder->count() > 0)) {
                            $transponder_id = $subscription->transponder->get_code();
                        }
                    ?>
                    <label for='transponder_id' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                        {{ ucfirst(__('subscriptions/consumption.subscription_transponder_code_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10 col-lg-8'>
                        <input type='text' readonly class='form-control-plaintext' id='transponder_code' value='{{ $transponder_id }}' />
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='subscription_type' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                        {{ ucfirst(__('subscriptions/consumption.subscription_type_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10 col-lg-8'>
                        <?php 
                            if($subscription->is_prepaid()) {
                                $subscription_type = ucfirst(__('subscriptions/consumption.subscription_type_field_prepaid_value_caption'));
                            } else {
                                $subscription_type = ucfirst(__('subscriptions/consumption.subscription_type_field_postpaid_value_caption'));
                            }
                        ?>
                        <input type='text' readonly class='form-control-plaintext' id='subscription_type' value='{{ $subscription_type }}'/>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='subscription_notes' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                        {{ ucfirst(__('subscriptions/consumption.subscription_notes_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10 col-lg-8'>
                        <textarea id='subscription_notes' 
                                  name='subscription_notes' 
                                  class='form-control'
                                  {{ !Auth::user()->is_full_admin() ? 'disabled' : '' }}>{{ $subscription->get_notes() }}</textarea>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='subscription_begin_timestamp' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                        {{ ucfirst(__('subscriptions/consumption.subscription_begin_timestamp_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10 col-lg-8'>
                        <input type='text' readonly class='form-control-plaintext' id='subscription_begin_timestamp'
                               value='{{ $subscription_begin_timestamp_to_display }}'/>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='subscription_end_timestamp' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                        {{ ucfirst(__('subscriptions/consumption.subscription_end_timestamp_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10 col-lg-8'>
                        <input type='text' readonly class='form-control-plaintext' id='subscription_end_timestamp'
                               value='{{ $subscription_end_timestamp_to_display }}'/>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='subscription_power_consumption' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                        {{ ucfirst(__('subscriptions/consumption.subscription_power_consumption_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10 col-lg-8'>
                        <input type='text' readonly class='form-control-plaintext' id='subscription_power_consumption'
                            value='{{ number_format($subscription_power_consumption, 1, '.', '') }} kWh'/>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='subscription_power_consumption' class='col-sm-10 col-lg-4 col-form-label font-weight-bold'>
                        {{ ucfirst(__('subscriptions/consumption.subscription_water_consumption_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10 col-lg-8'>
                        <input type='text' readonly class='form-control-plaintext' id='subscription_water_consumption'
                            value='{{ number_format($subscription_water_consumption, 3, '.', '') }} m³'/>
                    </div>
                </div>
            </div>
            <div id='buttons' class='text-center'>
                <?php
                    /* TODO: Non riesco a fare in modo che i button diventino elementi block-level solo su mobile usando bootstrap. Per il momento ho risolto con del css custom per la classe full_width_on_mobile. Potrebbe essere colpa dell'elemento <a class='btn'> perchè non è dello stesso tipo degli altri? Provare a sostituirlo con un button? */
                    /* TODO: i buttons vanno a capo perchè eccedono la larghezza del div. Risolvere. */
                ?>
                <a class='btn btn-lg btn-success full_width_on_mobile mb-2 mr-2' href='{{ route("subscription_list") }}'>
                    {{ ucfirst(__('subscriptions/consumption.back_btt_caption')) }}
                </a>
                @if($subscription->is_postpaid())
                    @if($subscription->payments()->count() > 0)
                        <button type='button' id='show-payments-btt'
                                class='btn btn-lg btn-primary full_width_on_mobile mb-2 mr-2'
                                onclick="change_payments_div_visibility_payments_button_caption('{{ ucfirst(__('subscriptions/consumption.show_payments_history_btt_caption')) }}', '{{ ucfirst(__('subscriptions/consumption.hide_payments_history_btt_caption')) }}', '{{ ucfirst(__('subscriptions/consumption.add_payment_btt_caption')) }}');">
                            {{ ucfirst(__('subscriptions/consumption.show_payments_history_btt_caption')) }}
                        </button>
                    @endif
                    @if(Auth::user()->is_full_admin())
                        <button type='button' id='show-new-payment-div-btt' class='btn btn-lg btn-orange full_width_on_mobile mb-2 mr-2'
                                onclick="change_new_payment_div_visibility_new_payment_button_caption('{{ ucfirst(__('subscriptions/consumption.add_payment_btt_caption')) }}', '{{ ucfirst(__('subscriptions/consumption.hide_add_payment_form_btt_caption')) }}', '{{ ucfirst(__('subscriptions/consumption.show_payments_history_btt_caption')) }}');">
                            {{ ucfirst(__('subscriptions/consumption.add_payment_btt_caption')) }}
                        </button>
                    @endif
                @endif
                @if(Auth::user()->is_full_admin())
                    <button type='submit'class='btn btn-lg btn-purple full_width_on_mobile mb-2 mr-2'
                            formaction="{{ route('subscription.update_notes', ['subscription_id' => $subscription->get_id()]) }}"                        
                            onclick='return(confirm("{{ $update_notes_btt_confirmation_request }}"));'>
                        {{ ucfirst(__('subscriptions/consumption.submit_changes_btt_caption')) }}
                    </button>
                    @if(($subscription->is_ongoing()) && ($subscription->is_bound_to_socket()))
                        <button type='submit' class='btn btn-lg btn-danger full_width_on_mobile mb-2 mr-2'
                                formaction="{{ route('subscription.close', ['subscription_id' => $subscription->get_id()]) }}"
                                onclick='return(confirm("{{ $end_subscription_btt_confirmation_request }}"));'>
                            {{ ucfirst(__('subscriptions/consumption.end_subscription_btt_caption')) }}
                        </button>
                    @endif
                @endif
            </div>
        </form>
        @if($subscription->is_postpaid())
            <table id='payments-history' class='container mt-4 d-none'>
            <?php // TODO: probabilmente vogliono poter inserire i pagamenti usando la valuta, non i kwh e i m3 consumati ?>
                <tr>
                    <th class='text-center'>
                        {{ ucfirst(__('subscriptions/consumption.payments_form_payment_timestamp_header_caption')) }}
                    </th>
                    <th class='text-center'>
                        {{ ucfirst(__('subscriptions/consumption.payments_form_paid_power_amount_header_caption')) }}
                    </th>
                    <th class='text-center'>
                        {{ ucfirst(__('subscriptions/consumption.payments_form_paid_water_amount_header_caption')) }}
                    </th>
                    <th class='text-center'>
                        {{ ucfirst(__('subscriptions/consumption.payments_form_operator_username_header_caption')) }}
                    </th>
                    <th class='text-center'>
                    </th>
                </tr>
                @if($subscription->payments()->count() > 0)
                    @foreach($subscription->payments as $payment)
                        <?php
                            $payment_timestamp_to_show = $payment->get_timestamp()
                                                                 ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                 ->format(config('dates_currency_and_locale.local_datetime_format_short_carbon'));
                        ?>
                        <tr>
                            <td class='text-center'>
                                {{ $payment_timestamp_to_show }}
                            </td>
                            <td class='text-center'>
                                {{ number_format($payment->get_paid_power_amount_kwh(), 1) }}
                            </td>
                            <td class='text-center'>
                                {{ number_format($payment->get_paid_water_amount_cm(), 3) }}
                            </td>
                            <td class='text-center'>
                                {{ $payment->operator->get_username() }}
                            </td>
                            <td class='text-center'>
                                @if(Auth::user()->is_full_admin())
                                    <?php
                                        $payment_delete_btt_tooltip = ucfirst(__('subscriptions/consumption.payments_form_delete_payment_btt_tooltip'));
                                        $payment_delete_confirmation_request = ucfirst(__('subscriptions/consumption.payments_form_delete_payment_btt_confirmation_request_caption'));
                                    ?>
                                    <form method='post' action="{{ route('payment.delete', ['payment_id' => $payment->id]) }}">
                                    @csrf
                                        <button type='submit' class='btn btn-danger btn-sm mb-2'
                                                data-toggle='tooltip' data-placement='top' title='{{ $payment_delete_btt_tooltip }}'
                                                onclick='return confirm("{{ $payment_delete_confirmation_request }}")'>
                                            <i class='fas fa-trash fa-sm'></i>
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <?php // TODO ?>
                @endif
            </table>
            @if(Auth::user()->is_full_admin())
                <div id='new-payment' class='mt-4 d-none'>
                    <?php // TODO: probabilmente vogliono poter inserire i pagamenti usando la valuta, non i kwh e i m3 consumati ?>
                    <form method='post' action='{{ route("payment.create", ["subscription_id" => $subscription->get_id()]) }}'>
                    @csrf
                        <div class='form-group row text-center'>
                            <div class='col'>
                                {{ ucfirst(__('subscriptions/consumption.payments_form_paid_power_amount_header_caption')) }}
                            </div>
                            <div class='col'>
                                {{ ucfirst(__('subscriptions/consumption.payments_form_paid_water_amount_header_caption')) }}
                            </div>
                            <div class='col'>
                            </div>
                        </div>
                        <?php
                            $add_payment_confirmation_request = ucfirst(__('subscriptions/consumption.new_payment_form_add_payment_btt_confirmation_request_caption'));
                            $add_payment_btt_tooltip_when_disabled = ucfirst(__('subscriptions/consumption.add_payment_btt_tooltip_paid_amount_has_to_be_greater_than_zero_to_submit_a_payment'));
                        ?>
                        <div class='form-group row text-center'>
                            <div class='col'>
                                <input type='number' class='form-control' id='paid_power_consumption_amount' name='paid_power_consumption_amount'
                                    oninput='if(paid_amount_is_greater_than_zero()) { enable_add_payment_button_and_disable_tooltip() } else { disable_add_payment_button_and_enable_tooltip("{{ $add_payment_btt_tooltip_when_disabled }}") }'
                                    min='0' max='<?php // TODO ?>' value='0' />&nbsp;kWh
                            </div>
                            <div class='col'>
                                <input type='number' class='form-control' id='paid_water_consumption_amount' name='paid_water_consumption_amount'
                                    oninput='if(paid_amount_is_greater_than_zero()) { enable_add_payment_button_and_disable_tooltip() } else { disable_add_payment_button_and_enable_tooltip("{{ $add_payment_btt_tooltip_when_disabled }}") }'
                                    min='0' max='<?php // TODO ?>' value='0' />&nbsp;m<sup>3</sup>
                            </div>
                            <div class='col'>
                                <button id='add_payment_btn' type='submit' class='btn btn-primary' disabled title='{{ $add_payment_btt_tooltip_when_disabled }}'
                                        onclick='return confirm("{{ $add_payment_confirmation_request }}");'>
                                    {{ ucfirst(__('subscriptions/consumption.new_payment_form_add_payment_btt_caption')) }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
        @endif
    </div>
@endsection
