@extends('layouts.app_menu')

@section('content')
    <?php /* TODO: convertire i dati di consumo energia in kW o Ah a seconda della preferenza dell'utente. Solo per le soglie o anche per i consumi visualizzati? controllare su SmartCloud 1.x */ ?>
    <div id='content' class='mt-4 pt-4'>
        <?php
            $current_min_power_threshold = $current_max_power_threshold = $current_min_credit_threshold = $current_water_tap_time_threshold_minutes = 0;
            $um_to_display_for_power_thresholds = 'kWh';
            $max_value_for_power_alarm_thresholds = number_format(config("marina.max_power_consumption_value_for_alarm_threshold"), 1);
            if(!is_null($alarm_thresholds)) {
                $current_min_power_threshold = $alarm_thresholds->min_power_threshold_kwh;
                $current_max_power_threshold = $alarm_thresholds->max_power_threshold_kwh;
                $current_min_credit_threshold = $alarm_thresholds->min_credit_threshold;
                $current_water_tap_time_threshold_minutes = $alarm_thresholds->max_water_tap_time_threshold_minutes;
            }
            if(Auth::user()->wants_power_alarm_thresholds_in_ampere() === true) {
            // se l'utente ha settato gli Ah come unità di misura preferita per le soglie allarmi nelle preferenze:
                // converto i valori delle soglie prima di visualizzarli:
                // TODO: meglio parametrizzare il valore "230"? Anche perchè lo uso anche nel metodo del SubscriptionController che scrive le soglie sul db, se devo riconvertirle da Ah a kWh
                // TODO: la conversione fatta così vale per tutti i tipi di presa? O per prese più grosse o più piccole di un tot devo convertire diversamente?
                $current_min_power_threshold = number_format($current_min_power_threshold / 230, 3);
                $current_max_power_threshold = number_format($current_max_power_threshold / 230, 3);
                // adeguo il limite massimo da utilizzare per gli elementi input delle soglie di consumo energia: 
                $max_value_for_power_alarm_thresholds = number_format(config("marina.max_power_consumption_value_for_alarm_threshold") / 230, 3);
                // e modifico l'unità di misura da visualizzare:
                $um_to_display_for_power_thresholds = 'Ah';
            } else {
            // se la preferenza dell'utente per l'unità di misura da usare per le soglie allarmi è kWh:
                // le prendo pari pari dal db:
                $current_min_power_threshold = number_format($current_min_power_threshold, 1);
                $current_max_power_threshold = number_format($current_max_power_threshold, 1);
            }
            $current_min_credit_threshold = number_format($current_min_credit_threshold, 2);  // TODO: la valuta ha sempre due decimali?
            $current_water_tap_time_threshold_minutes = intval($current_water_tap_time_threshold_minutes);
            $submit_confirmation_request = ucfirst(__('subscriptions/alarm_thresholds.submit_confirmation_request'));
            // TODO: se la subscription è postpagata, devo saltare la visualizzazione la label e l'input del min_credit!
        ?>
        <div class='container pt-5'>
            <form action="{{ route('write_alarm_thresholds', $subscription->id) }}" method='post'>
            @csrf
                <?php /* TODO: devo sistemare le dimensioni degli input per la visualizzazione desktop (prendono tutta la larghezza della pagina) e delle label per la visualizzazione mobile (eccedono la larghezza del viewport)- Valutare di fare la tabella come fatto su smartcloud 1.x? */ ?>
               <div class="form-group">
                    <label for='min_power_consumption_threshold'>
                        {{ ucfirst(__('subscriptions/alarm_thresholds.min_power_threshold_um_caption', ['um' => $um_to_display_for_power_thresholds]))}}
                    </label>
                    <input id='min_power_consumption_threshold' name='min_power_consumption_threshold' class='form-control'
                            type='number' min='0' max='{{ $max_value_for_power_alarm_thresholds }}' step='0.1'
                            value='{{ $current_min_power_threshold }}' />
                    <?php /* TODO: bisognerà controllare anche lato server che il valore rientri nel range */ ?>
                </div>
                <div class="form-group">
                    <label for='max_power_consumption_threshold'>
                        {{ ucfirst(__('subscriptions/alarm_thresholds.max_power_threshold_um_caption', ['um' => $um_to_display_for_power_thresholds]))}}
                    </label>
                    <input id='max_power_consumption_threshold' name='max_power_consumption_threshold' class='form-control'
                           type='number' min='0' max='{{ $max_value_for_power_alarm_thresholds }}' step='0.1' 
                           value='{{ $current_max_power_threshold }}' />
                    <?php /* TODO: bisognerà controllare anche lato server che il valore rientri nel range */ ?>
                </div>
                <div class="form-group">
                    <label for='min_credit_threshold'>
                        {{ ucfirst(__('subscriptions/alarm_thresholds.min_credit_threshold_currency_caption', ['currency' => config('dates_currency_and_locale.currency_character') ])) }}
                    </label>
                    <input id='min_credit_threshold' name='min_credit_threshold' class='form-control'
                            type='number' min='0' max='{{ config("marina.max_socket_credit_value_for_alarm_threshold") }}' step='0.01'
                            value='{{ $current_min_credit_threshold }}' />
                    <?php /* TODO: bisognerà controllare anche lato server che il valore rientri nel range */ ?>
                </div>
                <div class="form-group">
                    <label for='max_water_time_threshold'>
                        {{ ucfirst(__('subscriptions/alarm_thresholds.max_water_tap_time_threshold_minutes_caption')) }}
                    </label>
                    <input id='max_water_time_threshold' name='max_water_time_threshold' class='form-control'
                            type='number' min='0' max='{{ config("marina.max_water_tap_time_alarm_minutes_value_for_alarm_threshold") }}' step='1'
                            value='{{ $current_water_tap_time_threshold_minutes }}' />
                    <?php /* TODO: bisognerà controllare anche lato server che il valore rientri nel range */ ?>
                </div>
                <button type="submit" class="btn btn-primary" onclick="return confirm('{{ $submit_confirmation_request }}');">
                    {{ ucfirst(__('subscriptions/alarm_thresholds.submit_btt_caption')) }}
                </button>
            </form>
        </div>
    </div>
@endsection
