<?php
    use App\Http\Controllers\PagesController;
    use App\Berth;
?>
@extends('layouts.app_menu')

@section('custom_css')
    <link href="{{ URL::asset('css/subscriptions_index.css') }}" rel='stylesheet'>
@endsection

@section('custom_js')
    <script type="text/javascript" src="{{ URL::asset('js/subscriptions_index.js') }}"></script>
    <script>
        window.onload = function() {
        // appena la pagina viene caricata:
            if(!window.matchMedia || (window.matchMedia("(min-width: 768px)").matches)) {
            // a meno che non si stia visualizzando il sito da mobile:
                // abilito i tooltip:
                $('[data-toggle="tooltip"]').tooltip();
            }
            // pulisco i campi dei filtri dal contenuto rimasto prima dell'eventuale refresh della pagina, poi li applico:
            reset_filters();
            apply_filters();
        };
    </script>
@endsection

@section('content')
    <?php // TODO: i viewer_admin devono poter vedere i dati ma non modificarli ?>
    <div id='content' class='mt-5 pt-5'>
        @include('fragments.flash_message')
        <form id='filters_bar' class='form-horizontal px-2 py-1'>
        @csrf
            <?php // TODO: mancano i pulsanti per l'ordinamento, devo metterli? ?>
            <div class='card bg-light border-primary rounded mb-3'>
                <div class='card-body'> <?php // TODO: calare il padding (tra border e filtri) + aumentare il margin (tra border e fine pagina)?>
                    <div class='card-title'>
                        {{ ucfirst(__('common.filters_bar_title')) }}
                    </div>
                    <div class='p-3 m-3 form-row text-center'>
                        <?php // TODO: come faccio a cambiare il colore del margine azzurro degli input quando hanno il focus? ?>
                        <div class='form-group col-md-6'>
                            <input type='text' id='fulltext_filter_input' class='filter-input form-control' onkeyup='apply_filters();'
                                    placeholder='{{ ucfirst(__('subscriptions/index.fulltext_filter_caption')) }}'>
                        </div>
                        <div class='form-group col-md-6'>
                            <select id='combo_filter_input' class='filter-input custom-select' onchange='apply_filters();'>
                                <?php
                                    // TODO: occhio che se cambio i valori degli attributi id/value delle options, che attualmente sono definiti nel PagesController, i cambiamenti non si riflettono sul js che gestisce il filtro. In caso di necessità, ricordare che là devo modificarli a mano.
                                    // TODO: definire la option di default
                                ?>
                                <option id='{{ PagesController::SUBSCRIPTIONS_COMBO_FILTER_ACTIVE_SUBSCRIPTIONS_ONLY }}'
                                        value='{{ PagesController::SUBSCRIPTIONS_COMBO_FILTER_ACTIVE_SUBSCRIPTIONS_ONLY }}'>
                                    {{ ucfirst(__('subscriptions/index.combo_filter_active_subscriptions_only_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::SUBSCRIPTIONS_COMBO_FILTER_ENDED_SUBSCRIPTIONS_ONLY }}'
                                        value='{{ PagesController::SUBSCRIPTIONS_COMBO_FILTER_ENDED_SUBSCRIPTIONS_ONLY }}'>
                                    {{ ucfirst(__('subscriptions/index.combo_filter_ended_subscriptions_only_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::SUBSCRIPTIONS_COMBO_FILTER_ALL_SUBSCRIPTIONS }}'
                                        value='{{ PagesController::SUBSCRIPTIONS_COMBO_FILTER_ALL_SUBSCRIPTIONS }}'>
                                    {{ ucfirst(__('subscriptions/index.combo_filter_all_subscriptions_option_caption')) }}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @if(($subscriptions) && ($subscriptions->count() > 0))
            <?php
                // TODO: usare le responsive data tables come facevo sulla prima bozza di smartcloud 2? (https://css-tricks.com/responsive-data-tables/) (come le integro con Bootstrap?):
            ?>
            <div class='container table-responsive'>
                <table class='table table-striped table-hover'>
                    <thead>
                        <tr>
                            <th class='text-center'>{{ ucfirst(__('subscriptions/index.th_transponder')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('subscriptions/index.th_berth_name')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('subscriptions/index.th_user')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('subscriptions/index.th_boat')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('subscriptions/index.th_begin_date')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('subscriptions/index.th_end_date')) }}</th>
                            <th class='text-center'></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($subscriptions as $subscription)
                        @if(($subscription) && ($subscription->count() > 0))
                            <?php
                                $berth = $subscription->berth;
                                $user = $subscription->user;
                                $boat_name = '-';
                                $boat_size_tooltip = '';
                                if(($user) && ($user->count() > 0)) {
                                    $boat = $user->boat;
                                    if(($boat) && ($boat->count() > 0)) {
                                        $boat_name = $boat->get_name();
                                        $boat_size_tooltip = strtolower(__('common.boat_measurements_tooltip_caption', ['length' => $boat->get_length(),
                                                                                                                        'width' => $boat->get_width(),
                                                                                                                        'depth' => $boat->get_depth()]));
                                    }
                                }
                                if(!is_null($subscription->get_begin_timestamp())) {
                                    $subscription_begin_dt_to_display = $subscription->get_begin_timestamp()
                                                                                     ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                                     ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'));
                                    } else {
                                        $subscription_begin_dt_to_display = '-';
                                    }
                                if(!is_null($subscription->get_end_timestamp())) {
                                    $subscription_end_dt_to_display = $subscription->get_end_timestamp()
                                                                                   ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                                   ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'));
                                } else {
                                    $subscription_end_dt_to_display = '-';
                                }
                            ?>
                            <tr class='subscription_row'>
                                <td class='text-center'>
                                    @if($subscription->transponder()->count() > 0)
                                        <span class='transponder_name text-light bg-info p-2 rounded transponder_code_outer text-nowrap'>       <?php // TODO: cambiare la formattazione, dato che l'ho cambiata anche sulle views plant_status e my_subscriptions? ?>
                                            <img src="{{ URL::asset('resources/transponder_icon_white.png') }}" class='transponder_btt_img'>
                                            <span class='transponder_code_inner'>
                                                {{ $subscription->transponder->code }}
                                            </span>
                                        </span>
                                    @else
                                        <span class='unknown_transponder_span transponder_code_inner'>
                                            -
                                        </span>
                                    @endif
                                </td>
                                <td class='text-center'>
                                    @if(($subscription->is_bound_to_socket()) && ($berth) && ($berth->count() > 0))
                                        <span class='socket_name text-light bg-info p-2 rounded berth_name_outer text-nowrap'>                     <?php // TODO: cambiare la formattazione, dato che l'ho cambiata anche sulle views plant_status e my_subscriptions? ?>
                                            <i class='fas fa-anchor'></i>
                                            <span class='berth_name_inner'>
                                                {{ $berth->get_description() }}
                                            </span>
                                        </span>
                                    @else
                                        <span class='unknown_berth_span berth_name_inner'>
                                            -
                                        </span>
                                    @endif
                                </td>
                                @if(($user) && ($user->count() > 0))
                                    <td class='text-center user_full_name'>
                                        {{ $user->get_full_name() }}
                                        @if(!is_null($user->get_username()))
                                            <br>
                                            <span class='text-light bg-primary p-1 rounded user_username'>
                                                {{ $user->get_username() }}
                                            </span>
                                        @endif
                                    </td>
                                    <td @if(($boat) && ($boat->count() > 0))
                                            title='{{ $boat_size_tooltip }}'
                                        @endif
                                        class='text-center user_boat_name' data-toggle='tooltip' data-placement='top'>
                                        {{ $boat_name }}
                                    </td>
                                @else   <?php // TODO: l'else serve, a parte durante i test? se la subscription esiste, l'utente ci deve essere per forza, a meno che non sia stato eliminato ?>
                                    <td class='text-center user_full_name'>-</td>
                                    <td class='text-center user_boat_name'>-</td>
                                @endif
                                <td class='text-center subscription_begin_timestamp'>
                                    {{ $subscription_begin_dt_to_display }}
                                </td>
                                <td class='text-center subscription_end_timestamp'>
                                    {{ $subscription_end_dt_to_display }}
                                </td>
                                <td class='text-center'>
                                    <?php
                                        if($subscription->is_ongoing()) {
                                            $check_subscription_consumption_btt_class = 'btn-primary';
                                        } else {
                                            $check_subscription_consumption_btt_class = 'check_consumption_btn_for_ended_subscriptions';
                                        }
                                    ?>
                                    <form method='post' action='{{ route('subscription.check_consumption', $subscription->get_id()) }}'>
                                    @csrf
                                        <button type='submit' class='btn {{ $check_subscription_consumption_btt_class }}'>
                                            {{ strtoupper(__('common.check_subscription_consumption_btt_caption')) }}
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    <div id='no_entries_message' class='container-fluid mt-5 pt-5 text-center d-none'>
        <h6 class='text-secondary text-uppercase font-weight-bold'>
            {{ ucfirst(__('subscriptions/index.no_subscriptions_on_database_or_no_subscription_matches_search_criteria')) }}
        </h6>
    </div>
@endsection
