<?php
    use App\Http\Controllers\UserController;
    use Illuminate\Support\Facades\Auth;

    if(isset($user)) {
        if(Auth::user()->is_full_admin()) {
            $form_action = route('update_user', ['user_id' => $user->get_id()]);
            $form_method = 'post';
            $submit_btt_caption = ucfirst(__('users/edit.submit_btt_update_caption'));
            $submit_confirmation_request_msg = ucfirst(__('users/edit.update_confirmation_request'));
            $on_submit_form_attribute = "onsubmit=\'return confirm('".$submit_confirmation_request_msg."');\'";
        } else {
            $form_action = route('user_list');
            $form_method = 'get';
            $submit_btt_caption = ucfirst(__('users/edit.submit_btt_back_to_manage_users_page_caption'));
            $on_submit_form_attribute = "";
        }
    } else {
        $form_action = route('store_user');
        $form_method = 'post';
        $submit_btt_caption = ucfirst(__('users/edit.submit_btt_create_caption'));
        $submit_confirmation_request_msg = ucfirst(__('users/edit.create_confirmation_request'));
        $on_submit_form_attribute = "";
    }
?>
@extends('layouts.app_menu')

@section('custom_css')
    <link href="{{ URL::asset('css/users_edit.css') }}" rel='stylesheet'>
@endsection

@section('content')
    <?php // TODO: la validazione dati lato client e lato server manca/è da terminare. ?>
    <?php 
        // i viewer_admin devono poter vedere i dati ma non modificarli:
        $disable_all_input_elements_if_logged_user_is_not_a_full_admin = Auth::user()->is_full_admin() ? '' : 'disabled';
    ?>
    <div id='content' class='container mt-5 pt-5'>
        <form method='{{ $form_method }}' action='{{ $form_action }}' {{ $on_submit_form_attribute }}>
        @csrf
            @if(isset($user))
                <input type='hidden' name='id' value='{{ $user->get_id() }}' />
            @endif
            <div class='form-group row'>
                <label for='last_name' class='col-sm-2 col-form-label'>
                    {{ ucfirst(__('users/edit.user_last_name_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='text' 
                           class='form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}'
                           id='last_name'
                           name='last_name'
                           placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.user_last_name_field_label_caption')) : "" }}'
                           value='{{ isset($user) ? $user->get_last_name() : "" }}' {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                    <span class='invalid-feedback' role='alert'>
                        @error('last_name')
                            <strong>{{ $message }}</strong>
                        @enderror
                    </span>
                </div>
            </div>
            <div class='form-group row'>
                <label for='first_name' class='col-sm-2 col-form-label'>
                    {{ ucfirst(__('users/edit.user_first_name_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='text' 
                           class='form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}'
                           id='first_name' 
                           name='first_name'
                           placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.user_first_name_field_label_caption')) : "" }}'
                           value='{{ isset($user) ? $user->get_first_name() : "" }}' 
                           {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                    <span class='invalid-feedback' role='alert'>
                        @error('first_name')
                            <strong>{{ $message }}</strong>
                        @enderror
                    </span>
                </div>
            </div>
            <div class='form-group row'>
                <label for='username' class='col-sm-2 col-form-label'>
                    {{ ucfirst(__('users/edit.user_username_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='text' 
                           class='form-control {{ $errors->has('username') ? 'is-invalid' : '' }}' 
                           id='username'
                           name='username'
                           placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.user_username_field_label_caption')) : "" }}'
                           value='{{ (isset($user) && (!is_null($user->get_username()))) ? $user->get_username() : "" }}'
                           {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                    <span class='invalid-feedback' role='alert'>
                        @error('username')
                            <strong>{{ $message }}</strong>
                        @enderror
                    </span>
                </div>
            </div>
            @if(isset($user) && !is_null($user->password))
            <?php // mostro il campo dell'email solo se non sto inserendo un nuovo utente e se l'utente ha già completato la registrazione, perchè altrimenti, l'email dovrà essere decisa direttamente dall'utente quando si registra ?>
                <div class='form-group row'>
                    <label for='email' class='col-sm-2 col-form-label'>
                        {{ ucfirst(__('users/edit.user_email_address_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10'>
                        <input type='email' 
                               class='form-control {{ $errors->has('email') ? 'is-invalid' : '' }}'
                               id='email'
                               name='email'
                               placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.user_email_address_field_label_caption')) : "" }}'
                               value='{{ (isset($user) && (!is_null($user->get_email_address()))) ? $user->get_email_address() : "" }}'
                            {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                        <span class='invalid-feedback' role='alert'>
                            @error('email')
                                <strong>{{ $message }}</strong>
                            @enderror
                        </span>
                    </div>
                </div>
            @endif
            <div class='form-group row'>
                <label for='phone' class='col-sm-2 col-form-label'>
                    {{ ucfirst(__('users/edit.user_phone_number_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='tel' 
                           class='form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}' 
                           id='phone' 
                           name='phone'
                           placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.user_phone_number_field_label_caption')) : "" }}'
                           value='{{ (isset($user) && (!is_null($user->get_phone_number()))) ? $user->get_phone_number() : "" }}'
                           {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                    <span class='invalid-feedback' role='alert'>
                        @error('phone')
                            <strong>{{ $message }}</strong>
                        @enderror
                    </span>
                </div>
            </div>
            <div class='form-group row'>
                <label for='notes' class='col-sm-2 col-form-label'>
                    {{ ucfirst(__('users/edit.user_notes_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <textarea class='form-control' id='notes' name='notes' 
                              placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.user_notes_field_label_caption')) : "" }}' 
                              {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>{{ (isset($user) && (!is_null($user->get_notes()))) ? $user->get_notes() : "" }}</textarea>
                </div>
            </div>
            @if(isset($boat))
                <input type='hidden' name='boat_id' value='{{ $boat->get_id() }}' />
            @endif
            <div class='form-group row'>
                <label for='boat_name' class='col-sm-2 col-form-label'>
                    {{ ucfirst(__('users/edit.boat_name_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='text' 
                           class='form-control {{ $errors->has('boat_name') ? 'is-invalid' : '' }}'
                           id='boat_name'
                           name='boat_name'
                           placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.boat_name_field_label_caption')) : "" }}'
                           value='{{ (isset($boat) && (!is_null($boat->get_name()))) ? $boat->get_name() : "" }}'
                           {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                    <span class='invalid-feedback' role='alert'>
                        @error('boat_name')
                            <strong>{{ $message }}</strong>
                        @enderror
                    </span>
                </div>
            </div>
            <div class='form-group row'>
                <label for='boat_length' class='col-sm-2 col-form-label'>
                    {{ ucfirst(__('users/edit.boat_length_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='number' 
                           class='form-control {{ $errors->has('boat_length') ? 'is-invalid' : '' }}'
                           id='boat_length'
                           name='boat_length'
                           min='' max='' step=''    <?php // TODO ?>
                           placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.boat_length_field_label_caption')) : "" }}'
                           value='{{ (isset($boat) && (!is_null($boat->get_length()))) ? $boat->get_length() : "" }}'
                           {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                    <span class='invalid-feedback' role='alert'>
                        @error('boat_length')
                            <strong>{{ $message }}</strong>
                        @enderror
                    </span>
                </div>
            </div>
            <div class='form-group row'>
                <label for='boat_width' class='col-sm-2 col-form-label'>
                    {{ ucfirst(__('users/edit.boat_width_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='number' 
                           class='form-control {{ $errors->has('boat_width') ? 'is-invalid' : '' }}' 
                           id='boat_width' 
                           name='boat_width'
                           min='' max='' step=''    <?php // TODO ?>
                           placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.boat_width_field_label_caption')) : "" }}'
                           value='{{ (isset($boat) && (!is_null($boat->get_width()))) ? $boat->get_width() : "" }}'
                           {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                    <span class='invalid-feedback' role='alert'>
                        @error('boat_width')
                            <strong>{{ $message }}</strong>
                        @enderror
                    </span>
                </div>
            </div>
            <div class='form-group row'>
                <label for='boat_depth' class='col-sm-2 col-form-label'>
                    {{ ucfirst(__('users/edit.boat_depth_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='number' 
                           class='form-control {{ $errors->has('boat_depth') ? 'is-invalid' : '' }}'
                           id='boat_depth'
                           name='boat_depth'
                           min='' max='' step=''    <?php // TODO ?>
                           placeholder='{{ Auth::user()->is_full_admin() ? ucfirst(__('users/edit.boat_depth_field_label_caption')) : "" }}'
                           value='{{ (isset($boat) && (!is_null($boat->get_depth()))) ? $boat->get_depth() : "" }}'
                           {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                    <span class='invalid-feedback' role='alert'>
                        @error('boat_depth')
                            <strong>{{ $message }}</strong>
                        @enderror
                    </span>
                </div>
            </div>
            <fieldset class='form-group'>
                <div class='row'>
                    <legend class='col-form-label col-sm-2 pt-0'>
                        {{ ucfirst(__('users/edit.user_access_level_label_caption')) }}
                    </legend>
                    <div class='col-sm-10'>
                        <select class='form-control custom-select {{ $errors->has('access_level') ? 'is-invalid' : '' }}'
                                id='access_level'
                                name='access_level'
                                {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                            <option {{ (isset($user) && (!$user->is_viewer_admin())) ? 'selected' : '' }}
                                    value='{{ UserController::STANDARD_USER_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM }}'>
                                {{ ucfirst(__('users/edit.user_access_level_standard_caption')) }}
                            </option>
                            <option {{ (isset($user) && ($user->is_viewer_admin()) && (!$user->is_full_admin())) ? 'selected' : '' }}
                                    value='{{ UserController::VIEWER_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM }}'>
                                {{ ucfirst(__('users/edit.user_access_level_viewer_admin_caption')) }}
                            </option>
                            <option {{ (isset($user) && ($user->is_full_admin()) && (!$user->is_supervisor())) ? 'selected' : '' }}
                                    value='{{ UserController::FULL_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM }}'>
                                {{ ucfirst(__('users/edit.user_access_level_full_admin_caption')) }}
                            </option>
                            @if(Auth::user()->is_supervisor())
                                <option {{ (isset($user) && ($user->is_supervisor())) ? 'selected' : '' }}
                                    value='{{ UserController::SUPERVISOR_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM }}'>
                                    {{ ucfirst(__('users/edit.user_access_level_supervisor_caption')) }}
                                </option>
                            @endif
                        </select>
                        <span class='invalid-feedback' role='alert'>
                        <?php // TODO: se per il campo access_level viene submittato un valore non valido (manipolando il sorgente), la validazione lo blocca ma poi il messaggio di errore non viene visualizzato: vedo solo il valore in rosso. Perchè? ?>
                            @error('access_level')
                                <strong>{{ $message }}</strong>
                            @enderror
                        </span>
                    </div>
                </div>
            </fieldset>
            @if(!isset($user) || $user->is_cloud_user())
                <fieldset class='form-group'>
                    <div class='row'>
                        <legend class='col-form-label col-sm-2 pt-0'>
                            {{ ucfirst(__('users/edit.user_cloud_access_label_caption')) }}
                        </legend>
                        <div class='col-sm-10'>
                            <div class='form-check'>
                                <input type='radio' class='form-check-input' {{ (!isset($user) || !($user->is_disabled())) ? 'checked' : '' }}
                                       name='cloud_access' id='cloud_access_enabled_radio' value='enabled'
                                       {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                                <label class='form-check-label' for='cloud_access_enabled_radio'>
                                    {{ ucfirst(__('users/edit.user_cloud_access_enabled_chk_label_caption')) }}
                                </label>
                            </div>
                            <div class='form-check'>
                                <input type='radio' 
                                       class='form-check-input {{ $errors->has('cloud_access') ? 'is-invalid' : '' }}'
                                       {{ (isset($user) && $user->is_disabled()) ? 'checked' : '' }}
                                       name='cloud_access'
                                       id='cloud_access_disabled_radio'
                                       value='disabled'
                                       {{ $disable_all_input_elements_if_logged_user_is_not_a_full_admin }}>
                                <label class='form-check-label' for='cloud_access_disabled_radio'>
                                    {{ ucfirst(__('users/edit.user_cloud_access_disabled_chk_label_caption')) }}
                                </label>
                            </div>
                            <span class='invalid-feedback' role='alert'>
                                @error('cloud_access')
                                    <strong>{{ $message }}</strong>
                                @enderror
                            </span>
                        </div>
                    </div>
                </fieldset>
            @endif
            <div class='form-group row'>
                <div class='col-sm-10'>
                    <button type='submit' class='btn btn-primary'>
                        {{ $submit_btt_caption }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
