<?php
    use Illuminate\Support\Carbon;
    use Illuminate\Support\Facades\Auth;
    use App\User;
    use App\Notifications\SmartCloudNotification;
    use App\Notifications\SerialLineOfflineAlarmNotification;
    use App\Notifications\SocketOfflineAlarmNotification;
    use App\Notifications\SocketMaxWaterTapTimeAlarmNotificationForAdmins;
    use App\Notifications\SocketMinPowerConsumptionAlarmNotification;
    use App\Notifications\SocketMaxPowerConsumptionAlarmNotification;
    use App\Notifications\SocketMaxWaterTapTimeAlarmNotificationForUsers;
    use App\Notifications\SocketMinCreditAlarmNotification;
    use App\Notifications\SocketTheftAlarmNotification;
    use App\Notifications\ReportCreationCompletedNotification;
    // TODO: questa roba ha senso metterla in un dropdown direttamente nella navbar (e spostare quindi la view in /fragments ed evitare di estendere un layout), invece che tenerla in una view a sè? Sembra che si possa fare (ho fatto una prova via devtools sul dropdown di logout) ma diventa piuttosto scomodo da leggere. Forse meglio tenere una view a parte.
    use App\Http\Controllers\ConsumptionReportsController;
?>
@extends('layouts.app_menu')

@section('custom_css')
    <link href='{{ URL::asset('css/notifications.css') }}' rel='stylesheet'>
@endsection

@section('custom_js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js' type='application/javascript'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.31/moment-timezone-with-data-10-year-range.min.js' type='application/javascript'></script>
    <script>
        let translations = {
        <?php // TODO: sto passando le traduzioni al js manualmente, ma esiste un package che lo fa in automatico: https://github.com/rmariuzzo/Laravel-JS-Localization. Però sembra che vada rifatto il publish ogni volta che si fa una modifica ai langfiles ?>
            "alarm_switched_off_at": @json(strtolower(__('users/notifications.alarm_switched_off_at'))),
            "berth": @json(ucfirst(__('users/notifications.berth'))),
            "notification_msg_report_has_been_generated": @json(ucfirst(__('notifications/in-app.report_generated_message'))),
            "report_generated_notification_heading_berth": @json(ucfirst(__('notifications/in-app.report_generated_notification_heading_berth'))),
            "report_generated_notification_heading_marina": @json(ucfirst(__('notifications/in-app.report_generated_notification_heading_marina'))),
            "report_generated_notification_heading_pontoon": @json(ucfirst(__('notifications/in-app.report_generated_notification_heading_pontoon'))),
            "report_generated_notification_heading_subscription": @json(ucfirst(__('notifications/in-app.report_generated_notification_heading_subscription'))),
            "serial_line": @json(ucfirst(__('users/notifications.serial_line'))),
        };
        function hasNotificationBeenRead(notification) {
            return notification.read_at === null;
        }
        function refresh_notifications() {
            let notifications;
            let request = new XMLHttpRequest();
            let notifications_div = document.getElementById("notifications_list");
            let refreshing_notifications_message_div = document.getElementById("refreshing_notifications_message");
            let no_entries_message_div = document.getElementById("no_entries_message");
            let unread_notifications_count = 0;
            let unread_notifications_count_element_on_navbar = document.getElementById("navbar_unread_notifications_count_badge");
            request.open('GET', "{{ route('get_notifications_for_logged_user_via_ajax', ['username' => Auth::user()->get_username()]) }}");
            request.onload = function() {
                if(request.status === 200) {
                    refreshing_notifications_message.classList.add('d-none');
                    let parsed_response = JSON.parse(request.responseText);
                    notifications = parsed_response.notifications;
                    <?php // svuoto il div delle notifiche dal contenuto precedente: ?>
                    notifications_div.innerHTML = '';
                    if(Object.keys(notifications).length > 0) {
                    <?php // se ci sono notifiche da mostrare: ?>
                        <?php // mostro il div delle notifiche: ?>
                        notifications_div.classList.remove('d-none');
                        <?php // nascondo il div contenente il messaggio "non ci sono notifiche": ?>
                        no_entries_message_div.classList.add('d-none');
                        <?php // non ho bisogno di aggiornare il contatore delle notifiche non lette sulla navbar, viene già aggiornato in autonomia dalla view della navbar stessa ?>
                        for(current_key in notifications) {
                            current_notification = notifications[current_key];
                            let current_notification_element = document.createElement('a');
                            current_notification_element.classList.add('list-group-item', 'list-group-item-action', 'flex-column', 'align-items-start');
                            let current_notification_timestamp_local = moment.utc(current_notification.created_at).tz("{{ config('dates_currency_and_locale.local_timezone') }}");
                            current_notification_element.href = '#';
                            if(current_notification.read_at === null) {
                                current_notification_element.classList.add('active');
                                // TODO: come gestisco le notifiche già lette o non lette? se il refresh è avvenuto le segno direttamente come già lette? ma in questo caso l'utente potrebbe aver avuto la pagina aperta su una scheda non in foreground, e non averle viste! Potrei segnare come lette le ultime solo in seguito all'evento focus
                            }
                            let current_notification_heading_text = '';
                            let current_notification_heading_element = document.createElement('h5');
                            current_notification_heading_element.innerText = '&nbsp;';
                            current_notification_heading_element.classList.add('mb-1');
                            switch(current_notification.data.object_type)  {
                                case "{{ SocketMaxPowerConsumptionAlarmNotification::NOTIFICATION_OBJECT_TYPE_FOR_DB }}":
                                case "{{ SocketMinPowerConsumptionAlarmNotification::NOTIFICATION_OBJECT_TYPE_FOR_DB }}":
                                case "{{ SocketMaxWaterTapTimeAlarmNotificationForUsers::NOTIFICATION_OBJECT_TYPE_FOR_DB }}":
                                case "{{ SocketMinCreditAlarmNotification::NOTIFICATION_OBJECT_TYPE_FOR_DB }}":
                                case "{{ SocketTheftAlarmNotification::NOTIFICATION_OBJECT_TYPE_FOR_DB }}":
                                case "{{ SocketMaxWaterTapTimeAlarmNotificationForAdmins::NOTIFICATION_OBJECT_TYPE_FOR_DB }}":
                                default:
                                    current_notification_text = current_notification.data.text;
                                    current_notification_heading_text = translations.berth + ' ' + current_notification.data.object_name;
                                    break;
                                case "{{ SerialLineOfflineAlarmNotification::NOTIFICATION_OBJECT_TYPE_FOR_DB }}":
                                    current_notification_text = current_notification.data.text;
                                    current_notification_heading_text = translations.serial_line + ' ' + current_notification.data.object_id;
                                    break;
                                case "{{ ReportCreationCompletedNotification::NOTIFICATION_OBJECT_TYPE_FOR_DB }}":
                                    current_notification_text = translations.notification_msg_report_has_been_generated;
                                    current_notification_element.href = current_notification.data.object_name;
                                    current_notification_element.target = '_blank';
                                    switch(current_notification.data.subject_type) {
                                    // TODO: devo anche poter vedere le date di riferimento del report all'interno della notifica (e come le mostro? devo aggiungere altri campi in fase di creazione della notifica?)
                                        case "{{ ConsumptionReportsController::SUBJECT_TYPE_MARINA }}":
                                            current_notification_heading_text = translations.report_generated_notification_heading_marina;
                                            break;
                                        case "{{ ConsumptionReportsController::SUBJECT_TYPE_PONTOON }}":
                                            current_notification_heading_text = translations.report_generated_notification_heading_pontoon;
                                            break;
                                        case "{{ ConsumptionReportsController::SUBJECT_TYPE_BERTH }}":
                                            current_notification_heading_text = translations.report_generated_notification_heading_berth;
                                            break;
                                        case "{{ ConsumptionReportsController::SUBJECT_TYPE_SUBSCRIPTION }}":
                                            current_notification_heading_text = translations.report_generated_notification_heading_subscription;
                                            break;
                                    }
                                    if((current_notification_heading_text.includes(":name")) && (current_notification.data.subject_name !== 'undefined')) {
                                    <?php // se current_notification_heading_text include il placeholder ":name" e se la notifica corrente ha il campo "subject_name" popolato, lo aggiungo, sostituendolo al placeholder: ?>
                                        current_notification_heading_text = current_notification_heading_text.replace(":name", current_notification.data.subject_name);
                                    }
                            }
                            current_notification_heading_element.innerText = current_notification_heading_text;
                            let current_notification_heading_and_timestamp_container_element = document.createElement('div');
                            current_notification_heading_and_timestamp_container_element.classList.add('d-flex', 'w-100', 'justify-content-between');
                            let current_notification_timestamp_element = document.createElement('small');
                            current_notification_timestamp_element.innerText = current_notification_timestamp_local.format("{{ config('dates_currency_and_locale.local_datetime_format_full_momentjs') }}");
                            current_notification_heading_and_timestamp_container_element.appendChild(current_notification_heading_element);
                            current_notification_heading_and_timestamp_container_element.appendChild(current_notification_timestamp_element);
                            current_notification_element.appendChild(current_notification_heading_and_timestamp_container_element);
                            let current_notification_text_element = document.createElement('p');
                            current_notification_text_element.classList.add('mb-1');
                            current_notification_text_element.innerText = current_notification_text;
                            current_notification_element.appendChild(current_notification_text_element);
                            let current_notification_closing_timestamp_element = document.createElement('small');
                            let current_notification_closing_timestamp_element_innertext = '';
                            if(current_notification.data.closed_at !== null) {
                                let current_notification_closing_timestamp_local = moment.utc(current_notification.data.closed_at).tz("{{ config('dates_currency_and_locale.local_timezone') }}").format("{{ config('dates_currency_and_locale.local_datetime_format_full_momentjs') }}");
                                current_notification_closing_timestamp_element_innertext = translations.alarm_switched_off_at + ' ' + current_notification_closing_timestamp_local
                            }
                            current_notification_closing_timestamp_element.innerText = current_notification_closing_timestamp_element_innertext;
                            current_notification_element.appendChild(current_notification_closing_timestamp_element);
                            notifications_div.appendChild(current_notification_element);
                        }
                    } else {
                    <?php // se non ci sono notifiche da mostrare: ?>
                        <?php // nascondo il div contenente le notifiche: ?>
                        notifications_div.classList.add('d-none');
                        <?php // mostro il div contenente il messaggio "non ci sono notifiche": ?>
                        no_entries_message_div.classList.remove('d-none');
                    }
                }
            };
            request.send();
        }

        window.onload = function() {
        // recupero delle notifiche al caricamento della pagina:
            refresh_notifications();
        };

        window.setInterval(function() {
        // refresh delle notifiche ad intervalli regolari:
            refresh_notifications();
        }, {{ config('marina.notifications_refresh_interval_seconds') * 1000 }});
    </script>
@endsection

@section('content')
    <?php // TODO: sistemare il css mobile ?>
    <div id='content' class='mt-5 pt-5 mx-md-5 px-md-5 pl-2' onload="refresh_notifications();">
        <div class='list-group container'>
            <h5 id='notifications_list_heading'>
                {{ ucfirst(__('users/notifications.notifications_list_heading_caption')) }}
            </h5>
            <?php // messaggio che informa l'utente che è in corso il refresh delle notifiche. Al caricamento della pagina viene mostrato, poi viene nascosto una volta ricevuta la risposta dal server. ?>
            <div id='refreshing_notifications_message' class='container-fluid mt-5 pt-5 text-center'>
                <h6 class='text-secondary text-uppercase font-weight-bold'>
                    {{ ucfirst(__('users/notifications.refreshing_notifications')) }}
                </h6>
            </div>
            <div class='container-fluid mt-5 pt-5 text-center'>
                <?php // messaggio che informa l'utente che non ci sono notifiche da mostrare. Al caricamento della pagina viene mostrato, poi viene nascosto se ce ne sono: ?>
                <h6 id='no_entries_message' class='text-secondary text-uppercase font-weight-bold d-none'>
                    {{ ucfirst(__('users/notifications.you_have_not_received_any_notifications_yet')) }}
                </h6>
            </div>
            <?php // div contenente le notifiche da mostrare: ?>
            <div id='notifications_list'>
            </div>
        </div>
    </div>
@endsection
