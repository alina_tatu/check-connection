<?php
    use App\Http\Controllers\UserController;
?>
@extends('layouts.app_menu')

@section('content')
    <div id='content' class='mt-5 pt-5'>
        @include('fragments.flash_message')
        <div class='container'>
            <form id='user_settings_form' method='post' action='{{ route('set_user_settings', Auth::id()) }}'>
            @csrf
                <div class='form-row mt-3'>
                    <div class='col'>
                        <label for='email'>
                            {{ ucfirst(__('users/settings.email_address_label_caption')) }}
                        </label>
                    </div>
                    <div class='col'>
                        <input class='form-control' type='email' id='email' name='email' value='{{ Auth::user()->get_email_address() }}' />
                        <span class='invalid-feedback' role='alert'>
                            @error('email')
                                <strong>{{ $message }}</strong>
                            @enderror
                        </span>
                    </div>
                </div>
                <div class='form-row mt-3'>
                    <div class='col'>
                        <label for='language_preference'>
                            {{ ucfirst(__('users/settings.language_preference_label_caption')) }}
                        </label>
                    </div>
                    <div class='col'>
                        <select id='language_preference' name='language_preference' class='custom-select'>
                            <?php /*    // TODO: mostrare questa voce? nel caso, manca la clausola use del controller
                            <option id='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                                    name='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                                    value='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'>
                                {{ strtolower(__('users/settings.please_select_an_option_caption')) }}
                            </option>
                            */ ?>
                            @foreach($available_software_languages as $language)
                                <option <?=($language === $user_language_preference) ? 'selected' : '' ?> id='{{ $language }}' name='{{ $language }}' value='{{ $language }}'>
                                    {{ $language }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class='form-row mt-3'>
                    <div class='col'>
                        <label for='power_alarm_thresholds_preference'>
                            {{ ucfirst(__('users/settings.power_alarm_thresholds_preference_label_caption')) }}
                        </label>
                    </div>
                    <div class='col'>
                        <select id='power_alarm_thresholds_preference' name='power_alarm_thresholds_preference' class='custom-select'>
                            <?php /*    // TODO: mostrare questa voce? nel caso, manca la clausola use del controller
                            <option id='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                                    name='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                                    value='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'>
                                {{ strtolower(__('users/settings.please_select_an_option_caption')) }}
                            </option>
                            */ ?>
                            @foreach($available_um_for_power_alarm_thresholds as $current_um_array_entry)
                                <option <?=(boolval($current_um_array_entry['value']) === boolval($user_power_alarm_thresholds_um_preference)) ? 'selected' : '' ?> id='{{ $current_um_array_entry['value'] }}' name='{{ $current_um_array_entry['value'] }}' value='{{ $current_um_array_entry['value'] }}'>
                                    {{ $current_um_array_entry['label'] }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class='mt-3 float-right'>
                    <?php $form_submit_confirmation_request_msg = ucfirst(__('users/settings.form_submit_confirmation_request_caption')) ?>
                    <button class='btn btn-primary' type='submit' name='submit_report_request'
                            onclick='return confirm("{{ $form_submit_confirmation_request_msg }}")'>
                        {{ ucfirst(__('users/settings.submit_data_button_caption')) }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
