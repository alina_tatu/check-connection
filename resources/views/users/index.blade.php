<?php
    use App\Http\Controllers\PagesController;
?>
@extends('layouts.app_menu')

@section('custom_js')
    <script type="text/javascript" src="{{ URL::asset('js/users_index.js') }}"></script>
    <script>
        window.onload = function() {
        // appena la pagina viene caricata:
            if(!window.matchMedia || (window.matchMedia("(min-width: 768px)").matches)) {
            // a meno che non si stia visualizzando il sito da mobile:
                // abilito i tooltip:
                $('[data-toggle="tooltip"]').tooltip();
            }
            // pulisco i campi dei filtri dal contenuto rimasto prima dell'eventuale refresh della pagina, poi li applico:
            reset_filters();
            apply_filters();
        };
    </script>
@endsection

@section('content')
    <div id='content' class='mt-5 pt-5'>
        @include('fragments.flash_message')
        <form id='filters_bar' class='form-horizontal px-2 py-1'>
        @csrf
            <?php // TODO: mancano i pulsanti per l'ordinamento, devo metterli? ?>
            <div class='card bg-light border-primary rounded mb-3'>
                <div class='card-body'> <?php // TODO: calare il padding (tra border e filtri) + aumentare il margin (tra border e fine pagina)?>
                    <div class='card-title'>
                        {{ ucfirst(__('common.filters_bar_title')) }}
                    </div>
                    <div class='p-3 m-3 form-row text-center'>
                        <?php // TODO: come faccio a cambiare il colore del margine azzurro degli input quando hanno il focus? ?>
                        <div class='form-group col-md-6'>
                            <input type='text' id='fulltext_filter_input' class='filter-input form-control' onkeyup='apply_filters();'
                                   placeholder='{{ ucfirst(__('users/index.fulltext_filter_caption')) }}'>
                        </div>
                        <div class='form-group col-md-5'>
                            <select id='combo_filter_input' class='filter-input custom-select' onchange='apply_filters();'>
                                <?php
                                    // TODO: occhio che se cambio i valori degli attributi id/value delle options, che attualmente sono definiti nel PagesController, i cambiamenti non si riflettono sul js che gestisce il filtro. In caso di necessità, ricordare che là devo modificarli a mano.
                                ?>
                                <option id='{{ PagesController::USERS_COMBO_FILTER_ALL_USERS }}'
                                        value='{{ PagesController::USERS_COMBO_FILTER_ALL_USERS }}'>
                                    {{ ucfirst(__('users/index.combo_filter_all_users_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::USERS_COMBO_FILTER_CLOUD_USERS_ONLY }}'
                                        value='{{ PagesController::USERS_COMBO_FILTER_CLOUD_USERS_ONLY }}'>
                                    {{ ucfirst(__('users/index.combo_filter_cloud_users_only_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::USERS_COMBO_FILTER_NOT_CLOUD_USERS_ONLY }}'
                                        value='{{ PagesController::USERS_COMBO_FILTER_NOT_CLOUD_USERS_ONLY }}'>
                                    {{ ucfirst(__('users/index.combo_filter_not_cloud_users_only_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::USERS_COMBO_FILTER_ENABLED_USERS_ONLY }}'
                                        value='{{ PagesController::USERS_COMBO_FILTER_ENABLED_USERS_ONLY }}'>
                                    {{ ucfirst(__('users/index.combo_filter_enabled_users_only_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::USERS_COMBO_FILTER_DISABLED_USERS_ONLY }}'
                                        value='{{ PagesController::USERS_COMBO_FILTER_DISABLED_USERS_ONLY }}'>
                                    {{ ucfirst(__('users/index.combo_filter_disabled_users_only_option_caption')) }}
                                </option>
                                @if(Auth::user()->is_supervisor())
                                    <option id='{{ PagesController::USERS_COMBO_FILTER_SUPERVISOR_USERS_ONLY }}'
                                            value='{{ PagesController::USERS_COMBO_FILTER_SUPERVISOR_USERS_ONLY }}'>
                                        {{ ucfirst(__('users/index.combo_filter_supervisor_users_only_option_caption')) }}
                                    </option>
                                @endif
                                <option id='{{ PagesController::USERS_COMBO_FILTER_FULL_ADMIN_USERS_ONLY }}'
                                        value='{{ PagesController::USERS_COMBO_FILTER_FULL_ADMIN_USERS_ONLY }}'>
                                    {{ ucfirst(__('users/index.combo_filter_full_admin_users_only_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::USERS_COMBO_FILTER_VIEWER_ADMIN_USERS_ONLY }}'
                                        value='{{ PagesController::USERS_COMBO_FILTER_VIEWER_ADMIN_USERS_ONLY }}'>
                                    {{ ucfirst(__('users/index.combo_filter_viewer_admin_users_only_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::USERS_COMBO_FILTER_STANDARD_USERS_ONLY }}'
                                        value='{{ PagesController::USERS_COMBO_FILTER_STANDARD_USERS_ONLY }}'>
                                    {{ ucfirst(__('users/index.combo_filter_standard_users_only_option_caption')) }}
                                </option>
                            </select>
                        </div>
                        @if(Auth::user()->is_full_admin())
                            <div class="form-group col-md-1">
                                <?php // TODO: su mobile posso fare in modo che il pulsante sia full width ?>
                                <a class="btn btn-primary"
                                   id="new_user_btt" 
                                   href="{{ route('create_user') }}" 
                                   role="button"
                                   title="{{ ucfirst(__("users/index.new_user_btt_tooltip_caption")) }}"
                                   data-toggle="tooltip"
                                   data-placement="top">
                                    <i class="fa fa-user-plus"></i>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </form>
        @if(($users) && ($users->count() > 0))
            <?php
                // TODO: usare le responsive data tables come facevo sulla prima bozza di smartcloud 2? (https://css-tricks.com/responsive-data-tables/) (come le integro con Bootstrap?):
            ?>
            <div class='container table-responsive'>
                <table class='table table-striped table-hover'>
                    <thead>
                        <tr>
                            <th class='text-center'>{{ ucfirst(__('users/index.th_first_name')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('users/index.th_last_name')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('users/index.th_email')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('users/index.th_username')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('users/index.th_access_level')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('users/index.th_boat')) }}</th>
                            <th class='text-center'></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <?php
                            $boat_name = '-';
                            $boat_length = $boat_width = $boat_depth = null;
                            $boat = $user->boat;
                            if(($boat) && ($boat->count() > 0)) {
                                $boat_name = $boat->get_name();
                                $boat_size_tooltip = strtolower(__('common.boat_measurements_tooltip_caption', ['length' => $boat->get_length(),
                                                                                                                'width' => $boat->get_width(),
                                                                                                                'depth' => $boat->get_depth()]));
                            }
                        ?>
                        <tr class='user_row'>
                            <td class='text-center user_last_name'>
                                {{ $user->get_last_name() }}
                            </td>
                            <td class='text-center user_first_name'>
                                {{ $user->get_first_name() }}
                            </td>
                            <td class='text-center user_email_address'>
                                {{ strlen(strval($user->get_email_address())) > 0 ? $user->get_email_address() : '-' }}
                            </td>
                            <td class='text-center user_username'>
                                {{ strlen(strval($user->get_username())) > 0 ?  $user->get_username() : '-' }}
                            </td>
                            <td class='text-center access_level'>
                                @if(!$user->is_viewer_admin())
                                    <i class='fas fa-user text-secondary user-access-level-icon standard-user-icon'
                                       data-toggle='tooltip' data-placement='top'
                                       title='{{ ucfirst(__("users/index.user_access_level_standard_user_icon_tooltip_caption")) }}'></i>
                                @else
                                    @if($user->is_supervisor())
                                        <i class='fas fa-mask text-danger user-access-level-icon supervisor-user-icon'
                                           data-toggle='tooltip' data-placement='top'
                                           title='{{ ucfirst(__("users/index.user_access_level_supervisor_icon_tooltip_caption")) }}'></i>
                                    @elseif($user->is_full_admin())
                                        <i class='fas fa-wrench text-primary user-access-level-icon full-admin-user-icon'
                                           data-toggle='tooltip' data-placement='top'
                                           title='{{ ucfirst(__("users/index.user_access_level_full_administrator_icon_tooltip_caption")) }}'></i>
                                    @else
                                        <i class='fas fa-glasses text-success user-access-level-icon viewer-admin-user-icon'
                                           data-toggle='tooltip' data-placement='top'
                                           <?php // TODO: tooltip lunghi non vengono visualizzati correttamente (su chrome li vedo sfarfallare)? ?>
                                           title='{{ ucfirst(__("users/index.user_access_level_viewer_administrator_icon_tooltip_caption")) }}'></i>
                                    @endif
                                @endif
                            </td>
                            <td @if(($boat) && ($boat->count() > 0))
                                    title='{{ $boat_size_tooltip }}'
                                @endif
                                class='text-center boat_name' data-toggle='tooltip' data-placement='top'>
                                {{ $boat_name }}
                            </td>
                            <td class='text-center'>
                                <form class='form-inline'>
                                @csrf
                                    <a class="btn btn-primary mr-1"
                                       href="{{ route('edit_user', ['user_id' => $user->get_id()]) }}" 
                                       role="button"
                                       title="{{ Auth::user()->is_full_admin() ? ucfirst(__("users/index.edit_user_btt_tooltip")) : ucfirst(__("users/index.view_user_btt_tooltip")) }}"
                                       data-toggle="tooltip"
                                       data-placement="top">
                                        @if(Auth::user()->is_full_admin())
                                            <i class="fas fa-edit"></i>
                                        @else
                                            <i class="fas fa-eye"></i>
                                        @endif
                                    </a>
                                    <?php // solo i full_admin e i supervisor possono abilitare/disabilitare un utente:
                                        $enable_disable_user_btt_formaction = '#';
                                        $enable_disable_user_btt_class = 'enable-disable-user-btn btn invisible';
                                        $enable_disable_user_btt_confirmation_request_msg = $enable_disable_user_btt_tooltip = '';
                                        // TODO: su mobile, i pulsanti vengono visualizzati uno sotto l'altro. Sistemare.
                                    ?>
                                    @if(Auth::user()->is_full_admin())
                                        @if($user->is_cloud_user())
                                            <?php
                                                $enable_disable_user_btt_class = str_replace(' invisible', '', $enable_disable_user_btt_class);
                                                if($user->is_disabled()) {
                                                    $enable_disable_user_btt_class .= ' enable-disable-user-btn enable-user-btn btn btn-success';
                                                    $enable_disable_user_btt_formaction = route('enable_user', ['user_id' => $user->get_id()]);
                                                    $enable_disable_user_btt_confirmation_request_msg = ucfirst(__('users/index.enable_user_confirmation_request', ['user_full_name' => $user->get_full_name()]));
                                                    $enable_disable_user_btt_tooltip = ucfirst(__('users/index.enable_user_btt_tooltip'));
                                                } else {
                                                    $enable_disable_user_btt_class .= ' enable-disable-user-btn disable-user-btn btn btn-danger';
                                                    $enable_disable_user_btt_formaction = route('disable_user', ['user_id' => $user->get_id()]);
                                                    $enable_disable_user_btt_confirmation_request_msg = ucfirst(__('users/index.disable_user_confirmation_request', ['user_full_name' => $user->get_full_name()]));
                                                    $enable_disable_user_btt_tooltip = ucfirst(__('users/index.disable_user_btt_tooltip'));
                                                }
                                            ?>
                                        @endif
                                        <button type='submit' class='{{ $enable_disable_user_btt_class }}'
                                                formaction='{{ $enable_disable_user_btt_formaction }}'
                                                formmethod='post'
                                                onclick='return(confirm("{{ $enable_disable_user_btt_confirmation_request_msg }}"));'
                                                data-toggle='tooltip' data-placement='top' title='{{ $enable_disable_user_btt_tooltip }}'>
                                            @if($user->is_disabled())
                                                <i class='fas fa-check'></i>
                                            @else
                                                <i class='fas fa-ban'></i>
                                            @endif
                                        </button>
                                    @endif
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    <div id='no_entries_message' class='container-fluid mt-5 pt-5 text-center d-none'>
        <h6 class='text-secondary text-uppercase font-weight-bold'>
            {{ ucfirst(__('users/index.no_users_on_database_or_no_user_matches_search_criteria')) }}
        </h6>
    </div>
@endsection
