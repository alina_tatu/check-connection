<?php
    use Illuminate\Support\Carbon;
    use App\Berth;
    use App\SocketSnapshot;
    use App\User;
    use App\Http\Controllers\ConsumptionReportsController;
?>
@extends('layouts.app_menu')
<?php /* TODO: convertire anche i dati di consumo energia di questa view in kW o Ah a seconda della preferenza dell'utente? O devo farlo solo per le soglie allarmi sulla view set_alarm_thresholds (lì l'ho già fatto)? Controllare se su SmartCloud 1.x lo faccio */ ?>
<?php // TODO: qui devo mostrare i contatori di consumo effettuato dalla subscription anzichè i contatori totali della presa? Se sì, devo dare la possibilità di azzerarli manualmente, devo azzerarli automaticamente a fine mese oppure li lascio così come sono? ?>


@section('custom_css')
    <link href="{{ URL::asset('css/my_subscriptions.css') }}" rel='stylesheet'>
@endsection

@section('custom_js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js' type='application/javascript'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.31/moment-timezone-with-data-10-year-range.min.js' type='application/javascript'></script>
    <script>
        function change_chart_form_action_according_to_selected_period(form_id) {
            var form = document.getElementById(form_id);
            var form_id_number = form_id.substring(form_id.lastIndexOf('[') + 1, form_id.lastIndexOf(']'));
            var chart_period_select_id = form_id.substr(0, form_id.indexOf('[')) + '_period_select' + '[' + form_id_number + ']';
            var chart_period_select = document.getElementById(chart_period_select_id);
            if(event) {
                let form = document.getElementById(form_id);
                let chart_period_select = form.getElementsByTagName("select")[0];
                let selected_period_before = "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}";
                let period_to_set = chart_period_select.options[chart_period_select.selectedIndex].value;
                let current_form_action = form.action;
                let splitted_current_form_action;
                if(form.action.includes("{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}")) {
                    selected_period_before = "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}";
                } else if(form.action.includes("{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}")) {
                    selected_period_before = "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}";
                }
                let selected_sampling_before = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR }}";
                if(form.action.includes("{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY }}")) {
                    selected_sampling_before = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY }}";
                }
                let begin_timestamp_to_set;
                let end_timestamp_to_set = moment().format("X");
                switch(period_to_set) {
                    case "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}":
                    default:
                        begin_timestamp_to_set = moment().subtract(24, 'hours').format("X");
                        sampling_to_set = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR }}";
                        break;
                    case "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}":
                        begin_timestamp_to_set = moment().subtract(7, 'days').format("X");
                        sampling_to_set = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY }}";
                        break;
                    case "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}":
                        begin_timestamp_to_set = moment().subtract(moment().daysInMonth(), 'days').format("X");
                        sampling_to_set = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY }}";
                }
                let slashes_positions_in_current_form_action_from_end = [];
                let last_slash_position_in_current_form_action_temp;
                let i = 0;
                let current_form_action_temp = current_form_action;
                while(i < 5) {
                    last_slash_position_in_current_form_action_temp = current_form_action_temp.lastIndexOf("/");
                    if(last_slash_position_in_current_form_action_temp !== (last_slash_position_in_current_form_action_temp.length - 1)) {      // evito di considerare l'eventuale slash in fondo all'url
                        slashes_positions_in_current_form_action_from_end[i] = last_slash_position_in_current_form_action_temp;
                        i++;
                    }
                    current_form_action_temp = current_form_action_temp.substring(0, (last_slash_position_in_current_form_action_temp - 1))
                }
                let begin_timestamp_to_replace = current_form_action.substring(slashes_positions_in_current_form_action_from_end[4] + 1, slashes_positions_in_current_form_action_from_end[3]);
                let end_timestamp_to_replace = current_form_action.substring(slashes_positions_in_current_form_action_from_end[3] + 1, slashes_positions_in_current_form_action_from_end[2]);
                form.action = current_form_action.replace(begin_timestamp_to_replace, begin_timestamp_to_set)
                                                 .replace(end_timestamp_to_replace, end_timestamp_to_set)
                                                 .replace(selected_sampling_before, sampling_to_set);
            }
        }

        var app_settings = {
            "server_timezone": @json(config('app.timezone')),
            "local_timezone": @json(config('dates_currency_and_locale.local_timezone')),
            "datetime_format_full_momentjs": @json(config('dates_currency_and_locale.local_datetime_format_full_momentjs')),
            "currency_character": @json(config('dates_currency_and_locale.currency_character')),
        };
        var socket_link_status_values = {
            "connected": @json(SocketSnapshot::LINK_STATUS_CONNECTED),
            "not_connected": @json(SocketSnapshot::LINK_STATUS_CONNECTED),
            "theft_alarm": @json(SocketSnapshot::LINK_STATUS_THEFT_ALARM),
        };
        var socket_on_off_status_values = {
            "disabled": @json(SocketSnapshot::SOCKET_STATUS_DISABLED),
            "forced": @json(SocketSnapshot::SOCKET_STATUS_FORCED),
            "off": @json(SocketSnapshot::SOCKET_STATUS_OFF),
            "off_transaction_not_completed": @json(SocketSnapshot::SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED),
            "on": @json(SocketSnapshot::SOCKET_STATUS_ON),
            "on_transaction_not_completed": @json(SocketSnapshot::SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED),
            "not_connected": @json(SocketSnapshot::SOCKET_STATUS_NOT_CONNECTED),
        };
        var socket_snapshot_active_alarms_flags = {
            "not_connected_alarm_flag": @json(SocketSnapshot::ALARMS_FIELD_FLAG_NOT_CONNECTED),
            "theft_alarm_flag": @json(SocketSnapshot::ALARMS_FIELD_FLAG_THEFT),
            "min_power_consumption_alarm_flag": @json(SocketSnapshot::ALARMS_FIELD_FLAG_MIN_POWER_CONSUMPTION),
            "max_power_consumption_alarm_flag": @json(SocketSnapshot::ALARMS_FIELD_FLAG_MAX_POWER_CONSUMPTION),
            "max_water_time_alarm_flag": @json(SocketSnapshot::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_USERS),
            "min_credit_alarm_flag": @json(SocketSnapshot::ALARMS_FIELD_FLAG_MIN_CREDIT),
        };
        var socket_type_value_for_not_defined = @json(Berth::SOCKET_TYPE_NOT_DEFINED);
        var translations = {
        <?php // TODO: sto passando le traduzioni al js manualmente, ma esiste un package che lo fa in automatico: https://github.com/rmariuzzo/Laravel-JS-Localization. Però sembra che vada rifatto il publish ogni volta che si fa una modifica ai langfiles ?>
            "onoff_btt_alert_switchoff_confirmation_request": @json(__('users/my_subscriptions.onoff_btt_alert_switchoff_confirmation_request')),
            "onoff_btt_alert_switchon_confirmation_request": @json(__('users/my_subscriptions.onoff_btt_alert_switchon_confirmation_request')),
            "onoff_btt_caption_socket_is_not_connected": @json(__('users/my_subscriptions.onoff_btt_caption_socket_is_not_connected')),
            "onoff_btt_caption_socket_is_off": @json(__('users/my_subscriptions.onoff_btt_caption_socket_is_off')),
            "onoff_btt_caption_socket_is_on": @json(__('users/my_subscriptions.onoff_btt_caption_socket_is_on')),
            "onoff_btt_tooltip_click_to_switch_off_socket": @json(__('users/my_subscriptions.onoff_btt_tooltip_click_to_switch_off_socket')),
            "onoff_btt_tooltip_socket_is_not_responding_unable_to_control_it": @json(__('users/my_subscriptions.onoff_btt_tooltip_socket_is_not_responding_unable_to_control_it')),
            "onoff_btt_tooltip_socket_switchon_is_disabled_for_security_reasons": @json(__('users/my_subscriptions.onoff_btt_tooltip_socket_switchon_is_disabled_for_security_reasons')),
            "postpaid_user_span_caption": @json(__('users/my_subscriptions.postpaid_user_span_caption')),
            "socket_link_status_connected": @json(__('common.socket_link_status_connected')),
            "socket_link_status_not_connected": @json(__('common.socket_link_status_not_connected')),
            "socket_link_status_theft_alarm": @json(__('common.socket_link_status_theft_alarm')),
            "socket_status_not_connected": @json(__('common.socket_status_not_connected')),
            "socket_status_off": @json(__('common.socket_status_off')),
            "socket_status_on": @json(__('common.socket_status_on')),
        };
        <?php
            $now = Carbon::now(config('app.timezone'));
            $subscriptions = Auth::user()->get_subscriptions(User::GET_USER_SUBSCRIPTION_FLAG_CURRENT);
        ?>
        <?php // TODO: ATTENZIONE: quello che segue viene outputtato con i nomi degli attributi dei uguali a quelli dei campi del db. Per motivi di sicurezza sarebbe meglio cambiarli ?>
        var subscriptions = [
            @foreach($subscriptions as $current_subscription)
                {
                    "id": @json($current_subscription->get_id()),
                    "user_id": @json($current_subscription->user_id),
                    "transponder_id": @json($current_subscription->transponder_id),
                    "is_prepaid": @json($current_subscription->is_prepaid()),
                    "socket_id": @json($current_subscription->socket_id),
                    "begin_dt": @json($current_subscription->get_begin_timestamp()),  <?php // TODO: questo campo mi serve? ?>
                    "berths": [
                        <?php $subscription_berths = $current_subscription->get_berths_on_which_subscription_consumed(); ?>
                        @foreach($subscription_berths as $current_berth) {
                        <?php // TODO: in questo modo sto ripetendo l'intera anagrafica di ogni posto barca ogni volta. Mi basterebbe outputtare le anagrafiche una volta e qui outputtarne gli id. ?>
                            "id": @json($current_berth->get_id()),
                            "pontoon_name": @json($current_berth->get_pontoon_name()),
                            "berth_name": @json($current_berth->get_berth_name()),
                            "description": @json($current_berth->get_description()),
                            "socket_type": @json($current_berth->get_socket_type()),
                            "last_snapshot_for_subscription": @json($current_subscription->get_last_socket_snapshot_on_berth_before_timestamp($now, $current_berth->get_id()))
                        },
                        @endforeach
                    ]
                },
            @endforeach
        ];
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/my_subscriptions.js') }}"></script>
@endsection

@section('content')
    <div id='content' class='mt-5 pt-5'>
        @include('fragments.flash_message')
        <?php
            // inizializzo le variabili che mi serviranno successivamente per popolare la pagina e decidere cosa mostrare/nascondere:
            $no_subscriptions_in_progress_message_visibility_class = '';
        ?>
        <?php // TODO: devo mettere il button di reset presa anche qui, se l'utente loggato è un admin? il metodo reset_socket() del BerthController gestisce già la redirezione anche su questa pagina, ma al momento è inutilizzata, dato che non posso resettare da qui ?>
        @if(($subscriptions) && ($subscriptions->count() > 0))
        <?php // se subscriptions contiene almeno un record: ?>
            <?php $no_subscriptions_in_progress_message_visibility_class = 'd-none'; ?>
            @foreach($subscriptions as $subscription)
                <?php
                    // inizializzo ai valori di default le variabili che mi serviranno successivamente per popolare la tabella contenente lo stato del contratto e decidere cosa mostrare/nascondere:
                    $no_status_information_available_for_this_subscription_message_visibility_class = '';
                    $subscription_is_bound_to_socket = false;
                    $berths_used_by_the_subscription = [];
                    $berth_last_snapshot = $last_subscription_on_berth = null;
                    $berth_socket_type = Berth::SOCKET_TYPE_NOT_DEFINED;
                    $socket_type_row_class = 'd-none';
                    $snapshot_counters__visibility_class = '';
                    $berth_name_row_visibility_class = '';
                    $no_recent_data_about_berth_css_class = '';
                    $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute = $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute = $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute = '';
                    $snapshot_timestamp = '';
                    $link_status_class = $link_status_to_display = $link_status_row_class = '';
                    $power_consumption_row_class = $water_consumption_row_class = '';
                    $residual_credit_row_class = $residual_credit_chart_controls_class = $residual_credit_span_class = $residual_credit_span_content = $residual_credit_currency_character = '';
                    $disable_onoff_button_if_socket_is_off_or_not_reachable = '';
                    $on_off_btt_class = $on_off_btt_caption = $on_off_btt_tooltip = $on_off_btt_confirmation_request_caption = '';
                    $on_off_btt_href = route('turn_off_socket', ['socket_id' => -1]);
                    $alarm_thresholds = $subscription->get_thresholds_record();
                    $subscription_last_snapshot = $subscription->get_last_socket_snapshot();
                    if(!is_null($subscription->socket_id)) {
                    // se la subscription è su posto barca fisso (quindi l'utente non è in possesso del transponder. Questa è la modalità di noleggio dei posti barca in uso a Dubai, e da dicembre 2020 anche a PortoMontenegro):
                        $subscription_is_bound_to_socket = true;
                        $berth_name_row_visibility_class .= 'd-none ';                   // la riga della tabella che indica il posto barca attuale sarà nascosta
                        $berths_used_by_the_subscription[] = $subscription->berth();     // TODO: se l'anagrafica del posto barca non è ancora sul db, non funziona un cacchio. Come risolvere? EDIT: forse ora ho risolto? verificare.
                    } else {
                    // se la subscription è su un transponder (quindi l'utente nè è in possesso):
                        if(($subscription_last_snapshot) && ($subscription_last_snapshot->count() > 0)) {
                            $berths_used_by_the_subscription = $subscription->get_berths_on_which_subscription_consumed();
                        } else {
                            $berth_name_row_visibility_class .= 'd-none ';
                        }
                    }
                    if(($subscription_last_snapshot) && ($subscription_last_snapshot->count() > 0)) {
                    //  TODO: forse questo if lo ripeto più volte in giro, vedere se posso ottimizzare
                        $snapshot_timestamp = Carbon::parse($subscription_last_snapshot->get_timestamp());
                        $no_status_information_available_for_this_subscription_message_visibility_class = 'd-none';
                        if($subscription_last_snapshot->berth->get_socket_type() !== Berth::SOCKET_TYPE_NOT_DEFINED) {
                            $berth_socket_type = $subscription_last_snapshot->berth->get_socket_type();
                            $socket_type_row_class = '';
                        }
                        switch($subscription_last_snapshot->get_link_status()) {
                            case SocketSnapshot::LINK_STATUS_CONNECTED:
                                $link_status_class .= 'text-success ';
                                $link_status_to_display = __('common.socket_link_status_connected');
                                break;
                            case SocketSnapshot::LINK_STATUS_THEFT_ALARM:
                                $link_status_class .= 'text-danger ';
                                $link_status_to_display = __('common.socket_link_status_theft_alarm');
                                $link_status_row_class .= 'alarm_over_threshold_active ';     // TODO: riportare il testo del link status da rosso a nero, per evitare di avere la sovrapposizione testo rosso/sfondo rosso?
                                break;
                            case SocketSnapshot::LINK_STATUS_NOT_CONNECTED:
                            default:
                                $link_status_class .= 'text-danger ';
                                $link_status_to_display = __('common.socket_link_status_not_connected');
                                $disable_onoff_button_if_socket_is_off_or_not_reachable = 'disabled';
                                $link_status_row_class .= 'alarm_over_threshold_active ';     // TODO: riportare il testo del link status da rosso a nero, per evitare di avere la sovrapposizione testo rosso/sfondo rosso?
                                break;
                        }
                        switch($subscription_last_snapshot->get_on_off_status()) {
                            case SocketSnapshot::SOCKET_STATUS_ON:
                                $on_off_btt_class .= 'btn-success ';
                                $on_off_btt_caption = strtoupper(__('users/my_subscriptions.onoff_btt_caption_socket_is_on'));
                                $on_off_btt_tooltip = ucfirst(__('users/my_subscriptions.onoff_btt_tooltip_click_to_switch_off_socket'));
                                $on_off_btt_confirmation_request_caption = "return confirm('".ucfirst(__('users/my_subscriptions.onoff_btt_alert_switchoff_confirmation_request', ['berth' => $subscription_last_snapshot->berth->get_description()]))."');";
                                break;
                            case SocketSnapshot::SOCKET_STATUS_OFF:
                                $on_off_btt_class .= 'btn-danger ';
                                $on_off_btt_caption = strtoupper(__('users/my_subscriptions.onoff_btt_caption_socket_is_off'));
                                $disable_onoff_button_if_socket_is_off_or_not_reachable = 'disabled';                 // TODO: finchè la feature di accensione da remoto non sarà pronta lato SmartView, disabilito il button se la presa è spenta
                                $on_off_btt_href = route('turn_on_socket', ['socket_id' => $subscription_last_snapshot->socket_id]);        // TODO: al momento lo setto anche se la feature di accensione da remoto non è ancora pronta lato SmartView (ma tanto il button per il momento è disabilitato). Mi serve comunque perchè ora cambio i dati dello snapshot visualizzato via js, e con esso anche il pulsante on_off ed il suo href (dovrò anche fare in modo che sia possibile richiedere l'accensione solo se la funzionalità è abilitata da un apposito settaggio in .env)
                                $on_off_btt_tooltip = ucfirst(__('users/my_subscriptions.onoff_btt_tooltip_socket_switchon_is_disabled_for_security_reasons'));      // TODO: quando la funzionalità di accensione sarà abilitata lato supervisore, questo dovrà essere modificato (sempre che la funzionalità sia abilitata anche da .env)
                                $on_off_btt_confirmation_request_caption = "return confirm('".ucfirst(__('users/my_subscriptions.onoff_btt_alert_switchon_confirmation_request', ['berth' => $subscription_last_snapshot->berth->get_description()]))."');";      // TODO: quando la funzionalità di accensione sarà abilitata lato supervisore, verificare se questa è a posto (l'ho già definita nei messages)
                                $disable_onoff_button_if_socket_is_off = 'disabled';
                                $on_off_btt_href = route('turn_on_socket', ['socket_id' => $subscription_last_snapshot->socket_id]);        // TODO: al momento lo setto anche se la feature di accensione da remoto non è ancora pronta lato SmartView (ma tanto il button per il momento è disabilitato). Mi serve comunque perchè ora cambio i dati dello snapshot visualizzato via js, e con esso anche il pulsante on_off ed il suo href
                                break;
                            case SocketSnapshot::SOCKET_STATUS_NOT_CONNECTED:
                            default:
                                // TODO: c'è il default, quindi passo di qui anche nei casi diversi da quelli precisati nello switch, ma in realtà dovrei mostrare "NOT CONNECTED" solo se il valore è "1ERROR", e mostrare caption più precise + permettere lo spegnimento della presa in molti degli altri casi. Al momento però faccio così anche nel js quando ripopolo in seguito alla selezione del posto barca. Sistemare.
                                $on_off_btt_class .= 'btn-warning ';
                                $on_off_btt_caption = strtoupper(__('users/my_subscriptions.onoff_btt_caption_socket_is_not_connected'));
                                $on_off_btt_href = route('turn_on_socket', ['socket_id' => $subscription_last_snapshot->socket_id]);        // lo setto anche se la presa è in errore comunicazione (ma tanto il button in questo caso è disabilitato). Mi serve comunque perchè ora cambio i dati dello snapshot visualizzato via js, e con esso anche il pulsante on_off ed il suo href
                                $on_off_btt_tooltip = ucfirst(__('users/my_subscriptions.onoff_btt_tooltip_socket_is_not_responding_unable_to_control_it'));
                                $on_off_btt_confirmation_request_caption = '';      // TODO: devo valorizzarlo comunque, per fare sì che possa essere sostituito dal js all'occorrenza? o posso lasciarlo vuoto così? Tra l'altro, teoricamente lo inizializzo già a stringa vuota all'inizio del ciclo sulle subscriptions, non credo mi serva farlo qui
                                $disable_onoff_button_if_socket_is_off_or_not_reachable = 'disabled';
                                $on_off_btt_href = route('turn_on_socket', ['socket_id' => $subscription_last_snapshot->socket_id]);        // TODO: lo setto anche se non è possibile accendere una presa che non comunica. In teoria serve comunque perchè ora cambio i dati dello snapshot visualizzato via js, e con esso anche il pulsante on_off ed il suo href (verificare se posso toglierlo per evitare che venga lanciato comunque a mano dopo un ispeziona elemento)
                                $residual_credit_row_class .= 'd-none ';
                                $power_consumption_row_class .= 'd-none ';
                                $water_consumption_row_class .= 'd-none ';
                        }
                        if($subscription->is_prepaid() && ($subscription_last_snapshot->get_residual_credit() > 0)) {
                            $residual_credit_span_content = number_format($subscription_last_snapshot->get_residual_credit(), 2, '.', '');      // TODO: il numero di decimali da usare per la valuta va settato nelle config del locale, al momento l'ho hardcodato qui
                            $residual_credit_currency_character = html_entity_decode('&nbsp;'.config('dates_currency_and_locale.currency_character'));
                        } else {
                            if($subscription_last_snapshot->get_on_off_status() !== SocketSnapshot::SOCKET_STATUS_OFF) {
                                $residual_credit_span_class = 'text-primary d-inline text-center text-nowrap';
                            }
                            $residual_credit_span_content = strtoupper(__('users/my_subscriptions.postpaid_user_span_caption'));
                            $residual_credit_chart_controls_class = 'd-none';
                        }
                        if($subscription_last_snapshot->is_not_connected_alarm_set()) {
                            $link_status_row_class .= 'alarm_over_threshold_active ';     // TODO: devo riportare il testo del link status da rosso a nero, per evitare di avere la sovrapposizione testo rosso/sfondo rosso?
                        }
                        if($subscription_last_snapshot->is_theft_alarm_set()) {
                            $link_status_row_class .= 'alarm_over_threshold_active ';     // TODO: devo riportare il testo del link status da rosso a nero, per evitare di avere la sovrapposizione testo rosso/sfondo rosso?
                        }
                        if($subscription_last_snapshot->is_max_power_consumption_alarm_set()) {
                            $power_consumption_row_class .= 'alarm_over_threshold_active ';
                        }
                        if($subscription_last_snapshot->is_min_power_consumption_alarm_set()) {
                            $power_consumption_row_class .= 'alarm_under_threshold_active ';
                        }
                        if($subscription_last_snapshot->is_min_socket_credit_alarm_set()) {
                            $residual_credit_row_class .= 'alarm_over_threshold_active ';
                        }
                        if($subscription_last_snapshot->is_max_water_tap_time_alarm_for_users_set()) {
                            $water_consumption_row_class .= 'alarm_over_threshold_active ';
                        }
                        // verifico se il $subscription coincide con l'ultimo contratto che ha utilizzato il posto barca $subscription_last_snapshot->berth (ovvero se l'ultima volta che la presa è stata usata non coincide con l'ultima volta in cui l'ha usata l'utente loggato). Di norma non succede mai, ma è capitato a causa di un problema sui dati dei contratti:
                        $berth_last_snapshot = $subscription_last_snapshot->berth->last_snapshot;
                        if(($berth_last_snapshot) && ($berth_last_snapshot->count() > 0)) {
							if(boolval($berth_last_snapshot->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
					        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
								$last_subscription_on_berth = $berth_last_snapshot->subscription;
					        } else {
					        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
					        	$last_subscription_on_berth = $berth_last_snapshot->retrieve_connected_subscription();
					        }
                            if(($last_subscription_on_berth) && ($last_subscription_on_berth->count() > 0) && ($last_subscription_on_berth->get_id() !== $subscription->get_id())) {
                            // se il contratto legato all'utente loggato NON E' ultimo contratto che ha utilizzato $subscription_last_snapshot->berth (quindi se l'ultimo snapshot della presa non coincide con l'ultimo in cui l'utente loggato - o quantomeno il contratto $subscription - la stava usando):
                                // mostro i dati dello snapshot con uno sfondo grigio:
                                $no_recent_data_about_berth_css_class .= 'bg-light-gray ';
                                // e setto gli attributi per l'aggiunta di un tooltip on hover per le righe della tabella, che ne spieghi il motivo:
                                $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute = 'tooltip';
                                $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute = 'top';
                                $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute = ucfirst(__('users/my_subscriptions.no_recent_data_available_msg_caption'));
                            }
                        }
                    }
                ?>
                <div class='container mt-4 mb-5 mx-auto'>
                    <div class='row mb-1 pl-1'>
                        <span class='text-info p-2 d-inline text-center text-nowrap font-weight-bold'>
                            @if(!$subscription_is_bound_to_socket)
                                <img src="{{ URL::asset('resources/transponder_icon_teal.png') }}" class='transponder_btt_img' />
                                <span class='pr-0 ml-0 align-bottom'>{{ $subscription->transponder_id }}</span>
                            @else
                                <i class="fas fa-anchor text-info"></i>
                                @if(isset($subscription_last_snapshot) && ($subscription_last_snapshot) && ($subscription_last_snapshot->count() > 0))
                                    @if(($subscription_last_snapshot->berth) && ($subscription_last_snapshot->berth->count() > 0))
                                        {{ $subscription_last_snapshot->berth->get_description() }}
                                    @else
                                        {{ strtoupper(__('common.unknown_berth')) }}
                                    @endif
                                @else
                                    {{ strtoupper(__('common.unknown_berth')) }}
                                @endif
                            @endif
                        </span>&nbsp;
                        <form class='d-inline' method='post' action='{{ route("open_alarm_thresholds_page", ["subscription_id" => $subscription->id]) }}'>
                        @csrf
                            <button type='submit' class='btn p-2 btn-primary'>
                                {{ ucfirst(__('users/my_subscriptions.change_alarm_thresholds_btt_caption')) }}
                            </button>
                        </form>&nbsp;
                        <?php /* TODO: dovrei fare in modo che il pulsante finisca su una newline con un po' di margine dalla riga superiore, se il viewport non permette di tenerlo sulla stessa riga degli elementi precedenti. Attualmente, finisce sì su una newline ma non c'è margine verticale */ ?>
                        <form class='d-inline' method='post'>
                        @csrf
                            <button type='submit' class='btn p-2 {{ $on_off_btt_class }}' {{ $disable_onoff_button_if_socket_is_off_or_not_reachable }}
                                    data-toggle='tooltip' data-placement='top'  data-snapshot-attribute='on_off_btt'
                                    formaction='{{ $on_off_btt_href }}' title='{{ $on_off_btt_tooltip }}'
                                    onclick='{{ strlen($on_off_btt_confirmation_request_caption) > 0 ? $on_off_btt_confirmation_request_caption : "" }}'>
                                {{ $on_off_btt_caption }}
                            </button>
                        </form>&nbsp;
                    </div>              <?php /* chiusura del <div class='row mb-1 pl-1'> */ ?>
                    @if(isset($subscription_last_snapshot) && ($subscription_last_snapshot) && ($subscription_last_snapshot->count() > 0))
                    <?php // se esistono informazioni sullo stato del contratto (ovvero se il contratto ha già effettuato consumi nell'impianto): ?>
                        <div class='socket_status table-responsive row {{ $no_recent_data_about_berth_css_class }}' data-subscription_id='{{ $subscription->id }}'>
                        <?php // TODO: su pc la tabella è troppo larga (anche quelle della berthlist, della subscriptionlist e della userlist hanno lo stesso problema) ?>
                            <table class='table table-hover'>
                                <tbody>
                                    <tr class='{{ $berth_name_row_visibility_class }}' data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        data-snapshot-row-purpose='berth_name' title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                        <th class='col-5 col-md-4'>{{ ucfirst(__('users/my_subscriptions.berth_caption')) }}</th>
                                        <td class='col-7 col-md-8' colspan='3'>
                                            @if(count($berths_used_by_the_subscription) > 0)
                                                @if(count($berths_used_by_the_subscription) > 1)
                                                <?php // se il contratto è stato usato su più di un posto barca: ?>
                                                    <?php // permetto all'utente di selezionare il posto barca per il quale vuole visualizzare l'ultimo snapshot: ?>
                                                    <select class='custom-select' id='subscription_berth_select[{{ $subscription->id }}]'
                                                            data-element-purpose='subscription_berth_select' name='subscription_berth_select[{{ $subscription->id }}]'
                                                            oninput='change_shown_berth_info_according_to_selected_berth(this);'>
                                                        @foreach($berths_used_by_the_subscription as $current_berth)
                                                            <option <?=($current_berth->id === $subscription_last_snapshot->berth->get_id()) ? 'selected' : '' ?> value='{{ $current_berth->id }}'>
                                                                {{ $current_berth->get_description() }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                <?php // se il contratto è stato usato solamente su un posto barca (o se è una socket_subscription): ?>
                                                    <?php // mostro semplicemente il nome del posto barca, senza possibilità di selezione: ?>
                                                    {{ $subscription_last_snapshot->berth->get_description() }}
                                                @endif
                                            @else
                                            <?php // se il contratto non è ancora stato usato su nessun posto barca: ?>
                                                <?php // TODO: teoricamente non mi devo preoccupare: in questo caso la riga corrente sarà nascosta a causa di $berth_name_row_visibility_class ?>
                                                <?php // TODO. però per sicurezza mostro la dicitura di posto barca sconosciuto: ?>
                                                <span class='text-danger'>
                                                    {{ strtoupper(__('common.unknown_berth')) }}
                                                </span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr class='{{ $socket_type_row_class }}' data-snapshot-row-purpose='socket_type'>
                                        <th class='col-5 col-md-4'>{{ ucfirst(__('users/my_subscriptions.socket_type_caption')) }}</th>
                                        <td class='col-7 col-md-8' colspan='3'>
                                            <span data-snapshot-attribute='socket_type'>{{ $berth_socket_type }}</span>
                                        </td>
                                    </tr>
                                    <tr class='{{ $link_status_row_class }}' data-snapshot-row-purpose='link_status'
                                        data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                        <th class='col-5 col-md-4'>{{ ucfirst(__('users/my_subscriptions.linkstatus_caption')) }}</th>
                                        <td class='col-7 col-md-8' colspan='3'>
                                            <span class='{{ $link_status_class }}' data-snapshot-attribute='link_status'>
                                                {{ strtoupper($link_status_to_display) }}
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class='{{ $residual_credit_row_class }}' data-snapshot-row-purpose='residual_credit_data_part'
                                        data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                    <?php // TODO: questo elemento <tr> ed il successivo dovrebbero essere viste come un'unica riga per quanto riguarda la funzionalità hoverable rows di Bootstrap (https://getbootstrap.com/docs/4.0/content/tables/#hoverable-rows) ?>
                                        <th class='col-5 col-md-4'>{{ ucfirst(__('users/my_subscriptions.residualcredit_caption')) }}</th>
                                        <td class='col-7 col-md-8' colspan='3'>
                                            <span data-snapshot-attribute='socket_residual_credit' class='{{ $residual_credit_span_class }}'>
                                                @if($subscription_last_snapshot->get_on_off_status() !== SocketSnapshot::SOCKET_STATUS_OFF)
                                                    {{ $residual_credit_span_content }}{{ $residual_credit_currency_character }}
                                                @else
                                                    -
                                                @endif
                                            </span>
                                        </td>
                                    </tr>
                                    <?php // TODO: al momento sto mostrando sempre il <tr> che segue, ma devo mostrarlo solamente nei casi descritti dal commento qui sotto (definire una variabile apposita che conterrà la classe della riga ed outputtarla qui, settandola a d-none per default e togliendo il d-none se devo mostrare i dati. Ricordare anche che questa classe andrà manipolata anche dal js che viene triggerato alla selezione del posto barca per il quale visualizzare i dati): ?>
                                    <?php /* se $subscription_last_snapshot->berth è valorizzato ed esiste sul db, se il contratto non è postpagato (e - in assenza di dati sul contratto - se la presa non è accesa con credito a zero), mostro i controlli utili a richiedere il grafico del credito residuo sulla presa (per i contratti postpagati vedrei sempre il credito a zero): */ ?>
                                    <tr class='{{ $residual_credit_row_class }} {{ $residual_credit_chart_controls_class }}'
                                        data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-snapshot-row-purpose='residual_credit_chart_controls_part'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                        <td class='d-none d-sm-table-cell without_upper_border'>
                                        </td>
                                        <td class='col-12 col-md-8 without_upper_border' colspan='3'>
                                            <form id='subscription_chart_credit_consumption_frm[{{ $subscription->id }}]'
                                                  data-snapshot-attribute='credit_consumption_form'
                                                  action="{{ route('get_subscription_consumption_chart_on_berth', ['subscription_id' => $subscription->id,
                                                                                                                   'berth_id' => $subscription_last_snapshot->berth->get_id(),
                                                                                                                   'begin_timestamp' => Carbon::now()->subHours(24)->timestamp,
                                                                                                                   'end_timestamp' => Carbon::now()->timestamp,
                                                                                                                   'subject' => ConsumptionReportsController::REPORT_VALUE_CREDIT,
                                                                                                                   'sampling_interval_argument' => ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR]) }}">
                                            @csrf
                                                <div class='input-group'>
                                                    <?php // TODO: su mobile, il select + pulsante sforano la larghezza della pagina ?>
                                                    <select class='custom-select' id='subscription_chart_credit_consumption_frm_period_select[{{ $subscription->id }}]'
                                                            onchange='change_chart_form_action_according_to_selected_period("subscription_chart_credit_consumption_frm[{{ $subscription->id }}]");'>
                                                        <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}' selected>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_period_select_last_24hours_caption')) }}
                                                        </option>
                                                        <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}'>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_period_select_last_week_caption')) }}
                                                        </option>
                                                        <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}'>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_period_select_last_month_caption')) }}
                                                        </option>
                                                    </select>
                                                    <div class='input-group-append'>
                                                        <button class='btn btn-outline-success'
                                                                type='submit'
                                                                onclick='change_chart_form_action_according_to_selected_period("subscription_chart_credit_consumption_frm[{{ $subscription->id }}]");'>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_submit_btt_caption')) }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr class='{{ $power_consumption_row_class }}' data-snapshot-row-purpose='power_data_part'
                                        data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                    <?php // TODO: questo elemento <tr> ed il successivo dovrebbero essere viste come un'unica riga per quanto riguarda la funzionalità hoverable rows di Bootstrap (https://getbootstrap.com/docs/4.0/content/tables/#hoverable-rows) ?>
                                        <th class='col-5 col-md-4'>
                                            {{ ucfirst(__('users/my_subscriptions.socket_total_power_counter_caption')) }}
                                        </th>
                                        <td class='col-7 col-md-8' colspan='3'>
                                            <span data-snapshot-attribute='socket_power_cnt'>{{ number_format($subscription_last_snapshot->get_total_power_cnt(), 1, '.', '') }}</span>&nbsp;kWh
                                        </td>
                                    </tr>
                                    <?php // TODO: al momento sto mostrando sempre il <tr> che segue, ma devo mostrarlo solamente nei casi descritti dal commento qui sotto (definire una variabile apposita che conterrà la classe della riga ed outputtarla qui, settandola a d-none per default e togliendo il d-none se devo mostrare i dati. Ricordare anche che questa classe andrà manipolata anche dal js che viene triggerato alla selezione del posto barca per il quale visualizzare i dati): ?>
                                    <?php /* se $subscription_last_snapshot->berth è valorizzato ed esiste sul db, mostro i controlli utili a richiedere il grafico del consumo di energia della subscription sulla presa: */ ?>
                                    <tr class='{{ $power_consumption_row_class }}' data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        data-snapshot-row-purpose='power_chart_controls_part'
                                        title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                        <td class='d-none d-sm-table-cell without_upper_border'>
                                        </td>
                                        <td class='col-12 col-md-8 without_upper_border' colspan='3'>
                                            <form id='subscription_chart_power_consumption_frm[{{ $subscription->id }}]'
                                                data-snapshot-attribute='power_consumption_form'
                                                action="{{ route('get_subscription_consumption_chart_on_berth', ['subscription_id' => $subscription->id,
                                                                                                                 'berth_id' => $subscription_last_snapshot->berth->get_id(),
                                                                                                                 'begin_timestamp' => Carbon::now()->subHours(24)->timestamp,
                                                                                                                 'end_timestamp' => Carbon::now()->timestamp,
                                                                                                                 'subject' => ConsumptionReportsController::REPORT_VALUE_POWER,
                                                                                                                 'sampling_interval_argument' => ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR]) }}">
                                            @csrf
                                                <div class='input-group'>
                                                    <?php // TODO: su mobile, il select + pulsante sforano la larghezza della pagina ?>
                                                    <select class='custom-select' id='subscription_chart_power_consumption_frm_period_select[{{ $subscription->id }}]'
                                                            onchange='change_chart_form_action_according_to_selected_period("subscription_chart_power_consumption_frm[{{ $subscription->id }}]");'>
                                                        <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}' selected>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_period_select_last_24hours_caption')) }}
                                                        </option>
                                                        <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}'>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_period_select_last_week_caption')) }}
                                                        </option>
                                                        <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}'>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_period_select_last_month_caption')) }}
                                                        </option>
                                                    </select>
                                                    <div class='input-group-append'>
                                                        <button class='btn btn-outline-success' 
                                                                type='submit'
                                                                onclick='change_chart_form_action_according_to_selected_period("subscription_chart_power_consumption_frm[{{ $subscription->id }}]");'>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_submit_btt_caption')) }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr class='{{ $water_consumption_row_class }}' data-snapshot-row-purpose='water_data_part'
                                        data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                    <?php // TODO: questo elemento <tr> ed il successivo dovrebbero essere viste come un'unica riga per quanto riguarda la funzionalità hoverable rows di Bootstrap (https://getbootstrap.com/docs/4.0/content/tables/#hoverable-rows) ?>
                                        <th class='col-5 col-md-4'>
                                            {{ ucfirst(__('users/my_subscriptions.socket_total_water_counter_caption')) }}
                                        </th>
                                        <td class='col-7 col-md-8' colspan='3'>
                                            <span data-snapshot-attribute='socket_water_cnt'>{{ number_format($subscription_last_snapshot->get_total_water_cnt(), 3, '.', '') }}</span>&nbsp;m<sup>3</sup>
                                        </td>
                                    </tr>
                                    <?php // TODO: al momento sto mostrando sempre il <tr> che segue, ma devo mostrarlo solamente nei casi descritti dal commento qui sotto (definire una variabile apposita che conterrà la classe della riga ed outputtarla qui, settandola a d-none per default e togliendo il d-none se devo mostrare i dati. Ricordare anche che questa classe andrà manipolata anche dal js che viene triggerato alla selezione del posto barca per il quale visualizzare i dati): ?>
                                    <?php /* se $subscription_last_snapshot->berth è valorizzato ed esiste sul db, mostro i controlli utili a richiedere il grafico del consumo di energia della subscription sulla presa: */ ?>
                                    <tr class='{{ $water_consumption_row_class }}' data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        data-snapshot-row-purpose='water_chart_controls_part'
                                        title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                        <td class='d-none d-sm-table-cell without_upper_border'>
                                        </td>
                                        <td class='col-12 col-md-8 without_upper_border' colspan='3'>
                                            <form id='subscription_chart_water_consumption_frm[{{ $subscription->id }}]'
                                                data-snapshot-attribute='water_consumption_form'
                                                action="{{ route('get_subscription_consumption_chart_on_berth', ['subscription_id' => $subscription->id,
                                                                                                                 'berth_id' => $subscription_last_snapshot->berth->get_id(),
                                                                                                                 'begin_timestamp' => Carbon::now()->subHours(24)->timestamp,
                                                                                                                 'end_timestamp' => Carbon::now()->timestamp,
                                                                                                                 'subject' => ConsumptionReportsController::REPORT_VALUE_WATER,
                                                                                                                 'sampling_interval_argument' => ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR]) }}">
                                            @csrf
                                                <div class='input-group'>
                                                    <?php // TODO: su mobile, il select + pulsante sforano la larghezza della pagina ?>
                                                    <select class='custom-select' id='subscription_chart_water_consumption_frm_period_select[{{ $subscription->id }}]'
                                                            onchange='change_chart_form_action_according_to_selected_period("subscription_chart_water_consumption_frm[{{ $subscription->id }}]");'>
                                                        <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}' selected>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_period_select_last_24hours_caption')) }}
                                                        </option>
                                                        <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}'>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_period_select_last_week_caption')) }}
                                                        </option>
                                                        <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}'>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_period_select_last_month_caption')) }}
                                                        </option>
                                                    </select>
                                                    <div class='input-group-append'>
                                                        <button class='btn btn-outline-success' 
                                                                type='submit'
                                                                onclick='change_chart_form_action_according_to_selected_period("subscription_chart_water_consumption_frm[{{ $subscription->id }}]");'>
                                                            {{ ucfirst(__('users/my_subscriptions.chart_submit_btt_caption')) }}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        data-snapshot-row-purpose='instant_power_consumption'
                                        title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                        <th class='col-5 col-md-4'>
                                            {{ ucfirst(__('users/my_subscriptions.instantpowerconsumption_caption')) }}
                                        </th>
                                        <td class='col-7 col-md-8' colspan='3'>
                                            <span data-snapshot-attribute='socket_instant_power_consumption'>{{ number_format($subscription_last_snapshot->get_instant_power_consumption(), 3, '.', '') }}</span>&nbsp;kW
                                        </td>
                                    </tr>
                                    <tr data-toggle='{{ $no_recent_data_about_berth_snapshot_table_data_toggle_tooltip_attribute }}'
                                        data-placement='{{ $no_recent_data_about_berth_snapshot_table_data_placement_tooltip_attribute }}'
                                        data-snapshot-row-purpose='last_update'
                                        title='{{ $no_recent_data_about_berth_snapshot_table_title_tooltip_attribute }}'>
                                        <th class='col-5 col-md-4'>
                                            {{ ucfirst(__('users/my_subscriptions.lastupdatetimestamp_caption')) }}
                                        </th>
                                        <td class='col-7 col-md-8' colspan='3' data-snapshot-attribute='socket_last_update_timestamp'>
                                            {{ $snapshot_timestamp->format(config('dates_currency_and_locale.local_datetime_format_full_carbon')) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class='container mt-4 mb-5 mx-auto {{ $no_status_information_available_for_this_subscription_message_visibility_class }}'
                                data-snapshot-purpose='no_status_info_available'>
                                <div class='table-responsive row'>
                                    <h6 class='text-secondary text-uppercase font-weight-bold text-center'>
                                        {{ ucfirst(__('users/my_subscriptions.no_status_information_available_for_this_subscription')) }}
                                    </h6>
                                </div>
                            </div>
                        </div>  <?php // fine del div socket_status (TODO: rinominarlo in subscription_status: attenzione che la classe è utilizzata dal js per identificare il div, quindi andrà cambiata anche lì) ?>
                    @else       <?php /* se non esiste nessuno snapshot per il contratto (ovvero se il contratto non ha ancora effettuato consumi nell'impianto): */ ?>
                        <div class='container mt-5 mb-5 mx-auto'>
                            <div class='table-responsive row'>
                                <h6 class='text-secondary text-uppercase font-weight-bold text-center'>
                                    {{ ucfirst(__('users/my_subscriptions.no_status_information_available_for_this_subscription')) }}
                                </h6>
                            </div>
                        </div>
                    @endif
                </div>  <?php /* fine <div class='container' */ ?>
            @endforeach
        @endif
        <?php // se subscriptions non contiene record, verrà visualizzato il div seguente: ?>
        <div class='container mt-5 mb-5 text-center {{ $no_subscriptions_in_progress_message_visibility_class }}'>
            <h6 class='text-secondary text-uppercase font-weight-bold'>
                {{ ucfirst(__('users/my_subscriptions.you_currently_have_no_subscriptions_in_progress')) }}
            </h6>
        </div>
    </div>
@endsection













