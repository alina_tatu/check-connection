<?php
    use Illuminate\Support\Carbon;
    use App\Http\Controllers\ConsumptionReportsController;
    // TODO: devo rendere asincrona anche la generazione dei grafici di consumo, così come ho fatto per i report?
?>
@extends('layouts.app_menu')

@section('custom_js')
    <!--Load the AJAX API-->
    <script type='application/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
    <script type='application/javascript'>
        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages':['corechart']});
        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);
        // Callback that creates and populates a data table, instantiates the chart, passes in the data and draws it.
        function drawChart() {
            // Set chart options
            var options = {
                hAxis: {
                    direction: -1,
                    slantedText: true,
                    slantedTextAngle: 55,
                    title: '{{ $x_axis_caption }}',
                    textStyle: { 
                        fontSize: 13
                    },
                },
                vAxis:  {
                    format: '0.00',
                    title: '{{ $y_axis_caption }}',
                    @if((count($chart) > 0) && !(max($chart) > 0))
                    <?php // se il grafico non contiene record con consumo > 0 (ma contiene dei record di consumo: aggiungo questo controllo perchè se lanciato su un array vuoto, max() va in fatal error): ?>
                        <?php // inibisco la visualizzazione della parte negativa dell'asse y (viene mostrata solamente se 1. ci sono valori che vanno sotto lo zero oppure 2. non c'è nessun valore diverso da zero). Per nasconderla non mi basta settare un min ma devo settare anche il max, perciò lo faccio solo in questo caso (dato che in caso di consumi a zero, qualsiasi valore massimo non mi creerà problemi): ?>
                        viewWindow: {
                            min: 0,
                            max: 10
                        },
                    @endif
                },
                title: "{!! $chart_title !!}",  <?php /* evito l'escape di $chart_title per risolvere il problema degli apostrofi encodati come htmlentities quando l'utente usa la lingua italiana */ ?>
                width: '100%',
                height: window.innerHeight,
                legend: {
                    position: 'none',
                },
            };
            // Create the data table:
            var data = new google.visualization.DataTable();
            data.addColumn('string', '{{ $x_axis_caption }}');
            data.addColumn('number', '{{ $y_axis_caption }}');
            var data = google.visualization.arrayToDataTable([
            // ricordare che - anche se non si vedono alcune label sull'asse x - i dati potrebbero essere comunque graficati: quando le label sono troppo fitte, Google Charts ne salta alcune (ad esempio: ne scrive una sì e una no) per fare in modo che rimangano leggibili
                ['{{ $x_axis_caption }}', '{{ $y_axis_caption }}', { role: 'style' }],
                @foreach(array_reverse($chart) as $entry_key => $entry_value)           <?php /* Google Charts vuole che gli presenti l'array al contrario, non ho mai capito perchè */ ?>
                    <?php
                        if($entry_value < 0) {
                        // il 10.06.2020, concordato con raffaele che - se mi capita di calcolare un valore negativo per i consumi - è meglio troncarlo a zero che non visualizzarlo come negativo, dato che un consumo negativo non ha senso:
                            $entry_value = 0;
                        }
                    ?>
                    [ '{{ $entry_key }}', {{ $entry_value }}, '{{ $chart_data_color }}' ],
                @endforeach
            ]);
            @if(count($chart) > 0)
                // Instantiate and draw our chart, passing in some options:
                @switch($chart_type)
                    @case(ConsumptionReportsController::GOOGLE_CHARTS_TYPE_COLUMN)
                    @default
                        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                        @break
                    @case(ConsumptionReportsController::GOOGLE_CHARTS_TYPE_LINE)
                        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                        @break
                @endswitch
                chart.draw(data, options);
            @endif
        }
    </script>
    <script>
        <?php /* ridimensiona il grafico quando si ridimensiona la pagina: */ ?>
        window.onresize = function(event) {
            drawChart();
        };
    </script>
@endsection

@section('content')
    <div id='content' class='mt-3 mb-3 pt-2'>
        <div class='container'>
            @if(count($chart) > 0)
                @if(isset($epower_had_disconnections_during_interval) && ($epower_had_disconnections_during_interval === true))
                    <div class='text-center text-danger'>
                        {{ ucfirst(__('consumption_chart.epower_had_disconnections_during_interval_message')) }}
                    </div>
                @endif
                @if($epower_id_has_changed_during_interval === true)
                    <div class='text-center text-danger'>
                        {{ ucfirst(__('common.epower_has_been_replaced_message')) }}
                    </div>
                @endif
                <div id='chart_div'>
                </div>
            @else
                <div class='text-center text-secondary mt-5 pt-3 mb-4'>
                    <h6 class='font-weight-bold'>
                        @if(Auth::user()->is_viewer_admin())
                            {{ ucfirst(__('consumption_chart.there_is_not_enough_consumption_data_to_draw_the_chart_msg_admin_caption', ['app_name' => config('app.name')])) }}
                        @else
                            {{ ucfirst(__('consumption_chart.there_is_not_enough_consumption_data_to_draw_the_chart_msg_simple_user_caption', ['app_name' => config('app.name')])) }}
                        @endif
                    </h6>
                </div>
            @endif
        </div>
        @if(($epower_id_has_changed_during_interval === true) && (Auth::user()->is_viewer_admin()))
            <div class='container px-5 mt-3 text-center text-danger'>
                {{ __('common.epower_has_been_replaced_message') }}
            </div>
        @endif
        @if((config('marina.show_socket_disconnection_alerts_on_charts') === true) && (Auth::user()->is_viewer_admin()))
            @if($there_were_disconnections_during_interval === true)
                <div class='container px-5 mt-3 text-center text-danger'>
                    {{ __('consumption_chart.there_were_disconnections_message') }}
                </div>
            @endif
        @endif
        <div class='text-center pt-3'>
            <button type='button' class='btn btn-secondary' onclick='location.href="{{ url()->previous() }}"'>
                {{ ucfirst(__('consumption_chart.back_to_previous_page_btt_caption')) }}
            </button>
        </div>
    </div>
@endsection
