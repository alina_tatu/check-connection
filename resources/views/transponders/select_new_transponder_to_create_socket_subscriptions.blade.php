@extends('layouts.app_menu')

@section('content')
    <div id='content' class='container mt-5 pt-5'>
        @if($transponders_with_postpaid_subscriptions_not_bound_to_sockets->count() > 0)
            <form method="post" action="{{ route('enable_transponder_to_create_socket_subscriptions') }}">
            <?php // TODO: questo form è troppo largo sia su mobile, sia su desktop (confrontarlo ad esempio con il form di nuovo utente) ma non riesco a capire perchè ?>
            @csrf
                <input type="hidden" name="berth_on_which_the_subscription_must_be_created" 
                       value="{{ request()->input('berth_on_which_the_subscription_must_be_created') }}">
                <div class="form-group row">
                    <select class="form-control custom-select" name="transponder_id">
                        <option>{{ strtolower(__('transponders/select_new_transponder_to_create_socket_subscriptions.select_a_transponder_no_option_selected_caption')) }}</option>
                        @foreach($transponders_with_postpaid_subscriptions_not_bound_to_sockets as $current_transponder)
                            <?php 
                                $current_transponder_caption_to_display = $current_transponder->code;
                                $last_subscription = $current_transponder->active_subscriptions()
                                                                         ->newerFirst()
                                                                         ->first();
                                if(($last_subscription_user = $last_subscription->user) && ($last_subscription_user->count() > 0)) {
                                    $last_subscription_user_fullname_to_display = $last_subscription_user->get_full_name();
                                    $current_transponder_caption_to_display .= " (".$last_subscription_user->get_full_name().")";
                                }
                            ?>
                            <option value="{{ $current_transponder->code }}">{{ $current_transponder_caption_to_display }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group row">
                    <button type="submit" class="btn btn-primary">{{ ucfirst(__('transponders/select_new_transponder_to_create_socket_subscriptions.submit_btt_caption')) }}</button>
                </div>

            </form>
        @else
            <div class='container-fluid mt-5 pt-5 text-center'>
                <h6 id='no_entries_message' class='text-secondary text-uppercase font-weight-bold'>
                    {{ ucfirst(__('transponders/select_new_transponder_to_create_socket_subscriptions.select_a_transponder_no_transponders_found')) }}
                </h6>
            </div>
        @endif
    </div>
@endsection