<?php 
    use App\Berth;
    use App\SocketSnapshot;
?>
@extends('layouts.app_menu')

@section('content')
    <div id='content' class='mt-5 pt-5'>
        <?php  // TODO: aggiungere una toolbar per fare filtraggio/ordinamento dei record? ?>
        @if(($log) && ($log->count() > 0))
            <div class='container table-responsive'>
                <table class='table table-striped table-hover'>
                    <thead>
                        <tr>
                            <th class='text-center'>{{ ucfirst(__('refunds_log.th_timestamp')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('refunds_log.th_berth')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('refunds_log.th_action_type')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('refunds_log.th_subscription_owner_user')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('refunds_log.th_amount_to_refund')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('refunds_log.th_requesting_user')) }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($log as $refunds_entry)
                            <?php 
                                $berth = Berth::find($refunds_entry->action->get_object_id());
                                $socket_snapshot = SocketSnapshot::find($refunds_entry->socket_snapshot_id);
                            ?>
                            <tr class='user_row'>
                                @if(($refunds_entry->action) && ($refunds_entry->action->count() > 0))
                                    <td class='text-center'>
                                        @if(!is_null($refunds_entry->action->get_completion_timestamp()))
                                            {{ $refunds_entry->action
                                                             ->get_completion_timestamp()
                                                             ->tz(config('dates_currency_and_locale.local_timezone')) 
                                                             ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon')) }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class='text-center'>
                                        @if(($berth) && ($berth->count() > 0))
                                            <span class='text-info p-2 d-inline font-weight-bold text-nowrap'>
                                                <i class="fas fa-anchor"></i>
                                                {{ $berth->get_description() }}
                                            </span>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class='text-center'>
                                        {{ $refunds_entry->action->get_type() }}
                                    </td>
                                    <td class='text-center'>
                                        @if(($socket_snapshot) && ($socket_snapshot->count() > 0))
                                            <?php
                                            	if(boolval($socket_snapshot->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
										        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
													$subscription = $socket_snapshot->subscription;
										        } else {
										        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
										        	$subscription = $socket_snapshot->retrieve_connected_subscription();
										        }
                                            ?>
                                            @if((($subscription) && ($subscription->count() > 0)) && ($subscription->user->count() > 0))
                                                {{ $subscription->user->get_full_name() }}
                                                @if(!is_null($subscription->user->get_username()))
                                                    <span class='text-light bg-primary p-1 rounded socket_user_username_span'>
                                                        {{ $subscription->user->get_username() }}
                                                    </span>
                                                @endif
                                                <?php // TODO: devo mostrare anche il nome della barca? In quel caso, nel momento in cui aggiungerò i filtri dovrò poter filtrare anche per quel dato ?>
                                            @else
                                                -
                                            @endif
                                        @else
                                            -
                                        @endif
                                    </td>
                                @else
                                    <td class='text-center'>
                                        -
                                    </td>
                                    <td class='text-center'>
                                        -
                                    </td>
                                    <td class='text-center'>
                                        -
                                    </td>
                                @endif
                                <td class='text-center'>
                                    {{ number_format($refunds_entry->money_on_the_socket_when_resetted, 2, '.', '') }}&nbsp;{{ config('dates_currency_and_locale.currency_character') }}
                                    <?php // TODO: al momento c'è una entry anche per le azioni di spegnimento su prese che avevano il credito a zero. In quel caso non devo generarlo, in modo che qui finiscano solo gli spegnimenti che hanno avuto una perdita (oppure lascio così e metto la possibilità di filtrare togliendo quelli che hanno zero, magari settando questa opzione come default nel filtro quando carico la pagina? In questo modo - se un cliente dice di aver perso dei soldi anche quando non è vero - qui troverò la riga e potrò mostrargli che aveva zero) ?>
                                </td>
                                <td class='text-center'>
                                    @if(($refunds_entry->action->requesting_user) && ($refunds_entry->action->requesting_user->count() > 0))
                                        {{ $refunds_entry->action->requesting_user->get_full_name() }}
                                        @if(!is_null($refunds_entry->action->requesting_user->get_username()))
                                            <span class='text-light bg-primary p-1 rounded requesting_user_username_span'>
                                                {{ $refunds_entry->action->requesting_user->get_username() }}
                                            </span>
                                        @endif
                                    @else
                                        -
                                    @endif
                                </td>
                                <?php // TODO: alla fine dovrò prevedere la possibilità di flaggare il rimborso come effettuato, e di segnare chi l'ha effettuato. Forse devo poter filtrare anche per stato del rimborso (effettuato/non effettuato) ?>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div id='no_entries_message' class='container-fluid mt-5 pt-5 text-center'>      <?php // TODO: al momento viene mostrato solo staticamente, se non ci sono righe nel log. Una volta aggiunta la funzionalità di filtraggio, devo mostrarlo/nasconderlo dinamicamente (e modificare il messaggio) ?>
                <h6 class='text-secondary text-uppercase font-weight-bold'>
                    {{ ucfirst(__('refunds_log.no_entries_message')) }}         <?php // TODO: modificare le due translations per questo messaggio? Non mi piacciono granchè... ?>
                </h6>
            </div>
        @endif
    </div>
@endsection