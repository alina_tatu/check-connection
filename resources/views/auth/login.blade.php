<?php 
    $login_logo_img_url = file_exists(public_path('resources/plant_dependent/img/login_logo.png')) ? url('resources/plant_dependent/img/login_logo.png') : url('resources/login_logo.default.png');
?>
@extends('layouts.app_nomenu')

@section('content')
    <section class='container mt-5'>
        <section class='login-form'>
            <div class='row justify-content-center'>
                <div class='col-md-8'>
                    <div class='card'>
                        <div class='card-header'>{{ config('marina.name') }} - {{ config('app.name') }} {{ ucfirst(__('auth.login_card_header_caption')) }}</div>
                        <div class='card-body'>
                            <div class='text-center mt-2 mb-4'>
                                <img src="{{ $login_logo_img_url }}" class='img-responsive mw-100 h-auto' alt=''>
                            </div>
                            <form method='POST' action='{{ route('login') }}' class='mt-2'>
                            @csrf
                                <div class='form-group row'>
                                    <label for='login' class='col-md-4 col-form-label text-md-right'>
                                        {{ ucfirst(__('auth.login_username_or_email_field_label_caption')) }}
                                    </label>

                                    <div class='col-md-6'>
                                        <input id='login' type='text' name='login' value='{{ old('login') }}'
                                               class='form-control {{ $errors->has('login') || $errors->has('email') || $errors->has('username') ? ' is-invalid' : '' }}' required  autofocus>
                                        <span class='invalid-feedback' role='alert'>
                                            @error('login')
                                                <strong>{{ $message }}</strong>
                                            @enderror
                                            @error('email')
                                                <strong>{{ $message }}</strong>
                                            @enderror
                                            @error('username')
                                                <strong>{{ $message }}</strong>
                                            @enderror
                                        </span>
                                    </div>
                                </div>

                                <div class='form-group row'>
                                    <label for='password' class='col-md-4 col-form-label text-md-right'>
                                        {{ ucfirst(__('auth.login_password_field_label_caption')) }}
                                    </label>

                                    <div class='col-md-6'>
                                        <input id='password' type='password' class='form-control {{ $errors->has('password') ? ' is-invalid' : '' }}' name='password' required autocomplete='current-password'>
                                        <span class='invalid-feedback' role='alert'>
                                            @error('password')
                                                <strong>{{ $message }}</strong>
                                            @enderror
                                        </span>
                                    </div>
                                </div>

                                <div class='form-group row'>
                                    <div class='col-md-6 offset-md-4'>
                                        <div class='form-check'>
                                            <input class='form-check-input' type='checkbox' name='remember'
                                                   id='remember' {{ old('remember') ? 'checked' : '' }}>

                                            <label class='form-check-label' for='remember'>
                                                {{ ucfirst(__('auth.login_remember_me_chk_label_caption')) }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class='form-group row mb-0'>
                                    <div class='col-md-4 col-lg-2 offset-md-4'>
                                        <button type='submit' class='btn btn-primary btn-block'>
                                            {{ ucfirst(__('auth.login_confirm_btt_caption')) }}
                                        </button>
                                    </div>
                                    <a class='btn btn-link' href='{{ route('register') }}'>
                                        {{ ucfirst(__('auth.login_register_page_link_caption')) }}
                                    </a>
                                    @if(Route::has('password.request'))
                                        <a class='btn btn-link' href='{{ route('password.request') }}'>
                                            {{ ucfirst(__('auth.login_forgot_password_link_caption')) }}
                                        </a>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </section>
    </section>
@endsection
