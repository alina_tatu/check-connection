<?php
    /* TODO: da testare, ma mi sa che non la sto attualmente usando (cfr. https://www.codermen.com/blog/122/password-confirmation-flow-for-logged-in-users-in-laravel-6-2). In teoria non dovrebbe servirmi. Sembra essere la funzionalità che nel TagManager richiede la password dell'utente per ogni azione come scrittura tag, modifica costi, etc. */
    
    $login_logo_img_url = file_exists(public_path('resources/plant_dependent/img/login_logo.png')) ? URL::asset('resources/plant_dependent/img/login_logo.png') : URL::asset('resources/login_logo.default.png');
?>
@extends('layouts.app_nomenu')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class='card-header'>{{ config('marina.name') }} - {{ config('app.name') }} {{ ucfirst(__('auth.confirm_password_card_header_caption')) }}</div>

                <div class="card-body">
                    <div class='text-center mt-2 mb-4'>
                        <img src='{{ $login_logo_img_url }}' class='img-responsive mw-100 h-auto' alt=''>
                    </div>
                    {{ __('Please confirm your password before continuing.') }}

                    <form method="POST" action="{{ route('password.confirm') }}">
                    @csrf
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ ucfirst(__('auth.password_field_label') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ucfirst(__('auth.password_confirmation_field_label') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ ucfirst(__('auth.login_forgot_password_link_caption')) }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
