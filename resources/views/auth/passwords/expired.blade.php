@extends('layouts.app_menu')

@section('content')
    <?php // TODO: questa roba è da stilizzare meglio, fa schifo. Usare lo stesso form della view di login? ?> 
    <div class="container mt-5 pt-5">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ ucfirst(__('auth.password_expired_page_header_caption')) }}
                    </div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                            <a href="/">{{ ucfirst(__('auth.password_expired_page_return_to_homepage_link_caption')) }}</a>
                        @else
                        <div class="alert alert-info">
                            {{ ucfirst(__('auth.password_expired_page_your_password_has_expired_please_change_it_msg_caption')) }}
                        </div>
                        <form class="form-horizontal" method="POST" action="{{ route('password.post_expired') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                                <label for="current_password" class="col-md-4 control-label">{{ ucfirst(__('auth.password_expired_page_current_password_input_caption')) }}</label>

                                <div class="col-md-6">
                                    <input id="current_password" type="password" class="form-control" name="current_password" required="">

                                    @if ($errors->has('current_password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('current_password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">{{ ucfirst(__('auth.password_expired_page_new_password_input_caption')) }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required="">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">{{ ucfirst(__('auth.password_expired_page_confirm_new_password_input_caption')) }}</label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required="">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ ucfirst(__('auth.password_expired_page_submit_btt_caption')) }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection