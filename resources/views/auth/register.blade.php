<?php $login_logo_img_url = file_exists(public_path('resources/plant_dependent/img/login_logo.png')) ? URL::asset('resources/plant_dependent/img/login_logo.png') : URL::asset('resources/login_logo.default.png'); ?>
@extends('layouts.app_nomenu')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class='card-header'>{{ config('marina.name') }} - {{ config('app.name') }} {{ ucfirst(__('auth.signup_page_card_header_caption')) }}</div>
                <div class="card-body">
                    <div class='text-center mt-2 mb-4'>
                        <img src="{{ $login_logo_img_url }}" class='img-responsive mw-100 h-auto' alt=''>
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                    @csrf
                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ ucfirst(__('auth.user_first_name')) }}</label>

                            <div class="col-md-6">
                                <input id="first_name"
                                       type="text" 
                                       class="form-control @error('first_name') is-invalid @enderror" 
                                       name="first_name" 
                                       value="{{ !is_null(request()->input('first_name')) ? request()->input('first_name') : old('first_name') }}" 
                                       autocomplete="first_name" 
                                       required 
                                       autofocus>

                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ ucfirst(__('auth.user_last_name')) }}</label>

                            <div class="col-md-6">
                                <input id="last_name" 
                                       type="text" 
                                       class="form-control @error('last_name') is-invalid @enderror" 
                                       name="last_name" 
                                       value="{{ !is_null(request()->input('last_name')) ? request()->input('last_name') : old('last_name') }}" 
                                       autocomplete="last_name" 
                                       required 
                                       autofocus>

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ ucfirst(__('auth.user_email_address')) }}</label>

                            <div class="col-md-6">
                                <input id="email" 
                                       type="email" 
                                       class="form-control @error('email') is-invalid @enderror" 
                                       name="email" 
                                       value="{{ !is_null(request()->input('email')) ? request()->input('email') : old('email') }}" 
                                       autocomplete="email"
                                       required >

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $message !!}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ ucfirst(__('auth.user_username')) }}</label>

                            <div class="col-md-6">
                                <input id="username" 
                                       type="text" 
                                       class="form-control @error('username') is-invalid @enderror" 
                                       name="username" 
                                       value="{{ !is_null(request()->input('username')) ? request()->input('username') : old('username') }}" 
                                       autocomplete="username"
                                       required >

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{!! $message !!}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ ucfirst(__('auth.password_field_label')) }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ ucfirst(__('auth.password_confirmation_field_label')) }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
