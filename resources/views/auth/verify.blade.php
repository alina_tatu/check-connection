<?php
    /* TODO: da testare, ma mi sa che non la sto attualmente usando (cfr. https://laravel.com/docs/6.x/verification#verification-routing). In teoria non dovrebbe servirmi */

    $login_logo_img_url = file_exists(public_path('resources/plant_dependent/img/login_logo.png')) ? URL::asset('resources/plant_dependent/img/login_logo.png') : URL::asset('resources/login_logo.default.png');
?>
@extends('layouts.app_nomenu')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class='card-header'>{{ config('marina.name') }} - {{ config('app.name') }} {{ ucfirst(__('auth.email_verification_card_header_caption')) }}</div>

                <div class="card-body">
                    <div class='text-center mt-2 mb-4'>
                        <img src="{{ $login_logo_img_url }}" class='img-responsive mw-100 h-auto' alt=''>
                    </div>
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('auth.a_fresh_verification_link_has_been_sent_to_your_email_address') }}
                        </div>
                    @endif

                    {{ ucfirst(__('auth.before_proceeding_please_check_email_for_verification_link')) }}
                    {{ ucfirst(__('auth.if_you_did_not_receive_the_email')) }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                    @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ strtolower(__('auth.click_here_to_request_another_verification_link')) }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
