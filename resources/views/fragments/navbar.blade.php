<?php
    use Illuminate\Support\Facades\Auth;
    use App\User;
?>
<?php // TODO: evidenziare la voce relativa alla pagina corrente ?>
<link href='{{ URL::asset('css/navbar.css') }}' rel='stylesheet'>
<nav class='navbar fixed-top fixed-left navbar-expand-lg navbar-dark bg-secondary shadow-sm'>
    <div class='container-fluid'>
        <a class='navbar-brand' href='{{ config("app.url") }}' data-toggle='modal' data-target='#version_modal'>
            {{ config('app.name') }}
        </a>
        <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='{{ __('Toggle navigation') }}'>
            <span class='navbar-toggler-icon'></span>
        </button>
        <div class='collapse navbar-collapse' id='navbarSupportedContent'>
            @auth
                <ul id='navbar_left_container' class='navbar-nav mr-auto'>
                    <!-- Left Side Of Navbar -->
                    <li class='nav-item'>
                        <a class='nav-link' href='{{ route("notifications_page") }}'>
                           {{ ucfirst(__('navbar.notifications_link')) }}
                            <?php
                                $unread_notification_badge_visibility_class = 'd-none';
                                $unread_notifications_count = Auth::user()->unreadNotifications->count();
                                if($unread_notifications_count > 0) {
                                    $unread_notification_badge_visibility_class = '';
                                }
                            ?>
                            <span id="navbar_unread_notifications_count_badge" class="badge badge-dark {{ $unread_notification_badge_visibility_class }}">
                                {{ $unread_notifications_count }}
                            </span>
                        </a>
                    </li>
                    @if(Auth::user()->is_viewer_admin())
                        <li class='nav-item'>
                            <a class='nav-link' href='{{ route("plant_status") }}'>{{ ucfirst(__('navbar.plant_status_link')) }}</a>
                        </li>
                        <li class='nav-item'>
                            <a class='nav-link' href='{{ route("plant_map") }}'>{{ ucfirst(__('navbar.plant_map_link')) }}</a>
                        </li>
                        @if(config('marina.manage_customers_and_subscriptions_via_web') === true)
                        <?php // menu visualizzato solo nei porti in cui è possibile modificare i clienti ed i contratti legati ai posti barca via webapp (al momento della scrittura di questo commento, sono solo i porti gestiti da Farrokh nella zona di Dubai + PortoMontenegro a partire da dicembre 2020) e Ayia Napa a partire da febbraio 2021: ?>
                            <li class='nav-item dropdown'>
                                <a id='navbarManageDropdown' class='nav-link dropdown-toggle' href='#' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' v-pre>
                                    {{ ucfirst(__('navbar.manage_dropdown')) }}<span class='caret'></span>
                                </a>
                                <div class='dropdown-menu dropdown-menu-left' aria-labelledby='navbarManageDropdown'>
                                    <a class='dropdown-item' href='{{ route("user_list") }}'>
                                        {{ ucfirst(__('navbar.manage_users_link')) }}
                                    </a>
                                    <a class='dropdown-item' href='{{ route("subscription_list") }}'>
                                        {{ ucfirst(__('navbar.manage_subscriptions_link')) }}
                                    </a>
                                    <a class='dropdown-item' href='{{ route("berth_list") }}'>
                                        {{ ucfirst(__('navbar.manage_berths_link')) }}
                                    </a>
                                </div>
                            </li>
                        @endif
                    @endif
                    <li class='nav-item'>
                        <a class='nav-link' href='{{ route("my_subscriptions") }}'>{{ ucfirst(__('navbar.my_subscriptions_link')) }}</a>
                    </li>
                    @if((Auth::user()->is_viewer_admin()) || (config('marina.standard_users_can_access_consumption_reports_page') === true))
                    <?php // TODO: a marzo 2021 ho riscontrato un problema sui report richiesti da utenti non-admin: la notifica di report creato andava in crash e l'elaborazione del job non veniva mai completata. Il problema non si verificava per gli utenti admin o supervisor. Mentre cercavo di capire il problema ho quindi aggiunto la possibilità di nascondere questa pagina per gli utenti standard. Il problema è stato poi risolto con la versione 2.0.163d, perciò ora potrei anche (1) eliminare il parametro di configurazione in .env e nei file config/marina.php (2) togliere questo if e mostrare la voce di menu sempre, per tutti gli utenti ?>
                        <li class='nav-item'>
                            <a class='nav-link' href='{{ route("open_consumption_reports_page") }}'>{{ ucfirst(__('navbar.consumption_reports_page_link')) }}</a>
                        </li>
                    @endif
                    @if(Auth::user()->is_viewer_admin())
                        <li class='nav-item'>
                            <a class='nav-link' href='{{ route("open_refunds_log_page") }}'>{{ ucfirst(__('navbar.lost_credit_entries_for_resetted_sockets_page_link')) }}</a>
                        </li>
                    @endif
                </ul>
            @endauth
            <!-- Right Side Of Navbar -->
            <ul id='navbar_right_container' class='navbar-nav ml-auto'>
                <!-- Authentication Links -->
                @guest
                    <li class='nav-item'>
                        <a class='nav-link' href='{{ route("login") }}'>{{ ucfirst(__('navbar.login_link')) }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class='nav-item'>
                            <a class='nav-link' href='{{ route("register") }}'>{{ ucfirst(__('navbar.signup_link')) }}</a>
                        </li>
                    @endif
                @else
                    <li class='nav-item dropdown'>
                        <a id='navbarDropdownParent' class='nav-link dropdown-toggle' href='#' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' v-pre>
                            {{ ucfirst(__('navbar.welcome_msg')) }}, {{ Auth::user()->username }} <span class='caret'></span>
                        </a>
                        <div id='navbarDropdownContent' class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdownParent'>
                            <a class='dropdown-item' href='{{ route("open_user_settings_page") }}'>
                                {{ ucfirst(__('navbar.user_settings_link')) }}
                            </a>
                            <a class='dropdown-item' href='{{ route("logout") }}'
                                onclick='event.preventDefault();
                                                document.getElementById("logout-form").submit();'>
                                {{ ucfirst(__('navbar.logout_link')) }}
                            </a>
                            <form id='logout-form' action='{{ route("logout") }}' method='POST' style='display: none;'>
                            @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
<?php /* modal caricato quando si clicca per verificare il numero di versione: */ ?>
<div class='modal fade' id='version_modal' tabindex='-1' role='dialog'
     aria-labelledby='version_modal' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content'>
            <div class='modal-body'>
                {{ config('app.name') }} v.{{ config('app.version') }}
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary' data-dismiss='modal'>
                    {{ ucfirst(__('navbar.version_modal_dismiss_btt_caption')) }}
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    function refresh_unread_notifications_count_on_navbar() {
        let unread_notifications_count;
        let request = new XMLHttpRequest();
        let unread_notifications_count_element_on_navbar = document.getElementById("navbar_unread_notifications_count_badge");
        request.open('GET', "{{ route('get_unread_notifications_count_for_logged_user_via_ajax', ['username' => Auth::user()->get_username()]) }}");
        request.onload = function() {
            if(request.status === 200) {
                let parsed_response = JSON.parse(request.responseText);
                unread_notifications_count = parsed_response.unread_notifications_count;
                if(unread_notifications_count > 0) {
                    if(unread_notifications_count_element_on_navbar.classList.contains('d-none')) {
                        unread_notifications_count_element_on_navbar.classList.remove('d-none');
                    }
                    unread_notifications_count_element_on_navbar.innerHTML = unread_notifications_count;
                }
            }
        };
        request.send();
    }
    window.setInterval(function() {
        refresh_unread_notifications_count_on_navbar();
    }, {{ config('marina.notifications_refresh_interval_seconds') * 1000 }});
</script>
