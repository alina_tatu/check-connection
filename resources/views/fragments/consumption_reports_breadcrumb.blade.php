<?php 
    use App\Http\Controllers\ConsumptionReportsController;
    use App\Berth;
    use App\Subscription;
    use App\User;

    $breadcrumb_array = [];
    if(!is_null(request()->input('report_type_select'))) {
        switch(request()->input('report_type_select')) {
            case ConsumptionReportsController::SUBJECT_TYPE_MARINA:
                if(!in_array(ucfirst(__('consumption_reports.type_select_plant_consumption_option_caption')), $breadcrumb_array)) {
                    $breadcrumb_array[] = ucfirst(__('consumption_reports.type_select_plant_consumption_option_caption'));
                }
                break;
            case ConsumptionReportsController::SUBJECT_TYPE_PONTOON: 
                if(!in_array(ucfirst(__('consumption_reports.type_select_pontoon_consumption_option_caption')), $breadcrumb_array)) {
                    $breadcrumb_array[] = ucfirst(__('consumption_reports.type_select_pontoon_consumption_option_caption'));
                }
                if(!is_null(request()->input('pontoon_select'))) {
                    $selected_pontoon_name = ucfirst(__('consumption_reports.pontoon_field_label_caption'))." ".request()->input('pontoon_select');
                    if(!in_array($selected_pontoon_name, $breadcrumb_array)) {
                        $breadcrumb_array[] = $selected_pontoon_name;
                    }
                }
                break;
            case ConsumptionReportsController::SUBJECT_TYPE_BERTH: 
                if(!in_array(ucfirst(__('consumption_reports.type_select_berth_consumption_option_caption')), $breadcrumb_array)) {
                    $breadcrumb_array[] = ucfirst(__('consumption_reports.type_select_berth_consumption_option_caption'));
                }
                if(!is_null(request()->input('berth_select'))) {
                    $selected_berth = Berth::findOrFail(request()->input('berth_select'));
                    $selected_berth_name = $selected_berth->get_description();
                    if(!in_array($selected_berth_name, $breadcrumb_array)) {
                        $breadcrumb_array[] = $selected_berth_name;
                    }
                }
                break;
            case ConsumptionReportsController::SUBJECT_TYPE_SUBSCRIPTION:
                if(!in_array(ucfirst(__('consumption_reports.type_select_subscription_consumption_option_caption')), $breadcrumb_array)) {
                    $breadcrumb_array[] = ucfirst(__('consumption_reports.type_select_subscription_consumption_option_caption'));
                }
                if(!is_null(request()->input('subscription_select'))) {
                    $selected_subscription = Subscription::findOrFail(request()->input('subscription_select'));
                    $subscription_user = $selected_subscription->user;
                    if(($subscription_user) && ($subscription_user->count() > 0)) {
                        $selected_subscription_caption = $subscription_user->get_full_name();
                    } else {
                        $selected_subscription_caption = ucfirst(__('common.unknown_user'));
                    }
                    if($selected_subscription->is_bound_to_socket()) {
                        if(($selected_subscription->berth) && ($selected_subscription->berth->count() > 0)) {
                            $selected_subscription_caption .= ' ('.strtolower(__('consumption_reports.subscription_bound_berth_caption')).': '.$selected_subscription->berth->get_description().')';
                        }
                    } else {
                        if(($selected_subscription->transponder) && ($selected_subscription->transponder->count() > 0)) {
                            $selected_subscription_caption .= ' ('.strtolower(__('consumption_reports.subscription_bound_transponder_caption')).': '.$selected_subscription->transponder->get_code().')';
                        }
                    }
                    if(!in_array($selected_subscription_caption, $breadcrumb_array)) {
                        $breadcrumb_array[] = $selected_subscription_caption;
                    }
                }
                if(!is_null(request()->input('subscription_select'))) {
                    $selected_subscription = Subscription::findOrFail(request()->input('subscription_select'));
                    $subscription_user = $selected_subscription->user;
                    if(($subscription_user) && ($subscription_user->count() > 0)) {
                        $selected_subscription_caption = $subscription_user->get_full_name();
                    } else {
                        $selected_subscription_caption = ucfirst(__('common.unknown_user'));
                    }
                    if($selected_subscription->is_bound_to_socket()) {
                        if(($selected_subscription->berth) && ($selected_subscription->berth->count() > 0)) {
                            $selected_subscription_caption .= ' ('.strtolower(__('consumption_reports.subscription_bound_berth_caption')).': '.$selected_subscription->berth->get_description().')';
                        }
                    } else {
                        if(($selected_subscription->transponder) && ($selected_subscription->transponder->count() > 0)) {
                            $selected_subscription_caption .= ' ('.strtolower(__('consumption_reports.subscription_bound_transponder_caption')).': '.$selected_subscription->transponder->get_code().')';
                        }
                    }
                    if(!in_array($selected_subscription_caption, $breadcrumb_array)) {
                        $breadcrumb_array[] = $selected_subscription_caption;
                    }
                    if(!is_null(request()->input('subscription_berth_select'))) {
                        $selected_berth_for_subscription = Berth::findOrFail(request()->input('subscription_berth_select'));
                        $selected_berth_for_subscription_name = $selected_berth_for_subscription->get_description();
                        if(!in_array($selected_berth_for_subscription_name, $breadcrumb_array)) {
                            $breadcrumb_array[] = $selected_berth_for_subscription_name;
                        }
                    }
                }
                break;
        }
        // TODO: visualizzare nel breadcrumb anche le date selezionate?
    }
    if(count($breadcrumb_array) > 0) {
        $breadcrumb_classes = "bg-info text-light rounded p-2";
    } else {
        $breadcrumb_classes = "d-none";
    }
?>
<div class="mb-5">
    <span>
        {{ ucfirst(__('consumption_reports.you_are_requesting_a_report_for')).": " }}
    </span>
    <span class="{{ $breadcrumb_classes }}">
        @foreach($breadcrumb_array as $breadcrumb_key => $breadcrumb_item)
            {{ $breadcrumb_item }}{{ ($breadcrumb_key < (count($breadcrumb_array) - 1)) ? ' / ' : '' }}
        @endforeach
    </span>
</div>