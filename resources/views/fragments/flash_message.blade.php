<?php // mostra eventuali messaggi (ad esempio 'reset della presa richiesto'): ?>
@if(session('flash_message'))
    @switch(session('flash_background_color'))
        @case('red')
            <?php $alert_color_class = 'alert-danger'; ?>
            @break;
        @case('yellow')
            <?php $alert_color_class = 'alert-warning'; ?>
            @break;
        @case('green')
        @default
            <?php $alert_color_class = 'alert-success'; ?>
            @break;
    @endswitch
    <div class="alert {{ $alert_color_class }} alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('flash_message') }}</strong>
    </div>
@endif
