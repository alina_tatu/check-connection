<?php

// TODO: unificare questo template e quelli usati per l'intero marina e per i singoli pontili?

$login_logo_img_url = file_exists(public_path('resources/plant_dependent/img/login_logo.png')) ? public_path('resources/plant_dependent/img/login_logo.png') : public_path('resources/login_logo.default.png');
?>
<html>

<head>
    <title>{{ $page_title }}</title>
    <?php /* TODO: ma perchè mi appende ' - pdf' al $page_title? Se dumpo $page_title, non contiene la dicitura ' - pdf'. */ ?>
    <style>
        <?php /* il css su esterno su laravel-dompdf non funziona, lo metto in-file: */ ?>
        body {
            font-family: Helvetica;
        }

        div.page-break {
            page-break-after: always;
        }

        /** 
            Set the margins of the page to 0, so the footer and the header
            can be of the full height and width !
            **/
            @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            margin-top: 1.2cm;
            margin-left: 2cm;
            margin-right: 2cm;
            margin-bottom: 1.0cm;
        }

        /** Define the footer rules **/
        footer {
            position: fixed; 
            bottom: 0cm; 
            left: 16.2cm; 
            right: 0cm;
            height: 2cm;
            margin-bottom: 0.5cm;
        }

        footer img {
            display: block;
            max-width:125px;
            max-height: 75px;
            height: auto;
            width: auto;
            /* trasparenza: */ 
            opacity: 0.5;               /* Firefox, Chrome, Safari, Opera, IE >= 9 (preview) */
            filter: alpha(opacity=50);  /* for <= IE 8 */
        }
    </style>
</head>

<body>
    @if(config('marina.add_logo_on_pdf_consumption_reports'))
        <!-- Define (header and) footer blocks before your content -->
        <footer>
            <?php // TODO: verificare che - ora che stampo il logo - i report lunghi non abbiano le righe di intestazione sfalsate ad ogni nuova pagina 
            ?>
            <div style="height: 100%; text-align: center;">
                <img src="{{ $login_logo_img_url }}" />
            </div>
        </footer>
    @endif

    <!-- Wrap the content of your PDF inside a main tag -->
    <main>
        @if(($epower_id_has_changed_during_interval === true) && (Auth::user()->is_viewer_admin()))
        <h4>{{ ucfirst(__('common.epower_has_been_replaced_message')) }}</h4><br>
        <br>
        @endif
        <h4>{{ $report_title }}</h4>
        <b>{{ ucfirst(__('consumption_report_files.power_consumption_caption')) }}</b>: {{ round($power_consumption_in_interval, 1) }} kWh<br>
        <b>{{ ucfirst(__('consumption_report_files.water_consumption_caption')) }}</b>: {{ round($water_consumption_in_interval, 3) }} m<sup>3</sup>
    </main>
</body>

<html>