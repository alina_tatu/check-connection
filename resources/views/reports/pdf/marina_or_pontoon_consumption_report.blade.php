<?php 
    // TODO: unificare questo template e quello usato per i singoli berth/le singole subscription/i singoli pontili?
    $login_logo_img_url = file_exists(public_path('resources/plant_dependent/img/login_logo.png')) ? public_path('resources/plant_dependent/img/login_logo.png') : public_path('resources/login_logo.default.png');
    
    $available_chars_per_row = 89;
    $available_rows_in_first_page = 44; $available_rows_from_second_page = 43;
    $still_on_first_page = true;
    $rows_used_in_current_page = 0;
    $rows_used_per_entry = 1;               // righe usate per ogni entry della tabella (= per ogni posto barca)
?>
<html>
    <head>
        <title>{{ $page_title }}</title>
        <?php /* TODO: ma perchè mi appende ' - pdf' al $page_title? Se dumpo $page_title, non contiene la dicitura ' - pdf'. */ ?>
        <style>
            <?php /* il css su esterno su laravel-dompdf non funziona, lo metto in-file: */ ?>
            body {
                font-family: Helvetica;
            }

            div.page-break {
                page-break-after: always;
            }

            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
                **/
                @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 1.2cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 1.0cm;
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 16.2cm; 
                right: 0cm;
                height: 2cm;
                margin-bottom: 0.5cm;
            }

            footer img {
                display: block;
                max-width:125px;
                max-height: 75px;
                height: auto;
                width: auto;
                /* trasparenza: */ 
                opacity: 0.5;               /* Firefox, Chrome, Safari, Opera, IE >= 9 (preview) */
                filter: alpha(opacity=50);  /* for <= IE 8 */
            }
        </style>
        <link href='{{ URL::asset('css/reports/pdf/style.css') }}' rel='stylesheet'>
    </head>
    <body>
        @if(config('marina.add_logo_on_pdf_consumption_reports'))
            <!-- Define (header and) footer blocks before your content -->
            <footer>
                <?php // TODO: verificare che - ora che stampo il logo - i report lunghi non abbiano le righe di intestazione sfalsate ad ogni nuova pagina ?>
                <div style="height: 100%; text-align: center;">
                    <img src="{{ $login_logo_img_url }}" />
                </div>
            </footer>
        @endif

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
            @if($epower_id_has_changed_during_interval === true)
                <h4>{{ ucfirst(__('common.epower_has_been_replaced_message')) }}</h4><br>
                <br>
                <?php 
                    // se devo aggiungere il messaggio epower_has_been_replaced, sottraggo alle righe ancora disponibili sulla pagina la sua lunghezza in righe (+ una riga di <br>):
                    $rows_used_in_current_page += (strlen(__('common.epower_has_been_replaced_message')) / $available_chars_per_row) + 2; 
                    if(($rows_used_in_current_page % $available_chars_per_row) > 0) {
                        $rows_used_in_current_page++;
                    }
                ?>
            @endif
            <b>{{ $report_title }}</b><br>
            <?php
                // sottraggo alle righe ancora disponibili sulla pagina la lunghezza in righe del $report_title (+ una riga di <br>):
                $rows_used_in_current_page += (strlen($report_title) / $available_chars_per_row) + 1; 
                if(($rows_used_in_current_page % $available_chars_per_row) > 0) {
                    $rows_used_in_current_page++;
                }
            ?>
            <br>
            <table>
                <tr>
                    <td style='text-align: center;'>
                        <b>{{ ucfirst(__('consumption_report_files.berth_caption')) }}</b>
                    </td>
                    <td style='text-align: center;'>
                        <b>{{ ucfirst(__('consumption_report_files.power_consumption_caption')) }} (kWh)</b>
                    </td>
                    <td style='text-align: center;'>
                        <b>{{ ucfirst(__('consumption_report_files.water_consumption_caption')) }} (m<sup>3</sup>)</b>
                    </td>
                </tr>
                @foreach($berth_consumption_details_records as $detail_record)
                    <?php
                        if($still_on_first_page) {
                            $available_rows_in_current_page = $available_rows_in_first_page;
                        } else {
                            $available_rows_in_current_page = $available_rows_from_second_page;
                        }
                    ?>
                    <tr>
                        <td style='text-align: center;'>
                            [{{ $detail_record->berth_name }}]
                        </td>
                        <td style='text-align: center;'>
                            {{ round($detail_record->power_consumption, 1) }}
                        </td>
                        <td style='text-align: center;'>
                            {{ round($detail_record->water_consumption, 3) }}
                        </td>
                    </tr>
                    <?php $rows_used_in_current_page += $rows_used_per_entry; ?>
                    @if(($rows_used_in_current_page + $rows_used_per_entry) > $available_rows_in_current_page)
                        <tr>
                            <td style='text-align: center;'>
                                <b>{{ ucfirst(__('consumption_report_files.berth_caption')) }}</b>
                            </td>
                            <td style='text-align: center;'>
                                <b>{{ ucfirst(__('consumption_report_files.power_consumption_caption')) }} (kWh)</b>
                            </td>
                            <td style='text-align: center;'>
                                <b>{{ ucfirst(__('consumption_report_files.water_consumption_caption')) }} (m<sup>3</sup>)</b>
                            </td>
                        </tr>
                        <?php 
                            $rows_used_in_current_page = 1;
                            $still_on_first_page = false;
                        ?>
                    @endif
                @endforeach
            </table>
            <br>
            <table>
                <tr>
                    <td></td>
                    <td style='text-align: center;'>
                        <b>{{ ucfirst(__('consumption_report_files.total_power_consumption_caption')) }} (kWh)</b>
                    </td>
                    <td style='text-align: center;'>
                        <b>{{ ucfirst(__('consumption_report_files.total_water_consumption_caption')) }} (m<sup>3</sup>)</b>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style='text-align: center;'>
                        {{ round($power_consumption_in_interval, 1) }}
                    </td>
                    <td style='text-align: center;'>
                        {{ round($water_consumption_in_interval, 3) }}
                    </td>
                </tr>
            </table>
        </main>
    </body>
<html>
