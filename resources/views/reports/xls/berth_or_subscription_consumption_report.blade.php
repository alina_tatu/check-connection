<table>
    <?php /* TODO: unificare questo template e quelli usati per l'intero marina e per i singoli pontili? */ ?>
    <tr>
        <td>
            {{ $report_title_part_before_subject_description }}
        </td>
        <td>
            {{ $subject_description }}
        </td>
        <td>
            {{ strtolower(__('consumption_report_files.xls_title_from_part')) }}
        </td>
        <td>
            {{ $report_interval_begin_timestamp->format(config('dates_currency_and_locale.local_date_format_full_carbon')) }}
        </td>
        <td>
            {{ $report_interval_begin_timestamp->format(config('dates_currency_and_locale.local_time_format_full_carbon')) }}
        </td>
        <td>
            {{ strtolower(__('consumption_report_files.xls_title_to_part')) }}
        </td>
        <td>
            {{ $report_interval_end_timestamp->format(config('dates_currency_and_locale.local_date_format_full_carbon')) }}
        </td>
        <td>
            {{ $report_interval_end_timestamp->format(config('dates_currency_and_locale.local_time_format_full_carbon')) }}
        </td>
    </tr>
    @if(($epower_id_has_changed_during_interval === true) && (Auth::user()->is_viewer_admin()))
        <tr>
            <td>
                {{ ucfirst(__('common.epower_has_been_replaced_message')) }}
            </td>
        </tr>
    @endif
    <tr>
        <td>
            {{ ucfirst(__('consumption_report_files.power_consumption_caption')) }}
        </td>
        <td>
            {{ round($berth_power_consumption_in_interval, 1) }}
            <?php // TODO: il number_format() non funziona, vedere come fare sulla doc di laravel-excel (https://docs.laravel-excel.com/). EDIT: credo non abbia senso usarlo, lasciar perdere? ?>
        </td>
        <td>
            kWh
        </td>
    </tr>
    <tr>
        <td>
            {{ ucfirst(__('consumption_report_files.water_consumption_caption')) }}
        </td>
        <td>
            {{ round($berth_water_consumption_in_interval, 3) }}
            <?php // TODO: il number_format() non funziona, vedere come fare sulla doc di laravel-excel (https://docs.laravel-excel.com/). EDIT: credo non abbia senso usarlo, lasciar perdere? ?>
        </td>
        <td>
            m<sup>3</sup>
        </td>
    </tr>
</table>
