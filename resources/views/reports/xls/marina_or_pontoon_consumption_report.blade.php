<table>
    <?php /* TODO: unificare questo template e quello usato per i singoli berth/le singole subscription/i singoli pontili? */ ?>
    <tr>
        <td>
            {{ $report_title_part_before_interval }}
        </td>
        @if(isset($pontoon_name))
            <td>
                [{{ $pontoon_name }}]
            </td>
        @endif
        <td>
            {{ strtolower(__('consumption_report_files.xls_title_from_part')) }}
        </td>
        <td>
            {{ $report_interval_begin_timestamp->format(config('dates_currency_and_locale.local_date_format_full_carbon')) }}
        </td>
        <td>
            {{ $report_interval_begin_timestamp->format(config('dates_currency_and_locale.local_time_format_full_carbon')) }}
        </td>
        <td>
            {{ strtolower(__('consumption_report_files.xls_title_to_part')) }}
        </td>
        <td>
            {{ $report_interval_end_timestamp->format(config('dates_currency_and_locale.local_date_format_full_carbon')) }}
        </td>
        <td>
            {{ $report_interval_end_timestamp->format(config('dates_currency_and_locale.local_time_format_full_carbon')) }}
        </td>
    </tr>
    @if($epower_id_has_changed_during_interval === true)
        <tr>
            <td>
                {{ ucfirst(__('common.epower_has_been_replaced_message')) }}
            </td>
        </tr>
    @endif
    <tr></tr>
    <tr>
        <td>
            {{ ucfirst(__('consumption_report_files.berth_caption')) }}
        </td>
        <td>
            {{ ucfirst(__('consumption_report_files.power_consumption_caption')) }} (kWh)
        </td>
        <td>
            {{ ucfirst(__('consumption_report_files.water_consumption_caption')) }} (m<sup>3</sup>)
        </td>
    </tr>
    @foreach($berth_consumption_details_array as $detail_record)
        <tr>
            <td>
                [{{ $detail_record->berth_name }}]
            </td>
            <td>
                {{ round($detail_record->power_consumption, 1) }}
            </td>
            <td>
                {{ round($detail_record->water_consumption, 3) }}
            </td>
        </tr>
    @endforeach
    <tr></tr>
    <tr>
        <td>
            {{ ucfirst(__('consumption_report_files.total_consumption_caption')) }}
        </td>
        <td>
            {{ round($total_plant_power_consumption_in_interval, 1) }}
            <?php // TODO: il number_format() non funziona, vedere come fare sulla doc di laravel-excel (https://docs.laravel-excel.com/). EDIT: credo non abbia senso usarlo, lasciar perdere? ?>
        </td>
        <td>
            {{ round($total_plant_water_consumption_in_interval, 3) }}
            <?php // TODO: il number_format() non funziona, vedere come fare sulla doc di laravel-excel (https://docs.laravel-excel.com/). EDIT: credo non abbia senso usarlo, lasciar perdere? ?>
        </td>
    </tr>
</table>
