<?php 
    use App\Http\Controllers\PagesController;
    use App\Http\Controllers\ConsumptionReportsController;
?>
@extends('layouts.consumption_reports')

@section('form')
    <form method='post'
          action="{{ route('handle_consumption_reports_form_submit') }}"
          onsubmit="validate_data(event);">
        @csrf
        <input type="hidden" name="current_step" value="{{ ConsumptionReportsController::STEP_SELECT_BERTH_FOR_SUBSCRIPTION }}" />
        <input type="hidden" name="report_type_select" value="{{ request('report_type_select') }}" />
        @if(isset($parameters))
            @foreach($parameters as $current_parameter_key => $current_parameter_value)
                <?php // TODO: forse ha senso outputtarli selettivamente, per non permettere all'utente di aggiungere manualmente parametri random ?>
                <input type="hidden" name="{{ $current_parameter_key }}" value="{{ $current_parameter_value }}"/>
            @endforeach
        @endif
        <?php // TODO: settare un attributo value nei campi visualizzati se c'è una corrispondenza in request o in parameters, per gestire la possibilità di tornare indietro nel form? ?>
        <div class='form-row mt-3'>
            @if(count($berths_on_which_subscription_consumed) > 0)
                <div class='col-lg-3'>
                    <label for='subscription_berth_select'>
                        {{ ucfirst(__('consumption_reports.berth_field_label_caption')) }}
                    </label>
                </div>
                <div class='col'>
                    <select id='subscription_berth_select' name='subscription_berth_select' class='custom-select'>
                        <option id='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                                name='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                                value='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'>
                            {{ strtolower(__('consumption_reports.please_select_an_option_caption')) }}
                        </option>
                        @foreach($berths_on_which_subscription_consumed as $current_berth)
                            <option id='{{ $current_berth->get_id() }}'
                                    name='{{ $current_berth->get_id() }}' 
                                    value='{{ $current_berth->get_id() }}'>
                                {{ $current_berth->get_description() }}
                            </option>
                        @endforeach
                    </select>
                </div>
            @else 
                <?php 
                    $selected_subscription_has_not_made_any_consumption_yet = true; 
                ?>
                <div class='form-row mt-3'>
                    <div class='text-danger ml-5'>
                        {{ ucfirst(__('consumption_reports.subscription_berth_select_subscription_has_not_consumed_on_any_berth_option_caption')) }}
                    </div>
                </div>
            @endif
        </div>
        <div class='mt-3 float-right'>
            @if(!isset($selected_subscription_has_not_made_any_consumption_yet) || ($selected_subscription_has_not_made_any_consumption_yet === false))
                <button class='btn btn-primary' type='submit' name='submit_btt'>
                    {{ ucfirst(__('consumption_reports.button_caption_next_step')) }}
                </button>
            @else
                <a class='btn btn-secondary' href='{{ route("open_consumption_reports_page") }}'>
                    {{ ucfirst(__('consumption_reports.button_caption_request_new_report')) }}
                </a>
            @endif
        <div>
    </form>
@endsection