<?php 
    use App\Http\Controllers\PagesController;
    use App\Http\Controllers\ConsumptionReportsController;
?>
@extends('layouts.consumption_reports')

@section('form')
    <form method='post'
          action="{{ route('handle_consumption_reports_form_submit') }}"
          onsubmit="validate_data(event);">
        @csrf
        <input type="hidden" name="current_step" value="{{ ConsumptionReportsController::STEP_SELECT_SUBSCRIPTION }}" />
        <input type="hidden" name="report_type_select" value="{{ request('report_type_select') }}" />
        @if(isset($parameters))
            @foreach($parameters as $current_parameter_key => $current_parameter_value)
                <?php // TODO: forse ha senso outputtarli selettivamente, per non permettere all'utente di aggiungere manualmente parametri random ?>
                <input type="hidden" name="{{ $current_parameter_key }}" value="{{ $current_parameter_value }}"/>
            @endforeach
        @endif
        <?php // TODO: settare un attributo value nei campi visualizzati se c'è una corrispondenza in request o in parameters, per gestire la possibilità di tornare indietro nel form? ?>
        <div class='form-row mt-3'>
            <div class='col-lg-3'>
                <label for='subscription_select'>
                    {{ ucfirst(__('consumption_reports.subscription_field_label_caption')) }}
                </label>
            </div>
            <div class='col'>
                <select id='subscription_select' name='subscription_select' class='custom-select'>
                    <option id='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                            name='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                            value='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'>
                        {{ strtolower(__('consumption_reports.please_select_an_option_caption')) }}
                    </option>
                    @foreach($subscriptions as $current_subscription)
                        <?php
                            $subscription_user = $current_subscription->user;
                            if(($subscription_user) && ($subscription_user->count() > 0)) {
                                $current_subscription_full_caption = $subscription_user->get_full_name();
                            } else {
                                $current_subscription_full_caption = ucfirst(__('common.unknown_user'));
                            }
                            // i timestamp sulla view vengono già outputtati con la timezone corretta (non mi è ben chiaro come, ma sarò merito del trait TimeZoneAware):
                            $subscription_begin_timestamp = $current_subscription->get_begin_timestamp()
                                                                                 ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'));
                            $current_subscription_full_caption .= ': '.strtolower(__('consumption_reports.subscription_started_on_caption')).' '.$subscription_begin_timestamp;
                            if(!is_null($current_subscription->get_end_timestamp())) {
                                $subscription_end_timestamp = $current_subscription->get_end_timestamp()
                                                                                   ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'));
                                $current_subscription_full_caption .= ', '.strtolower(__('consumption_reports.subscription_ended_on_caption')).' '.$subscription_end_timestamp;
                            }
                            if($current_subscription->is_bound_to_socket()) {
                                if(($current_subscription->berth) && ($current_subscription->berth->count() > 0)) {
                                    $current_subscription_full_caption .= ' ('.strtolower(__('consumption_reports.subscription_bound_berth_caption')).': '.$current_subscription->berth->get_description().')';
                                }
                            } else {
                                if(($current_subscription->transponder) && ($current_subscription->transponder->count() > 0)) {
                                    $current_subscription_full_caption .= ' ('.strtolower(__('consumption_reports.subscription_bound_transponder_caption')).': '.$current_subscription->transponder->get_code().')';
                                }
                            }
                        ?>
                        <option value="{{ $current_subscription->get_id() }}">{{ $current_subscription_full_caption }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class='mt-3 float-right'>
            <?php // TODO: devo aggiungere anche il btt di annulla/previous? ?> 
            <button class='btn btn-primary' type='submit' name='submit_btt'>
                {{ ucfirst(__('consumption_reports.button_caption_next_step')) }}
            </button>
        <div>
    </form>
@endsection