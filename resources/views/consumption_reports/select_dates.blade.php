<?php
    use App\Http\Controllers\ConsumptionReportsController;
?>
@extends('layouts.consumption_reports')

@section('form')
    <form method='post'
          action="{{ route('handle_consumption_reports_form_submit') }}"
          onsubmit="validate_data(event);">
        @csrf
        <input type="hidden" name="current_step" value="{{ ConsumptionReportsController::STEP_SELECT_REPORT_BEGIN_AND_END_TIMESTAMPS }}" />
        <input type="hidden" name="report_type_select" value="{{ request('report_type_select') }}" />
        @if(isset($parameters))
            @foreach($parameters as $current_parameter_key => $current_parameter_value)
                <?php // TODO: forse ha senso outputtarli selettivamente, per non permettere all'utente di aggiungere manualmente parametri random ?>
                <input type="hidden" name="{{ $current_parameter_key }}" value="{{ $current_parameter_value }}"/>
            @endforeach
        @endif
        <?php // TODO: settare un attributo value nei campi visualizzati se c'è una corrispondenza in request o in parameters, per gestire la possibilità di tornare indietro nel form? ?>
        <div class='form-row mt-3'>
            <div class='col-lg-3'>
                <label for='begin_date'>
                    {{ ucfirst(__('consumption_reports.begin_timestamp_field_label_caption')) }}
                </label>
            </div>
            <div class='col'>
                <input type='date'
                       class='form-control'
                       id='report_begin_date'
                       name='report_begin_date'
                       value='{{ $default_report_begin_timestamp->format('Y-m-d') }}'
                       min='{{ $min_report_timestamp->format('Y-m-d') }}'
                       max='{{ $max_report_timestamp->format('Y-m-d') }}'
                       required />
                <input type='time' 
                       class='form-control'
                       id='report_begin_time' 
                       name='report_begin_time'
                       value='{{ $default_report_begin_timestamp->format('H:i:s') }}'
                       step='1' />
            </div>
        </div>
        <div class='form-row mt-3'>
            <div class='col-lg-3'>
                <label for='end_date'>
                    {{ ucfirst(__('consumption_reports.end_timestamp_field_label_caption')) }}
                </label>
            </div>
            <div class='col'>
                <input type='date'
                       class='form-control'
                       id='report_end_date'
                       name='report_end_date'
                       value='{{ $default_report_end_timestamp->format('Y-m-d') }}'
                       min='{{ $min_report_timestamp->format('Y-m-d') }}'
                       max='{{ $max_report_timestamp->format('Y-m-d') }}'
                       required />
                <input type='time' 
                       class='form-control'
                       id='report_end_time' 
                       name='report_end_time'  
                       value='{{ $default_report_end_timestamp->format('H:i:s') }}'
                       step='1' />
            </div>
        </div>
        <div class='mt-3 float-right'>
            <?php // TODO: devo aggiungere anche il btt di annulla/previous? ?> 
            <button class='btn btn-primary' type='submit' name='submit_btt'>
                {{ ucfirst(__('consumption_reports.button_caption_next_step')) }}
            </button>
        <div>
    </form>
@endsection