<?php 
    use App\Http\Controllers\PagesController;
    use App\Http\Controllers\ConsumptionReportsController;
?>
@extends('layouts.consumption_reports')

@section('form')
    <form method='post'
          action="{{ route('handle_consumption_reports_form_submit') }}"
          onsubmit="validate_data(event);">
        @csrf
        <input type="hidden" name="current_step" value="{{ ConsumptionReportsController::STEP_SELECT_REPORT_SUBJECT_TYPE }}" />
        <div class='form-row mt-3'>
            <?php // TODO: settare un attributo value nei campi visualizzati, per gestire la possibilità di tornare indietro nel form? ?>
            <div class='col-lg-3'>
                <label for='report_type_select'>
                    {{ ucfirst(__('consumption_reports.type_select_label_caption')) }}
                </label>
            </div>
            <div class='col'>
                <select id='report_type_select' name='report_type_select' class='custom-select'>
                    <option id='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                            name='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                            value='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'>
                        {{ strtolower(__('consumption_reports.please_select_an_option_caption')) }}
                    </option>
                    @foreach($available_subject_types as $current_type_key => $current_type_value)
                        <option value="{{ $current_type_key }}">
                            {{ $current_type_value }}
                        </option>
                    @endforeach
                </select>
                <?php // visualizzo eventuali errori di validazione riscontrati durante il processing della richiesta precedente: ?>
                @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class='mt-3 float-right'>
            <button class='btn btn-primary' type='submit' name='submit_btt'>
                {{ ucfirst(__('consumption_reports.button_caption_next_step')) }}
            </button>
        <div>
    </form>
@endsection