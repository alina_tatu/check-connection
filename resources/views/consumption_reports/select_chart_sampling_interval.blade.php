<?php 
    use App\Http\Controllers\PagesController;
    use App\Http\Controllers\ConsumptionReportsController;
?>
@extends('layouts.consumption_reports')

@section('form')
    <form method='post'
          action="{{ route('handle_consumption_reports_form_submit') }}"
          onsubmit="validate_data(event);">
        @csrf
        <input type="hidden" name="current_step" value="{{ ConsumptionReportsController::STEP_SELECT_CHART_SAMPLING_INTERVAL }}" />
        <input type="hidden" name="report_type_select" value="{{ request('report_type_select') }}" />
        @if(isset($parameters))
            @foreach($parameters as $current_parameter_key => $current_parameter_value)
                <?php // TODO: forse ha senso outputtarli selettivamente, per non permettere all'utente di aggiungere manualmente parametri random ?>
                <input type="hidden" name="{{ $current_parameter_key }}" value="{{ $current_parameter_value }}"/>
            @endforeach
        @endif
        <?php // TODO: settare un attributo value nei campi visualizzati se c'è una corrispondenza in request o in parameters, per gestire la possibilità di tornare indietro nel form? ?>
        <div class='form-row mt-3'>
            <div class='col-lg-3'>
                <label for='chart_sampling_interval_select'>
                    {{ ucfirst(__('consumption_reports.chart_sampling_interval_field_label_caption')) }}
                </label>
            </div>
            <div class='col'>
                <select id='chart_sampling_interval_select' name='chart_sampling_interval_select' class='custom-select'>
                    <option id='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                            name='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'
                            value='{{ PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}'>
                        {{ strtolower(__('consumption_reports.please_select_an_option_caption')) }}
                    </option>
                    @foreach($available_chart_sampling_intervals_with_translations as $current_interval_value => $current_interval_translation)
                        <option value="{{ $current_interval_value }}">{{ $current_interval_translation }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class='mt-3 float-right'>
            <?php // TODO: devo aggiungere anche il btt di annulla/previous? ?> 
            <button class='btn btn-primary' type='submit' name='submit_btt'>
                {{ ucfirst(__('consumption_reports.button_caption_next_step')) }}
            </button>
        <div>
    </form>
@endsection