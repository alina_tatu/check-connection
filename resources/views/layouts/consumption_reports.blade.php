@extends('layouts.app_menu')

@section('custom_css')
    <link href="{{ URL::asset('css/consumption_reports.css') }}" rel="stylesheet">      <?php // TODO: aggiungere il file css, se serve. Altrimenti togliere questo tag link ?>
@endsection

@section('custom_js')
    <script>
        function validate_data(event) {
            let form = document.getElementsByTagName('form')[0];
            let select_elements = document.getElementsByTagName('select');if(select_elements.length > 0) {
                let select_element = select_elements[0];
                let selected_option = select_element.options[select_element.selectedIndex];
                if(selected_option.value === "{{ App\Http\Controllers\PagesController::SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE }}") {
                    event.preventDefault();
                    return false;       // TODO: serve?
                    // TODO: mostrare un messaggio? probabilmente non ha senso, c'è già scritto 'please select an option'
                }
            }
        }
    </script>
@endsection

@section('content')
    <?php // TODO: forse posso raggruppare qui dentro più roba comune alle varie views, definendo poi delle sezioni nelle quali outputtare le varie parti del form ?>
    <div id='content' class='mt-5 pt-5'>
        @include('fragments.flash_message')
        <div class='container mt-3 pt-3'>
            @include('fragments.consumption_reports_breadcrumb')
            @yield('form')
        </div>
    </div>
@endsection