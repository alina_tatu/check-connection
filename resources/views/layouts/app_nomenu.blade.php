<!doctype html>
<html lang='{{ str_replace('_', '-', app()->getLocale()) }}'>
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no'>

    <!-- CSRF Token -->
    <meta name='csrf-token' content='{{ csrf_token() }}'>

    <title>{{ config('marina.name') }} - {{ config('app.name') }} v.{{ config('app.version') }}</title>

    <!-- Fonts -->
    <link rel='dns-prefetch' href='//fonts.gstatic.com'>
    <link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'>

    <!-- Styles -->
    <link href='{{ asset('css/app.css') }}' rel='stylesheet'>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossorigin='anonymous'><?php // teoricamente non dovrei importare questo dato che laravel si porta già dietro bootstrap, ma se non lo importo, noto differenze (ad esempio il font della navbar è diverso) ?>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css'>
    <link href='{{ URL::asset('css/common.css') }}' rel='stylesheet'>
    @yield('custom_css')
</head>
    <body class='mb-4 mb-md-0'>
        <div id='app'>
        </div>
        @yield('content')

        <script src='{{ asset('js/app.js') }}'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ URL::asset('js/common.js') }}"></script>
        @yield('custom_js')
    </body>
</html>
