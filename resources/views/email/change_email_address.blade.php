@component('mail::message')
# {{ config('marina.name') }}

{{ ucfirst(__('auth.email_change_verification_msg_header_caption')) }}

{{ ucfirst(__('auth.email_change_verification_msg_caption')) }}

@component('mail::button', ['url' => $url])
{{ ucfirst(__('auth.email_change_verification_btn_caption')) }}
@endcomponent

{{ ucfirst(__('auth.email_change_verification_msg_thanks_msg_caption')) }},<br>
{{ config('app.name') }}
@endcomponent