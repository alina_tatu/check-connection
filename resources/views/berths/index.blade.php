<?php
    use App\Http\Controllers\PagesController;
    use App\Berth;
?>
@extends('layouts.app_menu')

@section('custom_css')
    <link href="{{ URL::asset('css/berths.css') }}" rel='stylesheet'>
@endsection

@section('custom_js')
<script type="text/javascript" src="{{ URL::asset('js/berths_index.js') }}"></script>
<script>
    window.onload = function() {
    // appena la pagina viene caricata:
        if(!window.matchMedia || (window.matchMedia("(min-width: 768px)").matches)) {
        // a meno che non si stia visualizzando il sito da mobile:
            // abilito i tooltip:
            $('[data-toggle="tooltip"]').tooltip();
        }
        // pulisco i campi dei filtri dal contenuto rimasto prima dell'eventuale refresh della pagina, poi li applico:
        reset_filters();
        apply_filters();
    };
</script>
@endsection

@section('content')
    <div id='content' class='mt-5 pt-5'>
        @include('fragments.flash_message')
        <form id='filters_bar' class='form-horizontal px-2 py-1'>
        @csrf
            <?php // TODO: mancano i pulsanti per l'ordinamento, devo metterli? ?>
            <div class='card bg-light border-primary rounded mb-3'>
                <div class='card-body'> <?php // TODO: calare il padding (tra border e filtri) + aumentare il margin (tra border e fine pagina)?>
                    <div class='card-title'>
                        {{ ucfirst(__('common.filters_bar_title')) }}
                    </div>
                    <div class='p-3 m-3 form-row text-center'>
                        <?php // TODO: come faccio a cambiare il colore del margine azzurro degli input quando hanno il focus? ?>
                        <div class='form-group col-md-6'>
                            <input type='text' id='fulltext_filter_input' class='filter-input form-control' onkeyup='apply_filters();'
                                    placeholder='{{ ucfirst(__('berths/index.fulltext_filter_caption')) }}'>
                        </div>
                        <div class='form-group col-md-5'>
                            <select id='combo_filter_input' class='filter-input custom-select' onchange='apply_filters();'>
                                <?php
                                    // TODO: occhio che se cambio i valori degli attributi id/value delle options, che attualmente sono definiti nel PagesController, i cambiamenti non si riflettono sul js che gestisce il filtro. In caso di necessità, ricordare che là devo modificarli a mano.
                                    // TODO: definire la option di default
                                ?>
                                <option id='{{ PagesController::BERTHS_COMBO_FILTER_AVAILABLE_BERTHS_ONLY }}'
                                        value='{{ PagesController::BERTHS_COMBO_FILTER_AVAILABLE_BERTHS_ONLY }}'>
                                    {{ ucfirst(__('berths/index.combo_filter_available_berths_only_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::BERTHS_COMBO_FILTER_ASSIGNED_BERTHS_ONLY }}'
                                        value='{{ PagesController::BERTHS_COMBO_FILTER_ASSIGNED_BERTHS_ONLY }}'>
                                    {{ ucfirst(__('berths/index.combo_filter_assigned_berths_only_option_caption')) }}
                                </option>
                                <option id='{{ PagesController::BERTHS_COMBO_FILTER_ALL_BERTHS }}'
                                        value='{{ PagesController::BERTHS_COMBO_FILTER_ALL_BERTHS }}'>
                                    {{ ucfirst(__('berths/index.combo_filter_all_berths_option_caption')) }}
                                </option>
                                <?php // TODO: non metto le opzioni "ended_subscriptions" e "all_history", direi di farle vedere solo dalla views subscriptions ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @if(($berths) && ($berths->count() > 0))
            <div class='container table-responsive'>
                <table class='table table-striped table-hover'>
                    <?php
                        // TODO: usare le responsive data tables come facevo sulla prima bozza di smartcloud 2? (https://css-tricks.com/responsive-data-tables/) (come le integro con Bootstrap?):
                    ?>
                    <thead>
                        <tr>
                            <th class='text-center'>{{ ucfirst(__('berths/index.th_berth_name')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('berths/index.th_socket_type')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('berths/index.th_user')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('berths/index.th_boat')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('berths/index.th_begin_date')) }}</th>
                            <th class='text-center'>{{ ucfirst(__('berths/index.th_end_date')) }}</th>
                            <th class='text-center'></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($berths as $berth)
                            <?php
                                if($berth->get_socket_type() !== Berth::SOCKET_TYPE_NOT_DEFINED) {
                                    $socket_type_to_display = $berth->get_socket_type();
                                    $socket_type_caption_class = '';
                                } else {
                                    $socket_type_to_display = strtoupper(__('common.socket_type_not_set'));
                                    $socket_type_caption_class = 'font-weight-bold socket_type_not_defined';
                                }
                                if(($berth->currently_bound_subscription) && ($berth->currently_bound_subscription->count() > 0)) {
                                    $boat_name = '-';
                                    $boat_length = $boat_width = $boat_depth = null;
                                    $boat_size_tooltip = '';
                                    $subscription = $berth->currently_bound_subscription;
                                    if(($subscription) && ($subscription->count() > 0)) {
                                        $user = $subscription->user;
                                        if(($user) && ($user->count() > 0)) {
                                            if(($user->boat) && ($user->boat()->count() > 0)) {
                                                $boat = $user->boat;
                                                $boat_name = $boat->get_name();
                                                $boat_size_tooltip = strtolower(__('common.boat_measurements_tooltip_caption', ['length' =>  $boat->get_length(),
                                                                                                                                'width' =>  $boat->get_width(),
                                                                                                                                'depth' =>  $boat->get_depth()]));
                                            }
                                        }
                                    }
                                    $subscription_begin_date = $subscription->get_begin_timestamp()->setTimezone(config('dates_currency_and_locale.local_timezone'));
                                    $subscription_begin_date_to_display = $subscription_begin_date->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'));
                                    // TODO: ma la subscription_end_date mi serve su questa view?
                                    $subscription_end_date = $subscription->get_end_timestamp();
                                    if(!is_null($subscription_end_date)) {
                                        $subscription_end_date_to_display = $subscription_end_date->setTimezone(config('dates_currency_and_locale.local_timezone'))->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'));
                                    } else {
                                        $subscription_end_date_to_display = '-';
                                    }
                                } else {
                                // TODO: nello user mettere <currently unassigned>. La barca di conseguenza non va visualizzata:
                                    $user_boat = null;
                                }
                            ?>
                            <tr class='berth_row'>
                                <td class='text-center'>
                                    <span class='text-light bg-info p-2 text-nowrap rounded berth_name_outer'>             <?php // TODO: cambiare la formattazione, dato che l'ho cambiata anche sulle views plant_status e my_subscriptions? ?>
                                        <i class='fas fa-anchor'></i>
                                        <span class='berth_name_inner'>
                                            {{ $berth->get_description() }}
                                        </span>
                                    </span>
                                </td>
                                <td class='text-center socket_type {{ $socket_type_caption_class }}'>
                                    {{ $socket_type_to_display }}
                                </td>
                                @if(($berth->currently_bound_subscription) && ($berth->currently_bound_subscription->count() > 0))
                                    <td class='text-center user_full_name'>
                                        @if(!is_null($berth->currently_bound_subscription->user) && ($berth->currently_bound_subscription->user()->count() > 0))
                                            {{ $berth->currently_bound_subscription->user->get_full_name() }}
                                            @if(!is_null($berth->currently_bound_subscription->user->get_username()))
                                                <br>
                                                <span class='text-light bg-primary p-1 rounded user_username'>
                                                    {{ $berth->currently_bound_subscription->user->get_username() }}
                                                </span>
                                            @endif
                                        @else
                                            <span class='text-danger'>
                                                {{ strtoupper(__('common.unknown_user')) }}
                                            </span>
                                        @endif
                                    </td>
                                    <td @if(($berth->currently_bound_subscription->user->boat) && ($berth->currently_bound_subscription->user->boat()->count() > 0))
                                            title='{{ $boat_size_tooltip }}'
                                        @endif
                                        class='text-center user_boat_name' data-toggle='tooltip' data-placement='top'>
                                        {{ $boat_name }}
                                    </td>
                                    <td class='text-center subscription_begin_timestamp'>
                                        {{ $subscription_begin_date_to_display }}
                                    </td>
                                    <td class='text-center subscription_end_timestamp'>
                                        {{ $subscription_end_date_to_display }}
                                    </td>
                                @else
                                    <td class='text-center user_full_name'>
                                        <span class='text-success font-weight-bold'>
                                            {{ strtoupper(__('berths/index.currently_unassigned_berth_caption')) }}
                                        </span>
                                    </td>
                                    <td class='text-center user_boat_name'>-</td>
                                    <td class='text-center subscription_begin_timestamp'>-</td>
                                    <td class='text-center subscription_end_timestamp'>-</td>
                                @endif
                                <td class='text-center'>
                                    @if(($berth->currently_bound_subscription) && ($berth->currently_bound_subscription->count() > 0))
                                    <?php // se c'è una subscription in corso, mostro il pulsante che serve a vederne i dettagli: ?>
                                        <form class='d-inline' method='post' action='{{ route('subscription.check_consumption', $berth->currently_bound_subscription->get_id()) }}'>
                                        @csrf
                                            <button type='submit' class='btn btn-primary action_button'>
                                                {{ strtoupper(__('berths/index.check_subscription_consumption_btt_caption')) }}
                                            </button>
                                        </form>
                                    @else
                                    <?php // se NON c'è una subscription in corso: ?>
                                        @if(Auth::user()->is_full_admin())
                                        <?php // e se l'utente loggato è un full_admin: ?>
                                            <?php // se lo è, mostro il pulsante che serve ad assegnare il posto barca ad un utente: ?>
                                            <form class='d-inline' method='post' action="{{ route('new_berth_subscription', ['berth_id' => $berth->get_id()]) }}">
                                            @csrf
                                                <button type='submit' class='btn btn-success action_button'>
                                                    {{ strtoupper(__('berths/index.start_new_subscription_btt_caption')) }}
                                                </button>
                                            </form>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    <div id='no_entries_message' class='container-fluid mt-5 pt-5 text-center d-none'>
        <h6 class='text-secondary text-uppercase font-weight-bold'>
            {{ ucfirst(__('berths/index.no_berths_on_database_or_no_berth_matches_search_criteria')) }}
        </h6>
    </div>
@endsection
