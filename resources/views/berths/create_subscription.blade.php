<?php
    use Illuminate\Support\Carbon;
    use App\Berth;
    use App\Transponder;
?>
@extends('layouts.app_menu')

@section('custom_css')
    <link href='{{ URL::asset('css/create_subscription.css') }}' rel='stylesheet'>
@endsection

@section('custom_js')
    <script>
        var users = @json($users);
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/create_subscription.js') }}"></script>
@endsection

@section('content')
    <?php // TODO: la validazione dati lato client e lato server è tutta da fare. ?>
    <?php
        $form_submit_confirmation_request_msg = ucfirst(__('berths/create_subscription.submit_confirmation_request'));
        $user_not_found_alert_msg = ucfirst(__('berths/create_subscription.user_not_found_alert_msg_caption'));
    ?>
    <div id='content' class='container mt-5 pt-5'>
        <form>
        @csrf
            <div class='form-group row'>
                <label for='berth_id' class='col-sm-10 col-lg-2 col-form-label'>
                    {{ ucfirst(__('berths/create_subscription.berth_name_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='hidden' id='berth_id' name='berth_id' value='{{ $berth->get_id() }}'/>
                    <?php // TODO: usare testo normale anzichè un elemento input readonly? Però viene sfasato rispetto alla label. Altrimenti potrei usare un elemento span o div con class='form-control', ma avrebbe comunque l'aspetto di un input modificabile ?>
                    <input id='berth_name' name='berth_name' class='form-control @error("berth_found_on_db_id") is-invalid @enderror' type='text'
                           value='{{ $berth->get_description() }}' readonly>
                    @error('berth_found_on_db_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            @if(($berth_socket_type = $berth->get_socket_type()) !== Berth::SOCKET_TYPE_NOT_DEFINED)
                <div class='form-group row'>
                    <label for='socket_type' class='col-sm-10 col-lg-2 col-form-label'>
                        {{ ucfirst(__('berths/create_subscription.socket_type_field_label_caption')) }}
                    </label>
                    <div class='col-sm-10'>
                        <?php // TODO: usare testo normale anzichè un elemento input readonly? Però viene sfasato rispetto alla label. Altrimenti potrei usare un elemento span o div con class='form-control', ma avrebbe comunque l'aspetto di un input modificabile ?>
                        <input type='text' class='form-control' placeholder='{{ $berth_socket_type }} ' readonly>
                    </div>
                </div>
            @endif
            <div class='form-group row'>
                <label for='user' class='col-sm-10 col-lg-2 col-form-label'>
                    {{ ucfirst(__('berths/create_subscription.user_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input id='user' name='user_full_name' list='users' class='form-control custom-select
                           @error("user_full_name") is-invalid @enderror' autocomplete='on'
                           placeholder='{{ ucfirst(__('berths/create_subscription.search_for_users_caption')) }}'>
                    <datalist id='users'>
                    <?php // TODO: attenzione, pare che l'elemento datalist permetta il submit anche se il valore selezionato è diverso dalle opzioni disponibili. Devo gestire la validazione esplicitamente, sia lato client sia lato server ?>
                    <?php // TODO: forse posso sostituire il datalist con questo https://github.com/farzher/fuzzysort, oppure provare jquery chosen (https://harvesthq.github.io/chosen/) ?>
                        @foreach($users as $user)
                            <?php $user_full_name = $user->get_full_name(); ?>
                            <option value='{{ $user_full_name }}'>
                                {{ $user_full_name }}
                            </option>
                        @endforeach
                    </datalist>
                    @error('user_found_on_db_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class='form-group row'>
                <label for='socket_type' class='col-sm-10 col-lg-2 col-form-label'>
                    {{ ucfirst(__('berths/create_subscription.transponder_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <select name='transponder_id' class='form-control custom-select'>
                        @if(Transponder::where('used_for_socket_subscriptions', true)->count() > 0)
                            <option value='-1'>{{ ucfirst(__('berths/create_subscription.transponder_field_please_select_an_option_caption')) }}</option>
                            @foreach(Transponder::where('used_for_socket_subscriptions', true)->get() as $current_transponder)
                                <?php 
                                    $current_transponder_caption_for_option_element = $current_transponder->get_code();
                                    $postpaid_subscriptions_connected_to_current_transponder = $current_transponder->active_subscriptions;
                                    if(($postpaid_subscriptions_connected_to_current_transponder) && ($postpaid_subscriptions_connected_to_current_transponder->count() > 0)) {
                                        $postpaid_subscriptions_connected_to_current_transponder->filter(function ($current_subscription) {
                                            return ($current_subscription->is_postpaid() && !($current_subscription->is_bound_to_socket())) ;
                                        });  
                                        if($postpaid_subscriptions_connected_to_current_transponder->count() > 0) {
                                            $most_recent_postpaid_subscription_connected_to_current_transponder = $postpaid_subscriptions_connected_to_current_transponder->sortByDesc('begin_dt')->first();
                                            $user_full_name = $most_recent_postpaid_subscription_connected_to_current_transponder->user->get_full_name();
                                            // commento l'aggiunta dello username per non scrivere km di roba nella select:
                                            /* 
                                            if(strlen(strval($most_recent_postpaid_subscription_connected_to_current_transponder->user->get_username())) > 0) {
                                                $user_full_name .= " [".strtolower(__("berths/create_subscription.username")).": ".$most_recent_postpaid_subscription_connected_to_current_transponder->user->get_username()."]";
                                            }
                                            */
                                            $current_transponder_caption_for_option_element .= " (".$user_full_name.")";
                                        }
                                    }

                                ?>
                                <option value='{{ $current_transponder->get_code() }}'>{{ $current_transponder_caption_for_option_element }}</option>
                            @endforeach
                        @else
                            <option value='-1'>{{ ucfirst(__('berths/create_subscription.transponder_field_there_are_no_transponders_used_for_socket_subscriptions_caption')) }}</option>
                        @endif
                        <?php // TODO: devo impedire il submit se non è stato selezionato nessun transponder o se - non esistendo transponder usati per le socket_subscriptions - non è stato scelto nessun valore ?>
                    </select>
                </div>
            </div>
            <div class='form-group row'>
            <?php // TODO: visualizzare questo button solo se esistono altri transponder postpagati abilitabili alla creazione di socket_subscriptions? ?>
                <div class='col-sm-10 col-lg-2 col-form-label'>
                </div>
                <div class='col-sm-10'>
                    <input type="hidden" name="berth_on_which_the_subscription_must_be_created" value="{{ $berth->get_id() }}">
                    <button type="submit" class="btn btn-info" formmethod="get"
                            formaction="{{ route('select_new_transponder_to_create_socket_subscriptions') }}">
                        {{ ucfirst(__('berths/create_subscription.add_new_postpaid_transponder_used_for_socket_subscriptions_button_caption')) }}
                    </button>
                </div>
            </div>
            <?php
                $min_timestamp = Carbon::parse(config('dates_currency_and_locale.min_utc_timestamp'), new \DateTimeZone(config('app.timezone')))->setTimeZone(config('dates_currency_and_locale.local_timezone'));
                $min_date_value = $min_timestamp->format('Y-m-d');
                $max_timestamp = Carbon::parse(config('dates_currency_and_locale.max_utc_timestamp'), new \DateTimeZone(config('app.timezone')))->setTimeZone(config('dates_currency_and_locale.local_timezone'));
                $max_date_value = $max_timestamp->format('Y-m-d');
                $now = Carbon::now(config('app.timezone'), new \DateTimeZone(config('app.timezone')))->setTimeZone(config('dates_currency_and_locale.local_timezone'));
                $now_date_string = $now->format('Y-m-d');
                $now_time_string = $now->format('H:i:s');
            ?>
            <div class='form-group row'>
                <label for='begin_timestamp' class='col-sm-10 col-lg-2 col-form-label'>
                    {{ ucfirst(__('berths/create_subscription.begin_timestamp_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <input type='date' class='form-control'
                           name='subscription_start_date_local' value='{{ $now_date_string }}'
                           min='{{ $min_date_value }}' max='{{ $max_date_value }}' step='1' />
                </div>
            </div>
            <div class='form-group row'>
                <label class='col-sm-10 col-lg-2 col-form-label'></label>
                <div class='col-sm-10'>
                    <input type='time' class='form-control @error("subscription_start_datetime_string") is-invalid @enderror'
                           name='subscription_start_time' value='{{ $now_time_string }}' step='1' />
                    @error('subscription_start_datetime_string')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class='form-group row'>
                <label for='notes' class='col-sm-10 col-lg-2 col-form-label'>
                    {{ ucfirst(__('berths/create_subscription.notes_field_label_caption')) }}
                </label>
                <div class='col-sm-10'>
                    <textarea id='notes' class='form-control' name='notes'></textarea>
                </div>
            </div>
            <?php /* TODO: il pulsante lo metto a destra o lo lascio qui? */ ?>
            <div class='form-group row'>
                <div class='col-sm-10'>
                    <button class="btn btn-primary" type="submit" formmethod="post" formaction="{{ route('create_new_subscription_on_berth') }}"
                            onclick="return validate_selected_user('{{ $user_not_found_alert_msg }}');"
                            onsubmit="return confirm('{{ $form_submit_confirmation_request_msg }}');">
                        {{ ucfirst(__('berths/create_subscription.submit_btt_caption')) }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
