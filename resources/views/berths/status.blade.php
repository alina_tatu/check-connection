<?php
    use Illuminate\Support\Carbon;
    use App\Berth;
    use App\BaseSocketSnapshot;
    use App\Http\Controllers\ConsumptionReportsController;
?>
@extends('layouts.app_menu')

@section('custom_css')
    <link href="{{ URL::asset('css/berths_status.css') }}" rel="stylesheet">
@endsection

@section('custom_js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.33/moment-timezone-with-data-2012-2022.min.js'></script>
    <script>
        <?php // TODO: posso mettere tutto in un componente Vue? Come funzionano? ?>
        var app = new Vue({
            el: '#content',
            data: function () {
                return {
                    available_socket_types: @json($available_socket_types),
                    currency_character: "{{ config('dates_currency_and_locale.currency_character') }}",
                    sockets: @json($sockets),
                    pontoon_name_filter_content: "-1",
                    berth_name_filter_content: "",
                    customer_or_boat_filter_content: "",
                    socket_type_filter_content: "",
                    SOCKET_SNAPSHOT_ALARM_TYPES: { 
                        "MISSING_COMMUNICATION": 1,
                        "THEFT": 2, 
                        "POWER_CONSUMPTION_UNDER_MIN_THRESHOLD": 3, 
                        "POWER_CONSUMPTION_OVER_MAX_THRESHOLD": 4,
                        "WATER_CONSUMPTION_TIME_OVER_MAX_USER_THRESHOLD": 5,
                        "CREDIT_ON_SOCKET_UNDER_MIN_THRESHOLD": 6,
                        "WATER_CONSUMPTION_TIME_OVER_MAX_ADMIN_THRESHOLD": 7,
                    },
                    SOCKET_SNAPSHOT_ALARM_FLAGS: { 
                        "MISSING_COMMUNICATION": "{{ BaseSocketSnapshot::ALARMS_FIELD_FLAG_NOT_CONNECTED }}",
                        "THEFT": "{{ BaseSocketSnapshot::ALARMS_FIELD_FLAG_THEFT }}",
                        "POWER_CONSUMPTION_UNDER_MIN_THRESHOLD": "{{ BaseSocketSnapshot::ALARMS_FIELD_FLAG_MIN_POWER_CONSUMPTION }}", 
                        "POWER_CONSUMPTION_OVER_MAX_THRESHOLD": "{{ BaseSocketSnapshot::ALARMS_FIELD_FLAG_MAX_POWER_CONSUMPTION }}",
                        "WATER_CONSUMPTION_TIME_OVER_MAX_USER_THRESHOLD": "{{ BaseSocketSnapshot::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_USERS }}",
                        "CREDIT_ON_SOCKET_UNDER_MIN_THRESHOLD": "{{ BaseSocketSnapshot::ALARMS_FIELD_FLAG_MIN_CREDIT }}",
                        "WATER_CONSUMPTION_TIME_OVER_MAX_ADMIN_THRESHOLD": "{{ BaseSocketSnapshot::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_ADMINS }}",
                    },
                }
            },
            computed: {
                searched_pontoon() {
                    return document.querySelector("#pontoon_name_filter_select")
                                   .options[document.querySelector("#pontoon_name_filter_select").selectedIndex]
                                   .text;
                },
                filtered_sockets() {
                    
                    let filtered_sockets = this.sockets;
                    if((this.pontoon_name_filter_content !== "{{ ucfirst(__('berths/status.pontoon_filter_no_pontoon_detected_caption')) }}") && 
                       (this.pontoon_name_filter_content !== "-1")) {                      
                        filtered_sockets = filtered_sockets.filter(socket => {
                            return socket.pontoon_name.toLowerCase().trim() === this.pontoon_name_filter_content.toLowerCase().trim()
                        })
                    }

                    filtered_sockets = filtered_sockets.filter(socket => {
                        return (((socket.pontoon_name + " " + socket.berth_name).toLowerCase().trim().indexOf(this.berth_name_filter_content.toLowerCase().trim()) > -1) || 
                                ((socket.description !== null) && ((socket.description).toLowerCase().trim().indexOf(this.berth_name_filter_content.toLowerCase().trim()) > -1)))
                    });
                    
                    if(this.customer_or_boat_filter_content.trim().length > 0) {
                        filtered_sockets = filtered_sockets.filter(socket => {
                            if((socket.last_snapshot) && 
                               (this.transponder_id_on_snapshot_is_reliable(socket)) &&
                               (socket.last_snapshot.subscription) && 
                               (socket.last_snapshot.subscription.user)) {
                                let user_full_name_and_boat_string = socket.last_snapshot.subscription.user.last_name + " " + 
                                                                     socket.last_snapshot.subscription.user.first_name; 
                                if(socket.last_snapshot.subscription.user.username !== null) {
                                    user_full_name_and_boat_string += " " + socket.last_snapshot.subscription.user.username.toString();
                                }                                                                     
                                if(this.socket_user_has_a_boat(socket)) {
                                    user_full_name_and_boat_string += " " + socket.last_snapshot.subscription.user.boat.name.toLowerCase().trim();
                                }
                                return (user_full_name_and_boat_string.toLowerCase().trim().indexOf(this.customer_or_boat_filter_content.toLowerCase().trim()) > -1)
                            }
                        })
                    }
                    
                    if(this.socket_type_filter_content.trim().length > 0) {
                        filtered_sockets = filtered_sockets.filter(socket => {
                            if(socket.type) {
                                return (socket.type.toLowerCase().trim().indexOf(this.socket_type_filter_content.toLowerCase().trim()) > -1)
                            }
                        });
                    }
                    return filtered_sockets;
                }
            },
            mounted() {    
                
            },
            methods: {
                unique_pontoons(sockets) {
                    return sockets.map(socket => socket.pontoon_name)
                                  .filter(function(item, pos) { 
                                      return sockets.map(socket => socket.pontoon_name).indexOf(item) === pos; 
                                  });
                },
                berth_description: function(socket) {
                    if(((typeof socket.description !== 'undefined') && (socket.description !== null)) && 
                        (socket.description.length > 0)) {
                        return socket.description;
                    }
                    return socket.pontoon_name + " " + socket.berth_name;
                },                
                on_off_btt_class: function(socket) {
                    switch(socket.last_snapshot.on_off_status) {
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}": 
                            return "btn-success";
                            break;
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF }}": 
                            return "btn-danger";
                            break;
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_DISABLED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_FORCED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_NOT_CONNECTED }}": 
                        default:
                            return "btn-warning";
                            break;
                    }
                },
                on_off_btt_text: function(socket) {
                    switch(socket.last_snapshot.on_off_status) {
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}": 
                            return "{{ strtoupper(__('common.socket_status_on')) }}";
                            break;
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF }}": 
                            return "{{ strtoupper(__('common.socket_status_off')) }}";
                            break;
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_NOT_CONNECTED }}": 
                        default:
                            return "{{ strtoupper(__('common.socket_status_not_connected')) }}";
                            break;
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_DISABLED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_FORCED }}": 
                            return socket.last_snapshot.on_off_status.toUpperCase();            <?php // TODO: definire una translation per ognuno di questi casi? ?>
                            break;
                    }
                },
                on_off_btt_tooltip(socket) {
                    let tooltip = "";
                    switch(socket.last_snapshot.on_off_status) {
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}": 
                            tooltip = ucfirst("{{ __('berths/status.onoff_btt_tooltip_click_to_switch_off_socket') }}");
                            break;
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF }}": 
                            tooltip = ucfirst("{{ __('berths/status.onoff_btt_tooltip_socket_switchon_is_disabled_for_security_reasons') }}");
                            <?php // TODO: dovrò modificare il tooltip quando sarà possibile accendere le prese da remoto ?>
                            break;
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_DISABLED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_FORCED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_NOT_CONNECTED }}": 
                        default:
                            tooltip = ucfirst("{{ __('berths/status.onoff_btt_tooltip_socket_is_not_responding_unable_to_control_it') }}");
                            <?php // TODO: per il momento lascio un tooltip generico per tutti questi case. Andrebbero differenziati. ?>
                            break;
                    }
                    return tooltip;
                },
                on_off_btt_form_action(socket) {
                    let form_action = "#";
                    switch(this.socket_can_be_switched_on_or_off(socket)) {
                        case true:
                            switch(socket.last_snapshot.on_off_status) {
                                case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}":
                                    form_action = "{{ route('turn_off_socket', ['socket_id' => -1]) }}".replace("-1", socket.id);
                                    break;
                                case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF }}": 
                                case "{{ BaseSocketSnapshot::SOCKET_STATUS_DISABLED }}": 
                                case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED }}": 
                                case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED }}": 
                                case "{{ BaseSocketSnapshot::SOCKET_STATUS_FORCED }}": 
                                case "{{ BaseSocketSnapshot::SOCKET_STATUS_NOT_CONNECTED }}": 
                                default:
                                    <?php // in tutti questi casi lascio il form_action "#". TODO: dovrò modificare quello del SOCKET_STATUS_OFF quando sarà possibile accendere le prese da remoto ?>        
                            }
                            break;
                        case false:
                        default:
                            <?php // in questo caso lascio il form_action "#". ?>
                            break;
                    }
                    return form_action;
                },
                on_off_btt_confirmation_request(socket) {
                    let confirmation_request = null;
                    switch(socket.last_snapshot.on_off_status) {
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}":
                            confirmation_request = "{{ ucfirst(__('berths/status.onoff_btt_alert_switchoff_confirmation_request')) }}".replace(":berth", this.berth_description(socket));
                            break;
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_DISABLED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_FORCED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_NOT_CONNECTED }}": 
                        default:
                            <?php // in questo caso la confirmation request non deve essere visualizzata. TODO: dovrò modificare quella del SOCKET_STATUS_OFF quando sarà possibile accendere le prese da remoto ?>
                            break;
                    }
                    return confirmation_request;
                },
                reset_btt_confirmation_request(socket) {
                    let confirmation_request = null;
                    switch(this.socket_can_be_resetted(socket)) {
                        case true:
                            confirmation_request = "{{ ucfirst(__('berths/status.reset_btt_alert_action_confirmation_request')) }}".replace(":berth", this.berth_description(socket));
                            break;
                        case false:
                        default:
                            <?php // in questo caso la confirmation request non deve essere visualizzata ?>
                            break;
                    }
                    return confirmation_request;
                },
                reset_btt_form_action(socket) {
                    let form_action = "#";
                    switch(this.socket_can_be_resetted(socket)) {
                        case true:
                            form_action = "{{ route('reset_socket', ['socket_id' => -1]) }}".replace("-1", socket.id);
                            break;                        
                        case false:
                        default:
                            <?php // in questo caso lascio il form_action "#" ?>
                            break;
                    }
                    return form_action;
                },
                link_status_class(socket) {
                    switch(socket.last_snapshot.link_status) {
                        case "{{ BaseSocketSnapshot::LINK_STATUS_CONNECTED }}":
                            return "text-success";
                            break;
                        case "{{ BaseSocketSnapshot::LINK_STATUS_THEFT_ALARM }}":
                        <?php // TODO: l'allarme furto andrebbe mostrato da un'altra parte, non c'entra con lo stato comunicazione ?>
                            return "text-danger";
                            break;
                        case "{{ BaseSocketSnapshot::LINK_STATUS_NOT_CONNECTED }}":
                        default:
                            return "text-danger";
                    }
                },
                link_status_text: function(socket) {
                    switch(socket.last_snapshot.link_status) {
                        case "{{ BaseSocketSnapshot::LINK_STATUS_CONNECTED }}":
                            return "{{ strtoupper(__('common.socket_link_status_connected')) }}";
                            break;
                        case "{{ BaseSocketSnapshot::LINK_STATUS_THEFT_ALARM }}":
                        <?php // TODO: l'allarme furto andrebbe mostrato da un'altra parte, non c'entra con lo stato comunicazione ?>
                            return "{{ strtoupper(__('common.socket_link_status_theft_alarm')) }}";
                            break;
                        case "{{ BaseSocketSnapshot::LINK_STATUS_NOT_CONNECTED }}":
                        default:
                            return "{{ strtoupper(__('common.socket_link_status_not_connected')) }}";
                    }
                },
                postpaid_span_caption() {
                    return "{{ strtoupper(__('berths/status.postpaid_user_span_caption')) }}";
                },
                socket_is_online: function(socket) {
                    switch(socket.last_snapshot.link_status) {
                        case "{{ BaseSocketSnapshot::LINK_STATUS_CONNECTED }}":
                        case "{{ BaseSocketSnapshot::LINK_STATUS_THEFT_ALARM }}":
                            return true;
                            break;
                        case "{{ BaseSocketSnapshot::LINK_STATUS_NOT_CONNECTED }}":
                        default:
                            return false;
                    }
                },
                socket_is_switched_on: function(socket) {
                    switch(socket.last_snapshot.on_off_status) {
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}": 
                            return true;
                            break;
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_DISABLED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_FORCED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_NOT_CONNECTED }}": 
                        default:
                            return false;
                            break;
                    }
                },
                socket_can_be_switched_on_or_off: function(socket) {
                    if(this.socket_is_online(socket) && (socket.last_snapshot.on_off_status === "{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}")) {
                    // per ora l'on_off_btt può essere usato solo per spegnere, non per accendere le prese
                        return true;
                    }
                    return false;
                },
                socket_can_be_resetted: function(socket) {
                    switch(socket.last_snapshot.on_off_status) {
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF }}": 
                            return true;
                            break;                        
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_NOT_CONNECTED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_DISABLED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED }}": 
                        case "{{ BaseSocketSnapshot::SOCKET_STATUS_FORCED }}": 
                        default:
                            return false;
                            <?php // in tutti questi casi non permetto il reset e dico che la presa non è controllabile. TODO: è ok impedire il reset in tutti questi casi? ?>
                            break;
                    }
                },
                socket_has_a_connected_user: function(socket) {
                    if((socket.last_snapshot) && 
                       (socket.last_snapshot !== null) &&
                       (this.transponder_id_on_snapshot_is_reliable(socket)) &&
                       (socket.last_snapshot.subscription) && 
                       (socket.last_snapshot.subscription !== null) && 
                       (socket.last_snapshot.subscription.user) && 
                       (socket.last_snapshot.subscription.user !== null)) {
                        return true;
                    }
                    return false;
                },
                socket_user_has_a_username: function(socket) {
                    if(this.socket_has_a_connected_user(socket)) {
                        if((socket.last_snapshot.subscription.user.username !== 'undefined') && 
                           (socket.last_snapshot.subscription.user.username !== null) && 
                           (socket.last_snapshot.subscription.user.username.length > 0)) {
                            return true;
                        }
                    }
                    return false;
                }, 
                socket_user_has_a_boat(socket) {
                    if((socket.last_snapshot) &&
                       (socket.last_snapshot !== null) &&
                       (this.transponder_id_on_snapshot_is_reliable(socket)) &&
                       (socket.last_snapshot.subscription) &&
                       (socket.last_snapshot.subscription !== null) &&
                       (socket.last_snapshot.subscription.user) &&
                       (socket.last_snapshot.subscription.user !== null) &&
                       (socket.last_snapshot.subscription.user.boat !== null)) {
                        return true;
                    }
                    return false;
                },
                socket_user_boat_has_measurements_set(socket) {
                    if((socket.last_snapshot) && 
                       (socket.last_snapshot !== null) &&
                       (this.transponder_id_on_snapshot_is_reliable(socket)) &&
                       (socket.last_snapshot.subscription) && 
                       (socket.last_snapshot.subscription !== null) && 
                       (socket.last_snapshot.subscription.user) && 
                       (socket.last_snapshot.subscription.user !== null) && 
                       (socket.last_snapshot.subscription.user.boat !== null) && 
                       (Number.parseFloat(socket.last_snapshot.subscription.user.boat.width) > 0)) {
                        return true;
                    }
                    return false;
                },
                boat_measurements_span_content(boat) {
                    return "{{ strtolower(__('berths/status.boat_length')) }}: " + boat.length + ", {{ strtolower(__('berths/status.boat_width')) }}: " + boat.width + ", " + "{{ strtolower(__('berths/status.boat_depth')) }}: " + boat.depth;
                },
                transponder_id_on_snapshot_is_reliable(socket) {
                    if((socket.last_snapshot) && 
                       (socket.last_snapshot !== null) && 
                       (Boolean(socket.last_snapshot.transponder_id_field_is_reliable) === true)) {
                        return true;
                    }
                    return false;
                },
                change_chart_form_action_according_to_selected_period: function(event) {
                <?php // viene chiamata quando si richiede un grafico di consumo per un posto barca (sia all'evento change sulla select del periodo, sia al click sul pulsante di submit (in questo modo - anche se la pagina non viene refreshata da un po' - il grafico viene aggiornato all'istante del click). Legge il valore del periodo richiesto per il grafico e sistema di conseguenza l'url da chiamare per generarlo. ?>
                    if(event) {
                        let form = event.target.closest("form");
                        let chart_period_select = form.getElementsByTagName("select")[0];
                        let selected_period_before = "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}";
                        let period_to_set = chart_period_select.options[chart_period_select.selectedIndex].value;
                        let current_form_action = form.action;
                        let splitted_current_form_action;
                        if(form.action.includes("{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}")) {
                            selected_period_before = "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}";
                        } else if(form.action.includes("{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}")) {
                            selected_period_before = "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}";
                        }
                        let selected_sampling_before = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR }}";
                        if(form.action.includes("{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY }}")) {
                            selected_sampling_before = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY }}";
                        }
                        let begin_timestamp_to_set;
                        let end_timestamp_to_set = moment().format("X");
                        switch(period_to_set) {
                            case "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}":
                            default:
                                begin_timestamp_to_set = moment().subtract(24, 'hours').format("X");
                                sampling_to_set = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR }}";
                                break;
                            case "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}":
                                begin_timestamp_to_set = moment().subtract(7, 'days').format("X");
                                sampling_to_set = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY }}";
                                break;
                            case "{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}":
                                begin_timestamp_to_set = moment().subtract(moment().daysInMonth(), 'days').format("X");
                                sampling_to_set = "{{ ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY }}";
                        }
                        let slashes_positions_in_current_form_action_from_end = [];
                        let last_slash_position_in_current_form_action_temp;
                        let i = 0;
                        let current_form_action_temp = current_form_action;
                        while(i < 5) {
                            last_slash_position_in_current_form_action_temp = current_form_action_temp.lastIndexOf("/");
                            if(last_slash_position_in_current_form_action_temp !== (last_slash_position_in_current_form_action_temp.length - 1)) {      // evito di considerare l'eventuale slash in fondo all'url
                                slashes_positions_in_current_form_action_from_end[i] = last_slash_position_in_current_form_action_temp;
                                i++;
                            }
                            current_form_action_temp = current_form_action_temp.substring(0, (last_slash_position_in_current_form_action_temp - 1))
                        }
                        let begin_timestamp_to_replace = current_form_action.substring(slashes_positions_in_current_form_action_from_end[4] + 1, slashes_positions_in_current_form_action_from_end[3]);
                        let end_timestamp_to_replace = current_form_action.substring(slashes_positions_in_current_form_action_from_end[3] + 1, slashes_positions_in_current_form_action_from_end[2]);
                        form.action = current_form_action.replace(begin_timestamp_to_replace, begin_timestamp_to_set)
                                                         .replace(end_timestamp_to_replace, end_timestamp_to_set)
                                                         .replace(selected_sampling_before, sampling_to_set);
                    }
                },
                socket_snapshot_timestamp_formatted_string(socket) {
                    if(socket.last_snapshot) {
                        return moment.utc(socket.last_snapshot.timestamp)
                                     .tz("{{ config('dates_currency_and_locale.local_timezone') }}")
                                     .format("{{ config('dates_currency_and_locale.local_datetime_format_full_momentjs') }}");
                    }
                    return "";
                },                
                alarm_is_active_on_socket: function(socket, alarm_type) {
                    <?php 
                        /* 
                        // TODO: posso rendere più semplice questo metodo? 
                                 ho provato con 
                                 if((socket.last_snapshot.alarms !== null) && (socket.last_snapshot.alarms.includes(this.SOCKET_SNAPSHOT_ALARM_TYPES.alarm_type))) {
                                    return true;
                                 }
                                 ma non funziona
                        */
                    ?>
                    if(socket.last_snapshot.alarms !== null) {
                        switch(alarm_type) {
                            case this.SOCKET_SNAPSHOT_ALARM_TYPES.MISSING_COMMUNICATION:
                                if(socket.last_snapshot.alarms.includes(this.SOCKET_SNAPSHOT_ALARM_FLAGS.MISSING_COMMUNICATION)) {
                                    return true;
                                }
                                break;
                            case this.SOCKET_SNAPSHOT_ALARM_TYPES.THEFT:
                                if(socket.last_snapshot.alarms.includes(this.SOCKET_SNAPSHOT_ALARM_FLAGS.THEFT)) {
                                    return true;
                                }
                                break;
                            case this.SOCKET_SNAPSHOT_ALARM_TYPES.WATER_CONSUMPTION_TIME_OVER_MAX_ADMIN_THRESHOLD:
                                if(socket.last_snapshot.alarms.includes(this.SOCKET_SNAPSHOT_ALARM_FLAGS.WATER_CONSUMPTION_TIME_OVER_MAX_ADMIN_THRESHOLD)) {
                                    return true;
                                }
                                break;
                            case this.SOCKET_SNAPSHOT_ALARM_TYPES.POWER_CONSUMPTION_UNDER_MIN_THRESHOLD:
                                if(socket.last_snapshot.alarms.includes(this.SOCKET_SNAPSHOT_ALARM_FLAGS.POWER_CONSUMPTION_UNDER_MIN_THRESHOLD)) {
                                    return true;
                                }
                                break;
                            case this.SOCKET_SNAPSHOT_ALARM_TYPES.POWER_CONSUMPTION_OVER_MAX_THRESHOLD:
                                if(socket.last_snapshot.alarms.includes(this.SOCKET_SNAPSHOT_ALARM_FLAGS.POWER_CONSUMPTION_OVER_MAX_THRESHOLD)) {
                                    return true;
                                }
                                break;
                            case this.SOCKET_SNAPSHOT_ALARM_TYPES.WATER_CONSUMPTION_TIME_OVER_MAX_USER_THRESHOLD:
                                if(socket.last_snapshot.alarms.includes(this.SOCKET_SNAPSHOT_ALARM_FLAGS.WATER_CONSUMPTION_TIME_OVER_MAX_USER_THRESHOLD)) {
                                    return true;
                                }
                                break;
                            case this.SOCKET_SNAPSHOT_ALARM_TYPES.CREDIT_ON_SOCKET_UNDER_MIN_THRESHOLD:
                                if(socket.last_snapshot.alarms.includes(this.SOCKET_SNAPSHOT_ALARM_FLAGS.CREDIT_ON_SOCKET_UNDER_MIN_THRESHOLD)) {
                                    return true;
                                }
                        }
                    } 
                    return false;
                },
                link_status_row_class: function(socket) {
                    let row_class = "";
                    if((this.alarm_is_active_on_socket(socket, this.SOCKET_SNAPSHOT_ALARM_TYPES.MISSING_COMMUNICATION)) || 
                       (this.alarm_is_active_on_socket(socket, this.SOCKET_SNAPSHOT_ALARM_TYPES.THEFT))) {
                        row_class = "alarm_over_threshold_active";
                    }
                    return row_class;
                },
                power_consumption_row_class: function(socket) {
                    let row_class = "";
                    if(this.alarm_is_active_on_socket(socket, this.SOCKET_SNAPSHOT_ALARM_TYPES.POWER_CONSUMPTION_UNDER_MIN_THRESHOLD)) {
                        row_class = "alarm_under_threshold_active";
                    } else if(this.alarm_is_active_on_socket(socket, this.SOCKET_SNAPSHOT_ALARM_TYPES.POWER_CONSUMPTION_OVER_MAX_THRESHOLD)) {
                        row_class = "alarm_over_threshold_active";
                    }
                    return row_class;
                },
                water_consumption_row_class: function(socket) {
                    let row_class = "";
                    if(this.alarm_is_active_on_socket(socket, this.SOCKET_SNAPSHOT_ALARM_TYPES.WATER_CONSUMPTION_TIME_OVER_MAX_ADMIN_THRESHOLD)) {
                        row_class = "alarm_over_threshold_active";
                    } else if(this.alarm_is_active_on_socket(socket, this.SOCKET_SNAPSHOT_ALARM_TYPES.WATER_CONSUMPTION_TIME_OVER_MAX_USER_THRESHOLD)) {
                        row_class = "alarm_under_threshold_active";
                    }
                    return row_class;
                },
                credit_row_class: function(socket) {
                    let row_class = "";
                    if(this.alarm_is_active_on_socket(socket, this.SOCKET_SNAPSHOT_ALARM_TYPES.CREDIT_ON_SOCKET_UNDER_MIN_THRESHOLD)) {
                        row_class = "alarm_over_threshold_active";
                    }
                    return row_class;
                },
                request_socket_switch_off(socket) {
                    if(confirm(this.on_off_btt_confirmation_request(socket))) {
                        let button = document.getElementById("socket_on_off_btt[" + socket.id + "]");
                        button.classList.add("pressed");
                        let route = "{{ route('turn_off_socket', ['socket_id' => -1]) }}".replace("-1", socket.id);
                        let csrf_token = document.head.querySelector('meta[name="csrf-token"]');
                        axios.post(route, {
                            csrfToken: csrf_token,
                            actionsToken: "{{ Auth::user()->get_token_for_actions_on_sockets() }}",
                        })
                        .then((response) => {
                            button.classList.remove("pressed");
                            alert("{{ ucfirst(__('common.socket_shutdown_action_requested_msg')) }}".replace(":berth_name", this.berth_description(socket)));
                        }, (error) => {
                            button.classList.remove("pressed");
                            alert("{{ ucfirst(__('common.error_occurred_while_performing_action_on_socket_msg')) }}".replace(":berth_name", this.berth_description(socket)));
                        });
                    }
                },
                request_socket_reset(socket) {
                    if(confirm(this.reset_btt_confirmation_request(socket))) {
                        let button = document.getElementById("socket_reset_btt[" + socket.id + "]");
                        button.classList.add("pressed");
                        let route = "{{ route('reset_socket', ['socket_id' => -1]) }}".replace("-1", socket.id);
                        let csrf_token = document.head.querySelector('meta[name="csrf-token"]');
                        axios.post(route, {
                            csrfToken: csrf_token,
                            actionsToken: "{{ Auth::user()->get_token_for_actions_on_sockets() }}",
                        })
                        .then((response) => {
                            button.classList.remove("pressed");
                            alert("{{ ucfirst(__('common.socket_reset_action_requested_msg')) }}".replace(":berth_name", this.berth_description(socket)));
                        }, (error) => {
                            button.classList.remove("pressed");
                            alert("{{ ucfirst(__('common.error_occurred_while_performing_action_on_socket_msg')) }}".replace(":berth_name", this.berth_description(socket)));
                        });
                    }
                },
            },
            created () {
                
            },
        })
    </script>
@endsection

@section('content')
    <?php /* TODO: convertire anche i dati di consumo energia di questa view in kW o Ah a seconda della preferenza dell'utente? O devo farlo solo per le soglie allarmi sulla view set_alarm_thresholds (lì l'ho già fatto)? Controllare se su SmartCloud 1.x lo facevo */ ?>
    <div id="content" class="mt-5 pt-5">
        @include('fragments.flash_message')
        <div id="filters_bar" class="form-horizontal px-2 py-1">
            <?php   // TODO: mancano i pulsanti per l"ordinamento, devo metterli? ?>
            <div class="card bg-light border-primary rounded mb-3">
                <div class="card-body"> <?php // TODO: calare il padding (tra border e filtri) + aumentare il margin (tra border e fine pagina) ?>
                    <div class="card-title">
                        {{ ucfirst(__("common.filters_bar_title")) }}
                    </div>
                    <div class="p-3 m-3 row">
                        <?php // TODO: come faccio a cambiare il colore del margine azzurro degli input quando hanno il focus? ?>
                        <div class="col-sm control-group custom-control-inline">
                            <div class="form-group w-100">
                                <select id="pontoon_name_filter_select"
                                        v-model="pontoon_name_filter_content" 
                                        class="form-control custom-select">
                                    <option v-if="sockets.length > 0"
                                            value="-1">{{ ucfirst(__('berths/status.pontoon_filter_caption')) }}</option>
                                    <option v-if="sockets.length > 0" 
                                            v-for="pontoon,index in unique_pontoons(sockets)"
                                            v-text="pontoon">
                                    </option>
                                    <option v-if="sockets.length === 0" selected>
                                        {{ ucfirst(__('berths/status.pontoon_filter_no_pontoon_detected_caption')) }}
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm control-group custom-control-inline">
                            <div class="form-group w-100">
                                <input id="berth_filter_input"
                                       type="text"
                                       class="form-control" 
                                       v-model="berth_name_filter_content"
                                       placeholder="{{ ucfirst(__('berths/status.berth_filter_caption')) }}">
                            </div>
                        </div>

                        <div class="col-sm control-group custom-control-inline">
                            <div class="form-group w-100">
                                <input type="text" 
                                       id="customer_or_boat_filter_input" 
                                       class="form-control" 
                                       v-model="customer_or_boat_filter_content"
                                       placeholder="{{ ucfirst(__('berths/status.customer_filter_caption')) }}">
                            </div>
                        </div>

                        <div class="col-sm control-group custom-control-inline"
                             v-if="available_socket_types.length > 1">           <?php // mostro il filtro solamente se nell'impianto ci sono prese di più tipi differenti ?>
                            <div class="form-group w-100">
                                <input type="text" 
                                       id="socket_type_filter_input"
                                       class="form-control" 
                                       v-model="socket_type_filter_content"
                                       placeholder="{{ ucfirst(__('berths/status.socket_type_filter_caption')) }}">
                                    <?php // TODO: questa deve diventare una select ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div v-cloak class="container">
            <div v-for="current_socket in filtered_sockets"
             	 class="container socket_snapshot_div mb-3 pb-3">            
                <div class="row mb-1 pl-1">
                    <span v-if="current_socket.last_snapshot !== null">
                        <span data-toggle="tooltip">    
                        <?php // wrappo il button in uno <span> per poter specificare valori multipli per l'attributo data-toggle ?>
                            <button type="button" 
                                    :id="'socket_status_collapse_btt[' + current_socket.id + ']'"
                                    class="btn btn-primary mr-2 socket_status_collapse_btt"
                                    data-toggle="collapse"                                
                                    data-placement="top" 
                                    title="{{ ucfirst(__('berths/status.show_hide_snapshot_div_btt_tooltip_caption')) }}"                            
                                    aria-expanded="false" 
                                    :aria-controls="'socket_status_collapsible_div[' + current_socket.id + ']'"
                                    :data-target="'#socket_status_collapsible_div[' + current_socket.id + ']'"
                                    data-placement="top" 
                                    title="{{ ucfirst(__('berths/status.show_hide_snapshot_div_btt_tooltip_caption')) }}">
                                <i class="fas fa-eye"></i>
                            </button>
                        </span>
                    </span>

                    <span class="text-info p-2 d-inline text-nowrap">
                        <i class="fas fa-anchor"></i>
                        <span class="berth_description font-weight-bold" v-text="berth_description(current_socket)"></span>
                        <span class="pontoon_name d-none" v-text="current_socket.pontoon_name"></span>    <?php // campo nascosto, usato per il filtro sul pontile ?>
                    </span>&nbsp;

                    <span v-if="current_socket.last_snapshot !== null">
                        <form class="d-inline p-0 m-0" method="post">
                        @csrf
                            <?php // TODO: spostare i due pulsanti <on/off> e <reset> dentro la tabella? (aggiungendo la riga <status> ed eventualmente una seconda riga, ma direi che se ne potrebbe fare una sola, magari chiamandola <status/actions>) ?>
                            @if(Auth::user()->is_full_admin())
                            
                                <?php // se l'utente loggato è (almeno) un full_admin, può spegnere/accendere le prese: ?>
                                <span class="tooltip-btn-wrapper"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    v-bind:title="on_off_btt_tooltip(current_socket)">
                                    <button v-bind:id="'socket_on_off_btt[' + current_socket.id + ']'"
                                            type="button"
                                            class="btn"
                                            v-bind:class="on_off_btt_class(current_socket)"
                                            v-bind:disabled="socket_can_be_switched_on_or_off(current_socket) === false"
                                            v-bind:formaction="on_off_btt_form_action(current_socket)"
                                            v-text="on_off_btt_text(current_socket)"
                                            v-on:click="request_socket_switch_off(current_socket)"> <?php // TODO: al momento in Vue ho solo il metodo per lo spegnimento (tanto se la presa è spenta il pulsante è disabled). Dovrò aggiungere quello per l'accensione quando servirà (o aggiungere un parametro al metodo esistente, magari unendolo al metodo per il reset) ?>
                                    </button>
                                </span>
                                
                                <?php // e resettarle: ?>
                                <span class="tooltip-btn-wrapper"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="{{ ucfirst(__('berths/status.reset_btt_tooltip_click_to_reset_socket')) }}">
                                    <button v-bind:id="'socket_reset_btt[' + current_socket.id + ']'"
                                            type="button"
                                            class="btn btn-warning"
                                            v-if="socket_is_online(current_socket)"
                                            v-bind:formaction="reset_btt_form_action(current_socket)"
                                            v-on:click="request_socket_reset(current_socket)">
                                        {{ strtoupper(__('berths/status.reset_btt_caption')) }}
                                    </button>
                                </span>
                                
                            @else
                                <?php // se l'utente loggato è un viewer_admin, non può spegnere/accendere le prese nè resettarle, ma solo visualizzarne lo stato: ?>
                                <button type="button"
                                        formaction="#"
                                        class="btn"
                                        v-bind:class="on_off_btt_class(current_socket)"
                                        disabled
                                        v-text="on_off_btt_text(current_socket)">
                                    <?php // TODO: manca il tooltip del button (ma qui non ha senso abilitare/disabilitare in base ad una condizione: deve essere sempre disabilitato!) ?>
                                </button>
                            @endif
                        </form>
                    </span>
                </div>

                <div v-if="current_socket.last_snapshot !== null" 
                    class="collapse"
                    :id="'socket_status_collapsible_div[' + current_socket.id + ']'">

                    <div :id="'socket_status[' + current_socket.id + ']'"
                        class="socket_status table-responsive row">
                        <table class="table table-hover">
                            <tbody>                                
                                <tr v-if="available_socket_types.length > 1">
                                    <th class='col-5 col-md-4'>{{ ucfirst(__('berths/status.socket_type_caption')) }}</th>
                                    <td>
                                        <span v-if="current_socket.type !== '{{ Berth::SOCKET_TYPE_NOT_DEFINED }}'"
                                              v-text="current_socket.type">
                                        </span>
                                        <span v-else>
                                            {{ strtoupper(__('common.socket_type_not_set')) }}
                                        </span>
                                    </td>
                                </tr>
                                
                                <tr v-bind:class="link_status_row_class(current_socket)">
                                
                                    <?php // TODO: perchè l'intestazione di questa riga ha lunghezze diverse per link_status diversi? ?>
                                    <?php // TODO: riportare il testo del link status da rosso a nero quando c'è un allarme (furto o mancata comunicazione), per evitare di avere la sovrapposizione testo rosso/sfondo rosso? ?>

                                    <th class="col-5 col-md-4">{{ ucfirst(__("berths/status.linkstatus_caption")) }}</th>
                                    <td class="col-7 col-md-8" colspan="3">
                                        <span class="socket_link_status_span"
                                            v-bind:class="link_status_class(current_socket)"
                                            v-text="link_status_text(current_socket)">
                                        </span>
                                    </td>
                                </tr>
                            </tbody>

                            <tbody v-if="socket_is_online(current_socket)">     <?php // visualizzo le righe contenenti le info sul credito, sul consumo di energia/acqua, sul consumo istantaneo e sull'ultimo utente connesso solamente se la presa se sta comunicando: ?>
                            
                                <tr v-bind:class="credit_row_class(current_socket)">
                                    <th class="col-5 col-md-4">
                                        {{ ucfirst(__('berths/status.residualcredit_caption')) }}
                                    </th>
                                    <td class="col-7 col-md-8" colspan="3">
                                        <span v-if="current_socket.last_snapshot.on_off_status !== '{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}'">
                                            -
                                        </span>
                                        <span v-if="current_socket.last_snapshot.on_off_status === '{{ BaseSocketSnapshot::SOCKET_STATUS_ON }}'">
                                            <span v-if="(current_socket.last_snapshot.residual_credit > 0) || 
                                                        ((current_socket.last_snapshot.subscription !== null) && (current_socket.last_snapshot.subscription.is_prepaid === true))"
                                                v-text="(Math.round(current_socket.last_snapshot.residual_credit * 100) / 100).toFixed(2) + ' ' + '{{ config('dates_currency_and_locale.currency_character') }}'">
                                            </span>
                                            <span v-else
                                                class="text-primary d-inline text-center text-nowrap"
                                                v-text="postpaid_span_caption()">
                                            </span>
                                        </span>
                                    </td>
                                </tr>

                                <tr v-bind:class="credit_row_class(current_socket)">
                                <?php // TODO: devo mostrare questa riga solamente nel caso in cui sia possibile richiedere un grafico del credito (forse è già ok così) ?>
                                    <td class="d-none d-sm-table-cell without_upper_border">

                                    </td>
                                    <td class="col-12 col-md-8 without_upper_border" colspan="3">
                                        <form :id="'socket_chart_credit_consumption_frm[' + current_socket.id + ']'"
                                              :action="'{{ route('get_berth_consumption_chart', ['berth_id' => -1,
                                                                                                 'begin_timestamp' => Carbon::now()->subHours(24)->timestamp,
                                                                                                 'end_timestamp' => Carbon::now()->timestamp,
                                                                                                 'subject' => ConsumptionReportsController::REPORT_VALUE_CREDIT,
                                                                                                 'sampling_interval_argument' => ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR]) }}'.replace('-1', current_socket.id)"
                                              method="post">
                                        @csrf
                                            <div class="input-group">
                                                <select class="custom-select"
                                                        v-on:change="change_chart_form_action_according_to_selected_period($event)">
                                                    <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}' selected>
                                                        {{ ucfirst(__("berths/status.chart_period_select_last_24hours_caption")) }}
                                                    </option>
                                                    <option value="{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}">
                                                        {{ ucfirst(__("berths/status.chart_period_select_last_week_caption")) }}
                                                    </option>
                                                    <option value="{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}">
                                                        {{ ucfirst(__("berths/status.chart_period_select_last_month_caption")) }}
                                                    </option>
                                                </select>
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-success" 
                                                            type="submit"
                                                            v-on:click="change_chart_form_action_according_to_selected_period($event)">
                                                        {{ ucfirst(__("berths/status.chart_submit_btt_caption")) }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>

                                <tr v-bind:class="power_consumption_row_class(current_socket)">
                                <?php // TODO: devo dare uno sfondo alla riga in base agli allarmi ?>
                                    <th class="col-5 col-md-4">
                                        {{ ucfirst(__('berths/status.socket_total_power_counter_caption')) }}
                                    </th>
                                    <td class="col-7 col-md-8" colspan="3">
                                        <span v-text="(Math.round(current_socket.last_snapshot.total_power_cnt * 100) / 100).toFixed(1)"></span>&nbsp;kWh
                                    </td>
                                </tr>

                                <tr v-bind:class="power_consumption_row_class(current_socket)">
                                <?php // TODO: devo mostrare questa riga solamente nel caso in cui sia possibile richiedere un grafico dell'energia (forse è già ok così) ?>
                                    <td class="d-none d-sm-table-cell without_upper_border">

                                    </td>
                                    <td class="col-12 col-md-8 without_upper_border" colspan="3">
                                        <form :id="'socket_chart_power_consumption_frm[' + current_socket.id + ']'"
                                              :action="'{{ route('get_berth_consumption_chart', ['berth_id' => -1,
                                                                                                 'begin_timestamp' => Carbon::now()->subHours(24)->timestamp,
                                                                                                 'end_timestamp' => Carbon::now()->timestamp,
                                                                                                 'subject' => ConsumptionReportsController::REPORT_VALUE_POWER,
                                                                                                 'sampling_interval_argument' => ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR]) }}'.replace('-1', current_socket.id)"
                                            method="post">
                                        @csrf
                                            <div class="input-group">
                                                <select class="custom-select"
                                                        v-on:change="change_chart_form_action_according_to_selected_period($event)">
                                                    <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}' selected>
                                                        {{ ucfirst(__("berths/status.chart_period_select_last_24hours_caption")) }}
                                                    </option>
                                                    <option value="{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}">
                                                        {{ ucfirst(__("berths/status.chart_period_select_last_week_caption")) }}
                                                    </option>
                                                    <option value="{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}">
                                                        {{ ucfirst(__("berths/status.chart_period_select_last_month_caption")) }}
                                                    </option>
                                                </select>
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-success" 
                                                            type="submit"
                                                            v-on:click="change_chart_form_action_according_to_selected_period($event)">
                                                        {{ ucfirst(__("berths/status.chart_submit_btt_caption")) }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>

                                <tr v-bind:class="power_consumption_row_class(current_socket)">
                                    <th class="col-5 col-md-4">
                                        {{ ucfirst(__('berths/status.socket_postpaid_power_counter_caption')) }}
                                    </th>
                                    <td class="col-7 col-md-8" colspan="3">
                                        <span v-text="(Math.round(current_socket.last_snapshot.postpaid_power_cnt * 100) / 100).toFixed(1)"></span>&nbsp;kWh
                                    </td>
                                </tr>

                                <tr> 
                                    <th class="col-5 col-md-4">
                                        {{ ucfirst(__('berths/status.instantpowerconsumption_caption')) }}
                                    </th>
                                    <td class="col-7 col-md-8" colspan="3">
                                        <span v-text="(Math.round(current_socket.last_snapshot.instant_power_consumption * 100) / 100).toFixed(3)"></span>&nbsp;kW
                                    </td>
                                </tr>
                                
                                <tr v-bind:class="water_consumption_row_class(current_socket)">
                                    <th class="col-5 col-md-4">
                                        {{ ucfirst(__('berths/status.socket_total_water_counter_caption')) }}
                                    </th>
                                    <td class="col-7 col-md-8" colspan="3">
                                        <span v-text="(Math.round(current_socket.last_snapshot.total_water_cnt * 100) / 100).toFixed(3)"></span>&nbsp;m<sup>3</sup>
                                    </td>

                                </tr>

                                <tr v-bind:class="water_consumption_row_class(current_socket)">
                                <?php // TODO: devo mostrare questa riga solamente nel caso in cui sia possibile richiedere un grafico dell'acqua (forse è già ok così) ?>
                                    <td class="d-none d-sm-table-cell without_upper_border">

                                    </td>
                                    <td class="col-12 col-md-8 without_upper_border" colspan="3">
                                        <form :id="'socket_chart_water_consumption_frm[' + current_socket.id + ']'"
                                              :action="'{{ route('get_berth_consumption_chart', ['berth_id' => -1,
                                                                                                 'begin_timestamp' => Carbon::now()->subHours(24)->timestamp,
                                                                                                 'end_timestamp' => Carbon::now()->timestamp,
                                                                                                 'subject' => ConsumptionReportsController::REPORT_VALUE_WATER,
                                                                                                 'sampling_interval_argument' => ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR]) }}'.replace('-1', current_socket.id)"
                                              method="post">
                                        @csrf
                                            <div class="input-group">
                                                <select class="custom-select"
                                                        v-on:change="change_chart_form_action_according_to_selected_period($event)">
                                                    <option value='{{ ConsumptionReportsController::REPORT_PERIOD_LAST_24_HOURS }}' selected>
                                                        {{ ucfirst(__("berths/status.chart_period_select_last_24hours_caption")) }}
                                                    </option>
                                                    <option value="{{ ConsumptionReportsController::REPORT_PERIOD_LAST_WEEK }}">
                                                        {{ ucfirst(__("berths/status.chart_period_select_last_week_caption")) }}
                                                    </option>
                                                    <option value="{{ ConsumptionReportsController::REPORT_PERIOD_LAST_MONTH }}">
                                                        {{ ucfirst(__("berths/status.chart_period_select_last_month_caption")) }}
                                                    </option>
                                                </select>
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-success" 
                                                            type="submit"
                                                            v-on:click="change_chart_form_action_according_to_selected_period($event)">
                                                        {{ ucfirst(__("berths/status.chart_submit_btt_caption")) }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </td>
                                </tr>

                                <tr v-bind:class="water_consumption_row_class(current_socket)">
                                    <th class="col-5 col-md-4">
                                        {{ ucfirst(__('berths/status.socket_postpaid_water_counter_caption')) }}
                                    </th>
                                    <td class="col-7 col-md-8" colspan="3">
                                        <span v-text="(Math.round(current_socket.last_snapshot.postpaid_water_cnt * 100) / 100).toFixed(3)"></span>&nbsp;m<sup>3</sup>
                                    </td>

                                </tr>

                                <tr>
                                    <th class="col-5 col-md-4">
                                        <span v-if="socket_is_switched_on(current_socket)">
                                            {{ ucfirst(__('common.current_user_on_socket_caption')) }}
                                        </span>
                                        <span v-else>
                                            {{ ucfirst(__('common.last_user_on_socket_caption')) }}
                                        </span>
                                    </th>
                                    
                                    <td class="col-7 col-md-8 text-nowrap" colspan="3">
                                        <span v-if="socket_has_a_connected_user(current_socket)">
                                            <span v-text="current_socket.last_snapshot.subscription.user.last_name + ' ' + current_socket.last_snapshot.subscription.user.first_name">
                                            </span>
                                            <span v-if="socket_user_has_a_username(current_socket)"
                                                v-text="current_socket.last_snapshot.subscription.user.username"
                                                class="text-light bg-primary p-1 rounded socket_user_username_span">
                                            </span>
                                        </span>
                                        <span v-else>
                                            <span class="text-danger">
                                                {{ strtoupper(__('common.unknown_user')) }}
                                            </span>
                                        </span>
                                    </td>                                
                                </tr>

                                <tr v-if="socket_user_has_a_boat(current_socket)">
                                    <th class="col-5 col-md-4">
                                        {{ ucfirst(__('common.boat')) }}
                                    </th>
                                    <td class="col-7 col-md-8 text-nowrap" colspan="3">
                                        <span class="boat_name_span" 
                                            v-text="current_socket.last_snapshot.subscription.user.boat.name">
                                        </span>
                                        <span v-if="socket_user_boat_has_measurements_set(current_socket)">
                                            <span>(@{{ boat_measurements_span_content(current_socket.last_snapshot.subscription.user.boat) }})</span>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>

                            <tbody>
                                <tr>
                                    <th class="col-5 col-md-4">{{ ucfirst(__('berths/status.lastupdatetimestamp_caption')) }}</th>
                                    <td class="col-7 col-md-8 text-nowrap" colspan="3">
                                        <span>
                                            @{{ socket_snapshot_timestamp_formatted_string(current_socket) }}
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div v-if="current_socket.last_snapshot === null" class="container-fluid mt-4 mx-auto">
                <?php // se sul db non ci sono snapshot relativi a questa presa: ?>
                    <div class="socket_status table-responsive row">
                        <h6 class="text-secondary text-uppercase font-weight-bold">
                            {{ ucfirst(__('berths/status.no_status_information_available_for_this_berth')) }}
                        </h6>
                    </div>
                </div>

            </div>

            <div v-if="!(this.filtered_sockets.length > 0)" 
                id="no_entries_message"
                class="container-fluid mt-5 pt-5 text-center">       
                <h6 class="text-secondary text-uppercase font-weight-bold">
                    {{ ucfirst(__('berths/status.no_berths_on_database_or_no_berths_match_search_criteria')) }}
                </h6>
            </div>
        </div>
    </div>
@endsection