<?php
    use App\Pedestal;
    use App\Berth;
    use App\SocketSnapshot;
?>
@extends('layouts.app_menu')

<?php $map_img_url = file_exists(public_path('resources/plant_dependent/img/map.jpg')) ? URL::asset('resources/plant_dependent/img/map.jpg') : URL::asset('resources/map.default.jpg'); ?>

@section('custom_css')
    <link href='{{ URL::asset('css/map.css') }}' rel='stylesheet'>
    <style>
        .p-05 {
            padding: ($spacer * .125) !important;
        }
        <?php foreach($pedestals as $pedestal): ?>
        <?php // TODO: questo ciclo dovrei unificarlo con gli altri che faccio su $pedestals per velocizzare i tempi di caricamento. Però quei li faccio dentro al js, quindi è un casino ?>
            img#pedestal_marker_{{ $pedestal->get_id() }} {
            <?php /* rispetto alla versione 1.x di SmartCloud, ho aggiunto in config gli offset x e y per le coordinate delle colonnine, perchè mi sono accorto che pur mantenendo la stessa immagine della mappa e lo stesso divider, le colonnine venivano posizionate in un punto diverso. */ ?>
                left: {{ ($pedestal->get_x_coord() / config('map.divider')) + config('map.x_coord_offset_px') }}px;
                top: {{ ($pedestal->get_y_coord() / config('map.divider')) + config('map.y_coord_offset_px') }}px;
            }
        <?php endforeach; ?>
    </style>
@endsection
@section('custom_js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js' type='application/javascript'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.31/moment-timezone-with-data-10-year-range.min.js' type='application/javascript'></script>
    <script>
        var zoomCompleted = true;
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/map_zoom_utils.js') }}"></script>
    <script type='application/javascript'>
        var pedestals = @json($pedestals);
        var allowed_socket_types = @json(Berth::ALLOWED_SOCKET_TYPES);
        var available_socket_types = @json($available_socket_types);
        function hide_map_zoom_guidelines_container_for_desktop() {
            document.getElementById('map_zoom_guidelines_container_for_desktop').classList.remove('d-lg-block');
        }
        function pedestal_status_modal_close() {
            document.getElementById('modal_window').style.display = 'none';
            document.getElementById('modal_body').innerHTML = '{{ ucfirst(__('berths/map.no_data_available_for_pedestal_modal_msg')) }}';
        }
        function pedestal_status_modal_is_open() {
            return(document.getElementById('modal_window').style.display === 'inline');
        }

        function pedestal_status_modal_show(pedestal_id) {
            var modal_title = '{{ ucfirst(__('berths/map.pedestal')) }}:' + ' ' + pedestals.find(x => x.id === pedestal_id).name;
            var pedestal = pedestals.filter(pedestal => pedestal.id === pedestal_id, pedestal_id)[0];
            if(pedestal !== null) {
                var berths_found = 0;
                pedestal.berths.forEach(function(current_berth) {
                <?php /* scorro tutti i posti barca della colonnina sulla quale l'utente ha cliccato: */ ?>
                    if(berths_found === 0) {
                    <?php /* se il posto barca corrente è il primo che incontro per questa colonnina */ ?>
                        <?php /* elimino dal modal il contenuto rimasto dopo l'ultimo click su una colonnina: */ ?>
                        document.getElementById('modal_body').innerHTML = '';
                    } else {
                    <?php /* se il posto barca corrente NON E' il primo che incontro per questa colonnina */ ?>
                        <?php /* aggiungo un <br> per distanziarlo da quello precedente: */ ?>
                        document.getElementById('modal_body').appendChild(document.createElement('br'));
                    }
                    berths_found++;
                    <?php /* inizio ad inserire i dati nel modal: */ ?>
                    <?php /* nome del posto barca: */ ?>
                    berth_name_caption_node = document.createElement('span');
                    berth_name_caption_node.setAttribute('id', 'berth_caption[' + current_berth.id + ']');
                    bold_node = document.createElement('b');
                    bold_node.innerHTML = '{{ ucfirst(__('berths/map.berth_caption')) }}: ';
                    berth_name_caption_node.appendChild(bold_node);
                    document.getElementById('modal_body').appendChild(berth_name_caption_node);
                    berth_name_value_node = document.createElement('span');
                    berth_name_value_node.setAttribute('id', 'berth_value[' + current_berth.id + ']');
                    if((current_berth.description) && (current_berth.description.length > 0)) {
                        berth_name_value_node.innerHTML = current_berth.description;
                    } else {
                        berth_name_value_node.innerHTML = current_berth.pontoon_name + ' ' + current_berth.berth_name;
                    }
                    document.getElementById('modal_body').appendChild(berth_name_value_node);
                    document.getElementById('modal_body').appendChild(document.createElement('br'));
                    <?php /* tipo della presa: */ ?>
                    @if(count($available_socket_types) > 1)
                    <?php // mostro il tipo di presa solamente se nell'impianto ci sono prese di più tipi diversi: ?>
                        socket_type_caption_node = document.createElement('span');
                        socket_type_caption_node.setAttribute('id', 'socket_type_caption[' + current_berth.id + ']');
                        bold_node = document.createElement('b');
                        bold_node.innerHTML = '{{ ucfirst(__('berths/map.socket_type_caption')) }}: ';
                        socket_type_caption_node.appendChild(bold_node);
                        document.getElementById('modal_body').appendChild(socket_type_caption_node);
                        socket_type_value_node = document.createElement('span');
                        socket_type_value_node.setAttribute('id', 'socket_type_value[' + current_berth.id + ']');
                        if(allowed_socket_types.includes(current_berth.socket_type) && (current_berth.socket_type !== '{{ Berth::SOCKET_TYPE_NOT_DEFINED }}')) {
                            socket_type_value_node.innerHTML = current_berth.socket_type;
                        } else {
                            socket_type_value_node.innerHTML = '{{ strtoupper(__('common.socket_type_not_set')) }}';
                            <?php /* era 'socket_type_value_node.classList.add('text-warning');' ma il text-warning è troppo chiaro, lo scurisco un po': */ ?>
                            socket_type_value_node.style.color = '#e68a00';
                        }
                        document.getElementById('modal_body').appendChild(socket_type_value_node);
                        document.getElementById('modal_body').appendChild(document.createElement('br'));
                    @endif
                    if(current_berth.last_snapshot != undefined) {
                    <?php /* se ho informazioni riguardo l ultimo stato della presa corrente: */ ?>
                        <?php /* stato comunicazione: */ ?>
                        link_status_caption_node = document.createElement('span');
                        link_status_caption_node.setAttribute('id', 'link_status_caption[' + current_berth.id + ']');
                        bold_node = document.createElement('b');
                        bold_node.innerHTML = '{{ ucfirst(__('berths/map.linkstatus_caption')) }}: ';
                        link_status_caption_node.appendChild(bold_node);
                        document.getElementById('modal_body').appendChild(link_status_caption_node);
                        link_status_value_node = document.createElement('span');
                        link_status_value_node.setAttribute('id', 'link_status_value[' + current_berth.id + ']');
                        switch(current_berth.last_snapshot.link_status) {
                            case '{{ SocketSnapshot::LINK_STATUS_CONNECTED }}':
                                link_status_value_node.innerHTML = '{{ strtoupper(__('common.socket_link_status_connected')) }}';
                                link_status_value_node.classList.add('text-success');
                                break;
                            case '{{ SocketSnapshot::LINK_STATUS_NOT_CONNECTED }}':
                                link_status_value_node.innerHTML = '{{ strtoupper(__('common.socket_link_status_not_connected')) }}';
                                link_status_value_node.classList.add('text-danger');
                                break;
                            case '{{ SocketSnapshot::LINK_STATUS_THEFT_ALARM }}':
                                link_status_value_node.innerHTML = '{{ strtoupper(__('common.socket_link_status_theft_alarm')) }}';
                                <?php /* era 'link_status_value_node.classList.add('text-warning');' ma il text-warning è troppo chiaro, lo scurisco un po' */ ?>
                                link_status_value_node.style.color = '#e68a00';
                                break;
                            default:
                                link_status_value_node.innerHTML = current_berth.last_snapshot.link_status;     <?php /* TODO: tradurre gli stati rimanenti? vedere sul sorgente di smartview se ce ne sono: forse per il linkstatus sono solo questi tre, è per l'on_off_status che ci sono gli altri */ ?>
                                <?php /* era 'link_status_value_node.classList.add('text-warning');' ma il text-warning è troppo chiaro, lo scurisco un po' */ ?>
                                link_status_value_node.style.color = '#e68a00';
                        }
                        document.getElementById('modal_body').appendChild(link_status_value_node);
                        document.getElementById('modal_body').appendChild(document.createElement('br'));
                        if(current_berth.last_snapshot.link_status.toUpperCase() !== '{{ SocketSnapshot::LINK_STATUS_NOT_CONNECTED }}') {
                        <?php /* se la presa non è in errore comunicazione: */ ?>
                            <?php /* visualizzo le informazioni sullo stato on/off, sui contatori di consumo, sul consumo istantaneo, sul credito residuo e sull'utente connesso: */ ?>
                            <?php /* stato on/off: */ ?>
                            on_off_status_caption_node = document.createElement('span');
                            on_off_status_caption_node.setAttribute('id', 'on_off_status_caption[' + current_berth.id + ']');
                            bold_node = document.createElement('b');
                            bold_node.innerHTML = '{{ ucfirst(__('berths/map.onoffstatus_caption')) }}: ';
                            on_off_status_caption_node.appendChild(bold_node);
                            document.getElementById('modal_body').appendChild(on_off_status_caption_node);
                            on_off_status_value_node = document.createElement('span');
                            on_off_status_value_node.setAttribute('id', 'on_off_status_value[' + current_berth.id + ']');
                            switch(String((current_berth.last_snapshot.on_off_status).toUpperCase())) {     <?php /* TODO: ho usato String() perchè non riesco a tradurre lo stato 'ERR' (non entra nel case, vedere qui https://stackoverflow.com/questions/14910760/switch-case-as-string), ma anche così non va. come fare? */  ?>
                                case '{{ SocketSnapshot::SOCKET_STATUS_ON }}':
                                    on_off_status_value_node.innerHTML = '{{ strtoupper(__('common.socket_status_on')) }}';
                                    on_off_status_value_node.classList.add('text-success');
                                    break;
                                case '{{ SocketSnapshot::SOCKET_STATUS_OFF }}':
                                    on_off_status_value_node.innerHTML = '{{ strtoupper(__('common.socket_status_off')) }}';
                                    on_off_status_value_node.classList.add('text-danger');
                                case '{{ SocketSnapshot::SOCKET_STATUS_NOT_CONNECTED }}':
                                    on_off_status_value_node.innerHTML = '{{ strtoupper(__('common.socket_status_not_connected')) }}';
                                    <?php /* era 'on_off_status_value_node.classList.add('text-warning');' ma il text-warning è troppo chiaro, lo scurisco un po' */ ?>
                                    on_off_status_value_node.style.color = '#e68a00';
                                <?php /* TODO: gestire anche gli altri socket status (vedere le costanti definite nel model SocketSnapshot)? O lasciarli tutti gialli? */ ?>
                                default:
                                    on_off_status_value_node.innerHTML = current_berth.last_snapshot.on_off_status;     <?php /* TODO: tradurre ciascuno stato? */ ?>
                                    on_off_status_value_node.style.color = '#e68a00';
                            }
                            document.getElementById('modal_body').appendChild(on_off_status_value_node);
                            document.getElementById('modal_body').appendChild(document.createElement('br'));
                            <?php /* contatore consumo (totale) energia: */ ?>
                            total_power_consumption_caption_node = document.createElement('span');
                            total_power_consumption_caption_node.setAttribute('id', 'power_consumption_total_cnt_caption[' + current_berth.id + ']');
                            bold_node = document.createElement('b');
                            bold_node.innerHTML = '{{ ucfirst(__('berths/map.socket_total_power_counter_caption')) }}: ';
                            total_power_consumption_caption_node.appendChild(bold_node);
                            document.getElementById('modal_body').appendChild(total_power_consumption_caption_node);
                            total_power_consumption_value_node = document.createElement('span');
                            total_power_consumption_value_node.setAttribute('id', 'power_consumption_total_cnt_value[' + current_berth.id + ']');
                            total_power_consumption_value_node.innerHTML = parseFloat(current_berth.last_snapshot.total_power_cnt).toFixed(1) + ' kWh';
                            document.getElementById('modal_body').appendChild(total_power_consumption_value_node);
                            document.getElementById('modal_body').appendChild(document.createElement('br'));
                            <?php /* contatore consumo (postpagato) energia: */ ?>
                            postpaid_power_consumption_caption_node = document.createElement('span');
                            postpaid_power_consumption_caption_node.setAttribute('id', 'power_consumption_postpaid_cnt_caption[' + current_berth.id + ']');
                            bold_node = document.createElement('b');
                            bold_node.innerHTML = '{{ ucfirst(__('berths/map.socket_postpaid_power_counter_caption')) }}: ';
                            postpaid_power_consumption_caption_node.appendChild(bold_node);
                            document.getElementById('modal_body').appendChild(postpaid_power_consumption_caption_node);
                            postpaid_power_consumption_value_node = document.createElement('span');
                            postpaid_power_consumption_value_node.setAttribute('id', 'power_consumption_postpaid_cnt_value[' + current_berth.id + ']');
                            postpaid_power_consumption_value_node.innerHTML = parseFloat(current_berth.last_snapshot.postpaid_power_cnt).toFixed(1) + ' kWh';
                            document.getElementById('modal_body').appendChild(postpaid_power_consumption_value_node);
                            document.getElementById('modal_body').appendChild(document.createElement('br'));
                            <?php /* consumo energia istantaneo: */ ?>
                            instant_power_consumption_caption_node = document.createElement('span');
                            instant_power_consumption_caption_node.setAttribute('id', 'power_consumption_cnt_caption[' + current_berth.id + ']');
                            bold_node = document.createElement('b');
                            bold_node.innerHTML = '{{ ucfirst(__('berths/map.instantpowerconsumption_caption')) }}: ';
                            instant_power_consumption_caption_node.appendChild(bold_node);
                            document.getElementById('modal_body').appendChild(instant_power_consumption_caption_node);
                            instant_power_consumption_value_node = document.createElement('span');
                            instant_power_consumption_value_node.setAttribute('id', 'instant_power_consumption_value[' + current_berth.id + ']');
                            instant_power_consumption_value_node.innerHTML = parseFloat(current_berth.last_snapshot.instant_power_consumption).toFixed(3) + ' kW';
                            document.getElementById('modal_body').appendChild(instant_power_consumption_value_node);
                            document.getElementById('modal_body').appendChild(document.createElement('br'));
                            <?php /* contatore consumo (totale) acqua: */ ?>
                            total_water_consumption_caption_node = document.createElement('span');
                            total_water_consumption_caption_node.setAttribute('id', 'water_consumption_total_cnt_caption[' + current_berth.id + ']');
                            bold_node = document.createElement('b');
                            bold_node.innerHTML = '{{ ucfirst(__('berths/map.socket_total_water_counter_caption')) }}: ';
                            total_water_consumption_caption_node.appendChild(bold_node);
                            document.getElementById('modal_body').appendChild(total_water_consumption_caption_node);
                            total_water_consumption_value_node = document.createElement('span');
                            total_water_consumption_value_node.setAttribute('id', 'water_consumption_total_cnt_value[' + current_berth.id + ']');
                            total_water_consumption_value_node.innerHTML = parseFloat(current_berth.last_snapshot.total_water_cnt).toFixed(3) + ' m<sup>3</sup>';
                            document.getElementById('modal_body').appendChild(total_water_consumption_value_node);
                            document.getElementById('modal_body').appendChild(document.createElement('br'));
                            <?php /* contatore consumo (postpagato) acqua: */ ?>
                            postpaid_water_consumption_caption_node = document.createElement('span');
                            postpaid_water_consumption_caption_node.setAttribute('id', 'water_consumption_postpaid_cnt_caption[' + current_berth.id + ']');
                            bold_node = document.createElement('b');
                            bold_node.innerHTML = '{{ ucfirst(__('berths/map.socket_postpaid_water_counter_caption')) }}: ';
                            postpaid_water_consumption_caption_node.appendChild(bold_node);
                            document.getElementById('modal_body').appendChild(postpaid_water_consumption_caption_node);
                            postpaid_water_consumption_value_node = document.createElement('span');
                            postpaid_water_consumption_value_node.setAttribute('id', 'water_consumption_postpaid_cnt_value[' + current_berth.id + ']');
                            postpaid_water_consumption_value_node.innerHTML = parseFloat(current_berth.last_snapshot.postpaid_water_cnt).toFixed(3) + ' m<sup>3</sup>';
                            document.getElementById('modal_body').appendChild(postpaid_water_consumption_value_node);
                            document.getElementById('modal_body').appendChild(document.createElement('br'));
                            <?php /* credito residuo sulla presa. Lo visualizzo solo se la presa è accesa: */ ?>
                            if(current_berth.last_snapshot.on_off_status.toUpperCase() !== '{{ SocketSnapshot::SOCKET_STATUS_OFF }}') {
                                residual_credit_caption_node = document.createElement('span');
                                residual_credit_caption_node.setAttribute('id', 'residual_credit_caption[' + current_berth.id + ']');
                                bold_node = document.createElement('b');
                                bold_node.innerHTML = '{{ ucfirst(__('berths/map.residualcredit_caption')) }}: ';
                                residual_credit_caption_node.appendChild(bold_node);
                                document.getElementById('modal_body').appendChild(residual_credit_caption_node);
                                residual_credit_value_node = document.createElement('span');
                                residual_credit_value_node.setAttribute('id', 'residual_credit_value[' + current_berth.id + ']');
                                if(parseFloat(current_berth.last_snapshot.residual_credit) === 0) {
                                <?php /* se il credito residuo è zero, dato che la presa è accesa, visualizzo l'indicatore di postpagato */ ?>
                                    postpaid_user_span = document.createElement('span');
                                    <?php /* colore del testo dello span che conterrà l'indicatore di postpagato: */ ?>
                                    postpaid_user_span.classList.add('text-primary');
                                    <?php /* padding dello span che conterrà l'indicatore di postpagato: */ ?>
                                    postpaid_user_span.style.padding = '0.13vh';
                                    <?php /* margin-left dello span che conterrà l'indicatore di postpagato: */ ?>
                                    postpaid_user_span.classList.add('ml-1');
                                    <?php /* in questo modo non vado a capo a metà dello span dell'indicatore di postpagato: */ ?>
                                    postpaid_user_span.classList.add('text-nowrap');
                                    <?php /* testo dello span dell'indicatore di postpagato: */ ?>
                                    postpaid_user_span.innerHTML = '{{ strtoupper(__('berths/map.postpaid_user_span_caption')) }}';
                                    <?php /* accodo lo span dello username alle altre info sull'utente: */ ?>
                                    residual_credit_value_node.appendChild(postpaid_user_span);
                                } else {
                                <?php /* se il credito residuo è !== 0, il transponder è prepagato, perciò lo mostro: */ ?>
                                    residual_credit_value_node.innerHTML = parseFloat(current_berth.last_snapshot.residual_credit).toFixed(2) + ' {{ config('dates_currency_and_locale.currency_character') }}';
                                }
                                document.getElementById('modal_body').appendChild(residual_credit_value_node);
                                document.getElementById('modal_body').appendChild(document.createElement('br'));
                            }
                            if((current_berth.last_snapshot.on_off_status !== '{{ SocketSnapshot::SOCKET_STATUS_NOT_CONNECTED }}') && <?php /* TODO: DISAB. e FORCED cosa rappresentano? Vedere se serve visualizzare l'utente in quei casi */ ?>
                               (current_berth.last_snapshot.on_off_status !== '{{ SocketSnapshot::SOCKET_STATUS_DISABLED }}') &&
                               (current_berth.last_snapshot.on_off_status !== '{{ SocketSnapshot::SOCKET_STATUS_FORCED }}')) {
                                <?php /* aggiungo le informazioni riguardanti l'ultimo utente connesso alla presa/l'utente attuale: */ ?>
                                socket_user_caption_node = document.createElement('span');
                                socket_user_caption_node.setAttribute('id', 'socket_user_caption[' + current_berth.id + ']');
                                bold_node = document.createElement('b');
                                switch(String((current_berth.last_snapshot.on_off_status).toUpperCase())) {
                                    case '{{ SocketSnapshot::SOCKET_STATUS_ON }}':
                                    <?php /* se la presa è attualmente accesa, scrivo 'current user': */ ?>
                                        bold_node.innerHTML = '{{ ucfirst(__('common.current_user_on_socket_caption')) }}: ';
                                        break;
                                    case '{{ SocketSnapshot::SOCKET_STATUS_OFF }}':
                                    default:
                                    <?php /* se la presa è attualmente spenta o in uno degli altri stati (ON_T, OFF_T, etc.), scrivo 'last user': */ ?>
                                        bold_node.innerHTML = '{{ ucfirst(__('common.last_user_on_socket_caption')) }}: ';
                                }
                                socket_user_caption_node.appendChild(bold_node);
                                document.getElementById('modal_body').appendChild(socket_user_caption_node);
                                // istanzio lo span contenente il nome dell'utente:
                                socket_user_value_node = document.createElement('span');
                                socket_user_value_node.setAttribute('id', 'socket_user_value[' + current_berth.id + ']');
                                // inizializzandolo ad utente sconosciuto per default:
                                socket_user_value_node.innerHTML = '{{ strtoupper(__('common.unknown_user')) }}';
                                socket_user_value_node.classList.add('text-danger');
                                var connected_subscription;
                                var boat_found = false;
                                if(current_berth.last_snapshot.transponder_id_field_is_reliable === true) {
                                <?php /* verifico se il valore transponder_id dello snapshot può darmi informazioni affidabili sul contratto eventualmente collegato e sul suo intestatario: */ ?>
                                    <?php /* se sì: */ ?>
                                    if((current_berth.last_snapshot.transponder) &&
                                       ((current_berth.last_snapshot.transponder.active_subscriptions) && (current_berth.last_snapshot.transponder.active_subscriptions.length > 0))) {
                                    <?php /* verifico se il transponder esiste in anagrafica e se ha almeno un contratto legato a sè: */ ?>
                                        <?php /* se sì: */ ?>
                                        if((current_berth.last_snapshot.transponder.used_for_socket_subscriptions) && (current_berth.last_snapshot.transponder.used_for_socket_subscriptions > 0)) {
                                        <?php /* verifico se il transponder è usato (ANCHE) per le socket subscriptions: */ ?>
                                            <?php /* se sì: */ ?>
                                            connected_subscription = current_berth.last_snapshot.transponder.active_subscriptions.filter(function(current_subscription) {
                                            <?php /* il contratto che mi interessa sarà quello che ha il campo socket_id uguale all'id del posto barca attuale (se esiste): */ ?>
                                                return current_subscription.socket_id === current_berth.id;
                                            })[0];
                                        } 
                                        if((typeof connected_subscription) === "undefined") {
                                        <?php /* se il transponder è usato (ANCHE) per le socket subscriptions ma non ho trovato tra i suoi contratti nessun contratto legato al posto barca, oppure se il transponder NON è usato per le socket subscriptions: */ ?>
                                            <?php /* il contratto che mi interessa sarà l'unico contratto presente nell'array active_subscriptions: */ ?>
                                            connected_subscription = current_berth.last_snapshot.transponder.active_subscriptions[0];
                                        }
                                        if(((typeof connected_subscription) !== "undefined") && (connected_subscription.user)) {
                                        <?php /* ora, se ho trovato il contratto, verifico se in anagrafica esiste un utente ad esso collegato: */ ?>
                                            <?php /* se sì: */ ?>
                                            <?php /* tolgo allo span contenente il nome dell'utente la classe che l'avrebbe reso rosso in caso di utente sconosciuto: */ ?>
                                            socket_user_value_node.classList.remove('text-danger');
                                            <?php /* ci scrivo il cognome ed il nome dell'utente: */ ?>
                                            socket_user_value_node.innerHTML = connected_subscription.user.last_name + ' ' + connected_subscription.user.first_name;
                                            if(connected_subscription.user.username !== null) {
                                            <?php /* se l'utente ha anche uno username: */ ?>
                                            <?php /* istanzio lo span che conterrà l'intera info dello username: */ ?>
                                                username_span = document.createElement('span');
                                                <?php /* colore del testo dello span che conterrà l'intera info dello username: */ ?>
                                                username_span.classList.add('text-primary');
                                                <?php /* padding dello span che conterrà l'intera info dello username: */ ?>
                                                username_span.style.padding = '0.13vh';
                                                <?php /* margin-left dello span che conterrà l'intera info dello username: */ ?>
                                                username_span.classList.add('ml-1');
                                                <?php /* in questo modo non vado a capo a metà dello span dello username: */ ?>
                                                username_span.classList.add('text-nowrap');
                                                <?php /* setto lo username dell'utente come testo dello span: */ ?>
                                                username_span_text = connected_subscription.user.username;
                                                username_span.innerHTML = username_span_text;
                                                <?php /* aggiungo l'icona della nuvola: */ ?>
                                                cloud_icon = document.createElement('i');
                                                cloud_icon.classList.add('fas');
                                                cloud_icon.classList.add('fa-cloud');
                                                <?php /* distanzio l'icona della nuvola dal border dello span dello username: */ ?>
                                                cloud_icon.style.margin = '0.4vh';
                                                username_span.appendChild(cloud_icon);
                                                <?php /* aggiungo il border allo span dello username: */ ?>
                                                username_span.classList.add('border');
                                                username_span.classList.add('border-primary');
                                                username_span.classList.add('rounded');
                                                <?php /* accodo lo span dello username alle altre info sull'utente: */ ?>
                                                socket_user_value_node.appendChild(username_span);
                                            }
                                            if((typeof connected_subscription.user.boat !== 'undefined') && (connected_subscription.user.boat !== null)) {
                                            <?php /* se l'utente ha anche una barca collegata: */ ?>
                                                boat_found = true;
                                                <?php /* aggiungo le sue info al modal: */ ?>
                                                boat_name_caption_node = document.createElement('span');
                                                boat_name_caption_node.setAttribute('id', 'socket_user_boat_caption[' + current_berth.id + ']');
                                                bold_node = document.createElement('b');
                                                bold_node.innerHTML = '{{ ucfirst(__('common.boat')) }}: ';
                                                boat_name_caption_node.appendChild(bold_node);
                                                /*document.getElementById('modal_body').appendChild(boat_name_caption_node);*/
                                                boat_name_value_node = document.createElement('span');
                                                boat_name_value_node.setAttribute('id', 'socket_user_boat_value[' + current_berth.id + ']');
                                                boat_name_value_node.innerHTML = connected_subscription.user.boat.name;
                                                if((connected_subscription.user.boat.length > 0) || (connected_subscription.user.boat.width > 0) || (connected_subscription.user.boat.depth > 0)) {
                                                <?php /* misure della barca: */ ?>
                                                    boat_name_value_node.innerHTML += ' ' + '({{ strtolower(__('common.boat_measurements_tooltip_caption')) }})';
                                                    <?php /* non posso passare l'array delle misure della barca direttamente al metodo di Laravel che ne fa il replace nella stringa tradotta, perciò le inserisco con js: */ ?>
                                                    boat_name_value_node.innerHTML = boat_name_value_node.innerHTML.replace(':length', connected_subscription.user.boat.length);
                                                    boat_name_value_node.innerHTML = boat_name_value_node.innerHTML.replace(':width', connected_subscription.user.boat.width);
                                                    boat_name_value_node.innerHTML = boat_name_value_node.innerHTML.replace(':depth', connected_subscription.user.boat.depth);
                                                }
                                            }
                                        }
                                    }
                                }
                                <?php /* appendo il nodo con il nome dell'utente o la caption di utente sconosciuto: */ ?>
                                document.getElementById('modal_body').appendChild(socket_user_value_node);
                                document.getElementById('modal_body').appendChild(document.createElement('br'));
                                if(boat_found === true) {
                                <?php /* e - se ho trovato una barca collegata all'utente, appendo i nodi con la caption del nome della barca ed il nome stesso: */ ?>
                                    document.getElementById('modal_body').appendChild(boat_name_caption_node);
                                    document.getElementById('modal_body').appendChild(boat_name_value_node);
                                    document.getElementById('modal_body').appendChild(document.createElement('br'));
                                }
                            }
                        }
                        <?php /* ultimo aggiornamento ricevuto dalla presa: */ ?>
                        last_update_timestamp_caption_node = document.createElement('span');
                        bold_node = document.createElement('b');
                        bold_node.innerHTML = '{{ ucfirst(__('berths/map.lastupdatetimestamp_caption')) }}: ';
                        last_update_timestamp_caption_node.appendChild(bold_node);
                        document.getElementById('modal_body').appendChild(last_update_timestamp_caption_node);
                        last_update_timestamp_value_node = document.createElement('span');
                        var snapshot_timestamp = moment.utc(current_berth.last_snapshot.timestamp).tz("{{ config('dates_currency_and_locale.local_timezone') }}");
                        var snapshot_timestamp_formatted = snapshot_timestamp.format('{{ config('dates_currency_and_locale.local_datetime_format_full_momentjs') }}');
                        last_update_timestamp_value_node.setAttribute('id', 'timestamp_value[' + current_berth.id + ']');
                        last_update_timestamp_value_node.innerHTML = snapshot_timestamp_formatted;
                        document.getElementById('modal_body').appendChild(last_update_timestamp_value_node);
                        document.getElementById('modal_body').appendChild(document.createElement('br'));
                    } else {
                    <?php /* se non sono disponibili informazioni riguardo l'ultimo stato della presa corrente: */ ?>
                        var no_status_available_node = document.createElement('span');
                        no_status_available_node.setAttribute('id', 'no_info_available_about_pedestal[' + pedestal_id + ']');
                        no_status_available_node.innerHTML = '{{ ucfirst(__('berths/map.no_status_information_available_for_this_berth')) }}';
                        no_status_available_node.classList.add('text-danger');
                        document.getElementById('modal_body').appendChild(no_status_available_node);
                    }
                });         /* fine del foreach sui posti barca (current_berth) della colonnina cliccata */
                document.getElementById('modal_body').appendChild(document.createElement('br'));
                document.getElementById('modal_title').innerHTML = modal_title;
                document.getElementById('modal_window').style.display = 'inline';
                window.onclick = function(event) {
                /* When the user clicks anywhere outside of the modal, close it */
                    if(event.target == document.getElementById('modal_window')) {
                        pedestal_status_modal_close();
                    }
                }
            }
        }
        window.onload = function() {
            document.getElementById('modal_close_btt').onclick = function() {
                pedestal_status_modal_close();
            }
             document.getElementById('map_zoom_guidelines_container_for_desktop_close_btt').onclick = function() {
                hide_map_zoom_guidelines_container_for_desktop();
            }
            document.onkeydown = function(evt) {
                evt = evt || window.event;
                if(evt.keyCode == 27) {
                    if(pedestal_status_modal_is_open()) {
                        pedestal_status_modal_close();
                    }
                }
            }
            <?php foreach($pedestals as $pedestal): ?>
                document.getElementById('pedestal_link_{{ $pedestal->get_id() }}').onclick = function() {
                    pedestal_status_modal_show({{ $pedestal->get_id() }});
                }
            <?php endforeach; ?>
        }
    </script>
@endsection

@section('content')
    <div id='content' class='mt-2 pt-2'>
        <div id='map_zoom_guidelines_container_for_desktop' class='position-fixed d-none d-lg-block p-3 bg-secondary text-white rounded'>
        <?php /* div contenente le istruzioni per l'uso della mappa, visibile solo da desktop: */ ?>
            <span id='map_zoom_guidelines_container_for_desktop_close_btt' class='float-right'>
                <i class='fas fa-times'></i>
            </span>
            <span class='d-block'>{{ ucfirst(__('berths/map.desktop_guidelines_move_with_arrow_keys')) }}</span>
            <span class='d-block'>{{ ucfirst(__('berths/map.desktop_guidelines_press_plus_key_to_zoom_in')) }}</span>
            <span class='d-block'>{{ ucfirst(__('berths/map.desktop_guidelines_press_minus_key_to_zoom_out')) }}</span>
        </div>
        <?php /* TODO: sostituire questo modal con un modal di bootstrap come quello della navbar con la versione? (v. https://getbootstrap.com/docs/4.4/components/modal/) */ ?>
        <div class='container-fluid'>
            <div class='row'>
                <div id='modal_window'>
                    <div id='modal_content' class='col-md-12 col-lg-10'>
                    <?php /* TODO: se ci sono pochi dati da visualizzare, su mobile il modal copre solo metà schermo. Vorrei che fosse a full height in ogni caso. Risolvere. */ ?>
                        <div id='modal_heading'>
                            <span id='modal_close_btt'>
                                <i class='fas fa-times'></i>
                            </span>
                            <h2 id='modal_title'>&nbsp;</h2>
                        </div>
                        <div id='modal_body'>
                            {{ ucfirst(__('berths/map.no_data_available_for_pedestal_modal_msg')) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{ $map_img_url }}" id='map_img' />
        <map name='map'>
            @foreach($pedestals as $pedestal)
                <?php /* per ciascuna colonnina: */
                    $pedestal_marker_img_filename = 'map_pedestal_marker_yellow.png';   /* setto inizialmente il marker giallo */
                    $pedestal_berths = $pedestal->berths;
                    foreach($pedestal_berths as $berth) {
                    /* scorro tutti i posti barca: */
                        if(($berth->last_snapshot) && ($berth->last_snapshot()->count() > 0)) {
                        /* se esiste almeno uno snapshot per il posto barca corrente: */
                            if($pedestal_marker_img_filename === 'map_pedestal_marker_yellow.png') {
                            /* se questo è il primo snapshot che incontro per le prese di questa colonnina: */
                                /* setto il marker verde: */
                                $pedestal_marker_img_filename = 'map_pedestal_marker_green.png';
                            }
                            if($berth->last_snapshot->get_link_status() === SocketSnapshot::LINK_STATUS_NOT_CONNECTED) {
                            /* ma se la presa sul posto barca corrente non sta comunicando: */
                                /* il marker di questa colonnina sarà rosso, per segnalare un problema su uno delle sue prese: */
                                $pedestal_marker_img_filename = 'map_pedestal_marker_red.png';
                                /* TODO: visualizzare anche altri allarmi sulla mappa, come l'allarme furto o eventuali allarmi rivolti all'utente (max consumo, max tempo acqua, min credito...)? per ora non lo faccio */
                            }
                        }
                    }
                ?>
                <a id='pedestal_link_{{ $pedestal['id'] }}' class='point pedestal{{ $pedestal['id'] }}'>
                    <img src="{{ URL::asset('resources').'/'.$pedestal_marker_img_filename }}" class='pedestal_marker position-absolute'
                            id='pedestal_marker_{{ $pedestal['id'] }}'>
                </a>
            @endforeach
        </map>
        <?php /* TODO: la mappa va caricata in png e non in jpg */ ?>

        <div id='map_zoom_buttonbar_container_for_mobile' class="btn-group-vertical position-fixed d-lg-none">
        <?php // div contenente i controlli per lo zoom della mappa, visibile solo da mobile: ?>
            <button type="button" id='map_zoom_in_btn' class="btn btn-secondary mb-2">
                <i class="fas fa-plus-square"></i>
            </button>
            <button type="button" id="map_zoom_out_btn" class="btn btn-secondary">
                <i class="fas fa-minus-square"></i>
            </button>
        </div>
    </div>
@endsection
