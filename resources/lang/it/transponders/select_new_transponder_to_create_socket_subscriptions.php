<?php

return [
    'last_subscription_activated_on_date' => 'il',
    'last_subscription_activated_with_transponder' => 'ultimo contratto postpagato attivato',
    'select_a_transponder_no_option_selected_caption' => 'selezionare un transponder postpagato',
    'select_a_transponder_no_transponders_found' => 'non sono stati trovati altri transponder postpagati',    // TODO: nella translation, spiegare come procedere per aggiungerne
    'submit_btt_caption' => 'questo transponder postpagato verrà usato per attivare contratti postpagati su presa',
    'username' => 'username',
];