    
<?php

return [
    'min_power_threshold_um_caption' => 'soglia minima di consumo energia (:um)',
    'max_power_threshold_um_caption' => 'soglia massima di consumo energia (:um)',
    'min_credit_threshold_currency_caption' => 'soglia minima di credito (:currency)',
    'max_water_tap_time_threshold_minutes_caption' => 'tempo massimo di apertura acqua (minuti)',
    'submit_btt_caption' => 'modifica soglie',
    'submit_confirmation_request' => 'confermare queste soglie?',
];