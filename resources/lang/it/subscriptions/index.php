<?php

return [
    'combo_filter_active_subscriptions_only_option_caption' => 'solo i contratti in corso',
    'combo_filter_all_subscriptions_option_caption' => 'tutti i contratti',
    'combo_filter_ended_subscriptions_only_option_caption' => 'contratti terminati',
    'fulltext_filter_caption' => 'cerca contratti',
    'no_subscriptions_on_database_or_no_subscription_matches_search_criteria' => 'nessun contratto corrisponde ai criteri di ricerca, oppure nessun contratto presente sul database.',
    'subscription_successfully_closed_msg' => 'contratto terminato con successo',
    'subscription_successfully_created_msg' => 'contratto creato con successo',
    'subscription_successfully_updated_msg' => 'contratto modificato con successo',
    'th_berth_name' => 'posto barca',
    'th_begin_date' => 'data inizio',
    'th_boat' => 'barca',
    'th_end_date' => 'data fine',
    'th_transponder' => 'transponder',
    'th_user' => 'utente',
];