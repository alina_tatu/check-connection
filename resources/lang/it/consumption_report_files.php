<?php

return [
    'berth_caption' => 'posto barca',
    'pdf_berth_consumption_title_berth_consumption_from_to' => 'report dei consumi del posto barca [:berth_name] dal :begin_timestamp_formatted al :end_timestamp_formatted',
    'pdf_page_title' => 'report di consumo',
    'pdf_plant_consumption_title_plant_consumption_from_to' => 'report dei consumi del marina dal :begin_timestamp_formatted al :end_timestamp_formatted',
    'pdf_pontoon_consumption_title_pontoon_consumption_from_to' => 'report dei consumi del pontile [:pontoon_name] dal :begin_timestamp_formatted al :end_timestamp_formatted',
    'pdf_subscription_consumption_title_subscription_consumption_from_to' => 'report dei consumi del contratto :subscription_description dal :begin_timestamp_formatted al :end_timestamp_formatted',
    'pdf_title_subscription_description_with_berth_caption' => '[:user_name] sul posto barca [:berth_name]',
    'pdf_title_subscription_description_without_berth_caption' => '[:user_name]',
    'power_consumption_caption' => 'energia',
    'total_consumption_caption' => 'consumo totale',
    'total_power_consumption_caption' => 'consumo di energia totale',
    'total_water_consumption_caption' => 'consumo di acqua totale',
    'water_consumption_caption' => 'acqua',
    'xls_title_for_berth_part_1' => 'report di consumo per il posto barca',
    'xls_title_for_marina_part_1' => 'report dei consumi del marina',
    'xls_title_for_pontoon_part_1' => 'report dei consumi del pontile',
    'xls_title_for_subscription_part_1' => 'report dei consumi del contratto',
    'xls_title_from_part' => 'dal',
    'xls_title_to_part' => 'al',
];