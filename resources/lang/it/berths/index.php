<?php

return [
    'boat_measurements_tooltip' => 'lunghezza: :length, larghezza: :width, pescaggio: :depth',
    'check_subscription_consumption_btt_caption' => 'controllo consumi',
    'combo_filter_all_berths_option_caption' => 'tutti i posti barca',
    'combo_filter_assigned_berths_only_option_caption' => 'solo i posti barca attualmente occupati',
    'combo_filter_available_berths_only_option_caption' => 'solo i posti barca attualmente disponibili',
    'currently_unassigned_berth_caption' => 'non assegnato',
    'fulltext_filter_caption' => 'cerca posti barca',
    'no_berths_on_database_or_no_berth_matches_search_criteria' => 'nessun posto barca corrisponde ai criteri di ricerca, oppure nessun posto barca presente sul database.',
    'start_new_subscription_btt_caption' => 'nuovo contratto',
    'th_begin_date' => 'data inizio',
    'th_berth_name' => 'berth',
    'th_boat' => 'barca',
    'th_socket_type' => 'tipo presa',
    'th_user' => 'utente',
    'th_end_date' => 'data fine',
];