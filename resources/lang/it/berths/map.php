<?php

return [
    'berth_caption' => 'posto barca',
    'desktop_guidelines_move_with_arrow_keys' => 'spostarsi nella mappa usando le frecce direzionali, e cliccare sui marker per vedere lo stato delle colonnine',
    'desktop_guidelines_press_plus_key_to_zoom_in' => 'premere + per aumentare lo zoom',
    'desktop_guidelines_press_minus_key_to_zoom_out' => 'premere - per diminuire lo zoom',
    'instantpowerconsumption_caption' => 'consumo energia attuale',
    'lastupdatetimestamp_caption' => 'ultimo aggiornamento',
    'linkstatus_caption' => 'stato comunicazione',
    'onoffstatus_caption' => 'stato',
    'no_berths_on_database' => 'nessun posto barca presente sul database',
    'no_data_available_for_pedestal_modal_msg' => 'nessun dato disponibile',
    'no_status_information_available_for_this_berth' => 'non sono disponibili informazioni sullo stato',
    'pedestal' => 'colonnina',
    'postpaid_user_span_caption' => 'postpagato',
    'residualcredit_caption' => 'credito residuo',
    'socket_postpaid_power_counter_caption' => 'consumo energia postpagato',
    'socket_postpaid_water_counter_caption' => 'consumo acqua postpagato',
    'socket_total_power_counter_caption' => 'consumo energia totale',
    'socket_total_water_counter_caption' => 'consumo acqua totale',
    'socket_type_caption' => 'tipo di presa',
];