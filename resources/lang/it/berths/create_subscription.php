<?php

return [
    'add_new_postpaid_transponder_used_for_socket_subscriptions_button_caption' => 'abilita un altro transponder all\'attivazione di contratti postpagati su presa',
    'begin_timestamp_field_label_caption' => 'inizio',
    'berth_name_field_label_caption' => 'posto barca',
    'notes_field_label_caption' => 'note contratto',
    'search_for_users_caption' => 'cerca utenti',
    'selected_transponder_has_been_set_to_be_used_for_socket_subscriptions_msg' => 'ora è possibile usare il transponder :code  per attivare contratti postpagati su presa',
    'socket_type_field_label_caption' => 'tipo presa',
    'submit_btt_caption' => 'crea contratto',
    'submit_confirmation_request' => 'inserire il nuovo contratto?',
    'transponder_field_label_caption' => 'transponder',
    'transponder_field_please_select_an_option_caption' => 'selezionare il transponder postpagato che verrà usato per abilitare questo contratto sul posto barca',
    'transponder_field_there_are_no_transponders_used_for_socket_subscriptions_caption' => 'non è presente nessun transponder abilitato all\'attivazione di contratti postpagati su presa. Abilitarne uno utilizzando l\'apposito pulsante.',
    'user_field_label_caption' => 'utente',
    'user_not_found_alert_msg_caption' => 'l\'utente inserito non è valido. Se si intende crearne uno nuovo, usare la sezione Utenti.',
    'username' => 'username',
];