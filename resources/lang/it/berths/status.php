<?php

return [
    'berth_caption' => 'posto barca',
    'berth_filter_caption' => 'cerca per posto barca',
    'boat_depth' => 'profondità',
    'boat_length' => 'lunghezza',
    'boat_width' => 'larghezza',
    'chart_period_select_last_24hours_caption' => 'ultime 24 ore',
    'chart_period_select_last_month_caption' => 'ultimo mese',
    'chart_period_select_last_week_caption' => 'ultima settimana',
    'chart_submit_btt_caption' => 'andamento consumi',
    'customer_filter_caption' => 'cerca per utente/barca',
    'instantpowerconsumption_caption' => 'consumo energia attuale',
    'lastupdatetimestamp_caption' => 'ultimo aggiornamento',
    'linkstatus_caption' => 'stato comunicazione',
    'no_berths_on_database_or_no_berths_match_search_criteria' => 'nessun posto barca corrisponde ai criteri di ricerca, oppure nessun posto barca presente sul database.',
    'no_status_information_available_for_this_berth' => 'non sono disponibili informazioni sullo stato',
    'onoff_btt_alert_switchoff_confirmation_request' => 'spegnere la presa sul posto barca [:berth]?',
    'onoff_btt_alert_switchon_confirmation_request' => 'accendere la presa sul posto barca [:berth]?',
    'onoff_btt_caption_socket_is_off' => 'off',
    'onoff_btt_caption_socket_is_on' => 'on',
    'onoff_btt_caption_socket_is_not_connected' => 'stato non disponibile',
    'onoff_btt_tooltip_click_to_switch_off_socket' => 'cliccare per spegnere la presa',
    'onoff_btt_tooltip_socket_is_not_responding_unable_to_control_it' => 'la presa non risponde, impossibile inviare comandi',
    'onoff_btt_tooltip_socket_switchon_is_disabled_for_security_reasons' => 'per motivi di sicurezza, non è possibile accendere le prese da remoto',
    'pontoon_filter_caption' => 'cerca per pontile',
    'pontoon_filter_no_pontoon_detected_caption' => 'nessun pontile rilevato nel marina',
    'postpaid_user_span_caption' => 'postpagato',
    'reset_btt_alert_action_confirmation_request' => 'resettare la presa sul posto barca [:berth]?',
    'reset_btt_caption' => 'reset',
    'reset_filters_btt_caption' => 'reset filtri',
    'reset_btt_tooltip_click_to_reset_socket' => 'cliccare per resettare la presa',
    'residualcredit_caption' => 'credito residuo',
    'show_hide_snapshot_div_btt_tooltip_caption' => 'mostra/nascondi lo stato',
    'socket_credit_consumption_counting_caption' => 'consumo credito',
    'socket_power_consumption_counting_caption' => 'consumo energia',
    'socket_postpaid_power_counter_caption' => 'consumo energia postpagato',
    'socket_postpaid_water_counter_caption' => 'consumo acqua postpagato',
    'socket_total_power_counter_caption' => 'consumo energia totale',
    'socket_total_water_counter_caption' => 'consumo acqua totale',
    'socket_type_caption' => 'tipo di presa',
    'socket_type_filter_caption' => 'cerca per tipo di presa',
    'socket_water_consumption_counting_caption' => 'consumo acqua',
];