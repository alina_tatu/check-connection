<?php

return [
    'back_to_previous_page_btt_caption' => 'indietro',
    'chart_for_an_unknown_berth_requested_msg' => 'hai richiesto un grafico dei consumi per un posto barca sconosciuto. Per favore, verifica e riprova.',
    'there_is_not_enough_consumption_data_to_draw_the_chart_msg_admin_caption' => ':app_name non ha ancora ricevuto dati a sufficienza per generare il grafico. Riprovare più tardi o verificare eventuali errori di comunicazione con la presa.',
    'there_is_not_enough_consumption_data_to_draw_the_chart_msg_simple_user_caption' => ':app_name non ha ancora ricevuto dati a sufficienza per generare il grafico. Riprovare più tardi.',
    'there_were_disconnections_message' => 'ATTENZIONE: durante il periodo in esame, sul posto barca si sono verificate delle disconnessioni. Pertanto non è possibile garantire l\'affidabilità dei consumi mostrati.',
    'title_berth_consumption_caption' => ':consumption_type sul posto barca :berth_name :period',
    'title_consumption_type_residual_credit_consumption_caption' => 'consumo di credito',
    'title_consumption_type_residual_credit_trend_caption' => 'andamento del credito residuo',
    'title_consumption_type_power_caption' => 'consumo medio di energia',
    'title_consumption_type_water_caption' => 'consumo medio di acqua',
    'title_period_from_to' => 'da [:begin] a [:end]',
    'title_subscription_consumption_caption' => ':consumption_type del contratto :subscription_description: :period',
    'title_subscription_description_with_berth_caption' => '[:user_name] sul posto barca [:berth_name]',
    'title_subscription_description_without_berth_caption' => '[:user_name]',
    'x_axis_time_interval_caption' => 'tempo',
    'y_axis_average_credit_consumption_caption' =>  'consumo medio di credito (:currency)',
    'y_axis_average_power_consumption_caption' => 'consumo medio di energia (kWh)',
    'y_axis_average_water_consumption_caption' => 'consumo medio di acqua (m³/h)',
    'y_axis_residual_credit_on_socket_caption' =>  'credito residuo sulla presa (:currency)',
];