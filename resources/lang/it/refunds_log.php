<?php

return [
    'filter_berth_caption' => 'cerca per posto barca',
    'filter_user_caption' => 'cerca per utente intestatario/utente richiedente',
    'no_entries_message' => 'questo log non contiene ancora informazioni',
    'th_action_type' => 'azione',
    'th_amount_to_refund' => 'credito perso',
    'th_berth' => 'posto barca',
    'th_requesting_user' => 'utente richiedente',
    'th_subscription_owner_user' => 'intestatario del contratto',
    'th_timestamp' => 'data ed ora',
];