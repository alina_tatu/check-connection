<?php

return [
    'alarm_switched_off_at' => 'allarme risolto il',
    'berth' => 'presa sul posto barca',    
    'notifications_list_heading_caption' => 'notifiche',
    'refreshing_notifications' => 'aggiornamento in corso',
    'serial_line' => 'linea seriale',
    'you_have_not_received_any_notifications_yet' => 'non hai ancora ricevuto notifiche',
];