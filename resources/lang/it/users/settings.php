<?php

return [
    'an_error_has_occured_message_caption' => 'si è verificato un errore',
    'email_address_label_caption' => 'indirizzo email',
    'email_address_is_invalid_or_already_in_use' => 'l\'indirizzo email specificato non è valido, oppure è già usato da un altro utente',
    'form_submit_confirmation_request_caption' => 'confermare le impostazioni inserite?',
    'language_preference_label_caption' => 'lingua del software',
    'please_select_an_option_caption' => 'selezionare un\'opzione',
    'power_alarm_thresholds_preference_label_caption' => 'unità di misura per gli allarmi di consumo energia',
    'submit_data_button_caption' => 'conferma',
    'we_have_sent_a_confirmation_email_msg_to_update_your_email_address_flash_message' => 'per confermare la modifica del tuo indirizzo email, ti abbiamo inviato un messaggio a :email_address. Controlla la tua casella.',
    'your_settings_have_been_updated_flash_message' => 'le impostazioni sono state aggiornate',
];