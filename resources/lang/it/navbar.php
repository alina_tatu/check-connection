<?php

return [
    'consumption_reports_page_link' => 'report di consumo',
    'login_link' => 'login',
    'logout_link' => 'logout',
    'lost_credit_entries_for_resetted_sockets_page_link' => 'log spegnimenti prese',
    'manage_berths_link' => 'assegnazione posti barca',
    'manage_dropdown' => 'gestisci',
    'manage_subscriptions_link' => 'contratti',
    'manage_users_link' => 'utenti',
    'my_subscriptions_link' => 'i miei contratti',
    'notifications_link' => 'notifiche',
    'plant_map_link' => 'mappa del marina',
    'plant_status_link' => 'stato del marina',
    'signup_link' => 'registrati',
    'user_settings_link' => 'impostazioni utente',
    'version_modal_dismiss_btt_caption' => 'chiudi',
    'welcome_msg' => 'benvenuto',
];