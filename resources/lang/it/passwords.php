<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'La password è stata resettata.',
    'sent' => 'E-mail di reset password inviata, controlla la tua casella.',
    'throttled' => 'Attendi prima di riprovare.',
    'token' => 'Il token di reset password non è valido.',
    'user' => "Questo indirizzo e-mail non corrisponde ad un utente registrato.",

];
