<?php

return [
    'max_power_consumption_alarm_message' => 'il consumo di energia della presa sul posto barca ha superato la soglia massima impostata',
    'max_water_tap_time_alarm_for_admins_message' => 'il tempo di utilizzo acqua del rubinetto sul posto barca ha superato i :threshold minuti. Questo potrebbe essere dovuto ad una dimenticanza dell\'utente o alla rottura di una tubatura.',
    'max_water_tap_time_alarm_for_users_message' => 'il tempo di utilizzo acqua del rubinetto sul posto barca ha superato la soglia massima impostata',
    'min_power_consumption_alarm_message' => 'il consumo di energia della presa sul posto barca era inferiore alla soglia minima impostata',
    'min_socket_credit_alarm_message' => 'il credito disponibile sulla presa del posto barca era inferiore alla soglia minima impostata',
    'report_generated_message' => 'il report che hai richiesto è stato generato. Puoi vederlo cliccando su questa notifica',
    'report_generated_notification_heading_berth' => 'posto barca [:name]',
    'report_generated_notification_heading_marina' => 'marina',
    'report_generated_notification_heading_pontoon' => 'pontile [:name]',
    'report_generated_notification_heading_subscription' => 'contratto [:name]',
    'serial_line_offline_alarm_message' => 'la linea seriale non risulta raggiungibile',
    'socket_offline_alarm_message' => 'la presa non risulta raggiungibile',
    'socket_theft_alarm_message' => 'l\'allarme antifurto della presa era attivo',
];