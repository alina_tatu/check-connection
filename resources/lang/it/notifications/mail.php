<?php

return [
    'do_not_reply' => 'Questo è un messaggio automatico, si prega di non rispondere.',
    'greetings' => 'Ciao :recipient_name, questa è un messaggio di notifica da :sender_name.',
    'max_power_consumption_alarm_message' => 'ti informiamo che il giorno :formatted_date alle ore :formatted_time, il consumo di energia della presa tuo posto barca [:berth_description] a :plant_name ha superato la soglia massima impostata.',
    'max_water_tap_time_alarm_for_users_message' => 'ti informiamo che il giorno :formatted_date alle ore :formatted_time, il tempo di utilizzo acqua del rubinetto sul tuo posto barca [:berth_description] a :plant_name ha superato la soglia massima impostata.',
    'min_power_consumption_alarm_message' => 'ti informiamo che il giorno :formatted_date alle ore :formatted_time, il consumo di energia della presa sul tuo posto barca [:berth_description] in :plant_name era inferiore alla soglia minima impostata.',
    'min_socket_credit_alarm_message' => 'ti informiamo che il giorno :formatted_date alle ore :formatted_time, il credito sulla presa del posto barca [:berth_description] in :plant_name era inferiore alla soglia minima impostata.',
    'report_generated_message' => 'ti informiamo che il report che hai richiesto è stato generato. Puoi vederlo usando <a href=":link">questo link</a>.',
    'socket_offline_alarm_message' => 'ti informiamo che il giorno :formatted_date alle ore :formatted_time, la presa sul tuo posto barca [:berth_description] in :plant_name non era raggiungibile da :app_name.',
    'theft_alarm_message' => 'ti informiamo che il giorno :formatted_date alle ore :formatted_time, l\'allarme antifurto della presa sul tuo posto barca [:berth_description] a :plant_name era attivo.',
];