<?php

return [
    'boat' => 'barca',
    'boat_measurements_tooltip_caption' => 'lunghezza: :length, larghezza: :width, profondità: :depth',     // TODO: cambiare il nome della translation, dato che non viene usato solo nei tooltip?
    'check_subscription_consumption_btt_caption' => 'consumi',   
    'current_user_on_socket_caption' => 'attuale utilizzatore',
    'epower_has_been_replaced_message' => 'ATTENZIONE: durante il periodo in esame, la presa è stata sostituita su (almeno) uno dei posti barca in esame. Pertanto non è possibile garantire l\'affidabilità dei consumi mostrati.',
    'error_occurred_while_performing_action_on_socket_msg' => 'Si è verificato un errore nell esecuzione dell azione richiesta sulla presa del posto barca [:berth_name]. Verificare di essere connessi ad internet. Se il problema persiste, si prega di contattare l assistenza tecnica.',
    'filters_bar_title' => 'filtri',
    'last_user_on_socket_caption' => 'ultimo utilizzatore',
    'socket_link_status_connected' => 'connesso',
    'socket_link_status_not_connected' => 'non connesso',
    'socket_link_status_theft_alarm' => 'allarme furto',        // TODO: l'allarme furto non va visualizzato nel link status. Quando lo sposterò e cambierò la label della traduzione, eliminare questa entry   
    'socket_reset_action_requested_msg' => 'il reset della presa sul posto barca [:berth_name] è stato programmato e verrà eseguito a breve',
    'socket_shutdown_action_requested_msg' => 'lo spegnimento della presa sul posto barca [:berth_name] è stato programmato e verrà eseguito a breve',
    'socket_switch_on_action_requested_msg' => 'l accensione della presa sul posto barca [:berth_name] è stata programmata e verrà eseguita a breve',
    'socket_status_not_connected' => 'non connesso',
    'socket_status_off' => 'off',
    'socket_status_on' => 'on',
    'socket_type_not_set' => 'non impostato',
    'unknown_berth' => 'sconosciuto',
    'unknown_user' => 'utente sconosciuto',
];
