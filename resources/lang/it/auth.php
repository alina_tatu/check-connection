<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'a_fresh_verification_link_has_been_sent_to_your_email_address' => 'ti abbiamo inviato un nuovo link di verifica via e-mail.',
    'before_proceeding_please_check_email_for_verification_link' => 'prima di procedere, controlla di aver ricevuto il link di verifica via e-mail.',
    'click_here_to_request_another_verification_link' => 'clicca qui per richiederlo di nuovo',
    'confirm_password_card_header_caption' => 'conferma password',
    'email_change_verification_btn_caption' => 'conferma questo indirizzo email',
    'email_change_verification_msg_caption' => 'ricevi questo messaggio perchè è stata richiesta la modifica dell\'indirizzo email associato al tuo account. Clicca sul pulsante per confermare la modifica. Se non hai richiesto tu il cambio di indirizzo, segnalalo al personale del marina.',
    'email_change_verification_msg_header_caption' => 'modifica indirizzo email',
    'email_change_verification_msg_thanks_msg_caption' => 'grazie',
    'email_verification_card_header_caption' => 'verifica il tuo indirizzo e-mail',
    'failed' => 'Credenziali non valide o account non abilitato.',
    'if_you_did_not_receive_the_email' => 'se non hai ricevuto la mail',
    'login_card_header_caption' => 'login',
    'login_confirm_btt_caption' => 'login',
    'login_forgot_password_link_caption' => 'password dimenticata?',
    'login_password_field_label_caption' => 'password',
    'login_register_page_link_caption' => 'registrazione',
    'login_remember_me_chk_label_caption' => 'ricordami',
    'login_username_or_email_field_label_caption' => 'username o indirizzo email',
    'password_changed_successfully' => 'password modificata correttamente',
    'password_confirmation_field_label' => 'conferma password',
    'password_expired_page_confirm_new_password_input_caption' => 'conferma nuova password',
    'password_expired_page_current_password_input_caption' => 'password attuale',
    'password_expired_page_header_caption' => 'modifica della password',
    'password_expired_page_new_password_input_caption' => 'nuova password',
    'password_expired_page_return_to_homepage_link_caption' => 'torna alla homepage',
    'password_expired_page_submit_btt_caption' => 'conferma la modifica della password',
    'password_expired_page_your_password_has_expired_please_change_it_msg_caption' => 'la tua password è scaduta e deve essere modificata.',
    'password_field_label' => 'password',
    'password_new_cannot_be_the_same_as_the_current_one' => 'la nuova password deve essere diversa da quella attuale',
    'password_not_correct' => 'la password attuale non è valida',
    'reset_password' => 'reset password',
    'send_password_reset_link' => 'invia il link per il reset della password',
    'signup_page_card_header_caption' => 'registrazione',
    'signup_page_validation_error_email_address_already_registered' => 'questo indirizzo e-mail è già registrato. Se non ricordi la password, puoi <a href=":password_reset_url">richiederne il recupero</a>.',
    'signup_page_validation_error_username_already_registered' => 'questo username e-mail è già registrato. Se non ricordi la password, puoi <a href=":password_reset_url">richiederne il recupero</a>.',
    'signup_page_validation_error_username_already_exists_must_be_authorized_by_reception_staff' => 'per poterti registrare, devi essere autorizzato dallo staff del marina.',
    'throttle' => 'Hai superato il numero massimo di tentativi per il login. Riprova tra :seconds secondi.',
    'user_first_name' => 'nome',
    'user_email_address' => 'indirizzo e-mail',
    'user_last_name' => 'cognome',
    'user_username' => 'username',
];
