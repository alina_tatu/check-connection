<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Il campo :attribute deve essere accettato.',
    'active_url' => 'Il campo :attribute non è un URL valido.',
    'after' => 'Il campo :attribute deve essere successivo a :date.',
    'after_or_equal' => 'Il campo :attribute deve essere uguale o successivo a :date.',
    'alpha' => 'Il campo :attribute può contenere solo lettere.',
    'alpha_dash' => 'Il campo :attribute può contenere solo lettere, numeri, trattini ed underscores.',
    'alpha_num' => 'Il campo :attribute può contenere solo lettere e numeri.',
    'array' => 'Il campo :attribute deve essere un array',
    'before' => 'Il campo :attribute deve essere precedente a :date.',
    'before_or_equal' => 'Il campo :attribute deve essere uguale o precedente a :date.',
    'between' => [
        'numeric' => 'Il valore del campo :attribute deve essere compreso tra :min e :max.',
        'file' => 'La dimensione del file :attribute deve essere compresa tra :min e :max kilobytes.',
        'string' => 'La lunghezza del campo :attribute deve essere compresa tra :min e :max caratteri.',
        'array' => 'Il numero di elementi di :attribute deve essere compreso tra :min e :max items.',
    ],
    'boolean' => 'Il valore del campo :attribute può essere vero o falso.',
    'confirmed' => 'I valori del campo :attribute non coincidono.',
    'date' => 'Il campo :attribute non è una data valida.',
    'date_equals' => 'Il campo :attribute deve essere una data uguale a :date.',
    'date_format' => 'Il campo :attribute deve essere espresso nel formato :format.',
    'different' => 'I valori campi :attribute e :other non possono coincidere.',
    'digits' => 'Il campo :attribute deve essere composto da :digits cifre.',
    'digits_between' => 'La lunghezza del campo :attribute deve essere compresa tra :min e :max cifre.',
    'dimensions' => 'Le dimensioni di :attribute non sono valide.',
    'distinct' => 'Il campo :attribute è duplicato.',
    'email' => 'Il campo :attribute non è un indirizzo e-mail valido.',
    'ends_with' => 'Il campo :attribute deve terminare con uno dei seguenti valori: :values.',
    'exists' => 'Il valore selezionato per il campo :attribute non è valido.',
    'file' => ':attribute deve essere un file.',
    'filled' => 'Valore mancante per il campo :attribute.',
    'gt' => [
        'numeric' => 'Il valore del campo :attribute deve essere maggiore di :value.',
        'file' => 'La dimensione del file :attribute deve essere maggiore di :value kilobytes.',
        'string' => 'La lunghezza del campo :attribute deve essere maggiore di :value caratteri.',
        'array' => ':attribute deve contenere più di :value elementi.',
    ],
    'gte' => [
        'numeric' => 'Il valore del campo :attribute deve essere maggiore o uguale a :value.',
        'file' => 'La dimensione del file :attribute deve essere maggiore o uguale a :value kilobytes.',
        'string' => 'La lunghezza del campo :attribute deve essere maggiore o uguale a :value caratteri.',
        'array' => ':attribute deve contenere almeno :value elementi.',
    ],
    'image' => ':attribute deve essere un\'immagine.',
    'in' => 'Il valore del campo :attribute non è valido.',
    'in_array' => 'Il valore del campo :attribute non esiste in :other.',
    'integer' => 'Il valore del campo :attribute deve essere un intero.',
    'ip' => 'Il campo :attribute deve contenere un indirizzo IP valido.',
    'ipv4' => 'Il campo :attribute deve contenere un indirizzo IPv4 valido.',
    'ipv6' => 'Il campo :attribute deve contenere un indirizzo IPv6 valido.',
    'json' => 'Il campo :attribute deve essere una stringa JSON valida.',
    'lt' => [
        'numeric' => 'Il valore del campo :attribute deve essere minore di :value.',
        'file' => 'La dimensione del file :attribute deve essere minore di :value kilobytes.',
        'string' => 'La lunghezza del campo :attribute deve essere minore di :value caratteri.',
        'array' => ':attribute deve contenere meno di :value elementi.',
    ],
    'lte' => [
        'numeric' => 'Il valore del campo :attribute deve essere minore o uguale a :value.',
        'file' => 'La dimensione del file :attribute deve essere minore o uguale a :value kilobytes.',
        'string' => 'La lunghezza del campo :attribute deve essere minore o uguale a :value caratteri.',
        'array' => ':attribute deve contenere al massimo :value elementi.',
    ],
    'max' => [
        'numeric' => 'Il valore del campo :attribute non può essere maggiore di :value.',
        'file' => 'La dimensione del file :attribute non può essere maggiore di :value kilobytes.',
        'string' => 'La lunghezza del campo :attribute non può essere maggiore di :value caratteri.',
        'array' => ':attribute non può contenere più di :value elementi.',
    ],
    'mimes' => ':attribute deve essere un file di tipo: :values.',
    'mimetypes' => ':attribute deve essere un file di tipo: :values.',
    'min' => [
        'numeric' => 'Il valore del campo :attribute deve essere almeno :value.',
        'file' => 'La dimensione del file :attribute deve essere di almeno :value kilobytes.',
        'string' => 'La lunghezza del campo :attribute deve essere di almeno :value caratteri.',
        'array' => ':attribute deve contenere almeno :value elementi.',
    ],
    'not_in' => 'Il valore selezionato per :attribute non è valido',
    'not_regex' => 'Il formato di :attribute non è valido.',
    'numeric' => 'Il campo :attribute deve contenere un numero.',
    'password' => 'La password non è corretta.',
    'present' => 'Il campo :attribute non può essere vuoto.',
    'regex' => 'Il formato del campo :attribute non è valido.',
    'required' => 'Il campo :attribute è obbligatorio.',
    'required_if' => 'Il campo :attribute è obbligatorio se :other è :value.',
    'required_unless' => 'Il campo :attribute è obbligatorio se :other non è :values.',
    'required_with' => 'Il campo :attribute è obbligatorio in presenza del valore di :values.',
    'required_with_all' => 'Il campo :attribute è obbligatorio in presenza del valore di :values.',
    'required_without' => 'Il campo :attribute è obbligatorio se il valore di :values non è specificato.',
    'required_without_all' => 'Il campo :attribute è obbligatorio se il valore di :values non è specificato.',
    'same' => 'I campi :attribute e :other non coincidono.',
    'size' => [
        'numeric' => 'La dimensione di :attribute deve essere :size.',
        'file' => 'La dimensione di :attribute deve essere di :size kilobytes.',
        'string' => 'La dimensione di :attribute deve essere di :size caratteri.',
        'array' => ':attribute deve contenere :size elementi.',
    ],
    'starts_with' => 'Il campo :attribute deve iniziare con uno dei seguenti valori: :values.',
    'string' => 'Il campo :attribute deve essere testuale.',
    'timezone' => 'Il campo :attribute deve corrispondere ad una timezone valida.',
    'unique' => 'Il valore di :attribute è già in uso.',
    'uploaded' => 'Caricamento non riuscito per :attribute.',
    'url' => 'Il formato del campo :attribute non è valido.',
    'uuid' => ':attribute deve contenere un UUID valido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'generic' => [
            'invalid_value' => 'campo non valido',
            'required_value' => 'campo obbligatorio',
        ],
        'views' => [
            'berths' => [
                'create_subscription' => [
                    'transponder_id' => [
                        'not_found_in_transponders_table' => 'Il transponder :attribute è sconosciuto',
                    ],
                ],
            ],
            'consumption_reports' => [
                'chart' => [
                    'timespan_length' => [
                        'max_days_when_requesting_credit_chart' => 'Quando si richiede un grafico del credito residuo, l\'intervallo richiesto deve essere minore o uguale di :max_days giorni',
                        'max_days_when_sampling_by_day' => 'Quando si richiede un grafico dei consumi con campionamento giornaliero, l\'intervallo richiesto deve essere minore o uguale di :max_days giorni',
                        'max_days_when_sampling_by_hour' => 'Quando si richiede un grafico dei consumi con campionamento orario, l\'intervallo richiesto deve essere minore o uguale di :max_days giorni',
                    ],
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
