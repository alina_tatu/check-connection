<?php

return [
    'max_power_consumption_alarm_message' => 'the electricity consumption of the socket on your berth was higher than the maximum threshold you set',
    'max_water_tap_time_alarm_for_admins_message' => 'the water tap on the berth is consuming water by more than :threshold minutes. This could be due to a user forgetfulness or a pipe break.',
    'max_water_tap_time_alarm_for_users_message' => 'the water tap on your berth was consuming water for a longer time than the threshold you set',
    'min_power_consumption_alarm_message' => 'the electricity consumption of the socket on your berth was lower than the minimum threshold you set',
    'min_socket_credit_alarm_message' => 'the credit available on the socket for your berth was lower than the minimum threshold you set',
    'report_generated_message' => 'the report you requested has been generated. You can view it by clicking on this notification',   
    'report_generated_notification_heading_berth' => 'berth [:name]',
    'report_generated_notification_heading_marina' => 'marina',
    'report_generated_notification_heading_pontoon' => 'pontoon [:name]',
    'report_generated_notification_heading_subscription' => 'subscription [:name]',
    'serial_line_offline_alarm_message' => 'the serial line is not responding',
    'socket_offline_alarm_message' => 'the socket is not responding',
    'socket_theft_alarm_message' => 'the anti-theft alarm was active on your berth',
];