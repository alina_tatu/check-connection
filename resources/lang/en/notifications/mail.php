<?php

return [
    'do_not_reply' => 'This is an automatically generated email, please do not reply.',
    'greetings' => 'Hi :recipient_name, this is an automatic alarm notification message from :sender_name.',
    'max_power_consumption_alarm_message' => 'we wish to inform you that on :formatted_date at :formatted_time, the electricity consumption of the socket on your berth [:berth_description] in :plant_name was higher than the minimum threshold you set.',
    'max_water_tap_time_alarm_for_users_message' => 'we wish to inform you that on :formatted_date at :formatted_time, the water tap on your berth [:berth_description] in :plant_name was consuming water for a longer time than the threshold you set.',
    'min_power_consumption_alarm_message' => 'we wish to inform you that on :formatted_date at :formatted_time, the electricity consumption of the socket on your berth [:berth_description] in :plant_name was lower than the minimum threshold you set.',
    'min_socket_credit_alarm_message' => 'we wish to inform you that on :formatted_date at :formatted_time, the available credit of the socket on your berth [:berth_description] in :plant_name was lower than the minimum threshold you set.',
    'report_generated_message' => 'we wish to inform you that the report you requested has been generated. You can view it by clicking <a href=\":link\">this link</a>.',
    'socket_offline_alarm_message' => 'we wish to inform you that on :formatted_date at :formatted_time, the socket on your berth [:berth_description] in :plant_name was not reachable by :app_name.',
    'theft_alarm_message' => 'we wish to inform you that on :formatted_date at :formatted_time, the anti-theft alarm of the socket on your berth [:berth_description] in :plant_name was active.',
];