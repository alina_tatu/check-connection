<?php

return [
    'filter_berth_caption' => 'search by berth',
    'filter_user_caption' => 'search by subscription owner/requesting user',
    'no_entries_message' => 'this log does not yet contain any entries',
    'th_action_type' => 'action',
    'th_amount_to_refund' => 'amount lost',
    'th_berth' => 'berth',
    'th_requesting_user' => 'requesting user',
    'th_subscription_owner_user' => 'subscription owner',
    'th_timestamp' => 'date and time',
];