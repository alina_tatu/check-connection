<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'a_fresh_verification_link_has_been_sent_to_your_email_address' => 'a fresh verification link has been sent to your e-mail address.',
    'before_proceeding_please_check_email_for_verification_link' => 'before proceeding, please check your email for a verification link.',
    'click_here_to_request_another_verification_link' => 'click here to request another',
    'confirm_password_card_header_caption' => 'confirm password',
    'email_change_verification_btn_caption' => 'confirm this email address',
    'email_change_verification_msg_caption' => 'we are sending you this message because we received the request to change the email address associated with your account. Please click on the button to confirm the modification. If you did not request this modification, please report it to the marina staff.',
    'email_change_verification_msg_header_caption' => 'email address modification',
    'email_change_verification_msg_thanks_msg_caption' => 'thanks',
    'email_verification_card_header_caption' => 'verify your e-mail address',
    'failed' => 'Invalid credentials or disabled account.',
    'if_you_did_not_receive_the_email' => 'if you did not receive the e-mail',
    'login_card_header_caption' => 'login',
    'login_confirm_btt_caption' => 'login',
    'login_forgot_password_link_caption' => 'forgot your password?',
    'login_password_field_label_caption' => 'password',
    'login_register_page_link_caption' => 'sign up',
    'login_remember_me_chk_label_caption' => 'remember me',
    'login_username_or_email_field_label_caption' => 'username or email address',
    'password_changed_successfully' => 'password changed successfully',
    'password_confirmation_field_label' => 'confirm password',
    'password_expired_page_confirm_new_password_input_caption' => 'confirm new password',
    'password_expired_page_current_password_input_caption' => 'current password',
    'password_expired_page_header_caption' => 'reset password',
    'password_expired_page_new_password_input_caption' => 'new password',
    'password_expired_page_return_to_homepage_link_caption' => 'return to homepage',
    'password_expired_page_submit_btt_caption' => 'confirm password update',
    'password_expired_page_your_password_has_expired_please_change_it_msg_caption' => 'your password has expired, please change it.',
    'password_field_label' => 'password',
    'password_new_cannot_be_the_same_as_the_current_one' => 'the new password cannot be the same as the current one',
    'password_not_correct' => 'the current password is not correct',
    'reset_password' => 'reset password',
    'send_password_reset_link' => 'send password reset link',
    'signup_page_card_header_caption' => 'sign up',
    'signup_page_validation_error_email_address_already_registered' => 'this e-mail address is already registered. If you don\'t remember your password, please use the <a href=":password_reset_url">password recovery page</a>.',
    'signup_page_validation_error_username_already_registered' => 'this username is already registered. If you don\'t remember your password, please use the <a href=":password_reset_url">password recovery page</a>.',
    'signup_page_validation_error_username_already_exists_must_be_authorized_by_reception_staff' => 'before signing up, you must be authorized by the harbor reception staff.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'user_first_name' => 'first name',
    'user_email_address' => 'e-mail address',
    'user_last_name' => 'last name',
    'user_username' => 'username',
];
