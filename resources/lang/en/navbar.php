<?php

return [
    'consumption_reports_page_link' => 'consumption reports',
    'login_link' => 'login',
    'logout_link' => 'logout',
    'lost_credit_entries_for_resetted_sockets_page_link' => 'sockets switch off log',
    'manage_berths_link' => 'berths assignment',
    'manage_dropdown' => 'manage',
    'manage_subscriptions_link' => 'subscriptions',
    'manage_users_link' => 'users',
    'my_subscriptions_link' => 'my subscriptions',
    'notifications_link' => 'notifications',
    'plant_map_link' => 'plant map',
    'plant_status_link' => 'plant status',
    'signup_link' => 'register',
    'version_modal_dismiss_btt_caption' => 'close',
    'user_settings_link' => 'user settings',
    'welcome_msg' => 'welcome',
];