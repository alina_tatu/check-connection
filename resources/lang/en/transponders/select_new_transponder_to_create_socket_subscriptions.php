<?php

return [
    'last_subscription_activated_on_date' => 'on',
    'last_subscription_activated_with_transponder' => 'last postpaid subscription activated',
    'select_a_transponder_no_option_selected_caption' => 'select a postpaid transponder key',
    'select_a_transponder_no_transponders_found' => 'no other postpaid transponder keys have been found',       // TODO: nella translation, spiegare come procedere per aggiungerne
    'submit_btt_caption' => 'this postpaid transponder key will be used to activate postpaid subscriptions',
    'username' => 'username',
];