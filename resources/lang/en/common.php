<?php

return [
    'boat' => 'boat',
    'boat_measurements_tooltip_caption' => 'lenght: :length, width: :width, depth: :depth',
    'check_subscription_consumption_btt_caption' => 'check consumption',
    'current_user_on_socket_caption' => 'current user',
    'epower_has_been_replaced_message' => 'PLEASE NOTE: during the period under review, the socket was replaced on (at least) one of the berths used. Therefore, it is not possible to guarantee the reliability of the displayed consumption.',
    'error_occurred_while_performing_action_on_socket_msg' => 'An error has occurred while performing the requested action on the [:berth_name] socket. Make sure you are connected to the internet. If the problem persists, please contact technical support.',
    'filters_bar_title' => 'filters',
    'last_user_on_socket_caption' => 'last user',
    'socket_link_status_connected' => 'connected',
    'socket_link_status_not_connected' => 'not connected',
    'socket_link_status_theft_alarm' => 'theft alarm',                                               // TODO: l'allarme furto non va visualizzato nel link status. Quando lo sposto e cambio la label della traduzione, eliminare questa entry
    'socket_reset_action_requested_msg' => 'the request to reset of the socket [:berth_name] has been taken over and will be processed shortly',
    'socket_shutdown_action_requested_msg' => 'the request to switch off the socket [:berth_name] has been taken over and will be processed shortly',
    'socket_switch_on_action_requested_msg' => 'the request to switch on the socket [:berth_name] has been taken over and will be processed shortly',
    'socket_status_not_connected' => 'not connected',
    'socket_status_off' => 'off',
    'socket_status_on' => 'on',
    'socket_type_not_set' => 'not set',
    'unknown_berth' => 'unknown',
    'unknown_user' => 'unknown user',
];
