<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute must be accepted.',
    'active_url' => 'The :attribute field is not a valid URL.',
    'after' => 'The :attribute field must be a date after :date.',
    'after_or_equal' => ':attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute field may only contain letters.',
    'alpha_dash' => 'The :attribute field may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute field may only contain letters and numbers.',
    'array' => ':attribute must be an array.',
    'before' => 'The :attribute field must be a date before :date.',
    'before_or_equal' => ':attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute field value must be between :min and :max.',
        'file' => 'The :attribute file size must be between :min and :max kilobytes.',
        'string' => 'The :attribute field length must be between :min and :max characters.',
        'array' => ':attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => ':attribute is not a valid date.',
    'date_equals' => ':attribute must be a date equal to :date.',
    'date_format' => ':attribute does not match the format :format.',
    'different' => 'The :attribute and :other fields must be different.',
    'digits' => 'The :attribute field must be :digits digits.',
    'digits_between' => 'The :attribute field must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute field must be a valid email address.',
    'ends_with' => 'The :attribute field value must end with one of the following: :values.',
    'exists' => 'The selected value for :attribute is invalid.',
    'file' => ':attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute field value must be greater than :value.',
        'file' => 'The :attribute file size must be greater than :value kilobytes.',
        'string' => 'The :attribute field length must be greater than :value characters.',
        'array' => ':attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute field value must be greater than or equal to :value.',
        'file' => 'The :attribute file size must be greater than or equal to :value kilobytes.',
        'string' => 'The :attribute field length must be greater than or equal to :value characters.',
        'array' => ':attribute must have :value items or more.',
    ],
    'image' => ':attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute field value must be an integer.',
    'ip' => 'The :attribute field value must be a valid IP address.',
    'ipv4' => 'The :attribute field value must be a valid IPv4 address.',
    'ipv6' => 'The :attribute field value must be a valid IPv6 address.',
    'json' => 'The :attribute field value must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute field value must be less than :value.',
        'file' => 'The :attribute file size must be less than :value kilobytes.',
        'string' => 'The :attribute field length must be less than :value characters.',
        'array' => ':attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute field value must be less than or equal to :value.',
        'file' => 'The :attribute file size must be less than or equal to :value kilobytes.',
        'string' => 'The :attribute field length must be less than or equal to :value characters.',
        'array' => ':attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute field value may not be greater than :max.',
        'file' => 'The :attribute file size may not be greater than :max kilobytes.',
        'string' => 'The :attribute field length may not be greater than :max characters.',
        'array' => ':attribute may not have more than :max items.',
    ],
    'mimes' => ':attribute must be a file of type: :values.',
    'mimetypes' => ':attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute field value must be at least :min.',
        'file' => 'The :attribute file size must be at least :min kilobytes.',
        'string' => 'The :attribute field length must be at least :min characters.',
        'array' => ':attribute must have at least :min items.',
    ],
    'not_in' => 'The selected value for :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute field value must be a number.',
    'password' => 'The password is incorrect.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other field values must match.',
    'size' => [
        'numeric' => 'The :attribute field value must be :size.',
        'file' => 'The :attribute file size must be :size kilobytes.',
        'string' => 'The :attribute field length must be :size characters.',
        'array' => ':attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute value must start with one of the following: :values.',
    'string' => ':attribute must be a string.',
    'timezone' => ':attribute must be a valid zone.',
    'unique' => 'The value entered for :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute field format is invalid.',
    'uuid' => 'The :attribute field value must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'generic' => [
            'invalid_value' => 'this field is invalid',
            'required_value' => 'this field is required',
        ],
        'views' => [
            'berths' => [
                'create_subscription' => [
                    'transponder_id' => [
                        'not_found_in_transponders_table' => 'The :attribute transponder is unknown',
                    ],
                ],
            ],
            'consumption_reports' => [
                'chart' => [
                    'timespan_length' => [
                        'max_days_when_requesting_credit_chart' => 'When requesting a residual credit chart, the requested interval may not be longer than :max_days days',
                        'max_days_when_sampling_by_day' => 'When requesting a daily sampled consumption chart, the requested interval may not be longer than :max_days days',
                        'max_days_when_sampling_by_hour' => 'When requesting an hourly sampled consumption chart, the requested interval may not be longer than :max_days days',
                    ],
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
