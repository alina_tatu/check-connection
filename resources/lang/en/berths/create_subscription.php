<?php

return [
    'add_new_postpaid_transponder_used_for_socket_subscriptions_button_caption' => 'enable another postpaid transponder key to activate berth subscriptions',
    'begin_timestamp_field_label_caption' => 'starts on',
    'berth_name_field_label_caption' => 'berth',
    'notes_field_label_caption' => 'subscription notes',
    'search_for_users_caption' => 'search users',
    'selected_transponder_has_been_set_to_be_used_for_socket_subscriptions_msg' => 'the transponder key :code has been enabled to activate berth subscriptions',
    'socket_type_field_label_caption' => 'socket type',
    'submit_btt_caption' => 'create subscription',
    'submit_confirmation_request' => 'are you sure you want to create this subscription?',
    'transponder_field_label_caption' => 'transponder key',
    'transponder_field_please_select_an_option_caption' => 'please select the postpaid transponder key that will be used to activate this subscription on the berth',
    'transponder_field_there_are_no_transponders_used_for_socket_subscriptions_caption' => 'no transponder keys enabled to activate berth subscriptions have been found. Please enable one using the appropriate button.',
    'user_field_label_caption' => 'user',
    'user_not_found_alert_msg_caption' => 'the selected user does not exist. If you want to create one, please use the Users section.',
    'username' => 'username',
];