<?php

return [
    'berth_caption' => 'berth',
    'berth_filter_caption' => 'search by berth name',
    'boat_depth' => 'depth',
    'boat_length' => 'length',
    'boat_width' => 'width',
    'chart_period_select_last_24hours_caption' => 'last 24 hours',
    'chart_period_select_last_month_caption' => 'last month',
    'chart_period_select_last_week_caption' => 'last week',
    'chart_submit_btt_caption' => 'consumption trend',
    'customer_filter_caption' => 'search by customer or boat',
    'instantpowerconsumption_caption' => 'instant electricity consumption',
    'lastupdatetimestamp_caption' => 'last update',
    'linkstatus_caption' => 'link status',
    'no_berths_on_database_or_no_berths_match_search_criteria' => 'no berths match the search criteria, or there are no berths on the database yet.',
    'no_status_information_available_for_this_berth' => 'there is currently no status information available',
    'onoff_btt_alert_switchoff_confirmation_request' => 'are you sure you want to switch off the socket on berth [:berth]?',
    'onoff_btt_alert_switchon_confirmation_request' => 'are you sure you want to switch on the socket on berth [:berth]?',
    'onoff_btt_caption_socket_is_off' => 'off',
    'onoff_btt_caption_socket_is_on' => 'on',
    'onoff_btt_caption_socket_is_not_connected' => 'status not available',
    'onoff_btt_tooltip_click_to_switch_off_socket' => 'click to switch off this socket',
    'onoff_btt_tooltip_socket_is_not_responding_unable_to_control_it' => 'this socket is not responding, unable to control it',
    'onoff_btt_tooltip_socket_switchon_is_disabled_for_security_reasons' => 'the socket remote switch-on feature is disabled for security reasons',
    'pontoon_filter_caption' => 'search by pontoon',
    'pontoon_filter_no_pontoon_detected_caption' => 'no pontoon found',
    'postpaid_user_span_caption' => 'postpaid',
    'reset_btt_alert_action_confirmation_request' => 'are you sure you want to reset the socket on berth [:berth]?',
    'reset_btt_caption' => 'reset',
    'reset_btt_tooltip_click_to_reset_socket' => 'click to reset this socket',
    'reset_filters_btt_caption' => 'reset filters',
    'residualcredit_caption' => 'residual credit',
    'show_hide_snapshot_div_btt_tooltip_caption' => 'show/hide status',
    'socket_credit_consumption_counting_caption' => 'credit consumption',
    'socket_postpaid_power_counter_caption' => 'postpaid electricity meter',
    'socket_postpaid_water_counter_caption' => 'postpaid water meter',
    'socket_power_consumption_counting_caption' => 'electricity consumption',
    'socket_total_power_counter_caption' => 'total electricity meter',
    'socket_total_water_counter_caption' => 'total water meter',
    'socket_type_caption' => 'socket type',
    'socket_type_filter_caption' => 'search by socket type',
    'socket_water_consumption_counting_caption' => 'water consumption',
];