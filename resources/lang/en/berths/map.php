<?php

return [
    'berth_caption' => 'berth',
    'desktop_guidelines_move_with_arrow_keys' => 'move around the map using the arrow keys, and click on the markers to see the status of the pedestals',
    'desktop_guidelines_press_plus_key_to_zoom_in' => 'press + to zoom in',
    'desktop_guidelines_press_minus_key_to_zoom_out' => 'press - to zoom out',
    'instantpowerconsumption_caption' => 'instant electricity consumption',
    'lastupdatetimestamp_caption' => 'last update',
    'linkstatus_caption' => 'link status',
    'onoffstatus_caption' => 'status',
    'no_berths_on_database' => 'there are no berths on the database yet',
    'no_data_available_for_pedestal_modal_msg' => 'no data available',
    'no_status_information_available_for_this_berth' => 'there is currently no status information available',
    'pedestal' => 'pedestal',
    'postpaid_user_span_caption' => 'postpaid',
    'residualcredit_caption' => 'residual credit',
    'socket_postpaid_power_counter_caption' => 'postpaid electricity meter',
    'socket_postpaid_water_counter_caption' => 'postpaid water meter',
    'socket_total_power_counter_caption' => 'total electricity meter',
    'socket_total_water_counter_caption' => 'total water meter',
    'socket_type_caption' => 'socket type',
];