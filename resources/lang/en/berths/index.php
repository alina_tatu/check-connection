<?php

return [
    'boat_measurements_tooltip' => 'length: :length, width: :width, depth: :depth',
    'check_subscription_consumption_btt_caption' => 'check consumption',
    'combo_filter_all_berths_option_caption' => 'view all berths',
    'combo_filter_assigned_berths_only_option_caption' => 'view currenty assigned berths only',
    'combo_filter_available_berths_only_option_caption' => 'view currently available berths only',
    'currently_unassigned_berth_caption' => 'currently unassigned',
    'fulltext_filter_caption' => 'search berths',
    'no_berths_on_database_or_no_berth_matches_search_criteria' => 'no berth matches the search criteria, or there are no berths on the database yet.',
    'start_new_subscription_btt_caption' => 'start new subscription',
    'th_begin_date' => 'begin date',
    'th_berth_name' => 'berth',
    'th_boat' => 'boat',
    'th_socket_type' => 'socket type',
    'th_user' => 'user',
    'th_end_date' => 'end date',
];