<?php

return [
    'back_to_previous_page_btt_caption' => 'back to previous page',
    'chart_for_an_unknown_berth_requested_msg' => 'you requested a consumption chart for an unknown berth. Please check and try again.',
    'there_is_not_enough_consumption_data_to_draw_the_chart_msg_admin_caption' => ':app_name has not yet received enough data to draw the chart. Please try again later or check for any communication errors with the socket.',
    'there_is_not_enough_consumption_data_to_draw_the_chart_msg_simple_user_caption' => ':app_name has not yet received enough data to draw the chart. Please try again later.',
    'there_were_disconnections_message' => 'PLEASE NOTE: during the period under review, this berth had disconnections. Therefore, it is not possible to guarantee the reliability of the displayed consumption.',
    'title_berth_consumption_caption' => ':consumption_type :period on berth :berth_name',
    'title_consumption_type_residual_credit_consumption_caption' => 'credit consumption',
    'title_consumption_type_residual_credit_trend_caption' => 'residual credit trend',
    'title_consumption_type_power_caption' => 'average electricity consumption',
    'title_consumption_type_water_caption' => 'average water consumption',
    'title_period_from_to' => 'from [:begin] to [:end]',
    'title_subscription_consumption_caption' => ':consumption_type :period for :subscription_description',
    'title_subscription_description_with_berth_caption' => '[:user_name] on berth [:berth_name]',
    'title_subscription_description_without_berth_caption' => '[:user_name]',
    'x_axis_time_interval_caption' => 'time interval',
    'y_axis_average_credit_consumption_caption' =>  'average credit consumption (:currency)',
    'y_axis_average_power_consumption_caption' => 'average electricity consumption (kWh)',
    'y_axis_average_water_consumption_caption' => 'average water consumption (m³/h)',
    'y_axis_residual_credit_on_socket_caption' =>  'residual credit on socket (:currency)',
];