<?php

return [
    'an_error_has_occured_message_caption' => 'an error has occured',
    'email_address_label_caption' => 'email address',
    'email_address_is_invalid_or_already_in_use' => 'the email address is invalid or already in use',
    'form_submit_confirmation_request_caption' => 'are you sure you want to change your settings?',
    'language_preference_label_caption' => 'software language',
    'please_select_an_option_caption' => 'please select an option',
    'power_alarm_thresholds_preference_label_caption' => 'unit of measurement for electricity consumption alarm thresholds',
    'submit_data_button_caption' => 'confirm',
    'we_have_sent_a_confirmation_email_msg_to_update_your_email_address_flash_message' => 'to confirm the change of the email address, we have sent you an email at :email_address. Please check your inbox.',
    'your_settings_have_been_updated_flash_message' => 'your settings have been updated',
];