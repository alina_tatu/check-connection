<?php

return [
    'alarm_switched_off_at' => 'alarm switched off at',
    'berth' => 'socket on berth',
    'notifications_list_heading_caption' => 'notifications',
    'refreshing_notifications' => 'refreshing notifications...',
    'serial_line' => 'serial line',
    'you_have_not_received_any_notifications_yet' => 'you have not received any notifications yet',
];