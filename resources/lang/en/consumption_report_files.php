<?php

return [
    'berth_caption' => 'berth',
    'pdf_berth_consumption_title_berth_consumption_from_to' => 'consumption report for berth [:berth_name] from :begin_timestamp_formatted to :end_timestamp_formatted',
    'pdf_page_title' => 'consumption report',
    'pdf_plant_consumption_title_plant_consumption_from_to' => 'marina consumption report from :begin_timestamp_formatted to :end_timestamp_formatted',
    'pdf_pontoon_consumption_title_pontoon_consumption_from_to' => 'consumption report for pontoon [:pontoon_name] from :begin_timestamp_formatted to :end_timestamp_formatted',
    'pdf_subscription_consumption_title_subscription_consumption_from_to' => 'consumption report for subscription :subscription_description from :begin_timestamp_formatted to :end_timestamp_formatted',
    'pdf_title_subscription_description_with_berth_caption' => '[:user_name] on berth [:berth_name]',
    'pdf_title_subscription_description_without_berth_caption' => '[:user_name]',
    'power_consumption_caption' => 'electricity',
    'total_consumption_caption' => 'total consumption',
    'total_power_consumption_caption' => 'total electricity consumption',
    'total_water_consumption_caption' => 'total water consumption',
    'water_consumption_caption' => 'water',
    'xls_title_for_berth_part_1' => 'consumption report for berth',
    'xls_title_for_marina_part_1' => 'marina consumption report',
    'xls_title_for_pontoon_part_1' => 'consumption report for pontoon',
    'xls_title_for_subscription_part_1' => 'consumption report for subscription',
    'xls_title_from_part' => 'from',
    'xls_title_to_part' => 'to',
];