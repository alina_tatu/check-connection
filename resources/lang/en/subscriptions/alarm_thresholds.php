    
<?php

return [
    'min_power_threshold_um_caption' => 'min. electricity consumption alarm threshold (:um)',
    'max_power_threshold_um_caption' => 'max. electricity consumption alarm threshold (:um)',
    'min_credit_threshold_currency_caption' => 'min. credit alarm threshold (:currency)',
    'max_water_tap_time_threshold_minutes_caption' => 'max. water tap opening time alarm threshold (minutes)',
    'submit_btt_caption' => 'set alarm thresholds',
    'submit_confirmation_request' => 'do you confirm these thresholds?',
];