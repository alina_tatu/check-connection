<?php

return [
    'combo_filter_active_subscriptions_only_option_caption' => 'view current subscriptions',
    'combo_filter_all_subscriptions_option_caption' => 'view all subscriptions',
    'combo_filter_ended_subscriptions_only_option_caption' => 'view ended subscriptions',
    'fulltext_filter_caption' => 'search subscriptions',
    'no_subscriptions_on_database_or_no_subscription_matches_search_criteria' => 'no subscription matches the search criteria, or there are no subscriptions on the database yet.',
    'subscription_successfully_closed_msg' => 'subscription ended successfully',
    'subscription_successfully_created_msg' => 'subscription created successfully',
    'subscription_successfully_updated_msg' => 'subscription updated successfully',
    'th_berth_name' => 'berth',
    'th_begin_date' => 'begin date',
    'th_boat' => 'boat',
    'th_end_date' => 'end date',
    'th_transponder' => 'transponder key',
    'th_user' => 'user',
];