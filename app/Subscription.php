<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use App\User;
use App\Berth;
use App\SocketSnapshot;
use App\SubscriptionSnapshot;
use App\SubscriptionAlarmThresholdsRecord;
use App\SubscriptionCostsRecord;
use App\SubscriptionConsumptionPayment;
use App\Http\Controllers\ConsumptionReportsController;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use DateTimeInterface;

/**
 * App\Subscription
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $transponder_id
 * @property int|null $socket_id
 * @property Carbon $begin_dt
 * @property Carbon|null $end_dt
 * @property string|null $notes
 * @property-read Berth|null $berth
 * @property-read \Illuminate\Database\Eloquent\Collection|SubscriptionCostsRecord[] $costs_records
 * @property-read int|null $costs_records_count
 * @property-read \Illuminate\Database\Eloquent\Collection|SubscriptionConsumptionPayment[] $payments
 * @property-read int|null $payments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|SubscriptionSnapshot[] $subscription_snapshots
 * @property-read int|null $subscription_snapshots_count
 * @property-read \Illuminate\Database\Eloquent\Collection|SubscriptionAlarmThresholdsRecord[] $thresholds_records
 * @property-read int|null $thresholds_records_count
 * @property-read \App\Transponder|null $transponder
 * @property-read User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription active()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription ended()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription excludeSupervisorRelatedSubscriptions()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Subscription newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription newerFirst()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereBeginDt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereEndDt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereSocketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereTransponderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Subscription extends Model {
    use Cachable;
    use Traits\TimeZoneAware;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';
    protected $primaryKey = 'id';

    public $timestamps = false;
    protected $dates = ['begin_dt',
                        'end_dt'];
    public $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'transponder_id', 'socket_id', 'begin_dt', 'end_dt', 'notes'
    ];

    // TODO: devo chiarire meglio la distinzione tra i metodi di calculate_consumption() del model Subscription ed i metodi di get_consumption() che si occupano solo di fare la differenza tra i valori della tabella subscription_snapshot?

    protected function serializeDate(DateTimeInterface $date) {
    // metodo per mantenere retrocompatibilità dopo l'upgrade a Laravel 7 (v. https://laravel.com/docs/7.x/upgrade#date-serialization)
        return $date->format('Y-m-d H:i:s');
    }

    public function save(array $options = []) {
        return parent::save($options);
    }

    public function get_id() {
        return intval($this->id);
    }

    public function set_id(int $value) {
        $this->attributes[$this->primaryKey] = $value;
    }

    public function set_user_id(int $value) {
        $this->user_id = $value;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function transponder() {
        return $this->belongsTo(Transponder::class, 'transponder_id', 'code');
    }

    public function set_transponder_id(string $value = null) {
        $this->transponder_id = $value;
    }

    public function berth() {
        return $this->belongsTo(Berth::class, 'socket_id');
    }

    public function set_berth_id(int $value = null) {
        $this->socket_id = $value;
    }

    public function get_begin_timestamp(): Carbon {
    // restituisce il timestamp dello snapshot
        return $this->ConvertToLocalTimeZone($this->begin_dt);
    }

    public function set_begin_timestamp(Carbon $timestamp): bool {
        $timestamp->setTimezone(config('app.timezone'));
        $this->begin_dt = $this->ConvertToDefaultDatabaseTimeZone($timestamp);
        return true;
    }

    public function get_end_timestamp(): ?Carbon {
        return $this->ConvertToLocalTimeZone($this->end_dt);
    }

    public function set_end_timestamp(Carbon $timestamp = null): bool {
        $timestamp->setTimezone(config('app.timezone'));
        $this->end_dt = $this->ConvertToDefaultDatabaseTimeZone($timestamp);
        return true;
    }

    public function get_notes(): ?string {
        return $this->notes;
    }

    public function set_notes(string $value = null) {
        $this->notes = $value;
    }

    public function is_bound_to_socket(): bool {
        if(intval($this->socket_id) > 0) {
            return true;
        }
        return false;
    }

    public function payments() {
        return $this->hasMany(SubscriptionConsumptionPayment::class);
    }

    public function costs_records() {
        return $this->hasMany(SubscriptionCostsRecord::class);
    }

    public function get_costs_record(Carbon $timestamp = null) {
    // TODO: posso trasformarla in una relazione, in modo da poterla caricare con eager loading?
        if(is_null($timestamp)) {
            $costs_record = $this->costs_records()
                                 ->whereNull('end_dt')
                                 ->orderBy('begin_dt', 'desc')
                                 ->limit(1)
                                 ->first();
        } else {
            $timestamp->setTimezone(config('app.timezone'));
            $costs_record = $this->costs_records()
                                 ->where('begin_dt', '<=', $timestamp)
                                 ->where(function($f) use ($timestamp) {
                                      $f->where('end_dt', '>', $timestamp)
                                      ->orWhereNull('end_dt');
                                  })
                                 ->orderBy('begin_dt', 'desc')
                                 ->limit(1)
                                 ->first();
        }
        return $costs_record;
    }

    public function current_costs_record() {
    // TODO: ora che ho scritto questa relazione, il metodo get_costs_record ha ancora senso?
        return $this->costs_records()
                    ->whereNull('end_dt')
                    ->orderBy('begin_dt', 'desc')
                    ->limit(1);
    }

    public function end_current_costs_record(Carbon $end_timestamp) {
    // setta l'end_dt del record attuale dei costi della subscription all'istante end_timestamp
        $end_timestamp->setTimezone(config('app.timezone'));
        $current_costs_record = $this->get_costs_record();
        if(($current_costs_record) && ($current_costs_record->count() > 0)) {
            $current_costs_record->set_end_dt($end_timestamp);
            $current_costs_record->save();
        }
    }

    public function get_power_cost(Carbon $timestamp = null): float {
        $power_cost = 0;
        $costs_record = $this->get_costs_record($timestamp);
        if($costs_record) {
            $power_cost = $costs_record->get_power_cost_per_kwh();
        }
        return $power_cost;
    }

    public function get_water_cost(Carbon $timestamp = null): float {
        $water_cost = 0;
        $costs_record = $this->get_costs_record($timestamp);
        if($costs_record) {
            $water_cost = $costs_record->get_water_cost_per_m3();
        }
        return $water_cost;
    }

    public function set_costs_and_save_costs_record(float $power_cost_per_kwh, float $water_cost_per_m3): bool {
    /* prende in ingresso i costi energia ed acqua per la subscription e - se sono diversi da quelle attualmente in vigore - chiude il vecchio record costi a (now - 1 secondo), scrive quello nuovo facendolo partire da (now) e restituisce true. Se i costi passati sono uguali a quelli attuali, non fa nulla e restituisce false. */
        /* TODO: forse sarebbe meglio separare il codice che verifica se i costi sono cambiati da quello che fa l'aggiornamento, facendo due metodi diversi da richiamare separatamente nel controller */
        $need_to_update_costs = true;
        $current_costs_record_exists = false;
        // leggo il record attuale dei costi per questo contratto:
        $current_costs_record = $this->get_costs_record();
        if(($current_costs_record) && ($current_costs_record->count() > 0)) {
        // se il contratto ha un record dei costi attualmente in vigore:
            // verifico se i costi che andrò a impostare sono diversi da quelli attuali:
            $current_costs_record_exists = true;
            if(($power_cost_per_kwh === $current_costs_record->get_power_cost_per_kwh()) &&
               ($water_cost_per_m3 === $current_costs_record->get_water_cost_per_m3())) {
                $need_to_update_costs = false;
            }
        }
        if(($need_to_update_costs) || (!$current_costs_record_exists)) {
        // se i costi passati sono diversi dai costi attuali del contratto (oppure se il contratto non ha attualmente nessun record costi):
            if($current_costs_record_exists) {
            // se il contratto ha un record dei costi attualmente in vigore:
                $one_second_ago = Carbon::now(config('app.timezone'));
                $one_second_ago->subSeconds(1);
                // imposto la data di termine del record dei costi attuale ad un secondo fa:
                $this->end_current_costs_record($one_second_ago);
            }
            // scrivo sul db il nuovo record, facendolo entrare in vigore all'istante now():
            $new_costs_record = new SubscriptionCostsRecord();
            $new_costs_record->set_subscription_id($this->get_id());
            $new_costs_record->set_power_cost_per_kwh($power_cost_per_kwh);
            $new_costs_record->set_water_cost_per_m3($water_cost_per_m3);
            $new_costs_record->set_begin_dt(Carbon::now(config('app.timezone')));
            $new_costs_record->set_end_dt(null);
            $new_costs_record->save();
            return true;
        }
        return false;
    }

    public function is_prepaid(Carbon $timestamp = null) {
        if(!is_null($timestamp)) {
            $timestamp->setTimezone(config('app.timezone'));
        }
        $costs_were_zero = $socket_was_on_with_zero_credit = false;
        if(($this->get_power_cost($timestamp) == 0) && ($this->get_water_cost($timestamp) == 0)) {
        // ATTENZIONE: la comparazione con == anzichè === è corretta, dato che i valori confrontati potrebbero essere di tipo diverso
            $costs_were_zero = true;
        }
        $last_snapshot = $this->get_last_socket_snapshot($timestamp);
        if($last_snapshot) {
            if(($last_snapshot->get_on_off_status() === SocketSnapshot::SOCKET_STATUS_ON) &&
               ($last_snapshot->get_residual_credit() === 0)) {
                $socket_was_on_with_zero_credit = true;
            }
        }
        if($this->is_bound_to_socket() || $costs_were_zero || $socket_was_on_with_zero_credit) {
        // se la subscription è una socket_subscription, se i suoi costi all'istante $timestamp erano entrambi a zero, oppure se all'istante timestamp la presa era accesa con credito a zero, restituisco false (ovvero: la subscription in quel momento era postpagata):
            return false;
        }
        return true;
    }

    public function is_postpaid(Carbon $timestamp = null) {
        return !($this->is_prepaid($timestamp));
    }

    public function thresholds_records() {
        return $this->hasMany(SubscriptionAlarmThresholdsRecord::class);
    }

    public function get_thresholds_record(Carbon $timestamp = null) {
    // TODO: posso trasformarla in una relazione, in modo da poterla caricare con eager loading? Rinominarla in current_thresholds_record()
        if(is_null($timestamp)) {
            $thresholds_record = $this->thresholds_records()
                                      ->whereNull('end_dt')
                                      ->orderBy('begin_dt', 'desc')
                                      ->limit(1)
                                      ->first();
        } else {
            $timestamp->setTimezone(config('app.timezone'));
            $thresholds_record = $this->thresholds_records()
                                      ->where('begin_dt', '<=', $timestamp)
                                      ->where(function($f) use ($timestamp) {
                                            $f->where('end_dt', '>', $timestamp)
                                              ->orWhereNull('end_dt');
                                      })
                                      ->orderBy('begin_dt', 'desc')
                                      ->limit(1)
                                      ->first();
        }
        return $thresholds_record;
    }

    public function end_current_thresholds_record(Carbon $end_timestamp) {
    // setta l'end_dt del record attuale dei costi della subscription all'istante end_timestamp
        $end_timestamp = Carbon::now(config('app.timezone'));
        $current_thresholds_record = $this->get_thresholds_record();
        if(($current_thresholds_record) && $current_thresholds_record->count() > 0) {
            $current_thresholds_record->set_end_dt($end_timestamp);
            $current_thresholds_record->save();
        }
    }

    public function get_min_power_alarm_threshold(Carbon $timestamp = null): float {
        $threshold = 0;
        $thresholds_record = $this->get_thresholds_record($timestamp);
        if($thresholds_record) {
            $threshold = $thresholds_record->get_min_power_threshold_kwh();
        }
        return $threshold;
    }

    public function get_max_power_alarm_threshold(Carbon $timestamp = null): float {
        $threshold = 0;
        $thresholds_record = $this->get_thresholds_record($timestamp);
        if($thresholds_record) {
            $threshold = $thresholds_record->get_max_power_threshold_kwh();
        }
        return $threshold;
    }

    public function get_max_water_tap_time_alarm_threshold(Carbon $timestamp = null): int {
        $threshold = 0;
        $thresholds_record = $this->get_thresholds_record($timestamp);
        if($thresholds_record) {
            $threshold = $thresholds_record->get_max_water_tap_time_threshold_minutes();
        }
        return $threshold;
    }

    public function get_min_socket_credit_alarm_threshold(Carbon $timestamp = null): float {
        $threshold = 0;
        $thresholds_record = $this->get_thresholds_record($timestamp);
        if($thresholds_record) {
            $threshold = $thresholds_record->get_min_credit_threshold();
        }
        return $threshold;
    }

    public function set_alarm_thresholds_and_save_thresholds_record(float $min_power_alarm_threshold_kwh, float $max_power_alarm_threshold_kwh, int $max_water_tap_time_alarm_threshold_minutes, float $min_credit_alarm_threshold_currency): bool {
    /* prende in ingresso le soglie allarmi per la subscription e - se sono diverse da quelle attualmente in vigore - chiude il vecchio record soglie a (now - 1 secondo), scrive quello nuovo facendolo partire da (now) e restituisce true. Se le soglie passate sono uguali a quelle attuali, non fa nulla e restituisce false. */
        /* TODO: forse sarebbe meglio separare il codice che verifica se le soglie sono cambiate da quello che fa l'aggiornamento, facendo due metodi diversi da richiamare separatamente nel controller */
        $need_to_update_thresholds = true;
        $current_thresholds_record_exists = false;
        // leggo il record attuale delle soglie allarmi per questo contratto:
        $current_thresholds_record = $this->get_thresholds_record();
        if(($current_thresholds_record) && ($current_thresholds_record->count() > 0)) {
        // se il contratto ha un record delle soglie allarmi attualmente in vigore:
            // verifico se le soglie che andrò a impostare sono diverse da quelle attuali:
            $current_thresholds_record_exists = true;
            if(($current_thresholds_record->get_min_power_threshold_kwh() === $min_power_alarm_threshold_kwh) &&
               ($current_thresholds_record->get_max_power_threshold_kwh() === $max_power_alarm_threshold_kwh) &&
               ($current_thresholds_record->get_max_water_tap_time_threshold_minutes() === $max_water_tap_time_alarm_threshold_minutes) &&
               ($current_thresholds_record->get_min_credit_threshold() === $min_credit_alarm_threshold_currency)) {
                $need_to_update_thresholds = false;
            }
        }
        if(($need_to_update_thresholds) || (!$current_thresholds_record_exists)) {
        // se le soglie passate sono diverse da quelle attualmente settate sul contratto (oppure se il contratto attualmente non ha nessun record soglie):
            if($current_thresholds_record_exists) {
            // e se il contratto ha un record delle soglie allarmi attualmente in vigore:
                $one_second_ago = Carbon::now(config('app.timezone'));
                $one_second_ago->subSeconds(1);
                // imposto la data di termine del record attuale ad un secondo fa:
                $this->end_current_thresholds_record($one_second_ago);
            }
            // scrivo sul db il nuovo record, facendolo entrare in vigore all'istante now():
            $new_thresholds_record = new SubscriptionAlarmThresholdsRecord();
            $new_thresholds_record->set_subscription_id($this->get_id());
            $new_thresholds_record->set_min_power_threshold_kwh($min_power_alarm_threshold_kwh);
            $new_thresholds_record->set_max_power_threshold_kwh($max_power_alarm_threshold_kwh);
            $new_thresholds_record->set_max_water_tap_time_threshold_minutes($max_water_tap_time_alarm_threshold_minutes);
            $new_thresholds_record->set_min_credit_threshold($min_credit_alarm_threshold_currency);
            $new_thresholds_record->set_begin_dt(Carbon::now(config('app.timezone')));
            $new_thresholds_record->set_end_dt(null);
            $new_thresholds_record->save();
            return true;
        }
        return false;
    }

    public function end_on_timestamp_and_save(Carbon $timestamp) {
    // termina la subscription all'istante $timestamp, settandone la data di termine al valore di timestamp, e facendo la stessa cosa per gli eventuali record attivi per i costi e le soglie allarmi. Attenzione: questo determinerà il salvataggio dei record della subscription, dei costi e delle soglie allarmi sul db
        $timestamp->setTimezone(config('app.timezone'));
        $this->set_end_timestamp($timestamp);
        $this->save();
        $this->end_current_costs_record($timestamp);
        $this->end_current_thresholds_record($timestamp);
    }

    public function socket_snapshots() {
        if(!($this->is_bound_to_socket())) {
            $socket_snapshots = $this->transponder
                                     ->socket_snapshots();
        } else {
            $socket_snapshots = $this->transponder
                                     ->socket_snapshots()
                                     ->where('socket_id', $this->socket_id);
        }
        $socket_snapshots = $socket_snapshots->where('link_status', '!=', SocketSnapshot::LINK_STATUS_NOT_CONNECTED)
                                             ->afterTimestamp($this->get_begin_timestamp(), false);
        if(!$this->is_ongoing()) {
            $socket_snapshots = $socket_snapshots->beforeTimestamp($this->get_begin_timestamp(), false);
        }
        return $socket_snapshots->olderFirst();
    }

    public function subscription_snapshots() {
        return $this->hasMany(SubscriptionSnapshot::class);
    }

    public function get_last_socket_snapshot(Carbon $timestamp = null): ?SocketSnapshot {
    // TODO: posso trasformarla in una relazione, in modo da poterla caricare con eager loading? Rinominarla in last_socket_snapshot()
        $last_socket_snapshot = SocketSnapshot::where('transponder_id', $this->transponder_id);
        if($this->is_bound_to_socket()) {
            $last_socket_snapshot = $last_socket_snapshot->where('socket_id', $this->socket_id);
        }
        $last_socket_snapshot = $last_socket_snapshot->afterTimestamp($this->get_begin_timestamp(), false);
        if(!$this->is_ongoing()) {
            $last_socket_snapshot = $last_socket_snapshot->beforeTimestamp($this->get_end_timestamp(), false);
        }
        if(!is_null($timestamp)) {
            $timestamp->setTimezone(config('app.timezone'));
            $last_socket_snapshot = $last_socket_snapshot->beforeTimestamp($timestamp, false);
        }
        return $last_socket_snapshot->newerFirst()
                                    ->first();
    }

    public function get_first_socket_snapshot_on_berth_after_timestamp($timestamp, $berth_id, bool $strict_after = null): ?SocketSnapshot {
    /* restituisce lo snapshot meno recente in cui la subscription stava usando la presa con logico $berth_id ed il cui timestamp sia all'interno dei timestamp di inizio e fine della subscription e:
        1) >= del parametro $timestamp (caso $strict_after non passato oppure $strict_after === false)
        2) > del parametro $timestamp (caso $strict_after passato e === true) */
    // TODO: posso trasformarla in una relazione, in modo da poterla caricare con eager loading? Rinominarla in first_socket_snapshot_on_berth_after_timestamp()
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_after) && ($strict_after === true)) {
            $compare_symbol = '>';
        } else {
            $compare_symbol = '>=';
        }
        if($timestamp < $this->get_begin_timestamp()) {
        // se il timestamp passato è successivo all'inizio della subscription
            // devo cercare solo a partire dall'inizio della subscription:
            $timestamp = $this->get_begin_timestamp();
        }
        if(!is_null($this->get_end_timestamp())) {
        // se la subscription è già terminata:
            // il limite massimo oltre il quale non devo cercare è il suo timestamp di fine:
            $subscription_upper_limit_dt = $this->get_end_timestamp();
        } else {
        // se la subscription è ancora in corso:
            // il limite massimo oltre il quale non devo cercare è now:
            $subscription_upper_limit_dt = Carbon::now(config('app.timezone'));
        }
        if($timestamp > $subscription_upper_limit_dt) {
        // se il timestamp passato va oltre il limite massimo oltre il quale non devo cercare:
            // lo ridimensiono al limite massimo trovato:
            $timestamp = $subscription_upper_limit_dt;
        }
        // inizio a costruire la query con la quale cerco lo snapshot:
        $first_snapshot_on_berth_after_timestamp = $this->socket_snapshots()
                                                        ->where('socket_id', $berth_id)
                                                        ->where('timestamp', $compare_symbol, $timestamp)
                                                        ->sortBy('timestamp')
                                                        ->take(1)
                                                        ->first();
        return $first_snapshot_on_berth_after_timestamp;
    }

    public function get_last_socket_snapshot_on_berth_before_timestamp($timestamp, $berth_id, bool $strict_before = null): ?SocketSnapshot {
    /* restituisce lo snapshot più recente in cui la subscription stava usando la presa con logico $berth_id ed il cui timestamp sia all'interno dei timestamp di inizio e fine della subscription e:
        1) <= del parametro $timestamp (caso $strict_before non passato oppure $strict_before === false)
        2) < del parametro $timestamp (caso $strict_before passato e === true) */
    // TODO: posso trasformarla in una relazione, in modo da poterla caricare con eager loading? Rinominarla in last_socket_snapshot_on_berth_before_timestamp()
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_before) && ($strict_before === true)) {
            $compare_symbol = '<';
        } else {
            $compare_symbol = '<=';
        }
        if(!$this->is_ongoing()) {
        // se la subscription è già terminata:
            // il limite massimo oltre il quale non devo cercare è il suo timestamp di fine:
            $subscription_upper_limit_dt = $this->get_end_timestamp();
        } else {
        // se la subscription è ancora in corso:
            // il limite massimo oltre il quale non devo cercare è now:
            $subscription_upper_limit_dt = Carbon::now(config('app.timezone'));
        }
        $last_socket_snapshot = SocketSnapshot::where('transponder_id', $this->transponder_id)
                                              ->where('socket_id', $berth_id)
                                              ->afterTimestamp($this->get_begin_timestamp(), false)
                                              ->beforeTimestamp($subscription_upper_limit_dt, false)
                                              ->where('timestamp', $compare_symbol, $timestamp);
        return $last_socket_snapshot->orderBy('timestamp', 'desc')->first();
    }

    public function get_consumption_on_berth_during_interval(Carbon $begin_dt, Carbon $end_dt, string $subject, int $berth_id, bool &$epower_id_has_changed_during_interval, int $mode = ConsumptionReportsController::REPORT_MODE_PLAIN): ?float {
    /* Restituisce uno scalare che rappresenta il consumo effettuato dalla subscription SUL POSTO BARCA SPECIFICATO nel periodo trascorso tra $begin_dt e $end_dt.
       Parametri:
        - Carbon $begin_dt (obbligatorio): istante di inizio dell'intervallo per la verifica del consumo
        - Carbon $end_dt (obbligatorio): istante di fine dell'intervallo per la verifica del consumo
        - string $subject (obbligatorio): grandezza per la quale si sta richiedendo la verifica del consumo. I valori possibili sono definiti sul ConsumptionReportsController affinchè possano essere usati anche in altri punti del software, e sono i seguenti:
            - ConsumptionReportsController::REPORT_VALUE_POWER
            - ConsumptionReportsController::REPORT_VALUE_WATER
            - ConsumptionReportsController::REPORT_VALUE_CREDIT  (TODO: ha senso qui o meglio tenerlo solo nell'analogo metodo del model Berth?)
        - int $berth_id: specifica per quale posto barca vanno verificati i consumi. E' un parametro obbligatorio, e se la subscription è legata ad un posto barca, deve essere popolato con il suo id. NOTA: se l'anagrafica del posto barca per il quale vanno calcolati i consumi non esiste sul db, il metodo restituirà null. // TODO: CREDO CHE QUESTA AFFERMAZIONE NON VALGA PIU': ORA IL METODO DOVREBBE RESTITUIRE IL CONSUMO CORRETTO ANCHE SE L'ANAGRAFICA DEL POSTO BARCA NON ESISTE ANCORA SUL DB. VERIFICARE.
        - bool &$epower_id_has_changed_during_interval (passato per riferimento): è usato come secondo valore di ritorno, e se dopo aver lanciato il metodo, risulta true, significa che durante l'intervallo in esame è stata cambiata l'epower su almeno uno dei posti barca sui quali la subscription è stata utilizzata. Uso questo valore per segnalare all'utente che ha richiesto un report di consumo (grafico/reportfile o view che sia) che i valori che gli sto mostrando non sono attendibili per via del salto nei contatori di consumo
        - int $mode (facoltativo): specifica se il consumo trovato deve essere rapportato al tempo trascorso nell'intervallo (valore ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME) oppure restituito così com'è senza ulteriori calcoli (valore ConsumptionReportsController::REPORT_MODE_PLAIN, default) */

        // TODO: ora che questo metodo usa la tabella SubscriptionSnapshots, valutare di rinominarlo per distinguere i metodi che servono a calcolare i consumi della subscription dentro la tabella SubscriptionSnapshot da quelli che calcolano i consumi della subscription USANDO i SubscriptionSnapshot

        $begin_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
        $berth = Berth::find($berth_id);
        $epower_id_has_changed_during_interval = false;
        switch($this->is_bound_to_socket()) {
            case false: 
               default:
            // se il contratto NON E' una socket_subscription:
                // verifico che la presa - se esiste sul db - abbia avuto sempre lo stesso epower_id per tutto il periodo in esame. Restituirò questo dato attraverso il parametro $epower_id_has_changed_during_interval, che mi è stato passato per riferimento:
                if(($berth) && ($berth->count() > 0)) {
                    $epower_id_has_changed_during_interval = !($berth->had_the_same_epower_id_during_interval($begin_dt, $end_dt));
                }
                // cerco il primo snapshot disponibile per la subscription prima di $begin_dt o all'istante $begin_dt stesso:
                $subscription_status_on_berth_at_begin_dt = $this->get_last_subscription_snapshot_on_berth_before_timestamp($begin_dt, $berth_id, false);
                if(!$subscription_status_on_berth_at_begin_dt) {
                // se sul db NON ESISTE uno snapshot relativo all'istante begin_dt per la subscription in esame:
                    // lo cerco al primo istante disponibile dopo di esso:
                    $subscription_status_on_berth_just_after_begin_dt = $this->get_first_subscription_snapshot_on_berth_after_timestamp($begin_dt, $berth_id, false);
                    if(!$subscription_status_on_berth_just_after_begin_dt) {
                    // se nemmeno così trovo uno snapshot:
                        // significa che la subscription in esame non ha snapshot sul db, quindi posso tornare 0 come valore di consumo:
                        return 0;
                    }
                    $subscription_status_on_berth_at_begin_dt = $subscription_status_on_berth_just_after_begin_dt;
                }
                $subscription_status_on_berth_at_end_dt = $this->get_last_subscription_snapshot_on_berth_before_timestamp($end_dt, $berth_id, false);
                if((!$subscription_status_on_berth_at_end_dt)) {
                // se non abbiamo trovato $socket_status_on_berth_at_end_dt (TODO: è possibile questa cosa se ho già trovato $subscription_status_on_berth_at_begin_dt?):
                    return 0;
                }
                if($subscription_status_on_berth_at_end_dt->get_timestamp() === $subscription_status_on_berth_at_begin_dt->get_timestamp()) {
                // se $subscription_status_on_berth_at_end_dt e $subscription_status_on_berth_at_begin_dt sono lo stesso snapshot, il consumo tra i due sarà zero:
                    return 0;
                }
                switch($subject) {
                    case ConsumptionReportsController::REPORT_VALUE_POWER:
                    default:
                        $consumption = $subscription_status_on_berth_at_end_dt->get_power_consumption_on_socket() - $subscription_status_on_berth_at_begin_dt->get_power_consumption_on_socket();
                        break;
                    case ConsumptionReportsController::REPORT_VALUE_WATER:
                        $consumption = $subscription_status_on_berth_at_end_dt->get_water_consumption_on_socket() - $subscription_status_on_berth_at_begin_dt->get_water_consumption_on_socket();
                        break;
                    case ConsumptionReportsController::REPORT_VALUE_CREDIT:
                        $consumption = $subscription_status_on_berth_at_end_dt->get_credit_consumption_on_socket() - $subscription_status_on_berth_at_begin_dt->get_credit_consumption_on_socket();
                }
                if($mode === ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME) {
                // se il chiamante ha richiesto di avere il consumo del contratto rapportato al tempo trascorso (kWh o m3/h):
                    // calcolo il tempo trascorso tra $begin_dt e $end_dt:
                    $total_time_interval_in_hours = $begin_dt->floatDiffInHours($end_dt, true);
                    if($total_time_interval_in_hours > 0) {
                    // se il numero di ore trascorse è > 0:
                        // divido il consumo per le ore trascorse:
                        $consumption = $consumption / $total_time_interval_in_hours;
                    } else {
                    // se il numero di ore trascorse è === 0, dividendo per $total_time_interval_in_hours andrei in exception per division by zero.
                        // restituisco invece direttamente 0:
                        $consumption = 0;
                    }
                }
                break;
            case true: 
            // se il contratto E' una socket_subscription:
                // mi è sufficiente calcolare il consumo postpagato effettuato sul posto barca nel periodo richiesto, usando l'apposito metodo della classe Berth):
                $consumption = $berth->get_consumption_in_interval($begin_dt, $end_dt, $subject, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_POSTPAID, $mode);
        }
        return $consumption;
    }

    public function get_consumption_on_all_plant_during_interval(Carbon $begin_dt, Carbon $end_dt, string $subject, bool &$epower_id_has_changed_during_interval, int $mode = ConsumptionReportsController::REPORT_MODE_PLAIN): ?float {
    /* Restituisce uno scalare che rappresenta il consumo effettuato dalla subscription NELL'INTERO IMPIANTO durante il periodo trascorso tra $begin_dt e $end_dt.
        Parametri:
        - Carbon $begin_dt (obbligatorio): istante di inizio dell'intervallo per la verifica del consumo
        - Carbon $end_dt (obbligatorio): istante di fine dell'intervallo per la verifica del consumo
        - string $subject (obbligatorio): grandezza per la quale si sta richiedendo la verifica del consumo. I valori possibili sono definiti sul ConsumptionReportsController affinchè possano essere usati anche in altri punti del software, e sono i seguenti:
            - ConsumptionReportsController::REPORT_VALUE_POWER
            - ConsumptionReportsController::REPORT_VALUE_WATER
            - ConsumptionReportsController::REPORT_VALUE_CREDIT
        - bool &$epower_id_has_changed_during_interval (passato per riferimento): è usato come secondo valore di ritorno, e se dopo aver lanciato il metodo, risulta true, significa che durante l'intervallo in esame è stata cambiata l'epower su almeno uno dei posti barca sui quali la subscription è stata utilizzata. Uso questo valore per segnalare all'utente che ha richiesto un report di consumo (grafico/reportfile o view che sia) che i valori che gli sto mostrando non sono attendibili per via del salto nei contatori di consumo
        - int $mode (facoltativo): specifica se il consumo trovato deve essere rapportato al tempo trascorso nell'intervallo (valore ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME) oppure restituito così com'è senza ulteriori calcoli (valore ConsumptionReportsController::REPORT_MODE_PLAIN, default) */
        $begin_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
        // la subscription potrebbe aver avuto inizio prima dell'inizio del periodo richiesto, oppure essere terminata prima della sua fine: in questo caso, modifico il periodo in esame per farlo rientrare tra l'inizio e la fine della subscription:
        $subscription_begin_dt = $this->get_begin_timestamp();
        if($subscription_begin_dt > $begin_dt) {
            $begin_dt = $subscription_begin_dt;
        }
        $subscription_end_dt = $this->get_end_timestamp();
        if((is_null($subscription_end_dt)) || ($subscription_end_dt < Carbon::now(config('app.timezone')))) {
            $end_dt = Carbon::now(config('app.timezone'));
        }
        $berth_ids_on_which_to_calculate_consumption = [];
        if($this->is_bound_to_socket()) {
        // se il contratto è legato ad un posto barca:
            // cerco i consumi effettuati su quello:
            $berth_ids_on_which_to_calculate_consumption[] = $this->socket_id;
        } else {
        // se il contratto NON è legato ad un posto barca:
        	// cerco tutti i posti barca sui quali è stata usata la subscription nell'intervallo richiesto:
            $berth_ids_on_which_to_calculate_consumption = SocketSnapshot::where('transponder_id', $this->transponder_id)
                                                                         ->afterTimestamp($begin_dt, false)
                                                                         ->beforeTimestamp($end_dt, false)
                                                                         ->select('socket_id')
                                                                         ->distinct()
                                                                         ->get()
                                                                         ->toArray();
        }
        $total_subscription_consumption_during_interval = 0;
        $epower_id_has_changed_during_interval = false;
        if(count($berth_ids_on_which_to_calculate_consumption)) {
            foreach($berth_ids_on_which_to_calculate_consumption as $current_berth_id) {
                // TODO: Non ho capito se $current_berth_id è effettivamente sempre un array o meno, perciò per ora differenzio il recupero in base al suo tipo per evitare altri errori
                if(is_iterable($current_berth_id)) {
                    $berth_id = intval($current_berth_id['socket_id']);
                } else {
                    $berth_id = intval($current_berth_id);
                }
                // verifico che la presa - se esiste sul db - abbia avuto sempre lo stesso epower_id per tutto il periodo in esame. Metto il valore di $epower_id_has_changed_during_interval in OR con se stesso, in modo da avere true se - alla fine del ciclo - su almeno una delle prese è stata cambiata l'epower durante l'intervallo in esame. Restituirò questo dato attraverso il parametro $epower_id_has_changed_during_interval, che mi è stato passato per riferimento:
                $berth = Berth::find($berth_id);
                if(($berth) && ($berth->count() > 0)) {
                    $epower_id_has_changed_during_interval = $epower_id_has_changed_during_interval || (!($berth->had_the_same_epower_id_during_interval($begin_dt, $end_dt)));
                    $total_subscription_consumption_during_interval += $this->get_consumption_on_berth_during_interval($begin_dt, $end_dt, $subject, $berth_id, $mode);
                }
            }
        }
        return $total_subscription_consumption_during_interval;
    }

    public function is_ongoing(Carbon $timestamp = null): bool {
    // restituisce true se il contratto era in corso all'istante $timestamp, false in caso contrario.
        if(is_null($timestamp)) {
        // se il parametro $timestamp non è stato passato, la verifica verrà fatta per default sull'istante attuale_
            $timestamp = Carbon::now(config('app.timezone'));
        }
        if($this->get_begin_timestamp() <= $timestamp) {
        // se il contratto è iniziato prima di $timestamp (o all'istante $timestamp stesso):
            if(is_null($this->get_end_timestamp()) || ($this->get_end_timestamp() >= $timestamp)) {
            // ed è terminato dopo $timestamp (oppure non è ancora terminato):
                // restituisce true:
                return true;
            }
        }
        // in tutti gli altri casi restituisce false:
        return false;
    }

    public function get_last_subscription_snapshot_before_timestamp(Carbon $timestamp, bool $strict_before = null): ?SubscriptionSnapshot {
    /*  restituisce il primo snapshot della subscription in cui il timestamp sia
            1) <= del parametro $timestamp (caso $strict_before non passato oppure strict_before === false)
            2) < del parametro $timestamp (caso $strict_before passato oppure e === true) */

        // TODO: posso trasformarla in una relazione, in modo da poterla caricare con eager loading? Rinominarla in last_subscription_snapshot_before_timestamp()

        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_before) && ($strict_before === true)) {
            $compare_symbol = '<';
        } else {
            $compare_symbol = '<=';
        }
        $last_subscription_snapshot_before_timestamp = $this->subscription_snapshots()
                                                            ->where('timestamp', $compare_symbol, $timestamp)
                                                            ->orderBy('timestamp', 'desc')
                                                            ->take(1)
                                                            ->first();
        return $last_subscription_snapshot_before_timestamp;
    }

    public function get_last_subscription_snapshot_on_berth_before_timestamp(Carbon $timestamp, int $socket_id, bool $strict_before = null): ?SubscriptionSnapshot {
    /*  restituisce il primo snapshot della subscription in cui il socket_id sia uguale a quello passato ed il timestamp sia
            1) <= del parametro $timestamp (caso $strict_before non passato oppure strict_before === false)
            2) < del parametro $timestamp (caso $strict_before passato oppure e === true) */

        // TODO: posso trasformarla in una relazione, in modo da poterla caricare con eager loading? Rinominarla in last_subscription_snapshot_on_berth_before_timestamp()
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_before) && ($strict_before === true)) {
            $compare_symbol = '<';
        } else {
            $compare_symbol = '<=';
        }
        $snapshot_on_socket_at_timestamp = $this->subscription_snapshots()
                                                ->where('socket_id', $socket_id)
                                                ->where('timestamp', $compare_symbol, $timestamp)
                                                ->orderBy('timestamp', 'desc')
                                                ->take(1)
                                                ->first();
        return $snapshot_on_socket_at_timestamp;
    }

    public function get_first_subscription_snapshot_on_berth_after_timestamp(Carbon $timestamp, int $socket_id, bool $strict_after = null): ?SubscriptionSnapshot {
    /*  restituisce il primo snapshot della subscription in cui il socket_id sia uguale a quello passato edil timestamp sia
            1) >= del parametro $timestamp (caso $strict_after non passato oppure $strict_after === false)
            2) > del parametro $timestamp (caso $strict_after passato oppure e === true) */

        // TODO: posso trasformarla in una relazione, in modo da poterla caricare con eager loading? Rinominarla in first_subscription_snapshot_on_berth_after_timestamp()
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_after) && ($strict_after === true)) {
            $compare_symbol = '>';
        } else {
            $compare_symbol = '>=';
        }
        $first_snapshot_on_berth_after_timestamp = $this->subscription_snapshots()
                                                        ->where('socket_id', $socket_id)
                                                        ->where('timestamp', $compare_symbol, $timestamp)
                                                        ->orderBy('timestamp', 'asc')
                                                        ->take(1)
                                                        ->first();
        return $first_snapshot_on_berth_after_timestamp;
    }

    public function subscription_costs_have_changed_between_timestamps(Carbon $timestamp1, Carbon $timestamp2): bool {
    // restituisce true se tra i due timestamp passati è cambiato almeno uno dei valori dei costi, false altrimenti
        $costs_have_changed_between_timestamps = false;
        $timestamp1->setTimezone(config('app.timezone'));
        $timestamp2->setTimezone(config('app.timezone'));
        $record_1_power_cost = $record_1_water_cost = $record_2_power_cost = $record_2_water_cost = 0;
        if(!is_null($costs_record_1 = $this->get_costs_record($timestamp1)) && ($costs_record_1->count() > 0)) {
            $record_1_power_cost = $costs_record_1->get_power_cost_per_kwh();
            $record_1_water_cost = $costs_record_1->get_water_cost_per_m3();
        }
        if(!is_null($costs_record_2 = $this->get_costs_record($timestamp2)) && ($costs_record_2->count() > 0)) {
            $record_2_power_cost = $costs_record_2->get_power_cost_per_kwh();
            $record_2_water_cost = $costs_record_2->get_water_cost_per_m3();
        }
        $costs_have_changed_between_timestamps = $costs_have_changed_between_timestamps || ($record_1_power_cost !== $record_2_power_cost);
        $costs_have_changed_between_timestamps = $costs_have_changed_between_timestamps || ($record_1_water_cost !== $record_2_water_cost);
        return $costs_have_changed_between_timestamps;
    }

    public function subscription_alarm_thresholds_have_changed_between_timestamps(Carbon $timestamp1, Carbon $timestamp2): bool {
    // restituisce true se tra i due timestamp passati è cambiato almeno uno dei valori dei costi, false altrimenti
        $alarm_thresholds_have_changed_between_timestamps = false;
        $timestamp1->setTimezone(config('app.timezone'));
        $timestamp2->setTimezone(config('app.timezone'));
        $record_1_min_power_threshold = $record_1_max_power_threshold = $record_1_max_water_tap_time_threshold = $record_1_min_credit_threshold = $record_2_min_power_threshold = $record_2_max_power_threshold = $record_2_max_water_tap_time_threshold = $record_2_min_credit_threshold = 0;
        if(!is_null($thresholds_record_1 = $this->get_thresholds_record($timestamp1)) && ($thresholds_record_1->count() > 0)) {
            $record_1_min_power_threshold = $thresholds_record_1->get_min_power_threshold_kwh();
            $record_1_max_power_threshold = $thresholds_record_1->get_max_power_threshold_kwh();
            $record_1_max_water_tap_time_threshold = $thresholds_record_1->get_max_water_tap_time_threshold_minutes();
            $record_1_min_credit_threshold = $thresholds_record_1->get_min_credit_threshold();
        }
        if(!is_null($thresholds_record_2 = $this->get_thresholds_record($timestamp2)) && ($thresholds_record_1->count() > 0)) {
            $record_2_min_power_threshold = $thresholds_record_2->get_min_power_threshold_kwh();
            $record_2_max_power_threshold = $thresholds_record_2->get_max_power_threshold_kwh();
            $record_2_max_water_tap_time_threshold = $thresholds_record_2->get_max_water_tap_time_threshold_minutes();
            $record_2_min_credit_threshold = $thresholds_record_2->get_min_credit_threshold();
        }
        $alarm_thresholds_have_changed_between_timestamps = $alarm_thresholds_have_changed_between_timestamps || ($record_1_min_power_threshold !== $record_2_min_power_threshold);
        $alarm_thresholds_have_changed_between_timestamps = $alarm_thresholds_have_changed_between_timestamps || ($record_1_max_power_threshold !== $record_2_max_power_threshold);
        $alarm_thresholds_have_changed_between_timestamps = $record_1_max_water_tap_time_threshold || ($record_2_max_water_tap_time_threshold !== $record_2_max_power_threshold);
        $alarm_thresholds_have_changed_between_timestamps = $record_1_max_water_tap_time_threshold || (floatval($record_1_min_credit_threshold) !== floatval($record_2_min_credit_threshold));      // sul credito uso floatval() perchè se una subscription è postpagata, la soglia è null
        return $alarm_thresholds_have_changed_between_timestamps;
    }

    public function get_berths_on_which_subscription_consumed() {
    // restituisce un array contenente i posti barca sui quali il contratto ha consumato
        $berths = [];
        $subscription_snapshots = $this->subscription_snapshots;
        if($subscription_snapshots->count() > 0) {
            $subscription_snapshots = $subscription_snapshots->sortByDesc('timestamp')
                                                             ->groupBy('socket_id')
                                                             ->map
                                                             ->only('socket_id');
            foreach($subscription_snapshots as $key => $value) {
                $berths[] = Berth::find($key);
            }
        }
        return $berths;
    }

    public function get_berths_on_which_subscription_consumed_pero_meglio() {
    // restituisce una collection contenente ciascuno dei posti barca sui quali il contratto ha consumato
        // TODO: sto provando a riscrivere meglio il metodo get_berths_on_which_subscription_consumed(). Inoltre, credo che debba restituire un array delle istanze dei Berth, non solo i loro id (verificare). Una volta finito, rinominare questo metodo ed usarlo al posto di quello attuale
        // TODO: mi ci vorrebbe una relazione BelongsToManyThrough che su eloquent non esiste nativamente. Forse questo package la fornisce, verificare: https://github.com/staudenmeir/belongs-to-through
        // TODO: forse ho trovato, provo così
        $subscription_snapshots_berth_ids = $this->subscription_snapshots()->orderBy('timestamp', 'desc')->get('socket_id');
        return Berth::whereIn('id', $subscription_snapshots_berth_ids)->get('id')->toArray();
    }

    public function scopeExcludeSupervisorRelatedSubscriptions($query) {        // TODO: posso usare questo scope dappertutto quando voglio nascondere le supervisorrelatedsubscriptions, mi sa che a volte lo faccio a mano
        return $query->whereHas('user', function($query) {
            $query->where('access_level', '<', User::SUPERVISOR_USER_ACCESS_LEVEL);
        });
    }

    public function scopeActive($query, $timestamp = null) {   // TODO: posso usare active dappertutto invece dell'is_null(end_dt), però occhio a  controllare quanti record voglio (se serve - usarlo in combinazione con newerFirst()/latest). In questo modo posso anche evitare il problema dell'esposizione delle proprietà del model all'esterno!
        if(is_null($timestamp)) {
            $timestamp = Carbon::now();
        }
        return $query->where('subscriptions.begin_dt', '<=', $timestamp)
                     ->where(function($query) use ($timestamp) {
                        $query->whereNull('subscriptions.end_dt')
                              ->orWhere('subscriptions.end_dt', '>', $timestamp);
                     })
                     ->newerFirst();
    }

    public function scopeActiveInInterval($query, $start_dt, $end_dt) {
    // restituisce tutte le subscriptions che risultano attive durante parte dell'intervallo compreso tra $start_dt e $end_dt (quindi comprese quelle iniziate prima e/o finite dopo e iniziate prima o durante ed ancora in corso), ordinate dalla più recente alla meno recente. NOTA: E' DIVERSA DA scopeActiveDuringWholeInterval!!
        $start_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
                                                                                                    // devo restituire tutte le subscriptions:
        return $query->where(function($query) use ($start_dt, $end_dt) {                            // 1. iniziate prima di $begin_dt o all'istante $begin_dt, e:
                        $query->where('subscriptions.begin_dt', '<=', $start_dt)
                              ->where(function($query2) use ($end_dt) {
                                    $query2->where('subscriptions.end_dt', '<' , $end_dt)                         // 1a. finite prima di $end_dt
                                           ->orWhere('subscriptions.end_dt', '>=' , $end_dt);                     // 1b. finite dopo $end_dt o all'istante $end_dt stesso
                              });  
                    })
                    ->orWhere(function($query) use ($start_dt, $end_dt) {                           // oppure 2. iniziate dopo $begin_dt e:
                        $query->where('subscriptions.begin_dt', '>', $start_dt)
                              ->where(function($query2) use ($start_dt, $end_dt) {
                                    $query2->where('subscriptions.end_dt', '<' , $end_dt)                         // 2a. finite prima di $end_dt
                                            ->orWhere(function($query3) use ($end_dt) {             // oppure 2b. ancora in corso all'istante $end_dt, per uno dei seguenti motivi:
                                                $query3->whereNull('subscriptions.end_dt')                        // 2b-1. in corso perchè hanno data di fine === null
                                                       ->orWhere('subscriptions.end_dt', '>=' , $end_dt);         // oppure 2b-2. in corso perchè hanno data di fine > $end_dt
                                            });
                              }); 
                    })
                    ->newerFirst();
    }

    public function scopeActiveDuringWholeInterval($query, $start_dt, $end_dt) {
    // restituisce tutte le subscriptions che sono risultate in corso per TUTTA LA DURATA dell'intervallo compreso tra $start_dt ed $end_dt, ordinate dalla più recente alla meno recente. NOTA: E' DIVERSA DA scopeActiveinInterval()!!
        $start_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
                                                                                                    // devo restituire tutte le subscriptions:
        return $query->where(function($query) use ($start_dt, $end_dt) {
                        $query->where('subscriptions.begin_dt', '<=', $start_dt)                                  // iniziate prima di $begin_dt o all'istante $begin_dt, e:
                              ->where(function($query2) use($end_dt) {                              // ancora in corso all'istante $end_dt, per uno dei seguenti motivi:
                                    $query2->whereNull('subscriptions.end_dt')                                    // 1. in corso perchè hanno data di fine === null
                                           ->orWhere('subscriptions.end_dt', '>=', $end_dt);                      // oppure 2. in corso perchè hanno data di fine > $end_dt
                        });
                     })
                     ->newerFirst();
    }

    public function scopeEnded($query) {    // TODO: posso usare ended() dappertutto invece del !is_null(end_dt), però occhio a  controllare quanti record voglio (se serve - usarlo in combinazione con newerFirst())    // TODO: forse posso anche semplificare scrivendola come negazione di scopeActive()
        return $query->whereNotNull('subscriptions.end_dt');          // TODO: DA SISTEMARE, DEVO POTER PASSARE UN PARAMETRO E - SE PASSATO - CONFRONTARE CON QUELLO (per ora non gestisce le subscriptions con scadenza futura)
    }

    public function scopeNewerFirst($query) {           // TODO: posso usare newerFirst() dappertutto invece dell'orderby, però occhio a controllare quanti record voglio (potrei anche fare un take(1) qui dentro. Eventualmente nominare quella che ne prende una come newest()/newerFirst() e basta)
        return $query->orderBy('subscriptions.begin_dt', 'desc');
    }

    public function scopePrepaid($query) {
        return $query->join('subscription_costs', 'subscription_costs.subscription_id', '=', 'subscriptions.id')
                     ->where(function($query) { 
                            $query->where(function($nestedQuery1) { 
                                    $nestedQuery1->where('subscription_costs.power_cost_per_kwh', '!=', 0)
                                                 ->whereNotNull('subscription_costs.power_cost_per_kwh'); 
                            }); 
                     })
                     ->where(function($query) { 
                            $query->where(function($nestedQuery2) { 
                                    $nestedQuery2->where('subscription_costs.water_cost_per_m3', '!=', 0)
                                                 ->whereNotNull('subscription_costs.water_cost_per_m3'); 
                            }); 
                     });
    }

    public function scopePostpaid($query) {
        return $query->leftJoin('subscription_costs', 'subscription_costs.subscription_id', '=', 'subscriptions.id')
                     ->where(function($query) { 
                            $query->where(function($nestedQuery1) { 
                                    $nestedQuery1->where('subscription_costs.power_cost_per_kwh', 0)
                                                 ->orWhereNull('subscription_costs.power_cost_per_kwh'); 
                            })
                            ->where(function($nestedQuery2) { 
                                    $nestedQuery2->where('subscription_costs.water_cost_per_m3', 0)
                                                 ->orWhereNull('subscription_costs.water_cost_per_m3');
                            }); 
                     });
    }
}
