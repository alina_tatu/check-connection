<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use App\Http\Controllers\ConsumptionReportsController;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MarinaOrPontoonConsumptionExcelReport implements FromView, ShouldAutoSize {
    protected $report_interval_begin_timestamp, $report_interval_end_timestamp;
    protected $total_plant_power_consumption_in_interval, $total_plant_water_consumption_in_interval;
    protected $berth_consumption_details_array;
    protected $report_subject;
    protected $pontoon_name;
    protected $epower_id_has_changed_during_interval;

    function __construct($report_interval_begin_timestamp, $report_interval_end_timestamp, $total_plant_power_consumption_in_interval, $total_plant_water_consumption_in_interval, $berth_consumption_details_array, $epower_id_has_changed_during_interval, $report_subject, $pontoon_name = null) {
        $this->report_subject = $report_subject;
        if(!is_null($pontoon_name)) {
            $this->pontoon_name = $pontoon_name;
        }
        $this->report_interval_begin_timestamp = $report_interval_begin_timestamp;
        $this->report_interval_end_timestamp = $report_interval_end_timestamp;
        $this->total_plant_power_consumption_in_interval = floatval($total_plant_power_consumption_in_interval);
        $this->total_plant_water_consumption_in_interval = floatval($total_plant_water_consumption_in_interval);
        $this->berth_consumption_details_array = $berth_consumption_details_array;
        $this->epower_id_has_changed_during_interval = $epower_id_has_changed_during_interval;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View {
        switch($this->report_subject) {
            case ConsumptionReportsController::SUBJECT_TYPE_MARINA:
            default:
                $report_title_part_before_interval = ucfirst(__('consumption_report_files.xls_title_for_marina_part_1'));
                break;
            case ConsumptionReportsController::SUBJECT_TYPE_PONTOON:
                $report_title_part_before_interval = ucfirst(__('consumption_report_files.xls_title_for_pontoon_part_1'));
        }
        $view_data_array = ['report_title_part_before_interval' => $report_title_part_before_interval,
                            'report_interval_begin_timestamp' => $this->report_interval_begin_timestamp->setTimezone(config('dates_currency_and_locale.local_timezone')),
                            'report_interval_end_timestamp' => $this->report_interval_end_timestamp->setTimezone(config('dates_currency_and_locale.local_timezone')),
                            'total_plant_power_consumption_in_interval' => $this->total_plant_power_consumption_in_interval,
                            'total_plant_water_consumption_in_interval' => $this->total_plant_water_consumption_in_interval,
                            'berth_consumption_details_array' => $this->berth_consumption_details_array,
                            'epower_id_has_changed_during_interval' => $this->epower_id_has_changed_during_interval];
        if($this->report_subject === ConsumptionReportsController::SUBJECT_TYPE_PONTOON) {
            $view_data_array['pontoon_name'] = $this->pontoon_name;
        }
        return view('reports.xls.marina_or_pontoon_consumption_report', $view_data_array);
    }
}
