<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class SubscriptionConsumptionExcelReport  implements FromView, ShouldAutoSize {
    protected $subscription_description;
    protected $report_interval_begin_timestamp, $report_interval_end_timestamp;
    protected $subscription_power_consumption_in_interval, $subscription_water_consumption_in_interval;
    protected $epower_id_has_changed_during_interval;

    function __construct($subscription_description, $report_interval_begin_timestamp, $report_interval_end_timestamp, $subscription_power_consumption_in_interval, $subscription_water_consumption_in_interval, $epower_id_has_changed_during_interval) {
        $this->subscription_description = $subscription_description;
        $this->report_interval_begin_timestamp = $report_interval_begin_timestamp;
        $this->report_interval_end_timestamp = $report_interval_end_timestamp;
        $this->subscription_power_consumption_in_interval = floatval($subscription_power_consumption_in_interval);
        $this->subscription_water_consumption_in_interval = floatval($subscription_water_consumption_in_interval);
        $this->epower_id_has_changed_during_interval = $epower_id_has_changed_during_interval;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View {
        return view('reports.xls.berth_or_subscription_consumption_report', ['report_title_part_before_subject_description' => ucfirst(__('consumption_report_files.xls_title_for_subscription_part_1')),
                                                                             'subject_description' => $this->subscription_description,
                                                                             'report_interval_begin_timestamp' => $this->report_interval_begin_timestamp->setTimezone(config('dates_currency_and_locale.local_timezone')),
                                                                             'report_interval_end_timestamp' => $this->report_interval_end_timestamp->setTimezone(config('dates_currency_and_locale.local_timezone')),
                                                                             'berth_power_consumption_in_interval' => $this->subscription_power_consumption_in_interval,
                                                                             'berth_water_consumption_in_interval' => $this->subscription_water_consumption_in_interval,
                                                                             'epower_id_has_changed_during_interval' => $this->epower_id_has_changed_during_interval]);
    }
}
