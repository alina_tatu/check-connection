<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use DateTimeInterface;

/**
 * App\SubscriptionSnapshot
 *
 * @property int $id
 * @property int $subscription_id
 * @property Carbon $timestamp
 * @property int $socket_id
 * @property string $subscription_total_power_consumption
 * @property string $subscription_total_water_consumption
 * @property string|null $subscription_total_credit_consumption
 * @property string $subscription_power_consumption_on_socket
 * @property string $subscription_water_consumption_on_socket
 * @property string|null $subscription_credit_consumption_on_socket
 * @property-read \App\Berth $berth
 * @property-read \App\Subscription $subscription
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionSnapshot newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionSnapshot newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionSnapshot query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereSocketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereSubscriptionCreditConsumptionOnSocket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereSubscriptionPowerConsumptionOnSocket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereSubscriptionTotalCreditConsumption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereSubscriptionTotalPowerConsumption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereSubscriptionTotalWaterConsumption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereSubscriptionWaterConsumptionOnSocket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot whereTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionSnapshot withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class SubscriptionSnapshot extends Model {
    use Cachable;
    use Traits\TimeZoneAware;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subscription_snapshots';
    protected $primaryKey = 'id';

    public $timestamps = false;
    protected $dates = ['timestamp'];
    public $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subscription_id',
        'timestamp',
        'socket_id',
        'subscription_total_power_consumption',
        'subscription_total_water_consumption',
        'subscription_total_credit_consumption',
        'subscription_power_consumption_on_socket',
        'subscription_water_consumption_on_socket',
        'subscription_credit_consumption_on_socket',
    ];

    protected function serializeDate(DateTimeInterface $date) {
    // metodo per mantenere retrocompatibilità dopo l'upgrade a Laravel 7 (v. https://laravel.com/docs/7.x/upgrade#date-serialization)
        return $date->format('Y-m-d H:i:s');
    }

    public function get_id(): int {
    // restituisce l'id del record snapshot
        return intval($this->attributes[$this->primaryKey]);
    }

    public function set_id(int $id) {
        $this->attributes[$this->primaryKey] = $id;
    }

    public function set_subscription_id(int $subscription_id) {
        $this->subscription_id = $subscription_id;
    }

    public function subscription() {
        return $this->belongsTo(Subscription::class);
    }

    public function berth() {
        return $this->belongsTo(Berth::class, 'socket_id');
    }

    public function set_berth_id($value): bool {
        $this->socket_id = $value;
        return true;
    }

    public function get_timestamp(): Carbon {
        return $this->ConvertToLocalTimeZone($this->timestamp);
    }

    public function set_timestamp(Carbon $timestamp): bool {
        $this->timestamp = $this->ConvertToDefaultDatabaseTimeZone($timestamp);
        return true;
    }

    public function get_total_power_consumption(): float {
        return floatval($this->subscription_total_power_consumption);
    }

    public function set_total_power_consumption(float $value) {
        $this->subscription_total_power_consumption = $value;
    }

    public function get_total_water_consumption(): float {
        return floatval($this->subscription_total_water_consumption);
    }

    public function set_total_water_consumption(float $value) {
        $this->subscription_total_water_consumption = $value;
    }

    public function get_total_credit_consumption() {
        if(!is_null($this->subscription_total_credit_consumption)) {
            return floatval($this->subscription_total_credit_consumption);
        } else {
            return null;
        }
    }

    public function set_total_credit_consumption($value) {
        $this->subscription_total_credit_consumption = $value;
    }

    public function get_power_consumption_on_socket(): float {
        return floatval($this->subscription_power_consumption_on_socket);
    }

    public function set_power_consumption_on_socket(float $value) {
        $this->subscription_power_consumption_on_socket = $value;
    }

    public function get_water_consumption_on_socket(): float {
        return floatval($this->subscription_water_consumption_on_socket);
    }

    public function set_water_consumption_on_socket(float $value) {
        $this->subscription_water_consumption_on_socket = $value;
    }

    public function get_credit_consumption_on_socket() {
        if(!is_null($this->subscription_credit_consumption_on_socket)) {
            return floatval($this->subscription_credit_consumption_on_socket);
        } else {
            return null;
        }
    }

    public function set_credit_consumption_on_socket($value) {
        $this->subscription_credit_consumption_on_socket = $value;
    }
}