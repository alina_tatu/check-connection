<?php

namespace App\Http\Middleware;

use Closure;

class set_app_locale_on_login_based_on_user_preference {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        app()->setLocale($request->user()->get_language_preference());
        return $next($request);
    }
}
