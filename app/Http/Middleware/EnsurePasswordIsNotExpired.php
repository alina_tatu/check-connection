<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class EnsurePasswordIsNotExpired {
// redireziona l'utente alla view di cambio password se sono passati almeno config('auth.passwords_expire_interval_days') dall'ultimo cambio di password oppure se il flag needs_password_change del suo record è true
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $user = $request->user();
        // leggo il timestamp dell'ultimo cambio di password per l'utente loggato:
        $password_changed_at = new Carbon(($user->password_changed_at) ? $user->password_changed_at : $user->get_created_at_timestamp());
        $max_days_since_last_password_change = config('auth.passwords_expire_interval_days');
        // forzo l'utente al cambio della password se:
        if(!is_null($max_days_since_last_password_change) && (Carbon::now()->diffInDays($password_changed_at) >= config('auth.passwords_expire_interval_days'))) {
        // 1. la configurazione prevede che la password venga cambiata ogni auth.passwords_expire_interval_days ed il termine è già trascorso
            return redirect()->route('password.expired');
        } else if($user->needs_password_change() === true) {
        // 2. se il record dell'utente ha il flag needs_password_change a 1
            return redirect()->route('password.expired');
        }
        return $next($request);
    }
}
