<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class SetDebugModeIfLoggedUserIsSupervisor      // lo uso per mostrare la barra di debug bar (https://github.com/barryvdh/laravel-debugbar) se l'utente loggato è un supervisor, indipendentemente dai valori di app.environment e app.debug configurati in .env
// TODO: RICORDARE DI TOGLIERLO IN PRODUZIONE: POTREI DISINSTALLARE LA DEBUGBAR E QUESTO POTREBBE PRODURRE UN ERRORE!
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(Auth::check()) {
            $logged_user = User::find(Auth::id());
            if($logged_user && $logged_user->is_supervisor()) {
                Config::set('app.env', 'development');
                Config::set('app.debug', true);
            }
        }       
        return $next($request);
    }
}
