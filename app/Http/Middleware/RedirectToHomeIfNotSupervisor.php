<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\User;

use Closure;

class RedirectToHomeIfNotSupervisor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $logged_user = User::find(Auth::id());
        if($logged_user && $logged_user->is_supervisor()) {
            return $next($request);
        }
        return redirect('/home');
    }
}
