<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Plant;
use App\Jobs\GenerateConsumptionReport;

class PontoonController extends Controller {
    public function get_consumption_report_file(string $pontoon_name, int $begin_epoch_timestamp, int $end_epoch_timestamp, string $file_type) {
        $need_to_redirect_back = false;
        $flash_message_for_redirect = null;
        $pontoon_name = urldecode($pontoon_name);
        $all_berths_in_pontoon = Plant::get_all_berths_in_pontoon($pontoon_name);
        if((!$all_berths_in_pontoon) || ($all_berths_in_pontoon->count() === 0)) {
        // se non ci sono posti barca corrispondenti al pontile richiesto, redireziono alla pagina di provenienza con un flash_message di errore:
            $need_to_redirect_back = true;
            $flash_message_for_redirect = ucfirst(__('consumption_reports.cannot_generate_consumption_report_because_no_berths_for_the_selected_pontoon_msg'));
        }
        // valido i timestamp passati (mantengo la timezone perchè altrimenti laravel forse si aspetta di riceverli con la timezone locale):
        $min_dt = Carbon::createFromFormat('Y-m-d H:i:s', config('dates_currency_and_locale.min_utc_timestamp'), new \DateTimeZone(config('app.timezone')));
        $max_dt = Carbon::createFromFormat('Y-m-d H:i:s', config('dates_currency_and_locale.max_utc_timestamp'), new \DateTimeZone(config('app.timezone')));
        $begin_dt = Carbon::parse($begin_epoch_timestamp, new \DateTimeZone(config('app.timezone')));
        $end_dt = Carbon::parse($end_epoch_timestamp, new \DateTimeZone(config('app.timezone')));
        if (($begin_dt < $min_dt) || ($end_dt > $max_dt) || ($end_dt > $max_dt)) {
        // se i timestamp non passano la validazione, redireziono alla pagina di provenienza con un flash_message di errore:
            $need_to_redirect_back = true;
            $flash_message_for_redirect = ucfirst(__('consumption_reports.report_for_a_malformed_period_requested_msg'));
        }
        if($need_to_redirect_back) {
            $previous_page = array_slice(explode('/', url()->previous()), -1)[0];       // TODO: forse posso usare redirect()->back()
            return redirect($previous_page)->with('flash_message', $flash_message_for_redirect)
                                           ->with('flash_background_color', 'yellow');
        }
        GenerateConsumptionReport::dispatch(Auth::user(),
                                            ConsumptionReportsController::SUBJECT_TYPE_PONTOON,
                                            [$pontoon_name],
                                            $file_type,
                                            $begin_dt,
                                            $end_dt)
                                 ->onQueue('reports');
        $flash_message = ucfirst(__('consumption_reports.your_request_has_been_received_you_will_get_a_notification_when_its_available'));
        $flash_background_color = 'green';
        return redirect('consumption_reports')->with('flash_message', $flash_message)
                                              ->with('flash_background_color', $flash_background_color);
    }
}
