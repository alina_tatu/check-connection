<?php

namespace App\Http\Controllers;

use App\Berth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use App\Plant;
use App\Subscription;

class ConsumptionReportsController extends Controller {
    public const STEP_SELECT_REPORT_SUBJECT_TYPE = 's_st';
    public const STEP_SELECT_BERTH = 's_b';
    public const STEP_SELECT_PONTOON = 's_p';
    public const STEP_SELECT_SUBSCRIPTION = 's_s';
    public const STEP_SELECT_BERTH_FOR_SUBSCRIPTION = 's_bs';
    public const STEP_SELECT_REPORT_VIEW_TYPE = 's_vt';
    public const STEP_SELECT_REPORT_BEGIN_AND_END_TIMESTAMPS = 's_bet';
    public const STEP_SELECT_CHART_VALUE = 's_cv';
    public const STEP_SELECT_CHART_SAMPLING_INTERVAL = 's_cs';

    private const CHART_TIMESPAN_MINIMUM_DURATION_IN_HOURS_TO_REQUEST_SAMPLING_PER_DAY = 48;

    public const SUBJECT_TYPE_SUBSCRIPTION = 'st_s';
    public const SUBJECT_TYPE_BERTH = 'st_b';
    public const SUBJECT_TYPE_PONTOON = 'st_p';
    public const SUBJECT_TYPE_MARINA = 'st_m';
    public const SUBJECT_TYPE_USER = 'st_u';        // TODO: non ancora usato: si potrebbe usare per calcolare i consumi di tutti i contratti appartenenti ad un singolo utente
    
    public const REPORT_VALUE_POWER = 'power';
    public const REPORT_VALUE_WATER = 'water';
    public const REPORT_VALUE_CREDIT = 'credit';    

    public const REPORT_PERIOD_LAST_24_HOURS = 'day';
    public const REPORT_PERIOD_LAST_WEEK = 'week';
    public const REPORT_PERIOD_LAST_MONTH = 'month';

    public const CHART_SAMPLING_INTERVAL_HOUR = 'hour';
    public const CHART_SAMPLING_INTERVAL_DAY = 'day';

    public const REPORT_FLAG_TOTAL = 0;
    public const REPORT_FLAG_POSTPAID = 1;

    public const VIEW_TYPE_CHART = 'vt_c';
    public const VIEW_TYPE_PDF_FILE = 'vt_p';
    public const VIEW_TYPE_XLS_FILE = 'vt_x';
    public const VIEW_TYPE_WEBPAGE = 'vt_w';

    public const GOOGLE_CHARTS_TYPE_COLUMN = 'ColumnChart';
    public const GOOGLE_CHARTS_TYPE_LINE = 'LineChart';

    public const REPORT_MODE_PLAIN = 3;
    public const REPORT_MODE_COMPARED_TO_ELAPSED_TIME = 4;

    public const GOOGLE_CHARTS_WATER_CONSUMPTION_DATA_COLOR = '#187bed';
    public const GOOGLE_CHARTS_POWER_CONSUMPTION_DATA_COLOR = '#7be24f';
    public const GOOGLE_CHARTS_CREDIT_CONSUMPTION_DATA_COLOR = '#fc7303';

    public function handleFormSubmit() {
    // ad ogni submit del form, questo metodo si occupa di instradare la richiesta verso il metodo successivo, a seconda delle scelte effettuate        
        // TODO: al momento - se viene selezionato <select_an_option> - impedisco il submit solo lato client con js. Andrebbe fatta la stessa cosa lato server (con una validation rule)

        $nextStep = null;
        $parameters_to_pass_to_next_step = [];
        switch(request()->input('current_step')) {
        // valuto il da farsi a seconda dello step appena eseguito:
            case self::STEP_SELECT_REPORT_SUBJECT_TYPE:
                switch(request()->input('report_type_select')) {
                // e lo uso per determinare il prossimo step:
                    case self::SUBJECT_TYPE_SUBSCRIPTION:
                        $nextStep = 'selectSubscriptionStep';
                        break;
                    case self::SUBJECT_TYPE_BERTH:
                        $nextStep = 'selectBerthStep';
                        break;
                    case self::SUBJECT_TYPE_PONTOON:
                        $nextStep = 'selectPontoonStep';
                        break;
                    case self::SUBJECT_TYPE_MARINA:        
                        $nextStep = 'selectReportDatesStep';
                }
                break;
            case self::STEP_SELECT_SUBSCRIPTION:
            // se ho appena scelto il contratto per il quale richiedere il report: 
                // lo metto nei parametri da passare alla prossima view:
                $parameters_to_pass_to_next_step['subscription_select'] = request()->input('subscription_select');
                // che sarà quella che mi permetterà di selezionare il posto barca per il quale voglio vedere i consumi effettuati dal contratto:
                $nextStep = 'selectBerthForSubscriptionStep';
                break;
            case self::STEP_SELECT_BERTH_FOR_SUBSCRIPTION:
            // se ho chiesto il report di consumo di un contratto e ho appena scelto il posto barca per il quale mi interessano i consumi:
                // lo metto nei parametri da passare alla prossima view:
                $parameters_to_pass_to_next_step['subscription_berth_select'] = request()->input('subscription_berth_select');
                // che sarà quella che mi permetterà di selezionare le date di inizio e fine report:
                $nextStep = 'selectReportDatesStep';
                break;
            case self::STEP_SELECT_BERTH:
            // se ho appena scelto il posto barca per il quale richiedere il report:
                // lo metto nei parametri da passare alla prossima view:
                $parameters_to_pass_to_next_step['berth_select'] = request()->input('berth_select');
                // che sarà quella che mi permetterà di selezionare le date di inizio e fine report:
                $nextStep = 'selectReportDatesStep';
                break;
            case self::STEP_SELECT_PONTOON:
            // se ho appena scelto il pontile per il quale richiedere il report:
                // lo metto nei parametri da passare alla prossima view:
                $parameters_to_pass_to_next_step['pontoon_select'] = request()->input('pontoon_select');
                // che sarà quella che mi permetterà di selezionare le date di inizio e fine report:
                $nextStep = 'selectReportDatesStep';
                break;
            case self::STEP_SELECT_REPORT_BEGIN_AND_END_TIMESTAMPS:
            // se ho appena scelto le date/ore di inizio e fine del report:
                // li converto da stringhe al formato Carbon, portando la timezone da locale a UTC:
                $report_begin_timestamp_utc = Carbon::parse(request()->input('report_begin_date').' '.request()->input('report_begin_time'), 
                                                            new \DateTimeZone(config('dates_currency_and_locale.local_timezone')))
                                                    ->tz(new \DateTimeZone(config('app.timezone')));
                $report_end_timestamp_utc = Carbon::parse(request()->input('report_end_date').' '.request()->input('report_end_time'), 
                                                            new \DateTimeZone(config('dates_currency_and_locale.local_timezone')))
                                                    ->tz(new \DateTimeZone(config('app.timezone')));
                // e li metto nei parametri da passare alla prossima view:
                $parameters_to_pass_to_next_step['report_begin_timestamp_utc'] = $report_begin_timestamp_utc;
                $parameters_to_pass_to_next_step['report_end_timestamp_utc'] = $report_end_timestamp_utc;
                // che sarà quella che mi permetterà di selezionare il tipo di view per il report desiderato (file pdf/file xls/grafico):
                $nextStep = 'selectReportViewTypeStep';
                // passo anche il resto dei parametri selezionati negli step precedenti:   
                $parameters_to_pass_to_next_step['report_type_select'] = request()->input('report_type_select');
                break;
            case self::STEP_SELECT_REPORT_VIEW_TYPE:
            // se ho appena scelto il tipo di report (ad esempio pdf/xls/grafico):
                switch(request()->input('view_type_select')) {
                    case self::VIEW_TYPE_PDF_FILE:
                    case self::VIEW_TYPE_XLS_FILE:
                    // se ho selezionato un report pdf/xls:
                        // non devo più selezionare altro, posso passare alla generazione del report scelto:
                        $nextStep = 'requestReport';
                        break;
                    case self::VIEW_TYPE_CHART:
                    // se ho selezionato un grafico:
                        // devo scegliere se voglio graficare l'andamento del consumo di energia elettrica, di acqua o di credito:
                        $nextStep = 'selectChartValueStep';
                }
                break;
            case self::STEP_SELECT_CHART_VALUE:
            // se ho appena scelto la grandezza da graficare (energia/acqua/credito):
                $value_for_chart = request()->input('value_to_chart_select');
                $parameters_to_pass_to_next_step['value_to_chart_select'] = $value_for_chart;
                // devo passare alla selezione dell'intervallo di campionamento:
                $report_begin_timestamp_utc = Carbon::parse(request()->input('report_begin_timestamp_utc'));
                $report_end_timestamp_utc = Carbon::parse(request()->input('report_end_timestamp_utc'));
                $report_timespan_duration_in_hours = $report_begin_timestamp_utc->diffInRealHours($report_end_timestamp_utc);
                
                if($value_for_chart !== self::REPORT_VALUE_CREDIT) {
                // se ho scelto di graficare il consumo di acqua/energia:
                    if($report_timespan_duration_in_hours >= self::CHART_TIMESPAN_MINIMUM_DURATION_IN_HOURS_TO_REQUEST_SAMPLING_PER_DAY) {
                    // e se il periodo richiesto è pari o più lungo di <self::CHART_TIMESPAN_MINIMUM_DURATION_IN_HOURS_TO_REQUEST_SAMPLING_PER_DAY> ore:
                        // passo alla selezione dell'intervallo di campionamento del grafico:
                        $nextStep = 'selectChartSamplingIntervalStep';
                    } else {
                    // se invece il periodo richiesto è minore di <self::CHART_TIMESPAN_MINIMUM_DURATION_IN_HOURS_TO_REQUEST_SAMPLING_PER_DAY> ore:
                        // setto l'intervallo di campionamento a 1h senza chiederlo all'utente:
                        $parameters_to_pass_to_next_step['chart_sampling_interval_select'] = self::CHART_SAMPLING_INTERVAL_HOUR;
                        // e passo direttamente alla generazione del report:
                        $nextStep = 'requestReport';
                    }
                } else {
                // se ho scelto di graficare l'andamento del credito (che non prevede intervallo di campionamento): 
                    // l'intervallo di campionamento non verrà preso in considerazione, quindi posso passo passare indifferentemente il campionamento per ora o per giorni. 
                    // Dato che faccio la validazione sulla lunghezza massima dell'intervallo del grafico, imposto il campionamento per ora per essere più restrittivo, poi vado alla generazione del report:
                    $parameters_to_pass_to_next_step['chart_sampling_interval_select'] = self::CHART_SAMPLING_INTERVAL_HOUR;
                    $nextStep = 'requestReport';
                }
                break;
            case self::STEP_SELECT_CHART_SAMPLING_INTERVAL:
            // se ho richiesto un grafico e ne ho appena scelto l'intervallo di campionamento:
                // lo metto nei parametri da passare al prossimo step:
                $parameters_to_pass_to_next_step['chart_sampling_interval_select'] = request()->input('chart_sampling_interval_select');
                // a questo punto non devo più selezionare altro, posso passare alla generazione del report scelto:
                $nextStep = 'requestReport';
                break;
            // TODO: ci sono altri case da aggiungere?
            // TODO: ha senso riorganizzare meglio, strutturando i vari case in una gerarchia?
        }
        // qui di seguito copio nei parametri da passare al prossimo step i vari dati che mi trovo nella request() perchè raccolti negli step precedenti. TODO: occhio che probabilmente alcuni di questi li ho già messi nei $parameters_to_pass_to_next_step all'interno degli switch precedenti, ma forse ha senso toglierli da lì e lasciare l'assegnazione solo qui, in modo da farlo una volta sola:
        $parameters_to_pass_to_next_step['view_type_select'] = request()->input('view_type_select');
        if(!is_null(request()->input('report_type_select'))) {
            $parameters_to_pass_to_next_step['report_type_select'] = request()->input('report_type_select');
            switch(request()->input('report_type_select')) {
                case self::SUBJECT_TYPE_SUBSCRIPTION:
                    $parameters_to_pass_to_next_step['subscription_select'] = request()->input('subscription_select');
                    $parameters_to_pass_to_next_step['subscription_berth_select'] = request()->input('subscription_berth_select');
                    break;
                case self::SUBJECT_TYPE_BERTH:
                    $parameters_to_pass_to_next_step['berth_select'] = request()->input('berth_select');
                    break;
                case self::SUBJECT_TYPE_PONTOON:
                    $parameters_to_pass_to_next_step['pontoon_select'] = request()->input('pontoon_select');
                    break;
                case self::SUBJECT_TYPE_MARINA:
                    // qui non devo passare nulla
                    break;
            }
        }
        if(!is_null(request()->input('report_begin_timestamp_utc')) && !is_null(request()->input('report_end_timestamp_utc'))) {
            $parameters_to_pass_to_next_step['report_begin_timestamp_utc'] = request()->input('report_begin_timestamp_utc');
            $parameters_to_pass_to_next_step['report_end_timestamp_utc'] = request()->input('report_end_timestamp_utc');
        }
        // chiamo il prossimo metodo passandogli i parametri che servono:
        return $this->{$nextStep}($parameters_to_pass_to_next_step);
    }

    public function selectReportSubjectTypeStep() {
        $current_step = self::STEP_SELECT_REPORT_SUBJECT_TYPE;
        $available_subject_types = [];
        if(Auth::check()) {
            if(Auth::user()->is_viewer_admin()) {
            // se l'utente loggato è (almeno) un viewer_admin:
                // può accedere a tutti i tipi di report:
                $available_subject_types = [
                    self::SUBJECT_TYPE_MARINA => strtolower(__('consumption_reports.type_select_plant_consumption_option_caption')),
                    self::SUBJECT_TYPE_PONTOON => strtolower(__('consumption_reports.type_select_pontoon_consumption_option_caption')),
                    self::SUBJECT_TYPE_BERTH => strtolower(__('consumption_reports.type_select_berth_consumption_option_caption')),
                    self::SUBJECT_TYPE_SUBSCRIPTION => strtolower(__('consumption_reports.type_select_subscription_consumption_option_caption')),
                ];
            } else {
            // se l'utente loggato è un utente standard:
                // può accedere solo ai report di consumo dei propri contratti:
                $available_subject_types =  [
                    self::SUBJECT_TYPE_SUBSCRIPTION => strtolower(__('consumption_reports.type_select_subscription_consumption_option_caption'))
                ];
                
            }
        }
        return view('consumption_reports.select_subject_type', compact('current_step', 
                                                                       'available_subject_types'));
    }

    // TODO: nei metodi che seguono ha senso permettere il valore null per parameters? O tolgo il default a null?

    private function selectPontoonStep($parameters = null) {
        $current_step = self::STEP_SELECT_PONTOON;
        $pontoons = Plant::get_all_pontoons();
        return view('consumption_reports.select_pontoon', compact('current_step',
                                                                  'parameters',
                                                                  'pontoons'));
    }

    private function selectBerthStep($parameters = null) {
        $current_step = self::STEP_SELECT_BERTH;
        $berths = Berth::all();
        return view('consumption_reports.select_berth', compact('current_step',
                                                                'parameters', 
                                                                'berths'));
    }

    private function selectSubscriptionStep($parameters = null) {
        $current_step = self::STEP_SELECT_SUBSCRIPTION;
        if(Auth::user()->is_viewer_admin()) {
        // se l'utente loggato è (almeno) un viewer_admin:
            if(Auth::user()->is_supervisor()) {
            // verifico se è un supervisor. Se lo è:
                // può richiedere report di consumo per tutti i contratti:
                $subscriptions = Subscription::all();
            } else {
            // se l'utente loggato è almeno un viewer_admin ma non è un supervisor:
                // può richiedere report di consumo solo relativamente ai contratti non legati ad utenti supervisor:
                $subscriptions = Subscription::excludeSupervisorRelatedSubscriptions()
                                             ->get();
            }
        } else {
        // se l'utente loggato non è almeno un viewer_admin, è un utente standard:
            // in questo caso può richiedere report di consumo solo relativamente ai propri contratti:
            $subscriptions = Subscription::where('user_id', Auth::id())
                                         ->get();
        }
        return view('consumption_reports.select_subscription', compact('current_step', 
                                                                       'parameters', 
                                                                       'subscriptions'));
    }

    private function selectBerthForSubscriptionStep($parameters = null) {
        $current_step = self::STEP_SELECT_BERTH_FOR_SUBSCRIPTION;
        $subscription = Subscription::find(request('subscription_select'));
        // TODO: aggiungere la validazione: se l'utente loggato non è almeno un viewer_admin, la subscription passata gli deve appartenere!
        if(!$subscription->is_bound_to_socket()) {
        // se il contratto NON E' legato ad un posto barca:
            // cerco tutti i posti barca su quali ha consumato:
            $berths_on_which_subscription_consumed = $subscription->get_berths_on_which_subscription_consumed();    // TODO: devo ordinarli per ultimo utilizzo o lo sono già?
        } else {
        // se il contratto E' legato ad un posto barca:
             // $berths_on_which_subscription_consumed deve contenere solamente il posto barca al quale è legato:
             $berths_on_which_subscription_consumed = [];
             $berths_on_which_subscription_consumed[] = $subscription->berth;
        }
        if($subscription->is_bound_to_socket()) {
        // se il contratto è legato ad un posto barca:
            // $berths_on_which_subscription_consumed deve contenere solamente il posto barca al quale è legato:
            $berths_on_which_subscription_consumed = [];
            $berths_on_which_subscription_consumed[] = $subscription->berth;
        } else {
        // se il contratto NON è legato ad un posto barca:
            // cerco tutti i posti barca su quali ha consumato:
            $berths_on_which_subscription_consumed = $subscription->get_berths_on_which_subscription_consumed();    // TODO: devo ordinarli per ultimo utilizzo o lo sono già?
        }
        return view('consumption_reports.select_berth_for_subscription', compact('current_step', 
                                                                                 'parameters', 
                                                                                 'berths_on_which_subscription_consumed'));
    }

    private function selectReportDatesStep($parameters = null) {
        $current_step = self::STEP_SELECT_REPORT_BEGIN_AND_END_TIMESTAMPS;
        $min_report_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', 
                                                         config('dates_currency_and_locale.min_utc_timestamp'), 
                                                         new \DateTimeZone(config('app.timezone')))
                                      ->setTimeZone(config('dates_currency_and_locale.local_timezone'));
        $max_report_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', 
                                                         config('dates_currency_and_locale.max_utc_timestamp'), 
                                                         new \DateTimeZone(config('app.timezone')))
                                      ->setTimeZone(config('dates_currency_and_locale.local_timezone'));
        $default_report_begin_timestamp = Carbon::now(config('app.timezone'))
                                                ->sub(config('dates_currency_and_locale.default_report_interval_timespan'))
                                                ->setTimeZone(config('dates_currency_and_locale.local_timezone'));
        $default_report_end_timestamp = Carbon::now(config('app.timezone'))
                                              ->setTimeZone(config('dates_currency_and_locale.local_timezone'));
        return view('consumption_reports.select_dates', compact('current_step',
                                                                'parameters',
                                                                'min_report_timestamp',
                                                                'max_report_timestamp',
                                                                'default_report_begin_timestamp',
                                                                'default_report_end_timestamp'));
    }

    private function selectReportViewTypeStep($parameters = null) {
        $current_step = self::STEP_SELECT_REPORT_VIEW_TYPE;
        $available_view_types_with_translations = [
            self::VIEW_TYPE_PDF_FILE => strtolower(__('consumption_reports.view_type_select_pdf_file_option_caption')),
            self::VIEW_TYPE_XLS_FILE => strtolower(__('consumption_reports.view_type_select_xls_file_option_caption')),
        ];
        if(isset($parameters['report_type_select']) && 
           (in_array($parameters['report_type_select'], [self::SUBJECT_TYPE_BERTH, self::SUBJECT_TYPE_SUBSCRIPTION]))) {
        // la possibilità di richiedere un grafico deve essere data solo quando viene richiesto un report su presa o su contratto:
            if(config('marina.allow_requests_for_consumption_charts_with_custom_dates_and_sampling') === true) {
            // e solo se abilitata dall'apposita voce in config:
                $available_view_types_with_translations[self::VIEW_TYPE_CHART] = strtolower(__('consumption_reports.view_type_select_chart_option_caption'));
            }
        }
        return view('consumption_reports.select_view_type', compact('current_step',
                                                                    'parameters',
                                                                    'available_view_types_with_translations'));
    }

    private function selectChartValueStep($parameters = null) {
        $current_step = self::STEP_SELECT_CHART_VALUE;
        $user_has_requested_a_consumption_chart_for_a_subscription = $subscription_is_prepaid = false;
        $available_chart_values_with_translations = [
            self::REPORT_VALUE_POWER => strtolower(__('consumption_reports.chart_value_electricity')),
            self::REPORT_VALUE_WATER => strtolower(__('consumption_reports.chart_value_water')),
        ];
        if(!is_null(request()->input('subscription_select')) && !is_null(request()->input('subscription_berth_select'))) {
            $user_has_requested_a_consumption_chart_for_a_subscription = true;
        }
        if($user_has_requested_a_consumption_chart_for_a_subscription) {
        // se è stato richiesto il grafico per una subscription:
            $subscription = Subscription::findOrFail(request()->input('subscription_select'));      // TODO: mi serve il findOrFail? In teoria ho già validato l'esistenza della subscription negli step precedenti
            // verifico se si tratta di una subscription prepagata:
            $subscription_is_prepaid = $subscription->is_prepaid();
        }
        if((!$user_has_requested_a_consumption_chart_for_a_subscription) || ($user_has_requested_a_consumption_chart_for_a_subscription && $subscription_is_prepaid)) {
        // se è stato richiesto il grafico per una presa o per una subscription prepagata:
            // aggiungo il grafico del credito alle tipologie di grafico disponibili:
            $available_chart_values_with_translations[self::REPORT_VALUE_CREDIT] = strtolower(__('consumption_reports.chart_value_credit'));
        }
        return view('consumption_reports.select_chart_value', compact('current_step',
                                                                      'parameters',
                                                                      'available_chart_values_with_translations'));
    }

    private function selectChartSamplingIntervalStep($parameters = null) {
        $current_step = self::STEP_SELECT_CHART_SAMPLING_INTERVAL;
        $available_chart_sampling_intervals_with_translations = [
            self::CHART_SAMPLING_INTERVAL_HOUR => strtolower(__('consumption_reports.chart_sampling_interval_select_hour_option_caption')),
            self::CHART_SAMPLING_INTERVAL_DAY => strtolower(__('consumption_reports.chart_sampling_interval_select_day_option_caption')),
        ];
        // TODO: ho visto che gcharts non renderizza il grafico se si va oltre un certo numero di barre. Usare la validazione per fare in modo che le barre del grafico non siano più di un centinaio (quindi direi 4gg * 24h se il sampling è un'ora, 96gg se il sampling è un giorno). Per i grafici credito (che non hanno barre) come la gestisco? Stesso tempo?
        return view('consumption_reports.select_chart_sampling_interval', compact('current_step',
                                                                                  'parameters',
                                                                                  'available_chart_sampling_intervals_with_translations'));
    }

    private function requestReport($parameters = null) {
        // TODO: c'è poca consistenza tra i nomi dei parametri (ad esempio berth_name, pontoon_name) che i vari metodi di generazione dei report e dei grafici si aspettano. Avrebbe senso uniformarli per mantenere questo metodo il più semplice possibile.
        // TODO: alcuni parametri li recupero dalla request, altri li prendo dall'array $parameters. Perchè non sono tutti in parameters (forse è corretto, ma verificare)?
        // TODO: mi sa che faccio un po' di casino con il passaggio dei parametri: in teoria posso prenderli sempre da $parameters anzichè da request()

        switch(request()->input('report_type_select')) {
            case self::SUBJECT_TYPE_SUBSCRIPTION:
                $parametersArrayForControllerMethod['subscription_id'] = intval($parameters['subscription_select']);
                $parametersArrayForControllerMethod['berth_id'] = intval($parameters['subscription_berth_select']);
                $controllerToCall = 'SubscriptionController';
                break;
            case self::SUBJECT_TYPE_BERTH:
                $controllerToCall = 'BerthController';
                $parametersArrayForControllerMethod['berth_id'] = intval(request()->input('berth_select'));
                break;
            case self::SUBJECT_TYPE_PONTOON:
                $controllerToCall = 'PontoonController';
                $parametersArrayForControllerMethod['pontoon_name'] = request()->input('pontoon_select');
                break;
            case self::SUBJECT_TYPE_MARINA:
                $controllerToCall = 'PlantController';
        }
        switch(request()->input('view_type_select')) {
            case self::VIEW_TYPE_PDF_FILE:
            case self::VIEW_TYPE_XLS_FILE:
                $controllerMethodToCall = 'get_consumption_report_file';
                $parametersArrayForControllerMethod['begin_epoch_timestamp'] = intval(Carbon::parse(request()->input('report_begin_timestamp_utc'))->timestamp);
                $parametersArrayForControllerMethod['end_epoch_timestamp'] = intval(Carbon::parse(request()->input('report_end_timestamp_utc'))->timestamp);
                $parametersArrayForControllerMethod['file_type'] = request()->input('view_type_select');        // TODO: questo parametro prende un nome diverso a seconda di grafici/report files! uniformare?
                break;
            case self::VIEW_TYPE_CHART:
                $controllerMethodToCall = 'get_consumption_chart';
                $parametersArrayForControllerMethod['begin_timestamp'] = intval(Carbon::parse(request()->input('report_begin_timestamp_utc'))->timestamp);
                $parametersArrayForControllerMethod['end_timestamp'] = intval(Carbon::parse(request()->input('report_end_timestamp_utc'))->timestamp);
                $parametersArrayForControllerMethod['subject'] = request()->input('value_to_chart_select');
                $parametersArrayForControllerMethod['sampling_interval_argument'] = $parameters['chart_sampling_interval_select'];
                // verifico la lunghezza dell'intervallo richiesto e setto l'eventuale messaggio di errore da visualizzare nel caso in cui sia troppo lungo:
                if(in_array(request()->input('value_to_chart_select'), [self::REPORT_VALUE_POWER, self::REPORT_VALUE_WATER], true) ) {
                // se è stato richiesto un grafico di consumo dell'energia o dell'acqua:
                    switch($parameters['chart_sampling_interval_select']) {
                        case self::CHART_SAMPLING_INTERVAL_HOUR:
                        default:
                            $required_chart_bars_number_to_cover_the_requested_timespan = Carbon::parse($parametersArrayForControllerMethod['end_timestamp'])->floatDiffInHours(Carbon::parse($parametersArrayForControllerMethod['begin_timestamp']));
                            $max_chart_bars = 24 * 4;                   // se il grafico è campionato per ora, il massimo intervallo visualizzabile è 4 giorni
                            $max_days = $max_chart_bars / 24;
                            break;
                        case self::CHART_SAMPLING_INTERVAL_DAY:
                            $required_chart_bars_number_to_cover_the_requested_timespan = Carbon::parse($parametersArrayForControllerMethod['end_timestamp'])->floatDiffInDays(Carbon::parse($parametersArrayForControllerMethod['begin_timestamp']));
                            $max_chart_bars = $max_days = 31 * 3;       // se il grafico è campionato per giorno, il massimo intervallo visualizzabile è 3 mesi
                    }
                    switch($parameters['chart_sampling_interval_select']) {
                        case self::CHART_SAMPLING_INTERVAL_HOUR:
                        default:
                            $validation_error_message_for_total_timespan_too_long = ucfirst(__('validation.custom.views.consumption_reports.chart.timespan_length.max_days_when_sampling_by_hour', ['max_days' => $max_days]));
                            break;
                        case self::CHART_SAMPLING_INTERVAL_DAY:
                            $validation_error_message_for_total_timespan_too_long = ucfirst(__('validation.custom.views.consumption_reports.chart.timespan_length.max_days_when_sampling_by_day', ['max_days' => $max_days]));
                    }
                } else {
                // se è stato richiesto un grafico di consumo del credito:
                    // il massimo intervallo visualizzabile è 1 mese (per validarlo, faccio i conteggi come se ci fosse una barra al giorno):
                    $max_chart_bars = $max_days = 31;
                    $required_chart_bars_number_to_cover_the_requested_timespan = Carbon::parse($parametersArrayForControllerMethod['end_timestamp'])->floatDiffInDays(Carbon::parse($parametersArrayForControllerMethod['begin_timestamp']));
                    $validation_error_message_for_total_timespan_too_long = ucfirst(__('validation.custom.views.consumption_reports.chart.timespan_length.max_days_when_requesting_credit_chart', ['max_days' => $max_days]));
                }
                request()->merge([
                    'required_chart_bars_number_to_cover_the_requested_timespan' => $required_chart_bars_number_to_cover_the_requested_timespan
                ]);
                $validation_rules = [
                    'required_chart_bars_number_to_cover_the_requested_timespan' => 'numeric|max:'.$max_chart_bars,
                ];
                $validation_error_messages = [
                    'required_chart_bars_number_to_cover_the_requested_timespan.max' => $validation_error_message_for_total_timespan_too_long,
                ];
                $validator = Validator::make(request()->all(), $validation_rules, $validation_error_messages);
                if($validator->fails()) {
                    return redirect()->back()
                                     ->withInput()
                                     ->withErrors($validator);
                }

                // ATTENZIONE: a prima vista i grafici (sia subscription, sia berth) sembrano avere lo step interval troppo ampio. In realtà il problema è che google charts - se i dati sono troppo fitti - scrive le captions delle colonne alternandole (una sì e una no), perciò quelle che ci sono sembrano fare salti
                break;
        }
        return redirect()->action($controllerToCall.'@'.$controllerMethodToCall, $parametersArrayForControllerMethod);
    }
}
