<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Response as HttpResponse;
use stdClass;
use App\Plant;
use App\Pedestal;
use App\Berth;
use App\SocketSnapshot;
use App\User;
use App\Subscription;
use App\Transponder;
use App\Action;
use App\Notifications\SmartCloudNotification;
use App\Notifications\SocketOfflineAlarmNotification;
use App\Notifications\SerialLineOfflineAlarmNotification;
use App\Notifications\SocketTheftAlarmNotification;
use App\Notifications\SocketMinPowerConsumptionAlarmNotification;
use App\Notifications\SocketMaxPowerConsumptionAlarmNotification;
use App\Notifications\SocketMinCreditAlarmNotification;
use App\Notifications\SocketMaxWaterTapTimeAlarmNotificationForUsers;
use App\SocketLastSnapshot;
use App\LostMoneyForResettedSockets;
use App\Jobs\CreateSocketSnapshotAndSubscriptionSnapshot;
use Illuminate\Support\Facades\Validator;

class APIv0Controller extends Controller {
// TODO: per la ricezione delle snapshot, valutare di usare un websocket invece di un'api?

     /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    private const ALLOWED_USERNAMES = ['nembo'];

    private const ALLOWED_USER_IS_ADMIN_VALUES_FROM_TAGMANAGER = [
    // uso chiavi stringa perchè non posso usare float
        "0" => User::SIMPLE_USER_ACCESS_LEVEL,
        "0.5" => User::VIEWER_ADMIN_USER_ACCESS_LEVEL,
        "1" => User::FULL_ADMIN_USER_ACCESS_LEVEL,
    ];

    private function is_username_allowed(string $username): bool {
        if(in_array($username, $this::ALLOWED_USERNAMES)) {
            return true;
        }
        return false;
    }

    public function insert_router(Request $request) {
        // disabilito la debugbar, per evitare che influisca sulle risposte date a SmartView/TagManager:
        if((env('APP_DEBUG') === true) || (env('APP_ENV') !== 'production') || (env('DEBUGBAR_ENABLED') === true)) {
            app('debugbar')->disable();     // TODO: RICORDARE DI TOGLIERLO IN PRODUZIONE: POTREI DISINSTALLARE LA DEBUGBAR E QUESTO POTREBBE PRODURRE UN ERRORE!
        }
        $mode_parameter = $request->query('mode');
        $table_parameter = $request->query('table');
        if(strcmp($mode_parameter, 'entity_tables') !== 0) {
        // se il mode è diverso da 'entity_tables', non autorizzo a proseguire:
            return response('Unauthorized', 403);
        }
        switch($table_parameter) {
            case 'snapshotpresa':   // riceve un nuovo snapshot presa
                // qui non c'è la verifica dello username (probabilmente quando ho fatto SmartCloud v1.x ho dimenticato di metterla)
                return($response = $this->write_socket_snapshot_record($request))->header('Content-Type', 'text/plain');
                break;
            case 'presa':           // inserisce o aggiorna l'anagrafica di una presa
                // qui non c'è la verifica dello username
                return($response = $this->write_berth_record($request))->header('Content-Type', 'text/plain');
                break;
            case 'utente':          // inserisce o aggiorna l'anagrafica di un utente
                // qui non c'è la verifica dello username, perchè il dato con quel nome viene usato come username dell'utente da inserire/aggiornare
                return($response = $this->write_user_record($request))->header('Content-Type', 'text/plain');
                break;
            case 'transponder':     // inserisce o aggiorna l'anagrafica di un contratto (solo quelli su transponder, dato che quelli su presa vengono gestiti interamente da SmartCloud)
                // qui non c'è la verifica dello username (probabilmente quando ho fatto SmartCloud v1.x ho dimenticato di metterla)
                return($response = $this->write_transponder_subscription_record($request));
                break;
            case 'colonnina':   // su SmartCloud 1.x non esisteva: lo uso per creare/aggiornare le anagrafiche delle colonnine da TagManager. Far implementare le chiamate a sergio.
                return($response = $this->write_pedestal_record($request))->header('Content-Type', 'text/plain');
                break;
            case 'notification-insert':
                return($response = $this->write_notification_record($request))->header('Content-Type', 'text/plain');
                break;
            case 'action-setcompleted':
                return($response = $this->set_action_as_completed($request))->header('Content-Type', 'text/plain');
            default:
            // se il valore di $table non è tra quelli previsti, non autorizzo a proseguire:
                return response('Unauthorized', 403);
        }
    }

    public function select_router(Request $request) {
        // disabilito la debugbar, per evitare che influisca sulle risposte date a SmartView/TagManager:
        if((env('APP_DEBUG') === true) || (env('APP_ENV') !== 'production') || (env('DEBUGBAR_ENABLED') === true)) {
            app('debugbar')->disable();             // TODO: RICORDARE DI TOGLIERLO IN PRODUZIONE: POTREI DISINSTALLARE LA DEBUGBAR E QUESTO POTREBBE PRODURRE UN ERRORE!
        }
        $mode_parameter = $request->query('mode');
        switch ($mode_parameter) {
            case 'pending_actions':
                return($response = $this->get_pending_actions());
                break;
            default:
            // se il valore di $mode non è tra quelli previsti, non autorizzo a proseguire:
                return response('Unauthorized', 403);
        }
    }

    private function write_pedestal_record(Request $request) {
        if($request->has('indLogicoColonnina')) {
            if(is_numeric($request->query('indLogicoColonnina')) && ctype_digit($request->query('indLogicoColonnina'))) {
                $pedestal_id = intval($request->query('indLogicoColonnina'));
            } else {
                return response('Bad request. Unexpected format for parameter \'indLogicoColonnina\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'indLogicoColonnina\'.', 422);
        }
        if($request->has('nomeColonnina')) {
            $pedestal_name = $request->query('nomeColonnina');
        } else {
            return response('Bad request. Missing mandatory parameter \'nomeColonnina\'.', 422);
        }
        if($request->has('coord_x')) {
            $x_coord = floatval($request->query('coord_x'));
        } else {
            return response('Bad request. Missing mandatory parameter \'postoBarca\'.', 422);
        }
        if($request->has('coord_x')) {
            // il supervisore mi invia i decimali usando la virgola come separatore. La sostituisco con il punto:
            $x_coord = str_replace(',', '.', $request->query('coord_x'));
            if(is_numeric($x_coord)) {
                $x_coord = floatval($x_coord);
            } else {
                return response('Bad request. Unexpected format for parameter \'coord_x\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'coord_x\'.', 422);
        }
        if($request->has('coord_y')) {
            // il supervisore mi invia i decimali usando la virgola come separatore. La sostituisco con il punto:
            $y_coord = str_replace(',', '.', $request->query('coord_y'));
            if(is_numeric($y_coord)) {
                $y_coord = floatval($y_coord);
            } else {
                return response('Bad request. Unexpected format for parameter \'coord_y\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'coord_y\'.', 422);
        }
        // cerco la colonnina corrispondente all'id ricevuto: dovrò aggiornarne i campi con i nuovi dati.
        $pedestal_record = Pedestal::find($pedestal_id);
        $new_record_created = false;
        if((!$pedestal_record) || ($pedestal_record->count() === 0)) {
        // se non lo trovo. quella appena ricevuta è l'anagrafica di una colonnina che non esiste ancora sul db. In questo caso, dovrò creare un record nuovo.
            // quindi lo istanzio e ne setto l'id al valore ricevuto:
            $pedestal_record = new Pedestal();
            $pedestal_record->set_id($pedestal_id);
            $new_record_created = true;
        }
        $pedestal_record->set_name($pedestal_name);
        $pedestal_record->set_x_coord($x_coord);
        $pedestal_record->set_y_coord($y_coord);
        if($success = $pedestal_record->save()) {
        // salvo il record. Se il salvataggio va a buon fine
            switch($new_record_created) {
                case true:
                    return response('Created', 201);
                    break;
                case false:
                    return response('Updated', 200);
            }
        } else {
        // se il salvataggio non va a buon fine
            return response('Internal server error', 500);
        }
    }

    private function write_berth_record(Request $request): HttpResponse {
        if($request->has('indLogicoPresa')) {
            if(is_numeric($request->query('indLogicoPresa')) && ctype_digit($request->query('indLogicoPresa'))) {
                $socket_id = intval($request->query('indLogicoPresa'));
            } else {
                return response('Bad request. Unexpected format for parameter \'indLogicoPresa\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'indLogicoPresa\'.', 422);
        }
        if($request->has('pontile')) {
            $pontoon_name = $request->query('pontile');
        } else {
            return response('Bad request. Missing mandatory parameter \'pontile\'.', 422);
        }
        if($request->has('postoBarca')) {
            $berth_name = $request->query('postoBarca');
        } else {
            return response('Bad request. Missing mandatory parameter \'postoBarca\'.', 422);
        }
        if($request->has('descrPresa') && (strlen($request->query('descrPresa')) > 0)) {
            $berth_description = $request->query('descrPresa');
        } else {
        // se la descrizione della presa non viene passata o è vuota, la setta pontile + posto barca:
            $berth_description = $pontoon_name.' '.$berth_name;
        }
        if($request->has('tipoPresa') && (strlen($request->query('tipoPresa')) > 0)) {
            $berth_socket_type = $request->query('tipoPresa');
        } else {
        // se il tipo della presa non è settato o è vuoto, la setta al valore di default:
            $berth_socket_type = Berth::DEFAULT_SOCKET_TYPE;
        }
        // cerco il berth corrispondente all'id ricevuto: dovrò aggiornarne i campi con i nuovi dati.
        $berth_record = Berth::find($socket_id);
        $new_record_created = false;
        if((!$berth_record) || ($berth_record->count() === 0)) {
        // se non lo trovo. quella appena ricevuta è l'anagrafica di un posto barca che non esiste ancora sul db. In questo caso, dovrò creare un record nuovo.
            // quindi lo istanzio e ne setto l'id al valore ricevuto:
            $berth_record = new Berth();
            $berth_record->set_id($socket_id);
            $new_record_created = true;
        }
        $berth_record->set_pontoon_name($pontoon_name);
        $berth_record->set_berth_name($berth_name);
        $berth_record->set_description($berth_description);
        $berth_record->set_socket_type($berth_socket_type);
        // inizializzo il valore di ritorno come da caso peggiore (ovvero il caso in cui il salvataggio del record fallisca):
        $response = response('Internal server error', 500);
        if($success = $berth_record->save()) {
        // salvo il record. Se il salvataggio va a buon fine, modifico il valore di ritorno a seconda che il record sia stato modificato da uno esistente, oppure creato come nuovo:
            switch($new_record_created) {
                case true:
                    $response = response('Created', 201);
                    break;
                case false:
                    $response = response('Updated', 200);
            }
        }
        return $response;
    }

    private function write_user_record(Request $request): HttpResponse {
        Validator::extend('numeric_gte0_with_dot_or_comma', function ($attribute, $value, $parameters) {
        // restituisce true se $value è un numero intero o decimale nullo o positivo (con separatore decimale virgola o punto):
            $value = str_replace(',', '.', $value);
            return (is_numeric($value)) && (floatval($value) >= 0);
        });
        Validator::extend('in_access_level_allowed_values', function ($attribute, $value, $parameters) {
            $value = str_replace(',', '.', $value);
            return array_key_exists($value, $this::ALLOWED_USER_IS_ADMIN_VALUES_FROM_TAGMANAGER);
        });
        $validation_rules = [
            'idUtente' => 'required|numeric|min:0',
            'cognome' => 'required|string',
            'nome' => 'required|string',
            'username' => 'nullable|string',
            'isAdmin' => 'required|numeric_gte0_with_dot_or_comma|in_access_level_allowed_values',
            'disabled' => 'required|numeric|in:0,1',
        ];
        $validation_error_messages = [
            'required' => "Bad request. Missing mandatory parameter ':attribute'.",
            'numeric' => "Bad request. Unexpected format for parameter ':attribute'.",
            'min' => "Bad request. Unexpected format for parameter ':attribute'.",
            'string' => "Bad request. Unexpected format for parameter ':attribute'.",
            'in' => "Bad request. Unexpected value for parameter ':attribute'.",
            'numeric_gte0_with_dot_or_comma' => "Bad request. Unexpected value for parameter ':attribute'.",
            'in_access_level_allowed_values' => "Bad request. Unexpected value for parameter ':attribute'.",
        ];
        $validator = Validator::make($request->all(), $validation_rules, $validation_error_messages);
        if($validator->fails()) {
            return response($validator->errors()->first(), 422);
        }
        $local_software_user_id = intval($request->query('idUtente'));
        $user_is_new = false;
        // cerco l'utente corrispondente all'id ricevuto: dovrò aggiornarne i campi con i nuovi dati.
        $user_record = User::find_by_local_software_id($local_software_user_id);
        $now_timestamp = Carbon::now(config('app.timezone'));
        if((!$user_record) || ($user_record->count() === 0)) {
        // se non lo trovo. quella appena ricevuta è l'anagrafica di un utente che non esiste ancora sul db. In questo caso, dovrò creare un record nuovo.
            $user_is_new = true;
            // quindi lo istanzio e ne setto il local_software_id al valore ricevuto:
            $user_record = new User();
            $user_record->set_local_software_id($local_software_user_id);
            $token_for_actions_on_sockets = User::generate_token_for_actions_on_sockets();
            $user_record->set_token_for_actions_on_sockets($token_for_actions_on_sockets);
        }
        /* per il set dei parametri first_name, last_name e username uso mb_convert_encoding(), perchè a fine dicembre 2020 ho avuto problemi con utenti a PortoMontenegro che contenevano caratteri speciali (come "€") e mandavano in exception il save.
        TODO: però - sebbene in questo modo il salvataggio vada a buon fine - il carattere speciale viene ignorato (es.: un utente che ha come first_name la stringa "10 €" - che encodata in query string mi arriva come "&nome=10%80" - viene salvato solo come "10"). Risolvere. */
        $user_record->set_last_name(mb_convert_encoding(utf8_encode($request->query('cognome')), 'UTF-8'));
        $user_record->set_first_name(mb_convert_encoding(utf8_encode($request->query('nome')), 'UTF-8'));
        if(!$user_record->login_is_protected()) {
        // se il bool protect_login_against_username_deletion del record dell'utente è true, evito di sovrascrivere lo username ed il campo disabled dell'utente, altrimenti li scrivo normalmente. Questo per evitare situazioni come quella verificatasi ad Ayia Napa a settembre 2020: magari posso aver creato un account cloud per un utente reale su un impianto reale, a scopo di demo o per debug. Se qualcuno ne disabilitasse l'accesso dal TagManager e non ci fosse questo if, io non potrei più accedervi.
            $user_is_disabled = false;
            if(intval($request->query('disabled')) > 0) {
                $user_is_disabled = true;
            }
            if(!is_null($request->query('username')) && (strlen($request->query('username')) > 0)) {
                $user_record->set_username(mb_convert_encoding(utf8_encode($request->query('username')), 'UTF-8'));
            } else {
            // se per l'utente non mi è stato passato uno username, lo setto come disabilitato per default:
                $user_record->set_username(null);
                $user_is_disabled = true;
            }
            $user_record->set_as_disabled($user_is_disabled);
        }
        // il livello di accesso di default di un utente ricevuto da TagManager è sempre User::SIMPLE_USER_ACCESS_LEVEL
        $access_level = User::SIMPLE_USER_ACCESS_LEVEL;
        if($request->has('isAdmin') && (floatval(str_replace(',', '.', $request->query('isAdmin'))) > 0)) {
        // a meno che non venga passato un valore > 0 per il parametro isAdmin:
            $request->merge([
            // (prima di leggerlo, sostituisco l'eventuale virgola con un punto):
                'isAdmin' => str_replace(',', '.', $request->query('isAdmin')),
            ]);
            // e setto $access_level al valore contenuto in $this::ALLOWED_USER_IS_ADMIN_VALUES_FROM_TAGMANAGER corrispondente al valore di isAdmin passato: 
            $access_level = $this::ALLOWED_USER_IS_ADMIN_VALUES_FROM_TAGMANAGER[$request->query('isAdmin')];
        }
        $user_record->set_access_level($access_level);
        if($user_record->save()) {
        // salvo il record. Se il salvataggio va a buon fine:
            switch($user_is_new) {
                case true:
                default:
                    $response = response('Created', 201);
                    break;
                case false:
                    $response = response('Updated', 200);
            }
        } else {
        // se il salvataggio non va a buon fine:
            $response = response('Internal server error', 500);
        }
        return $response;
    }

    private function write_socket_snapshot_record(Request $request): HttpResponse {
        Validator::extend('timestamp_string_or_epoch', function ($attribute, $value) {
        // restituisce true se:
            if(is_numeric($value) && ctype_digit($value) && (intval($value) > 0)) {
            // se $value è una stringa contenente solo numeri ed è > 0 (quindi potenzialmente un epoch)
                $is_valid_timestamp_string_or_epoch = true;
            } else {
            // oppure:
                // se $value è una stringa in formato 'Y-m-d H:i:s':
                try {
                // (catcho l'eccezione per poter restituire il messaggio di errore anzichè far esplodere tutto)
                    $is_valid_timestamp_string_or_epoch = Carbon::createFromFormat('Y-m-d H:i:s', $value);
                } catch (\Exception $e) {
                    $is_valid_timestamp_string_or_epoch = false;
                }
            }
            return $is_valid_timestamp_string_or_epoch;
        });
        Validator::extend('numeric_gte0_with_dot_or_comma', function ($attribute, $value, $parameters) {
        // restituisce true se $value è un numero intero o decimale nullo o positivo (con separatore decimale virgola o punto):
            $value = str_replace(',', '.', $value);
            return (is_numeric($value)) && (floatval($value) >= 0);
        });
        $validation_rules = [
            'indLogicoPresa' => 'required|numeric|min:0',       // TODO: devo gestire le prese per le quali non esiste anagrafica? oppure inserisco comunque lo snapshot, che tanto poi l'anagrafica mi arriverà?
            'timestamp' => 'required|timestamp_string_or_epoch',
            'statoComunicazione' => 'required|string',
            'statoOnOffPresa' => 'required|string',
            'contEnergia' => 'required|numeric_gte0_with_dot_or_comma',
            'contAcqua' => 'required|numeric_gte0_with_dot_or_comma',
            'contEnergiaAbb' => 'required|numeric_gte0_with_dot_or_comma',
            'contAcquaAbb' => 'required|numeric_gte0_with_dot_or_comma',
            'transponderID' => 'required|alpha-num|max:8',
            'creditoResiduo' => 'required|numeric_gte0_with_dot_or_comma',
            'codiceEPower' => 'required|numeric',
            'potenza' => 'required|numeric|min:0',
            'allarmi' => 'nullable'
        ];
        $validation_error_messages = [
            'required' => "Bad request. Missing mandatory parameter ':attribute'.",
            'numeric' => "Bad request. Unexpected format for parameter ':attribute'.",
            'min' => "Bad request. Unexpected format for parameter ':attribute'.",
            'string' => "Bad request. Unexpected format for parameter ':attribute'.",
            'timestamp_string_or_epoch' => "Bad request. Unexpected format for parameter ':attribute'.",
            'numeric_gte0_with_dot_or_comma' => "Bad request. Unexpected format for parameter ':attribute'.",
            'alpha-num' => "Bad request. Unexpected format for parameter ':attribute'.",
            'max' => "Bad request. Unexpected format for parameter ':attribute'."
        ];
        $validator = Validator::make($request->all(), $validation_rules, $validation_error_messages);
        if($validator->fails()) {
            return response($validator->errors()->first(), 422);
        }
        $socket_id = intval($request->query('indLogicoPresa'));
        if(is_numeric($request->query('timestamp')) && ctype_digit($request->query('timestamp'))) {
        // il timestamp è in formato UNIX_TIMESTAMP
            $timestamp = Carbon::createFromTimestampUTC($request->query('timestamp'));
        } else if(boolval(Carbon::createFromFormat('Y-m-d H:i:s', $request->query('timestamp')))) {
        // il timestamp è in formato 'yyyy-mm-dd H:i:s (timezone UTC)
            $timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $request->query('timestamp'), new \DateTimeZone(config('app.timezone')));
        }
        $socket_id = intval($request->query('indLogicoPresa'));
        $link_status = $request->query('statoComunicazione');
        $on_off_status = strval($request->query('statoOnOffPresa'));
        $total_power_cnt = floatval(str_replace(',', '.', $request->query('contEnergia')));
        $total_water_cnt = floatval(str_replace(',', '.', $request->query('contAcqua')));
        $postpaid_power_cnt = floatval(str_replace(',', '.', $request->query('contEnergiaAbb')));
        $postpaid_water_cnt = floatval(str_replace(',', '.', $request->query('contAcquaAbb')));
        $transponder_id = $request->query('transponderID');
        $residual_credit = floatval(str_replace(',', '.', $request->query('creditoResiduo')));
        $epower_id = intval($request->query('codiceEPower'));
        // dato che - per ottenere il valore in kW della potenza istantanea erogata dalla presa, devo moltiplicare per 10^-5 il dato ricevuto da SmartView - ne converto la scala direttamente prima di scriverlo sul db:
        $instant_power_consumption = floatval($request->query('potenza')) * pow(10, -5);
        $alarms_string = null;
        if($request->has('allarmi') && (strlen(strval($request->query('allarmi'))) > 0)) {
        // alcuni allarmi (come antifurto o comunicazione assente) mi vengono passati da SmartView nel parametro 'allarmi' della query string
            $alarms_string = strval($request->query('allarmi'));
        }
        if((config('marina.always_dispatch_socket_snapshots_synchronously') === true) || (config('queue.default') === "sync")) {
        // se la configurazione di SmartCloud dice che le snapshot vengono elaborate in maniera SINCRONA (e/o se il dispatching asincrono non è contemplato), la elaboro immediatamente, tenendo il supervisore in attesa della risposta fino alla fine del processo:
            
            // TODO: occhio che dispatchNow() su laravel 8 è deprecata (devo usare dispatchSync()?)
            CreateSocketSnapshotAndSubscriptionSnapshot::dispatchNow($timestamp,
                                                                     $socket_id,
                                                                     $link_status,
                                                                     $on_off_status,
                                                                     $transponder_id,
                                                                     $total_power_cnt,
                                                                     $total_water_cnt,
                                                                     $postpaid_power_cnt,
                                                                     $postpaid_water_cnt,
                                                                     $residual_credit,
                                                                     $instant_power_consumption,
                                                                     $epower_id,
                                                                     $alarms_string);
            $response_for_smartview =  response('Accepted', 202);       // in questo caso rispondo sempre "Accepted" perchè il job non restituisce una risposta utile a conoscere l'esito della scrittura, nè sapere se lo snapshot è stato creato come nuovo oppure aggiornato. Posso comunque verificare a posteriori eventuali errori avvenuti durante l'esecuzione del job, controllando i log di Laravel. TODO: c'è modo di fare diversamente? Ovvero: posso fare in modo che il job - se chiamato in sync - restituisca una variabile che mi dà l'esito, e di conseguenza elaborarla per rispondere in maniera appropriata al supervisore?
        } else {
        // se la configurazione di SmartCloud dice che posso elaborare le snapshot in maniera ASINCRONA e se le code sono configurate, accodo il job e rispondo immediatamente al supervisore, senza farlo attendere:
            CreateSocketSnapshotAndSubscriptionSnapshot::dispatchAfterResponse($timestamp,
                                                                               $socket_id,
                                                                               $link_status,
                                                                               $on_off_status,
                                                                               $transponder_id,
                                                                               $total_power_cnt,
                                                                               $total_water_cnt,
                                                                               $postpaid_power_cnt,
                                                                               $postpaid_water_cnt,
                                                                               $residual_credit,
                                                                               $instant_power_consumption,
                                                                               $epower_id,
                                                                               $alarms_string);
            $response_for_smartview = response('Accepted', 202);       // in questo caso rispondo "Accepted" perchè - dato che la scrittura dei dati avviene in maniera asincrona - al momento della risposta non ho modo di sapere se verrà scritto un nuovo snapshot o se verrà aggiornato l'ultimo snapshot ricevuto per la presa con id socket_id, nè posso sapere se il processo andrà a buon fine o se si verificheranno errori (quest'ultima verifica posso farla a posteriori, controllando i log di Laravel).
        }
        return $response_for_smartview;
    }

    private function write_transponder_subscription_record(Request $request): HttpResponse {
        $need_to_write_thresholds = $need_to_write_costs = false;
        if($request->has('numTransponder')) {
            if(ctype_alnum($request->query('numTransponder'))) {
            // il transponderID contiene solo lettere e numeri
                $transponder_id = $request->query('numTransponder');
            } else {
                return response('Bad request. Unexpected format for parameter \'numTransponder\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'numTransponder\'.', 422);
        }
        if($request->has('utenteId')) {
            if(is_numeric($request->query('utenteId')) && ctype_digit($request->query('utenteId'))) {
                $local_software_user_id = intval($request->query('utenteId'));
            } else {
                return response('Bad request. Unexpected format for parameter \'utenteId\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'utenteId\'.', 422);
        }
        if($request->has('costoPerKWH')) {
            // il supervisore mi invia i decimali usando la virgola come separatore. La sostituisco con il punto:
            $power_cost_per_kwh = str_replace(',', '.', $request->query('costoPerKWH'));
            if(is_numeric($power_cost_per_kwh)) {
                $power_cost_per_kwh = floatval($power_cost_per_kwh);
                $need_to_write_costs = true;
            } else {
                return response('Bad request. Unexpected format for parameter \'costoPerKWH\'.', 422);
            }
        }
        if($request->has('costoPerM3')) {
            // il supervisore mi invia i decimali usando la virgola come separatore. La sostituisco con il punto:
            $water_cost_per_m3 = str_replace(',', '.', $request->query('costoPerM3'));
            if(is_numeric($water_cost_per_m3)) {
                $water_cost_per_m3 = floatval($water_cost_per_m3);
                $need_to_write_costs = true;
            } else {
                return response('Bad request. Unexpected format for parameter \'costoPerM3\'.', 422);
            }
        }
        if($request->has('sogliakWhMin')) {
            // il supervisore mi invia i decimali usando la virgola come separatore. La sostituisco con il punto:
            $min_kwh_threshold = str_replace(',', '.', $request->query('sogliakWhMin'));
            if(is_numeric($min_kwh_threshold)) {
                $min_kwh_threshold = floatval($min_kwh_threshold);
                $need_to_write_thresholds = true;
            } else {
                return response('Bad request. Unexpected format for parameter \'sogliakWhMin\'.', 422);
            }
        }
        if($request->has('sogliakWhMax')) {
            // il supervisore mi invia i decimali usando la virgola come separatore. La sostituisco con il punto:
            $max_kwh_threshold = str_replace(',', '.', $request->query('sogliakWhMax'));
            if(is_numeric($max_kwh_threshold)) {
                $max_kwh_threshold = floatval($max_kwh_threshold);
                $need_to_write_thresholds = true;
            } else {
                return response('Bad request. Unexpected format for parameter \'sogliakWhMax\'.', 422);
            }
        }
        if($request->has('sogliaCreditoMin')) {
            // il supervisore mi invia i decimali usando la virgola come separatore. La sostituisco con il punto:
            $min_credit_threshold = str_replace(',', '.', $request->query('sogliaCreditoMin'));
            if(is_numeric($min_credit_threshold)) {
                $min_credit_threshold = floatval($min_credit_threshold);
                $need_to_write_thresholds = true;
            } else {
                return response('Bad request. Unexpected format for parameter \'sogliaCreditoMin\'.', 422);
            }
        }
        if($request->has('sogliaTempoAcquaMax')) {
            if(is_numeric($request->query('sogliaTempoAcquaMax')) && ctype_digit($request->query('sogliaTempoAcquaMax'))) {
                $max_water_tap_time_minutes_threshold = intval($request->query('sogliaTempoAcquaMax'));
                $need_to_write_thresholds = true;
            } else {
                return response('Bad request. Unexpected format for parameter \'sogliaTempoAcquaMax\'.', 422);
            }
        }
        if($request->has('dataInizio')) {
            if(is_numeric($request->query('dataInizio')) && ctype_digit($request->query('dataInizio'))) {
            // il timestamp è in formato UNIX_TIMESTAMP
                $subscription_begin_timestamp = Carbon::createFromTimestampUTC($request->query('dataInizio'));
            } else if(boolval(Carbon::createFromFormat('Y-m-d H:i:s', $request->query('dataInizio')))) {
            // il timestamp è in formato 'yyyy-mm-dd H:i:s (timezone UTC)
                $subscription_begin_timestamp = Carbon::parse($request->query('dataInizio'), new \DateTimeZone(config('app.timezone')));
            } else {
                return response('Bad request. Unexpected format for parameter \'dataInizio\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'dataInizio\'.', 422);
        }
        if($request->has('dataFine')) {
        // qui l'else non c'è perchè dataFine non è obbligatorio (a dire il vero, forse sergio non ha mai implementato nel TagManager le chiamate da fare quando una subscription termina: forse semplicemente me ne accorgo perchè ne è iniziata un'altra con lo stesso transponder)
            if(is_numeric($request->query('dataFine')) && ctype_digit($request->query('dataFine'))) {
            // il timestamp è in formato UNIX_TIMESTAMP
                $subscription_end_timestamp = Carbon::createFromTimestampUTC($request->query('dataFine'));
            } else if(boolval(Carbon::createFromFormat('Y-m-d H:i:s', $request->query('dataFine')))) {
            // il timestamp è in formato 'yyyy-mm-dd H:i:s (timezone UTC)
                $subscription_end_timestamp = Carbon::parse($request->query('dataFine'), new \DateTimeZone(config('app.timezone')));
            } else {
                return response('Bad request. Unexpected format for parameter \'dataFine\'.', 422);
            }
        }
        $one_second_ago = (Carbon::now(config('app.timezone')))->subSeconds(1);
        // verifico se l'utente passato esiste già sul mio db:
        $user = User::find_by_local_software_id($local_software_user_id);
        if((!$user) || ($user->count() === 0)) {
        // se l'utente passato NON esiste ancora sul mio db:
            // se NON esiste ancora:
            return response('Error: the user with id '.$local_software_user_id.' doesn\'t exist on database. Please create it before sending his subscriptions.', 422);
        }
        $subscription_with_provided_user_already_exists = false;
        // verifico anche se il transponder_id dello snapshot ha già una corrispondenza in anagrafica. Se non ce l'ha, dovrò inserirlo (altrimenti, nel momento in cui verrà creata una subscription legata a quel transponder, mancando il transponder in anagrafica non potrò calcolarne il proprietario):
        // se il transponder NON esiste ancora in anagrafica, prima di proseguire lo inserisco (sul db, il valore di default del campo used_for_socket_subscriptions è false. Dovrebbe essere il modo corretto di procedere, perchè verrà settato come usato per le socket subscriptions solo nel momento in cui verrà usato per crearne una):
        $transponder = Transponder::firstOrCreate(['code' => $transponder_id]);
        if(!($transponder->is_used_for_socket_subscriptions())) {
        // verifico se il transponder passato è marcato come utilizzato per creare dei contratti su presa
            // se non lo è:
            $transponder_active_subscriptions = $transponder->active_subscriptions;
            if(!is_null($transponder_active_subscriptions) && ($transponder_active_subscriptions->count() > 0)) {
            // controllo se ha subscriptions attive (nel caso dei transponder non usati per creare contratti su presa, possono essere al massimo una):
                // se ne ha, le scorro:
                foreach($transponder_active_subscriptions as $active_subscription) {
                    if((!($active_subscription->user) || ($active_subscription->user->count() === 0)) ||
                       (intval($active_subscription->user->get_local_software_id()) !== intval($local_software_user_id))) {
                    // (se l'utente legato alla subscription attuale non è l'utente passato nella chiamata - oppure se l'utente non esiste sul db)
                        // le termino all'istante attuale - 1 secondo:
                        $active_subscription->end_on_timestamp_and_save($one_second_ago);
                    } else {
                        $subscription_with_provided_user_already_exists = true;
                        $subscription_with_provided_user = $active_subscription;
                    }
                }
                // TODO: forse semplificherei, dato che in questo caso la subscription attiva è al massimo una
            }
            if($subscription_with_provided_user_already_exists) {
            // se la subscription che lega $transponder_id e $user_id esiste già:
                if(isset($subscription_end_timestamp)) {
                // e se mi è stato passato un datetime di fine associazione:
                    // verifico che sia successivo al datetime di inizio associazione che mi risulta dal db:
                    $subscription_start_timestamp_from_db = $subscription_with_provided_user->get_begin_timestamp();
                    if($subscription_end_timestamp->lessThan($subscription_start_timestamp_from_db)) {
                        // se il timestamp di fine subscription passato è PRECEDENTE a quello di inizio subscription trovato sul db:
                        // termino restituendo un errore:
                        return response('Error: the provided timestamp for \'dataFine\' is before the subscription creation.', 422);
                    }
                    // in caso contrario, termino la subscription (assieme ai suoi eventuali record di costi e soglie) al datetime di termine passato:
                    $active_subscription->end_on_timestamp_and_save($subscription_end_timestamp);
                    $msg = 'Subscription ended successfully';
                    if($need_to_write_costs || $need_to_write_thresholds) {
                    // se in questa chiamata mi sono stati passati anche dei costi o delle soglie:
                        // aggiungo un messaggio che informa che sono stati ignorati, dato che l'associazione è stata terminata:
                        $msg .= ' (costs and/or thresholds have been ignored).';
                    } else {
                        $msg .= '.';
                    }
                    return response($msg, 200);
                } else {
                // se NON mi è stato passato un timestamp di fine associazione:
                    $msg = 'Error: neither an end timestamp nor new costs or alarm thresholds have been provided for the subscription. Nothing to do here.';
                    $code = 422;
                    if(($need_to_write_costs) || ($need_to_write_thresholds)) {
                    // se mi sono stati passati nuovi costi e/o soglie allarmi:
                        $code = 200;
                        if($need_to_write_costs) {
                        // se mi sono stati passati nuovi costi:
                            // li aggiorno:
                            $active_subscription->set_costs_and_save_costs_record($power_cost_per_kwh, $water_cost_per_m3);     // il metodo set_costs_an_save_costs_record() verifica già se è effettivamente necessario scrivere un nuovo record, prima di farlo
                            $msg = 'Subscription costs ';
                            // TODO: il metodo set_costs_an_save_costs_record() chiude i vecchi costi 1 secondo fa e apre il nuovo record costi a now(). Fare in modo di poter specificare uno/i due timestamp?
                            $costs_updated = true;
                        }
                        if($need_to_write_thresholds) {
                        // se mi sono stati passati nuove soglie allarmi:
                            // le aggiorno:
                            $active_subscription->set_alarm_thresholds_and_save_thresholds_record($min_kwh_threshold, $max_kwh_threshold, $max_water_tap_time_minutes_threshold, $min_credit_threshold);     // il metodo set_alarm_thresholds_and_save_thresholds_record() verifica già se è effettivamente necessario scrivere un nuovo record, prima di farlo
                            // TODO: il metodo set_alarm_thresholds_and_save_thresholds_record() chiude le vecchie soglie 1 secondo fa e apre il nuovo record soglie a now(). Fare in modo di poter specificare uno/i due timestamp?
                            if(!$need_to_write_costs) {
                                $msg = 'Subscription alarm thresholds ';
                            } else {
                                $msg .= 'and alarm thresholds ';
                            }
                        }
                        $msg .= 'have been updated.';

                        // TODO: ma se io utente aggiorno le mie soglie da SmartCloud, poi il tagmanager me le rimanda come erano prima e questa api le riaggiorna, cosa succede? Considerare che ci sono impianti in cui il TagManager è in funzione e contemporaneamente è possibile creare le subscriptions da SmartCloud. EDIT: non ricordo bene quando abbiamo introdotto questa cosa dell'invio delle soglie da TagManager (probabilmente viene dalla versione 1.x di SmartCloud), ma non mi pare che abbia senso: pur essendo dati che mi vengono effettivamente inviati dal software, credo siano sempre settati a zero per tutti i contratti: come potrebbe un utente modificarsi le soglie da TagManager, dato che TagManager lo usa lo staff del marina? Una volta appurato che non serve, togliere la gestione delle soglie da questo metodo.

                        // se ho aggiornato i costi e/o le soglie del contratto, dovrò fare in modo che il prossimo socket_snapshot che arriverà per questo contratto rimanga al sicuro da eventuali sovrascritture e cancellazioni:
                        // recupero l'ultimo socket_snapshot legato al contratto
                        $subscription_last_socket_snapshot = $active_subscription->get_last_socket_snapshot();
                        if(($subscription_last_socket_snapshot) && ($subscription_last_socket_snapshot->count() > 0)) {
                        // se l'ultimo socket_snapshot legato al contratto esiste:
                            // setto il suo keep_snapshot_flag a true, per fare in modo che non venga sovrascritto o cancellato:
                            $subscription_last_socket_snapshot->set_keep_snapshot_flag(true);
                             // setto il suo keep_next_snapshot_flag a true, per fare in modo che nemmeno il prossimo record per la stessa presa venga sovrascritto o cancellato:
                            $subscription_last_socket_snapshot->set_keep_next_snapshot_flag(true);
                            // e lo salvo:
                            $subscription_last_socket_snapshot->save();
                        }
                        return response($msg, $code);
                    } else {
                    // se NON mi sono stati passati nuovi costi, nè soglie allarmi:
                        //  restituisco un messaggio di errore, dato che non avendo un datetime di chiusura, a questo punto mi aspetto che l'unico motivo per cui è stata fatta la chiamata sia l'aggiornamento di costi o soglie:
                        return response('Error: neither an end timestamp nor new costs or alarm thresholds have been provided for the subscription. Nothing to do here.', 422);
                    }
                }
            } else {
            // se la subscription che lega $transponder_id e $local_software_user_id NON esiste sul db:
                // la creo:
                $subscription = new Subscription();
                $subscription->set_transponder_id($transponder_id);
                $subscription->set_user_id($user->get_id());
                $subscription->set_begin_timestamp($subscription_begin_timestamp);
                if(isset($subscription_end_timestamp)) {
                    $subscription->set_end_timestamp($subscription_end_timestamp);
                }
                $subscription->save();
                if(!isset($min_kwh_threshold)) {
                    $min_kwh_threshold = 0;
                }
                if(!isset($max_kwh_threshold)) {
                    $max_kwh_threshold = 0;
                }
                if(!isset($max_water_tap_time_minutes_threshold)) {
                    $max_water_tap_time_minutes_threshold = 0;
                }
                if(!isset($min_credit_threshold)) {
                    $min_credit_threshold = 0;
                }
                $subscription->set_alarm_thresholds_and_save_thresholds_record($min_kwh_threshold, $max_kwh_threshold, $max_water_tap_time_minutes_threshold, $min_credit_threshold);
                if(!isset($power_cost_per_kwh)) {
                    $power_cost_per_kwh = 0;
                }
                if(!isset($water_cost_per_m3)) {
                    $water_cost_per_m3 = 0;
                }
                $subscription->set_costs_and_save_costs_record($power_cost_per_kwh, $water_cost_per_m3);
                return response('Subscription created successfully.', 201);
            }
        } else {
        // se il transponder viene usato per creare contratti su presa:
        
            /* TODO: come gestire il caso in cui mi venga passata una subscription legata ad un transponder che ho già in anagrafica e che ho marcato come usato per le socket subscriptions? Avevo pensato di terminare istantaneamente tutte le subscriptions ad esso legate (così come i relativi record di costi e soglie) ed informarne l'utente al termine del processamento della richiesta, ma se lo faccio, devo ricordare all'utente che setta il transponder come usato per le socket subscriptions, che nel momento in cui lo userà per creare una subscription prepagata usando tagmanager, terminerà istantaneamente tutti i contratti ad esso legati.
            Potrebbe avere senso in questo caso aggiungere una quarta pagina al menu Manage della webapp, che permette di vedere tutti i transponder usati nell'impianto, se sono usati per socket subscription o meno, e di recuperare per ciascuno le subscriptions attuali e quelle passate (sarebbe solo per consultazione, no modifica). Però direi di pensarci più avanti
            EDIT: in realtà, nel momento in cui creo una transponder subscription col ws di insert, termino automaticamente anche un'eventuale precedente subscription su transponder. Quindi è un discorso più generale. La differenza è solo che se il transponder è usato per le socket subscriptions, ne vado a terminare piu di una.
            Oppure - più semplicemente - potrei chiedere di terminare le subscriptions attuali e settare il transponder come NON usato per le socket subscriptions? Per ora è quello che faccio. 
            EDIT: ma come gestisco questa cosa ora che ho modificato il modo di procedere relativamente alle socket_subscriptions? (v. richiesta roddy blair di gennaio 2021 + branch bugfix/socket_subscriptions_arent_saved_with_transponder_id)
            */

            return response('Unable to bind a new subscription to this transponder key, as it is being used for socket subscriptions only.', 409);
        }
    }

    private function get_pending_actions() {
    // TODO: funziona, ma riscriverlo usando un model decente invece di una stdClass?
        $pending_actions =  Plant::get_pending_actions();
        $msg = [];
        foreach($pending_actions as $action) {
            $entry = new stdClass;
            $entry->action = new stdClass;
            $entry->action->idCodaAzioni = $action->id;
            $entry->action->tipoAzione = $action->type;
            $entry->action->tipoOggettoAzione = $action->object_type;
            $entry->action->idOggettoAzione = $action->object_id;
            $entry->action->dataInserimento = $action->request_timestamp;
            $entry->action->utenteIDrichiesta = $action->requesting_user_id;            // TODO: attenzione: al momento questo è lo user_id "vero" e non il local_software_user_id, quindi smartview non può associarlo ad un utente del suo db (al momento non ha bisogno di farlo, ma tenere presente questa cosa)
            $entry->action->dataCompletamento = $action->completion_timestamp;
            $entry->action->idTerminaleCompletamento = $action->completing_terminal_name;
            $msg[] = $entry;
        }
        return response()->json(['status' => 1, 'msg' => $msg]);
    }

    public function set_action_as_completed(Request $request): HttpResponse {
        if($request->has('idCodaAzioni')) {
        // id dell'azione da settare come completata:
            if(is_numeric($request->query('idCodaAzioni')) && ctype_digit($request->query('idCodaAzioni'))) {
                $action_id = intval($request->query('idCodaAzioni'));
            } else {
                return response('Bad request. Unexpected format for parameter \'idCodaAzioni\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'idCodaAzioni\'.', 422);
        }
        if($request->has('dataCompletamento')) {
        // timestamp in cui l'azione è stata completata da parte del supervisore:
            if(is_numeric($request->query('dataCompletamento')) && ctype_digit($request->query('dataCompletamento'))) {
            // il timestamp è in formato UNIX_TIMESTAMP
                $completion_timestamp = Carbon::createFromTimestampUTC($request->query('dataCompletamento'));
            } else if(boolval(Carbon::createFromFormat('Y-m-d H:i:s', $request->query('dataCompletamento')))) {
            // il timestamp è in formato 'yyyy-mm-dd H:i:s (timezone UTC)
                $completion_timestamp = Carbon::parse($request->query('dataCompletamento'), new \DateTimeZone(config('app.timezone')));
            } else {
                return response('Bad request. Unexpected format for parameter \'dataCompletamento\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'dataCompletamento\'.', 422);
        }
        $action = Action::find($action_id);
        if((!$action) || (!($action->count() > 0))) {
            return response('Action with id='.$action_id.' not found.', 404);
        }
        try {
            $action->set_completion_timestamp($completion_timestamp);
            if(($action->get_type() === Action::ACTION_TYPE_RESET_SOCKET) || ($action->get_type() === Action::ACTION_TYPE_TURN_OFF_SOCKET)) {
            // se l'azione è uno spegnimento o un reset: 
                // recupero la relativa presa e l'ultimo snapshot conosciuto prima di $completion_timestamp, se esistono sul db:
                $resetted_socket = Berth::find($action->get_object_id());
                if(($resetted_socket) && ($resetted_socket->count() > 0)) {
                    $last_socket_snapshot_at_time_of_resetting = $resetted_socket->get_last_socket_snapshot_before_timestamp($completion_timestamp, true);
                    if(($last_socket_snapshot_at_time_of_resetting) && ($last_socket_snapshot_at_time_of_resetting->count() > 0)) {
                    // se lo snapshot esiste:
                        if($last_socket_snapshot_at_time_of_resetting->get_residual_credit() > 0) {
                        // e se il credito disponibile sulla presa al momento dello snapshot era > 0 (potrebbe essere uguale a zero in caso di contratto postpagato: in quel caso, non ha senso creare una riga di log)
                            // creo un nuovo LostMoneyForResettedSockets:
                            $lost_money_entry = new LostMoneyForResettedSockets();
                            // lo collego ad $action:
                            $lost_money_entry->action_id = $action->get_id();
                            // lo collego a $last_socket_snapshot_at_time_of_resetting:
                            $lost_money_entry->socket_snapshot_id = $last_socket_snapshot_at_time_of_resetting->get_id();
                            // setto l'ultimo credito conosciuto per la presa all'istante dello spegnimento al valore del credito residuo di $last_socket_snapshot_at_time_of_resetting: 
                            $lost_money_entry->money_on_the_socket_when_resetted = $last_socket_snapshot_at_time_of_resetting->get_residual_credit();
                            $lost_money_entry->save();
                            // poi imposto il collegamento inverso (da $action a $lost_money_entry): 
                            $action->lost_money_entry_id = $lost_money_entry->id;
                            // TODO: se mi perdo delle snapshot (oppure se quando il supervisore mi invia la conferma di avvenuto reset, mi ha già inviato il nuovo snapshot con la presa in off) il credito residuo che registro potrebbe non essere quello corretto. Forse è meglio far settare il valore direttamente dal supervisore con un campo in più, invece recuperarlo dal db?
                        }
                    } else {
                        // TODO: cosa faccio se non ho info sullo stato della presa quando viene spenta? (può succedere: magari l'installazione di smartcloud è nuova e il supervisore non mi ha ancora mandato nulla per quella presa)
                    }
                }
            }
            // infine salvo $action:
            $action->save();
        } catch(\Exception $e) {
            return response('Error while setting the action as completed. Invalid format for field \'dataCompletamento\'.', 422);
        }
        return response('Updated', 200);
    }

    public function write_notification_record(Request $request): HttpResponse {
    // TODO: questo metodo serve ancora? Ho l'impressione che SmartView/TagManager non chiamino mai questo endpoint, verificare ed eventualmente togliere
        if($request->has('utenteId')) {
            if(is_numeric($request->query('utenteId')) && ctype_digit($request->query('utenteId'))) {
                $local_software_user_id = intval($request->query('utenteId'));
            } else {
                return response('Bad request. Unexpected format for parameter \'utenteId\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'utenteId\'.', 422);
        }
        if($request->has('tipoNotifica')) {
            $notification_type = $request->query('tipoNotifica');
        } else {
            $notification_type = null;
        }
        if($request->has('tipoOggetto')) {
            $notification_object_type = $request->query('tipoOggetto');
        } else {
            $notification_object_type = null;
        }
        if($request->has('nomeOggetto')) {
            $notification_object_name = $request->query('nomeOggetto');
        } else {
            return response('Bad request. Missing mandatory parameter \'nomeOggetto\'.', 422);
        }
        if($request->has('testo')) {
            $notification_text = $request->query('testo');
        } else {
            $notification_text = null;
        }
        if($request->has('dataOraInizio')) {
            if(is_numeric($request->query('dataOraInizio')) && ctype_digit($request->query('dataOraInizio'))) {
            // il campo mi è stato inviato in formato UNIX_TIMESTAMP
                $notification_creation_timestamp = Carbon::createFromTimestampUTC($request->query('dataOraInizio'));
            } else if(boolval(Carbon::createFromFormat('Y-m-d H:i:s', $request->query('dataOraInizio')))) {
            // il timestamp è in formato 'yyyy-mm-dd H:i:s (timezone UTC)
                $notification_creation_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $request->query('dataOraInizio'), new \DateTimeZone(config('app.timezone')));
            } else {
                return response('Bad request. Unexpected format for parameter \'dataOraInizio\'.', 422);
            }
        } else {
            return response('Bad request. Missing mandatory parameter \'dataOraInizio\'.', 422);
        }
        if($notification_type === SmartCloudNotification::NOTIFICATION_TYPE_STRING_USED_BY_SMARTVIEW_TO_CLOSE_ERRORS) {
        // se lo scopo della chiamata è la chiusura di una notifica aperta in precedenza:
            if($request->has('dataOraChiusura')) {
                if(is_numeric($request->query('dataOraChiusura')) && ctype_digit($request->query('dataOraChiusura'))) {
                    // il campo mi è stato inviato in formato UNIX_TIMESTAMP
                    $notification_closing_timestamp = Carbon::createFromTimestampUTC($request->query('dataOraChiusura'));
                } elseif(boolval(Carbon::createFromFormat('Y-m-d H:i:s', $request->query('dataOraInizio')))) {
                    // il timestamp è in formato 'yyyy-mm-dd H:i:s (timezone UTC)
                    $notification_closing_timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $request->query('dataOraChiusura'), new \DateTimeZone(config('app.timezone')));
                } else {
                    return response('Bad request. Unexpected format for parameter \'dataOraChiusura\'.', 422);
                }
            } else {
                return response('Bad request. The specified value for \'tipoNotifica\' is \''.SmartCloudNotification::NOTIFICATION_TYPE_STRING_USED_BY_SMARTVIEW_TO_CLOSE_ERRORS.'\' but the parameter \'dataOraChiusura\' is missing.', 422);
            }
            $open_notifications_for_specified_object_name = Plant::get_open_notifications_by_object_name($notification_object_name);
            if(!is_null($open_notifications_for_specified_object_name)) {
                foreach($open_notifications_for_specified_object_name as $current_notification) {
                    $current_notification->set_closed_at_timestamp($notification_closing_timestamp);
                    // se chiudo una notifica esistente, devo eliminarne l'eventuale timestamp di lettura da parte dell'utente interessato, non saprà mai della sua chiusura (la view delle notifiche mostra per prime le notifiche non lette):
                    $current_notification->set_read_at_timestamp(null);
                    $current_notification->save();
                    return response('Updated', 200);
                }
            } else {
                return response('Error: no open notifications found for the object named \''.$notification_object_name.'\'.', 422);
            }
        } else {
        // se lo scopo della chiamata NON è la chiusura di una notifica aperta in precedenza, ma l'apertura di una nuova notifica:
            $users_to_notify = [];
            if($local_software_user_id === 0) {
            // quando il supervisore mi invia le notifiche destinate agli admin, mi passa utenteId === 0:
                // cerco gli utenti admin non disabilitati:
                $users_to_notify = User::where('access_level', '>', 0)
                                       ->where('is_disabled', '=', 0)
                                       ->get();
            } else {
            // se il supervisore mi ha inviato l'id di uno specifico utente al quale inviare la notifica:
                // lo cerco sul db:
                $users_to_notify = User::find_by_local_software_id($local_software_user_id);
                /* TODO: se non erro, questo caso non avviene mai, perchè il supervisore non può sapere a quali utenti non-admin inviare una notifica. Togliere questo else se non serve. EDIT: può avere senso - quando viene inviata una notifica relativamente ad una presa che non comunica - verificare qui se quella presa è attualmente usata da un utente cloud? (e fare la stessa cosa per le notifiche relative alle linee seriali che non comunicano, cercando tutte le prese che ne fanno parte e verificando per ciascuna presa se ci sono utenti che la stanno usando?). In questo modo potrei inviare una notifica in-app e via mail a ciascuno degli utenti utilizzatori. Però non so se sia conveniente avvisare gli utilizzatori che c'è un problema di comunicazione, i gestori del porto potrebbero non essere d'accordo. */
            }
            $inserted_notifications = 0;
            if(($users_to_notify) && ($users_to_notify->count() > 0)) {
            // se ho trovato almeno un utente al quale inviare la notifica:
                switch($notification_type) {
                // setto il campo $notification_type della notifica da scrivere sul db a seconda del valore del campo tipoNotifica inviato dal supervisore:
                    case SerialLineOfflineAlarmNotification::NOTIFICATION_TYPE_FROM_SMARTVIEW:
                        $notification_type = SerialLineOfflineAlarmNotification::class;
                        // siccome il campo $notification_object_id non è previsto dall'attuale versione di SmartView (v.3.28 nel momento in cui scrivo), devo calcolarlo io. Per quanto riguarda le notifiche di seriale non comunicante è facile: è semplicemente il numero della seriale, che SmartView passa come nomeOggettoNotifica:
                        $notification_object_id = intval($notification_object_name);
                        break;
                    default:
                        // TODO: al momento non mi pare sia previsto che il supervisore possa valorizzare il campo tipoNotifica con valori diversi da  quello di SerialLineOfflineAlarmNotification::NOTIFICATION_TYPE_FROM_SMARTVIEW. Verificare, ed eventualmente aggiungere qui il tipo di notifica da settare in questo caso. Al momento, se si verifica questa eventualità, setto il valore del tipo notifica a stringa vuota per evitare eccezioni (non posso settarlo a null perchè poi il metodo User::has_already_received_notification non accetta valori null per il campo $type. EDIT: il 19.05.2020 ho fatto modifiche sul campo $type, vefificare se questa cosa è ancora valida):
                        $notification_type = '';
                        // TODO: al momento, dato che non prevedo di arrivare mai qui, setto anche in questo caso $notification_object_id prendendo l'intval del nomeOggettoNotifica (ma è solo per evitare eccezioni. Nel momento in cui il supervisore potrà inviare notifiche diverse da quelle di seriale offline, dovrò modificare questa parte):
                        $notification_object_id = intval($notification_object_name);
                }
                foreach($users_to_notify as $current_user) {
                // scorro gli utenti ai quali la devo inviare:

                    // $notification_already_sent = $current_user->has_already_received_notification($notification_type, $notification_object_id);
                    // TODO: il metodo User::has_already_received_notification() non sta funzionando correttamente (v. commenti sul metodo), quindi provo a fare la stessa verifica direttamente qui di seguito, in attesa di sistemarlo:

                    $open_notifications_with_same_data_count =  $current_user->notifications()
                                                                             ->where('type', $notification_type)
                                                                             ->where('data->object_id', 'like', '%'.$notification_object_id.'%')
                                                                             ->where('data->closed_at', 'like', '%null%')
                                                                             ->count();
                    if($open_notifications_with_same_data_count === 0) {
                    // se l'utente corrente non ha già ricevuto la stessa notifica:
                        // gliela invio ora:
                        switch($notification_type) {
                        // a seconda del valore di $notification_type, invio la notifica appropriata:
                            case SerialLineOfflineAlarmNotification::class:
                                $current_user->notify(new SerialLineOfflineAlarmNotification($notification_creation_timestamp,
                                                                                             $notification_object_id,
                                                                                             $notification_object_name));
                                break;
                            default:
                                // TODO: al momento non mi pare sia previsto che il supervisore possa valorizzare il campo tipoNotifica con valori diversi da  quello di SerialLineOfflineAlarmNotification::NOTIFICATION_TYPE_FROM_SMARTVIEW. Verificare, ed eventualmente aggiungere qui l'invio della notifica appropriata per ciascun caso.
                        }
                        $inserted_notifications++;
                    }
                }
            }
            if($inserted_notifications > 0) {
                $message = 'Notification';
                if($inserted_notifications > 0) {
                    $message .= 's';
                }
                $message .= ' created successfully.';
                return response($message, 201);
            } else {
                return response('No users to notify.', 200);
            }
        }
    }
}
