<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Plant;
use App\Pedestal;
use App\Berth;
use App\SocketLastSnapshot;
use App\User;
use App\LostMoneyForResettedSockets;
use App\Jobs\GenerateConsumptionReport;

class PlantController extends Controller {

    public function plant_status_page() {
        $available_socket_types = Plant::get_available_socket_types();
        $sockets = Berth::with(['last_snapshot',
                                'last_snapshot.subscription',
                                'last_snapshot.subscription.user',
                                'last_snapshot.subscription.user.boat'])
                        ->orderBy('pontoon_name', 'asc');
        if(!boolval(config('marina.alphanumeric_berth_names'))) {
            $sockets = $sockets->orderByRaw('CAST(berth_name as UNSIGNED) ASC');
        } else {
            $sockets = $sockets->orderBy('berth_name', 'asc');
        }
        $sockets = $sockets->orderBy('description', 'asc')
                           ->orderBy('id', 'asc')
                           ->get();
        return view('berths/status', compact('available_socket_types',
                                             'sockets'));
    }

    public function plant_map_page() {
        $available_socket_types = Plant::get_available_socket_types();
        $pedestals = Pedestal::with(['berths',
                                     'berths.last_snapshot',
                                     'berths.last_snapshot.transponder',
                                     'berths.last_snapshot.transponder.active_subscriptions',
                                     'berths.last_snapshot.transponder.active_subscriptions.user',
                                     'berths.last_snapshot.transponder.active_subscriptions.user.boat'])->get();
        return View::make('berths/map', ['pedestals' => $pedestals,
                                         'available_socket_types' => $available_socket_types]);
    }

    public function get_consumption_report_file(int $begin_epoch_timestamp, int $end_epoch_timestamp, string $file_type) {
        $need_to_redirect_back = false;
        $flash_message_for_redirect = null;
        $all_berths_in_plant = Plant::get_all_berths();
        if((!$all_berths_in_plant) || ($all_berths_in_plant->count() === 0)) {
        // se non c'è nessun posto barca in anagrafica, redireziono alla pagina di provenienza con un flash_message di errore:
            $need_to_redirect_back = true;
            $flash_message_for_redirect = ucfirst(__('consumption_reports.cannot_generate_consumption_report_because_no_berths_in_plant_msg'));
        }
        // valido i timestamp passati (mantengo la timezone perchè forse Laravel altrimenti si aspetta di riceverli con la timezone locale):
        $min_dt = Carbon::createFromFormat('Y-m-d H:i:s', config('dates_currency_and_locale.min_utc_timestamp'), new \DateTimeZone(config('app.timezone')));
        $max_dt = Carbon::createFromFormat('Y-m-d H:i:s', config('dates_currency_and_locale.max_utc_timestamp'), new \DateTimeZone(config('app.timezone')));
        $begin_dt = Carbon::parse($begin_epoch_timestamp, new \DateTimeZone(config('app.timezone')));
        $end_dt = Carbon::parse($end_epoch_timestamp, new \DateTimeZone(config('app.timezone')));
        if(($begin_dt < $min_dt) || ($end_dt > $max_dt) || ($end_dt > $max_dt)) {
        // se i timestamp non passano la validazione, redireziono alla pagina di provenienza con un flash_message di errore:
            $need_to_redirect_back = true;
            $flash_message_for_redirect = ucfirst(__('consumption_reports.report_for_a_malformed_period_requested_msg'));
        }
        if($need_to_redirect_back) {
            $previous_page = array_slice(explode('/', url()->previous()), -1)[0];       // TODO: forse posso usare redirect()->back()
            return redirect($previous_page)->with('flash_message', $flash_message_for_redirect)
                                           ->with('flash_background_color', 'yellow');
        }
        GenerateConsumptionReport::dispatch(Auth::user(),
                                            ConsumptionReportsController::SUBJECT_TYPE_MARINA, 
                                            [], 
                                            $file_type, 
                                            $begin_dt, 
                                            $end_dt)
                                 ->onQueue('reports');
        $flash_message = ucfirst(__('consumption_reports.your_request_has_been_received_you_will_get_a_notification_when_its_available'));
        $flash_background_color = 'green';
        return redirect('consumption_reports')->with('flash_message', $flash_message)
                                              ->with('flash_background_color', $flash_background_color);
    }

    public function open_refunds_log_page() {
        $log = LostMoneyForResettedSockets::with('action')
                                          ->orderBy('created_at', 'desc')
                                          ->get();
        return View::make('refunds_log', compact('log'));
    }


    // TODO: spostare qui dentro i metodi degli altri controller che non hanno strettamente a che fare con il controller in cui si trovano, ma che si adattano meglio a questo?
}
