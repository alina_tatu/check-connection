<?php

namespace App\Http\Controllers;
use App\User;
use App\Berth;
use App\Plant;
use App\Subscription;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use App\Jobs\GenerateConsumptionReport;

class SubscriptionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        // get all the subscriptions
        $subscriptions = Plant::get_all_subscriptions();
        // load the view and pass the subscriptions
        return View::make('subscriptions.index', ['subscriptions' => $subscriptions]);
    }

    public function open_new_subscription_on_berth_form(Request $request, $berth_id) {
        $users = Plant::get_all_users();
        $berth = Berth::findOrFail($berth_id);
        // TODO: devo verificare che non ci già una berth_subscription in essere su $berth. Se c'è, devo tornare alla pagina di provenienza con un flash message
        return View::make('berths.create_subscription', ['berth' => $berth,
                                                         'users' => $users]);
    }

    public function create_new_subscription_on_berth(Request $request) {
        $subscription_start_datetime_string = $request->input('subscription_start_date').' '.$request->input('subscription_start_time');
        $subscription_start_datetime = Carbon::parse($subscription_start_datetime_string, new \DateTimeZone(config('dates_currency_and_locale.local_timezone')))->setTimezone(config('app.timezone'));
        $subscription_start_datetime_string = $subscription_start_datetime->format('Y-m-d H:i:s');
        // specifico la timezone perchè altrimenti credo che laravel se li aspetti in timezone locale:
        $subscription_min_creation_datetime_string = Carbon::parse(config('dates_currency_and_locale.min_utc_timestamp', new \DateTimeZone(config('app.timezone'))));
        $subscription_max_creation_datetime_string = Carbon::parse(config('dates_currency_and_locale.max_utc_timestamp', new \DateTimeZone(config('app.timezone'))));
        $user_found_on_db = Plant::get_user_by_full_name($request->input('user_full_name'));
        if(($user_found_on_db) && ($user_found_on_db->count() > 0)) {
            $user_found_on_db_id = $user_found_on_db->get_id();
        } else {
            $user_found_on_db_id = null;
        }
        $berth_found_on_db = Berth::find($request->input('berth_id'));
        if(($berth_found_on_db) && ($berth_found_on_db->count() > 0)) {
            $berth_found_on_db_id = $berth_found_on_db->id;
        } else {
            $berth_found_on_db_id = null;
        }
        $transponder_id = $request->input('transponder_id');
        $request->merge([
            'user_found_on_db_id' => $user_found_on_db_id,
            'berth_found_on_db_id' => $berth_found_on_db_id,
            'subscription_start_datetime_string' => $subscription_start_datetime_string
        ]);
        $validation_rules = [
            // TODO: devo anche verificare che non ci sia già una subscription in corso su quel posto barca
            'berth_found_on_db_id' => 'required|numeric|min:1',
            'user_found_on_db_id' => 'required|numeric|min:1',
            'transponder_id' => 'required|exists:transponders,code',
            'subscription_start_datetime_string' => 'required|date_format:Y-m-d H:i:s|before:'.$subscription_max_creation_datetime_string.'|after:'.$subscription_min_creation_datetime_string,
        ];
        $validation_error_messages = [
            'min' => ucfirst(__('validation.custom.generic.invalid_value')),
            'numeric' => ucfirst(__('validation.custom.generic.invalid_value')),
            'before' => ucfirst(__('validation.custom.generic.invalid_value')),
            'after' => ucfirst(__('validation.custom.generic.invalid_value')),
            'date_format' => ucfirst(__('validation.custom.generic.invalid_value')),
            'required' => ucfirst(__('validation.custom.generic.required_value')),
            'transponder_id.exists' => ucfirst(__('validation.custom.views.berths.create_subscription.transponder_id.not_found_in_transponders_table')),
        ];
        $validator = Validator::make($request->all(), $validation_rules, $validation_error_messages);
        if($validator->fails()) {
            return redirect()->back()
                             ->withInput()
                             ->withErrors($validator);
        }
        // TODO: fare la validazione di tutti i form così, invece di validare manualmente come faccio ora
        $subscription = new Subscription;
        $subscription->set_user_id($user_found_on_db_id);
        $subscription->set_berth_id($berth_found_on_db_id);
        $subscription->set_transponder_id($transponder_id);
        $subscription->set_begin_timestamp($subscription_start_datetime);
        $subscription->set_notes($request->input('notes'));
        $subscription->save();
        return redirect()->route('subscription_list')
                         ->with('flash_message', ucfirst(__('subscriptions/index.subscription_successfully_created_msg')))
                         ->with('flash_background_color', 'green');
    }

    public function open_alarm_thresholds_page(Request $request, $subscription_id) {
        $subscription = Subscription::find($subscription_id);
        $subscription_user = $subscription->user;
        if(!($subscription) || ($subscription_user->count() === 0) || (Auth::id() !== $subscription_user->get_id())) {
        // se la subscription per la quale si stanno cercando di settare le soglie non esiste sul db, oppure non appartiene all'utente loggato:
            // redireziono l'utente alla route my_subscriptions:
            return redirect()->route('my_subscriptions');
        }
        $alarm_thresholds = $subscription->get_thresholds_record();
        return View::make('subscriptions.set_alarm_thresholds', ['subscription' => $subscription,
                                                                 'alarm_thresholds' => $alarm_thresholds]);
    }

    public function write_alarm_thresholds(Request $request, $subscription_id) {
        // recupero la subscription per la quale devo modificare le soglie:
        $subscription = Subscription::find($subscription_id);
        $subscription_user = $subscription->user;
        if(!($subscription) || ($subscription_user->count() === 0) || (Auth::id() !== $subscription_user->get_id())) {
        // se la subscription non esiste sul db oppure non appartiene all'utente loggato:
            // redireziono l'utente alla route my_subscriptions:
            return redirect()->route('my_subscriptions');
        }
        // recupero le soglie submittate dal form:
        $min_power_threshold_from_request = floatval($request->input('min_power_consumption_threshold'));
        $max_power_threshold_from_request = floatval($request->input('max_power_consumption_threshold'));
        if((($subscription_user) && ($subscription_user->count() > 0)) &&
           ($subscription_user->wants_power_alarm_thresholds_in_ampere())) {
        // se la preferenza dell'utente per la visualizzazione delle soglie di consumo energia è settata sugli Ah
            // devo convertire i valori delle soglie in kWh prima di scriverli sul db:
            // TODO: meglio parametrizzare il valore "230"? Anche perchè lo uso anche sulla view set_alarm_thresholds se devo convertire le soglie in Ah prima di visualizzarle. Inoltre: la conversione fatta così vale per tutti i tipi di presa? O per prese più grosse o più piccole di un tot devo convertire diversamente?
            $min_power_threshold_from_request = $min_power_threshold_from_request * 230;
            $max_power_threshold_from_request = $max_power_threshold_from_request * 230;
        }
        $max_water_time_threshold_from_request = intval($request->input('max_water_time_threshold'));
        // siccome non è detto che la soglia di credito minimo sia stata passata nel form, dato che per le subscription postpagate non esiste:
        if(!is_null($request->input('min_credit_threshold'))) {
        // se è stata passata nel form:
            // la recupero dalla request e la passo direttamente al metodo del model che aggiorna le soglie:
            $min_credit_threshold_from_request = floatval($request->input('min_credit_threshold'));
            $thresholds_have_been_changed = $subscription->set_alarm_thresholds_and_save_thresholds_record($min_power_threshold_from_request, $max_power_threshold_from_request, $max_water_time_threshold_from_request, $min_credit_threshold_from_request);
        } else {
        // se la soglia di credito minimo non è stata passata nel form:
            // aggiorno il record soglie passando come soglia di credito minimo quella settata precedentementente sul db:
            $alarm_thresholds_from_db = $subscription->get_thresholds_record();
            $min_credit_threshold_from_db = floatval($alarm_thresholds_from_db->min_credit_threshold);
            $thresholds_have_been_changed = $subscription->set_alarm_thresholds_and_save_thresholds_record($min_power_threshold_from_request, $max_power_threshold_from_request, $max_water_time_threshold_from_request, $min_credit_threshold_from_db);
        }
        // (il metodo set_alarm_thresholds_and_save_thresholds_record() verifica già da solo se le soglie sono cambiate, prima di scriverle), e restituisce true se c'è stata effettivamente una modifica rispetto alle soglie precedenti
        if($thresholds_have_been_changed) {
        // se le soglie sono cambiate rispetto a quelle precedenti:
            // redireziono a my_subscriptions informando l'utente che le ho modificate:
            return redirect()->route('my_subscriptions')
                             ->with('flash_message', ucfirst(__('users/my_subscriptions.alarm_thresholds_have_been_updated_flash_message')))
                             ->with('flash_background_color', 'green');
        } else {
        // se l'utente ha submittato il form senza fare modifiche:
            // redireziono a my_subscriptions senza passare nessun messaggio:
            return redirect()->route('my_subscriptions');
        }
    }

    public function get_consumption_chart(int $subscription_id, int $berth_id, Carbon|int|string $begin_timestamp, Carbon|int|string $end_timestamp, string $subject, string $sampling_interval_argument) {
    	// TODO: è necessario l'union type su $begin_timestamp e $end_timestamp? Forse posso specificare uno solo dei tre tipi, e ci pensa Laravel a castare
        // TODO: avrebbe senso anche definire un limite superiore per l'ampiezza del grafico, oltre il quale non dovrebbe essere possibile andare (anche un limite inferiore?). Aggiungere un po' di validazione all'inizio del metodo
        if(!($begin_timestamp instanceof Carbon)) {
            $begin_timestamp = Carbon::createFromTimestampUTC($begin_timestamp);
        }
        if(!($end_timestamp instanceof Carbon)) {
            $end_timestamp = Carbon::createFromTimestampUTC($end_timestamp);
        }
        $chart = [];
        $subscription = Subscription::find($subscription_id);
        if(!$subscription) {
        // se il contratto richiesto non esiste, redireziono alla pagina di provenienza
        // TODO: implementare il redirect con un middleware? E fare la stessa cosa con gli altri redirect dello stesso tipo?
        // TODO: redirectare con un messaggio che spiega che non si è autorizzati a vederlo? E fare la stessa cosa con gli altri redirect dello stesso tipo?
            $previous_page = array_slice(explode('/', url()->previous()), -1)[0];
            // metto uno switch per poter mettere un default: se ad esempio l'utente avesse refreshato la pagina del grafico, o l'avesse aperta inserendo manualmente l'url, $previous_page non corrisponderebbe alla pagina dalla quale è partita la richiesta. In questo modo inoltre, posso sempre aggiungere più opzioni quando servono
            switch($previous_page) {
                case 'plant_status':
                default:
                    $page_to_redirect = 'plant_status';
                    break;
                case 'consumption_reports':
                    $page_to_redirect = 'consumption_reports';
                    break;
                case 'my_subscriptions':
                    $page_to_redirect = 'my_subscriptions';
            }
            return redirect($page_to_redirect)->with('flash_message', ucfirst(__('users/my_subscriptions.chart_for_an_unknown_subscription_requested_msg')))
                                              ->with('flash_background_color', 'yellow');
        }
        // TODO: devo aggiungere un redirect anche nel caso in cui il posto barca richiesto non esista (forse, se lo faccio, posso togliere anche la parte di codice più in basso che mostra [undefined] se il posto barca non esiste)
        switch($sampling_interval_argument) {
            case ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR:
                $sampling_interval_carbon = CarbonInterval::make('1hour');
                $date_time_format_for_chart_title = config('dates_currency_and_locale.local_datetime_format_short_carbon');
                break;
            case ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY:
                $sampling_interval_carbon = CarbonInterval::make('24hours');
                $date_time_format_for_chart_title = config('dates_currency_and_locale.local_date_format_short_carbon');
        }
        $chart_title_period_caption = strtolower(__('consumption_chart.title_period_from_to', ['begin' => $begin_timestamp->tz(config('dates_currency_and_locale.local_timezone'))
                                                                                                                          ->format($date_time_format_for_chart_title),
                                                                                               'end' => $end_timestamp->tz(config('dates_currency_and_locale.local_timezone'))
                                                                                                                      ->format($date_time_format_for_chart_title) ]));
        $subscription_user =  $subscription->user;
        if(($subscription_user) && ($subscription_user->count() > 0)) {
            $subscription_user_fullname = $subscription_user->get_full_name();
        } else {
            $subscription_user_fullname = strtolower(__('common.unknown_user'));
        }
        $chart_total_timespan_in_hours = $end_timestamp->floatDiffInHours($begin_timestamp, true);
        switch($subject) {
            case ConsumptionReportsController::REPORT_VALUE_POWER:
                $chart_data_color = ConsumptionReportsController::GOOGLE_CHARTS_POWER_CONSUMPTION_DATA_COLOR;
                $consumption_report_mode = ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME;
                $y_axis_caption =  ucfirst(__('consumption_chart.y_axis_average_power_consumption_caption'));
                $consumption_type_chart_title_caption = __('consumption_chart.title_consumption_type_power_caption');
                break;
            case ConsumptionReportsController::REPORT_VALUE_WATER:
                $chart_data_color = ConsumptionReportsController::GOOGLE_CHARTS_WATER_CONSUMPTION_DATA_COLOR;
                $consumption_report_mode = ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME;
                $y_axis_caption =  ucfirst(__('consumption_chart.y_axis_average_water_consumption_caption'));
                $consumption_type_chart_title_caption = __('consumption_chart.title_consumption_type_water_caption');
                break;
            case ConsumptionReportsController::REPORT_VALUE_CREDIT:
                $chart_data_color = ConsumptionReportsController::GOOGLE_CHARTS_CREDIT_CONSUMPTION_DATA_COLOR;
                $consumption_report_mode = ConsumptionReportsController::REPORT_MODE_PLAIN;
                $y_axis_caption =  ucfirst(__('consumption_chart.y_axis_average_credit_consumption_caption', ['currency' => config('dates_currency_and_locale.currency_character')]));
                $consumption_type_chart_title_caption = __('consumption_chart.title_consumption_type_residual_credit_consumption_caption');
        }
        // inizializzo il bool che mi dirà se questo contratto ha effettuato consumi nell'intervallo richiesto. Se non l'ha fatto, questo metodo restituirà un array dei consumi vuoto. Di conseguenza la view chart informerà l'utente che non esistono dati di consumo relativamente al contratto ed al periodo richiesti:
        $there_are_snapshots_for_this_subscription_in_requested_interval = true;
        // ricavo i timestamp di inizio e di fine del periodo da graficare (la subscription potrebbe aver avuto inizio prima dell'inizio del periodo richiesto, oppure essere terminata prima della sua fine):
        $subscription_begin_dt = $subscription->get_begin_timestamp();
        if($subscription_begin_dt > $begin_timestamp) {
            $begin_timestamp = $subscription_begin_dt;
        }
        $subscription_end_dt = $subscription->get_end_timestamp();
        if((is_null($subscription_end_dt)) || ($subscription_end_dt >= Carbon::now(config('app.timezone')))) {
        // se la subscription è ancora in corso o se la sua fine è programmata per un istante futuro:
            // posso mantenere l'end_timestamp che mi è stato passato come parametro (quindi qui non faccio nulla)
        } else {
        // se il termine della subscription è prima di now(), accorcio il periodo del grafico facendolo arrivare fino al momento del termine:
            $end_timestamp = $subscription->get_end_timestamp();
        }
        $berth_name = "[undefined]";        // TODO: è il valore di default nel caso in cui non esista il posto barca o la subscription non abbia ancora consumato nell'impianto. E' temporaneo, devo usare una translation
        $berth = Berth::find($berth_id);
        if(($berth) && ($berth->count() > 0)) {
            $berth_name = $berth->get_description();
        }
        $chart_title_subscription_description_full = lcfirst(__('consumption_chart.title_subscription_description_with_berth_caption', ['user_name' => $subscription_user_fullname,
                                                                                                                                        'berth_name' => $berth_name]));
        $chart_title = ucfirst(__('consumption_chart.title_subscription_consumption_caption', ['period' => $chart_title_period_caption,
                                                                                               'consumption_type' => $consumption_type_chart_title_caption,
                                                                                               'subscription_description' => $chart_title_subscription_description_full]));
        $x_axis_caption = ucfirst(__('consumption_chart.x_axis_time_interval_caption'));
        $chart_type = ConsumptionReportsController::GOOGLE_CHARTS_TYPE_COLUMN;
        $there_were_disconnections_during_interval = false;
        if($there_are_snapshots_for_this_subscription_in_requested_interval) {
            $there_were_disconnections_during_interval = $berth->had_disconnections_during_interval($begin_timestamp, $end_timestamp);
            $index_dt_low = $begin_timestamp->copy();
            $index_dt_high = $begin_timestamp->copy()->add($sampling_interval_carbon);
            $previous_epower_id_has_changed_during_interval_value = $epower_id_has_changed_during_interval = false;
            // TODO: dato che sono tornato a vincolare tutti i grafici subscription ad un singolo posto barca, questo grafico deve tornare ad essere un LineChart o lo lascio ColumnChart?
            while($index_dt_high <= $end_timestamp) {
                $epower_id_has_changed_during_interval = false;
                if($sampling_interval_argument === ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR) {
                // se sto campionando i dati ogni ora:
                    // le captions sull'asse x saranno nel formato "hh:mm - hh:mm" (o equivalente, a seconda del formato regionale settato per data e ora nella configurazione):
                    $array_key_hour_low_part = $index_dt_low->copy()
                                                            ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                            ->format(config('dates_currency_and_locale.local_time_format_short_carbon'));
                    $array_key_hour_high_part = $index_dt_high->copy()
                                                              ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                              ->format(config('dates_currency_and_locale.local_time_format_short_carbon'));
                    $array_key = '';
                    if($chart_total_timespan_in_hours > 24) {
                    // ma se l'intervallo totale coperto dal grafico supera le 24 ore:
                        // aggiungo la data alle captions dell'asse x, nel formato "dd/mm" (o equivalente, a seconda del formato regionale settato per data e ora nella configurazione):
                        $array_key_day_part = $index_dt_low->copy()
                                                           ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                           ->format(config('dates_currency_and_locale.local_date_format_short_carbon')).' ';
                        $array_key .= $array_key_day_part;
                    }
                    $array_key .= $array_key_hour_low_part.' - '.$array_key_hour_high_part;
                } else {
                // se sto campionando i dati ogni giorno:
                    // le captions sull'asse x saranno nel formato "dd/mm - dd/mm" (o equivalente, a seconda del formato regionale settato per data e ora nella configurazione):
                    $array_key_day_low_part = $index_dt_low->copy()
                                                           ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                           ->format(config('dates_currency_and_locale.local_date_format_short_carbon'));
                    $array_key_day_high_part = $index_dt_high->copy()
                                                             ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                             ->format(config('dates_currency_and_locale.local_date_format_short_carbon'));
                    $array_key = $array_key_day_low_part.' - '.$array_key_day_high_part;
                }
                
                // calcolo il consumo effettuato sul posto barca $berth nell'intervallo compreso tra index_dt_low e index_dt_high:
                $chart[$array_key] = $subscription->get_consumption_on_berth_during_interval($index_dt_low, $index_dt_high, $subject, $berth->get_id(), $epower_id_has_changed_during_interval, $consumption_report_mode);
                // (fino alla versione 2.0.17a, se la subscription non era legata ad un posto barca, usavo $subscription->get_consumption_on_all_plant_during_interval() per calcolare il consumo effettuato nell'intero impianto durante il periodo compreso tra index_dt_low e index_dt_high, ma poi con Raffaele abbiamo concordato che anche le TransponderSubscriptions vengono abitualmente utilizzate su un solo posto barca, e che in caso contrario, qui vanno mostrati solo i consumi effettuati sul posto barca sul quale si sta consumando attualmente)
                $index_dt_low = $index_dt_high->copy();
                $index_dt_high->add($sampling_interval_carbon);
                $epower_id_has_changed_during_interval = $epower_id_has_changed_during_interval || $previous_epower_id_has_changed_during_interval_value;
                $previous_epower_id_has_changed_during_interval_value = $epower_id_has_changed_during_interval;
            }
        }
        return view('chart', ['chart_title' => $chart_title,
                              'chart_type' => $chart_type,
                              'chart_period' => $chart_title_period_caption,
                              'chart_subject' => $subject,
                              'x_axis_caption' => $x_axis_caption,
                              'y_axis_caption' => $y_axis_caption,
                              'chart_data_color' => $chart_data_color,
                              'chart' => $chart,
                              'there_were_disconnections_during_interval' => $there_were_disconnections_during_interval,
                              'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval]);
    }

    public function get_consumption_report_file(int $subscription_id, int $berth_id, int $begin_epoch_timestamp, int $end_epoch_timestamp, string $file_type) {
    // TODO: fare distinzione tra i metodi di calculate_consumption() del model Subscription ed i metodi di get_consumption() che si occupano solo di fare la differenza tra i valori della tabella subscription_snapshot?
    // TODO: devo limitare la visualizzazione dei consumi ad un singolo posto barca (quindi 1. devo prevedere un parametro in più che è il posto barca sul quale verificarli, oppure cercarlo direttamente da questo metodo (EDIT: TODO: per le transponder_subscriptions, devo ancora richiederlo dal form, dato che deve essere possibile chiedere esplicitamente un grafico/report per i consumi effettuati su un determinato posto barca), 2. per il calcolo dei consumi devo usare il metodo Subscription::get_consumption_on_berth_during_interval(), 3. sulla pagina di destinazione devo mostrare sempre il posto barca al quale i consumi visualizzati sono riferiti, e rendere chiaro il fatto che questi consumi si riferiscono solo a quel posto barca)
        $need_to_redirect_back = false;
        $flash_message_for_redirect = null;
        $berth = Berth::find($berth_id);
        if((!$berth) || (!$berth->count() > 0)) {
        // se il posto barca richiesto non esiste, redireziono alla pagina di provenienza con un flash_message di errore:
            $need_to_redirect_back = true;
            $flash_message_for_redirect = ucfirst(__('consumption_reports.report_for_an_unknown_berth_requested_msg'));
        }
        $subscription = Subscription::find($subscription_id);
        if((!$subscription) || (!$subscription->count() > 0)) {
        // se il contratto richiesto non esiste, redireziono alla pagina di provenienza con un flash_message di errore:
            $need_to_redirect_back = true;
            $flash_message_for_redirect = ucfirst(__('consumption_reports.report_for_an_unknown_subscription_requested_msg'));
        }
        // valido i timestamp passati:
        // (mantengo specificata la timezone perchè altrimenti credo che Laravel se li aspetti in timezone locale)
        $min_dt = Carbon::createFromFormat('Y-m-d H:i:s', config('dates_currency_and_locale.min_utc_timestamp'), new \DateTimeZone(config('app.timezone')));
        $max_dt = Carbon::createFromFormat('Y-m-d H:i:s', config('dates_currency_and_locale.max_utc_timestamp'), new \DateTimeZone(config('app.timezone')));
        $begin_dt = Carbon::parse($begin_epoch_timestamp, new \DateTimeZone(config('app.timezone')));
        $end_dt = Carbon::parse($end_epoch_timestamp, new \DateTimeZone(config('app.timezone')));
        if(($begin_dt < $min_dt) || ($end_dt > $max_dt) || ($end_dt > $max_dt)) {
        // se i timestamp passati non passano la validazione, redireziono alla pagina di provenienza con un flash_message di errore:
            $need_to_redirect_back = true;
            $flash_message_for_redirect = ucfirst(__('consumption_reports.report_for_a_malformed_period_requested_msg'));
        }
        if($need_to_redirect_back) {
            $previous_page = array_slice(explode('/', url()->previous()), -1)[0];       // TODO: forse posso usare redirect()->back()
            return redirect($previous_page)->with('flash_message', $flash_message_for_redirect)
                                           ->with('flash_background_color', 'yellow');
        }
        GenerateConsumptionReport::dispatch(Auth::user(),
                                            ConsumptionReportsController::SUBJECT_TYPE_SUBSCRIPTION,
                                            [$subscription_id, $berth_id],
                                            $file_type,
                                            $begin_dt,
                                            $end_dt)
                                 ->onQueue('reports');
        $flash_message = ucfirst(__('consumption_reports.your_request_has_been_received_you_will_get_a_notification_when_its_available'));
        $flash_background_color = 'green';
        return redirect('consumption_reports')->with('flash_message', $flash_message)
                                              ->with('flash_background_color', $flash_background_color);
    }

    public function check_consumption(Request $request, $subscription_id) {
    // metodo che apre la pagina di verifica del consumo della subscription.
    // TODO: al momento (21.04.2020), viene richiamato solo dalle views subscriptions/index e berths/index (quindi solo per le socket_subscriptions), ma andrà richiamato probabilmente anche dalla view my_subscriptions per la verifica del consumo di una subscription prepagata. Nel caso, per le transponder_subscriptions, dovrò richiedere esplicitamente il posto barca dal form che ha come destinatario questa pagina, dato che deve essere possibile chiedere un grafico/report per i consumi effettuati su un determinato posto barca)
        $request->merge([
            'subscription_id' => $subscription_id,
        ]);
        // TODO: dovrò verificare che l'utente richiedente sia un admin o l'intestatario della subscription, prima di farlo accedere alla route. Metto la logica di verifica direttamente qui?
        $validation_rules = [
            'subscription_id' => 'required|exists:subscriptions,id',
        ];
        $validation_error_messages = [
            'subscription_id.required' => ucfirst(__('validation.required')),
            'subscription_id.exists' => ucfirst(__('validation.exists')),
        ];
        $validator = Validator::make($request->all(), $validation_rules, $validation_error_messages);
        if($validator->fails()) {
            return redirect()->back()
                             ->withInput()
                             ->withErrors($validator);
            // TODO: i messaggi di mancata validazione non si vedono sulla view di destinazione. Manca l'include dei flash messages o il problema è un altro?
        }
        $subscription = Subscription::find($subscription_id);
        $begin_dt = $subscription->get_begin_timestamp();
        if($subscription->is_ongoing()) {
        // se la subscription è ancora in corso:
            // i consumi andranno calcolati fino all'istante attuale:
            $end_dt = Carbon::now(config('app.timezone'));
        } else {
        // se la subscription è terminata:
            // i consumi andranno calcolati fino al suo istante di fine:
            $end_dt = $subscription->get_end_timestamp();
        }
        $epower_id_has_changed_during_interval = false;
        // calcolo i consumi nell'intervallo tra $begin_dt e $end_dt:
        if($subscription->is_prepaid() || (!$subscription->is_bound_to_socket())) {
        // se il contratto è prepagato, oppure è un postpagato su transponder (quindi non vincolato al posto barca): 
            // TODO: verificare se questo calcolo è corretto in entrambi i casi (sia che il contratto sia prepagato, sia che sia un postpagato su transponder, quindi non vincolato al posto barca):
            $subscription_power_consumption = $subscription->get_consumption_on_all_plant_during_interval($begin_dt, $end_dt, ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
            $subscription_water_consumption = $subscription->get_consumption_on_all_plant_during_interval($begin_dt, $end_dt, ConsumptionReportsController::REPORT_VALUE_WATER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
        } else {
        // se il contratto è una socket_subscription:
            $subscription_power_consumption = $subscription->get_consumption_on_berth_during_interval($begin_dt, $end_dt, ConsumptionReportsController::REPORT_VALUE_POWER, $subscription->berth->get_id(), $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
            $subscription_water_consumption = $subscription->get_consumption_on_berth_during_interval($begin_dt, $end_dt, ConsumptionReportsController::REPORT_VALUE_WATER, $subscription->berth->get_id(), $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
            // TODO: dubbio: il metodo get_consumption_on_berth_during_interval() sa già che deve prendere i contatori di consumo postpagato? In teoria sì: il consumo viene calcolato attraverso i subscription_snapshots, i quali vengono generati distinguendo le subscriptions prepagate da quelle postpagate. Ma devo fare un'ulteriore distinzione tra socket_subscriptions e subscriptions postpagate non vincolate ad un posto barca? Per ora il problema non si pone, perchè la view subscriptions/consumption viene usata solo per quel genere di contratto. Però bisognerà tenerne conto nel momento in cui dovrò usarla anche per gli altri contratti.
        }
        // il 10.06.2020, concordato con raffaele che - se mi capita di calcolare un valore negativo per i consumi - è meglio troncarlo a zero che non visualizzarlo come negativo, dato che un consumo negativo non ha senso:
        if($subscription_power_consumption < 0) {
            $subscription_power_consumption = 0;
        }
        if($subscription_water_consumption < 0) {
            $subscription_water_consumption = 0;
        }
        return View::make('subscriptions.consumption', ['subscription' => $subscription,
                                                        'subscription_power_consumption' => $subscription_power_consumption,
                                                        'subscription_water_consumption' => $subscription_water_consumption,
                                                        'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval]);
    }

    public function update_notes(Request $request, $subscription_id) {
        $subscription = Subscription::find($subscription_id);
        $subscription->set_notes($request->input('subscription_notes'));
        $subscription->save();
        return redirect()->route('subscription_list')
                         ->with('flash_message', ucfirst(__('subscriptions/index.subscription_successfully_updated_msg')))
                         ->with('flash_background_color', 'green');
    }

    public function close($subscription_id) {
        $subscription = Subscription::find($subscription_id);
        $subscription->end_on_timestamp_and_save(Carbon::now(config('app.timezone')));
        return redirect()->route('subscription_list')
                         ->with('flash_message', ucfirst(__('subscriptions/index.subscription_successfully_closed_msg')))
                         ->with('flash_background_color', 'green');
    }
}
