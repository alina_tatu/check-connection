<?php

namespace App\Http\Controllers;

use App\Plant;
use App\Berth;
use App\Action;
use App\User;
use App\SocketSnapshot;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Carbon\CarbonInterval;
use App\Jobs\GenerateConsumptionReport;


class BerthController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $berths = Plant::get_all_berths();
        return View::make('berths.index', ['berths' => $berths]);
    }

    public function schedule_action_on_socket_ajax(int $socket_id) {
        // TODO: manca la validazione
        // TODO: verificare che l'utente richiedente abbia il diritto di eseguire l'azione sulla presa, prima di schedulare l'azione: un admin può resettare o spegnere tutte le prese, l'attuale utilizzatore di una presa può spegnerla ma non resettarla (all'atto pratico l'azione è la stessa, ma a livello di UI sembrano due azioni diverse). Al momento nessuno può spegnere le prese, ma quando Sergio implementerà l'accensione su SmartView, andrà permesso agli admin e all'attuale utilizzatore
        $action_record = new Action();
        $action_record->set_object_type(Action::OBJECT_TYPE_SOCKET);
        $action_record->set_object_id(intval($socket_id));
        if(str_contains(url()->current(), 'switch_off')) {
            $action_type = Action::ACTION_TYPE_TURN_OFF_SOCKET;
        } elseif(str_contains(url()->current(), 'reset')) {
            $action_type = Action::ACTION_TYPE_RESET_SOCKET;
        } /* elseif(str_contains(url()->current(), 'turn_on')) {
            // TODO: questa non è abilitata per ora: non devo poter accendere una presa da remoto.
            // TODO: fare in modo che vada in errore (e che restituisca uno stato + messaggio di errore)
        } */ else {
            return response()->json(['status' => 'Error: bad request'], 400);
        }
        $action_record->set_type($action_type);
        $action_record->set_request_timestamp(Carbon::now(config('app.timezone')));
        $user = User::where('token_for_actions_on_sockets', request()->input('actionsToken'))
                    ->first();
        // TODO: aggiungere un indice sul campo token per velocizzare le cose?
        if((!$user) || ($user->count() === 0)) {
            return response()->json(['status' => 'Error: operation not allowed'], 403);
        }
        $action_record->set_requesting_user_id($user->get_id());
        $action_record->save();
        return response()->json(['status' => 'ok'], 202);
    }

    public function get_consumption_chart(int $berth_id, Carbon|int|string $begin_timestamp, Carbon|int|string $end_timestamp, string $subject, string $sampling_interval_argument) {
    	// TODO: è necessario l'union type su $begin_timestamp e $end_timestamp? Forse posso specificare uno solo dei tre tipi, e ci pensa Laravel a castare
        // TODO: avrebbe senso anche definire un limite superiore per l'ampiezza del grafico, oltre il quale non dovrebbe essere possibile andare (anche un limite inferiore?). Aggiungere un po' di validazione all'inizio del metodo
        if(!($begin_timestamp instanceof Carbon)) {
            $begin_timestamp = Carbon::createFromTimestampUTC($begin_timestamp);
        }
        if(!($end_timestamp instanceof Carbon)) {
            $end_timestamp = Carbon::createFromTimestampUTC($end_timestamp);
        }
        if(!$berth = Berth::find($berth_id)) {
            // se il posto barca richiesto non esiste, redireziono alla pagina di provenienza
            $previous_page = array_slice(explode('/', url()->previous()), -1)[0];
            // metto uno switch per poter mettere un default: se ad esempio l'utente avesse refreshato la pagina del grafico, o l'avesse aperta inserendo manualmente l'url, $previous_page non corrisponderebbe alla pagina dalla quale è partita la richiesta. In questo modo inoltre, posso sempre aggiungere più opzioni quando servono
            switch($previous_page) {
                case 'plant_status':
                default:
                    $page_to_redirect = 'plant_status';
                    break;
                case 'consumption_reports':
                    $page_to_redirect = 'consumption_reports';
                    break;
                case 'plant_map':
                    $page_to_redirect = 'plant_map';            // ancora non è possibile richiedere grafici dalla mappa, ma nel caso...
            }
            return redirect($page_to_redirect)->with('flash_message', ucfirst(__('consumption_chart.chart_for_an_unknown_berth_requested_msg')))
                                              ->with('flash_background_color', 'yellow');
        }
        switch($sampling_interval_argument) {
            case ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR:
                $sampling_interval_carbon = CarbonInterval::make('1hour');
                $date_time_format_for_chart_title = config('dates_currency_and_locale.local_datetime_format_short_carbon');
                break;
            case ConsumptionReportsController::CHART_SAMPLING_INTERVAL_DAY:
                $sampling_interval_carbon = CarbonInterval::make('24hours');
                $date_time_format_for_chart_title = config('dates_currency_and_locale.local_date_format_short_carbon');
        }
        $chart_title_period_caption = strtolower(__('consumption_chart.title_period_from_to', ['begin' => $begin_timestamp->tz(config('dates_currency_and_locale.local_timezone'))
                                                                                                                          ->format($date_time_format_for_chart_title),
                                                                                               'end' => $end_timestamp->tz(config('dates_currency_and_locale.local_timezone'))
                                                                                                                      ->format($date_time_format_for_chart_title) ]));
        switch($subject) {
            case ConsumptionReportsController::REPORT_VALUE_POWER:
                $chart_type = ConsumptionReportsController::GOOGLE_CHARTS_TYPE_COLUMN;
                $chart_data_color = ConsumptionReportsController::GOOGLE_CHARTS_POWER_CONSUMPTION_DATA_COLOR;
                $consumption_report_mode = ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME;
                $y_axis_caption =  ucfirst(__('consumption_chart.y_axis_average_power_consumption_caption'));
                $consumption_type_chart_title_caption = __('consumption_chart.title_consumption_type_power_caption');
                break;
            case ConsumptionReportsController::REPORT_VALUE_WATER:
                $chart_type = ConsumptionReportsController::GOOGLE_CHARTS_TYPE_COLUMN;
                $chart_data_color = ConsumptionReportsController::GOOGLE_CHARTS_WATER_CONSUMPTION_DATA_COLOR;
                $consumption_report_mode = ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME;
                $y_axis_caption =  ucfirst(__('consumption_chart.y_axis_average_water_consumption_caption'));
                $consumption_type_chart_title_caption = __('consumption_chart.title_consumption_type_water_caption');
                break;
            case ConsumptionReportsController::REPORT_VALUE_CREDIT:
                $chart_type = ConsumptionReportsController::GOOGLE_CHARTS_TYPE_LINE;
                // per i grafici credito dei posti barca, devo tornare indietro di 1h/1gg in più rispetto a quanto faccio per i grafici energia/acqua:
                $chart_data_color = ConsumptionReportsController::GOOGLE_CHARTS_CREDIT_CONSUMPTION_DATA_COLOR;
                $consumption_report_mode = ConsumptionReportsController::REPORT_MODE_PLAIN;
                $y_axis_caption =  ucfirst(__('consumption_chart.y_axis_residual_credit_on_socket_caption', ['currency' => config('dates_currency_and_locale.currency_character')]));
                $consumption_type_chart_title_caption = __('consumption_chart.title_consumption_type_residual_credit_trend_caption');
                break;
        }
        
        $chart_title = ucfirst(__('consumption_chart.title_berth_consumption_caption', ['period' => $chart_title_period_caption,
                                                                                        'consumption_type' => $consumption_type_chart_title_caption,
                                                                                        'berth_name' => $berth->get_description()]));
        $x_axis_caption = ucfirst(__('consumption_chart.x_axis_time_interval_caption'));
        $chart = [];
        $previous_epower_id_has_changed_during_interval_value = $epower_id_has_changed_during_interval = false;
        $there_were_disconnections_during_interval = $berth->had_disconnections_during_interval($begin_timestamp, $end_timestamp);        
        $chart_total_timespan_in_hours = $end_timestamp->floatDiffInHours($begin_timestamp, true);
        switch ($subject) {
            case ConsumptionReportsController::REPORT_VALUE_POWER:
            case ConsumptionReportsController::REPORT_VALUE_WATER:
            default:
                // se sto graficando il consumo di energia o acqua:
                $index_timestamp_low = $begin_timestamp->clone();
                $index_timestamp_high = $begin_timestamp->clone()
                                                        ->add($sampling_interval_carbon);
                while($index_timestamp_low < $end_timestamp) {
                    $epower_id_has_changed_during_interval = false;
                    if($sampling_interval_argument === ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR) {
                    // se sto campionando i dati ogni ora:
                        // le captions sull'asse x saranno nel formato "hh:mm - hh:mm" (o equivalente, a seconda del formato regionale settato per data e ora nella configurazione):
                        $array_key_hour_low_part = $index_timestamp_low->copy()
                                                                       ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                       ->format(config('dates_currency_and_locale.local_time_format_short_carbon'));
                        $array_key_hour_high_part = $index_timestamp_high->copy()
                                                                         ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                         ->format(config('dates_currency_and_locale.local_time_format_short_carbon'));
                        $array_key = '';
                        if($chart_total_timespan_in_hours > 24) {
                        // ma se l'intervallo totale coperto dal grafico supera le 24 ore:
                            // aggiungo la data alle captions dell'asse x, nel formato "dd/mm" (o equivalente, a seconda del formato regionale settato per data e ora nella configurazione):
                            $array_key_day_part = $index_timestamp_low->copy()
                                                                      ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                      ->format(config('dates_currency_and_locale.local_date_format_short_carbon')).' ';
                            $array_key .= $array_key_day_part;
                        }
                        $array_key .= $array_key_hour_low_part.' - '.$array_key_hour_high_part;
                    } else {
                    // se sto campionando i dati ogni giorno:
                        // le captions sull'asse x saranno nel formato "dd/mm - dd/mm" (o equivalente, a seconda del formato regionale settato per data e ora nella configurazione):
                        $array_key_day_low_part = $index_timestamp_low->copy()
                                                                      ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                      ->format(config('dates_currency_and_locale.local_date_format_short_carbon'));
                        $array_key_day_high_part = $index_timestamp_high->copy()
                                                                        ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                        ->format(config('dates_currency_and_locale.local_date_format_short_carbon'));
                        $array_key = $array_key_day_low_part.' - '.$array_key_day_high_part;
                    }
                    $chart[$array_key] = $berth->get_consumption_in_interval($index_timestamp_low, $index_timestamp_high, $subject, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_TOTAL, $consumption_report_mode);
                    $index_timestamp_low = $index_timestamp_high->copy();
                    $index_timestamp_high->add($sampling_interval_carbon);
                    $epower_id_has_changed_during_interval = $epower_id_has_changed_during_interval || $previous_epower_id_has_changed_during_interval_value;
                    $previous_epower_id_has_changed_during_interval_value = $epower_id_has_changed_during_interval;
                }
                break;
            case ConsumptionReportsController::REPORT_VALUE_CREDIT:
                // se sto graficando il consumo del credito:
                $index_dt = $begin_timestamp->clone();
                do {
                    if($sampling_interval_argument === ConsumptionReportsController::CHART_SAMPLING_INTERVAL_HOUR) {
                    // se sto campionando i dati ogni ora:
                        // le captions sull'asse x saranno nel formato "dd/mm hh:mm" (o equivalente, a seconda del formato regionale settato per data e ora nella configurazione):
                        $array_key = $index_dt->copy()
                                              ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                              ->format(config('dates_currency_and_locale.local_datetime_format_short_carbon'));
                    } else {
                    // se sto campionando i dati ogni giorno:
                        // le captions sull'asse x saranno nel formato "dd/mm" (o equivalente, a seconda del formato regionale settato per data e ora nella configurazione):
                        $array_key = $index_dt->copy()
                                              ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                              ->format(config('dates_currency_and_locale.local_date_format_short_carbon'));
                    }
                    $snapshot_at_index_dt = $berth->get_last_linking_status_before_timestamp($index_dt, false);
                    if($snapshot_at_index_dt) {
                        $chart[$array_key] = $snapshot_at_index_dt->get_residual_credit();
                    }
                    $index_dt->add($sampling_interval_carbon);
                } while($index_dt <= $end_timestamp);
        }
        return view('chart', ['chart_title' => $chart_title,
                              'chart_type' => $chart_type,
                              'chart_period' => $sampling_interval_argument,
                              'chart_subject' => $subject,
                              'x_axis_caption' => $x_axis_caption,
                              'y_axis_caption' => $y_axis_caption,
                              'chart_data_color' => $chart_data_color,
                              'chart' => $chart,
                              'there_were_disconnections_during_interval' => $there_were_disconnections_during_interval,
                              'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval]);
    }

    public function get_consumption_report_file(int $berth_id, int $begin_epoch_timestamp, int $end_epoch_timestamp, string $file_type) {
        $need_to_redirect_back = false;
        $flash_message_for_redirect = null;
        if(!($berth = Berth::find($berth_id))) {
        // se il posto barca richiesto non esiste, redireziono alla pagina di provenienza:
            $need_to_redirect_back = true;
            $flash_message_for_redirect = ucfirst(__('consumption_reports.report_for_an_unknown_berth_requested_msg'));
        }
        // valido i timestamp passati:
        // (specifico la timezone perchè altrimenti forse laravel se li aspetta nella timezone locale)
        $min_dt = Carbon::createFromFormat('Y-m-d H:i:s', config('dates_currency_and_locale.min_utc_timestamp'), new \DateTimeZone(config('app.timezone')));
        $max_dt = Carbon::createFromFormat('Y-m-d H:i:s', config('dates_currency_and_locale.max_utc_timestamp'), new \DateTimeZone(config('app.timezone')));
        $begin_dt = Carbon::parse($begin_epoch_timestamp, new \DateTimeZone(config('app.timezone')));
        $end_dt = Carbon::parse($end_epoch_timestamp, new \DateTimeZone(config('app.timezone')));
        if(($begin_dt < $min_dt) || ($end_dt > $max_dt) || ($end_dt > $max_dt)) {
        // se i timestamp passati non passano la validazione, redireziono alla pagina di provenienza con un flash_message di errore:
            $need_to_redirect_back = true;
            $flash_message_for_redirect = ucfirst(__('consumption_reports.report_for_a_malformed_period_requested_msg'));
        }
        if($need_to_redirect_back) {
            $previous_page = array_slice(explode('/', url()->previous()), -1)[0];       // TODO: forse posso usare redirect()->back()
            return redirect($previous_page)->with('flash_message', $flash_message_for_redirect)
                                           ->with('flash_background_color', 'yellow');
        }
        GenerateConsumptionReport::dispatch(Auth::user(), 
                                            ConsumptionReportsController::SUBJECT_TYPE_BERTH, 
                                            [$berth_id], 
                                            $file_type, 
                                            $begin_dt, 
                                            $end_dt)
                                 ->onQueue('reports');
        $flash_message = ucfirst(__('consumption_reports.your_request_has_been_received_you_will_get_a_notification_when_its_available'));
        $flash_background_color = 'green';
        return redirect('consumption_reports')->with('flash_message', $flash_message)
                                              ->with('flash_background_color', $flash_background_color);
    }
}
