<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use Illuminate\Http\Request;
use App\Notifications\EmailChangeNotification;
use Illuminate\Notifications\AnonymousNotifiable;
use App\Plant;
use App\Subscription;
use App\Berth;
use App\User;
use App\Boat;

class UserController extends Controller {
    public const SUPERVISOR_ACCESS_LEVEL_FIELD_VALUE_FOR_DB = 2;
    public const FULL_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_DB = 1;
    public const VIEWER_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_DB = 0.5;
    public const STANDARD_USER_ACCESS_LEVEL_FIELD_VALUE_FOR_DB = 0;
    public const DEFAULT_ACCESS_LEVEL_FIELD_VALUE_FOR_DB = 0;
    public const DEFAULT_IS_DISABLED_LEVEL_FIELD_VALUE_FOR_DB = 0;

    public const SUPERVISOR_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM = 'supervisor';
    public const FULL_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM = 'admin';
    public const VIEWER_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM = "viewer_admin";
    public const STANDARD_USER_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM = 'standard';
    public const DEFAULT_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM = 'standard';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        // get all the users
        $users = Plant::get_all_users();
        // load the view and pass the users
        return View::make('users.index', ['users' => $users]);
    }

    public function edit($user_id) {
        $data_array = [];
        $data_array['user'] =  User::findOrFail($user_id);
        $boat = $data_array['user']->boat;
        if(($boat) && ($boat->count() > 0)) {
            $data_array['boat'] = $boat;
        }
        return View::make('users.edit', $data_array);
    }

    public function create() {
        return View::make('users.edit');
    }

    private function get_validation_rules($user = null) {
        $validation_rules = ['last_name' => 'required|min:1|max:255',
                             'first_name' => 'required|min:1|max:255',
                             'username' => 'nullable|min:1|unique:users,username'.(!is_null($user) ? ','.$user->get_id() : '').'|max:255',
                             'email' => 'nullable|email|unique:users,email'.(!is_null($user) && (strlen(strval($user->get_email_address())) > 0) ? ','.$user->get_email_address() : ''),
                             'phone' => 'nullable|max:255',      // TODO: avrebbe senso validare il numero di telefono per verificare che abbia un valore plausibile, al momento non lo faccio
                             'notes' => 'nullable|max:65535',
                             'boat_name' => 'nullable|max:255',
                             'cloud_access' => 'required|in:enabled,disabled'];
        if(Auth::user()->is_supervisor()) {
        // se l'utente loggato E' un supervisor, può creare ogni tipo di utente:
            $validation_rules['access_level'] = 'required|in:standard,viewer_admin,admin,supervisor';
        } else {
        // se l'utente loggato NON E' un supervisor, non deve poter creare un utente supervisor:
            $validation_rules['access_level'] = 'required|in:standard,viewer_admin,admin';
        }
        return $validation_rules;
    }

    private function get_validation_messages() {
        $validation_messages = [
            'last_name.required' => __('validation.required', ['attribute' => '['.ucfirst(__('users/edit.user_last_name_field_label_caption')).']']),
            'last_name.min' => __('validation.min.string', ['attribute' => '['.ucfirst(__('users/edit.user_last_name_field_label_caption')).']', 
                                                            'min' => 1]),
            'last_name.max' => __('validation.max.string', ['attribute' => '['.ucfirst(__('users/edit.user_last_name_field_label_caption')).']', 
                                                            'max' => 255]),
            'first_name.required' => __('validation.required', ['attribute' => '['.ucfirst(__('users/edit.user_first_name_field_label_caption')).']']),
            'first_name.min' => __('validation.min.string', ['attribute' => '['.ucfirst(__('users/edit.user_first_name_field_label_caption')).']', 
                                                             'min' => 1]),
            'first_name.max' => __('validation.max.string', ['attribute' => '['.ucfirst(__('users/edit.user_first_name_field_label_caption')).']', 
                                                             'max' => 255]),
            'username.min' => __('validation.min.string', ['attribute' => '['.ucfirst(__('users/edit.user_username_field_label_caption')).']', 
                                                           'min' => 1]),
            'username.max' => __('validation.max.string', ['attribute' => '['.ucfirst(__('users/edit.user_username_field_label_caption')).']', 
                                                           'max' => 255]),
            'username.unique' => __('validation.unique', ['attribute' => '['.ucfirst(__('users/edit.user_username_field_label_caption')).']']),
            'email.email' => __('validation.email', ['attribute' => '['.ucfirst(__('users/edit.user_email_field_label_caption')).']']),
            'email.unique' => __('validation.unique', ['attribute' => '['.ucfirst(__('users/edit.user_email_field_label_caption')).']']),
            'phone.max' => __('validation.max.string', ['attribute' => '['.ucfirst(__('users/edit.user_phone_number_field_label_caption')).']', 
                                                        'max' => 255]),
            'notes.max' => __('validation.max.string', ['attribute' => '['.ucfirst(__('users/edit.user_notes_field_label_caption')).']',
                                                        'max' => 65535]),
            'boat_name.max' => __('validation.max.string', ['attribute' => '['.ucfirst(__('users/edit.user_notes_field_label_caption')).']', 
                                                            'max' => 255]),
            'cloud_access.required' => __('validation.required', ['attribute' => '['.ucfirst(__('users/edit.user_cloud_access_label_caption')).']']),
            'cloud_access.in' => __('validation.in', ['attribute' => '['.ucfirst(__('users/edit.user_cloud_access_label_caption')).']']),
            'access_level.required' => __('validation.required', ['attribute' => '['.ucfirst(__('users/edit.user_access_level_label_caption')).']']),
            'access_level.in' => __('validation.in', ['attribute' => '['.ucfirst(__('users/edit.user_access_level_label_caption')).']']),
        ];
        return $validation_messages;
    }

    private function get_access_level_field_value_for_db($access_level_field_value_from_form) {
        switch($access_level_field_value_from_form) {
            case self::SUPERVISOR_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM:
                $access_level_field_value_for_db = self::SUPERVISOR_ACCESS_LEVEL_FIELD_VALUE_FOR_DB;
                break;
            case self::FULL_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM:
                $access_level_field_value_for_db = self::FULL_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_DB;
                break;
            case self::VIEWER_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM:
                $access_level_field_value_for_db = self::VIEWER_ADMIN_ACCESS_LEVEL_FIELD_VALUE_FOR_DB;
                break;
            case self::STANDARD_USER_ACCESS_LEVEL_FIELD_VALUE_FOR_FORM:
            default:
                $access_level_field_value_for_db = self::STANDARD_USER_ACCESS_LEVEL_FIELD_VALUE_FOR_DB;
        }
        return $access_level_field_value_for_db;
    }

    private function get_is_disabled_field_value_for_db($cloud_access_field_value_from_form) {
        switch($cloud_access_field_value_from_form) {
        // TODO: definire delle costanti qui nel controller (o nel PagesController) e riutilizzarle anche negli attributi value della view, anzichè usare delle stringhe hardcodate?
            case 'disabled':
                $is_disabled_field_value_for_db = true;
                break;
            case 'enabled':
            default:
                $is_disabled_field_value_for_db = false;
        }
        return $is_disabled_field_value_for_db;
    }

    // TODO: devo impedire anche lato server che un utente viewer_admin possa editare gli utenti o inserirne di nuovi

    public function store(Request $request) {
    	$validator = Validator::make($request->all(), $this->get_validation_rules(), $this->get_validation_messages());
        if($validator->fails()) {
            return redirect()->back()
                             ->withErrors($validator)
                             ->withInput();
        }
        $access_level_field_value_for_db = $this->get_access_level_field_value_for_db($request->access_level);
        if(isset($request->cloud_access)) {
            $is_disabled_field_value_for_db = $this->get_is_disabled_field_value_for_db($request->cloud_access);
        } else {
            $is_disabled_field_value_for_db = self::DEFAULT_IS_DISABLED_LEVEL_FIELD_VALUE_FOR_DB;
        }
        $user = User::create([
            'last_name' => $request->last_name,
            'first_name' => $request->first_name,
            'username' => isset($request->username) ? $request->username : null,
            'email' => isset($request->email) ? $request->email : null,
            'phone' => isset($request->phone) ? $request->phone : null,
            'notes' => isset($request->notes) && (strlen(trim($request->notes)) > 0) ? trim($request->notes) : null,
            'access_level' => $access_level_field_value_for_db,
            'is_disabled' => $is_disabled_field_value_for_db,
            'token_for_actions_on_sockets' => User::generate_token_for_actions_on_sockets(),
            // TODO: la generazione del token ci mette un po' di tempo perchè deve confrontare il nuovo token con quello di tutti gli altri utenti, per assicurarsi che sia univoco. Questo ritarda il feedback all'utente. Come risolvere? Rendo il campo nullable sul db e schedulo la generazione del token con un job asincrono, da lanciare sia dopo la creazione di un nuovo utente dall'APIv0Controller, sia alla fine di questo metodo?
        ]);
        if(isset($request->boat_name) && (strlen(trim($request->boat_name)) > 0)) {
            $boat = Boat::create([
                'user_id' => $user->get_id(),
                'name' => $request->boat_name,
                'length' => isset($request->boat_length) ? $request->boat_length : 0,
                'width' => isset($request->boat_width) ? $request->boat_width : 0,
                'depth' => isset($request->boat_depth) ? $request->boat_depth : 0,
            ]);
        }
        return redirect()->route('user_list')
                         ->with('flash_message', ucfirst(__('users/index.user_successfully_created_msg')))
                         ->with('flash_background_color', 'green');     // TODO: potrei avere anche degli errori in fase di creazione dell'utente? Sui campi unique no: ho già messo le validation_rules. Nel caso in cui possano verificarsi: gestirli e restituire un eventuale flash_message di errore rosso
    }

    public function update(Request $request) {
        $user = User::find($request->id);
        $validator = Validator::make($request->all(), $this->get_validation_rules($user), $this->get_validation_messages());
        if($validator->fails()) {
        	return redirect()->back()
                             ->withErrors($validator)
                             ->withInput();
        }
        $access_level_field_value_for_db = $this->get_access_level_field_value_for_db($request->access_level);
        if(isset($request->cloud_access)) {
            $is_disabled_field_value_for_db = $this->get_is_disabled_field_value_for_db($request->cloud_access);
        } else {
            $is_disabled_field_value_for_db = self::DEFAULT_IS_DISABLED_LEVEL_FIELD_VALUE_FOR_DB;
        }
        $user->set_last_name($request->last_name);
        $user->set_first_name($request->first_name);
        if(isset($request->username) && (strlen(strval($request->username)) > 0)) {
            $user->set_username($request->username);
        } else {
            $user->set_username(null);
        }
        if(isset($request->email) && (strlen(strval($request->email)) > 0)) {
            $user->set_email_address($request->email);
        }
        if(isset($request->phone) && (strlen(strval($request->phone)) > 0)) {
            $user->set_phone_number($request->phone);
        } else {
            $user->set_phone_number(null);
        }
        if(isset($request->notes) && (strlen(trim(strval($request->notes))) > 0)) {
            $user->set_notes(trim($request->notes));
        } else {
            $user->set_notes(null);
        }
        $user->set_access_level($access_level_field_value_for_db);
        $user->set_as_disabled($is_disabled_field_value_for_db);        // TODO: e se l'utente loggato disabilita se stesso? valutare se non dare questa possibilità, o se sloggarlo subito dopo aver scritto le modifiche sul db nel caso si verifichi questa eventualità. Forse avrebbe più senso aggiungere un middleware su ogni richiesta, che verifica se l'utente loggato è abilitato e lo slogga se non è così (magari aggiungendo un flash message da visualizzare sulla pagina di login, che spieghi il motivo).
        $user->save();
        if(isset($request->boat_name) && (strlen(trim($request->boat_name)) > 0)) {
            if(isset($request->boat_id) && (intval($request->boat_id) > 0)) {
                $boat = Boat::find($request->boat_id);
            } else {
                $boat = new Boat();
                $boat->set_user_id($user->get_id());
            }
            $boat->set_name($request->boat_name);
            $boat->set_length(floatval($request->boat_length));
            $boat->set_width(floatval($request->boat_width));
            $boat->set_depth(floatval($request->boat_depth));
            $boat->save();
        }
        return redirect()->route('user_list')
                         ->with('flash_message', ucfirst(__('users/index.user_successfully_updated_msg')))
                         ->with('flash_background_color', 'green');     // TODO: potrei avere anche degli errori in fase di update dell'utente? Sui campi unique no, ho già messo le validation_rules. Nel caso in cui possano verificarsi: gestirli e restituire un eventuale flash_message di errore rosso
    }

    public function enable($user_id) {
        $user = User::findOrFail($user_id);
        $user->set_as_disabled(false);
        $user->save();
        return redirect()->route('user_list')
                         ->with('flash_message', ucfirst(__('users/index.user_cloud_access_successfully_enabled_msg', ['name' => $user->get_full_name()])))
                         ->with('flash_background_color', 'green');
    }

    public function disable($user_id) {
        $user = User::findOrFail($user_id);
        $user->set_as_disabled(true);
        $user->save();
        return redirect()->route('user_list')
                         ->with('flash_message', ucfirst(__('users/index.user_cloud_access_successfully_disabled_msg', ['name' => $user->get_full_name()])))
                         ->with('flash_background_color', 'green');
    }

    public function my_subscriptions() {
        return View::make('users.my_subscriptions');
    }

    public function open_notifications_page() {
        $logged_user = Auth::user();
        foreach($logged_user->unreadNotifications as $notification) {
        // prima di mostrare le notifiche all'utente, le marco tutte come lette sul db, dato che sto per visualizzare la pagina. Sulla view posso comunque distinguere quelle che erano già lette prima del caricamento da quelle che non lo erano ancora, dato che ho salvato l'array $user_unread_notifications prima di questo ciclo.
            $notification->markAsRead();
        }
        return View::make('users.notifications', ['all_user_notifications' => $logged_user->notifications->sortByDesc('updated_at'),
                                                  'unread_notifications' => $logged_user->unreadNotifications->sortByDesc('updated_at')]);
    }

    public function open_user_settings_page() {
        $logged_user = Auth::user();
        // recupero delle lingue disponibili per il software:
        $language_files_dir = resource_path('lang');
        $available_software_languages = array_diff(scandir($language_files_dir), array('..', '.'));
        foreach($available_software_languages as $language) {
            $language = strtolower($language);
        }
        $available_um_for_power_alarm_thresholds = [PagesController::KILOWATTS_UNITS_OF_MEASUREMENT_FOR_POWER_ALARM_THRESHOLDS,
                                                    PagesController::AMPERE_UNITS_OF_MEASUREMENT_FOR_POWER_ALARM_THRESHOLDS ];
        return View::make('users.settings', ['available_software_languages' => $available_software_languages,
                                             'user_language_preference' => strtolower($logged_user->get_language_preference()),
                                             'available_um_for_power_alarm_thresholds' => $available_um_for_power_alarm_thresholds,
                                             'user_power_alarm_thresholds_um_preference' => $logged_user->wants_power_alarm_thresholds_in_ampere()]);
    }

    public function set_user_settings_and_redirect($user_id) {
        $logged_user = Auth::user();
        if(intval(Auth::id()) === intval($user_id)) {
        // se l'utente loggato è lo stesso per il quale si stanno aggiornando le preferenze:
            $submitted_language_preference = request()->input('language_preference');
            $submitted_power_alarm_thresholds_preference = intval(request()->input('power_alarm_thresholds_preference'));
            // recupero delle lingue disponibili:
            $language_files_dir = resource_path('lang');
            $available_software_languages = array_diff(scandir($language_files_dir), array('..', '.'));
            if(in_array($submitted_language_preference, $available_software_languages)) {
            // se la lingua scelta rientra tra quelle disponibili:
                // imposta la nuova preferenza dell'utente:
                $logged_user->set_language_preference($submitted_language_preference);
                // imposta la lingua della webapp a runtime:
                App::setLocale($submitted_language_preference);
            }
            // recupero delle unità di misura disponibili per gli allarmi di consumo energia:
            $available_um_for_power_alarm_thresholds = [PagesController::KILOWATTS_UNITS_OF_MEASUREMENT_FOR_POWER_ALARM_THRESHOLDS,
                                                        PagesController::AMPERE_UNITS_OF_MEASUREMENT_FOR_POWER_ALARM_THRESHOLDS ];
            foreach($available_um_for_power_alarm_thresholds as $available_um_entry) {
                if($submitted_power_alarm_thresholds_preference === $available_um_entry['value']) {
                // se l'unità di misura scelta rientra tra quelle disponibili, imposta la nuova preferenza dell'utente:
                    $logged_user->set_wants_power_alarm_thresholds_in_ampere(boolval($submitted_power_alarm_thresholds_preference));
                }
                // e salva le modifiche alle preferenze dell'utente:
                $logged_user->save();
            }
            // poi prepara il messaggio di conferma della modifica da mostrare all'utente:
            $flash_message = ucfirst(__('users/settings.your_settings_have_been_updated_flash_message'));
            $flash_message_background_color = 'green';
        } else {
        // se l'utente loggato non è quello per il quale si stanno loggando le preferenze:
            // prepara un messaggio di errore da mostrare all'utente:
            $flash_message = ucfirst(__('users/settings.an_error_has_occured_message_caption'));
            $flash_message_background_color = 'yellow';
        }
        
        // TODO: qui se è cambiata la mail devo mostrare un messaggio che spiega cosa fare quando faccio il redirect alla view (forse quel messaggio va tenuto fisso fino al termine della procedura)


        // valido l'indirizzo email:
        $validation_rules = ['email' => 'required|email|unique:users,email,'.$logged_user->get_id()];
        $validation_error_messages = ['email' => ucfirst(__('users/settings.email_address_is_invalid_or_already_in_use'))];
        $validator = Validator::make(request()->all(), $validation_rules, $validation_error_messages);
        if($validator->fails()) {
            // rimandando indietro l'utente con un messaggio di errore se non è valido:
            return redirect()->back()
                             ->withInput()
                             ->withErrors($validator);
        }
        if(Auth::user()->email !== request()->email) {
        // se l'indirizzo email è stato modificato: 
            // invio la mail di verifica del nuovo indirizzo (devo usare un oggetto AnonymousNotifiable per poter inviare una notifica mail ad un indirizzo che non è ancora assegnato a nessun utente): 
            $anonymousNotifiable = new AnonymousNotifiable();
            $anonymousNotifiable->route('mail', [request()->email => Auth::user()->get_full_name()]);
            $anonymousNotifiable->notify(new EmailChangeNotification(Auth::user()->id));
            // ed aggiungo le istruzioni per confermare il cambio al messaggio che conferma che le impostazioni sono state modificate: 
            $flash_message .= ". ".ucfirst(__('users/settings.we_have_sent_a_confirmation_email_msg_to_update_your_email_address_flash_message', ['email_address' => request()->email]));
        }
        // infine redireziono alla pagina delle impostazioni utente passando il messaggio appena settato:
        return redirect()->route('open_user_settings_page')
                         ->with('flash_message', $flash_message)
                         ->with('flash_background_color', $flash_message_background_color);
    }

    public function get_unread_notifications_count_via_ajax($username) {
        $unread_notifications_count = 0;
        $user = User::where('username', $username)->first();
        if(!is_null($user) && ($user->count() > 0)) {
            $unread_notifications_count = $user->unreadNotifications->count();
        }
        return response()->json(compact('unread_notifications_count'));
    }

    public function get_notifications_via_ajax($username) {
        $notifications = collect();
        $user = User::where('username', $username)->first();
        if($user->count() > 0) {
            $notifications = $user->notifications->sortByDesc('updated_at')->toArray();
        }
        return response()->json(compact('notifications'));
    }
}
