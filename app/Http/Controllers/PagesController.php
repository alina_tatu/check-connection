<?php

namespace App\Http\Controllers;

class PagesController extends Controller {
// TODO: togliere questo controller, spostando il codice nel PlantController? O rinominarlo in RequestsController? Forse ha più senso smembrarlo: ho iniziato a spostare le costanti che riguardano i report e i grafici di consumo dentro al controller che se ne occupa

    public const SELECT_ELEMENT_OPTION_NONE_SELECTED_VALUE = 'no_option_selected';

    public const USERS_COMBO_FILTER_ALL_USERS = 'all_users';
    public const USERS_COMBO_FILTER_CLOUD_USERS_ONLY = 'cloud_users_only';
    public const USERS_COMBO_FILTER_NOT_CLOUD_USERS_ONLY = 'not_cloud_users_only';
    public const USERS_COMBO_FILTER_ENABLED_USERS_ONLY = 'enabled_users_only';
    public const USERS_COMBO_FILTER_DISABLED_USERS_ONLY = 'disabled_users_only';
    public const USERS_COMBO_FILTER_SUPERVISOR_USERS_ONLY = 'supervisor_users_only';
    public const USERS_COMBO_FILTER_FULL_ADMIN_USERS_ONLY = 'full_admin_users_only';
    public const USERS_COMBO_FILTER_VIEWER_ADMIN_USERS_ONLY = 'viewer_admin_users_only';
    public const USERS_COMBO_FILTER_STANDARD_USERS_ONLY = 'standard_users_only';

    public const BERTHS_COMBO_FILTER_AVAILABLE_BERTHS_ONLY = 'available_berths_only';
    public const BERTHS_COMBO_FILTER_ASSIGNED_BERTHS_ONLY = 'assigned_berths_only';
    public const BERTHS_COMBO_FILTER_ALL_BERTHS = 'all_berths';

    public const SUBSCRIPTIONS_COMBO_FILTER_ACTIVE_SUBSCRIPTIONS_ONLY = 'active_subscriptions_only';
    public const SUBSCRIPTIONS_COMBO_FILTER_ENDED_SUBSCRIPTIONS_ONLY = 'ended_subscriptions_only';
    public const SUBSCRIPTIONS_COMBO_FILTER_ALL_SUBSCRIPTIONS = 'all_subscriptions';

    public const KILOWATTS_UNITS_OF_MEASUREMENT_FOR_POWER_ALARM_THRESHOLDS = ['label' => 'kW', 'value' => 0];
    public const AMPERE_UNITS_OF_MEASUREMENT_FOR_POWER_ALARM_THRESHOLDS = ['label' => 'A', 'value' => 1];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
    // TODO: questo metodo è ancora usato dalla route '/'. Spostarlo nel PlantController o lasciarlo qui?
        if(auth()->user()->is_viewer_admin()) {
        // se l'utente è (almeno) un viewer_admin:
            switch(config('marina.admin_home_view')) {
            // controllo il setting relativo alla vista di default per gli admin, e lo redireziono a quella:
                case 'plant_status':
                            default:
                    return redirect()->route('plant_status');
                break;
                case 'plant_map':
                    return redirect()->route('plant_map');
            }
        } else {
        // se l'utente NON è (almeno) un viewer_admin:
            // lo redireziono alla route my_subscriptions:
            return redirect()->route('my_subscriptions');
        }
    }
}
