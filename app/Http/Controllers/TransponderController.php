<?php

namespace App\Http\Controllers;

use App\Transponder;

class TransponderController extends Controller {
    public function select_new_transponder_to_create_socket_subscriptions() {
        // TODO: non riesco ad ordinare la collection in modo che vengano visualizzati per primi i transponder che hanno attivato subscriptions prepagate più di recente. Sul model Subscription ho scritto degli scopes che potrebbero essermi utili, provare con quelli.
        
        // TODO: serve validare qualcosa?

        $transponders_with_postpaid_subscriptions_not_bound_to_sockets = Transponder::with(['active_subscriptions',
                                                                                            'active_subscriptions.current_costs_record'])
                                                                                    ->whereHas('active_subscriptions', function($query) {
                                                                                        $query->whereNull('socket_id')
                                                                                                ->whereNull('end_dt')
                                                                                                /*->orderBy('begin_dt', 'desc')*/;
                                                                                    })
                                                                                    ->whereHas('active_subscriptions.current_costs_record', function($query) {
                                                                                        $query->where(function($query) { 
                                                                                            $query->where(function($nestedQuery1) { 
                                                                                                        $nestedQuery1->where('power_cost_per_kwh', 0)
                                                                                                                     ->orWhereNull('power_cost_per_kwh'); 
                                                                                                })
                                                                                                ->where(function($nestedQuery2) { 
                                                                                                        $nestedQuery2->where('water_cost_per_m3', 0)
                                                                                                                     ->orWhereNull('water_cost_per_m3');
                                                                                                }); 
                                                                                            });
                                                                                    })
                                                                                    ->where(function($query) {
                                                                                        $query->whereNull('used_for_socket_subscriptions')
                                                                                              ->orWhere('used_for_socket_subscriptions', false);
                                                                                    })
                                                                                    ->get();
        return view('transponders.select_new_transponder_to_create_socket_subscriptions', compact('transponders_with_postpaid_subscriptions_not_bound_to_sockets'));
    }

    public function enable_to_socket_subscriptions_activation() {
        // TODO: validazione

        $transponder = Transponder::findByCode(request()->input("transponder_id"));
        $transponder->set_as_used_for_socket_subscriptions(true);
        $transponder->save();
        $flash_message = ucfirst('berths/create_subscription.selected_transponder_has_been_set_to_be_used_for_socket_subscriptions_msg');        
        return redirect()->route('new_berth_subscription', ['berth_id' => request()->input('berth_on_which_the_subscription_must_be_created')])
                         ->with('flash_message', $flash_message)
                         ->with('flash_background_color', 'green');
    }
}
