<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\Controller;
use App\Notifications\EmailChangeNotification;
use App\User;

class EmailChangeController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Email Change Controller
    |--------------------------------------------------------------------------
    |
    | This controller allows the user to change his email address after he
    | verifies it through a message delivered to the enew email address.
    | This uses a temporarily signed url to validate the email change.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        // Only the authenticated user can change its email, but he should be able
        // to verify his email address using other device without having to be
        // authenticated. This happens a lot when they confirm by phone.
        $this->middleware('auth')->only('change');

        // A signed URL will prevent anyone except the User to change his email.
        $this->middleware('signed')->only('verify');
    }

    /**
     * Verifies and completes the Email change
     *
     * @param Request $request
     * @param User $user
     * @param string $email
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function verify(Request $request) {
        request()->validate([
            'user' => 'required|integer|exists:users,id',
            'email' => 'required|email|unique:users'
        ]);
        $user = User::find(request()->user);

        // TODO: devo gestire il caso in cui non esista nessun utente che corrisponde all'id specificato: devo mostrare un errore? (posso farlo eventualmente con gli error messages della validazione di laravel)


        // Change the Email
        $user->update([
            'email' => request()->email
        ]);

        // And finally return the view telling the change has been done
        return redirect(route('open_user_settings_page'))->with('flash_message', 
                                                                ucfirst(__('users/settings.your_settings_have_been_updated_flash_message')))
                                                         ->with('flash_background_color', 'green');
    }
}