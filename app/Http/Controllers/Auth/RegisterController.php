<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Rules\UsernameToRegisterAlreadyHasAPassword;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        $validation_rules = ['first_name' => ['required', 'string', 'max:255'],
                             'last_name' => ['required', 'string', 'max:255'],
                             'username' => ['required', 'exists:users,username', 'string', 'max:255', new UsernameToRegisterAlreadyHasAPassword],
                             'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
                             'password' => ['required', 'string', 'min:8', 'confirmed']];
        $validation_messages = ['password.confirmed' => __('validation.confirmed', ['attribute' => 'password']),
                                'email.email' =>  __('validation.email', ['attribute' => '[:attribute]']),
                                'email.unique' => ucfirst(__('auth.signup_page_validation_error_email_address_already_registered', ['password_reset_url' => route('password.reset', ['token' => csrf_token()])])),
                                'username.exists' => ucfirst(__('auth.signup_page_validation_error_username_already_exists_must_be_authorized_by_reception_staff')),
                                'max' => __('validation.lte.string', ['attribute' => '[:attribute]',
                                                                      'value' => '[:max]']),
                                'min' => __('validation.gte.string', ['attribute' => '[:attribute]',
                                                                      'value' => '[:min]']),
                                'required' => __('validation.required', ['attribute' => '[:attribute]'])];
        return Validator::make($data, $validation_rules, $validation_messages);
    }

    /**
     * Register the user after the validation.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {
        $user_with_same_username = User::where('username', '=', $data['username'])->first();
        $user_with_same_username->set_hashed_password(Hash::make($data['password']));
        $user_with_same_username->set_email_address($data['email']);
        $user_with_same_username->save();
        return $user_with_same_username;
    }
}
