<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\PasswordExpiredRequest;

class ExpiredPasswordController extends Controller {
    public function expired() {
        return view('auth.passwords.expired');
    }

    protected function validator(array $data) {
        $validation_rules = ['password' => ['required', 
                                            'string', 
                                            'min:8', 
                                            'confirmed']];
        $validation_messages = ['password.confirmed' => __('validation.confirmed', ['attribute' => 'password']),
                                'max' => __('validation.lte.string', ['attribute' => '[:attribute]',
                                                                      'value' => '[:max]']),
                                'min' => __('validation.gte.string', ['attribute' => '[:attribute]',
                                                                      'value' => '[:min]']),
                                'required' => __('validation.required', ['attribute' => '[:attribute]'])];
        return Validator::make($data, $validation_rules, $validation_messages);
        
    }

    public function postExpired(PasswordExpiredRequest $request) {
        // Verifica correttezza password attuale:
        if(!Hash::check($request->current_password, $request->user()->password)) {
            return redirect()->back()
                             ->withErrors(['current_password' => ucfirst(__('auth.password_not_correct'))]);
        }
        // Verifica che la nuova password sia diversa da quella precedente:
        if(Hash::check($request->password, $request->user()->password)) {
            return redirect()->back()
                             ->withErrors(['password' => ucfirst(__('auth.password_new_cannot_be_the_same_as_the_current_one'))]);
        }
        // verifica nuova password:
        $validation_rules = ['password' => ['required', 'string', 
                                            'min:8', 
                                            'confirmed']];
        $validation_messages = ['password.confirmed' => __('validation.confirmed', ['attribute' => 'password']),
                                'max' => __('validation.lte.string', ['attribute' => '[:attribute]',
                                                                      'value' => '[:max]']),
                                'min' => __('validation.gte.string', ['attribute' => '[:attribute]',
                                                                      'value' => '[:min]']),
                                'required' => __('validation.required', ['attribute' => '[:attribute]'])];

        // TODO: la password vecchia e quella nuova non possono essere uguali: aggiungere la regola e il messaggio

        $validator = Validator::make($request->all(), $validation_rules, $validation_messages);
        if($validator->fails()) {
            return redirect()->back()
                             ->withInput()
                             ->withErrors($validator);
        }
        // aggiornamento della password e dei campi relativi al cambio:
        $request->user()->update([
            'password' => Hash::make($request->password),
            'password_changed_at' => Carbon::now()->toDateTimeString(),
            'needs_password_change' => false,
        ]);
        // TODO: i flash messages non si vedono (nè sulla berths/status, nè sulla users/my_subscriptions)! perchè?
        return redirect('/')->with('flash_message', ucfirst(__('auth.password_changed_successfully')))
                            ->with('flash_background_color', 'green');
    }
}
