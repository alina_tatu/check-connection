<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\SubscriptionConsumptionPayment;
use App\Subscription;

class SubscriptionConsumptionPaymentsController extends Controller {
    public function create(Request $request, $subscription_id) {
        // TODO: fare in modo che non sia possibile pagare più di quanto consumato? o lasciare questa possibilità per poter gestire pagamenti anticipati?
        $request->merge(['subscription_id' => $subscription_id]);
        $validation_rules = [
            'subscription_id' => 'required|numeric|exists:subscriptions,id',
            'paid_power_consumption_amount' => 'exclude_if:paid_water_consumption_amount.gte,true|required|numeric|gte:0',  // TODO: con l'exclude_if vorrei escludere solo la regola required, ma secondo me in questo modo le escludo tutte. Verificare.
            'paid_water_consumption_amount' => 'exclude_if:paid_power_consumption_amount.gte,true|required|numeric|gte:0',  // TODO: con l'exclude_if vorrei escludere solo la regola required, ma secondo me in questo modo le escludo tutte. Verificare.
        ];
        $validation_error_messages = [
            'required' => ucfirst(__('validation.custom.generic.invalid_value')),
            'numeric' => ucfirst(__('validation.custom.generic.invalid_value')),
            'exists' => ucfirst(__('validation.custom.generic.invalid_value')),
            'gte' => ucfirst(__('validation.custom.generic.invalid_value')),
        ];
        $validator = Validator::make($request->all(), $validation_rules, $validation_error_messages);
        if($validator->fails()) {
            return redirect()->back()
                             ->withInput()
                             ->withErrors($validator);
        }
        $payment = new SubscriptionConsumptionPayment();
        $payment->set_subscription_id($subscription_id);
        $payment->set_timestamp(Carbon::now('UTC'));
        $payment->set_paid_power_amount_kwh($request->input('paid_power_consumption_amount'));
        $payment->set_paid_water_amount_cm($request->input('paid_water_consumption_amount'));
        // TODO: teoricamente - almeno nei porti gestiti da Farrokh - i clienti vorrebbero sapere la quantità di energia e acqua pagate in valuta, non in kWh e m3. Quindi ho previsto i due campi paid_power_amount e paid_water_amount. Ma al momento non posso gestirli perchè - per le subscriptions postpagate - il TagManager mi manda i costi a zero. Di conseguenza setto a zero i due campi perchè non sono nullable, ma la cosa andrà gestita (potrei prevedere di far inserire i costi del contratto nella view subscription/consumption)
        $payment->set_paid_power_amount_currency(0);        
        $payment->set_paid_water_amount_currency(0);
        $payment->set_operator_id(Auth::id());
        $payment->save();
        $subscription = Subscription::find($subscription_id);
        $begin_dt = $subscription->get_begin_timestamp();
        $end_dt = Carbon::now(config('dates_currency_and_locale.local_timezone'));
        $epower_id_has_changed_during_interval = false;
        $subscription_power_consumption = $subscription->get_consumption_on_all_plant_during_interval($begin_dt, $end_dt, ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
        $subscription_water_consumption = $subscription->get_consumption_on_all_plant_during_interval($begin_dt, $end_dt, ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
        return View::make('subscriptions.consumption', ['subscription' => $subscription,
                                                        'subscription_power_consumption' => $subscription_power_consumption,
                                                        'subscription_water_consumption' => $subscription_water_consumption,
                                                        'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval])
                   ->with('flash_message', ucfirst(__('subscriptions/consumption.payment_successfully_added_msg')))
                   ->with('flash_background_color', 'green');
                   // TODO: perchè non riesco a visualizzare i messaggi di pagamento inserito?
    }

    public function delete(Request $request, int $payment_id) {
        $payment = SubscriptionConsumptionPayment::find($payment_id);
        $validation_rules = [
            'payment_id' => 'exists:subscription_consumption_payments,id',
        ];
        $validation_error_messages = [
            'exists' => ucfirst(__('validation.custom.generic.invalid_value')),
        ];
        if(($payment) && ($payment->count() > 0)) {
            $subscription = Subscription::find($payment->subscription_id);
            if(($subscription) && ($subscription->count() > 0)) {
                $request->merge([
                    'subscription_id' => $subscription->id,
                ]);
                $validation_rules['subscription_id'] = 'exists:subscriptions,id';
            }
        }
        $validator = Validator::make($request->all(), $validation_rules, $validation_error_messages);
        if($validator->fails()) {
            // TODO: non sono sicuro che qui vengano intercettati tutti gli errori del validator, verificare
            return redirect()->back()
                             ->withInput()
                             ->withErrors($validator);
        } else {
            $payment->delete();
        }
        $begin_dt = $subscription->get_begin_timestamp();
        $end_dt = Carbon::now(config('dates_currency_and_locale.local_timezone'));
        $epower_id_has_changed_during_interval = false;
        $subscription_power_consumption = $subscription->get_consumption_on_all_plant_during_interval($begin_dt, $end_dt, ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
        $subscription_water_consumption = $subscription->get_consumption_on_all_plant_during_interval($begin_dt, $end_dt, ConsumptionReportsController::REPORT_VALUE_WATER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
        return View::make('subscriptions.consumption', ['subscription' => $subscription,
                                                        'subscription_power_consumption' => $subscription_power_consumption,
                                                        'subscription_water_consumption' => $subscription_water_consumption,
                                                        'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval])
                   ->with('flash_message', ucfirst(__('subscriptions/consumption.payment_successfully_deleted_msg')))
                   ->with('flash_background_color', 'green');
                   // TODO: perchè non riesco a visualizzare i messaggi di conferma eliminazione?
    }
}
