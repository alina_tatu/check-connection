<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use DateTimeInterface;

/**
 * App\Action
 *
 * @property int $id
 * @property string $type
 * @property string $object_type
 * @property int $object_id
 * @property Carbon $request_timestamp
 * @property int $requesting_user_id
 * @property Carbon|null $completion_timestamp
 * @property string|null $completing_terminal_name
 * @property-read \App\User $requesting_user
 * @method static \Illuminate\Database\Eloquent\Builder|Action disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Action newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Action newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Action query()
 * @method static \Illuminate\Database\Eloquent\Builder|Action whereCompletingTerminalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Action whereCompletionTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Action whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Action whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Action whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Action whereRequestTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Action whereRequestingUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Action whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Action withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Action extends Model {
    use Cachable;
    use Traits\TimeZoneAware;

    protected $table = 'actions';
    protected $primaryKey = 'id';

    public $timestamps = false;
    protected $dates = ['request_timestamp',
                        'completion_timestamp'];
    public $dateFormat = 'Y-m-d H:i:s';

    public const ACTION_TYPE_RESET_SOCKET = 'RESET';
    public const ACTION_TYPE_TURN_OFF_SOCKET = 'OFF';
    public const ACTION_TYPE_TURN_ON_SOCKET = 'ON';

    public const OBJECT_TYPE_SOCKET = 'SOCKET';

    protected function serializeDate(DateTimeInterface $date) {
    // metodo per mantenere retrocompatibilità dopo l'upgrade a Laravel 7 (v. https://laravel.com/docs/7.x/upgrade#date-serialization)
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'object_type',
        'object_id',
        'request_timestamp',
        'requesting_user_id',
        'completion_timestamp',
        'completing_terminal_name',
        'lost_money_entry_id',
    ];

    public function get_id(): int {
    // restituisce l'id del record action
        return intval($this->attributes[$this->primaryKey]);
    }

    public function set_id(int $id): bool {
    // imposta l'id del record action al valore passato, se è maggiore di zero. Restiutisce false se l'id passato è minore di zero.
        if($id > 0) {
            $this->attributes[$this->primaryKey] = $id;
            return true;
        } else {
            return false;
        }
    }

    public function set_object_id(int $object_id): bool {
    // imposta l'id dell'oggetto al quale si riferisce l'azione al parametro passato, se maggiore o uguale a zero. Restituisce false se il parametro è negativo.
        if($object_id >= 0) {
            $this->object_id = $object_id;
            return true;
        } else {
            return false;
        }
    }

    public function get_object_id(): int {
    // restituisce l'id del'oggetto al quale si riferisce l'azione
        return intval($this->object_id);
    }

    public function get_request_timestamp(): Carbon {
    // restituisce il timestamp nel quale l'azione è stata completata
        return $this->ConvertToLocalTimeZone($this->request_timestamp);
    }

    public function set_request_timestamp(Carbon $request_timestamp): bool {
    // imposta il timestamp nel quale l'azione è stata completata
        $this->request_timestamp = $this->ConvertToDefaultDatabaseTimeZone($request_timestamp);
        return true;
    }

    public function get_type(): string {
    // restituisce il tipo dell'azione
        return strval($this->type);
    }

    public function set_type(string $type): bool {
    // imposta il tipo dell'azione, se combacia con uno dei valori previsti. Restiutisce false se il tipo passato non corrisponde a nessuno dei valori previsti.
        if(($type === $this::ACTION_TYPE_RESET_SOCKET) ||
           ($type === $this::ACTION_TYPE_TURN_OFF_SOCKET) ||
           ($type === $this::ACTION_TYPE_TURN_ON_SOCKET)) {
            $this->type = $type;
            return true;
        } else {
            return false;
        }
    }

    public function get_object_type(): string {
    // restituisce il tipo dell'oggetto target dell'azione
        return strval($this->type);
    }

    public function set_object_type(string $object_type): bool {
    // imposta il tipo dell'oggetto target dell'azione, se combacia con uno dei valori previsti. Restiutisce false se il tipo passato non corrisponde a nessuno dei valori previsti.
        if(($object_type === $this::OBJECT_TYPE_SOCKET)) {    // al momento sono previste solo le azioni sulle prese. Se ne verranno aggiunte altre, va definita la relativa costante pubblica + aggiunta la clausola qui
            $this->object_type = $object_type;
            return true;
        } else {
            return false;
        }
    }

    public function requesting_user() {
    // restituisce l'utente che ha richiesto l'azione
        return $this->belongsTo(User::class, 'requesting_user_id', 'id');
    }

    public function set_requesting_user_id(int $requesting_user_id): bool {
    // imposta l'id dell'utente che ha richiesto l'azione al valore passato, se > 0. Restiutisce false se il valore passato non è > 0.
        if(($requesting_user_id > 0)) {
            $this->requesting_user_id = $requesting_user_id;
            return true;
        } else {
            return false;
        }
    }

    public function get_completion_timestamp(): ?Carbon {
    // restituisce il timestamp nel quale l'azione è stata completata
        return $this->ConvertToLocalTimeZone($this->completion_timestamp);
    }

    public function set_completion_timestamp(Carbon $completion_timestamp = null): bool {
    // imposta il timestamp nel quale l'azione è stata completata
        $this->completion_timestamp = $this->ConvertToDefaultDatabaseTimeZone($completion_timestamp);
        return true;
    }

    public function get_completing_terminal_name(): ?string {
    // restituisce il nome del terminale che ha eseguito l'azione
        // TODO: al momento questo campo non viene settato dal supervisore, perciò sarà sempre null. Implementare quando possibile (sempre che abbia senso: il supervisore attualmente è uno solo per ciascun impianto. Nel momento in cui verranno previsti più tipi di azioni e di oggetti target, forse le azioni verranno eseguite anche dal TagManager)
        return strval($this->completing_terminal_name);
    }

    public function set_completing_terminal_name(string $completing_terminal_name): bool {
    // imposta il nome del terminale che ha eseguito l'azione alla stringa passata. Restituisce false se la stringa è lunga zero caratteri.
        // TODO: al momento questo campo non viene settato dal supervisore, perciò sarà sempre null. Implementare quando possibile (sempre che abbia senso: il supervisore attualmente è uno solo per ciascun impianto. Nel momento in cui verranno previsti più tipi di azioni e di oggetti target, forse le azioni verranno eseguite anche dal TagManager)
        if(strlen($completing_terminal_name) > 0) {
            $this->completing_terminal_name = $completing_terminal_name;
            return true;
        } else {
            return false;
        }
    }

    public function lost_money_entry() {
        return $this->hasOne(LostMoneyForResettedSockets::class, 'action_id');
    }
}
