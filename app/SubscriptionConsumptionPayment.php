<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use DateTimeInterface;

/**
 * App\SubscriptionConsumptionPayment
 *
 * @property int $id
 * @property int $subscription_id
 * @property string $paid_power_amount_kwh
 * @property string $paid_power_amount_currency
 * @property string $paid_water_amount_cm
 * @property string $paid_water_amount_currency
 * @property int $operator_id
 * @property Carbon $created_at
 * @property Carbon|null $deleted_at
 * @property-read \App\User $operator
 * @property-read \App\Subscription $subscription
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionConsumptionPayment newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionConsumptionPayment newQuery()
 * @method static \Illuminate\Database\Query\Builder|SubscriptionConsumptionPayment onlyTrashed()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionConsumptionPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment whereOperatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment wherePaidPowerAmountCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment wherePaidPowerAmountKwh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment wherePaidWaterAmountCm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment wherePaidWaterAmountCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionConsumptionPayment withCacheCooldownSeconds($seconds = null)
 * @method static \Illuminate\Database\Query\Builder|SubscriptionConsumptionPayment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SubscriptionConsumptionPayment withoutTrashed()
 * @mixin \Eloquent
 */
class SubscriptionConsumptionPayment extends Model {
    use Cachable;
    use Traits\TimeZoneAware;
    use SoftDeletes;

    protected $table = 'subscription_consumption_payments';
    protected $primaryKey = 'id';

    public $timestamps = true;
    const UPDATED_AT = null;
    protected $dates = ['created_at'];
    public $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'timestamp',
        'subscription_id',
        'paid_power_amount_kwh',
        'paid_power_amount_currency',
        'paid_water_amount_cm',
        'paid_water_amount_currency',
    ];
    
    protected function serializeDate(DateTimeInterface $date) {
    // metodo per mantenere retrocompatibilità dopo l'upgrade a Laravel 7 (v. https://laravel.com/docs/7.x/upgrade#date-serialization)
        return $date->format('Y-m-d H:i:s');
    }

    public function get_id(): int {
        return intval($this->attributes[$this->primaryKey]);
    }

    public function set_id(int $value) {
        $this->attributes[$this->primaryKey] = $value;
    }

    public function get_timestamp(): Carbon {
        return $this->ConvertToLocalTimeZone($this->created_at);
    }

    public function set_timestamp(Carbon $timestamp): bool {
        $timestamp->setTimezone(config('app.timezone'));
        $this->created_at = $this->ConvertToDefaultDatabaseTimeZone($timestamp);
        return true;
    }

    public function subscription() {
        return $this->belongsTo(Subscription::class);
    }

    public function set_subscription_id(int $subscription_id): bool {
        $this->subscription_id = $subscription_id;
        return true;
    }

    public function get_paid_power_amount_kwh(): float {
        return $this->paid_power_amount_kwh;
    }

    public function set_paid_power_amount_kwh(float $value) {
        $this->paid_power_amount_kwh = $value;
    }

    public function get_paid_power_amount_currency(): float {
        return $this->paid_power_amount_currency;
    }

    public function set_paid_power_amount_currency(float $value) {
        $this->paid_power_amount_currency = $value;
    }

    public function get_paid_water_amount_cm(): float {
        return $this->paid_water_amount_cm;
    }

    public function set_paid_water_amount_cm(float $value) {
        $this->paid_water_amount_cm = $value;
    }

    public function get_paid_water_amount_currency(): float {
        return $this->paid_water_amount_currency;
    }

    public function set_paid_water_amount_currency(float $value) {
        $this->paid_water_amount_currency = $value;
    }

    public function set_operator_id(int $operator_id): bool {
        $this->operator_id = $operator_id;
        return true;
    }

    public function operator() {
        return $this->belongsTo(User::class, 'operator_id');
    }
}
