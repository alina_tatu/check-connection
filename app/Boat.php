<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Boat
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string|null $length
 * @property string|null $width
 * @property string|null $depth
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Boat disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Boat newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Boat newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Boat query()
 * @method static \Illuminate\Database\Eloquent\Builder|Boat whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Boat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Boat whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Boat whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Boat whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Boat whereWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Boat withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Boat extends Model {
    use Cachable;
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'boats';
    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'length', 'width', 'depth',
    ];

    public function get_id(): int {
        return intval($this->attributes[$this->primaryKey]);
    }

    public function set_id(int $value) {
        $this->attributes[$this->primaryKey] = $value;
    }

    public function get_name(): string {
        return $this->name;
    }

    public function set_name(string $value) {
        $this->name = $value;
    }

    public function get_length(): ?float {
        return $this->length;
    }

    public function set_length(float $value = null) {
        $this->length = $value;
    }

    public function get_width(): ?float {
        return $this->width;
    }

    public function set_width(float $value = null) {
        $this->width = $value;
    }

    public function get_depth(): ?float {
        return $this->depth;
    }

    public function set_depth(float $value = null) {
        $this->depth = $value;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function set_user_id(int $value) {
        $this->user_id = $value;
    }
}
