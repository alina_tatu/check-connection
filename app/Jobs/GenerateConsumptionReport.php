<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\PDF;

use App\Exports\BerthConsumptionExcelReport;
use App\Exports\SubscriptionConsumptionExcelReport;
use App\Exports\MarinaOrPontoonConsumptionExcelReport;
use App\Notifications\ReportCreationCompletedNotification;

use App\Http\Controllers\ConsumptionReportsController;
use App\Plant;
use App\Subscription;
use App\Berth;
use App\User;

class GenerateConsumptionReport implements ShouldQueue {
    // devo assicurarmi sempre che il link che va da public/storage a storage/app esista e sia corretto, altrimenti il download del file non sarà possibile
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3600;                 // setto ad un'ora il tempo massimo di processing per un report, perchè essendo jobs potenzialmente lunghi rischiano di andare in timeout
    public $tries = 3;                     // TODO: togliere, lo metto solo temporaneamente per ripulire la coda dei failed jobs a pmne il 12.01.2020

    protected $requesting_user;
    protected $subject_identifier;
    protected $subject_type;
    protected $view_type;
    protected $start_time, $end_time;
    protected $report_file_path;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $requesting_user, string $subject_type, $report_subject_identifier, string $view_type, Carbon $epoch_start_timestamp, Carbon $epoch_end_timestamp) {
        $this->requesting_user = $requesting_user;
        $this->subject_type = $subject_type;
        $this->subject_identifier = $report_subject_identifier;
        $this->view_type = $view_type;
        $this->start_time = $epoch_start_timestamp;
        $this->end_time = $epoch_end_timestamp;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        // TODO: qui devo anche verificare che $this->subject_identifier non sia vuoto e che contenga valori validi. Il Model::find() dei subject devo farlo qui o direttamente dentro al metodo di generazione del report (ovvero nel controller)?
        $subject = $subject_name = null;
        switch($this->subject_type) {
            case ConsumptionReportsController::SUBJECT_TYPE_BERTH:
                $this->report_file_path = $this->generate_socket_consumption_report();
                $subject = Berth::find($this->subject_identifier[0]);
                if(!is_null($subject) && ($subject->count() > 0)) {
                    $subject_name = $subject->get_description();
                }
                break;
            case ConsumptionReportsController::SUBJECT_TYPE_SUBSCRIPTION:
                $this->report_file_path = $this->generate_subscription_consumption_report();
                $subject = Subscription::find($this->subject_identifier[0]);
                if(!is_null($subject) && ($subject->count() > 0)) {
                    if(!is_null($subject->user) && ($subject->user->count() > 0)) {
                        $subject_name = $subject->user->get_full_name();
                    }
                }
                break;
            /*
            case ConsumptionReportsController::SUBJECT_TYPE_USER:
            // TODO: per ora lo commento perchè non è previsto. Se dovrò scommentarlo, dovrò definire anche la costante ConsumptionReportsController::CONSUMPTION_REPORT_TYPE_USER_CONSUMPTION, che ancora non esiste + gestire il subject_name:
                //$this->report_file_path = $this->generate_user_consumption_report();
                break;
            */
            case ConsumptionReportsController::SUBJECT_TYPE_PONTOON:
                $this->report_file_path = $this->generate_pontoon_consumption_report();
                $subject = $subject_name = $this->subject_identifier[0];
                break;
            case ConsumptionReportsController::SUBJECT_TYPE_MARINA:
                $this->report_file_path = $this->generate_marina_consumption_report();
                // in questo caso gli identificatori non mi servono, li lascio a null. Dalla pagina notifications posso vedere il tipo di report e riportarlo nel div delle notifiche direttamente, senza dover verificare il subject_name della notifica.
                break;
        }
        // di seguito genero la notifica di report pronto per l'utente (se la metto qui in teoria viene generata dopo che il report è stato generato, quindi non ho bisogno di mettere mano ai service provider. In alternativa, potrei usare gli eventi di Laravel per generare le notifiche di report pronto, vedere la documentazione di Laravel riguardante gli eventi):
        if(!$this->requesting_user->is_disabled()) {
            $this->requesting_user->notify(new ReportCreationCompletedNotification(Carbon::now(), $this->report_file_path, $this->subject_type, $subject_name, $this->start_time, $this->end_time));
        }
    }

    private function generate_socket_consumption_report() {
        $socket = Berth::find($this->subject_identifier[0]);            // TODO: e se il model richiesto non esiste? al momento gestisco la cosa fuori da questo metodo, ma verificare dove mi conviene farlo e cosa mi conviene fare
        // calcolo i consumi da visualizzare sul report:
        $epower_id_has_changed_during_interval = false;
        $total_power_consumption_in_interval = $socket->get_consumption_in_interval($this->start_time, $this->end_time, ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_TOTAL, ConsumptionReportsController::REPORT_MODE_PLAIN);
        $total_water_consumption_in_interval = $socket->get_consumption_in_interval($this->start_time, $this->end_time, ConsumptionReportsController::REPORT_VALUE_WATER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_TOTAL, ConsumptionReportsController::REPORT_MODE_PLAIN);
        // il 10.06.2020, concordato con raffaele che - se mi capita di calcolare un valore negativo per i consumi - è meglio troncarlo a zero che non visualizzarlo come negativo, dato che un consumo negativo non ha senso:
        if($total_power_consumption_in_interval < 0) {
            $total_power_consumption_in_interval = 0;
        }
        if($total_water_consumption_in_interval < 0) {
            $total_water_consumption_in_interval = 0;
        }
        // TODO: Ha senso usare un job per un report su entità singola come un berth? o è meglio continuare a farli sync? o magari lascio questa scelta configurabile da un parametro in .env?)
        $date_string_for_file_name = Carbon::now()
                                           ->tz(config('dates_currency_and_locale.local_timezone'))
                                           ->format('Ymd_His-u');
        $file_name_without_extension = 'report_'.$this->requesting_user->username.'_'.$date_string_for_file_name;
        // TODO: il codice che segue forse è meglio spostarlo in un altro metodo che sia comune a tutti i report, e che gestisca unicamente il rendering:
        switch($this->view_type) {
        // genero il report utilizzando la view corretta a seconda del tipo di file richiesto:
            case ConsumptionReportsController::VIEW_TYPE_PDF_FILE:
            default:
                $report_file_full_path_for_saving = storage_path('app/public/reports/'.$file_name_without_extension.'.pdf');
                $report_file_full_path_for_saving_for_notification_link = asset('storage/reports/'.$file_name_without_extension.'.pdf');
                $report_title = ucfirst(__('consumption_report_files.pdf_berth_consumption_title_berth_consumption_from_to', ['berth_name' => $socket->get_description(),
                                                                                                                              'begin_timestamp_formatted' => $this->start_time
                                                                                                                                                              	  ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                                                                                                              	  ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon')),
                                                                                                                          	  'end_timestamp_formatted' => $this->end_time
                                                                                                                                                            	->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                                                                                                            	->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'))]));
                $report_data = ['page_title' => ucfirst(__('consumption_report_files.pdf_page_title')),
                                'report_title' => $report_title,
                                'power_consumption_in_interval' => $total_power_consumption_in_interval,
                                'water_consumption_in_interval' => $total_water_consumption_in_interval,
                                'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval];
                $report = App::make('dompdf.wrapper');
                $report->loadView('reports.pdf.berth_or_subscription_consumption_report', $report_data)
                       ->save($report_file_full_path_for_saving)
                       ->stream('report.pdf');
                // TODO: se può essere utile, vedere anche questo link per i pdf: https://appdividend.com/2019/09/13/laravel-6-generate-pdf-from-view-example-tutorial-from-scratch/
                break;
            case ConsumptionReportsController::VIEW_TYPE_XLS_FILE:
                $report_file_full_path_for_saving = storage_path('app/public/reports/'.$file_name_without_extension.'.xlsx');
                // compongo il path da passare a Excel::store(), che a differenza di $report_file_full_path_for_saving non deve contenere la parte iniziale fino a "/storage/app/" compreso:
                $report_file_full_path_for_saving = str_replace(storage_path().'/app/', '', $report_file_full_path_for_saving);
                $report_file_full_path_for_saving_for_notification_link = asset('storage/reports/'.$file_name_without_extension.'.xlsx');
                // app('debugbar')->info($report_file_full_path_for_saving);
                if(ob_get_length() > 0) {
                    ob_end_clean();
                }
                ob_start();
                // TODO: se $epower_id_has_changed_during_interval è true, devo unire le celle in modo che la seconda cella della riga relativa al suo messaggio sul foglio non finisca in fondo al viewport, shiftando a destra anche il resto del contenuto dell'intero foglio. Solo che non capisco su che cosa devo chiamare mergeCells()
                Excel::store(new BerthConsumptionExcelReport($socket->get_description(), $this->start_time, $this->end_time, $total_power_consumption_in_interval, $total_water_consumption_in_interval, $epower_id_has_changed_during_interval), $report_file_full_path_for_saving);
        }
        return $report_file_full_path_for_saving_for_notification_link;
    }

    private function generate_subscription_consumption_report() {
        // TODO: al momento sto vincolando la richiesta dei consumi del contratto ad un singolo posto barca (una transponder subscription potrebbe essere stata usata su più di un posto barca). NOTA: se scelgo di fare in modo che il report di un contratto comprenda i consumi effettuati da esso su tutti i posti barca del porto, dovrò modificare anche la richiesta dei grafici di consumo per subscription, perchè per quelli, attualmente vincolo il calcolo ad un posto barca specifico!

        // TODO: ora che i report sono async, vengono generati nella lingua di default dell'installazione e non in quella settata per l'utente. Sistemare mettendo un set temporaneo del lang


        // l'id del contratto sarà nella posizione 0 dell'array $subject_identifier:
        $subscription = Subscription::find($this->subject_identifier[0]);            // TODO: e se il model richiesto non esiste? al momento gestisco la cosa fuori da questo metodo, ma verificare dove mi conviene farlo e cosa mi conviene fare
        // l'id del posto barca sarà nella posizione 1 dell'array $subject_identifier:
        $berth = Berth::find($this->subject_identifier[1]);            // TODO: e se il model richiesto non esiste? al momento gestisco la cosa fuori da questo metodo, ma verificare dove mi conviene farlo e cosa mi conviene fare
        $subscription_user = $subscription->user;
        if(($subscription_user) && ($subscription_user->count() > 0)) {
            $subscription_user_fullname = $subscription_user->get_full_name();
        } else {
            $subscription_user_fullname = strtolower(__('common.unknown_user'));
        }
        // TODO: ha senso usare un job per un report su entità singola come una subscription? o è meglio continuare a farli sync? o magari lascio questa scelta configurabile da un parametro in .env?
        // la subscription potrebbe aver avuto inizio prima dell'inizio del periodo richiesto, oppure essere terminata prima della sua fine: in questo caso, modifico il periodo in esame per farlo rientrare tra l'inizio e la fine della subscription:
        $subscription_begin_dt = $subscription->get_begin_timestamp();
        if($subscription_begin_dt > $this->start_time) {
            $this->start_time = $subscription_begin_dt;
        }
        $subscription_end_dt = $subscription->get_end_timestamp();
        if(!is_null($subscription_end_dt) && ($subscription_end_dt < $this->end_time)) {
            $this->end_time = Carbon::now(config('app.timezone'));
        }
        $epower_id_has_changed_during_interval = false;
        $subscription_power_consumption_on_berth_in_interval = $subscription->get_consumption_on_berth_during_interval($this->start_time, $this->end_time, ConsumptionReportsController::REPORT_VALUE_POWER, $berth->get_id(), $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
        $subscription_water_consumption_on_berth_in_interval = $subscription->get_consumption_on_berth_during_interval($this->start_time, $this->end_time, ConsumptionReportsController::REPORT_VALUE_WATER, $berth->get_id(), $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_MODE_PLAIN);
        // il 10.06.2020, concordato con raffaele che - se mi capita di calcolare un valore negativo per i consumi - è meglio troncarlo a zero che non visualizzarlo come negativo, dato che un consumo negativo non ha senso:
        if($subscription_power_consumption_on_berth_in_interval < 0) {
            $subscription_power_consumption_on_berth_in_interval = 0;
        }
        if($subscription_water_consumption_on_berth_in_interval < 0) {
            $subscription_water_consumption_on_berth_in_interval = 0;
        }
        // raccolgo il resto dei dati comuni alle views dei report:
        $subscription_power_consumption_in_interval = $subscription_power_consumption_on_berth_in_interval;
        $subscription_water_consumption_in_interval = $subscription_water_consumption_on_berth_in_interval;
        $subscription_description = lcfirst(__('consumption_report_files.pdf_title_subscription_description_with_berth_caption', ['user_name' => $subscription_user_fullname,
                                                                                                                              	  'berth_name' => $berth->get_description()]));
        $date_string_for_file_name = Carbon::now()
                                           ->tz(config('dates_currency_and_locale.local_timezone'))
                                           ->format('Ymd_His-u');
        $file_name_without_extension = 'report_'.$this->requesting_user->username.'_'.$date_string_for_file_name;
        // TODO: il codice che segue forse è meglio spostarlo in un altro metodo che sia comune a tutti i report, e che gestisca unicamente il rendering:
        switch($this->view_type) {
        // genero il report utilizzando la view corretta a seconda del tipo di file richiesto:
            case ConsumptionReportsController::VIEW_TYPE_PDF_FILE:
            default:
                $report_file_full_path_for_saving = storage_path('app/public/reports/'.$file_name_without_extension.'.pdf');
                $report_file_full_path_for_saving_for_notification_link = asset('storage/reports/'.$file_name_without_extension.'.pdf');
                $report_title = ucfirst(__('consumption_report_files.pdf_subscription_consumption_title_subscription_consumption_from_to', ['subscription_description' => $subscription_description,
                                                                                                                                        	'begin_timestamp_formatted' => $this->start_time->copy()->setTimezone(config('dates_currency_and_locale.local_timezone'))->format(config('dates_currency_and_locale.local_datetime_format_full_carbon')),
                                                                                                                                        	'end_timestamp_formatted' => $this->end_time->copy()->setTimezone(config('dates_currency_and_locale.local_timezone'))->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'))]));
                $report_data = ['page_title' => ucfirst(__('consumption_report_files.pdf_page_title')),
                                'report_title' => $report_title,
                                'power_consumption_in_interval' => $subscription_power_consumption_on_berth_in_interval,
                                'water_consumption_in_interval' => $subscription_water_consumption_on_berth_in_interval,
                                'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval];
                $report = App::make('dompdf.wrapper');
                $report->loadView('reports.pdf.berth_or_subscription_consumption_report', $report_data)
                       ->save($report_file_full_path_for_saving)
                       ->stream('report.pdf');
                // TODO: se può essere utile, vedere anche questo link per i pdf: https://appdividend.com/2019/09/13/laravel-6-generate-pdf-from-view-example-tutorial-from-scratch/
                break;
            case ConsumptionReportsController::VIEW_TYPE_XLS_FILE:
                $report_file_full_path_for_saving = storage_path('app/public/reports/'.$file_name_without_extension.'.xlsx');
                // compongo il path da passare a Excel::store(), che a differenza di $report_file_full_path_for_saving non deve contenere la parte iniziale fino a "/storage/app/" compreso:
                $report_file_full_path_for_saving = str_replace(storage_path().'/app/', '', $report_file_full_path_for_saving);
                $report_file_full_path_for_saving_for_notification_link = asset('storage/reports/'.$file_name_without_extension.'.xlsx');
                // app('debugbar')->info($report_file_full_path_for_saving);
                if(ob_get_length() > 0) {
                    ob_end_clean();
                }
                ob_start();
                // TODO: se $epower_id_has_changed_during_interval è true, devo unire le celle in modo che la seconda cella della riga relativa al suo messaggio sul foglio non finisca in fondo al viewport, shiftando a destra anche il resto del contenuto dell'intero foglio. Solo che non capisco su che cosa devo chiamare mergeCells()
                Excel::store(new SubscriptionConsumptionExcelReport($subscription_description, $this->start_time, $this->end_time, $subscription_power_consumption_on_berth_in_interval, $subscription_water_consumption_on_berth_in_interval, $epower_id_has_changed_during_interval), $report_file_full_path_for_saving);
        }
        return $report_file_full_path_for_saving_for_notification_link;
    }

    private function generate_pontoon_consumption_report() {
        $pontoon_name = $this->subject_identifier[0];
        $all_berths_in_pontoon = Plant::get_all_berths_in_pontoon($pontoon_name);       // TODO: questa query la faccio anche nel controller che gestisce la richiesta del report, ma per non ripeterla dovrei passare a questo metodo direttamente la collection di tutti i posti barca, oltre al nome del pontile

        $total_power_consumption_in_interval = $total_water_consumption_in_interval = 0;
        $berth_consumption_details = [];
        $previous_epower_id_has_changed_during_interval = $epower_id_has_changed_during_interval = false;
        if(($all_berths_in_pontoon) && ($all_berths_in_pontoon->count() > 0)) {
            foreach($all_berths_in_pontoon as $current_berth) {
                $berth_power_consumption = $current_berth->get_consumption_in_interval($this->start_time, $this->end_time, ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_TOTAL, ConsumptionReportsController::REPORT_MODE_PLAIN);
                $berth_water_consumption = $current_berth->get_consumption_in_interval($this->start_time, $this->end_time, ConsumptionReportsController::REPORT_VALUE_WATER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_TOTAL, ConsumptionReportsController::REPORT_MODE_PLAIN);
                $epower_id_has_changed_during_interval = $previous_epower_id_has_changed_during_interval || $epower_id_has_changed_during_interval;
                $previous_epower_id_has_changed_during_interval = $epower_id_has_changed_during_interval;
                // il 10.06.2020, concordato con raffaele che - se mi capita di calcolare un valore negativo per i consumi - è meglio troncarlo a zero che non visualizzarlo come negativo, dato che un consumo negativo non ha senso:
                if($berth_power_consumption < 0) {
                    $berth_power_consumption = 0;
                }
                if($berth_water_consumption < 0) {
                    $berth_water_consumption = 0;
                }
                $total_power_consumption_in_interval += $berth_power_consumption;
                $total_water_consumption_in_interval += $berth_water_consumption;
                $berth_consumption_details[$current_berth->get_id()] = new \stdClass();
                $berth_consumption_details[$current_berth->get_id()]->berth_name = $current_berth->get_description();
                $berth_consumption_details[$current_berth->get_id()]->power_consumption = $berth_power_consumption;
                $berth_consumption_details[$current_berth->get_id()]->water_consumption = $berth_water_consumption;
            }
        }
        $date_string_for_file_name = Carbon::now()
                                           ->tz(config('dates_currency_and_locale.local_timezone'))
                                           ->format('Ymd_His-u');
        $file_name_without_extension = 'report_'.$this->requesting_user->username.'_'.$date_string_for_file_name;
        // TODO: il codice che segue forse è meglio spostarlo in un altro metodo che sia comune a tutti i report, e che gestisca unicamente il rendering:
        switch($this->view_type) {
        // genero il report utilizzando la view corretta a seconda del tipo di file richiesto:
            case ConsumptionReportsController::VIEW_TYPE_PDF_FILE:
            default:
                $report_file_full_path_for_saving = storage_path('app/public/reports/'.$file_name_without_extension.'.pdf');
                $report_file_full_path_for_saving_for_notification_link = asset('storage/reports/'.$file_name_without_extension.'.pdf');
                $report_title = ucfirst(__('consumption_report_files.pdf_pontoon_consumption_title_pontoon_consumption_from_to', ['pontoon_name' => $pontoon_name,
                                                                                                                              	  'begin_timestamp_formatted' => $this->start_time
                                                                                                                              	  									  ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                                                                              	  									  ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon')),
                                                                                                                              	  'end_timestamp_formatted' => $this->end_time
                                                                                                                              	  									->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                                                                              	  									->format(config('dates_currency_and_locale.local_datetime_format_full_carbon')),
                                                                                                                              	  'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval]));
                $report_data = ['page_title' => ucfirst(__('consumption_report_files.pdf_page_title')),
                                'report_title' => $report_title,
                                'power_consumption_in_interval' => $total_power_consumption_in_interval,
                                'water_consumption_in_interval' => $total_water_consumption_in_interval,
                                'berth_consumption_details_records' => $berth_consumption_details,
                                'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval];
                $report = App::make('dompdf.wrapper');
                $report->loadView('reports.pdf.marina_or_pontoon_consumption_report', $report_data)
                       ->save($report_file_full_path_for_saving)
                       ->stream('report.pdf');
                // TODO: se può essere utile, vedere anche questo link per i pdf: https://appdividend.com/2019/09/13/laravel-6-generate-pdf-from-view-example-tutorial-from-scratch/
                break;
            case ConsumptionReportsController::VIEW_TYPE_XLS_FILE:
                $report_file_full_path_for_saving = storage_path('app/public/reports/'.$file_name_without_extension.'.xlsx');
                // compongo il path da passare a Excel::store(), che a differenza di $report_file_full_path_for_saving non deve contenere la parte iniziale fino a "/storage/app/" compreso:
                $report_file_full_path_for_saving = str_replace(storage_path().'/app/', '', $report_file_full_path_for_saving);
                $report_file_full_path_for_saving_for_notification_link = asset('storage/reports/'.$file_name_without_extension.'.xlsx');
                // app('debugbar')->info($report_file_full_path_for_saving);
                if(ob_get_length() > 0) {
                    ob_end_clean();
                }
                ob_start();
                // TODO: se $epower_id_has_changed_during_interval è true, devo unire le celle in modo che la seconda cella della riga relativa al suo messaggio sul foglio non finisca in fondo al viewport, shiftando a destra anche il resto del contenuto dell'intero foglio. Solo che non capisco su che cosa devo chiamare mergeCells()
                Excel::store(new MarinaOrPontoonConsumptionExcelReport($this->start_time, $this->end_time, $total_power_consumption_in_interval, $total_water_consumption_in_interval, $berth_consumption_details, $epower_id_has_changed_during_interval, ConsumptionReportsController::SUBJECT_TYPE_PONTOON, $pontoon_name), $report_file_full_path_for_saving);
        }
        return $report_file_full_path_for_saving_for_notification_link;
    }

    private function generate_marina_consumption_report() {
        $all_berths_in_plant = Plant::get_all_berths();       // TODO: questa query la faccio anche nel controller che gestisce la richiesta del report, ma per non ripeterla dovrei passare a questo metodo direttamente la collection di tutti i posti barca
        $total_power_consumption_in_interval = $total_water_consumption_in_interval = 0;
        $berth_consumption_details = [];
        if(($all_berths_in_plant) && ($all_berths_in_plant->count() > 0)) {
            $epower_id_has_changed_during_interval = $previous_epower_id_has_changed_during_interval = false;
            foreach($all_berths_in_plant as $current_berth) {
                $berth_power_consumption = $current_berth->get_consumption_in_interval($this->start_time, $this->end_time, ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_TOTAL, ConsumptionReportsController::REPORT_MODE_PLAIN);
                $berth_water_consumption = $current_berth->get_consumption_in_interval($this->start_time, $this->end_time, ConsumptionReportsController::REPORT_VALUE_WATER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_TOTAL, ConsumptionReportsController::REPORT_MODE_PLAIN);
                // il 10.06.2020, concordato con raffaele che - se mi capita di calcolare un valore negativo per i consumi - è meglio troncarlo a zero che non visualizzarlo come negativo, dato che un consumo negativo non ha senso:
                if($berth_power_consumption < 0) {
                    $berth_power_consumption = 0;
                }
                if($berth_water_consumption < 0) {
                    $berth_water_consumption = 0;
                }
                $total_power_consumption_in_interval += $berth_power_consumption;
                $total_water_consumption_in_interval += $berth_water_consumption;
                $berth_consumption_details[$current_berth->get_id()] = new \stdClass();
                $berth_consumption_details[$current_berth->get_id()]->berth_name = $current_berth->get_description();
                $berth_consumption_details[$current_berth->get_id()]->power_consumption = $berth_power_consumption;
                $berth_consumption_details[$current_berth->get_id()]->water_consumption = $berth_water_consumption;
                $epower_id_has_changed_during_interval = $epower_id_has_changed_during_interval || $previous_epower_id_has_changed_during_interval;
                $previous_epower_id_has_changed_during_interval = $epower_id_has_changed_during_interval;
            }
        }
        $date_string_for_file_name = Carbon::now()
                                           ->tz(config('dates_currency_and_locale.local_timezone'))
                                           ->format('Ymd_His-u');
        $file_name_without_extension = 'report_'.$this->requesting_user->username.'_'.$date_string_for_file_name;
        // TODO: il codice che segue forse è meglio spostarlo in un altro metodo che sia comune a tutti i report, e che gestisca unicamente il rendering:
        switch($this->view_type) {
        // carico la view corretta a seconda del tipo di file richiesto:
            case ConsumptionReportsController::VIEW_TYPE_PDF_FILE:
            default:
                $report_file_full_path_for_saving = storage_path('app/public/reports/'.$file_name_without_extension.'.pdf');
                $report_file_full_path_for_saving_for_notification_link = asset('storage/reports/'.$file_name_without_extension.'.pdf');
                $report_title = ucfirst(__('consumption_report_files.pdf_plant_consumption_title_plant_consumption_from_to', ['begin_timestamp_formatted' => $this->start_time
                																																				  ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                																																				  ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon')),
                                                                                                                              'end_timestamp_formatted' => $this->end_time
                                                                                                                                                                ->setTimezone(config('dates_currency_and_locale.local_timezone'))
                                                                                                                                                                ->format(config('dates_currency_and_locale.local_datetime_format_full_carbon'))]));
                $report_data = ['page_title' => ucfirst(__('consumption_report_files.pdf_page_title')),
                                'report_title' => $report_title,
                                'power_consumption_in_interval' => $total_power_consumption_in_interval,
                                'water_consumption_in_interval' => $total_water_consumption_in_interval,
                                'berth_consumption_details_records' => $berth_consumption_details,
                                'epower_id_has_changed_during_interval' => $epower_id_has_changed_during_interval];
                $report = App::make('dompdf.wrapper');
                $report->loadView('reports.pdf.marina_or_pontoon_consumption_report', $report_data)
                       ->save($report_file_full_path_for_saving)
                       ->stream('report.pdf');
                // TODO: se può essere utile, vedere anche questo link per i pdf: https://appdividend.com/2019/09/13/laravel-6-generate-pdf-from-view-example-tutorial-from-scratch/
                break;
            case ConsumptionReportsController::VIEW_TYPE_XLS_FILE:
                $report_file_full_path_for_saving = storage_path('app/public/reports/'.$file_name_without_extension.'.xlsx');
                // compongo il path da passare a Excel::store(), che a differenza di $report_file_full_path_for_saving non deve contenere la parte iniziale fino a "/storage/app/" compreso:
                $report_file_full_path_for_saving = str_replace(storage_path().'/app/', '', $report_file_full_path_for_saving);
                $report_file_full_path_for_saving_for_notification_link = asset('storage/reports/'.$file_name_without_extension.'.xlsx');
                // app('debugbar')->info($report_file_full_path_for_saving);
                if(ob_get_length() > 0) {
                    ob_end_clean();
                }
                ob_start();
                // TODO: se $epower_id_has_changed_during_interval è true, devo unire le celle in modo che la seconda cella della riga relativa al suo messaggio sul foglio non finisca in fondo al viewport, shiftando a destra anche il resto del contenuto dell'intero foglio. Solo che non capisco su che cosa devo chiamare mergeCells()
                Excel::store(new MarinaOrPontoonConsumptionExcelReport($this->start_time, $this->end_time, $total_power_consumption_in_interval, $total_water_consumption_in_interval, $berth_consumption_details, $epower_id_has_changed_during_interval, ConsumptionReportsController::SUBJECT_TYPE_MARINA), $report_file_full_path_for_saving);
        }
        return $report_file_full_path_for_saving_for_notification_link;
    }
}
