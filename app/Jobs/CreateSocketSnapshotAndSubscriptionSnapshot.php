<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\BaseSocketSnapshot;
use App\SocketSnapshot;
use App\SocketLastSnapshot;
use App\Transponder;
use App\Berth;
use App\Notifications\SmartCloudNotification;
use App\Notifications\SocketOfflineAlarmNotification;
use App\Notifications\SerialLineOfflineAlarmNotification;
use App\Notifications\SocketTheftAlarmNotification;
use App\Notifications\SocketMinPowerConsumptionAlarmNotification;
use App\Notifications\SocketMaxPowerConsumptionAlarmNotification;
use App\Notifications\SocketMaxWaterTapTimeAlarmNotificationForAdmins;
use App\Notifications\SocketMinCreditAlarmNotification;
use App\Notifications\SocketMaxWaterTapTimeAlarmNotificationForUsers;

class CreateSocketSnapshotAndSubscriptionSnapshot implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3600;                 // setto ad un'ora il tempo massimo di processing per un nuovo snapshot, perchè essendo jobs potenzialmente lunghi rischiano di andare in timeout. TODO: se scrivo un job che elimina in background le snapshots inutili e gira periodicamente (EDIT: l'ho già scritto), probabilmente poi posso togliere questa riga perchè le snapshot presenti sul db diventano molte meno e dovrei ridurre drasticamente il tempo necessario a fare i calcoli per la creazione di un nuovo snapshot
    public $tries = 5000;

    protected $timestamp;
    protected $socket_id;
    protected $link_status, $on_off_status;
    protected $transponder_id;
    protected $epower_id;
    protected $total_power_cnt, $postpaid_power_cnt, $total_water_cnt, $postpaid_water_cnt;
    protected $instant_power_consumption;
    protected $residual_credit;
    protected $alarms_string_from_smartview;


    
    

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($timestamp,
                                $socket_id,
                                $link_status,
                                $on_off_status,
                                $transponder_id,
                                $total_power_cnt,
                                $total_water_cnt,
                                $postpaid_power_cnt,
                                $postpaid_water_cnt,
                                $residual_credit,
                                $instant_power_consumption,
                                $epower_id,
                                $alarms_string_from_smartview) {
        $this->timestamp = $timestamp;
        $this->socket_id = $socket_id;
        $this->link_status = $link_status;
        $this->on_off_status = $on_off_status;
        $this->transponder_id = $transponder_id;
        $this->total_power_cnt = $total_power_cnt;
        $this->total_water_cnt = $total_water_cnt;
        $this->postpaid_power_cnt = $postpaid_power_cnt;
        $this->postpaid_water_cnt = $postpaid_water_cnt;
        $this->residual_credit = $residual_credit;
        $this->instant_power_consumption = $instant_power_consumption;
        $this->epower_id = $epower_id;
        $this->alarms_string_from_smartview = $alarms_string_from_smartview;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $socket_snapshot_record = new SocketSnapshot();
        $socket_snapshot_record->set_timestamp($this->timestamp);
        $socket_snapshot_record->set_berth_id($this->socket_id);
        $socket_snapshot_record->set_link_status($this->link_status);
        $socket_snapshot_record->set_on_off_status($this->on_off_status);
        $socket_snapshot_record->set_transponder_id($this->transponder_id);
        $socket_snapshot_record->set_total_power_cnt($this->total_power_cnt);
        $socket_snapshot_record->set_total_water_cnt($this->total_water_cnt);
        $socket_snapshot_record->set_postpaid_power_cnt($this->postpaid_power_cnt);
        $socket_snapshot_record->set_postpaid_water_cnt($this->postpaid_water_cnt);
        $socket_snapshot_record->set_residual_credit($this->residual_credit);
        $socket_snapshot_record->set_transponder_id_field_is_reliable_to_retrieve_connected_subscription_flag(true);      // setto momentaneamente questo flag a true come valore di default. Lo riprenderò più avanti per verificare se va settato a false.
        $socket_snapshot_record->set_can_retrieve_subscription_via_subscription_id_flag(true);
        if(($this->on_off_status === SocketSnapshot::SOCKET_STATUS_OFF) && ($this->instant_power_consumption > 0)) {
        // 02.07.2020: Fabio ha notato che il software mostra un consumo > 0 anche se la presa è spenta. Succede anche su SmartView e sembra essere un problema del firmware dell'EPower. Concordato con Fabio e Sergio che in quel caso la forziamo a zero perchè il cliente non veda anomalie (Sergio dice che non è un consumo che viene contabilizzato).
            $this->instant_power_consumption = 0;
        }
        $socket_snapshot_record->set_instant_power_consumption($this->instant_power_consumption);
        $socket_snapshot_record->set_epower_id($this->epower_id);
        if(!is_null($this->alarms_string_from_smartview)) {
            $socket_snapshot_record->set_temporary_alarms_from_smartview($this->alarms_string_from_smartview);
        }
        // per evitare di riempire il db di snapshot identiche tra loro se non per il timestamp, di seguito verifico se ha senso scrivere un nuovo record, oppure aggiornare l'ultimo già presente sul db per la presa che ha logico $this->socket_id. Per ora, setto a true il bool che mi dirà se crearne uno nuovo:
        
        // salvo prima il record, perchè per poter calcolare gli allarmi che lo snapshot triggera è necessario che sia già stato scritto sul db:
        $socket_snapshot_record->save();
        $previous_snapshot_for_berth = $second_previous_snapshot_for_berth = null;
        $berth = Berth::find($this->socket_id);
        $last_two_previous_snapshots_for_berth = SocketSnapshot::where('socket_id', $this->socket_id)
                                                               ->where('timestamp', '<', $this->timestamp)
                                                               ->orderBy('timestamp', 'desc')
                                                               ->take(2)
                                                               ->get();
        if(isset($last_two_previous_snapshots_for_berth[0]) && !is_null($last_two_previous_snapshots_for_berth[0])) {
            $previous_snapshot_for_berth = $last_snapshot_received_for_berth = $last_two_previous_snapshots_for_berth[0];
        } else {
            $last_snapshot_received_for_berth = null;
        }
        if(isset($last_two_previous_snapshots_for_berth[1]) && !is_null($last_two_previous_snapshots_for_berth[1])) {
            $second_previous_snapshot_for_berth = $last_two_previous_snapshots_for_berth[1];
        }
        $reason_to_keep_the_snapshot = '';      // la istanzio solo perchè il metodo SocketSnapshot::is_useful_for_calculating_consumption() la richiede, ma in questo caso non verrà usata
        
        if($socket_snapshot_record->is_useful_for_calculating_consumption(false, true, false, true, $reason_to_keep_the_snapshot, $berth, $previous_snapshot_for_berth, $second_previous_snapshot_for_berth, $last_snapshot_received_for_berth)) {
        // se devo creare un nuovo snapshot:
            // non devo fare altro: non mi serve ri-salvare perchè ho già salvato per poter fare il calcolo degli allarmi
        } else {
        // se devo aggiornare il record snapshot precedente: 
            // ne modifico il timestamp a quello appena ricevuto:
            $new_timestamp = $socket_snapshot_record->get_timestamp()
                                                    ->tz(config('app.timezone'));
            $previous_snapshot_for_berth->set_timestamp($new_timestamp);
            // e lo aggiorno:
            $socket_snapshot_record_save_success = $previous_snapshot_for_berth->update();
            // elimino (senza soft-delete) $socket_snapshot_record (cioè l'ultimo snapshot ricevuto, che ho dovuto scrivere temporaneamente poco fa per poter calcolare gli allarmi): 
            $socket_snapshot_record->forceDelete();
            // poi aggiorno il riferimento di $socket_snapshot_record, facendolo puntare a $previous_snapshot_for_berth:
            $socket_snapshot_record = $previous_snapshot_for_berth;
            // e ne rilancio il salvataggio per fare in modo che venga aggiornato anche il record sulla tabella socket_last_snapshots
            $socket_snapshot_record->save();
        }
        $socket_snapshot_record_on_socket_last_snapshots_table = SocketSnapshot::create_instance_for_socket_last_snapshot_table_and_delete_old_socket_last_snapshots_for_same_sockets($socket_snapshot_record);
        $is_not_connected_alarm_set = $is_theft_alarm_set = $is_max_water_tap_time_alarm_for_admins_set = $is_min_power_consumption_alarm_set = $is_max_power_consumption_alarm_set = $is_min_socket_credit_alarm_set = $is_max_water_tap_time_alarm_for_users_set = false;
        if(($socket_snapshot_record->berth) && ($socket_snapshot_record->berth->count() > 0)) {
            if(!($previous_snapshot_for_berth) || ($previous_snapshot_for_berth->count() === 0) ||
               !($socket_snapshot_record->triggers_same_alarms_as($previous_snapshot_for_berth))) {
                /*  se:
                    1. non ho trovato snapshot precedenti per questa presa e/o
                    2. lo snapshot triggera allarmi diversi da quello precedente */
                // salvo i vari stati allarme per non doverli ri-verificare ogni volta che mi serviranno nel codice che segue:
                $is_not_connected_alarm_set = $socket_snapshot_record->is_not_connected_alarm_set();
                $is_theft_alarm_set = $socket_snapshot_record->is_theft_alarm_set();
                $is_max_water_tap_time_alarm_for_admins_set = $socket_snapshot_record->is_max_water_tap_time_alarm_for_admins_set();
                $is_min_power_consumption_alarm_set = $socket_snapshot_record->is_min_power_consumption_alarm_set();
                $is_max_power_consumption_alarm_set = $socket_snapshot_record->is_max_power_consumption_alarm_set();
                $is_min_socket_credit_alarm_set = $socket_snapshot_record->is_min_socket_credit_alarm_set();
                $is_max_water_tap_time_alarm_for_users_set = $socket_snapshot_record->is_max_water_tap_time_alarm_for_users_set();
                if($is_not_connected_alarm_set || $is_theft_alarm_set || $is_max_water_tap_time_alarm_for_admins_set) {
                    // agli admin per il momento invio solo le notifiche relative a mancata comunicazione e allarme furto. Non mando notifiche riguardo a consumi oltre le soglie settate dall'utilizzatore
                    // cerco gli utenti admin non disabilitati:
                    $admin_users_to_notify = User::where('access_level', '>', 0)
                                                 ->where('is_disabled', '=', 0)
                                                 ->get();
                    if($admin_users_to_notify) {
                        foreach($admin_users_to_notify as $admin) {
                            // e a ciascun utente admin trovato, invio le notifiche relative agli allarmi attivi:
                            // NOTA: al momento, agli admin non sto inviando le notifiche relative a soglie di consumo superate. Direi di mantenerle attive solo per l'intestatario della subscription.
                            if($is_not_connected_alarm_set) {
                            // invio dell'eventuale notifica di mancata comunicazione agli admin:
                                $open_notifications_with_same_data_count =  $admin->notifications()
                                                                                  ->where('type', SocketOfflineAlarmNotification::class)
                                                                                  ->where('data->object_id', 'like', '%'.$this->socket_id.'%')
                                                                                  ->where('data->closed_at', 'like', '%null%')
                                                                                  ->count();
                                if($open_notifications_with_same_data_count === 0) {
                                // se non ho già inviato la stessa notifica all'utente:
                                    // gliela invio ora:
                                    $admin->notify(new SocketOfflineAlarmNotification($this->timestamp,
                                                                                      $this->socket_id,
                                                                                      $socket_snapshot_record->berth->get_description()));
                                }
                            } else {
                            // se la presa sta comunicando, ha senso verificare la presenza degli allarmi rimanenti:
                                if($is_theft_alarm_set) {
                                    // invio dell'eventuale notifica di allarme furto agli admin:
                                    $open_notifications_with_same_data_count =  $admin->notifications()
                                                                                      ->where('type', SocketTheftAlarmNotification::class)
                                                                                      ->where('data->object_id', 'like', '%'.$this->socket_id.'%')
                                                                                      ->where('data->closed_at', 'like', '%null%')
                                                                                      ->count();
                                    if($open_notifications_with_same_data_count === 0) {
                                    // se non ho già inviato la stessa notifica all'utente:
                                        // gliela invio ora:
                                        $admin->notify(new SocketTheftAlarmNotification($this->timestamp,
                                                                                        $this->socket_id,
                                                                                        $socket_snapshot_record->berth->get_description()));
                                    }
                                }
                                if($is_max_water_tap_time_alarm_for_admins_set) {
                                    // invio dell'eventuale notifica di rubinetto acqua aperto da un tempo maggiore della soglia massima settata (soglia del marina) agli admin:
                                    $open_notifications_with_same_data_count =  $admin->notifications()
                                                                                      ->where('type', SocketTheftAlarmNotification::class)
                                                                                      ->where('data->object_id', 'like', '%'.$this->socket_id.'%')
                                                                                      ->where('data->closed_at', 'like', '%null%')
                                                                                      ->count();
                                    if($open_notifications_with_same_data_count === 0) {
                                    // se non ho già inviato la stessa notifica all'utente:
                                        // gliela invio ora:
                                        $admin->notify(new SocketMaxWaterTapTimeAlarmNotificationForAdmins($this->timestamp,
                                                                                                           $this->socket_id,
                                                                                                           $socket_snapshot_record->berth->get_description()));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $connected_subscription = collect();
            if(!($socket_snapshot_record->is_not_connected_alarm_set())) {
            // se la presa sta comunicando:
                // cerco sul db il transponder con codice $this->transponder_id
                $transponder = Transponder::findByCode($this->transponder_id);
                if(($transponder) && ($transponder->count() > 0)) {
                // se lo trovo:
                    // recupero la subscription collegata:
                    $connected_subscription = $socket_snapshot_record->retrieve_connected_subscription();
                }
                if(($connected_subscription) && ($connected_subscription->count() > 0)) {
                // se ho trovato una subscription:
                    // la collego allo snapshot:
                    
                    //$socket_snapshot_record->set_subscription_id($connected_subscription->get_id());     // TODO: non capisco perchè ma non funziona, provo a settare direttamente subscription_id invece di usare il setter:
                    $socket_snapshot_record->subscription_id = $connected_subscription->id;        // TODO: posso usare associate() (o attach()?)
                    $socket_snapshot_record->save();
                    // TODO: se uso attach()/associate() qui per collegare lo snapshot alla subscription (subscription hasmany-> snapshots) dopo aver definito la relationship (che posso chiamare direttamente BaseSocketSnapshot::subscription() dato che connected_subscription() mi serve in questo metodo per calcolarla), poi posso usare questa nuova relationship direttamente al posto di connected_subscription() e farne l'eager loading. Inoltre posso eliminare il metodo SocketLastSnapshot->set_subscription_id()
                    
                    if($socket_snapshot_record->get_on_off_status() !== SocketSnapshot::SOCKET_STATUS_ON) {
                    // se all'istante dello snapshot la presa non era accesa:
                        /* può essere che non sia mai stata accesa prima dell'inizio di $connected_subscription: di conseguenza il transponder mostrato sullo snapshot non apparteneva all'utente intestatario di $connected_subscription, ma sulla plant_status e sulla plant_map verrebbe mostrato comunque. Qui verifico se l'ultimo snapshot nel quale la presa è risultata accesa è precedente o successivo l'inizio di $connected_subscription: */;
                        if(($socket_snapshot_record->berth) && ($socket_snapshot_record->berth->count() > 0)) {
                        // se l'anagrafica della presa esiste già sul db:
                            // controllo se esiste almeno uno snapshot prima dell'inizio di $connected_subscription nel quale la presa è stata accesa:
                            $socket_has_ever_been_on_before_subscription_started = $socket_snapshot_record->berth->on_off_status_has_occurred_before_timestamp($connected_subscription->get_begin_timestamp(), SocketSnapshot::SOCKET_STATUS_ON, true);
                            if(!$socket_has_ever_been_on_before_subscription_started) {
                            // se non ho trovato nessuno snapshot prima dell'inizio di $connected_subscription nel quale la presa è stata accesa:
                                // non devo restituirla, assegno una collection vuota a $connected_subscription:
                                $connected_subscription = collect();
                                // setto il flag transponder_id_is_reliable_to_retrieve_connected_subscription_flag a false: verrà usato sulle views plant_status e plant_map per non dover rifare questa verifica per ciascuno snapshot (cosa che aumenterebbe parecchio il tempo necessario per restituire le views)
                                $socket_snapshot_record->set_transponder_id_field_is_reliable_to_retrieve_connected_subscription_flag(false);
                                // e salvo di nuovo lo snapshot per rendere effettiva la modifica:
                                $socket_snapshot_record->save();
                                // poi faccio la stessa modifica sul record corrispondente nella tabella socket_last_snapshots:
                                if(isset($socket_snapshot_record_on_socket_last_snapshots_table) && ($socket_snapshot_record_on_socket_last_snapshots_table) && ($socket_snapshot_record_on_socket_last_snapshots_table->count() > 0)) {
                                    $socket_snapshot_record_on_socket_last_snapshots_table->set_transponder_id_field_is_reliable_to_retrieve_connected_subscription_flag(false);
                                    $socket_snapshot_record_on_socket_last_snapshots_table->save();
                                }
                            }
                        }
                    }
                }
            } else {
            // se la presa non sta comunicando
                // recupero la subscription collegata all'ultimo snapshot in cui comunicava:
                $last_linking_socket_snapshot = $socket_snapshot_record->berth->get_last_linking_status_before_timestamp($this->timestamp, true);
                if($last_linking_socket_snapshot) {
					if(boolval($last_linking_socket_snapshot->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
			        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
						$connected_subscription = $last_linking_socket_snapshot->subscription;
			        } else {
			        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
			        	$connected_subscription = $last_linking_socket_snapshot->retrieve_connected_subscription();
			        }
                }
            }
            if(($connected_subscription) && ($connected_subscription->count() > 0)) {
            // se esiste una subscription collegata allo snapshot:
                // recupero l'utente collegato:
                $owner_user_to_notify = $connected_subscription->user;
                if(($owner_user_to_notify) && ($owner_user_to_notify->count() > 0)) {
                // se c'è un utente collegato alla subscription
                    // gli invio le notifiche relative agli allarmi attivi:
                    if($socket_snapshot_record->is_not_connected_alarm_set()) {
                    // invio dell'eventuale notifica di mancata comunicazione all'utente connesso:
                        // verifico se ho già inviato a questo utente la stessa notifica (l'intestatario della subscription potrebbe essere anche un admin, e quindi aver ricevuto la notifica relativa a questo allarme quando - poche righe fa - le ho mandate a tutti gli admin):

                        // $notification_already_sent = $owner_user_to_notify->has_already_received_notification(SocketOfflineAlarmNotification::class, $this->socket_id);
                        // TODO: il metodo User::has_already_received_notification() non sta funzionando correttamente (v. commenti sul metodo), quindi provo a fare la stessa verifica direttamente qui di seguito, in attesa di sistemarlo:
                        $open_notifications_with_same_data_count =  $owner_user_to_notify->notifications()
                                                                                         ->where('type', SocketOfflineAlarmNotification::class)
                                                                                         ->where('data->object_id', 'like', '%'.$this->socket_id.'%')
                                                                                         ->where('data->closed_at', 'like', '%null%')
                                                                                         ->count();
                        if($open_notifications_with_same_data_count === 0) {
                        // se non ho già inviato la stessa notifica all'utente:
                            // gliela invio ora:
                            $owner_user_to_notify->notify(new SocketOfflineAlarmNotification($this->timestamp,
                                                                                             $this->socket_id,
                                                                                             $socket_snapshot_record->berth->get_description()));
                        }
                    } else {
                    // se la presa sta comunicando, ha senso controllare la presenza degli allarmi rimanenti:
                        if($is_theft_alarm_set) {
                        // invio dell'eventuale notifica di allarme furto all'utente connesso:
                            // verifico se ho già inviato a questo utente la stessa notifica (l'intestatario della subscription potrebbe essere anche un admin, e quindi aver ricevuto la notifica relativa a questo allarme quando - poche righe fa - le ho mandate a tutti gli admin):

                            //$notification_already_sent = $owner_user_to_notify->has_already_received_notification(SocketTheftAlarmNotification::class, $this->socket_id);
                            // TODO: il metodo User::has_already_received_notification() non sta funzionando correttamente (v. commenti sul metodo), quindi provo a fare la stessa verifica direttamente qui di seguito, in attesa di sistemarlo:
                            $open_notifications_with_same_data_count =  $owner_user_to_notify->notifications()
                                                                                             ->where('type', SocketTheftAlarmNotification::class)
                                                                                             ->where('data->object_id', 'like', '%'.$this->socket_id.'%')
                                                                                             ->where('data->closed_at', 'like', '%null%')
                                                                                             ->count();
                            if($open_notifications_with_same_data_count === 0) {
                            // se non ho già inviato la stessa notifica all'utente:
                                // gliela invio ora:
                                $owner_user_to_notify->notify(new SocketTheftAlarmNotification($this->timestamp,
                                                                                               $this->socket_id,
                                                                                               $socket_snapshot_record->berth->get_description()));
                            }
                        }
                        if($is_min_power_consumption_alarm_set) {
                        // invio all'utente connesso dell'eventuale notifica di consumo energia inferiore alla soglia minima settata:
                            $owner_user_to_notify->notify(new SocketMinPowerConsumptionAlarmNotification($this->timestamp,
                                                                                                         $this->socket_id,
                                                                                                         $socket_snapshot_record->berth->get_description()));
                        }
                        if($is_max_power_consumption_alarm_set) {
                        // invio all'utente connesso dell'eventuale notifica di consumo energia superiore alla soglia massima settata:
                            $owner_user_to_notify->notify(new SocketMaxPowerConsumptionAlarmNotification($this->timestamp,
                                                                                                         $this->socket_id,
                                                                                                         $socket_snapshot_record->berth->get_description()));
                        }
                        if($is_min_socket_credit_alarm_set) {
                        // invio all'utente connesso dell'eventuale notifica di credito disponibile sulla presa inferiore alla soglia massima settata:
                            $owner_user_to_notify->notify(new SocketMinCreditAlarmNotification($this->timestamp,
                                                                                               $this->socket_id,
                                                                                               $socket_snapshot_record->berth->get_description()));
                        }
                        if($is_max_water_tap_time_alarm_for_users_set) {
                        // invio all'utente connesso dell'eventuale notifica di rubinetto acqua aperto da un tempo maggiore della soglia massima settata (soglia dell'utente):
                            $owner_user_to_notify->notify(new SocketMaxWaterTapTimeAlarmNotificationForUsers($this->timestamp,
                                                                                                     $this->socket_id,
                                                                                                     $socket_snapshot_record->berth->get_description()));
                        }
                    }
                }
            }
        }
        // setto anche sul record nella tabella socket_last_snapshots il subscription_id che ho settato su $socket_snapshot_record (dato che la copia su socket_last_snapshots è avvenuta prima del set):
        $socket_snapshot_record_on_socket_last_snapshots_table->set_subscription_id($socket_snapshot_record->subscription_id);
        $socket_snapshot_record_on_socket_last_snapshots_table->save();


        // TODO: mi sono accorto che sulla tabella socket_snapshots c'è qualcosa che non va: se mando snapshots per la stessa presa a distanza di 10secondi l'uno dall'altro con gli stessi valori (nello specifico, contatori e transponder_id a zero con presa spenta), il vecchio non viene eliminato ma portato avanti come timestamp, però nel frattempo vengono creati anche degli snapshot nuovi. In pratica, il vecchio viene aggiornato ma ne viene creato anche uno nuovo EDIT: forse ora questa cosa è risolta
        
        $socket_snapshot_record->generate_and_write_subscription_snapshot_record();
        // TODO: quando ho provato a rendere asincrona tutta la generazione delle socket_snapshots e delle subscription_snapshots per non dover tenere in attesa il supervisore fino alla fine del processo, ho avuto problemi perchè le chiamate tendevano a non arrivare nell'ordine corretto, e di conseguenza mi ritrovavo a calcolare delle subscriptionsnapshot quando ancora non avevo ricevuto delle socketsnapshot precedenti (quindi sballavo il calcolo dei consumi. Però potrei separare la generazione delle socketsnapshots e quella delle subscriptionsnapshots, mettendo queste ultime in una coda a parte che viene elaborata ad intervalli regolari un chunk alla volta (avendo cura di non arrivare mai troppo avanti nel tempo quando si elabora un chunk, per non rischiare di coprire un intervallo di tempo per il quale i dati non sono ancora arrivati tutti)
    }
}
