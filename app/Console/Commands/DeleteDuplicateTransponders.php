<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Transponder;

class DeleteDuplicateTransponders extends Command {
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transponders:delete_duplicates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elimina le righe duplicate nella tabella transponders, che sono il risultato di un bug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        $unique_transponder_rows = Transponder::select('code', 'used_for_socket_subscriptions')
                                              ->groupBy(['code', 'used_for_socket_subscriptions'])
                                              ->get()
                                              ->toArray();
        foreach($unique_transponder_rows as $current_unique_transponder_row) {
            $first_transponder_row_with_this_code_and_flag = Transponder::where('code', $current_unique_transponder_row['code'])
                                                                        ->where('used_for_socket_subscriptions', $current_unique_transponder_row['used_for_socket_subscriptions'])
                                                                        ->first();
            if(($first_transponder_row_with_this_code_and_flag) && ($first_transponder_row_with_this_code_and_flag->count() > 0)) {
                if(!($first_transponder_row_with_this_code_and_flag->is_used_for_socket_subscriptions())) {
                // se il transponder NON E' usato per le socket_subscriptions
                    // elimino tutti gli altri record Transponder che hanno lo stesso valore di code e che non sono usati per le socket_subscriptions
                    Transponder::where('code', $first_transponder_row_with_this_code_and_flag->get_code())
                                ->where('used_for_socket_subscriptions', intval($first_transponder_row_with_this_code_and_flag->is_used_for_socket_subscriptions()))
                                ->where('id', '!=', $first_transponder_row_with_this_code_and_flag->get_id())
                                ->delete();
                } else {
                // se il transponder E' usato per le socket_subscriptions
                    // posso eliminare tutti gli altri record Transponder che hanno lo stesso valore di code, compresi quelli che non risultano usati per le socket_subscriptions, perchè farà fede questo
                    Transponder::where('code', $first_transponder_row_with_this_code_and_flag->get_code())
                                ->where('id', '!=', $first_transponder_row_with_this_code_and_flag->get_id())
                                ->delete();
                }
            }

            // TODO: ma alla fine questo problema della duplicazione è risolto oppure è ancora esistente? nell'APIv0Controller, quando ricevo una subscription, potrei semplicemente usare createOrUpdate() (funziona per campi non-chiave come code?), mettendo il default del campo used_for_socket_subscriptions a false (se non è già così dalla definizione della tabella). In questo modo il nuovo transponder verrebbe creato solo se non esiste già
        }
        return 0;
        // TODO: e se rendessi questa roba un metodo static del model Transponder, anzichè un command? Potrei chiamarlo ovunque, incluso da qui
    }
}
