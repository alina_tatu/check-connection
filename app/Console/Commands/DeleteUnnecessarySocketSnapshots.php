<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SocketSnapshot;
use App\Berth;
use Illuminate\Support\Facades\Log;

class DeleteUnnecessarySocketSnapshots extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SocketSnapshot:DeleteUnnecessary {snapshot_to_examine_chunk_size?} {--scheduled}';
    private $snapshot_to_examine_default_chunk_size = 500;
    private $snapshot_to_examine_chunk_size;
    private $snapshot_to_delete_chunk_size = 500;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elimina le SocketSnapshot non fondamentali per il calcolo dei consumi, al fine di migliorare la velocità della webapp. Utilizzare il parametro opzionale snapshot_to_examine_chunk_size per definire il numero di snapshot da processare ad ogni chunk. In assenza di questo parametro, la grandezza del chunk è 500';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        // se chiamo il comando con l'opzione "--scheduled", nel log verrà specificato che il comando è stato lanciato dallo scheduler, altrimenti dal log vedrò che è stato lanciato on-demand:
        $scheduled_logged_string = '(on-demand)';
        if($this->option('scheduled') === true) {
            $scheduled_logged_string = ' (scheduled)';
        }
        // specificando l'argomento $snapshot_to_examine_chunk_size, posso modificare il la dimensione del chunk delle snapshot esaminate (diversamente, viene usato il valore di default):
        $this->snapshot_to_examine_chunk_size = $this->snapshot_to_examine_default_chunk_size;
        if(!is_null($this->argument('snapshot_to_examine_chunk_size'))) {
            $this->snapshot_to_examine_chunk_size = intval($this->argument('snapshot_to_examine_chunk_size'));
        }
        $all_berths_in_plant = Berth::all();
        Log::channel('plus_log_generic')->info('Starting'.$scheduled_logged_string.' unnecessary socket_snapshots deletion... Chunk size: '.$this->snapshot_to_examine_chunk_size);
        Log::channel('plus_deleted_snapshots')->info('Starting'.$scheduled_logged_string.' unnecessary socket_snapshots deletion...');
        echo(PHP_EOL.'Starting unnecessary socket_snapshots deletion...');
        Log::channel('plus_deleted_snapshots')->info($all_berths_in_plant->count().' berths found.');
        echo(PHP_EOL.$all_berths_in_plant->count().' berths found.');
        Log::channel('plus_deleted_snapshots')->info('chunk size: '.$this->snapshot_to_examine_chunk_size);
        echo(PHP_EOL.'chunk size: '.$this->snapshot_to_examine_chunk_size);
        $total_processed_snapshots_count = 0;
        $total_snapshots_count = SocketSnapshot::count();
        foreach($all_berths_in_plant as $current_berth) {
        // scorro ognuno dei posti barca del porto:
            echo(PHP_EOL.'Start examining snapshots for the berth ['.$current_berth->get_description().'] (address: '.$current_berth->get_id().')...');
            Log::channel('plus_deleted_snapshots')->info('Start examining snapshots for the berth ['.$current_berth->get_description().'] (address: '.$current_berth->get_id().')...');
            $snapshots_to_delete_for_current_berth = collect();
            $previous_snapshot_for_berth = $second_previous_snapshot_for_berth = null;
            // leggo tutte le snapshots del posto barca corrente:
            $all_snapshots_for_current_berth = SocketSnapshot::where('socket_id', $current_berth->get_id())
                                                             ->orderBy('timestamp', 'asc')
                                                             ->get();
            echo(PHP_EOL.'The berth has '.$all_snapshots_for_current_berth->count().' snapshots');
            Log::channel('plus_deleted_snapshots')->info('The berth has '.$all_snapshots_for_current_berth->count().' snapshots');
            $processed_snapshots_for_current_berth_count = 0;
            $current_berth->snapshots()->orderBy('timestamp', 'asc')->chunk($this->snapshot_to_examine_chunk_size, function ($snapshots_chunk_for_current_berth) use ($current_berth, $all_snapshots_for_current_berth, &$previous_snapshot_for_berth, &$second_previous_snapshot_for_berth, &$snapshots_to_delete_for_current_berth, &$processed_snapshots_for_current_berth_count, &$total_processed_snapshots_count, $total_snapshots_count) {
                // (dividendoli in chunks per risparmiare memoria)
                echo(PHP_EOL.'Next chunk incoming...');
                foreach($snapshots_chunk_for_current_berth as $current_snapshot_for_berth) {
                // per ciascuno snapshot del posto barca corrente:
                    $processed_snapshots_for_current_berth_count++;
                    $total_processed_snapshots_count++;
                    $processed_snapshots_percentage_for_current_berth = round(($processed_snapshots_for_current_berth_count * 100 / $all_snapshots_for_current_berth->count()), 2);
                    $total_processed_snapshots_percentage = round(($total_processed_snapshots_count * 100 / $total_snapshots_count), 2);
                    echo(PHP_EOL.'Processing snapshot '.$processed_snapshots_for_current_berth_count.' of '.$all_snapshots_for_current_berth->count().' ('.$processed_snapshots_percentage_for_current_berth.'% of current berth, '.$total_processed_snapshots_percentage.'% of total snapshots count)');
                    $reason_to_keep_the_snapshot = '';
                    $it_is_safe_to_delete_the_current_snapshot = !($current_snapshot_for_berth->is_useful_for_calculating_consumption(false, false, true, false, $reason_to_keep_the_snapshot, $current_berth, $previous_snapshot_for_berth, $second_previous_snapshot_for_berth, $all_snapshots_for_current_berth->last()));
                    if(!$it_is_safe_to_delete_the_current_snapshot) {
                        //Log::channel('plus_deleted_snapshots')->info('Snapshot with id='.$current_snapshot_for_berth->id.' needs to be kept. Reason: '.$reason_to_keep_the_snapshot.'.');
                        echo(PHP_EOL.'Snapshot with id='.$current_snapshot_for_berth->id.' needs to be kept. Reason: '.$reason_to_keep_the_snapshot.'.');
                    } else {
                    // se - dopo tutte le verifiche - risulta che lo snapshot corrente può essere eliminato senza compromettere il corretto log dei consumi e calcolo degli allarmi: 
                        // lo aggiungo alla lista delle snapshot da eliminare:
                        $snapshots_to_delete_for_current_berth->push($current_snapshot_for_berth);
                        echo(PHP_EOL.'Snapshot with id='.$current_snapshot_for_berth->id.' scheduled for deletion.');
                    }
                    // poi salvo lo snapshot corrente e quello precedente per poterli usare nelle verifiche del prossimo giro:
                    $second_previous_snapshot_for_berth = $previous_snapshot_for_berth;
                    $previous_snapshot_for_berth = $current_snapshot_for_berth;
                }
                echo(PHP_EOL.'End of the chunk reached.'.PHP_EOL.PHP_EOL);
            });
            // una volta esaminate tutte le snapshot del posto barca corrente, elimino quelle che ho contrassegnato:
            echo(PHP_EOL.'Deleting '.$snapshots_to_delete_for_current_berth->count().' SocketSnapshot records...');
            Log::channel('plus_deleted_snapshots')->info('Deleting '.$snapshots_to_delete_for_current_berth->count().' SocketSnapshot records...');
            // faccio l'eliminazione in chunk della dimensione di [$this->snapshot_to_delete_chunk_size] snapshots, perchè quando ci sono molte snapshots da eliminare, eliminarle tutte assieme causa un [General error: 1390 Prepared statement contains too many placeholders]:
            $socket_snapshot_ids_to_delete = $snapshots_to_delete_for_current_berth->pluck('id');
            $start_pos = 0;
            while($start_pos <= $socket_snapshot_ids_to_delete->count()) {
                $current_chunk_of_socket_snapshot_ids_to_delete = $socket_snapshot_ids_to_delete->slice($start_pos, $this->snapshot_to_delete_chunk_size);
                SocketSnapshot::whereIn('id', $current_chunk_of_socket_snapshot_ids_to_delete)->delete();
                $start_pos += $this->snapshot_to_delete_chunk_size;
            }
            echo(PHP_EOL.$snapshots_to_delete_for_current_berth->count().' records deleted.');
            Log::channel('plus_deleted_snapshots')->info($snapshots_to_delete_for_current_berth->count().' records deleted.');

            /*  TODO: in teoria il comando è a posto, ma fare qualche verifica per vedere che non elimini snapshot utili

                un'altra verifica che posso fare è vedere se il socket_snapshot che sto eliminando ha un corrispondente subscription_snapshot e non eliminarlo se esiste (oppure, dato che se tutti i criteri sono soddisfatti dovrebbe trattarsi comunque di un subscription_snapshot inutile, eliminare anche quello)

                poi potrei pensare di fare in modo che - se quando arriva un nuovo snapshot non è cambiato nient'altro che i contatori di consumo (quindi stessa subscription/stesse soglie, intervallo di tempo massimo non ancora passato, etc) e l'incremento dei contatori (totali? o entrambi?) è inferiore a una soglia configurabile (da .env/config? o fissata con una costante nel job/nel model SocketSnapshot?), $need_to_create_a_new_snapshot_record vada a false e quindi venga aggiornato il vecchio anzichè scritto uno nuovo

                se questo snasphot non può essere eliminato, lo scrivo (ma chi me lo setta il flag in questo caso?)
                se lo snapshot precedente dice che il next_snapshot non può essere eliminato, setto il flag keep_snapshot per questo snapaghot a true e lo scrivo
            */

        }
        Log::channel('plus_log_generic')
           ->info('Unnecessary socket_snapshots deletion has ended.');
    }
}
