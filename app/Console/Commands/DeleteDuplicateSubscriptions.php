<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use App\Transponder;
use App\Subscription;
use App\SubscriptionAlarmThresholdsRecord;
use App\SubscriptionCostsRecord;
use App\SubscriptionConsumptionPayment;
use App\SubscriptionSnapshot;

class DeleteDuplicateSubscriptions extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Subscriptions:DeleteDuplicates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Nasce da un problema riscontrato ad Ayia Napa ad agosto 2020: i contratti venivano inseriti più volte sul db con gli stessi dati. Questo comando si occupa di eliminare i duplicati ed eliminare/unificare opportunamente i record collegati.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
    // a fine agosto 2020, ad ayia napa ho notato che c'erano parecchie subscriptions duplicate: stesso transponder_id, stesso user_id, stesse date di inizio/fine ma diverso id. Cerco di sistemare il problema con questo metodo:
        $all_subscriptions_in_plant = Subscription::all();
        $all_subscriptions_in_plant->sortBy('id');
        $this->info('Found '.$all_subscriptions_in_plant->count().' total subscriptions');
        foreach($all_subscriptions_in_plant as $current_subscription) {
            $subscriptions_with_same_data_as_the_current_one = Subscription::where('id', '!=', $current_subscription->id)
                                                                           ->where('transponder_id', $current_subscription->transponder_id)
                                                                           ->where('socket_id', $current_subscription->socket_id)
                                                                           ->where('begin_dt', $current_subscription->begin_dt)
                                                                           ->where('end_dt', $current_subscription->end_dt)
                                                                           ->get();
            $this->info('Subscription with id='.$current_subscription->get_id().' has '.$subscriptions_with_same_data_as_the_current_one->count().' duplicates');
            if($subscriptions_with_same_data_as_the_current_one->count() > 0) {
                foreach($subscriptions_with_same_data_as_the_current_one as $current_duplicate_subscription) {
                    $this->info('Taking care of the alarm thresholds records for the duplicate subscription with id='.$current_duplicate_subscription->get_id().'...');
                    foreach($current_duplicate_subscription->thresholds_records as $current_thresholds_record_on_duplicate_subscription) {
                    // per ogni subscription duplicata, unifico i record delle soglie allarmi:
                        $identical_thresholds_records_on_current_subscription = $current_subscription->thresholds_records()
                                                                                                     ->where('min_power_threshold_kwh', $current_thresholds_record_on_duplicate_subscription->min_power_threshold_kwh)
                                                                                                     ->where('max_power_threshold_kwh', $current_thresholds_record_on_duplicate_subscription->max_power_threshold_kwh)
                                                                                                     ->where('max_water_tap_time_threshold_minutes', $current_thresholds_record_on_duplicate_subscription->max_water_tap_time_threshold_minutes)
                                                                                                     ->where('min_credit_threshold', $current_thresholds_record_on_duplicate_subscription->min_credit_threshold)
                                                                                                     ->where('begin_dt', $current_thresholds_record_on_duplicate_subscription->begin_dt)
                                                                                                     ->where('end_dt', $current_thresholds_record_on_duplicate_subscription->end_dt);
                        if($identical_thresholds_records_on_current_subscription->count() > 0) {
                        // se su $current_subscription esistono record soglie identici a $current_thresholds_record_on_duplicate_subscription:
                            // elimino $current_thresholds_record_on_duplicate_subscription
                            SubscriptionAlarmThresholdsRecord::destroy($current_thresholds_record_on_duplicate_subscription->get_id());
                        } else {
                        // se su $current_subscription NON ESISTONO record soglie identici a $current_thresholds_record_on_duplicate_subscription:
                            // porto questo record su $current_subscription:
                            $current_thresholds_record_on_duplicate_subscription->set_subscription_id($current_subscription->get_id());
                            $current_thresholds_record_on_duplicate_subscription->save();
                        }
                    }
                    $this->info('Taking care of the costs records for the duplicate subscription with id='.$current_duplicate_subscription->get_id().'...');
                    foreach($current_duplicate_subscription->costs_records as $current_costs_record_on_duplicate_subscription) {
                    // per ogni subscription duplicata, unifico i record dei costi:
                        $identical_costs_records_on_current_subscription = $current_subscription->costs_records()
                                                                                                ->where('power_cost_per_kwh', $current_costs_record_on_duplicate_subscription->power_cost_per_kwh)
                                                                                                ->where('water_cost_per_m3', $current_costs_record_on_duplicate_subscription->water_cost_per_m3)
                                                                                                ->where('begin_dt', $current_costs_record_on_duplicate_subscription->begin_dt)
                                                                                                ->where('end_dt', $current_costs_record_on_duplicate_subscription->end_dt);
                        if($identical_costs_records_on_current_subscription->count() > 0) {
                        // se su $current_subscription esistono record costi identici a $current_costs_record_on_duplicate_subscription:
                            // elimino $current_costs_record_on_duplicate_subscription:
                            SubscriptionCostsRecord::destroy($current_costs_record_on_duplicate_subscription->get_id());
                        } else {
                        // se su $current_subscription NON ESISTONO record costi identici a $current_costs_record_on_duplicate_subscription:
                            // porto questo record su $current_subscription:
                            $current_costs_record_on_duplicate_subscription->set_subscription_id($current_subscription->get_id());
                            $current_costs_record_on_duplicate_subscription->save();
                        }
                    }
                    $this->info('Taking care of the payments records for the duplicate subscription with id='.$current_duplicate_subscription->get_id().'...');
                    foreach($current_duplicate_subscription->payments as $current_payments_record_on_duplicate_subscription) {
                    // per ogni subscription duplicata, unifico i record pagamenti duplicati
                        $current_payments_record_on_duplicate_subscription->set_subscription_id($current_subscription->get_id());
                        $current_payments_record_on_duplicate_subscription->save();
                    }
                    if(strlen(strval($current_duplicate_subscription->notes)) > 0) {
                    // per ogni subscription duplicata, unifico il campo delle note:
                        if(strlen(strval($current_subscription->notes)) > 0) {
                            $new_notes_field = $current_subscription->get_notes().". ".$current_duplicate_subscription->get_notes();
                            $current_subscription->set_notes($new_notes_field);
                            $current_subscription->save();
                        }
                    }
                    // infine, elimino la subscription duplicata:
                    $this->info('Deleting the duplicate subscription with id='.$current_duplicate_subscription->get_id().'...');
                    Subscription::destroy($current_duplicate_subscription->get_id());
                }
            }
        }
        // una volta arrivato qui, avrò ancora sul db le subscription_snapshots collegate ai contratti che ho eliminato:
        // compatto le socket_snapshots, eliminando quelle non più necessarie per velocizzare la prossima operazione:
        // TODO: al momento evito di farlo, perchè questo comando consuma troppa memoria e finisce per crashare
        /*
        $this->info('Deleting unnecessary socket snapshots...');
        Artisan::call('SocketSnapshot:DeleteUnnecessary');
        */

        // e lancio il comando che svuota la tabella delle subscription_snapshots e la ricostruisce basandosi sulle socket_snapshots e sulle subscription che sono rimaste sul db:
        $this->info('Re-generating subscription snapshots...');
        Artisan::call('SubscriptionSnapshots:generate');
        $this->info('Done.');
    }
}
