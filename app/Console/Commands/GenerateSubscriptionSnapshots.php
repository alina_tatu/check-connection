<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SocketSnapshot;
use App\SubscriptionSnapshot;

class GenerateSubscriptionSnapshots extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SubscriptionSnapshots:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Svuota la tabella subscription_snapshots e la rigenera partendo dai dati di socket_snapshots e subscriptions';
    private $processed_socket_snapshots_count = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        SubscriptionSnapshot::truncate();
        $all_socket_snapshots = SocketSnapshot::orderBy('timestamp', 'asc');
        $all_socket_snapshots_count = $all_socket_snapshots->count();
        $all_socket_snapshots->chunk(200, function ($socket_snapshots) use($all_socket_snapshots_count) {
            foreach($socket_snapshots as $current_socket_snapshot) {
                $current_socket_snapshot->generate_and_write_subscription_snapshot_record();
                $this->processed_socket_snapshots_count++;
                $processed_socket_snapshots_percentage = round($this->processed_socket_snapshots_count * 100 / $all_socket_snapshots_count, 3);
                $this->info('Processed '.$this->processed_socket_snapshots_count.' socket_snapshots out of '.$all_socket_snapshots_count.' ('.$processed_socket_snapshots_percentage.'%)');
            }
        });
    }
}
