<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Carbon;
use App\Console\Commands\DeleteUnnecessarySocketSnapshots;


class Kernel extends ConsoleKernel {
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        DeleteUnnecessarySocketSnapshots::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        // in questo modo posso vedere la sezione Metrics di Horizon:
        $schedule->command('horizon:snapshot')
                 ->everyFiveMinutes();


        // con questo, elimino periodicamente le socket_snapshots inutili dal db:
        // lo faccio due volte al giorno, agli orari definiti nei parametri di configurazione dell'impianto
        $delete_unnecessary_socket_snapshot_daily_hour_1 = intval(Carbon::createFromFormat('H', 
                                                                                           config('marina.delete_unnecessary_socket_snapshots_daily_hour_1'), 
                                                                                           config('dates_currency_and_locale.local_timezone'))
                                                                        ->tz(config('app.timezone'))
                                                                        ->format('H'));
        $delete_unnecessary_socket_snapshot_daily_hour_2 = intval(Carbon::createFromFormat('H', 
                                                                                           config('marina.delete_unnecessary_socket_snapshots_daily_hour_2'), 
                                                                                           config('dates_currency_and_locale.local_timezone'))
                                                                        ->tz(config('app.timezone'))
                                                                        ->format('H'));
        $schedule->command('SocketSnapshot:DeleteUnnecessary 500 --scheduled')
                 ->twiceDaily($delete_unnecessary_socket_snapshot_daily_hour_1, $delete_unnecessary_socket_snapshot_daily_hour_2)
                 ->runInBackground()
                 ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
