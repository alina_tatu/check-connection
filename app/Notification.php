<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use DateTimeInterface;


/**
 * App\Notification
 *
 * @property int $id
 * @property string $type
 * @property string $notifiable_type
 * @property int $notifiable_id
 * @property string $data
 * @property Carbon|null $read_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Notification disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Notification newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Notification newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Notification query()
 * @method static \Illuminate\Database\Eloquent\Builder|Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notification whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notification whereNotifiableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notification whereNotifiableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notification whereReadAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notification whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notification withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Notification extends Model {
    use Cachable;
    use Traits\TimeZoneAware;

    protected $table = 'notifications';
    protected $primaryKey = 'id';

    public $timestamps = true;
    protected $dates = ['created_at',
                        'updated_at',
                        'read_at'];
    public $dateFormat = 'Y-m-d H:i:s';
    
    protected function serializeDate(DateTimeInterface $date) {
    // metodo per mantenere retrocompatibilità dopo l'upgrade a Laravel 7 (v. https://laravel.com/docs/7.x/upgrade#date-serialization)
        return $date->format('Y-m-d H:i:s');
    }

    // TODO: ma questa roba che segue mi serve ancora? Credo di sì, ma verificare. Peraltro - in entrambi i metodi setCreatedAtAttribute() e setUpdatedAtAttribute - il parametro $value non viene utilizzato.

    public function setCreatedAtAttribute($value) {
    // questo dovrebbe aggiornare automaticamente il campo created_at all'atto dell'insert di un record
        $this->attributes['created_at'] = Carbon::now(config('app.timezone'));
    }

    public function setUpdatedAtAttribute($value) {
    // questo dovrebbe aggiornare automaticamente il campo updated_at all'atto dell'update di un record
        $this->attributes['updated_at'] = Carbon::now(config('app.timezone'));
    }
}
