<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use App\Subscription;
use App\Boat;
use App\Action;
use App\SubscriptionConsumptionPayment;
// use GeneaLabs\LaravelModelCaching\Traits\Cachable;            fare il caching del model User è sconsigliato da GeneaLabs: v. https://github.com/GeneaLabs/laravel-model-caching
use DateTimeInterface;
use Illuminate\Support\Facades\Log;

/**
 * App\User
 *
 * @property int $id
 * @property string $last_name
 * @property string|null $first_name
 * @property string|null $username
 * @property string|null $email
 * @property string|null $phone
 * @property Carbon|null $email_verified_at
 * @property string|null $password
 * @property string|null $notes
 * @property string|null $language
 * @property int $tresholds_in_ampere
 * @property int $access_level
 * @property int $is_disabled
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Boat|null $boat
 * @property-read \Illuminate\Database\Eloquent\Collection|SubscriptionConsumptionPayment[] $insertedPayments
 * @property-read int|null $inserted_payments_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Action[] $requestedActions
 * @property-read int|null $requested_actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @method static \Illuminate\Database\Eloquent\Builder|User excludeSupervisorUsers()
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAccessLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsDisabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTresholdsInAmpere($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable {
    // use Cachable;            fare il caching del model User è sconsigliato da GeneaLabs: v. https://github.com/GeneaLabs/laravel-model-caching
    use Notifiable;
    use Traits\TimeZoneAware;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $primaryKey = 'id';

    public $timestamps = true;
    protected $dates = ['created_at',
                        'updated_at',
                        'email_verified_at'];
    public $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name',
        'first_name',
        'email',
        'username',
        'access_level',
        'is_disabled',
        'notes',
        'language',
        'tresholds_in_ampere',
        'token_for_actions_on_sockets',
        'needs_password_change',
        'password_changed_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // valori dei livelli di accesso:
    const SIMPLE_USER_ACCESS_LEVEL = 0;
    const VIEWER_ADMIN_USER_ACCESS_LEVEL = 0.5;
    const FULL_ADMIN_USER_ACCESS_LEVEL = 1;
    const SUPERVISOR_USER_ACCESS_LEVEL = 2;

    const GET_USER_SUBSCRIPTION_FLAG_ALL = 0;
    const GET_USER_SUBSCRIPTION_FLAG_CURRENT = 1;
    const GET_USER_SUBSCRIPTION_FLAG_ENDED = 2;

    // TODO: manca la gestione del campo regDT ed i relativi getter/setter

    protected function serializeDate(DateTimeInterface $date) {
    // metodo per mantenere retrocompatibilità dopo l'upgrade a Laravel 7 (v. https://laravel.com/docs/7.x/upgrade#date-serialization)
        return $date->format('Y-m-d H:i:s');
    }

    public function get_id(): int {
        return intval($this->attributes[$this->primaryKey]);
    }

    public function set_id(int $id) {
        $this->attributes[$this->primaryKey] = $id;
    }

    public function get_local_software_id(): int {
        return intval($this->local_software_id);
    }

    public function set_local_software_id(int $value) {
        $this->local_software_id = $value;
    }

    public static function find_by_local_software_id(int $value) {
        return self::where('local_software_id', $value)->first();
    }

    public function get_first_name(): ?string {
        return $this->first_name;
    }

    public function set_first_name(string $value) {
        $this->first_name = $value;
    }

    public function get_last_name(): string {
        return $this->last_name;
    }

    public function set_last_name(string $value) {
        $this->last_name = $value;
    }

    public function get_full_name(): string {
    // restituisce cognome e nome separati da uno spazio. NOTA: non include lo username.
        return strval($this->last_name.' '.$this->first_name);
    }

    public function get_username(): ?string {
        return $this->username;
    }

    public function set_username(string $value = null) {
        $this->username = $value;
    }

    public function is_cloud_user(): bool {
        return !is_null($this->username);
    }

    public function get_hashed_password(): ?string {
        return $this->password;
    }

    public function set_hashed_password(string $hashed_password = null) {
        $this->password = $hashed_password;
    }

    public function get_access_level(): float {
        return floatval($this->access_level);
    }

    public function set_access_level(float $value) {
        $this->access_level = $value;
    }

    public function get_email_address(): ?string {
        return $this->email;
    }

    public function set_email_address(string $value = null) {
        // TODO: per il momento evito di validarlo lato server perchè tanto non gestisco eventuali errori nel codice chiamante
        /*
        if(is_null($value) || (filter_var($value, FILTER_VALIDATE_EMAIL))) {
            $this->email = $value;
        }
        */
        $this->email = $value;
    }

    public function get_phone_number(): ?string {
        return $this->phone;
    }

    public function set_phone_number(string $value = null) {
        $this->phone = $value;
    }

    public function get_notes(): ?string {
        return $this->notes;
    }

    public function set_notes(string $value = null) {
        if(!is_null($value)) {
            $value = trim($value);
        }
        $this->notes = $value;
    }

    public function get_language_preference(): ?string {
        return $this->language;
    }

    public function set_language_preference(string $value = null) {
        $this->language = $value;
    }

    public static function generate_token_for_actions_on_sockets() {
    // genera un token diverso da tutti quelli già esistenti, che verrà utilizzato per spegnere/resettare le prese
        $existing_users_tokens = User::all()
                                     ->pluck('token_for_actions_on_sockets')
                                     ->toArray();
        do {
            $token_to_assign = Str::random(100);
            $this_token_already_exists = false;
            foreach($existing_users_tokens as $current_existing_token) {
                if(strcmp($current_existing_token, $token_to_assign) === 0) {
                    $this_token_already_exists = true;
                }
            }

        } while($this_token_already_exists === true);
        return $token_to_assign;
    }

    public function get_token_for_actions_on_sockets(): string {
        return $this->token_for_actions_on_sockets;
    }

    public function set_token_for_actions_on_sockets(string $value) {
        $this->token_for_actions_on_sockets = $value;
    }

    public function wants_power_alarm_thresholds_in_ampere(): bool {
        return boolval($this->tresholds_in_ampere);
    }

    public function set_wants_power_alarm_thresholds_in_ampere(bool $boolvalue) {
        $this->tresholds_in_ampere = $boolvalue;
    }

    public function is_full_admin(): bool {
        if($this->get_access_level() >= self::FULL_ADMIN_USER_ACCESS_LEVEL) {
            return true;
        }
        return false;
    }

    public function is_viewer_admin(): bool {
        if($this->get_access_level() >= self::VIEWER_ADMIN_USER_ACCESS_LEVEL) {
            return true;
        }
        return false;
    }

    public function is_supervisor(): bool {
        if($this->get_access_level() >= self::SUPERVISOR_USER_ACCESS_LEVEL) {
            return true;
        }
        return false;
    }

    public function set_as_disabled(bool $value) {
        $this->is_disabled = $value;
    }

    public function is_disabled(): bool {
        return boolval($this->is_disabled);
    }

    public function login_is_protected(): bool {
        return boolval($this->protect_login_against_username_deletion);
    }

    public function get_created_at_timestamp(): Carbon {
        return $this->ConvertToLocalTimeZone($this->created_at);
    }

    public function set_created_at_timestamp(Carbon $timestamp): bool {
        $timestamp->setTimezone(config('app.timezone'));
        $this->created_at = $this->ConvertToDefaultDatabaseTimeZone($timestamp);
        return true;
    }

    public function get_updated_at_at_timestamp(): ?Carbon {
        return $this->ConvertToLocalTimeZone($this->updated_at);
    }

    public function set_updated_at_timestamp(Carbon $timestamp = null): bool {
        if(!is_null($timestamp)) {
            $timestamp->setTimezone(config('app.timezone'));
        }
        $this->updated_at = $this->ConvertToDefaultDatabaseTimeZone($timestamp);
        return true;
    }

    public function needs_password_change(): bool {
        return boolval($this->needs_password_change);
    }

    public function set_needs_password_change_field(bool $value): bool {
        $this->needs_password_change = $value;
        return true;
    }

    public function get_password_changed_at_timestamp(): ?Carbon {
        if(!is_null($this->password_changed_at)) {
            return $this->ConvertToLocalTimeZone($this->password_changed_at);
        }
        return null;
    }
        
    public function set_password_changed_at_timestamp(Carbon $timestamp = null): bool {
        if(!is_null($timestamp)) {
            $timestamp->setTimezone(config('app.timezone'));
            $this->password_changed_at = $this->ConvertToDefaultDatabaseTimeZone($timestamp);
        }
        return true;
    }

    public function boat() {
        return $this->hasOne(Boat::class);
    }

    public function owns_susbscription(Subscription $subscription) {
    // restituisce true se la subscription passata è in corso ed appartiene all'utente, false in caso contrario
        if((!$subscription->user) || ($subscription->user->count() === 0)) {
            return false;
        }
        if($subscription->user->get_id() !== $this->get_id()) {
            return false;
        }
        $now = Carbon::now(config('app.timezone'));
        $subscription_begin_dt = $subscription->get_begin_timestamp();
        if($subscription_begin_dt > $now) {
            return false;
        }
        $subscription_end_dt = $subscription->get_end_timestamp();
        if(($subscription_end_dt) && ($subscription_end_dt < $now)) {
            return false;
        }
        return true;
    }

    public function subscriptions() {
        return $this->hasMany(Subscription::class);
    }

    public function get_subscriptions(int $flag) {
        // TODO: questo è un metodo normale. Creo una relationship? Ma in quel caso come faccio a passarle il parametro quando la chiamo, dato che non vanno usate le parentesi?
        $subscriptions_from_db = $this->subscriptions();
        switch($flag) {
            case self::GET_USER_SUBSCRIPTION_FLAG_ALL:
                // qui non faccio nulla: non devo filtrare ulteriormente i dati presenti in $subscriptions_from_db
                break;
            case self::GET_USER_SUBSCRIPTION_FLAG_CURRENT:
                $subscriptions_from_db = $subscriptions_from_db->whereNull('end_dt')
                                                               ->newerFirst();
                break;
            case self::GET_USER_SUBSCRIPTION_FLAG_ENDED:
                $subscriptions_from_db = $subscriptions_from_db->whereNotNull('end_dt')
                                                               ->newerFirst();
                break;
        }
        return $subscriptions_from_db->get();
    }

    public function has_already_received_notification(string $type, string $object_id): bool {
    // restituisce true se l'utente ha già ricevuto una notifica contenente il type e l'object_id passati che non sia ancora stata chiusa (risolta), false altrimenti
        // TODO: 07/07/2020: non sta funzionando! mi restituisce sempre false, e di conseguenza ogni notifica viene reinviata di continuo perchè risulta che l'utente non l'abbia ancora ricevuta. Ho fatto delle prove anche con tinker: se chiamo il metodo ottengo false, se però lancio direttamente la query con lo stesso $type e lo stesso $object_id, le notifiche ci sono. Per ora provo a sostituire la chiamata a questo metodo con la chiamata esplicita alla query nell'APIv0Controller, in attesa di risolvere.
        $notifications_with_same_data =  $this->notifications()
                                              ->where('type', $type)
                                              ->where('data->object_id', $object_id)
                                              ->whereNull('data->closed_at')
                                              ->get();
        if($notifications_with_same_data->count() > 0) {
            return true;
        }
        return false;
    }

    public function insertedPayments() {
        return $this->hasMany(SubscriptionConsumptionPayment::class, 'operator_id');
    }

    public function requestedActions() {
        return $this->hasMany(Action::class, 'requesting_user_id');
    }

    public function scopeExcludeSupervisorUsers($query) {
        $query->where('access_level', '<', self::SUPERVISOR_USER_ACCESS_LEVEL);
    }
}
