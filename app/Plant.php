<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\Plant
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Plant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plant query()
 * @mixin \Eloquent
 */
class Plant extends Model {
    // TODO: spostare qui dentro i metodi degli altri model che non hanno strettamente a che fare con la classe in cui si trovano, ma che si adattano meglio a questo?

    public static function get_all_pontoons() {
        $pontoons_collection = collect();
        $pontoons = Berth::select('pontoon_name')
                         ->distinct()
                         ->orderBy('pontoon_name')
                         ->get();
        if(($pontoons) && ($pontoons->count() > 0)) {
            $pontoons_collection = collect();
            foreach($pontoons as $pontoon) {
                $pontoons_collection->push($pontoon->pontoon_name);
            }
        }
        return $pontoons_collection;
    }

    public static function get_all_pedestals() {
        return Pedestal::all();
    }

    public static function get_all_berths() {
        $berths = Berth::with('currently_bound_subscription')->get();
        return $berths;
    }

    public static function get_all_berths_in_pontoon($pontoon) {
        return Berth::where('pontoon_name', $pontoon)->get();
    }

    public static function get_user_by_full_name($full_name) {
        $user = collect();
        foreach(Plant::get_all_users() as $current_user) {
            if($current_user->get_full_name() === $full_name) {
                $user = $current_user;
            }
        }
        return $user;
    }

    public static function get_all_users(bool $exclude_supervisor_users = null) {
        $logged_user = Auth::user();
        if((!Auth::check()) ||
           ($logged_user->is_supervisor() && (is_null($exclude_supervisor_users) || (!$exclude_supervisor_users)))) {
        // se non c'è un utente loggato (quindi se sto chiamando questo metodo da una API) oppure se l'utente loggato è un supervisore e non mi ha chiesto espressamente di tralasciare gli utenti supervisori:
            // restituisco tutti gli utenti
            return User::all();
        } else {
        // se l'utente loggato NON è un supervisore, oppure se lo è ma ha chiamato il metodo con $exclude_supervisor_users === true
            // restituisco solo gli utenti standard e admin (sia full_ che viewer_):
            return User::where('access_level', '<', User::SUPERVISOR_USER_ACCESS_LEVEL)->get();

            // TODO: dovrei provare con gli scope, ma questo al momento non funziona:
            // return User::excludeSupervisorUsers()->get();
        }
    }

    public static function get_all_boats(bool $exclude_supervisor_users_boats = null) {
        if((!Auth::check()) ||
           (Auth::user()->is_supervisor() && (is_null($exclude_supervisor_users_boats) || (!$exclude_supervisor_users_boats)))) {
        // se l'utente loggato è un supervisore e non mi ha chiesto espressamente di tralasciare le barche degli utenti supervisori:
            // restituisco tutte le barche
            $boats = Boat::all();
        } else {
        // se l'utente loggato NON è un supervisore, oppure se lo è ma ha chiamato il metodo con $exclude_supervisor_users === true
            // restituisco solo le barche degli utenti standard e admin (sia full_ che viewer_):
            $non_supervisor_user_ids = User::excludeSupervisorUsers()
                                           ->pluck('id')
                                           ->toArray();
            $boats = Boat::whereIn('user_id', $non_supervisor_user_ids)->get();

        }
        /* modifico le keys della collection $boats in modo che l'indice di ogni barca sia uguale all'id dell'utente al quale la barca appartiene, così sulla view posso recuperare la barca dell'utente $user cercando $boats[$user->id] (ATTENZIONE: in questo modo, nessun utente potrà avere più di una barca, perchè la seconda barca sovrascriverebbe la prima. Al momento in cui scrivo non è un problema: non è previsto che gli utenti abbiano più di una barca): */
        $boats = $boats->keyBy(function ($value, $key) {
            $key = $value->user_id;
            return $key;
        });
        return $boats;
    }

    public static function get_all_subscriptions(bool $exclude_supervisor_users_subscriptions = null) {
        if((!Auth::check()) ||
           (Auth::user()->is_supervisor() && (is_null($exclude_supervisor_users_subscriptions) || ($exclude_supervisor_users_subscriptions === false)))) {
        // se non c'è un utente loggato (quindi se sto chiamando questo metodo da una API) oppure se l'utente loggato è un supervisore e non mi ha chiesto espressamente di tralasciare le subscription collegate agli utenti supervisori:
            // restituisco tutte le subscriptions (faccio l'eager loading delle relazioni per evitare tempi biblici di caricamento)
            $subscriptions = Subscription::with('user', 'berth')->get();
        } else {
        // se l'utente loggato NON è un supervisore, oppure se lo è ma ha chiamato il metodo con $exclude_supervisor_users_subscriptions === true:
            // restituisco solo le subscriptions collegate agli utenti standard e admin (sia full_che viewer_). Faccio l'eager loading delle relazioni per evitare tempi biblici di caricamento:
            $subscriptions = Subscription::whereHas('user', function ($query) { return $query->where('access_level', '<', User::SUPERVISOR_USER_ACCESS_LEVEL); })
                                         ->with('user', 'berth')
                                         ->get();
            // TODO: dovrei provare con gli scope, ma questo al momento non funziona:
            //$subscriptions = Subscription::excludeSupervisorRelatedSubscriptions()->with('user', 'berth')->get();
        }
        return $subscriptions;
    }

    public static function get_all_current_subscriptions(bool $exclude_supervisor_users_subscriptions = null) {
    // TODO: aggiungere un parametro facoltativo $timestamp di tipo Carbon, per poter recuperare le subscription attive in un determinato momento, all'occorrenza?
        if((!Auth::check()) ||
           (Auth::user()->is_supervisor() && (is_null($exclude_supervisor_users_subscriptions) || ($exclude_supervisor_users_subscriptions === false)))) {
        // se non c'è un utente loggato (quindi se sto chiamando questo metodo da una API) oppure se l'utente loggato è un supervisore e non mi ha chiesto espressamente di tralasciare le subscription collegate agli utenti supervisori:
            // restituisco tutte le subscriptions
            $all_current_subscriptions = Subscription::active();
        } else {
        // altrimenti:
            // restituisco solo le subscriptions collegate agli utenti standard e admin (sia full_ che viewer_):
            $all_current_subscriptions = Subscription::active()
                                                     ->excludeSupervisorRelatedSubscriptions();
        }
        return $all_current_subscriptions->get();
    }

    public static function get_all_transponders() {
        return Transponder::all();
    }

    public static function get_open_notifications_by_object_name(string $object_name) {
        $notifications = Notification::whereJsonContains('object_name', $object_name)
                                     ->whereJsonContains('closed_at', null)
                                     ->orderBy('created_at', 'ASC')
                                     ->get();
        return $notifications;
    }

    public static function get_pending_actions() {
    // restituisce tutte le azioni non ancora eseguite
        return Action::whereNull('completion_timestamp')->get();
    }

    public static function get_available_socket_types() {
        return Berth::pluck('socket_type')->unique()->toArray();
    }
}
