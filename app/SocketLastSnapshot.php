<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\SocketLastSnapshot
 *
 * @property int $id
 * @property int $socket_id
 * @property Carbon $timestamp
 * @property string $link_status
 * @property string $on_off_status
 * @property string $total_power_cnt
 * @property string $postpaid_power_cnt
 * @property string $total_water_cnt
 * @property string $postpaid_water_cnt
 * @property string $transponder_id
 * @property string $residual_credit
 * @property int $epower_id
 * @property string $instant_power_consumption
 * @property string|null $alarms
 * @property int $keep_snapshot_flag
 * @property int $keep_next_snapshot_flag
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SocketLastSnapshot newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SocketLastSnapshot newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SocketLastSnapshot query()
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereAlarms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereEpowerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereInstantPowerConsumption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereKeepNextSnapshotFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereKeepSnapshotFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereLinkStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereOnOffStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot wherePostpaidPowerCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot wherePostpaidWaterCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereResidualCredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereSocketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereTotalPowerCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereTotalWaterCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot whereTransponderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketLastSnapshot withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 * @property-read \App\Berth $berth
 * @property-read \App\Transponder $transponder
 */
class SocketLastSnapshot extends SocketSnapshot {
    use Cachable;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'socket_last_snapshots';

    public function __construct() {
        parent::__construct();
    }

}
