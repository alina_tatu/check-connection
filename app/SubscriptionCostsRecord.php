<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use DateTimeInterface;

/**
 * App\SubscriptionCostsRecord
 *
 * @property int $id
 * @property int $subscription_id
 * @property string $power_cost_per_kwh
 * @property string $water_cost_per_m3
 * @property Carbon $begin_dt
 * @property Carbon|null $end_dt
 * @property-read \App\Subscription $subscription
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionCostsRecord disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionCostsRecord newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionCostsRecord newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionCostsRecord query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionCostsRecord whereBeginDt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionCostsRecord whereEndDt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionCostsRecord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionCostsRecord wherePowerCostPerKwh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionCostsRecord whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionCostsRecord whereWaterCostPerM3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionCostsRecord withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class SubscriptionCostsRecord extends Model {
    use Cachable;
    use Traits\TimeZoneAware;

    protected $table = 'subscription_costs';
    protected $primaryKey = 'id';

    public $timestamps = true;
    const UPDATED_AT = null;
    const CREATED_AT = null;
    protected $dates = ['begin_dt', 'end_dt'];
    public $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subscription_id',
        'begin_dt',
        'end_dt',
        'power_cost_per_kwh',
        'water_cost_per_m3',
    ];

    protected function serializeDate(DateTimeInterface $date) {
    // metodo per mantenere retrocompatibilità dopo l'upgrade a Laravel 7 (v. https://laravel.com/docs/7.x/upgrade#date-serialization)
        return $date->format('Y-m-d H:i:s');
    }

    public function get_begin_dt(): Carbon {
        return $this->ConvertToLocalTimeZone($this->begin_dt);
    }

    public function set_begin_dt(Carbon $begin_dt): bool {
        $begin_dt->setTimezone(config('app.timezone'));
        $this->begin_dt = $this->ConvertToDefaultDatabaseTimeZone($begin_dt);
        return true;
    }

    public function get_end_dt(): ?Carbon {
        $end_dt_to_return = null;
        if(!is_null($this->end_dt)) {
            $end_dt_to_return = $this->ConvertToLocalTimeZone($this->end_dt);
        }
        return $end_dt_to_return;
    }

    public function set_end_dt(Carbon $end_dt = null): bool {
        if(!is_null($end_dt)) {
            $end_dt->setTimezone(config('app.timezone'));
        }
        $this->end_dt = $this->ConvertToDefaultDatabaseTimeZone($end_dt);
        return true;
    }
    
    public function subscription() {
        return $this->belongsTo(Subscription::class);
    }

    public function set_subscription_id(int $value = null) {
        $this->subscription_id = $value;
        return true;
    }

    public function get_power_cost_per_kwh(): ?float {
        return $this->power_cost_per_kwh;
    }

    public function set_power_cost_per_kwh(float $value = null) {
        $this->power_cost_per_kwh = $value;
        return true;
    }

    public function get_water_cost_per_m3(): ?float {
        return $this->water_cost_per_m3;
    }

    public function set_water_cost_per_m3(float $value = null) {
        $this->water_cost_per_m3 = $value;
        return true;
    }

    public function is_equal_to(SubscriptionCostsRecord $other_costs_record): bool {
        $costs_record_are_equal = false;
        if(($other_costs_record) && ($other_costs_record->count() > 0)) {
            $costs_record_are_equal = ($this->subscription->get_id() === $other_costs_record->subscription->get_id());      // TODO: se per disgrazia la subscription non esiste piu sul db, questo crasha
            $costs_record_are_equal = $costs_record_are_equal &&  ($this->get_power_cost_per_kwh() === $other_costs_record->get_power_cost_per_kwh());
            $costs_record_are_equal = $costs_record_are_equal && ($this->get_water_cost_per_m3() === $other_costs_record->get_water_cost_per_m3());
        }
        return $costs_record_are_equal;
    }
}
