<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use DateTimeInterface;

/**
 * App\SubscriptionAlarmThresholdsRecord
 *
 * @property int $id
 * @property int $subscription_id
 * @property string $min_power_threshold_kwh
 * @property string $max_power_threshold_kwh
 * @property int $max_water_tap_time_threshold_minutes
 * @property string $min_credit_threshold
 * @property Carbon $begin_dt
 * @property Carbon|null $end_dt
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionAlarmThresholdsRecord newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionAlarmThresholdsRecord newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SubscriptionAlarmThresholdsRecord query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord whereBeginDt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord whereEndDt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord whereMaxPowerThresholdKwh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord whereMaxWaterTapTimeThresholdMinutes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord whereMinCreditThreshold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord whereMinPowerThresholdKwh($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionAlarmThresholdsRecord withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class SubscriptionAlarmThresholdsRecord extends Model {
    use Cachable;
    use Traits\TimeZoneAware;

    protected $table = 'subscription_alarm_thresholds';
    protected $primaryKey = 'id';

    public $timestamps = true;
    const UPDATED_AT = null;
    const CREATED_AT = null;
    protected $dates = ['begin_dt', 'end_dt'];
    public $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subscription_id',
        'begin_dt',
        'end_dt',
        'min_power_threshold_kwh',
        'max_power_threshold_kwh',
        'max_water_tap_time_threshold_minutes',
        'min_credit_threshold',
    ];
    
    protected function serializeDate(DateTimeInterface $date) {
    // metodo per mantenere retrocompatibilità dopo l'upgrade a Laravel 7 (v. https://laravel.com/docs/7.x/upgrade#date-serialization)
        return $date->format('Y-m-d H:i:s');
    }

    public function get_id(): int {
        return intval($this->attributes[$this->primaryKey]);
    }

    public function set_id(int $value) {
        $this->attributes[$this->primaryKey] = $value;
    }

    public function get_begin_dt(): Carbon {
        return $this->ConvertToLocalTimeZone($this->begin_dt);
    }

    public function set_begin_dt(Carbon $begin_dt): bool {
        $begin_dt->setTimezone(config('app.timezone'));
        $this->begin_dt = $this->ConvertToDefaultDatabaseTimeZone($begin_dt);
        return true;
    }

    public function get_end_dt(): ?Carbon {
        $end_dt_to_return = null;
        if(!is_null($this->end_dt)) {
            $end_dt_to_return = $this->ConvertToLocalTimeZone($this->end_dt);
        }
        return $end_dt_to_return;
    }

    public function set_end_dt(Carbon $end_dt = null): bool {
        if(!is_null($end_dt)) {
            $end_dt->setTimezone(config('app.timezone'));
        }
        $this->end_dt = $this->ConvertToDefaultDatabaseTimeZone($end_dt);
        return true;
    }

    public function set_subscription_id(int $value = null) {
        $this->subscription_id = $value;
        return true;
    }

    public function get_min_power_threshold_kwh(): ?float {
        return $this->min_power_threshold_kwh;
    }

    public function set_min_power_threshold_kwh(float $value = null) {
        $this->min_power_threshold_kwh = $value;
        return true;
    }

    public function get_max_power_threshold_kwh(): ?float {
        return $this->max_power_threshold_kwh;
    }

    public function set_max_power_threshold_kwh(float $value = null) {
        $this->max_power_threshold_kwh = $value;
        return true;
    }

    public function get_max_water_tap_time_threshold_minutes(): ?int {
        return $this->max_water_tap_time_threshold_minutes;
    }

    public function set_max_water_tap_time_threshold_minutes(int $value = null) {
        $this->max_water_tap_time_threshold_minutes = $value;
        return true;
    }

    public function get_min_credit_threshold(): ?float {
        return $this->min_credit_threshold;
    }

    public function set_min_credit_threshold(float $value = null) {
        $this->min_credit_threshold = $value;
        return true;
    }

    public function is_equal_to(SubscriptionAlarmThresholdsRecord $other_thresholds_record): bool {
        $thresholds_records_are_equal = false;
        if(($other_thresholds_record) && ($other_thresholds_record->count() > 0)) {
            $thresholds_records_are_equal = ($this->subscription->get_id() === $other_thresholds_record->subscription->get_id());             // TODO: se per disgrazia la subscription non esiste piu sul db, questo crasha
            $thresholds_records_are_equal = $thresholds_records_are_equal && ($this->get_min_power_threshold_kwh() === $other_thresholds_record->get_min_power_threshold_kwh());
            $thresholds_records_are_equal = $thresholds_records_are_equal && ($this->get_max_power_threshold_kwh() === $other_thresholds_record->get_max_power_threshold_kwh());
            $thresholds_records_are_equal = $thresholds_records_are_equal && ($this->get_max_water_tap_time_threshold_minutes() === $other_thresholds_record->get_max_water_tap_time_threshold_minutes());
            $thresholds_records_are_equal = $thresholds_records_are_equal && ($this->get_min_credit_threshold() === $other_thresholds_record->get_min_credit_threshold());
        }
        return $thresholds_records_are_equal;
    }
}
