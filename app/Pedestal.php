<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Pedestal
 *
 * @property int $id
 * @property string $name
 * @property int $x_coord
 * @property int $y_coord
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Berth[] $berths
 * @property-read int|null $berths_count
 * @method static \Illuminate\Database\Eloquent\Builder|Pedestal disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Pedestal newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Pedestal newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Pedestal query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pedestal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pedestal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pedestal whereXCoord($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pedestal whereYCoord($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pedestal withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Pedestal extends Model {
    use Cachable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pedestals';

    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'x_coord', 'y_coord',
    ];

    public function get_id(): int {
        return intval($this->attributes[$this->primaryKey]);
    }

    public function set_id(int $id) {
        $this->attributes[$this->primaryKey] = intval($id);
    }

    public function get_name(): ?string {
        return $this->name;
    }

    public function set_name(string $value) {
        $this->name = $value;
    }

    public function get_x_coord(): ?float {
        return $this->x_coord;
    }

    public function set_x_coord(float $value) {
        $this->x_coord = $value;
    }

    public function get_y_coord(): ?float {
        return $this->y_coord;
    }

    public function set_y_coord(float $value) {
        $this->y_coord = $value;
    }

    public function berths() {
        return $this->hasMany(Berth::class);
    }
}
