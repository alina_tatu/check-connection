<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subscription;
use App\SocketSnapshot;
use Illuminate\Support\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Transponder
 *
 * @property int $id
 * @property string $code
 * @property int $used_for_socket_subscriptions
 * @property-read \Illuminate\Database\Eloquent\Collection|SocketSnapshot[] $socket_snapshots
 * @property-read int|null $socket_snapshots_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @method static \Illuminate\Database\Eloquent\Builder|Transponder disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Transponder newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Transponder newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Transponder query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transponder whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transponder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transponder whereUsedForSocketSubscriptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transponder withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 */
class Transponder extends Model {
    use Cachable;
    use Traits\TimeZoneAware;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transponders';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public const CODE_FOR_UNKNOWN_TRANSPONDER = '00000000';     // codice usato da smartview quando invia le SocketSnapshot per indicare un transponder_id sconosciuto (ad esempio perchè la presa è in errore comunicazione, è nuova o è appena stata resettata)

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'used_for_socket_subscriptions',
    ];

    public static function findByCode(string $code): ?Transponder {
        if($code !== self::CODE_FOR_UNKNOWN_TRANSPONDER) {
            return Transponder::where('code', $code)->first();
        }
        return null;
    }

    public function get_id(): int {
        return intval($this->attributes[$this->primaryKey]);
    }

    public function get_code(): String {
    // restituisce il codice del transponder
        return $this->code;
    }

    public function set_code(String $code): bool {
    // imposta il codice del transponder, se è compreso tra 1 e 8 caratteri. Restiutisce false in caso contrario.
        if((strlen($code) >= 1) && (strlen($code) <= 8)) {
            $this->code = $code;
            return true;
        } else {
            return false;
        }
    }

    public function exists_on_db(): bool {
        if(strlen(strval($this->code)) > 0) {
            $transponder_record = self::where('code', $this->code)
                                      ->first();
            if($transponder_record) {
                return true;
            }
        }
        return false;
    }

    public function set_as_used_for_socket_subscriptions(bool $boolvalue) {
    // se il parametro passato è true, setta il transponder come usato per creare delle subscriptions su presa. Se il parametro è true, lo setta come transponder standard (che viene affidato ai clienti)
        $this->used_for_socket_subscriptions = $boolvalue;
    }

    public function is_used_for_socket_subscriptions(): bool {
    // restituisce true se il transponder viene usato per creare delle subscriptions su presa, false in caso contrario
        return boolval($this->used_for_socket_subscriptions);
    }

    public function subscriptions() {
        return $this->hasMany(Subscription::class, 'transponder_id', 'code');
    }

    public function active_subscriptions(Carbon $timestamp = null) {
    // Se il parametro non viene passato, restituisce una collection di tutte le subscriptions attualmente attive per questo transponder. Altrimenti restituisce tutte le subscription attive sul transponder all'istante $timestamp.
        return $this->subscriptions()
                    ->active($timestamp);
    }

    public function active_subscriptions_in_interval(Carbon $start_dt, Carbon $end_dt) {
    // restituisce tutte le subscriptions che sono risultate attive durante l'intervallo compreso tra $start_dt e $end_dt, comprese quelle iniziate prima e/o finite dopo e iniziate prima o durante ed ancora in corso.
        $start_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
        return $this->subscriptions()->activeInInterval($start_dt, $end_dt);
    }         // TODO: dato che - avendo dei parametri - non è usabile come relationship (nel senso che non posso chiamarla senza parentesi), ha senso metterci un get() prima del return? Al momento, il get() lo fa il chiamante

    public function socket_snapshots() {
        return $this->hasMany(SocketSnapshot::class, 'transponder_id', 'code');
    }

    public function scopeUsedForSocketSubscriptions($query) {
        return $query->where('used_for_socket_subscriptions', '<>', 0)
                     ->whereNotNull('used_for_socket_subscriptions');
    }

    public function scopeNotUsedForSocketSubscriptions($query) {
        $query->where(function($query) {
                $query->where(function($nestedQuery) {
                        $nestedQuery->where('used_for_socket_subscriptions', 0)
                                    ->orWhereNull('used_for_socket_subscriptions');
                });
        });
    }
}
