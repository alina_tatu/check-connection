<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LostMoneyForResettedSockets extends Model {
    use HasFactory;

    public $timestamps = true;

    public function action() {
        return $this->belongsTo(Action::class);
    }

    public function socket_snapshot() {
        return $this->belongsTo(SocketSnapshot::class);
    }
}
