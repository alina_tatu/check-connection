<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\SocketLastSnapshot;

/**
 * App\SocketSnapshot
 *
 * @property int $id
 * @property int $socket_id
 * @property Carbon $timestamp
 * @property string $link_status
 * @property string $on_off_status
 * @property string $total_power_cnt
 * @property string $postpaid_power_cnt
 * @property string $total_water_cnt
 * @property string $postpaid_water_cnt
 * @property string $transponder_id
 * @property string $residual_credit
 * @property int $epower_id
 * @property string $instant_power_consumption
 * @property string|null $alarms
 * @property int $keep_snapshot_flag
 * @property int $keep_next_snapshot_flag
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SocketSnapshot newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SocketSnapshot newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|SocketSnapshot query()
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereAlarms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereEpowerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereInstantPowerConsumption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereKeepNextSnapshotFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereKeepSnapshotFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereLinkStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereOnOffStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot wherePostpaidPowerCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot wherePostpaidWaterCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereResidualCredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereSocketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereTotalPowerCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereTotalWaterCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot whereTransponderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SocketSnapshot withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 * @property-read \App\Berth $berth
 * @property-read \App\Transponder $transponder
 */
class SocketSnapshot extends BaseSocketSnapshot {
    use Cachable;
    use SoftDeletes;

    protected $dates = ['timestamp', 'deleted_at'];

    public static function create_instance_for_socket_last_snapshot_table_and_delete_old_socket_last_snapshots_for_same_sockets($snapshot_instance): SocketLastSnapshot {
        $snapshot_instance_on_socket_last_snapshots_table = new SocketLastSnapshot;
        $previous_last_snapshot_instance_for_the_same_socket = SocketLastSnapshot::where('socket_id', $snapshot_instance->socket_id)->get();
        $snapshot_instance_on_socket_last_snapshots_table->socket_id = $snapshot_instance->socket_id;
        $snapshot_instance_on_socket_last_snapshots_table->timestamp = $snapshot_instance->timestamp;
        $snapshot_instance_on_socket_last_snapshots_table->link_status = $snapshot_instance->link_status;
        $snapshot_instance_on_socket_last_snapshots_table->on_off_status = $snapshot_instance->on_off_status;
        $snapshot_instance_on_socket_last_snapshots_table->transponder_id = $snapshot_instance->transponder_id;
        $snapshot_instance_on_socket_last_snapshots_table->subscription_id = $snapshot_instance->subscription_id;
        $snapshot_instance_on_socket_last_snapshots_table->can_retrieve_subscription_via_subscription_id = $snapshot_instance->can_retrieve_subscription_via_subscription_id;
        $snapshot_instance_on_socket_last_snapshots_table->total_power_cnt = $snapshot_instance->total_power_cnt;
        $snapshot_instance_on_socket_last_snapshots_table->total_water_cnt = $snapshot_instance->total_water_cnt;
        $snapshot_instance_on_socket_last_snapshots_table->postpaid_power_cnt = $snapshot_instance->postpaid_power_cnt;
        $snapshot_instance_on_socket_last_snapshots_table->postpaid_water_cnt = $snapshot_instance->postpaid_water_cnt;
        $snapshot_instance_on_socket_last_snapshots_table->residual_credit = $snapshot_instance->residual_credit;
        $snapshot_instance_on_socket_last_snapshots_table->instant_power_consumption = $snapshot_instance->instant_power_consumption;
        $snapshot_instance_on_socket_last_snapshots_table->epower_id = $snapshot_instance->epower_id;
        $snapshot_instance_on_socket_last_snapshots_table->alarms = $snapshot_instance->alarms;
        $snapshot_instance_on_socket_last_snapshots_table->save();
        if((!is_null($previous_last_snapshot_instance_for_the_same_socket)) && ($previous_last_snapshot_instance_for_the_same_socket->count() > 0)) {
            SocketLastSnapshot::whereIn('id', $previous_last_snapshot_instance_for_the_same_socket->pluck('id'))->forceDelete();
        }
        return $snapshot_instance_on_socket_last_snapshots_table;
    }
}
