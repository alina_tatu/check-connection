<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\User;

class UsernameToRegisterAlreadyHasAPassword implements Rule {
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value) {
        $user = User::where($attribute, $value)->first();
        if(($user) && ($user->count() > 0)) {
            return is_null($user->get_hashed_password());
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return ucfirst(__('auth.signup_page_validation_error_username_already_registered', ['password_reset_url' => route('password.reset', ['token' => csrf_token()])]));
    }
}
