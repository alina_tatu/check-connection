<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Carbon;

class SocketMaxPowerConsumptionAlarmNotification extends SmartCloudNotification {
    use Queueable;

    public const NOTIFICATION_OBJECT_TYPE_FOR_DB = 'SOCKET';

    public function __construct(Carbon $created_at,
                                int $object_id,
                                string $object_name,
                                Carbon $read_at = null,
                                Carbon $closed_at = null) {
        parent::__construct($created_at, $object_id, $object_name, $read_at, $closed_at);
        $this->object_type = $this::NOTIFICATION_OBJECT_TYPE_FOR_DB;
        $this->type = self::class;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
    /* TODO: qui devo creare modelli di messaggio diversi:
        a) se l'utente destinatario ($notifiable) è un admin e non è l'intestatario della subscription (come faccio a capirlo? posso passare un secondo parametro di tipo bool al metodo toMail()? credo di sì) devo usare un modello
        b) se l'utente destinatario ($notifiable) non è un admin ed è l'intestatario della subscription, userò un altro modello di notifica
        c) se l'utente destinatario ($notifiable) è sia un admin, sia l'intestatario della subscription, forse devo mandargli un terzo modello di notifica. E soprattutto, in questi casi devo evitare di mandargliene due! (lo evito qui o nel chiamante? o meglio controllare in entrambi?)

        per ora scrivo una mail in testo classico uguale in ogni caso, per riprodurre intanto il funzionamento di SmartCloud 1 */
        if(!($notifiable->is_disabled())) {
            if(!is_null($notifiable->get_language_preference())) {
            // per la traduzione del testo della notifica, leggo la lingua preferita dall'utente:
                $language = $notifiable->get_language_preference();
            } else {
            // se l'utente non ha settato una preferenza:
                // uso la lingua di default della webapp:
                $language = Lang::getLocale();
            }
            App::setLocale($language);
            $notification_timestamp = $this->get_created_at_timestamp();
            $formatted_date = $notification_timestamp->format(config('dates_currency_and_locale.local_date_format_full_carbon'));
            $formatted_time = $notification_timestamp->format(config('dates_currency_and_locale.local_time_format_full_carbon'));
            return (new MailMessage)
                        ->line(ucfirst(__('notifications/mail.greetings', ['sender_name' => config('app.name'),
                        												   'recipient_name' => $notifiable->get_full_name() ])))
                        ->line(ucfirst(__('notifications/mail.max_power_consumption_alarm_message', ['formatted_date' => $formatted_date,
                                                                                                     'formatted_time' => $formatted_time,
                                                                                                     'berth_description' => $this->object_name,
                                                                                                     'plant_name' => config('marina.name')])))
                        ->line('')
                        ->line(ucfirst(__('notifications/mail.do_not_reply')));
        }
    }

    public function toArray($notifiable) {
        return parent::toArray($notifiable);
    }

    public function via($notifiable) {
        if(!is_null($notifiable->get_username()) && !is_null($notifiable->get_hashed_password()) && !is_null($notifiable->get_email_address()) && !($notifiable->is_disabled())) {
        /* invio la notifica all'utente solamente se:
            - ha uno username (quindi è un utente cloud)
            - ha una password (quindi ha già effettuato la registrazione)
            - ha un indirizzo email settato
            - non ha un account disabilitato
        */
            // le notifiche di questo tipo vengono inviate sia in-app, sia via mail:
            return ['database', 'mail'];
        }
        return [];
    }
}
