<?php

namespace App\Notifications;

use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;

abstract class SmartCloudNotification extends Notification {
    use \App\Traits\TimeZoneAware;

    protected $casts = [
        'data' => 'array',
    ];

    public const NOTIFICATION_TYPE_STRING_USED_BY_SMARTVIEW_TO_CLOSE_ERRORS = 'ERROR_CLOSING';

    protected $created_at;
    protected $object_id;
    protected $type;
    protected $object_type;
    protected $object_name;
    protected $text;
    protected $read_at;
    protected $closed_at;

    public function get_created_at_timestamp(): Carbon {
    // restituisce il timestamp nel quale la notifica è stata creata
        return $this->ConvertToLocalTimeZone($this->created_at);
    }

    public function set_created_at_timestamp(Carbon $created_at): bool {
    // imposta il timestamp al quale la notifica è stata creata
        $this->created_at = $this->ConvertToDefaultDatabaseTimeZone($created_at);
        return true;
    }

    public function get_updated_at_timestamp(): Carbon {
    // restituisce il timestamp nel quale la notifica è stata aggiornata l'ultima volta
        return $this->ConvertToLocalTimeZone($this->updated_at);
    }

    public function set_updated_at_timestamp(Carbon $updated_at): bool {
    // imposta il timestamp al quale la notifica è stata aggiornata l'ultima volta
        $this->updated_at = $this->ConvertToDefaultDatabaseTimeZone($updated_at);
        return true;
    }

    public function get_read_at_timestamp(): ?Carbon {
    // restituisce il timestamp nel quale la notifica è stata letta
        return $this->ConvertToLocalTimeZone($this->read_at);
    }

    public function set_read_at_timestamp(Carbon $read_at = null): bool {
    // imposta il timestamp al quale la notifica è stata letta
        $this->read_at = $this->ConvertToDefaultDatabaseTimeZone($read_at);
        return true;
    }

    public function get_closed_at_timestamp(): ?Carbon {
    // restituisce il timestamp al quale l'azione è stata completata
        return $this->ConvertToLocalTimeZone($this->data['closed_at']);
    }

    public function set_closed_at_timestamp(Carbon $closed_at = null): bool {
    // imposta il timestamp al quale l'azione è stata completata
        $this->data['closed_at'] = $this->ConvertToDefaultDatabaseTimeZone($closed_at);
        return true;
    }

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Carbon $created_at,
                                int $object_id = null,
                                string $object_name = null,
                                Carbon $read_at = null,
                                Carbon $closed_at = null) {
        $this->set_created_at_timestamp($created_at);
        $this->set_updated_at_timestamp($created_at);
        $this->object_id = $object_id;
        $this->object_name = $object_name;
        $this->set_read_at_timestamp($read_at);
        $this->set_closed_at_timestamp($closed_at);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        $via_array = [];
        if(!is_null($notifiable->get_username()) && !is_null($notifiable->get_hashed_password()) &&
            !($notifiable->is_disabled())) {
        // invio le notifiche in-app agli utenti solo se hanno un account già attivato e non attualmente disabilitato:
            $via_array[] = 'database';
        }
        if(!is_null($notifiable->get_email_address())) {
        // invio le notifiche via mail agli utenti solo se hanno un'email settata:
            $via_array[] = 'mail';
        }
        return $via_array;
    }

     /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable) {
        if(!is_null($notifiable->get_language_preference())) {
        // per la traduzione del testo della notifica, leggo la lingua preferita dall'utente:
            $language = $notifiable->get_language_preference();
        } else {
        // se l'utente non ha settato una preferenza:
            // uso la lingua di default della webapp:
            $language = Lang::getLocale();
        }
        switch($this->type) {
        // TODO: posso spostare questo switch nel metodo toArray delle varie classi derivate, lanciando poi parent::toArray()?
            case SerialLineOfflineAlarmNotification::class:
                $this->text = ucfirst(__('notifications/in-app.serial_line_offline_alarm_message', ['app_name' => config('app.name')], $language));
                break;
            case SocketOfflineAlarmNotification::class:
                $this->text = ucfirst(__('notifications/in-app.socket_offline_alarm_message', ['app_name' => config('app.name')], $language));
                break;
            case SocketTheftAlarmNotification::class:
                $this->text = ucfirst(__('notifications/in-app.socket_theft_alarm_message', [], $language));
                break;
            case SocketMaxWaterTapTimeAlarmNotificationForAdmins::class:
                $this->text = ucfirst(__('notifications/in-app.max_water_tap_time_alarm_for_admins_message', ['threshold' => config('marina.max_water_tap_time_alarm_threshold_for_admin_users_minutes')], $language));
                break;
            case SocketMinPowerConsumptionAlarmNotification::class:
                $this->text = ucfirst(__('notifications/in-app.min_power_consumption_alarm_message', [], $language));
                break;
            case SocketMaxPowerConsumptionAlarmNotification::class:
                $this->text = ucfirst(__('notifications/in-app.max_power_consumption_alarm_message', [], $language));
                break;
            case SocketMinCreditAlarmNotification::class:
                $this->text = ucfirst(__('notifications/in-app.min_socket_credit_alarm_message', [], $language));
                break;
            case SocketMaxWaterTapTimeAlarmNotificationForUsers::class:
                $this->text = ucfirst(__('notifications/in-app.max_water_tap_time_alarm_for_users_message', [], $language));
                break;
            // la notifica di tipo ReportCreationCompletedNotification implementa un proprio metodo toArray()
        }
        if(!is_null($this->get_closed_at_timestamp())) {
            $closed_at_timestamp = $this->get_closed_at_timestamp();
        } else {
            $closed_at_timestamp = null;
        }
        if(!($notifiable->is_disabled())) {
            return [
                'object_id' => $this->object_id,
                'object_type' => $this->object_type,
                'object_name' => $this->object_name,
                'text' => $this->text,
                'closed_at' => $closed_at_timestamp,
            ];
        }
        return;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    abstract public function toMail($notifiable);
}
