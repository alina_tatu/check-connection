<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\URL;

class EmailChangeNotification extends Notification implements ShouldQueue {
    use Queueable;

    /**
     * The user Email
     *
     * @var string
     */
    protected $userId;

    /**
     * Create a new notification instance.
     *
     * @param string $userId
     */
    public function __construct(string $userId) {
        $this->userId = $userId;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(AnonymousNotifiable $notifiable) {
        // TODO: ma se la mail viene inviata al nuovo indirizzo da un malintenzionato, il vecchio potrebbe non sapere mai che l'account gli è stato rubato. Verificare.        
        return (new MailMessage)->markdown('email.change_email_address', [
            'notifiable' => $notifiable,
            'url' => $this->verifyRoute($notifiable)
        ]);
    }

    /**
     * Returns the Reset URl to send in the Email
     *
     * @param AnonymousNotifiable $notifiable
     * @return string
     */
    protected function verifyRoute(AnonymousNotifiable $notifiable) {
        return URL::temporarySignedRoute('user.email-change-verify', 60 * 60, [
            'user' => $this->userId,
            'email' => $notifiable->routes['mail']
        ]);
    }
}