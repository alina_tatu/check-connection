<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;

class SocketMaxWaterTapTimeAlarmNotificationForAdmins extends SmartCloudNotification {
    use Queueable;

    public const NOTIFICATION_OBJECT_TYPE_FOR_DB = 'SOCKET';

    public function __construct(Carbon $created_at,
                                int $object_id,
                                string $object_name,
                                Carbon $read_at = null,
                                Carbon $closed_at = null) {
        parent::__construct($created_at, $object_id, $object_name, $read_at, $closed_at);
        $this->object_type = $this::NOTIFICATION_OBJECT_TYPE_FOR_DB;
        $this->type = self::class;
    }

     /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        if(!is_null($notifiable->get_username()) && !is_null($notifiable->get_hashed_password()) && ($notifiable->is_viewer_admin()) && !($notifiable->is_disabled())) {
        /* invio la notifica all'utente solamente se:
            - ha uno username (quindi è un utente cloud)
            - ha una password (quindi ha già effettuato la registrazione)
            - è almeno un viewer_admin
            - non ha un account disabilitato
        */
            // le notifiche di questo tipo vengono inviate agli admin solamente in-app, non via mail:
            return ['database'];
        }
        return [];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable) {
        return parent::toArray($notifiable);
    }

    public function toMail($notifiable) {
    // TODO: per il momento qui non faccio nulla, in modo che l'invio delle mail per questo tipo di notifica sia disattivato, dato che vengono ricevute solo dagli admin e che gli admin ricevono le notifiche solamente in-app:
    }
}
