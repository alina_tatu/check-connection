<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Carbon;

class ReportCreationCompletedNotification extends SmartCloudNotification {
    use Queueable;

    protected $subject_type;

    public const NOTIFICATION_OBJECT_TYPE_FOR_DB = 'REPORT CREATED';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Carbon $created_at,
                                string $file_path,
                                string $subject_type,
                                ?string $subject_name,
                                Carbon $start_timestamp,
                                Carbon $end_timestamp) {
        $object_id = $object_name = $read_at = $closed_at = null;
        parent::__construct($created_at, $object_id, $object_name, $read_at, $closed_at);
        $this->object_type = $this::NOTIFICATION_OBJECT_TYPE_FOR_DB;
        $this->type = self::class;
        $this->object_name = $file_path;
        $this->subject_type = $subject_type;
        $this->subject_name = $subject_name;
        $this->start_timestamp = $start_timestamp;
        $this->end_timestamp = $end_timestamp;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return [/*'mail', */'database'];        // TODO:  per ora lascio commentato 'mail', perciò le notifiche dei report vengono inviate solamente in-app. Se devono essere inviate via mail: scommentare, aggiungere aggiungere nel metdoo toMail() la tipologia e l'oggetto del report e verificare che la mail ed il link al report vengano generati correttamente.
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        if(!is_null($notifiable->get_language_preference())) {
        // per la traduzione del testo della notifica, leggo la lingua preferita dall'utente:
            $language = $notifiable->get_language_preference();
        } else {
        // se l'utente non ha settato una preferenza:
            // uso la lingua di default della webapp:
            $language = Lang::getLocale();
        }
        App::setLocale($language);
        return (new MailMessage)
                    ->line(ucfirst(__('notifications/mail.greetings', ['sender_name' => config('app.name'),
                    'recipient_name' => $notifiable->get_full_name() ])))
                    ->line(ucfirst(__('notifications/mail.report_generated_message', ['link' => $this->text])))
                    ->line('')
                    ->line(ucfirst(__('notifications/mail.do_not_reply')));
        // TODO: quando deciderò di mandare le notifiche di report generato anche via mail, dovrò aggiungere anche qui la tipologia e l'oggetto del report
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable) {
        if(!is_null($notifiable->get_language_preference())) {
        // per la traduzione del testo della notifica, leggo la lingua preferita dall'utente:
            $language = $notifiable->get_language_preference();
        } else {
        // se l'utente non ha settato una preferenza:
            // uso la lingua di default della webapp:
            $language = Lang::getLocale();
        }
        $this->text = ucfirst(__('notifications/in-app.report_generated_message', [], $language));       // il placeholder :link presente nella translation verrà sostituito direttamente sulla pagina delle notifiche
        if(!is_null($this->get_closed_at_timestamp())) {
            $closed_at_timestamp = $this->get_closed_at_timestamp();
        } else {
            $closed_at_timestamp = null;
        }
        if(!($notifiable->is_disabled())) {
            return [
                'object_id' => $this->object_id,
                'object_type' => $this->object_type,
                'object_name' => $this->object_name,
                'subject_type' => $this->subject_type,
                'subject_name' => $this->subject_name,
                'start_timestamp' => $this->start_timestamp,
                'end_timestamp' => $this->end_timestamp,
                'text' => $this->text,
                'closed_at' => $closed_at_timestamp,
            ];
        }
        return;
    }
}
