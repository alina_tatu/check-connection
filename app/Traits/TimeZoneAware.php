<?php

    namespace App\Traits;
    use Illuminate\Support\Carbon;

    trait TimeZoneAware {
    // TODO: ma questa roba viene mai chiamata? l'ho presa da qui: https://anderly.com/2017/12/21/laravel-user-timezone-aware-trait/ ma non sono sicuro di aver capito come funziona e se viene chiamata automaticamente, oppure se devo chiamarla io esplicitamente: non ho ancora capito se è questo trait che fa in modo che io veda i dati nella timezone corretta, oppure se sono le varie chiamata a Carbon::setTimeZone()/Carbon::tz()

        public function ConvertToDefaultDatabaseTimeZone(Carbon $timestamp = null): ?Carbon {
        /**
         * Converts the passed timestamp to the default app timezone set in config/app.php
         *
         * @return Carbon|null
         */
            if(!is_null($timestamp)) {
                if(!($timestamp instanceof Carbon)) {
                    $timestamp = Carbon::parse($timestamp);
                }
                $timestamp->setTimezone(config('app.timezone'));
            }
            return $timestamp;
        }

        public function ConvertToLocalTimeZone(Carbon $timestamp = null): ?Carbon {
        /**
         * Converts the passed timestamp to the local timezone
         *
         * @return Carbon|null
         */
            if(!is_null($timestamp)) {
                if(!($timestamp instanceof Carbon)) {
                    $timestamp = Carbon::parse($timestamp);
                }
                $timestamp->setTimezone(config('dates_currency_and_locale.local_timezone'));
            }
            return $timestamp;
        }
    }
