<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Carbon\CarbonInterval;
use App\SocketSnapshot;
use App\Subscription;
use App\Support\DisconnectionPeriod;
use App\Http\Controllers\ConsumptionReportsController;

/**
 * App\Berth
 *
 * @property int $id
 * @property string $pontoon_name
 * @property string $berth_name
 * @property string|null $description
 * @property string $socket_type
 * @property int $pedestal_id
 * @property-read Collection|Subscription[] $boundSubscriptions
 * @property-read int|null $bound_subscriptions_count
 * @property-read \App\SocketLastSnapshot|null $last_snapshot
 * @property-read \App\Pedestal $pedestal
 * @property-read Collection|SocketSnapshot[] $snapshots
 * @property-read int|null $snapshots_count
 * @method static \Illuminate\Database\Eloquent\Builder|Berth disableCache()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Berth newModelQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Berth newQuery()
 * @method static \GeneaLabs\LaravelModelCaching\CachedBuilder|Berth query()
 * @method static \Illuminate\Database\Eloquent\Builder|Berth whereBerthName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berth whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berth whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berth wherePedestalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berth wherePontoonName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berth whereSocketType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Berth withCacheCooldownSeconds($seconds = null)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|Berth newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Berth newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Berth query()
 */
class Berth extends Model {
    use Cachable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'berths';
    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'pontoon_name', 
        'berth_name', 
        'description', 
        'socket_type', 
        'pedestal_id',
    ];

    public const ALLOWED_SOCKET_TYPES =  ['16 A SP',
                                          '16 A TP',
                                          '32 A SP',
                                          '32 A TP',
                                          '63 A SP',
                                          '63 A TP',
                                          '125 A SP',
                                          '125 A TP',
                                          '250 A TP',
                                          '630 A TP',
                                          '1000 A TP',
                                          'not defined'];

    public const SOCKET_TYPE_NOT_DEFINED = 'not defined';
    public const DEFAULT_SOCKET_TYPE = 'not defined';

    public function get_id(): int {
        return intval($this->attributes[$this->primaryKey]);
    }

    public function set_id(int $id) {
        $this->attributes[$this->primaryKey] = intval($id);
        // mentre setto l'id del posto barca, calcolo anche l'id della colonnina di appartenenza:
        $this->calculate_and_set_pedestal_id();
    }

    public function get_pontoon_name(): ?string {
        return strval($this->pontoon_name);
    }

    public function set_pontoon_name(string $value) {
        $this->pontoon_name = $value;
    }

    public function get_berth_name(): ?string {
        return $this->berth_name;
    }

    public function set_berth_name(string $value) {
        $this->berth_name = $value;
    }

    public function get_description(): string {
        if(strlen(strval($this->description)) > 0) {
            return strval($this->description);
        } else {
            return($this->pontoon_name.' '.$this->berth_name);
        }
    }

    public function set_description(string $value) {
        $this->description = $value;
    }

    public function get_socket_type(): ?string {
        if(in_array($this->socket_type, $this::ALLOWED_SOCKET_TYPES)) {
            return $this->socket_type;
        } else {
            return $this::SOCKET_TYPE_NOT_DEFINED;
        }
    }

    public function set_socket_type(string $value) {
    // imposta il tipo della presa al valore del parametro passato, se rientra tra i valori possibili. In caso contrario, lo setta al valore di default.
        if(in_array($value, $this::ALLOWED_SOCKET_TYPES)) {
            $this->socket_type = strval($value);
        } else {
            $this->socket_type = $this::SOCKET_TYPE_NOT_DEFINED;
        }
    }

    public function get_serial_line_id(int $max_sockets_per_serial_line): int {
    // dato il max_prese_per_linea_seriale, calcola e restituisce l'id della linea seriale alla quale la presa appartiene
        $serial_line_id = intdiv((($this->attributes[$this->primaryKey]) - 1), $max_sockets_per_serial_line) + 1;
        return $serial_line_id;
    }

    public function get_sequential_number_in_serial_line(int $max_sockets_per_serial_line): int {
    // dato il max_prese_per_linea_seriale, calcola e restituisce il progressivo della presa all'interno della linea seriale
        $sequential_number_in_serial_line = $this->attributes[$this->primaryKey] % $max_sockets_per_serial_line;
        return $sequential_number_in_serial_line;
    }

    public function get_pedestal_sequential_number_in_serial_line(int $max_sockets_per_serial_line, int $max_sockets_per_pedestal): int {
    // dati i valori di max_prese_per_linea_seriale e max_prese_per_colonnina, calcola e restituisce il progressivo della colonnina di appartenenza della presa all'interno della linea seriale
        $pedestal_sequential_number_in_serial_line = intdiv(($this->get_sequential_number_in_serial_line($max_sockets_per_serial_line) - 1), $max_sockets_per_pedestal) + 1;
        return $pedestal_sequential_number_in_serial_line;
    }

    public function calculate_and_set_pedestal_id(): int {
    // Viene richiamata al momento della ricezione di un'anagrafica presa inviata dal TagManager. Calcola l'id della colonnina di appartenenza del posto barca (che serve per assegnare correttamente ogni presa alla sua colonnina sulla view plant_map).
        $pedestal_id_to_set = (($this->get_serial_line_id(config('marina.max_sockets_per_serial_line')) - 1) * config('marina.max_sockets_per_serial_line')) + $this->get_pedestal_sequential_number_in_serial_line(config('marina.max_sockets_per_serial_line'), config('marina.max_sockets_per_pedestal'));
        $this->set_pedestal_id($pedestal_id_to_set);
        return $pedestal_id_to_set;
    }

    public function get_pedestal_id(): ?int {
        return $this->pedestal_id;
    }

    public function set_pedestal_id(int $value) {
        $this->pedestal_id = $value;
    }

    public function pedestal() {
        return $this->belongsTo(Pedestal::class);
    }

    public function boundSubscriptions() {
        return $this->hasMany(Subscription::class, 'socket_id');
    }

    public function currently_bound_subscription() {
        if((Auth::guest()) || (Auth::user()->is_supervisor())) {
        // se non c'è un utente loggato (quindi se sto chiamando questo metodo da una API) oppure se c'è ed è un supervisore, posso cercare la subscription tra tutte quelle legate al posto barca:
            return $this->hasOne(Subscription::class, 'socket_id')
                        ->active()
                        ->newerFirst();
        } else {
         // se c'è un utente loggato e non è un supervisore, devo escludere dalla ricerca le subscription legate agli utenti supervisore:
            return $this->hasOne(Subscription::class, 'socket_id')
                        ->excludeSupervisorRelatedSubscriptions()
                        ->active()
                        ->newerFirst();
        }
    }

    public function snapshots() {
        return $this->hasMany(SocketSnapshot::class, 'socket_id');
    }

    public function linking_snapshots() {
        return $this->hasMany(SocketSnapshot::class, 'socket_id')->where('link_status', '!=', SocketSnapshot::LINK_STATUS_NOT_CONNECTED);
    }

    public function last_snapshot() {
        return $this->hasOne(SocketLastSnapshot::class, 'socket_id');
    }

    public function get_last_socket_snapshot_before_timestamp(Carbon $timestamp, bool $strict_before = null): ?SocketSnapshot {
    /*  restituisce il primo snapshot della presa in cui il timestamp sia
            1) <= del parametro $timestamp (caso $strict_before non passato oppure strict_before === false)
            2) < del parametro $timestamp (caso $strict_before passato oppure e === true)
        a prescindere dallo stato comunicazione. Se non trova nessuno snapshot nell'intervallo richiesto, restituisce null; */
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_before) && ($strict_before === true)) {
            $compare_symbol = '<';
        } else {
            $compare_symbol = '<=';
        }

        // TODO: forse posso sostituire con return $this->hasOne(SocketSnapshot::class, 'socket_id')->where('timestamp', $compare_symbol, $timestamp)->latest('timestamp'):
        $snapshot_at_timestamp = $this->snapshots()
                                      ->where('timestamp', $compare_symbol, $timestamp)
                                      ->orderBy('timestamp', 'desc')
                                      ->first();
        return $snapshot_at_timestamp;
    }

    public function get_last_on_off_status_occurrence_before_timestamp(Carbon $timestamp, string $status_to_search, bool $strict_before = null): ?SocketSnapshot {
    /*  restituisce il primo snapshot nel quale la presa aveva stato_on_off === $status_to_search ed in cui il timestamp sia
            1) <= del parametro $timestamp (caso $strict_before non passato oppure $strict_before === false)
            2) < del parametro $timestamp (caso $strict_before passato oppure e === true)
        se non trova nessuno snapshot corrispondente ai criteri, restituisce null; */
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_before) && ($strict_before === true)) {
            $compare_symbol = '<';
        } else {
            $compare_symbol = '<=';
        }
        // TODO: forse posso sostituire con return $this->hasOne(SocketSnapshot::class, 'socket_id')->where('timestamp', $compare_symbol, $timestamp)->where('on_off_status', '=' , $status_to_search)->latest('timestamp'):
        $last_occurrence_before_timestamp = $this->snapshots()
                                                 ->where('timestamp', $compare_symbol, $timestamp)
                                                 ->where('on_off_status', '=', $status_to_search)
                                                 ->orderBy('timestamp', 'desc')
                                                 ->first();
        return $last_occurrence_before_timestamp;
    }

    public function on_off_status_has_occurred_before_timestamp(Carbon $timestamp, string $status_to_search, bool $strict_before = null): bool {
    /* verifica se la presa ha mai avuto lo stato on/off/err $status_to_search prima (< o <=, a seconda del valore di $strict_before) di $timestamp */
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_before) && ($strict_before === true)) {
            $compare_symbol = '<';
        } else {
            $compare_symbol = '<=';
        }
        $status_occurrence_count_before_timestamp = $this->snapshots()
                                                         ->where('timestamp', $compare_symbol, $timestamp)
                                                         ->where('on_off_status', '=', $status_to_search)
                                                         ->count();
        return($status_occurrence_count_before_timestamp > 0);
    }

    public function get_last_link_status_occurrence_before_timestamp(Carbon $timestamp, string $status_to_search, bool $strict_before = null): ?SocketSnapshot {
    /*  restituisce il primo snapshot nel quale la presa aveva stato comunicazione === $status_to_search ed in cui il timestamp sia
            1) <= del parametro $timestamp (caso $strict_before non passato oppure $strict_before === false)
            2) < del parametro $timestamp (caso $strict_before passato oppure e === true)
        se non trova nessuno snapshot corrispondente ai criteri, restituisce null; */
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_before) && ($strict_before === true)) {
            $compare_symbol = '<';
        } else {
            $compare_symbol = '<=';
        }
        // TODO: forse posso sostituire con return $this->hasOne(SocketSnapshot::class, 'socket_id')->where('timestamp', $compare_symbol, $timestamp)->where('link_status', '=' , $status_to_search)->latest('timestamp'):
        $last_occurrence_before_timestamp = $this->snapshots()
                                                 ->where('timestamp', $compare_symbol, $timestamp)
                                                 ->where('link_status', '=', $status_to_search)
                                                 ->orderBy('timestamp', 'desc')
                                                 ->first();
        return $last_occurrence_before_timestamp;
    }

    public function get_last_linking_status_before_timestamp(Carbon $timestamp, bool $strict_before = null): ?SocketSnapshot {
    /*  restituisce il primo snapshot nel quale la presa aveva stato comunicazione !== SocketSnapshot::LINK_STATUS_NOT_CONNECTED ed in cui il timestamp sia
            1) <= del parametro $timestamp (caso $strict_before non passato oppure $strict_before === false)
            2) < del parametro $timestamp (caso $strict_before passato oppure e === true)
        se non trova nessuno snapshot corrispondente ai criteri, restituisce null; */
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_before) && ($strict_before === true)) {
            $compare_symbol = '<';
        } else {
            $compare_symbol = '<=';
        }
        // TODO: forse posso sostituire con return $this->hasOne(SocketSnapshot::class, 'socket_id')->where('timestamp', $compare_symbol, $timestamp)->where('link_status', '=' , $status_to_search)->latest('timestamp'):
        $last_occurrence_before_timestamp = $this->snapshots()
                                                 ->where('timestamp', $compare_symbol, $timestamp)
                                                 ->where('link_status', '!=', SocketSnapshot::LINK_STATUS_NOT_CONNECTED)
                                                 ->orderBy('timestamp', 'desc')
                                                 ->first();
        return $last_occurrence_before_timestamp;
    }

    public function get_first_link_status_occurrence_after_timestamp(Carbon $timestamp, string $status_to_search, bool $strict_after = null): ?SocketSnapshot {   // TODO: rinominare come i metodi analoghi sul model Subscription
    /*  restituisce il primo snapshot nel quale la presa aveva stato comunicazione === $status_to_search ed in cui il timestamp sia
            1) >= del parametro $timestamp (caso $strict_after non passato oppure strict_after === false)
            2) > del parametro $timestamp (caso $strict_after passato e === true)
        se non trova nessuno snapshot corrispondente ai criteri, restituisce null; */
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_after) && ($strict_after === true)) {
            $compare_symbol = '>';
        } else {
            $compare_symbol = '>=';
        }
        // TODO: forse posso sostituire con return $this->hasOne(SocketSnapshot::class, 'socket_id')->where('timestamp', $compare_symbol, $timestamp)->where('link_status', '=' , $status_to_search)->first('timestamp'):
        $first_occurrence_after_timestamp = $this->snapshots()
                                                 ->where('timestamp', $compare_symbol, $timestamp)
                                                 ->where('link_status', '=', $status_to_search)
                                                 ->orderBy('timestamp', 'asc')
                                                 ->first();
        return $first_occurrence_after_timestamp;
    }

    public function get_first_linking_status_after_timestamp(Carbon $timestamp, bool $strict_after = null): ?SocketSnapshot {   // TODO: rinominare come i metodi analoghi sul model Subscription
    /*   restituisce il primo snapshot nel quale la presa aveva stato comunicazione !== SocketSnapshot::LINK_STATUS_NOT_CONNECTED ed in cui il timestamp sia
            1) >= del parametro $timestamp (caso $strict_after non passato oppure strict_after === false)
            2) > del parametro $timestamp (caso $strict_after passato e === true)
        se non trova nessuno snapshot corrispondente ai criteri, restituisce null; */
        $timestamp->setTimezone(config('app.timezone'));
        if(!is_null($strict_after) && ($strict_after === true)) {
            $compare_symbol = '>';
        } else {
            $compare_symbol = '>=';
        }
        // TODO: forse posso sostituire con return $this->hasOne(SocketSnapshot::class, 'socket_id')->where('timestamp', $compare_symbol, $timestamp)->where('link_status', '=' , $status_to_search)->first('timestamp'):
        $first_occurrence_after_timestamp = $this->snapshots()
                                                 ->where('timestamp', $compare_symbol, $timestamp)
                                                 ->where('link_status', '!=', SocketSnapshot::LINK_STATUS_NOT_CONNECTED)
                                                 ->orderBy('timestamp', 'asc')
                                                 ->first();
        return $first_occurrence_after_timestamp;
    }

    public function get_epower_first_replacement_timestamp_during_interval(Carbon $begin_dt, Carbon $end_dt): ?Carbon {
    /* Dati un istante di inizio ed un istante di fine intervallo, restituisce il primo timestamp nel quale l'id dell'epower della presa è cambiato rispetto alle snapshot precedenti. Se non rileva cambi di epower_id o se la presa non ha mai comunicato durante l'intervallo in esame, restituisce null */
        $epower_first_replacement_timestamp_during_interval = null;
        $begin_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
        $first_linking_socket_snapshot_at_begin_dt = $this->get_first_linking_status_after_timestamp($begin_dt, false);
        if(($first_linking_socket_snapshot_at_begin_dt) && ($first_linking_socket_snapshot_at_begin_dt->count() > 0) && ($first_linking_socket_snapshot_at_begin_dt->get_timestamp() <= $end_dt)) {
            $epower_id_on_first_linking_socket_snapshot_at_begin_dt = $first_linking_socket_snapshot_at_begin_dt->get_epower_id();
            $first_socket_snapshot_with_a_different_epower_id_after_begin_dt = $this->snapshots()
                                                                                    ->where('timestamp', '>', $first_linking_socket_snapshot_at_begin_dt->get_timestamp())
                                                                                    ->where('timestamp', '<=', $end_dt)
                                                                                    ->where('link_status', '!=', SocketSnapshot::LINK_STATUS_NOT_CONNECTED)
                                                                                    ->where('epower_id', '!=', $epower_id_on_first_linking_socket_snapshot_at_begin_dt)
                                                                                    ->orderBy('timestamp', 'asc')
                                                                                    ->first();
            if(($first_socket_snapshot_with_a_different_epower_id_after_begin_dt) && ($first_socket_snapshot_with_a_different_epower_id_after_begin_dt->count() > 0)) {
                $epower_first_replacement_timestamp_during_interval = $first_socket_snapshot_with_a_different_epower_id_after_begin_dt->get_timestamp();
            }
        }
        return $epower_first_replacement_timestamp_during_interval;
    }

    public function get_consumption_in_interval(Carbon $begin_dt, Carbon $end_dt, string $subject, bool &$epower_id_has_changed_during_interval, int $flag = ConsumptionReportsController::REPORT_FLAG_TOTAL, int $mode = ConsumptionReportsController::REPORT_MODE_PLAIN): float {
    /* Restituisce uno scalare che rappresenta il consumo effettuato dal posto barca nel periodo trascorso tra $begin_dt e $end_dt.
       Parametri:
        - Carbon $begin_dt (obbligatorio): istante di inizio dell'intervallo per la verifica del consumo
        - Carbon $end_dt (obbligatorio): istante di fine dell'intervallo per la verifica del consumo
        - int $subject (obbligatorio): grandezza per la quale si sta richiedendo la verifica del consumo. I valori possibili sono definiti sul ConsumptionReportsController affinchè possano essere usati anche in altri punti del software, e sono i seguenti:
            - ConsumptionReportsController::REPORT_VALUE_POWER
            - ConsumptionReportsController::REPORT_VALUE_WATER
            - ConsumptionReportsController::REPORT_VALUE_CREDIT
        - int $flag (facoltativo): specifica se il consumo deve essere calcolato utilizzando i contatori totali della presa, o quelli postpagati (TODO: ha senso usare quelli postpagati qui? Non è meglio usarli solo nel metodo analogo presente nel model Subscription? Valutare se è il caso di togliere questo parametro). Valori possibili:
            - ConsumptionReportsController::REPORT_FLAG_TOTAL (default)
            - ConsumptionReportsController::REPORT_FLAG_POSTPAID
        - bool &$epower_id_has_changed_during_interval (passato per riferimento): è usato come secondo valore di ritorno, e se dopo aver lanciato il metodo, risulta true, significa che durante l'intervallo in esame è stata cambiata l'epower sul posto barca. Uso questo valore per segnalare all'utente che ha richiesto un report di consumo (grafico/reportfile o view che sia) che i valori che gli sto mostrando non sono attendibili per via del salto nei contatori di consumo
        - int $mode (facoltativo): specifica se il consumo trovato deve essere rapportato al tempo trascorso nell'intervallo (valore ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME) oppure restituito così com'è senza ulteriori calcoli (valore ConsumptionReportsController::REPORT_MODE_PLAIN, default) */
        $begin_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
        // verifico se la presa ha sempre avuto lo stesso epower_id per tutto il periodo in esame:
        $epower_id_has_changed_during_interval = !($this->had_the_same_epower_id_during_interval($begin_dt, $end_dt));
        // cerco il primo snapshot disponibile per la presa prima di $begin_dt o all'istante $begin_dt stesso:
        $socket_status_at_begin_dt = $this->get_last_linking_status_before_timestamp($begin_dt, false);
        if(is_null($socket_status_at_begin_dt) || ($socket_status_at_begin_dt->count() === 0)) {
        // se sul db NON ESISTE uno snapshot relativo all'istante begin_dt per la presa in esame:
            // lo cerco al primo istante disponibile dopo di esso:
            $socket_status_just_after_begin_dt = $this->get_first_linking_status_after_timestamp($begin_dt, true);
            if(is_null($socket_status_just_after_begin_dt) || ($socket_status_just_after_begin_dt->count() === 0)) {
            // se nemmeno così trovo uno snapshot:
                // significa che la presa in esame non ha snapshot sul db, quindi posso tornare 0 come valore di consumo:
                return 0;
            }
            $socket_status_at_begin_dt = $socket_status_just_after_begin_dt;
        }
        // cerco il primo snapshot disponibile per la presa prima di end_dt o all'istante $end_dt stesso:
        $socket_status_at_end_dt = $this->get_last_linking_status_before_timestamp($end_dt, false);
        if(is_null($socket_status_at_end_dt) || ($socket_status_at_end_dt->count() === 0)) {
        // se non abbiamo trovato $socket_status_at_end_dt (TODO: è possibile questa cosa se ho già trovato $socket_status_at_begin_dt?):
            return 0;
        }
        if($socket_status_at_end_dt->get_timestamp() === $socket_status_at_begin_dt->get_timestamp()) {
        // se $socket_status_at_end_dt e $socket_status_at_begin_dt sono lo stesso snapshot, il consumo tra i due sarà zero:
            return 0;
        }
        // cerco l'istante dell'eventuale (prima) sostituzione dell'epower all'interno dell'intervallo in esame:
        $epower_first_replacement_timestamp_during_interval = $this->get_epower_first_replacement_timestamp_during_interval($begin_dt, $end_dt);
        if(($epower_id_has_changed_during_interval) && (!is_null($epower_first_replacement_timestamp_during_interval))) {
        // se l'epower E' STATA SOSTITUITA nel periodo di tempo tra $socket_status_at_begin_dt e $socket_status_at_end_dt:            
            // $consumption dovrà essere calcolato come la somma tra (il consumo tra $begin_dt e $epower_first_replacement_during_interval) e (il consumo tra $epower_first_replacement_during_interval e $end_dt), quindi chiamo ricorsivamente get_consumption_in_interval() su questi due intervalli:
            // (TODO: attenzione che qui diventa fondamentale la distinzione tra < e <= / > e >= nel calcolo dei consumi dentro i model: c'è qualcosa che devo sistemare?)
            $consumption_between_begin_dt_and_epower_first_replacement_timestamp = $this->get_consumption_in_interval($begin_dt, $epower_first_replacement_timestamp_during_interval->subSeconds(1), $subject, $epower_id_has_changed_during_interval, $flag, $mode);
            $consumption_between_epower_first_replacement_timestamp_and_end_dt = $this->get_consumption_in_interval($epower_first_replacement_timestamp_during_interval, $end_dt, $subject, $epower_id_has_changed_during_interval, $flag, $mode);
            $consumption = $consumption_between_begin_dt_and_epower_first_replacement_timestamp + $consumption_between_epower_first_replacement_timestamp_and_end_dt;
        } else {
        // se l'epower NON E' STATA SOSTITUITA nel periodo di tempo tra $socket_status_at_begin_dt e $socket_status_at_end_dt:
            // calcolo normalmente il consumo effettuato:
            switch($subject) {
                case ConsumptionReportsController::REPORT_VALUE_POWER:
                default:
                    if($flag === ConsumptionReportsController::REPORT_FLAG_TOTAL) {
                        $consumption = ($socket_status_at_end_dt->get_total_power_cnt()) - ($socket_status_at_begin_dt->get_total_power_cnt());
                    } else {
                        $consumption = ($socket_status_at_end_dt->get_postpaid_power_cnt()) - ($socket_status_at_begin_dt->get_postpaid_power_cnt());
                    }
                    break;
                case ConsumptionReportsController::REPORT_VALUE_WATER:
                    if($flag === ConsumptionReportsController::REPORT_FLAG_TOTAL) {
                        $consumption = ($socket_status_at_end_dt->get_total_water_cnt()) - ($socket_status_at_begin_dt->get_total_water_cnt());
                    } else if($flag === ConsumptionReportsController::REPORT_FLAG_POSTPAID) {
                        $consumption = ($socket_status_at_end_dt->get_postpaid_water_cnt()) - ($socket_status_at_begin_dt->get_postpaid_water_cnt());
                    } else {
                        $consumption = 0;
                    }
                    break;
                case ConsumptionReportsController::REPORT_VALUE_CREDIT:
                    // nel caso del credito, il consumo è positivo quando il credito cala, al contrario di quanto succede per acqua ed energia. Perciò devo calcolarlo sottraendo il credito dello snapshot più recente da quello dello snapshot più vecchio:
                    $consumption = ($socket_status_at_begin_dt->get_residual_credit()) - ($socket_status_at_end_dt->get_residual_credit());
            }
        }
        if($mode === ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME) {
        // se devo rapportare il consumo trovato al tempo trascorso nell'intervallo (quindi restituire kWh, m3/h €/h):
            // calcolo il tempo trascorso nell'intervallo:
            $total_time_interval_in_hours = $begin_dt->floatDiffInHours($end_dt, true);
            if($total_time_interval_in_hours > 0) {
            // se le ore trascorse nell'intervallo sono > 0:
                // divido il consumo per le ore trascorse:
                $consumption = $consumption / $total_time_interval_in_hours;
            } else {
            // se il tempo totale trascorso è 0:
                // dividendo per $total_time_interval_in_hours andrei in exception per division by zero. Restituisco invece direttamente 0:
                $consumption = 0;
            }
        }
        return $consumption;
    }

    public function get_current_not_connected_alarm_status(SocketSnapshot $snapshot): bool {
    // verifica lo stato attuale dell'allarme di mancata comunicazione sulla presa
        if($snapshot->get_link_status() === SocketSnapshot::LINK_STATUS_NOT_CONNECTED) {
            return true;
        }
        return false;
    }

    public function get_current_theft_alarm_status(SocketSnapshot $snapshot): bool {
    // verifica lo stato attuale dell'allarme antifurto sulla presa
        if($snapshot->get_link_status() === SocketSnapshot::LINK_STATUS_THEFT_ALARM) {
            return true;
        }
        return false;
    }

    public function had_disconnections_during_interval(Carbon $begin_dt, Carbon $end_dt) {
    // se la presa ha avuto snapshot con stato di mancata comunicazione durante l'intervallo di tempo passato restituisce false, altrimenti restituisce true
        $begin_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
        $total_snapshots_with_missing_communication_alarm_count_in_interval = $this->snapshots()
                                                                                   ->where('timestamp', '>=', $begin_dt)
                                                                                   ->where('timestamp', '<=', $end_dt)
                                                                                   ->where('link_status', '=', SocketSnapshot::LINK_STATUS_NOT_CONNECTED)
                                                                                   ->count();
        if($total_snapshots_with_missing_communication_alarm_count_in_interval > 0) {
        // se nell'intervallo in esame, esistono snapshot in cui la presa non stava comunicando, ritorno true:
            return true;
        }
        // altrimenti ritorno false:
        return false;
    }

    public function was_always_on_during_interval(Carbon $begin_dt, Carbon $end_dt, bool $ignore_disconnections = false) {
    /* - se il flag $ignore_disconnections NON viene passato oppure è false: se la presa è sempre stata accesa E NON HA MAI AVUTO DISCONNESSIONI durante durante l'intervallo di tempo passato restituisce true, altrimenti restituisce false
       - se il flag $ignore_disconnections viene passato ed è true: se la presa è sempre stata accesa durante durante l'intervallo di tempo passato restituisce true, altrimenti restituisce false. Questo anche nel caso in cui - durante l'intervallo passato - la presa abbia avuto disconnessioni */
        $begin_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
        // conto tutte le snapshot nell'intervallo a prescindere dallo stato comunicazione della presa:
        $total_snapshots_count_in_interval = $this->snapshots()
                                                  ->where('timestamp', '>=', $begin_dt)
                                                  ->where('timestamp', '<=', $end_dt)
                                                  ->count();
        // conto tutte le snapshot nell'intervallo nelle quali la presa stava comunicando:
        $total_snapshots_count_without_missing_communication_alarm_in_interval = $this->snapshots()
                                                                                      ->where('timestamp', '>=', $begin_dt)
                                                                                      ->where('timestamp', '<=', $end_dt)
                                                                                      ->where('link_status', '!=', SocketSnapshot::LINK_STATUS_NOT_CONNECTED)
                                                                                      ->count();
        if(!$ignore_disconnections) {
        // se non devo ignorare le disconnessioni
            if((!$total_snapshots_count_in_interval) ||
               ($total_snapshots_count_in_interval !== $total_snapshots_count_without_missing_communication_alarm_in_interval)) {
            // e se non ho trovato snapshot per la presa nell'intervallo, oppure se ne ho trovate ma in alcune di esse la presa non stava comunicando:
                return false;
            }
        }
        // a questo punto conto le snapshot nell'intervallo nelle quali la presa era accesa:
        $total_snapshots_with_socket_status_on_count_in_interval = $this->snapshots()
                                                                        ->where('timestamp', '>=', $begin_dt)
                                                                        ->where('timestamp', '<=', $end_dt)
                                                                        ->count();
        if($total_snapshots_with_socket_status_on_count_in_interval === $total_snapshots_count_without_missing_communication_alarm_in_interval) {
        // se il numero trovato combacia con il numero delle snapshot totali dell'intervallo in cui la presa stava comunicando:
            // restituisco true:
            return true;
        }
        // altrimenti restituisco false:
        return false;
    }

    public function always_the_same_subscription_was_used_on_the_socket_during_interval(Carbon $begin_dt, Carbon $end_dt, bool $ignore_disconnections = null): bool {
    // dato un timestamp di inizio intervallo e uno di fine intervallo, se sulla presa è sempre stata attiva la stessa subscription per tutto il periodo restituisce true, altrimenti restituisce false. Se $ignore_disconnections viene passato ed è true, non si preoccupa di eventuali snapshot in cui la presa non stava comunicando (restituendo quindi true se trova la stessa subscription per tutto il periodo, anche in caso di incertezza dovuta a momenti di mancata comunicazione); in caso contrario, in presenza di snapshot in cui la presa non comunicava (oppure se nell'intervallo richiesto non risulta esserci nessuno snapshot per la presa richiesta), restituisce false anche se per tutto il resto del tempo è stata attiva la stessa subscription
        $begin_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
        // cerco tutte le snapshot nell'intervallo nelle quali la presa stava comunicando:
        $snapshots_without_missing_communication_alarm_during_interval = $this->linking_snapshots()
                                                                              ->where('timestamp', '>=', $begin_dt)
                                                                              ->where('timestamp', '<=', $end_dt)
                                                                              ->orderBy('timestamp', 'asc')
                                                                              ->get();
        if(is_null($snapshots_without_missing_communication_alarm_during_interval) ||
           ($snapshots_without_missing_communication_alarm_during_interval->count() === 0)) {
        // se nell'intervallo specificato non trovo snapshots nelle quali la presa stava comunicando:
            if(!is_null($ignore_disconnections) && ($ignore_disconnections === true)) {
            // e se devo ignorare le snapshot in cui la presa non comunicava:
                // ritorno true
                return true;
            } else {
            // se invece NON devo ignorare le snapshot in cui la presa non comunicava:
                // ritorno false
                return false;
            }
        }
        $socket_had_disconnections_during_interval = $this->had_disconnections_during_interval($begin_dt, $end_dt);
        if($socket_had_disconnections_during_interval) {
        // se nell'intervallo richiesto ci sono snapshot in cui la presa non comunicava
            if(is_null($ignore_disconnections) || ($ignore_disconnections === false)) {
            // e se NON devo ignorare le snapshot in cui la presa non comunicava:
                // ritorno false
                return false;
            }
        }
        $last_linking_snapshot = $snapshots_without_missing_communication_alarm_during_interval->last();
        $transponder_id_from_last_linking_snapshot = $last_linking_snapshot->get_transponder_id();
        // cerco tutte le snapshot nell'intervallo in cui la presa stava comunicando e su di essa era in uso il transponder con id=$transponder_id_from_last_linking_snapshot
        $snapshots_without_missing_communication_alarm_and_with_transponder_id_from_last_linking_snapshot_in_interval_count = $this->linking_snapshots()
                                                                                                                                   ->where('timestamp', '>=', $begin_dt)
                                                                                                                                   ->where('timestamp', '<=', $end_dt)
                                                                                                                                   ->where('transponder_id', $transponder_id_from_last_linking_snapshot)
                                                                                                                                   ->orderBy('timestamp', 'asc')
                                                                                                                                   ->count();
        if($snapshots_without_missing_communication_alarm_and_with_transponder_id_from_last_linking_snapshot_in_interval_count !== $snapshots_without_missing_communication_alarm_during_interval->count()) {
        // se il numero di snapshot in cui la presa stava comunicando e su di essa era in uso il transponder con id=$transponder_id_from_last_linking_snapshot è diverso dal numero di snapshot totale in cui la presa stava comunicando nell'intervallo:
            // ritorno false:
            return false;
        }
        // istanzio il transponder relativo all'id trovato sull'ultimo snapshot (che - se sono arrivato fino a qui - è anche l'unico transponder usato sulla presa nell'intero intervallo):
        $transponder_from_last_linking_snapshot = Transponder::findByCode($transponder_id_from_last_linking_snapshot);
        if((!$transponder_from_last_linking_snapshot) || ($transponder_from_last_linking_snapshot->count() === 0)) {
        // se non trovo istanze di quel transponder sul db:
            // posso comunque presumere che la "subscription" sia stata la stessa durante tutto il periodo in esame, dato che sul db non esisteranno nemmeno subscriptions ad esso collegate:
            return true;
        }
        // se invece il transponder esiste sul db:
        // recupero le subscriptions alle quali era collegato durante il periodo in esame:
        $active_transponder_subscriptions_in_interval = $transponder_from_last_linking_snapshot->active_subscriptions_in_interval($begin_dt, $end_dt)->get();
        if(!is_null($active_transponder_subscriptions_in_interval) && ($active_transponder_subscriptions_in_interval->count() > 1)) {
        // se durante l'intervallo in esame esisteva più di una subscription collegata al transponder:
            // controllo se tra di esse c'era qualche subscription non collegata ad una presa:
            $all_transponder_subscriptions_were_bound_to_a_socket = true;
            $transponder_ids_from_subscriptions_not_bound_to_a_socket = [];
            foreach($active_transponder_subscriptions_in_interval as $subscription) {
                if(!($subscription->is_bound_to_socket())) {
                    $all_transponder_subscriptions_were_bound_to_a_socket = false;
                    // e se sì, ne salvo il transponder_id:
                    $transponder_ids_from_subscriptions_not_bound_to_a_socket[] = $subscription->get_transponder_id();
                }
            }
            if(!($all_transponder_subscriptions_were_bound_to_a_socket)) {
            // se tra le subscription collegate al transponder all'istante dell'ultimo snapshot c'era qualche subscription non bindata ad una presa:
                // significa che l'utilizzatore di quella presa è cambiato durante l'intervallo in esame. Quindi restituisco false:
                return false;
            }
        }
        // se sono arrivato qui, significa che durante l'intervallo in esame sono esistite 0 o 1 subscriptions collegate al transponder:
        // quindi l'utilizzatore di quella presa non è mai cambiato durante l'intervallo in esame. Restituisco true:
        return true;
    }

    public function get_current_min_power_consumption_alarm_status(SocketSnapshot $snapshot): bool {
        $now = Carbon::now(config('app.timezone'));
        if($snapshot->get_link_status() !== $snapshot::LINK_STATUS_NOT_CONNECTED) {
        // se la presa sta attualmente comunicando
            // mantengo come riferimento lo snapshot passato come parametro:
            $last_linking_snapshot = $snapshot;
        } else {
        // se la presa NON sta attualmente comunicando
            // recupero l'ultimo stato della presa nel quale stava comunicando:
            // TODO: ma ha senso? se voglio calcolare lo stato dell'allarme all'istante relativo a SocketSnapshot, e a quell'istante la presa non stava comunicando, l'allarme sarà false e basta (quindi dovrò ritornare false qui e fare a meno di usare $last_linking_snapshot, usando solo $snapshot). Soprattutto se questo metodo viene usato solo al momento dell'inserimento dello snapshot sul db per poter settare il campo allarmi
            $last_linking_snapshot = $this->get_last_linking_status_before_timestamp($now, false);
        }
        if((!$last_linking_snapshot) || ($last_linking_snapshot->count() === 0)) {
        // se non esiste nessuno snapshot precedente a $snapshot nel quale la presa stava comunicando, l'allarme non è attivo
            return false;
        }
        $last_linking_snapshot_timestamp = $snapshot->get_timestamp();
        // cerco la subscription presente sulla presa all'istante dello snapshot appena recuperato
        if(boolval($last_linking_snapshot->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
			$connected_subscription_at_last_linking_timestamp = $last_linking_snapshot->subscription;
        } else {
        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
        	$connected_subscription_at_last_linking_timestamp = $last_linking_snapshot->retrieve_connected_subscription();
        }
        if((!$connected_subscription_at_last_linking_timestamp) || ($connected_subscription_at_last_linking_timestamp->count() === 0)) {
        // se a quello snapshot non era attiva nessuna subscription sulla presa, l'allarme non è attivo:
            return false;
        }
        $interval = Carboninterval::createFromFormat('i', strval(config('marina.power_alarms_minutes')));
        $difference_between_now_and_last_linking_snapshot_timestamp = $last_linking_snapshot_timestamp->diffAsCarbonInterval($now, true);
        if($difference_between_now_and_last_linking_snapshot_timestamp->totalSeconds > $interval->totalSeconds) {
        // se lo stato presa usato come riferimento è relativo a più di <$interval> tempo fa
            // ritorno false: non posso sapere se l'allarme è attivo:
            return false;
        }
        // recupero lo stato della presa <interval> fa:
        $one_interval_ago_dt = $now->copy()->sub($interval);
        $first_snapshot_before_interval = $this->get_last_linking_status_before_timestamp($one_interval_ago_dt, false);
        if(!$first_snapshot_before_interval) {
        // se non esistono snapshot per la presa prima di $one_interval_ago_dt, ritorno false:
            return false;
        }
        // leggo la soglia della subscription relativa all'allarme al momento del $last_linking_snapshot_timestamp:
        $threshold = $connected_subscription_at_last_linking_timestamp->get_min_power_alarm_threshold($last_linking_snapshot_timestamp);
        if(floatval($threshold) === floatval(0)) {
        // se la soglia della subscription è 0, ritorno false:
            return false;
        }
        $seconds_between_last_linking_snapshot_and_first_snapshot_before_interval = $last_linking_snapshot->get_timestamp()->diffInSeconds($first_snapshot_before_interval->get_timestamp());
        if($seconds_between_last_linking_snapshot_and_first_snapshot_before_interval < $interval->totalSeconds) {
        // se tra $last_linking_snapshot e $first_snapshot_before_interval è trascorsa meno di 1h, ritorno false:
            return false;
        }
        $always_same_subscription_in_interval = $this->always_the_same_subscription_was_used_on_the_socket_during_interval($first_snapshot_before_interval->get_timestamp(), $last_linking_snapshot->get_timestamp());
        if(!$always_same_subscription_in_interval) {
        // se nell'intervallo di tempo compreso tra le snapshot, sulla presa si sono alternate più subscriptions, ritorno false:
            return false;
        }
        // altrimenti, calcolo il consumo effettuato sulla presa tra le due snapshot:
        $epower_id_has_changed_during_interval = false;
        if($connected_subscription_at_last_linking_timestamp->is_prepaid()) {
            $socket_power_consumption_in_interval = $this->get_consumption_in_interval($first_snapshot_before_interval->get_timestamp(), $last_linking_snapshot->get_timestamp(), ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_TOTAL, ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME);
        } else {
            $socket_power_consumption_in_interval = $this->get_consumption_in_interval($first_snapshot_before_interval->get_timestamp(), $last_linking_snapshot->get_timestamp(), ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_POSTPAID, ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME);
        }
        if($epower_id_has_changed_during_interval === true) {
        // se risulta che sulla presa sia stata cambiata l'epower durante il periodo in esame, restituisco false: non potendo sapere se l'allarme è effettivamente da attivare, non lo attivo:
            return false;
        }
        if($socket_power_consumption_in_interval > $threshold) {
        // se il consumo trovato supera la soglia relativa all'allarme settata per la subscription:
            return false;
        }
        // altrimenti, controllo se in tutte le snapshot, la presa è stata accesa (PER QUESTO ALLARME NON POSSO IGNORARE EVENTUALI DISCONNESSIONI, PERCHE SE CI SONO STATE, LA PRESA POTEVA ESSERE SPENTA)
        if($this->was_always_on_during_interval($first_snapshot_before_interval->get_timestamp(), $last_linking_snapshot->get_timestamp(), false)) {
        // se in tutte le snapshot la presa è stata accesa, ritorno true:
            return true;
        }
        // altrimenti ritorno false:
        return false;
    }

    public function get_current_max_power_consumption_alarm_status(SocketSnapshot $snapshot): bool {
        $now = Carbon::now(config('app.timezone'));
        if($snapshot->get_link_status() !== $snapshot::LINK_STATUS_NOT_CONNECTED) {
        // se la presa sta attualmente comunicando
            // mantengo come riferimento lo snapshot passato come parametro:
            $last_linking_snapshot = $snapshot;
        } else {
        // se la presa NON sta attualmente comunicando
            // recupero l'ultimo stato della presa nel quale stava comunicando:
            // TODO: ma ha senso? se voglio calcolare lo stato dell'allarme all'istante relativo a SocketSnapshot, e a quell'istante la presa non stava comunicando, l'allarme sarà false e basta (quindi dovrò ritornare false qui e fare a meno di usare $last_linking_snapshot, usando solo $snapshot). Soprattutto se questo metodo viene usato solo al momento dell'inserimento dello snapshot sul db per poter settare il campo allarmi
            $last_linking_snapshot = $this->get_last_linking_status_before_timestamp($now, false);
        }
        if((!$last_linking_snapshot) || ($last_linking_snapshot->count() === 0)) {
        // se non esiste nessuno snapshot precedente a $snapshot nel quale la presa stava comunicando, l'allarme non è attivo
            return false;
        }
        $last_linking_snapshot_timestamp = $snapshot->get_timestamp();
        // cerco la subscription presente sulla presa all'istante dello snapshot appena recuperato
		if(boolval($last_linking_snapshot->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
			$connected_subscription_at_last_linking_timestamp = $last_linking_snapshot->subscription;
        } else {
        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
        	$connected_subscription_at_last_linking_timestamp = $last_linking_snapshot->retrieve_connected_subscription();
        }
        if((!$connected_subscription_at_last_linking_timestamp) || ($connected_subscription_at_last_linking_timestamp->count() === 0)) {
        // se a quello snapshot non era attiva nessuna subscription sulla presa, l'allarme non è attivo:
            return false;
        }
        $interval = Carboninterval::createFromFormat('i', strval(config('marina.power_alarms_minutes')));
        $difference_between_now_and_last_linking_snapshot_timestamp = $last_linking_snapshot_timestamp->diffAsCarbonInterval($now, true);
        if($difference_between_now_and_last_linking_snapshot_timestamp->totalSeconds > $interval->totalSeconds) {
        // se lo stato presa usato come riferimento è relativo a più di <$interval> tempo fa:
            // ritorno false: non posso sapere se l'allarme è attivo:
            return false;
        }
        // recupero lo stato della presa <interval> fa:
        $one_interval_ago_dt = $now->copy()->sub($interval);
        $first_snapshot_before_interval = $this->get_last_linking_status_before_timestamp($one_interval_ago_dt, false);
        if(!$first_snapshot_before_interval) {
        // se non esistono snapshot per la presa prima di $one_interval_ago_dt, ritorno false:
            return false;
        }
        // leggo la soglia della subscription relativa all'allarme al momento del $last_linking_snapshot_timestamp:
        $threshold = $connected_subscription_at_last_linking_timestamp->get_max_power_alarm_threshold($last_linking_snapshot_timestamp);
        if(floatval($threshold) === floatval(0)) {
        // se la soglia della subscription è 0, ritorno false:
            return false;
        }
        $always_same_subscription_in_interval = $this->always_the_same_subscription_was_used_on_the_socket_during_interval($first_snapshot_before_interval->get_timestamp(), $last_linking_snapshot->get_timestamp());
        if(!$always_same_subscription_in_interval) {
        // se nell'intervallo di tempo compreso tra le snapshot, sulla presa si sono alternate più subscriptions, ritorno false:
            return false;
        }
        // altrimenti, calcolo il consumo effettuato sulla presa tra le due snapshot:
        $epower_id_has_changed_during_interval = false;
        if($connected_subscription_at_last_linking_timestamp->is_prepaid()) {
            $socket_power_consumption_in_interval = $this->get_consumption_in_interval($first_snapshot_before_interval->get_timestamp(), $last_linking_snapshot->get_timestamp(), ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_TOTAL, ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME);
        } else {
            $socket_power_consumption_in_interval = $this->get_consumption_in_interval($first_snapshot_before_interval->get_timestamp(), $last_linking_snapshot->get_timestamp(), ConsumptionReportsController::REPORT_VALUE_POWER, $epower_id_has_changed_during_interval, ConsumptionReportsController::REPORT_FLAG_POSTPAID, ConsumptionReportsController::REPORT_MODE_COMPARED_TO_ELAPSED_TIME);
        }
        if($epower_id_has_changed_during_interval === true) {
        // se risulta che sulla presa sia stata cambiata l'epower durante il periodo in esame, restituisco false: non potendo sapere se l'allarme è effettivamente da attivare, non lo attivo:
            return false;
        }
        // in questo caso non mi interessa sapere se è passata più o meno di un'ora nell'intervallo in esame, e nemmeno sapere se la presa è stata accesa durante tutto il periodo: se il consumo supera la soglia dell'allarme, ritorno true:
        if($socket_power_consumption_in_interval >= $threshold) {
        // e se il consumo rilevato tra le snapshot super la soglia dell'allarme
            // posso già ritornare true:
            return true;
        }
        // altrimenti ritorno false:
        return false;
        // TODO: ma forse allora ha senso mettere i metodi di calculate nella subscription
    }

    public function get_current_min_socket_credit_alarm_status(SocketSnapshot $snapshot): bool {
		if(boolval($snapshot->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
			$connected_subscription = $snapshot->subscription;
        } else {
        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
        	$connected_subscription = $snapshot->retrieve_connected_subscription();
        }
        if((!$connected_subscription) || ($connected_subscription->count() === 0)) {
            return false;
        }


        if($connected_subscription instanceof \Illuminate\Database\Eloquent\Collection) {
        // TODO: devo usare first() perchè connected_subscription() mi restituisce sempre una collection di un elemento. Sistemare in modo che restituisca un record anzichè una collection.
            $connected_subscription = $connected_subscription->first();
        }

        
        if(!(floatval($connected_subscription->get_min_socket_credit_alarm_threshold()) > 0)) {
            return false;
        }
        if($snapshot->get_residual_credit() <= $connected_subscription->get_min_socket_credit_alarm_threshold()) {
            return true;
        }
        return false;
    }

    public function get_current_max_water_tap_time_alarm_status_for_admins(SocketSnapshot $snapshot): bool {
    // TODO: ha molto codice in comune con get_current_max_water_tap_time_alarm_status_for_users(): posso estrarre parte del codice in un metodo da usare in entrambi i casi, per evitare duplicazione?
        $now = Carbon::now(config('app.timezone'));
        $threshold = intval(config('marina.max_water_tap_time_alarm_threshold_for_admin_users_minutes'));
        if(!($threshold > 0)) {
            return false;
        }
        if($snapshot->get_link_status() !== $snapshot::LINK_STATUS_NOT_CONNECTED) {
        // se la presa sta attualmente comunicando
            // mantengo come riferimento lo snapshot passato come parametro:
            $last_linking_snapshot = $snapshot;
        } else {
        // se la presa NON sta attualmente comunicando
            // recupero l'ultimo stato della presa nel quale stava comunicando:
            // TODO: ma ha senso? se voglio calcolare lo stato dell'allarme all'istante relativo a SocketSnapshot, e a quell'istante la presa non stava comunicando, l'allarme sarà false e basta (quindi dovrò ritornare false qui e fare a meno di usare $last_linking_snapshot, usando solo $snapshot). Soprattutto se questo metodo viene usato solo al momento dell'inserimento dello snapshot sul db per poter settare il campo allarmi
            $last_linking_snapshot = $this->get_last_linking_status_before_timestamp($now, false);
        }
        if((!$last_linking_snapshot) || ($last_linking_snapshot->count() === 0)) {
        // se non esiste nessuno snapshot precedente a $snapshot nel quale la presa stava comunicando, l'allarme non è attivo
            return false;
        }
        if($now->diffInMinutes($last_linking_snapshot->get_timestamp(), true) > $threshold) {
        // se lo stato presa usato come riferimento è relativo a + di <$threshold> tempo fa
            // ritorno false: non posso sapere se l'allarme è attivo:
            return false;
        }
        $one_interval_ago_dt = $now->copy()
                                   ->subRealMinutes($threshold);
        if(!($this->was_always_on_during_interval($one_interval_ago_dt, $last_linking_snapshot->get_timestamp(), false))) {
        // se nell'intervallo, la presa ha avuto disconnessioni o spegnimenti, ritorno false:
            // (non ignoro le disconnessioni mettendo a true il terzo parametro perchè - nei momenti in cui la presa non comunicava - non posso essere sicuro che il consumo d'acqua fosse in crescita, non avendo i dati)
            return true;
        }
        // recupero tutte le snapshot comprese nell'intervallo:
        // TODO: per risparmare tempo, avrebbe senso lanciare la query in modo da mantenere sufficientemente distanziate le snapshot selezionate già a partire dalla select (v. la funzione della variabile $minimum_seconds_between_snapshots_to_see_consumption_growth). La cosa si potrebbe fare sia aggiungendo un parametro al metodo get_snapshots_in_interval con il quale specifico i secondi minimi tra uno snapshot e l'altro, sia scrivendo un metodo differente; valutare cosa è meglio. Potrei risparmiare parecchio dato che in questo modo mi eviterei i controlli e i continue() all'interno del for(), dato che l'sql è tipicamente più veloce di un ciclo php. PERO' ATTENZIONE! DEVO RICORDARE DI LANCIARLA DOPO LA was_always_on_during_interval() (mantenendo $ignore_disconnections a false) e la always_the_same_subscription_was_used_on_the_socket_during_interval(), altrimenti rischio di perdermi le snapshot in cui la presa era usata da qualcun altro o non comunicava!
        $snapshots_in_interval = $this->get_snapshots_in_interval($one_interval_ago_dt, $last_linking_snapshot->get_timestamp());
        // recupero della lettura immediatamente precedente l'inizio dell'intervallo in esame:
        $first_snapshot_before_interval =  $this->get_last_linking_status_before_timestamp($one_interval_ago_dt, true);
        if($first_snapshot_before_interval) {
        // se trovo una lettura immediatamente precedente l'inizio dell'intervallo in esame:
            // ne salvo il contatore di consumo ed il timestamp:
            $previous_snapshot_timestamp = $first_snapshot_before_interval->get_timestamp();
            $previous_snapshot_water_cnt = $first_snapshot_before_interval->get_total_water_cnt();
        } else {
        // se NON trovo una lettura immediatamente precedente l'inizio dell'intervallo in esame (tipicamente perchè l'intervallo in esame inizia allo stesso momento in cui sul db iniziano le snapshot per la presa):
            // prendo il primo snapshot della presa all'interno dell'intervallo. Tanto, all'accensione la presa invia sempre uno snapshot prima di iniziare il consumo, quindi non mi sto perdendo consumo precedente:
            $first_snapshot_in_interval = $this->get_first_link_status_occurrence_after_timestamp($one_interval_ago_dt, SocketSnapshot::LINK_STATUS_CONNECTED, false);
            $previous_snapshot_timestamp = $first_snapshot_in_interval->get_timestamp();
            $previous_snapshot_water_cnt = $first_snapshot_in_interval->get_total_water_cnt();
        }
        $water_consumption_grew_throughout_interval = true;
        /* di seguito calcolo la differenza minima che dovrò tenere tra le snapshot da controllare per poter apprezzare un consumo di acqua da parte della presa (che fissiamo in 5 litri tra uno snapshot e l'altro).
        Per chiarire: ponendo di avere una soglia minima di consumo orario acqua di 6 l/minuto (quindi 360 l/h) per poter apprezzare un consumo, allora per poter vedere un consumo di almeno 5 litri tra gli snapshot, dovrò leggerli ad una distanza di (60 * 5) / 6 = 50 secondi l'uno dall'altro */
        $minimum_seconds_between_snapshots_to_see_consumption_growth = config('marina.max_water_tap_time_alarm_minimum_consumption_in_liters_between_snapshots_to_see_growth_in_consumption') / config('marina.max_water_tap_time_alarm_minimum_consumption_in_liters_per_minute_for_water_tap_to_be_considered_open');
        foreach($snapshots_in_interval as $current_snapshot) {
        // scorro tutte le snapshot comprese nell'intervallo:
            $current_snapshot_timestamp = $current_snapshot->get_timestamp();
            // calcolo la differenza in secondi tra lo snapshot attuale e quello precedente:
            $interval_since_previous_snapshot_in_minutes = $current_snapshot_timestamp->diffInRealMinutes($previous_snapshot_timestamp);
            $interval_since_previous_snapshot_in_seconds = $interval_since_previous_snapshot_in_minutes * 60;
            if($interval_since_previous_snapshot_in_seconds < $minimum_seconds_between_snapshots_to_see_consumption_growth) {
            /* se tra questo snapshot e quello precedente non è passato abbastanza tempo per poter vedere un consumo di acqua apprezzabile, passo al prossimo snapshot.
            (grazie a questo check posso anche non preoccuparmi nel caso in cui abbia preso lo snapshot con il consumo di partenza dall'interno dell'intervallo anzichè prima: in questo caso mi accorgerei che non è passato abbastanza tempo dallo snapshot precedente e salterei alla prossima iterazione): */
                continue;
            }
            $current_snapshot_water_cnt = $current_snapshot->get_total_water_cnt();
            // calcolo i litri/minuto consumati tra lo snapshot attuale e quello precedente:
            $water_consumption_since_previous_snapshot_in_liters_per_minute = (($current_snapshot_water_cnt - $previous_snapshot_water_cnt) * 1000) / $interval_since_previous_snapshot_in_minutes;
            // su SmartCloud 1.x a questo punto controllavo se la presa era ancora accesa, ma qui l'ho già fatto per l'intero intervallo prima di entrare nel ciclo
            if($water_consumption_since_previous_snapshot_in_liters_per_minute < config('marina.max_water_tap_time_alarm_minimum_consumption_in_liters_per_minute_for_water_tap_to_be_considered_open')) {
            // se tra questo snapshot e l'ultimo snapshot considerato, il consumo in litri/minuto è stato minore di quello minimo affinchè il rubinetto possa considerarsi aperto, posso considerare che la crescita del consumo sia stata interrotta (ovvero che il rubinetto sia stato chiuso)
                $water_consumption_grew_throughout_interval = false;
                break;
            }
            $previous_snapshot_timestamp = $current_snapshot_timestamp;
            $previous_snapshot_water_cnt = $current_snapshot_water_cnt;
        }
        // se alla fine del ciclo, $water_consumption_grew_throughout_interval è ancora true, significa che il rubinetto è rimasto aperto per tutto il periodo esaminato, quindi l'allarme sarà attivo. Se al contrario, $water_consumption_grew_throughout_interval è false, il rubinetto è stato chiuso durante l'intervallo, quindi l'allarme non sarà attivo:
        return $water_consumption_grew_throughout_interval;
    }

    public function get_snapshots_in_interval(Carbon $begin_dt, Carbon $end_dt): ?Collection {
        // TODO: meglio usare usare <=/=> come faccio ora, oppure è meglio un'altra combinazione di [<, >, <= e >=]? O ancora lasciare la scelta aggiungendo 1-2 parametri?
        $begin_dt->setTimezone(config('app.timezone'));
        $end_dt->setTimezone(config('app.timezone'));
        $snapshots_in_interval = $this->snapshots()
                                      ->where('timestamp', '>=', $begin_dt)
                                      ->where('timestamp', '<=', $end_dt)
                                      ->get();
        return $snapshots_in_interval;
    }

    public function get_current_max_water_tap_time_alarm_status_for_users(SocketSnapshot $snapshot): bool {
    // TODO: ha molto codice in comune con get_current_max_water_tap_time_alarm_status_for_admins(): posso estrarre parte del codice in un metodo da usare in entrambi i casi, per evitare duplicazione?
        $now = Carbon::now(config('app.timezone'));
        if(boolval($snapshot->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
			$connected_subscription = $snapshot->subscription;
        } else {
        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
        	$connected_subscription = $snapshot->retrieve_connected_subscription();
        }

        if((!$connected_subscription) || ($connected_subscription->count() === 0)) {
            return false;
        }
        $threshold = $connected_subscription->get_max_water_tap_time_alarm_threshold();
        if(!(floatval($threshold) > 0)) {
            return false;
        }
        if($snapshot->get_link_status() !== $snapshot::LINK_STATUS_NOT_CONNECTED) {
        // se la presa sta attualmente comunicando
            // mantengo come riferimento lo snapshot passato come parametro:
            $last_linking_snapshot = $snapshot;
        } else {
        // se la presa NON sta attualmente comunicando
            // recupero l'ultimo stato della presa nel quale stava comunicando:
            // TODO: ma ha senso? se voglio calcolare lo stato dell'allarme all'istante relativo a SocketSnapshot, e a quell'istante la presa non stava comunicando, l'allarme sarà false e basta (quindi dovrò ritornare false qui e fare a meno di usare $last_linking_snapshot, usando solo $snapshot). Soprattutto se questo metodo viene usato solo al momento dell'inserimento dello snapshot sul db per poter settare il campo allarmi
            $last_linking_snapshot = $this->get_last_linking_status_before_timestamp($now, false);
        }
        if((!$last_linking_snapshot) || ($last_linking_snapshot->count() === 0)) {
        // se non esiste nessuno snapshot precedente a $snapshot nel quale la presa stava comunicando, l'allarme non è attivo
            return false;
        }
        $last_linking_snapshot_timestamp = $snapshot->get_timestamp();      // TODO: o forse dovrei prenderlo da $last_linking_snapshot?
        // cerco la subscription presente sulla presa all'istante dello snapshot appena recuperato:
        $connected_subscription_at_last_linking_timestamp = $last_linking_snapshot->connected_subscription;


        if($connected_subscription_at_last_linking_timestamp instanceof \Illuminate\Database\Eloquent\Collection) {
        // TODO: devo usare first() perchè connected_subscription() mi restituisce sempre una collection di un elemento. Sistemare in modo che restituisca un record anzichè una collection.
            $connected_subscription_at_last_linking_timestamp = $connected_subscription_at_last_linking_timestamp->first();
        }


        if((!$connected_subscription_at_last_linking_timestamp) || ($connected_subscription_at_last_linking_timestamp->count() === 0)) {
        // se a quello snapshot non era attiva nessuna subscription sulla presa, l'allarme non è attivo:
            return false;
        }
        if($now->diffInMinutes($last_linking_snapshot_timestamp, true) > $threshold) {
        // se lo stato presa usato come riferimento è relativo a + di <$threshold> tempo fa
            // ritorno false: non posso sapere se l'allarme è attivo:
            return false;
        }
        $one_interval_ago_dt = $now->copy()->subRealMinutes($threshold);
        if(!($this->was_always_on_during_interval($one_interval_ago_dt, $last_linking_snapshot->get_timestamp(), false))) {
        // se nell'intervallo, la presa ha avuto disconnessioni o spegnimenti, ritorno false:
            // (non ignoro le disconnessioni perchè nei momenti in cui la presa non comunicava, non posso essere sicuro che il consumo d'acqua fosse in crescita, non avendo i dati)
            return true;
        }
        $always_same_subscription_in_interval = $this->always_the_same_subscription_was_used_on_the_socket_during_interval($one_interval_ago_dt, $last_linking_snapshot->get_timestamp());
        if(!$always_same_subscription_in_interval) {
        // se nell'intervallo di tempo compreso tra le snapshot, sulla presa si sono alternate più subscriptions, ritorno false:
            return false;
        }
        // recupero tutte le snapshot comprese nell'intervallo:
        // TODO: per risparmare tempo, avrebbe senso lanciare la query in modo da mantenere sufficientemente distanziate le snapshot selezionate già a partire dalla select (v. la funzione della variabile $minimum_seconds_between_snapshots_to_see_consumption_growth). La cosa si potrebbe fare sia aggiungendo un parametro al metodo get_snapshots_in_interval con il quale specifico i secondi minimi tra uno snapshot e l'altro, sia scrivendo un metodo differente; valutare cosa è meglio. Potrei risparmiare parecchio dato che in questo modo mi eviterei i controlli e i continue() all'interno del for(), dato che l'sql è tipicamente più veloce di un ciclo php. PERO' ATTENZIONE! DEVO RICORDARE DI LANCIARLA DOPO LA was_always_on_during_interval() (mantenendo $ignore_disconnections a false) e la always_the_same_subscription_was_used_on_the_socket_during_interval(), altrimenti rischio di perdermi le snapshot in cui la presa era usata da qualcun altro o non comunicava!
        $snapshots_in_interval = $this->get_snapshots_in_interval($one_interval_ago_dt, $last_linking_snapshot_timestamp);
        // recupero della lettura immediatamente precedente l'inizio dell'intervallo in esame:
        $first_snapshot_before_interval =  $this->get_last_linking_status_before_timestamp($one_interval_ago_dt, true);
        if($first_snapshot_before_interval) {
        // se trovo una lettura immediatamente precedente l'inizio dell'intervallo in esame:
            // ne salvo il contatore di consumo ed il timestamp:
            $previous_snapshot_timestamp = $first_snapshot_before_interval->get_timestamp();
            $previous_snapshot_water_cnt = $first_snapshot_before_interval->get_total_water_cnt();
        } else {
        // se NON trovo una lettura immediatamente precedente l'inizio dell'intervallo in esame (tipicamente perchè l'intervallo in esame inizia allo stesso momento in cui sul db iniziano le snapshot per la presa):
            // prendo il primo snapshot della presa all'interno dell'intervallo. Tanto, all'accensione la presa invia sempre uno snapshot prima di iniziare il consumo, quindi non mi sto perdendo consumo precedente:
            $first_snapshot_in_interval = $this->get_first_link_status_occurrence_after_timestamp($one_interval_ago_dt, SocketSnapshot::LINK_STATUS_CONNECTED, false);
            $previous_snapshot_timestamp = $first_snapshot_in_interval->get_timestamp();
            $previous_snapshot_water_cnt = $first_snapshot_in_interval->get_total_water_cnt();
        }
        $water_consumption_grew_throughout_interval = true;
        /* di seguito calcolo la differenza minima che dovrò tenere tra le snapshot da controllare per poter apprezzare un consumo di acqua da parte della presa (che fissiamo in 5 litri tra uno snapshot e l'altro).
        Per chiarire: ponendo di avere una soglia minima di consumo orario acqua di 6 l/minuto (quindi 360 l/h) per poter apprezzare un consumo, allora per poter vedere un consumo di almeno 5 litri tra gli snapshot, dovrò leggerli ad una distanza di (60 * 5) / 6 = 50 secondi l'uno dall'altro */
        $minimum_seconds_between_snapshots_to_see_consumption_growth = config('marina.max_water_tap_time_alarm_minimum_consumption_in_liters_between_snapshots_to_see_growth_in_consumption') / config('marina.max_water_tap_time_alarm_minimum_consumption_in_liters_per_minute_for_water_tap_to_be_considered_open');
        foreach($snapshots_in_interval as $current_snapshot) {
        // scorro tutte le snapshot comprese nell'intervallo:
            $current_snapshot_timestamp = $current_snapshot->get_timestamp();
            // calcolo la differenza in secondi tra lo snapshot attuale e quello precedente:
            $interval_since_previous_snapshot_in_minutes = $current_snapshot_timestamp->diffInRealMinutes($previous_snapshot_timestamp);
            $interval_since_previous_snapshot_in_seconds = $interval_since_previous_snapshot_in_minutes * 60;
            if($interval_since_previous_snapshot_in_seconds < $minimum_seconds_between_snapshots_to_see_consumption_growth) {
            /* se tra questo snapshot e quello precedente non è passato abbastanza tempo per poter vedere un consumo di acqua apprezzabile, passo al prossimo snapshot.
            (grazie a questo check posso anche non preoccuparmi nel caso in cui abbia preso lo snapshot con il consumo di partenza dall'interno dell'intervallo anzichè prima: in questo caso mi accorgerei che non è passato abbastanza tempo dallo snapshot precedente e salterei alla prossima iterazione): */
                continue;
            }
            $current_snapshot_water_cnt = $current_snapshot->get_total_water_cnt();
            // calcolo i litri/minuto consumati tra lo snapshot attuale e quello precedente:
            $water_consumption_since_previous_snapshot_in_liters_per_minute = (($current_snapshot_water_cnt - $previous_snapshot_water_cnt) * 1000) / $interval_since_previous_snapshot_in_minutes;
            // su SmartCloud 1.x a questo punto controllavo se la presa era ancora accesa, ma qui l'ho già fatto per l'intero intervallo prima di entrare nel ciclo
            if($water_consumption_since_previous_snapshot_in_liters_per_minute < config('marina.max_water_tap_time_alarm_minimum_consumption_in_liters_per_minute_for_water_tap_to_be_considered_open')) {
            // se tra questo snapshot e l'ultimo snapshot considerato, il consumo in litri/minuto è stato minore di quello minimo affinchè il rubinetto possa considerarsi aperto, posso considerare che la crescita del consumo sia stata interrotta (ovvero che il rubinetto sia stato chiuso)
                $water_consumption_grew_throughout_interval = false;
                break;
            }
            $previous_snapshot_timestamp = $current_snapshot_timestamp;
            $previous_snapshot_water_cnt = $current_snapshot_water_cnt;
        }
        // se alla fine del ciclo, $water_consumption_grew_throughout_interval è ancora true, significa che il rubinetto è rimasto aperto per tutto il periodo esaminato, quindi l'allarme sarà attivo. Se al contrario, $water_consumption_grew_throughout_interval è false, il rubinetto è stato chiuso durante l'intervallo, quindi l'allarme non sarà attivo:
        return $water_consumption_grew_throughout_interval;
    }

    public function had_the_same_epower_id_during_interval(Carbon $begin_dt, Carbon $end_dt = null): bool {
    // dato un intervallo delimitato da $begin_dt e $end_dt (nullable: se null, utilizza now), restituisce true se durante l'intero intervallo la presa ha sempre avuto lo stesso epower_id, false altrimenti.
        $begin_dt->setTimezone(config('app.timezone'));
        if(!is_null($end_dt)) {
            $end_dt->setTimezone(config('app.timezone'));
        } else {
            $end_dt = Carbon::now(config('app.timezone'));
        }
        $result = true;
        $all_socket_snapshots_in_interval_count = $this->snapshots()
                                                       ->where('timestamp', '>=', $begin_dt)
                                                       ->where('timestamp', '<=', $end_dt)
                                                       ->where('link_status', '!=', SocketSnapshot::LINK_STATUS_NOT_CONNECTED)
                                                       ->count();
        if($all_socket_snapshots_in_interval_count > 0) {
            $first_socket_snapshot_in_interval = $this->get_first_link_status_occurrence_after_timestamp($begin_dt, SocketSnapshot::LINK_STATUS_CONNECTED, false);
            if(!is_null($first_socket_snapshot_in_interval)) {
                $epower_id_from_first_snapshot_in_interval = $first_socket_snapshot_in_interval->get_epower_id();
                $socket_snapshots_in_interval_with_same_epowerid_than_first_snapshot_count  = $this->snapshots()
                                                                                                   ->where('timestamp', '>=', $begin_dt)
                                                                                                   ->where('timestamp', '<=', $end_dt)
                                                                                                   ->where('link_status', '!=', SocketSnapshot::LINK_STATUS_NOT_CONNECTED)
                                                                                                   ->where('epower_id', '=', $epower_id_from_first_snapshot_in_interval)
                                                                                                   ->count();
                if($all_socket_snapshots_in_interval_count !== $socket_snapshots_in_interval_with_same_epowerid_than_first_snapshot_count) {
                    $result = false;
                }
            }
        }
        return $result;
    }
}
