<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * App\BaseSocketSnapshot
 *
 * @property int $id
 * @property int $socket_id
 * @property Carbon $timestamp
 * @property string $link_status
 * @property string $on_off_status
 * @property string $total_power_cnt
 * @property string $postpaid_power_cnt
 * @property string $total_water_cnt
 * @property string $postpaid_water_cnt
 * @property string $transponder_id
 * @property string $residual_credit
 * @property int $epower_id
 * @property string $instant_power_consumption
 * @property string|null $alarms
 * @property int $keep_snapshot_flag
 * @property int $keep_next_snapshot_flag
 * @property-read \App\Berth $berth
 * @property-read \App\Transponder $transponder
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot query()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereAlarms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereEpowerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereInstantPowerConsumption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereKeepNextSnapshotFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereKeepSnapshotFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereLinkStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereOnOffStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot wherePostpaidPowerCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot wherePostpaidWaterCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereResidualCredit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereSocketId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereTimestamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereTotalPowerCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereTotalWaterCnt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BaseSocketSnapshot whereTransponderId($value)
 * @mixin \Eloquent
 */
class BaseSocketSnapshot extends Model {
/* Questo è il model di base delle SocketSnapshot, sul quale non viene fatto caching. Da questo derivano:
     - SocketSnapshot: qui viene fatto caching, perchè sono dati non necessariamente recentissimi e tendenzialmente pesanti da caricare (soprattutto nel caso del calcolo dei consumi, generazione grafici...)
     - SocketLastSnapshot: qui sto valutando se fare il caching e ogni quanto farlo, perchè questo model si basa sulla view (db) SocketLastSnapshot e non su una tabella vera e propria. Questa view (db) contiene il SocketSnapshot più recente per ciascuna presa. Ad agosto 2020 abbiamo notato che la view (pagina) socket_status non sempre mostra la entry più recente di ogni presa. Sospetto che sia perchè stavo facendo il caching anche dei socket_last_snapshot. In questo modo provo a differenziare la politica di caching tra i due model. */

    use Traits\TimeZoneAware;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'socket_snapshots';
    protected $primaryKey = 'id';

    public $timestamps = false;
    protected $dates = ['timestamp', 'deleted_at'];
    public $dateFormat = 'Y-m-d H:i:s';

    protected $temporary_alarms_from_smartview_array;
    protected $alarms_have_been_calculated;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'socket_id',
        'timestamp',
        'link_status',
        'on_off_status',
        'total_power_cnt',
        'postpaid_power_cnt',
        'total_water_cnt',
        'postpaid_water_cnt',
        'transponder_id',
        'transponder_id_field_is_reliable',
        'subscription_id',
        'can_retrieve_subscription_via_subscription_id',
        'residual_credit',
        'epower_id',
        'instant_power_consumption',
        'alarms',
        'keep_snapshot_flag',
        'keep_next_snapshot_flag',
    ];

    protected $casts = [
        'transponder_id_field_is_reliable' => 'boolean',
        'keep_snapshot_flag' => 'boolean',
        'keep_next_snapshot_flag' => 'boolean',
    ];

    public const LINK_STATUS_CONNECTED = '0NORMAL';
    public const LINK_STATUS_NOT_CONNECTED = '1ERROR';
    public const LINK_STATUS_THEFT_ALARM = '1ALLARMEFURTO';
    public const SOCKET_STATUS_ON = 'ON';
    public const SOCKET_STATUS_OFF = 'OFF';
    public const SOCKET_STATUS_NOT_CONNECTED = 'ERR';
    public const SOCKET_STATUS_DISABLED = 'DISAB.';
    public const SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED = 'ON_T';
    public const SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED = 'OFF_T';
    public const SOCKET_STATUS_FORCED = 'FORCED';
    public const TRANSPONDER_ID_VALUE_RECEIVED_FROM_SMARTVIEW_WHEN_TRANSPONDER_IS_NULL = '00000000';        // valore del campo transponder_id ricevuto dal supervisore quando la presa è offline o è appena stata resettata (NOTA: ad oggi 09/04/2020 non lo uso mai per assegnarlo a transponder_id, lo uso solo per i confronti)

    public const ALARMS_FIELD_FLAG_NOT_CONNECTED = 'E';
    public const ALARMS_FIELD_FLAG_THEFT = 'T';
    public const ALARMS_FIELD_FLAG_MIN_POWER_CONSUMPTION = 'L';
    public const ALARMS_FIELD_FLAG_MAX_POWER_CONSUMPTION = 'H';
    public const ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_USERS = 'W';
    public const ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_ADMINS = 'P';         // non potendo più usare "W" (water) o "T" (tap) indico "P" per "pipe", dato che lo scopo è aiutare lo staff del marina ad individuare eventuali tubature rotte nel pontile
    public const ALARMS_FIELD_FLAG_MIN_CREDIT = 'C';

    protected const TEMPORARY_ALARM_FROM_SMARTVIEW_NOT_CONNECTED = 'NO COM';
    protected const TEMPORARY_ALARM_FROM_SMARTVIEW_THEFT = 'THEFT';
    // TODO: al momento  non gestisco gli allarmi che seguono. Li ho riportati qui solo perchè li ho trovati nel sorgente di SmartView. Vanno gestiti e visualizzati:
    protected const TEMPORARY_ALARM_FROM_SMARTVIEW_TRANSACTION_NOT_COMPLETED = 'T.N.T';         // stato di transazione non terminata
    protected const TEMPORARY_ALARM_FROM_SMARTVIEW_WATER_LEAK = 'PERD.ACQUA';                   // TODO: come viene calcolato questo allarme da smartview? potrebbe essere l'equivalente del mio WATER_TIME_ALARM?
    protected const TEMPORARY_ALARM_FROM_SMARTVIEW_FORCED = 'FORCED';

    public function __construct() {
        parent::__construct();
        $this->temporary_alarms_from_smartview_array = array();
    }

    protected function serializeDate(DateTimeInterface $date) {
    // metodo per mantenere retrocompatibilità dopo l'upgrade a Laravel 7 (v. https://laravel.com/docs/7.x/upgrade#date-serialization)
        return $date->format('Y-m-d H:i:s');
    }

    public function get_id(): int {
    // restituisce l'id del record snapshot
        return intval($this->attributes[$this->primaryKey]);
    }

    public function set_id(int $id): bool {
    // imposta l'id del record snapshot al valore passato, se è maggiore di zero. Restiutisce false se l'id passato è minore di zero.
        if($id > 0) {
            $this->attributes[$this->primaryKey] = $id;
            return true;
        } else {
            return false;
        }
    }

    public function berth() {
        return $this->belongsTo(Berth::class, 'socket_id');
    }

    public function get_berth_id(): ?int {
        return $this->socket_id;
    }

    public function set_berth_id(?int $value): bool {
        $this->socket_id = $value;
        return true;
    }

    public function get_timestamp(): Carbon {
    // restituisce il timestamp dello snapshot
        return $this->ConvertToLocalTimeZone($this->timestamp);
    }

    public function set_timestamp(Carbon $timestamp): bool {
    // imposta il timestamp dello snapshot
        $timestamp->setTimezone(config('app.timezone'));
        $this->timestamp = $this->ConvertToDefaultDatabaseTimeZone($timestamp);
        return true;
    }

    public function get_link_status(): string {
    // restituisce lo stato di comunicazione della presa
        return strval($this->link_status);
    }

    public function set_link_status(string $link_status): bool {
    // imposta lo stato di comunicazione della presa, se combacia con uno dei valori previsti. Restituisce false se lo stato passato non corrisponde a nessuno dei valori previsti.
        if(($link_status === $this::LINK_STATUS_CONNECTED) ||
           ($link_status === $this::LINK_STATUS_NOT_CONNECTED) ||
           ($link_status === $this::LINK_STATUS_THEFT_ALARM)) {
            $this->link_status = $link_status;
            return true;
        } else {
            return false;
        }
    }

    public function get_on_off_status(): string {
    // restituisce lo stato on/off/err della presa
        return strval($this->on_off_status);
    }

    public function set_on_off_status(string $on_off_status): bool {
    // imposta lo stato on/off/err della presa, se combacia con uno dei valori previsti. Restiutisce false se lo stato passato non corrisponde a nessuno dei valori previsti.
        if(($on_off_status === $this::SOCKET_STATUS_ON) ||
           ($on_off_status === $this::SOCKET_STATUS_OFF) ||
           ($on_off_status === $this::SOCKET_STATUS_NOT_CONNECTED) ||
           ($on_off_status === $this::SOCKET_STATUS_DISABLED) ||
           ($on_off_status === $this::SOCKET_STATUS_ON_TRANSACTION_NOT_COMPLETED) ||
           ($on_off_status === $this::SOCKET_STATUS_OFF_TRANSACTION_NOT_COMPLETED) ||
           ($on_off_status === $this::SOCKET_STATUS_FORCED)) {
            $this->on_off_status = $on_off_status;
            return true;
        } else {
            return false;
        }
    }

    public function socket_is_on(): bool {
    // restituisce true se la presa sta comunicando ed è accesa, false in tutti gli altri casi
        return boolval($this->on_off_status === $this::SOCKET_STATUS_ON);
    }

    public function socket_is_off(): bool {
    // restituisce true se la presa sta comunicando ed è spenta, false in tutti gli altri casi
        return boolval($this->on_off_status === $this::SOCKET_STATUS_OFF);
    }

    public function socket_on_off_status_has_changed_from_snapshot(SocketSnapshot $other_snapshot) {
        if($this->on_off_status !== $other_snapshot->on_off_status) {
            return true;
        }
        return false;
    }

    public function get_total_power_cnt(): float {
    // restituisce il contatore totale di consumo energia della presa
        return floatval($this->total_power_cnt);
    }

    public function set_total_power_cnt(float $total_power_cnt): bool {
    // imposta il contatore totale di consumo energia della presa al parametro passato, se maggiore o uguale a zero. Restituisce false se il parametro è negativo.
        if($total_power_cnt >= 0) {
            $this->total_power_cnt = $total_power_cnt;
            return true;
        } else {
            return false;
        }
    }

    public function get_postpaid_power_cnt(): float {
    // restituisce il contatore di consumo energia per le utenze postpagate della presa
        return floatval($this->postpaid_power_cnt);
    }

    public function set_postpaid_power_cnt(float $postpaid_power_cnt): bool {
    // imposta il contatore di consumo energia per le utenze postpagate della presa al parametro passato, se maggiore o uguale a zero. Restituisce false se il parametro è negativo.
        if($postpaid_power_cnt >= 0) {
            $this->postpaid_power_cnt = $postpaid_power_cnt;
            return true;
        } else {
            return false;
        }
    }

    public function get_total_water_cnt(): float {
    // restituisce il contatore totale di consumo acqua della presa
        return floatval($this->total_water_cnt);
    }

    public function set_total_water_cnt(float $total_water_cnt): bool {
    // imposta il contatore totale di consumo acqua della presa al parametro passato, se maggiore o uguale a zero. Restituisce false se il parametro è negativo.
        if($total_water_cnt >= 0) {
            $this->total_water_cnt = $total_water_cnt;
            return true;
        } else {
            return false;
        }
    }

    public function get_postpaid_water_cnt(): float {
    // restituisce il contatore di consumo acqua per le utenze postpagate della presa
        return floatval($this->postpaid_water_cnt);
    }

    public function set_postpaid_water_cnt(float $postpaid_water_cnt): bool {
    // imposta il contatore di consumo acqua per le utenze postpagate della presa al parametro passato, se maggiore o uguale a zero. Restituisce false se il parametro è negativo.
        if($postpaid_water_cnt >= 0) {
            $this->postpaid_water_cnt = $postpaid_water_cnt;
            return true;
        } else {
            return false;
        }
    }

    public function transponder() {
        return $this->belongsTo(Transponder::class, 'transponder_id', 'code');
    }

    public function get_transponder_id(): string {
    // restituisce il transponder ID dello snapshot
        return strval($this->transponder_id);
    }

    public function set_transponder_id(string $transponder_id): void {
    // imposta il transponder ID dello snapshot al parametro passato
        // TODO: validarlo per verificare se è di 8 caratteri e abortire + restituire false in caso contrario?
        $this->transponder_id = $transponder_id;
    }

    public function get_transponder_id_field_is_reliable_to_retrieve_connected_subscription_flag(): bool {
    // restituisce il valore del flag che avvisa se l'ultima volta che la presa è stata accesa, la subscription attuale non era ancora in vigore (e di conseguenza, se il calcolo della subscription attualmente collegata allo snapshot non è affidabile)
        return boolval($this->transponder_id_field_is_reliable);
    }

    public function set_transponder_id_field_is_reliable_to_retrieve_connected_subscription_flag(bool $value) {
    // imposta il valore del flag che avvisa se l'ultima volta che la presa è stata accesa, la subscription attuale non era ancora in vigore (e di conseguenza, se il calcolo della subscription attualmente collegata allo snapshot non è affidabile)
        $this->transponder_id_field_is_reliable = $value;
    }

    public function subscription() {
        return $this->belongsTo(Subscription::class);
    }

    public function get_subscription_id(): ?int {
    // restituisce il subscription_id dello snapshot
        return strval($this->subscription_id);
    }

    public function set_subscription_id(?int $subscription_id): void {
    // imposta il subscription_id dello snapshot al parametro passato
        $this->subscription_id = $subscription_id;
    }

    public function connected_subscription_can_be_retrieved_using_subscription_id_field() {
        return boolval($this->can_retrieve_subscription_via_subscription_id);
    }

    public function set_can_retrieve_subscription_via_subscription_id_flag(bool $value): void {
		$this->can_retrieve_subscription_via_subscription_id = $value;
    }    

    public function get_residual_credit(): float {
    // restituisce il credito residuo sulla presa
        return floatval($this->residual_credit);
    }

    public function set_residual_credit(float $residual_credit): bool {
    // imposta il credito residuo sulla presa al parametro passato, se maggiore o uguale a zero. Restituisce false se il parametro è negativo.
        if($residual_credit >= 0) {
            $this->residual_credit = $residual_credit;
            return true;
        } else {
            return false;
        }
    }

    public function get_epower_id(): int {
    // restituisce l'id della scheda epower montata sulla presa
        return intval($this->epower_id);
    }

    public function set_epower_id(int $epower_id) {
    // imposta l'id della scheda epower montata sulla presa al parametro passato
        $this->epower_id = $epower_id;
    }

    public function get_instant_power_consumption(): float {
    // restituisce il consumo istantaneo di energia rilevato sulla presa
        return floatval($this->instant_power_consumption);
    }

    public function set_instant_power_consumption(float $instant_power_consumption): bool {
    // imposta il consumo istantaneo di energia rilevato sulla presa al parametro passato, se maggiore o uguale a zero. Restituisce false se il parametro è negativo.
        if($instant_power_consumption >= 0) {
            $this->instant_power_consumption = $instant_power_consumption;
            return true;
        } else {
            return false;
        }
    }

    public function set_temporary_alarms_from_smartview(string $alarm_string_from_smartview): void {
        $this->temporary_alarms_from_smartview_array[] = $alarm_string_from_smartview;
    }

    public function save(array $options = []) {
    // faccio l'override del metodo save() del model per poter calcolare gli allarmi al momento della scrittura del record. Restituisce true se il record è stato creato sul db al momento del save(), false se esisteva già prima del save e l'operazione è stata un update
        // per poter calcolare gli allarmi, devo prima salvare il record sul db:
        $return_value = parent::save($options);
        // verifico anche se il transponder_id dello snapshot ha già una corrispondenza in anagrafica. Se non ce l'ha, dovrò inserirlo (altrimenti, nel momento in cui verrà creata una subscription legata a quel transponder, mancando il transponder in anagrafica non potrò calcolarne il proprietario):
        $snapshot_transponder_id = $this->get_transponder_id();
        if(($snapshot_transponder_id) && ($snapshot_transponder_id !== $this::TRANSPONDER_ID_VALUE_RECEIVED_FROM_SMARTVIEW_WHEN_TRANSPONDER_IS_NULL)) {
            $bound_transponder = Transponder::findByCode($snapshot_transponder_id);
            if((!$bound_transponder) || ($bound_transponder->count() === 0)) {
                $bound_transponder = new Transponder;
                $bound_transponder->set_code($snapshot_transponder_id);
                // per default setto il nuovo transponder come NON usato per le socket subscriptions. Verrà settato come usato per le socket subscriptions solo nel momento in cui verrà usato per crearne una:
                $bound_transponder->set_as_used_for_socket_subscriptions(false);
                $bound_transponder->save();
            }
        }
        // poi recupero l'anagrafica della presa:
        $berth = Berth::find($this->socket_id);
        if($berth) {
        // se l'anagrafica della presa esiste già sul db, posso procedere con il calcolo degli allarmi (se lo facessi senza l'anagrafica, avrei degli errori in fase di calcolo):
            $this->calculate_and_set_triggered_alarms();
            // salvo di nuovo il record per rendere effettiva la modifica al campo allarmi:
            $return_value = parent::save($options);
        }
        return $return_value;
    }

    public function is_theft_alarm_set(): bool {
        if(strpos($this->alarms, $this::ALARMS_FIELD_FLAG_THEFT) !== false) {
            return true;
        }
        return false;
    }

    protected function set_theft_alarm_status(bool $value): void {
        // recupero l'attuale stringa allarmi dello snapshot:
        $alarms_string = strval($this->alarms);
        switch($value) {
            case true:
                // se il valore da settare per l'allarme è true
                if(!($this->is_theft_alarm_set())) {
                // e attualmente l'allarme NON E' settato:
                    // aggiunge al campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= $this::ALARMS_FIELD_FLAG_THEFT;
                }
                break;
            case false:
                // se il valore da settare per l'allarme è false
                if($this->is_theft_alarm_set()) {
                // e attualmente l'allarme E' settato:
                    // toglie dal campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= trim($alarms_string, $this::ALARMS_FIELD_FLAG_THEFT);
                }
        }
        if(strlen($alarms_string) > 0) {
        // a questo punto, se la stringa allarmi ottenuta è più lunga di zero caratteri, la assegna effettivamente al campo allarmi dello snapshot
            $this->alarms = $alarms_string;
        }
    }

    public function is_not_connected_alarm_set(): bool {
        if(strpos($this->alarms, $this::ALARMS_FIELD_FLAG_NOT_CONNECTED) !== false) {
            return true;
        }
        return false;
    }

    protected function set_not_connected_alarm_status(bool $value): void {
        // recupero l'attuale stringa allarmi dello snapshot:
        $alarms_string = strval($this->alarms);
        switch($value) {
            case true:
                // se il valore da settare per l'allarme è true
                if(!($this->is_not_connected_alarm_set())) {
                // e attualmente l'allarme NON E' settato:
                    // aggiunge al campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= $this::ALARMS_FIELD_FLAG_NOT_CONNECTED;
                }
                break;
            case false:
                // se il valore da settare per l'allarme è false
                if($this->is_not_connected_alarm_set()) {
                // e attualmente l'allarme E' settato:
                    // toglie dal campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= trim($alarms_string, $this::ALARMS_FIELD_FLAG_NOT_CONNECTED);
                }
        }
        if(strlen($alarms_string) > 0) {
        // a questo punto, se la stringa allarmi ottenuta è più lunga di zero caratteri, la assegna effettivamente al campo allarmi dello snapshot
            $this->alarms = $alarms_string;
        }
    }

    public function is_min_socket_credit_alarm_set(): bool {
        if(strpos($this->alarms, $this::ALARMS_FIELD_FLAG_MIN_CREDIT) !== false) {
            return true;
        }
        return false;
    }

    protected function set_min_socket_credit_alarm_status(bool $value): void {
        // recupero l'attuale stringa allarmi dello snapshot:
        $alarms_string = strval($this->alarms);
        switch($value) {
            case true:
                // se il valore da settare per l'allarme è true
                if(!($this->is_min_socket_credit_alarm_set())) {
                // e attualmente l'allarme NON E' settato:
                    // aggiunge al campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= $this::ALARMS_FIELD_FLAG_MIN_CREDIT;
                }
                break;
            case false:
                // se il valore da settare per l'allarme è false
                if($this->is_min_socket_credit_alarm_set()) {
                // e attualmente l'allarme E' settato:
                    // toglie dal campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= trim($alarms_string, $this::ALARMS_FIELD_FLAG_MIN_CREDIT);
                }
        }
        if(strlen($alarms_string) > 0) {
        // a questo punto, se la stringa allarmi ottenuta è più lunga di zero caratteri, la assegna effettivamente al campo allarmi dello snapshot
            $this->alarms = $alarms_string;
        }
    }

    public function is_min_power_consumption_alarm_set(): bool {
        if(strpos($this->alarms, $this::ALARMS_FIELD_FLAG_MIN_POWER_CONSUMPTION) !== false) {
            return true;
        }
        return false;
    }

    protected function set_min_power_consumption_alarm_status(bool $value): void {
        // recupero l'attuale stringa allarmi dello snapshot:
        $alarms_string = strval($this->alarms);
        switch($value) {
            case true:
                // se il valore da settare per l'allarme è true
                if(!($this->is_min_power_consumption_alarm_set())) {
                // e attualmente l'allarme NON E' settato:
                    // aggiunge al campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= $this::ALARMS_FIELD_FLAG_MIN_POWER_CONSUMPTION;
                }
                break;
            case false:
                // se il valore da settare per l'allarme è false
                if($this->is_min_power_consumption_alarm_set()) {
                // e attualmente l'allarme E' settato:
                    // toglie dal campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= trim($alarms_string, $this::ALARMS_FIELD_FLAG_MIN_POWER_CONSUMPTION);
                }
        }
        if(strlen($alarms_string) > 0) {
        // a questo punto, se la stringa allarmi ottenuta è più lunga di zero caratteri, la assegna effettivamente al campo allarmi dello snapshot
            $this->alarms = $alarms_string;
        }
    }

    public function is_max_power_consumption_alarm_set(): bool {
        if(strpos($this->alarms, $this::ALARMS_FIELD_FLAG_MAX_POWER_CONSUMPTION) !== false) {
            return true;
        }
        return false;
    }

    protected function set_max_power_consumption_alarm_status(bool $value): void {
        // recupero l'attuale stringa allarmi dello snapshot:
        $alarms_string = strval($this->alarms);
        switch($value) {
            case true:
                // se il valore da settare per l'allarme è true
                if(!($this->is_max_power_consumption_alarm_set())) {
                // e attualmente l'allarme NON E' settato:
                    // aggiunge al campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= $this::ALARMS_FIELD_FLAG_MAX_POWER_CONSUMPTION;
                }
                break;
            case false:
                // se il valore da settare per l'allarme è false
                if($this->is_max_power_consumption_alarm_set()) {
                // e attualmente l'allarme E' settato:
                    // toglie dal campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= trim($alarms_string, $this::ALARMS_FIELD_FLAG_MAX_POWER_CONSUMPTION);
                }
        }
        if(strlen($alarms_string) > 0) {
        // a questo punto, se la stringa allarmi ottenuta è più lunga di zero caratteri, la assegna effettivamente al campo allarmi dello snapshot
            $this->alarms = $alarms_string;
        }
    }

    public function is_max_water_tap_time_alarm_for_admins_set(): bool {
        if(strpos($this->alarms, $this::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_ADMINS) !== false) {
            return true;
        }
        return false;
    }

    protected function set_max_water_time_alarm_status_for_admins(bool $value): void {
        // recupero l'attuale stringa allarmi dello snapshot:
        $alarms_string = strval($this->alarms);
        switch($value) {
            case true:
                // se il valore da settare per l'allarme è true
                if(!($this->is_max_water_tap_time_alarm_for_admins_set())) {
                // e attualmente l'allarme NON E' settato:
                    // aggiunge al campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= $this::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_ADMINS;
                }
                break;
            case false:
                // se il valore da settare per l'allarme è false
                if($this->is_max_water_tap_time_alarm_for_admins_set()) {
                // e attualmente l'allarme E' settato:
                    // toglie dal campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= trim($alarms_string, $this::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_ADMINS);
                }
        }
        if(strlen($alarms_string) > 0) {
        // a questo punto, se la stringa allarmi ottenuta è più lunga di zero caratteri, la assegna effettivamente al campo allarmi dello snapshot
            $this->alarms = $alarms_string;
        }
    }

    public function is_max_water_tap_time_alarm_for_users_set(): bool {
        if(strpos($this->alarms, $this::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_USERS) !== false) {
            return true;
        }
        return false;
    }

    protected function set_max_water_time_alarm_status_for_users(bool $value): void {
        // recupero l'attuale stringa allarmi dello snapshot:
        $alarms_string = strval($this->alarms);
        switch($value) {
            case true:
                // se il valore da settare per l'allarme è true
                if(!($this->is_max_water_tap_time_alarm_for_users_set())) {
                // e attualmente l'allarme NON E' settato:
                    // aggiunge al campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= $this::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_USERS;
                }
                break;
            case false:
                // se il valore da settare per l'allarme è false
                if($this->is_max_water_tap_time_alarm_for_users_set()) {
                // e attualmente l'allarme E' settato:
                    // toglie dal campo allarmi la lettera relativa a questo allarme:
                    $alarms_string .= trim($alarms_string, $this::ALARMS_FIELD_FLAG_MAX_WATER_TIME_FOR_USERS);
                }
        }
        if(strlen($alarms_string) > 0) {
        // a questo punto, se la stringa allarmi ottenuta è più lunga di zero caratteri, la assegna effettivamente al campo allarmi dello snapshot
            $this->alarms = $alarms_string;
        }
    }

    public function calculate_and_set_triggered_alarms(): void {
    // calcola lo stato degli allarmi innescati dallo snapshot - chiamando i metodi deputati al calcolo di ciascun allarme - poi chiama i metodi privati deputati a settare di conseguenza il campo allarmi per ciascun allarme
        $berth = $this->berth;
        if((!$berth) || ($berth->count() === 0)) {
        // se l'anagrafica della presa non esiste, setta tutti gli allarmi a false ed esce:
            // (questo è un caso possibile su impianti nuovi o comunque work in progress: magari il TagManager non mi ha ancora inviato le anagrafiche)
            return;
        }
        // verifico se - nel campo allarmi temporaneo inviato dal supervisore - è presente la stringa che segnala l'allarme di mancata comunicazione o l'allarme antifurto:
        $alarms_from_smartview_contains_missing_communication_alarm = $alarms_from_smartview_contains_theft_alarm = false;
        foreach($this->temporary_alarms_from_smartview_array as $temporary_alarm_from_smartview_entry) {
            if(strpos($temporary_alarm_from_smartview_entry, $this::TEMPORARY_ALARM_FROM_SMARTVIEW_NOT_CONNECTED) > 0) {
                $alarms_from_smartview_contains_missing_communication_alarm = true;
            }
            if(strpos($temporary_alarm_from_smartview_entry, $this::TEMPORARY_ALARM_FROM_SMARTVIEW_THEFT) > 0) {
                $alarms_from_smartview_contains_theft_alarm = true;
            }
        }
        if(($alarms_from_smartview_contains_missing_communication_alarm === true) || ($berth->get_current_not_connected_alarm_status($this) === true)) {
        // se nel campo allarmi temporaneo inviato dal supervisore ho trovato un allarme di mancata comunicazione attivo, oppure se questo snapshot mi triggera l'allarme di mancata comunicazione, setto l'allarme a true:
            $this->set_not_connected_alarm_status(true);
            // in questo caso, tutti gli allarmi che seguono saranno per forza inattivi. Ne setto lo stato:
            $this->set_theft_alarm_status(false);
            $this->set_max_water_time_alarm_status_for_admins(false);
            $this->set_min_power_consumption_alarm_status(false);
            $this->set_max_power_consumption_alarm_status(false);
            $this->set_min_socket_credit_alarm_status(false);
            $this->set_max_water_time_alarm_status_for_users(false);
        } else {
        // altrimenti lo setto a false:
            $this->set_not_connected_alarm_status(false);
            // in questo caso ha senso controllare anche gli altri allarmi:
            if(($alarms_from_smartview_contains_theft_alarm === true) || ($berth->get_current_theft_alarm_status($this) === true)) {
            // se nel campo allarmi temporaneo inviato dal supervisore ho trovato un allarme furto attivo, oppure se questo snapshot mi triggera l'allarme furto, setto l'allarme a true:
                $this->set_theft_alarm_status(true);
            } else {
                $this->set_theft_alarm_status(false);
            }
            // per tutti gli altri allarmi, chiamo i metodi della classe Berth deputati alla verifica e setto il campo allarmi di conseguenza:
            if($berth->get_current_max_water_tap_time_alarm_status_for_admins($this) === true) {
                $this->set_max_water_time_alarm_status_for_admins(true);
            } else {
                $this->set_max_water_time_alarm_status_for_admins(false);
            }
            if($berth->get_current_min_power_consumption_alarm_status($this) === true) {
                $this->set_min_power_consumption_alarm_status(true);
            } else {
                $this->set_min_power_consumption_alarm_status(false);
            }
            if($berth->get_current_max_power_consumption_alarm_status($this) === true) {
                $this->set_max_power_consumption_alarm_status(true);
            } else {
                $this->set_max_power_consumption_alarm_status(false);
            }
            if($berth->get_current_min_socket_credit_alarm_status($this) === true) {
                $this->set_min_socket_credit_alarm_status(true);
            } else {
                $this->set_min_socket_credit_alarm_status(false);
            }
            if($berth->get_current_max_water_tap_time_alarm_status_for_users($this) === true) {
                $this->set_max_water_time_alarm_status_for_users(true);
            } else {
                $this->set_max_water_time_alarm_status_for_users(false);
            }
        }
        $this->alarms_have_been_calculated = true;
    }

    public function set_keep_snapshot_flag(bool $value) {
    // imposta il keep_snapshot_flag del SocketSnapshot al valore passato
        $this->keep_snapshot_flag = $value;
        return true;
    }

    public function can_be_deleted(): bool {
    // restituisce false se lo snapshot non è essenziale per il calcolo dei consumi e può essere sovrascritto (dall'APIv0Controller quando - durante l'inserimento di un nuovo SocketSnapshot - il valore di $need_to_create_a_new_snapshot_record risulta false) oppure eliminato (durante le operazioni di pulizia del db, che al momento in cui scrivo questo commento ancora non faccio ma vorrei aggiungere in futuro)
        return !(boolval($this->keep_snapshot_flag));
    }

    public function set_keep_next_snapshot_flag(bool $value) {
    // imposta il keep_next_snapshot_flag del SocketSnapshot al valore passato
        $this->keep_next_snapshot_flag = $value;
        return true;
    }

    public function can_next_snapshot_be_deleted(): bool {
    // restituisce false se lo snapshot successivo a questo non è essenziale per il calcolo dei consumi e può essere sovrascritto (dall'APIv0Controller quando - durante l'inserimento di un nuovo SocketSnapshot - il valore di $need_to_create_a_new_snapshot_record risulta false) oppure eliminato (durante le operazioni di pulizia del db, che al momento in cui scrivo questo commento ancora non faccio ma vorrei aggiungere in futuro)
        return !(boolval($this->keep_next_snapshot_flag));
    }

    public function all_subscriptions_with_same_transponder_id() {
    // restituisce tutte le subscriptions che hanno lo stesso transponder_id di $this (indipendentemente dal fatto che siano in corso o meno)
        $all_subscriptions_with_same_transponder_id = Subscription::where('transponder_id', $this->transponder_id);
        if((Auth::check()) && (!Auth::user()->is_supervisor())) {
            $all_subscriptions_with_same_transponder_id = $all_subscriptions_with_same_transponder_id->excludeSupervisorRelatedSubscriptions();
        }
        return $all_subscriptions_with_same_transponder_id;
    }

    private function ongoing_subscriptions_with_same_transponder_id() {
    // restituisce tutte le subscriptions che hanno lo stesso transponder_id di $this e che erano in corso all'istante $this->timestamp
        $ongoing_subscriptions_with_same_transponder_id = $this->all_subscriptions_with_same_transponder_id()
                                                               ->activeDuringWholeInterval($this->timestamp, $this->timestamp);
        if((Auth::check()) && !(Auth::user()->is_supervisor())) {
            $ongoing_subscriptions_with_same_transponder_id = $ongoing_subscriptions_with_same_transponder_id->excludeSupervisorRelatedSubscriptions();
        }
        return $ongoing_subscriptions_with_same_transponder_id;
    }

    public function retrieve_connected_subscription() {
        if(($this->transponder) && ($this->transponder()->count() > 0)) {
            $connected_subscription = $this->transponder
                                           ->active_subscriptions()
                                           ->orderBy('begin_dt', 'desc');
            if($this->transponder->is_used_for_socket_subscriptions()) {
                $connected_subscription_on_same_socket_as_this_snapshot_relationship = $this->transponder
                                                                                            ->active_subscriptions()
                                                                                            ->where('socket_id', $this->socket_id)
                                                                                            ->orderBy('begin_dt', 'desc');
                if($connected_subscription_on_same_socket_as_this_snapshot_relationship->count() > 0) {
                    $connected_subscription = $connected_subscription_on_same_socket_as_this_snapshot_relationship;
                }
            }
        } else {
            $connected_subscription = $this->ongoing_subscriptions_with_same_transponder_id()
                                           ->orderBy('begin_dt', 'desc');
        }
        if((Auth::check()) && !(Auth::user()->is_supervisor())) {
            $connected_subscription = $connected_subscription->excludeSupervisorRelatedSubscriptions();
        }
        $connected_subscription = $connected_subscription->first();
        return $connected_subscription;
    }

    public function triggers_same_alarms_as(SocketSnapshot $other_snapshot): bool {
    // dato un altro snapshot, verifica se per entrambi sono attivi gli stessi allarmi
        if(!$this->exists) {            // ATTENZIONE: la property exists è diversa dal metodo omonimo, qui è corretto usare la property (vedere qui per le differenze: https://laracasts.com/discuss/channels/laravel/what-is-the-difference-between-exists-and-exists)
        // se questo snapshot è nuovo (ovvero se non è stato pescato dal db, ma devo ancora scrivercelo per la prima volta)
            if(!isset($this->alarms_have_been_calculated)) {
            // controllo se ne ho già fatto il calcolo degli allarmi: se non l'ho ancora fatto, non posso sapere se triggera gli stessi allarmi di $other_snapshot:
                // perciò restituisco false:
                return false;
            }
        }
        return $this->alarms === $other_snapshot->alarms;
    }

    public function has_same_values_except_for_timestamp_as(SocketSnapshot $other_snapshot, &$reason = null): bool {
    // dato un altro snapshot, verifica se entrambi hanno lo stesso socketID, transponderID, linkStatus, onOffStatus, gli stessi contatori di consumo, stesso credito disponibile sulla presa, stesso consumo istantaneo e stesso ePowerID. In pratica, se differiscono solamente per il timestamp. Lo uso in fase di ricezione di un nuovo snapshot da parte di una presa, per determinare se ha senso scriverne uno nuovo o aggiornare il più recente già presente sul db per la stessa presa. Prende in ingresso un parametro opzionale $reason, passato per riferimento: se viene passato, è possibile sapere qual è il dato che differisce tra i due snapshot (se le snapshot differiscono tra loro per più di un dato, verrà riportato solo l'ultimo verificato)

        $snapshots_have_same_values = true;
        
        $snapshots_have_same_values = $snapshots_have_same_values && ($this->get_berth_id() === $other_snapshot->get_berth_id());
        if(!$snapshots_have_same_values) { 
            $reason = '(at least) the value of socket_id is different from the previous snapshot'; 
            return $snapshots_have_same_values;
        }

        $snapshots_have_same_values = $snapshots_have_same_values && ($this->get_link_status() === $other_snapshot->get_link_status());
        if(!$snapshots_have_same_values) { 
            $reason = '(at least) the value of link_status is different from the previous snapshot';
            return $snapshots_have_same_values;
        }

        $snapshots_have_same_values = $snapshots_have_same_values && ($this->get_on_off_status() === $other_snapshot->get_on_off_status());
        if(!$snapshots_have_same_values) { 
            $reason = '(at least) the value of on_off_status is different from the previous snapshot';
            return $snapshots_have_same_values;
        }

        $snapshots_have_same_values = $snapshots_have_same_values && ($this->get_transponder_id() === $other_snapshot->get_transponder_id());
        if(!$snapshots_have_same_values) { 
            $reason = '(at least) the value of transponder_id is different from the previous snapshot';
            return $snapshots_have_same_values;
        }

        $snapshots_have_same_values = $snapshots_have_same_values && ($this->get_total_power_cnt() === $other_snapshot->get_total_power_cnt());
        if(!$snapshots_have_same_values) { 
            $reason = '(at least) the value of total_power_cnt is different from the previous snapshot';
            return $snapshots_have_same_values;
        }

        $snapshots_have_same_values = $snapshots_have_same_values && ($this->get_total_water_cnt() === $other_snapshot->get_total_water_cnt());
        if(!$snapshots_have_same_values) { 
            $reason = '(at least) the value of total_water_cnt is different from the previous snapshot';
            return $snapshots_have_same_values;
        }
               
        // qui non ha senso confrontare i contatori di consumo postpagato, perchè se si incrementano i totali, si incrementano anche quelli
        
        $snapshots_have_same_values = $snapshots_have_same_values && ($this->get_residual_credit() === $other_snapshot->get_residual_credit());
        if(!$snapshots_have_same_values) { 
            $reason = '(at least) the value of residual_credit is different from the previous snapshot';
            return $snapshots_have_same_values; 
        }
        
        $snapshots_have_same_values = $snapshots_have_same_values && ($this->get_epower_id() === $other_snapshot->get_epower_id());
        if(!$snapshots_have_same_values) { 
            $reason = '(at least) the value of epower_id is different from the previous snapshot';
            return $snapshots_have_same_values;
        }
        
        /*
        // evito di confrontare le potenze istantanee, perchè è molto difficile trovare due snapshot che abbiano lo stesso valore:
        $snapshots_have_same_values = $snapshots_have_same_values && ($this->get_instant_power_consumption() === $other_snapshot->get_instant_power_consumption());
        if(!$snapshots_have_same_values) { $reason = 'the value of instant_power_consumption is different from the previous snapshot'; }
        */

        // non confronto il campo alarms perchè questo metodo viene chiamato su uno snapshot prima del metodo save(), quando quel campo non è ancora stato calcolato
	
        return $snapshots_have_same_values;
    }

    public function generate_and_write_subscription_snapshot_record(): ?SubscriptionSnapshot {
    /*  - valuta se scrivere un subscription_snapshot collegato all'istanza di SocketSnapshot
        - calcola il subscription_snapshot
        - scrive sul db il subscription_snapshot
    */
        // TODO: questo metodo fa un po' troppe cose, forse meglio splittarlo una volta che funziona correttamente

        // inizializzo i contatori di consumo per il subscription_snapshot che andrò a creare (tranne quelli del credito, che inizializzerò tra poco):
        $subscription_total_power_consumption = $subscription_total_water_consumption = $subscription_total_credit_consumption = $subscription_power_consumption_on_socket = $subscription_water_consumption_on_socket = $subscription_credit_consumption_on_socket = $subscription_power_consumption_on_socket_until_last_subscription_snapshot = $subscription_water_consumption_on_socket_until_last_subscription_snapshot = $subscription_credit_consumption_on_socket_until_last_subscription_snapshot = 0;
        $need_to_create_a_new_subscription_snapshot_record = false;
        // recupero alcuni valori dello snapshot corrente che riutilizzerò in seguito, per evitare di dover richiamare ogni volta i getter della classe SocketSnapshot:
        $current_socket_snapshot_timestamp = $this->get_timestamp();
        $current_socket_snapshot_on_off_status = $this->get_on_off_status();
        $current_socket_snapshot_residual_credit = $this->get_residual_credit();
        $berth = $this->berth;
        // recupero la subscription collegata al socket_snapshot:
        if(boolval($this->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
			// $connected_subscription = $this->subscription;   // TODO: non capisco perchè non funzioni, provo così:
            $connected_subscription = Subscription::find($this->subscription_id);
        } else {
        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
        	$connected_subscription = $this->retrieve_connected_subscription();
        }
        if(!($this->is_not_connected_alarm_set()) &&
           (($berth) && ($berth->count() > 0)) &&
           (($connected_subscription) && ($connected_subscription->count() > 0))) {
        // se l'anagrafica del posto barca al quale si riferisce $this esiste sul db, se all'istante dello snapshot la presa stava comunicando e se c'era una subscription collegata allo snapshot:
            // posso valutare di scrivere il subscription_snapshot:
            // inizializzo i contatori di consumo del credito per il subscription_snapshot che andrò a scrivere (a valori diversi a seconda che la subscription sia prepagata o postpagata):
            if($connected_subscription->is_prepaid()) {
                $subscription_total_credit_consumption = $subscription_credit_consumption_on_socket = $subscription_total_credit_consumption_until_last_subscription_snapshot = $subscription_credit_consumption_on_socket_until_last_subscription_snapshot = 0;
            } else {
                $subscription_total_credit_consumption = $subscription_credit_consumption_on_socket = $subscription_credit_consumption_on_socket_until_last_subscription_snapshot = $subscription_total_credit_consumption_until_last_subscription_snapshot = null;
            }
            // cerco l'ultimo snapshot per questa presa prima di $this:
            $previous_socket_snapshot = $berth->get_last_socket_snapshot_before_timestamp($current_socket_snapshot_timestamp, true);
            if((!$previous_socket_snapshot) || ($this->socket_on_off_status_has_changed_from_snapshot($previous_socket_snapshot))) {
            // se lo stato on/off della presa è cambiato rispetto allo snapshot precedente (oppure se questo è il primo snapshot per questa presa):
                // il record di SubscriptionSnapshot va certamente scritto:
                $need_to_create_a_new_subscription_snapshot_record = true;
            }
            // cerco l'ultimo subscription_snapshot per la subscription corrente:
            $last_subscription_snapshot_record = $connected_subscription->get_last_subscription_snapshot_before_timestamp($current_socket_snapshot_timestamp, true);
            if(is_null($last_subscription_snapshot_record) || ($last_subscription_snapshot_record->count() === 0)) {
            // se non esiste un subscription_snapshot precedente per questa subscription:
                // questo sarà il primo: devo scriverlo. Dato che per calcolarlo mi serviranno i contatori di consumo delle subscription_snapshot precedenti (e in questo caso non esistono), li inizializzo a zero:
                $need_to_create_a_new_subscription_snapshot_record = true;
                $subscription_total_power_consumption_until_last_subscription_snapshot = $subscription_total_water_consumption_until_last_subscription_snapshot = 0;
                if($connected_subscription->is_prepaid()) {
                    $subscription_total_credit_consumption = $subscription_credit_consumption_on_socket = $subscription_total_credit_consumption_until_last_subscription_snapshot = $subscription_credit_consumption_on_socket_until_last_subscription_snapshot = 0;
                } else {
                    $subscription_total_credit_consumption = $subscription_credit_consumption_on_socket = $subscription_credit_consumption_on_socket_until_last_subscription_snapshot = $subscription_total_credit_consumption_until_last_subscription_snapshot = null;
                }
            } else {
            // se esiste almeno uno snapshot per questa subscription sul db:
                // ne recupero i contatori di consumo:
                $subscription_total_power_consumption_until_last_subscription_snapshot = $last_subscription_snapshot_record->get_total_power_consumption();
                $subscription_total_water_consumption_until_last_subscription_snapshot = $last_subscription_snapshot_record->get_total_water_consumption();
                $subscription_total_credit_consumption_until_last_subscription_snapshot = $last_subscription_snapshot_record->get_total_credit_consumption();
                // per calcolare i contatori di consumo della subscription su questa presa, verifico se esiste un subscription_snapshot nel quale stava consumando su di essa (non è detto che coincida con $last_subscription_snapshot_record):
                $last_subscription_snapshot_record_on_same_socket = $connected_subscription->get_last_subscription_snapshot_on_berth_before_timestamp($current_socket_snapshot_timestamp, $berth->get_id(), true);
                if(!is_null($last_subscription_snapshot_record_on_same_socket) && ($last_subscription_snapshot_record_on_same_socket->count() > 0)) {
                // se esiste un subscription_snapshot per $connected_subscription su $berth prima di $current_socket_snapshot_timestamp:
                    // ne recupero i contatori di consumo:
                    $subscription_power_consumption_on_socket_until_last_subscription_snapshot =  $last_subscription_snapshot_record_on_same_socket->get_power_consumption_on_socket();
                    $subscription_water_consumption_on_socket_until_last_subscription_snapshot =  $last_subscription_snapshot_record_on_same_socket->get_water_consumption_on_socket();
                    if($connected_subscription->is_prepaid()) {
                        $subscription_credit_consumption_on_socket_until_last_subscription_snapshot =  $last_subscription_snapshot_record_on_same_socket->get_credit_consumption_on_socket();
                    }
                } else {
                    // se NON ESISTE un subscription_snapshot per $connected_subscription su $berth prima di $current_socket_snapshot_timestamp:
                    // significa che è la prima volta che $connected_subscription consuma su $socket_id, quindi i suoi contatori di consumo su di essa dovranno essere settati a zero (qui non devo fare nulla, li ho già inizializzati a zero prima)
                }
                $previous_socket_snapshot = $berth->get_last_socket_snapshot_before_timestamp($this->get_timestamp(), true);

                if(!is_null($previous_socket_snapshot) && ($previous_socket_snapshot->count() > 0)) {
                // se sul db esiste uno snapshot precedente all'istante $timestamp per questa presa (ho già fatto la query in precedenza, quindi mi basta recuperare $previous_socket_snapshot):
                    // controllo se anche in quel momento, chi stava consumando sulla presa era sempre la stessa subscription di ora:
					if(boolval($previous_socket_snapshot->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
			        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
						$previous_socket_snapshot_connected_subscription = $previous_socket_snapshot->subscription;
			        } else {
			        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
			        	$previous_socket_snapshot_connected_subscription = $previous_socket_snapshot->retrieve_connected_subscription();
			        }
                    if(!is_null($previous_socket_snapshot_connected_subscription) && ($previous_socket_snapshot_connected_subscription->count() > 0) && ($previous_socket_snapshot_connected_subscription->get_id() === $connected_subscription->get_id())) {
                    // se sì:
                    // calcolo la differenza di consumo tra il socket_snapshot appena ricevuto e quello precedente per la stessa subscription:
                        // verifico se l'epower montata sul posto barca è cambiata rispetto a $previous_socket_snapshot:
                        $epower_id_has_changed_since_previous_socket_snapshot = ($previous_socket_snapshot->get_epower_id() === $this->get_epower_id());
                        if($epower_id_has_changed_since_previous_socket_snapshot) {
                        // se l'epower montata sul posto barca all'istante $previous_socket_snapshot->get_timestamp() E' LA STESSA che è montata attualmente:
                            if($connected_subscription->is_prepaid()) {
                            // se la subscription è prepagata:
                                // il calcolo andrà fatto usando i contatori totali delle due socket_snapshot:
                                $power_consumption_counter_on_last_socket_snapshot = $previous_socket_snapshot->get_total_power_cnt();
                                $water_consumption_counter_on_last_socket_snapshot = $previous_socket_snapshot->get_total_water_cnt();
                                $credit_consumption_counter_on_last_socket_snapshot = $previous_socket_snapshot->get_residual_credit();
                                $power_consumption_counter_on_this_socket_snapshot = $this->get_total_power_cnt();
                                $water_consumption_counter_on_this_socket_snapshot = $this->get_total_water_cnt();
                            } else {
                            // se la subscription è postpagata:
                                // il calcolo andrà fatto usando i contatori di consumo postpagati delle socket_snapshot:
                                $power_consumption_counter_on_last_socket_snapshot = $previous_socket_snapshot->get_postpaid_power_cnt();
                                $water_consumption_counter_on_last_socket_snapshot = $previous_socket_snapshot->get_postpaid_water_cnt();
                                $credit_consumption_counter_on_last_socket_snapshot = null;
                                $power_consumption_counter_on_this_socket_snapshot = $this->get_postpaid_power_cnt();
                                $water_consumption_counter_on_this_socket_snapshot = $this->get_postpaid_water_cnt();
                            }
                        } else {
                        // se l'epower montata sul posto barca all'istante $previous_socket_snapshot->get_timestamp() NON E' LA STESSA che è montata attualmente:
                            // significa che l'epower è appena stata sostituita - e dato che ci sarà inevitabilmente un salto nei contatori di consumo tra le due socket_snapshot - devo fare in modo che questo salto non sia visibile nei subscription_snapshots. Inoltre, dato che questo sarà il primo subscription_snapshot calcolato su questo posto barca dopo la sostituzione, e come sappiamo, la prima rilevazione dei contatori di una presa dopo la sua accensione è uguale all'ultima.
                            // perciò questo caso è assimilabile alla prima volta che un contratto consuma su un determinato posto barca, e posso prendere contatori di consumo di partenza da questo stesso socket snapshot, in modo che la differenza calcolata rispetto all'ultimo subscription_snapshot risulti nulla:
                            if($connected_subscription->is_prepaid()) {
                            // se la subscription è prepagata:
                                // il calcolo andrà fatto usando i contatori totali:
                                $power_consumption_counter_on_last_socket_snapshot = $power_consumption_counter_on_this_socket_snapshot = $this->get_total_power_cnt();
                                $water_consumption_counter_on_last_socket_snapshot = $water_consumption_counter_on_this_socket_snapshot = $this->get_total_water_cnt();
                                // (per quanto riguarda il credito, posso tranquillamente continuare a prendere il valore precedente da $previous_socket_snapshot, dato che non è un valore del quale l'epower mantiene memoria):
                                $credit_consumption_counter_on_last_socket_snapshot = $previous_socket_snapshot->get_residual_credit();
                            } else {
                            // se la subscription è postpagata:
                                // il calcolo andrà fatto usando i contatori postpagati:
                                $power_consumption_counter_on_last_socket_snapshot = $power_consumption_counter_on_this_socket_snapshot = $this->get_postpaid_power_cnt();
                                $water_consumption_counter_on_last_socket_snapshot = $water_consumption_counter_on_this_socket_snapshot = $this->get_postpaid_water_cnt();
                                // mentre non c'è necessità di tenere conto dell'andamento del credito:
                                $credit_consumption_counter_on_last_socket_snapshot = null;
                            }
                        }
                        // calcolo l'incremento dei consumi della subscription rispetto all'ultimo snapshot:
                        $subscription_total_power_consumption_increment_since_last_subscription_snapshot = $power_consumption_counter_on_this_socket_snapshot - $power_consumption_counter_on_last_socket_snapshot;

                        $subscription_total_water_consumption_increment_since_last_subscription_snapshot = $water_consumption_counter_on_this_socket_snapshot - $water_consumption_counter_on_last_socket_snapshot;

                        // l'incremento dei consumi da registrare sulla subscription su questa presa, in questo caso sarà uguale a quello appena calcolato per l'intero impianto:
                        $subscription_power_consumption_on_socket_increment_since_last_subscription_snapshot = $power_consumption_counter_on_this_socket_snapshot - $power_consumption_counter_on_last_socket_snapshot;

                        $subscription_water_consumption_on_socket_increment_since_last_subscription_snapshot = $water_consumption_counter_on_this_socket_snapshot - $water_consumption_counter_on_last_socket_snapshot;

                        // TODO: siamo sicuri? non è che devo anche controllare che l'istante del $last_socket_snapshot_record sia uguale a quello del $last_subscription_snapshot_record prima di fare queste assunzioni?

                        // infine calcolo i nuovi contatori di consumo della subscription incrementando quelli del suo ultimo subscription_snapshot degli incrementi appena calcolati:
                        $subscription_total_power_consumption = $subscription_total_power_consumption_until_last_subscription_snapshot + $subscription_total_power_consumption_increment_since_last_subscription_snapshot;

                        $subscription_total_water_consumption = $subscription_total_water_consumption_until_last_subscription_snapshot + $subscription_total_water_consumption_increment_since_last_subscription_snapshot;

                        $subscription_power_consumption_on_socket = $subscription_power_consumption_on_socket_until_last_subscription_snapshot + $subscription_power_consumption_on_socket_increment_since_last_subscription_snapshot;

                        $subscription_water_consumption_on_socket = $subscription_water_consumption_on_socket_until_last_subscription_snapshot +  $subscription_water_consumption_on_socket_increment_since_last_subscription_snapshot;
                        if($connected_subscription->is_prepaid()) {
                        // se la subscription è prepagata:
                            // calcolo il consumo di credito della presa dall'ultimo snapshot
                            if(($current_socket_snapshot_on_off_status !== SocketSnapshot::SOCKET_STATUS_OFF)) {
                                // se la presa non è spenta:
                                $credit_consumption_counter_on_this_socket_snapshot = $current_socket_snapshot_residual_credit;
                                // (l'ordine dei sottraendi per il calcolo del consumo di credito è invertito rispetto ai consumi di energia ed acqua, perchè il credito cala anzichè crescere):
                                $subscription_credit_consumption_on_socket_increment_since_last_subscription_snapshot = $credit_consumption_counter_on_last_socket_snapshot - $credit_consumption_counter_on_this_socket_snapshot;
                                if($subscription_credit_consumption_on_socket_increment_since_last_subscription_snapshot < 0) {
                                    // dato che non avrebbe senso loggare gli incrementi di credito della presa (non mi porterebbe a conoscere gli importi delle ricariche effettuate sul transponder, perchè credito trasferito alla presa e credito disponibile sul transponder sono concetti diversi), se il consumo di credito risulta negativo, lo setto a zero prima di scriverlo nel subscription_snapshot:
                                    $subscription_credit_consumption_on_socket_increment_since_last_subscription_snapshot = $subscription_total_credit_consumption_increment_since_last_subscription_snapshot = 0;
                                }
                                $subscription_credit_consumption_on_socket = $subscription_credit_consumption_on_socket_until_last_subscription_snapshot +  $subscription_credit_consumption_on_socket_increment_since_last_subscription_snapshot;
                                $subscription_total_credit_consumption = $subscription_total_credit_consumption_until_last_subscription_snapshot + $subscription_credit_consumption_on_socket_increment_since_last_subscription_snapshot;
                            } else {
                                // se la presa è spenta:
                                // setto il consumo di credito della subscription agli ultimi valori disponibili
                                // (se calcolassi il consumo di credito allo stesso modo di quando la presa è accesa, conteggerei l'azzeramento del credito sulla presa come un consumo dell'intero credito che era disponibile allo snapshot precedente)
                                $subscription_credit_consumption_on_socket = $subscription_credit_consumption_on_socket_until_last_subscription_snapshot;
                                $subscription_total_credit_consumption = $subscription_total_credit_consumption_until_last_subscription_snapshot;
                            }
                        }
                        // dato che c'è stato un incremento nei consumi, dovrò scrivere un nuovo record di subscription_snapshot:
                        $need_to_create_a_new_subscription_snapshot_record = true;
                    } else {
                        // se allo snapshot precedente all'istante $timestamp per questa presa, la presa era occupata da una subscription diversa da $connected_subscription:
                        // significa che il consumo di $connected_subscription su questa presa è appena iniziato:
                        // recupero l'ultimo subscription_snapshot in cui $connected_subscription era utilizzata su questa presa prima di $timestamp:
                        $last_subscription_snapshot_record_on_same_socket = $connected_subscription->get_last_subscription_snapshot_on_berth_before_timestamp($current_socket_snapshot_timestamp, $berth->get_id(), true);
                        if(($last_subscription_snapshot_record_on_same_socket) && ($last_subscription_snapshot_record_on_same_socket->count() > 0)) {
                            // e se lo trovo
                            // utilizzo i suoi stessi contatori di consumo sulla presa nel subscription_snapshot che sto per scrivere (infatti a meno di errori di comunicazione - che non posso controllare - una presa mi manda uno snapshot appena viene accesa e prima che la subscription abbia iniziato a consumare):
                            $subscription_power_consumption_on_socket = $last_subscription_snapshot_record_on_same_socket->get_power_consumption_on_socket();
                            $subscription_water_consumption_on_socket = $last_subscription_snapshot_record_on_same_socket->get_water_consumption_on_socket();
                            $subscription_credit_consumption_on_socket = $last_subscription_snapshot_record_on_same_socket->get_credit_consumption_on_socket();
                        } else {
                            if($connected_subscription->is_prepaid()) {
                                $subscription_credit_consumption_on_socket = 0;
                            }
                        }
                        if(($last_subscription_snapshot_record) && ($last_subscription_snapshot_record->count() > 0)) {
                            // poi setto il contatori di consumo totali del subscription_snapshot che sto per scrivere al valore che avevano in $last_subscription_snapshot_record (se esiste):
                            $subscription_total_power_consumption = $last_subscription_snapshot_record->get_total_power_consumption();
                            $subscription_total_water_consumption = $last_subscription_snapshot_record->get_total_water_consumption();
                            $subscription_total_credit_consumption = $last_subscription_snapshot_record->get_total_credit_consumption();
                        }
                        // dato che c'è stato un cambiamento di presa per la subscription, dovrò scrivere un nuovo record di subscription_snapshot:
                        $need_to_create_a_new_subscription_snapshot_record = true;
                    }
                } else {
                // se sul db NON ESISTE uno snapshot precedente all'istante $timestamp per questa presa:
                    // posso presumere che questo sia il primo istante in cui questa presa inizia a consumare in tutto l'impianto:
                    // quindi dovrò scrivere un nuovo record di subscription_snapshot:
                    $need_to_create_a_new_subscription_snapshot_record = true;
                    // setto i contatori di consumo totali di $connected_subscription al valore che avevano in $last_subscription_snapshot_record:
                    $subscription_total_power_consumption = $last_subscription_snapshot_record->get_total_power_consumption();
                    $subscription_total_water_consumption = $last_subscription_snapshot_record->get_total_water_consumption();
                    // e setto i contatori di consumo di $connected_subscription su questa presa a zero:
                    $subscription_power_consumption_on_socket = $subscription_water_consumption_on_socket = 0;
                    if($connected_subscription->is_prepaid()) {
                        $subscription_credit_consumption_on_socket = $subscription_total_credit_consumption = 0;
                    }
                }
            }
        }
        if($need_to_create_a_new_subscription_snapshot_record) {
        // se ho determinato di dover scrivere il record di subscription_snapshot:
            // lo istanzio, lo popolo con i valori appena calcolati e lo salvo:
            $subscription_snapshot_record = new SubscriptionSnapshot();
            $subscription_snapshot_record->set_timestamp($current_socket_snapshot_timestamp);
            $subscription_snapshot_record->set_subscription_id($connected_subscription->get_id());
            $subscription_snapshot_record->set_berth_id($berth->get_id());
            $subscription_snapshot_record->set_total_power_consumption($subscription_total_power_consumption);
            $subscription_snapshot_record->set_total_water_consumption($subscription_total_water_consumption);
            $subscription_snapshot_record->set_total_credit_consumption($subscription_total_credit_consumption);
            $subscription_snapshot_record->set_power_consumption_on_socket($subscription_power_consumption_on_socket);
            $subscription_snapshot_record->set_water_consumption_on_socket($subscription_water_consumption_on_socket);
            $subscription_snapshot_record->set_credit_consumption_on_socket($subscription_credit_consumption_on_socket);
            $subscription_snapshot_record->save();
        } else {
            $subscription_snapshot_record = null;
        }
        return $subscription_snapshot_record;
    }

    public function is_useful_for_calculating_consumption(bool $need_to_perform_queries_to_get_berth_and_previous_snapshots_for_berth, bool $need_to_keep_the_snapshot_if_the_minutes_elapsed_since_last_snapshot_for_the_same_berth_are_greater_than_the_configuration_parameter, bool $need_to_check_if_this_is_the_last_snapshot_for_the_berth, bool $need_to_set_keep_snapshot_flag_and_keep_next_snapshot_flag, string &$reason_to_keep_the_snapshot, Berth $berth = null, SocketSnapshot $previous_snapshot_for_berth = null, SocketSnapshot $second_previous_snapshot_for_berth = null, SocketSnapshot $last_snapshot_received_for_berth = null): bool {
    // questo metodo viene usato per determinare se l'esistenza di $this è necessaria a mantenere l'integrità e la coerenza delle snapshot al fine di calcolare correttamente i consumi, oppure se può essere eliminato/sovrascritto per evitare che la tabella delle snapshot cresca troppo velocemente. Può essere usato all'interno di un ciclo, settando $need_to_perform_queries_to_get_berth_and_previous_snapshots_for_berth a false e passandogli mano a mano l'istanza del posto barca di appartenenza, le snapshot precedenti per lo stesso posto barca + l'ultimo snapshot in ordine cronologico ricevuto per il posto barca (rispettivamente come terzo, quarto, quinto e sesto parametro) oppure può fare le query in autonomia se chiamato con $need_to_perform_queries_to_get_berth_and_previous_snapshots_for_berth === true (in questo caso ovviamente non vanno passate le snapshot precedenti). Se si vuole che verifichi quanti minuti sono passati dall'ultimo snapshot per lo stesso posto barca, e che lo snapshot corrente venga scritto (o non venga cancellato) se i minuti trascorsi sono > del valore massimo settato in config tra uno snapshot e l'altro per lo stesso posto barca (a prescindere dal fatto che sia cambiato qualcosa o meno), va settato a true il parametro $need_to_keep_the_snapshot_if_the_minutes_elapsed_since_last_snapshot_for_the_same_berth_are_greater_than_the_configuration_parameter. Questo metodo viene usato sia in fase di inserimento di un nuovo snapshot, sia a posteriori per eliminare le snapshot non utili al calcolo dei consumi. In questo secondo caso, dovrò tenere lo snapshot corrente se è l'ultimo ricevuto per la sua presa. Posso fare questo check passando il flag $need_to_check_if_this_is_the_last_snapshot_for_the_berth a true. Infine, in fase di ricezione dello snapshot da parte dell'APIv0Controller, bisognerà anche settare il keep_snapshot_flag ed il keep_next_snapshot_flag per il record. In questo caso, il metodo dovrà essere chiamato con il parametro $need_to_set_keep_snapshot_flag_and_keep_next_snapshot_flag a true.
        $is_useful_for_calculating_consumption = false;
        $berth_exists = false;
        if($need_to_perform_queries_to_get_berth_and_previous_snapshots_for_berth) {
            $berth = Berth::find($this->socket_id);
            if(($berth) && ($berth->count() > 0)) {
                $berth_exists = true;
                $last_two_previous_snapshots_for_berth = SocketSnapshot::where('socket_id', $this->socket_id)
                                                                       ->where('timestamp', '<', $this->timestamp)
                                                                       ->orderBy('timestamp', 'desc')
                                                                       ->take(2)
                                                                       ->get();
                if(isset($last_two_previous_snapshots_for_berth[0]) && !is_null($last_two_previous_snapshots_for_berth[0])) {
                    $previous_snapshot_for_berth = $last_two_previous_snapshots_for_berth[0];
                }
                if(isset($last_two_previous_snapshots_for_berth[1]) && !is_null($last_two_previous_snapshots_for_berth[1])) {
                    $second_previous_snapshot_for_berth = $last_two_previous_snapshots_for_berth[1];
                }
                $last_snapshot_received_for_berth = SocketSnapshot::where('socket_id', $this->socket_id)
                                                                  ->orderBy('timestamp', 'desc')
                                                                  ->take(1)
                                                                  ->get();
            } else {
                $berth_exists = false;
            }
        } else {
            if(!is_null($berth) && ($berth->count() > 0)) {
                $berth_exists = true;
            }
        }
        if($berth_exists) {
        // se l'anagrafica del posto barca collegato allo snapshot esiste sul db, è possibile determinarne l'utilità. Procedo:
            if(($need_to_check_if_this_is_the_last_snapshot_for_the_berth) && ($this->get_id() === $last_snapshot_received_for_berth->get_id())) {
            // se devo controllare se $this è l'ultimo snapshot ricevuto per questo posto barca, e se lo è:
                // non posso eliminare eliminare $this
                $is_useful_for_calculating_consumption = true;
                $reason_to_keep_the_snapshot = 'it is the last snapshot received for the socket with address='.$this->socket_id;
            } else if((!$previous_snapshot_for_berth) || ($previous_snapshot_for_berth->count() === 0)) {
            // se non esiste uno snapshot precedente a $this per la stessa presa:
                // non posso eliminare $this:
                $is_useful_for_calculating_consumption = true;
                $reason_to_keep_the_snapshot = 'it is the first snapshot received for the socket with address='.$this->socket_id;
                if($need_to_set_keep_snapshot_flag_and_keep_next_snapshot_flag) {
                // in questo caso, se $need_to_set_keep_snapshot_flag_and_keep_next_snapshot_flag è true, setto a true i valori di keep_snapshot_flag e di keep_next_snapshot_flag per il record corrente e lo salvo:
                    $this->set_keep_snapshot_flag(true);
                    $this->set_keep_next_snapshot_flag(true);
                    $this->save();
                }
            } else if(!$previous_snapshot_for_berth->can_next_snapshot_be_deleted()) {
            // se $previous_snapshot_for_berth esiste ma ha il keep_next_snapshot_flag a true: 
                // non posso eliminare $this:
                $is_useful_for_calculating_consumption = true;
                $reason_to_keep_the_snapshot = 'the previous snapshot for the same socket has keep_next_snapshot_flag = true';
                if($need_to_set_keep_snapshot_flag_and_keep_next_snapshot_flag) {
                // in questo caso, se $need_to_set_keep_snapshot_flag_and_keep_next_snapshot_flag è true, setto a true il valore di keep_snapshot_flag per il record corrente e lo salvo:
                    $this->set_keep_snapshot_flag(true);
                    $this->save();
                }
            } else if((!$second_previous_snapshot_for_berth) || ($second_previous_snapshot_for_berth->count() === 0)) {
            // se non esiste uno snapshot precedente a $previous_snapshot_for_berth per la stessa presa:
                // non posso eliminare $this:
                $is_useful_for_calculating_consumption = true;
                $reason_to_keep_the_snapshot = 'it is the second snapshot received for the socket with address='.$this->socket_id;
            } else if(!$this->can_be_deleted()) {
            // se $this ha il keep_snapshot_flag a 1:
                // non posso eliminarlo:
                $is_useful_for_calculating_consumption = true;
                $reason_to_keep_the_snapshot = 'it has keep_snapshot_flag = true';
            } else if(!$this->has_same_values_except_for_timestamp_as($previous_snapshot_for_berth, $reason_to_keep_the_snapshot)) {
            // se $this e $previous_snapshot_for_berth non hanno gli stessi valori per stato comunicazione, stato on/off, transponder in uso, contatori acqua/energia, credito residuo, consumo di energia istantaneo o epower_id:
                // non posso eliminare eliminare $this:
                $is_useful_for_calculating_consumption = true;
            } else if(!$this->triggers_same_alarms_as($previous_snapshot_for_berth)) {
            // se $this e $previous_snapshot_for_berth non triggerano gli stessi allarmi:
                // non posso eliminare eliminare $this
                $is_useful_for_calculating_consumption = true;
                if(is_null($this->alarms) || (strlen(strval($this->alarms)) === 0)) {
                    $current_snapshots_alarms_string = 'nessuno';
                } else {
                    $current_snapshots_alarms_string = $this->alarms;
                }
                if(is_null($previous_snapshot_for_berth->alarms) || (strlen(strval($previous_snapshot_for_berth->alarms)) === 0)) {
                    $previous_snapshots_alarms_string = 'nessuno';
                } else {
                    $previous_snapshots_alarms_string = $previous_snapshot_for_berth->alarms;
                }
                $reason_to_keep_the_snapshot = 'it triggers different alarms compared to the previous snapshot for the same berth (alarms from the current snapshot: ['.$current_snapshots_alarms_string.'], alarms from the previous snapshot: ['.$previous_snapshots_alarms_string.'])';
            } else {
            	if(boolval($this->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
		        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
					$subscription_for_this_snapshot = $this->subscription;
		        } else {
		        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
		        	$subscription_for_this_snapshot = $this->retrieve_connected_subscription();
		        }
		        if(boolval($second_previous_snapshot_for_berth->connected_subscription_can_be_retrieved_using_subscription_id_field())) {
		        	// dall'introduzione del campo subscription_id sulle tabelle degli snapshot, posso usare la relationship subscription() per recuperare il contratto collegato:
					$subscription_for_second_previous_snapshot_for_berth = $second_previous_snapshot_for_berth->subscription;
		        } else {
		        	// per gli snapshot scritti prima dell'introduzione del campo subscription_id, il contratto collegato va ricercato usando il metodo retrieve_connected_subscription():
		        	$subscription_for_second_previous_snapshot_for_berth = $second_previous_snapshot_for_berth->retrieve_connected_subscription();
		        }
                if(!(is_null($subscription_for_this_snapshot) && (is_null($subscription_for_second_previous_snapshot_for_berth)))) {
                // se i contratti collegati a $this e $second_previous_snapshot_for_berth non sono entrambi nulli (ovvero se almeno uno dei due appartiene ad un contratto):
                    if((is_null($subscription_for_this_snapshot) && !is_null($subscription_for_second_previous_snapshot_for_berth)) || 
                        (!is_null($subscription_for_this_snapshot) && is_null($subscription_for_second_previous_snapshot_for_berth))) {
                    // verifico se è null almeno uno dei due contratti collegati alle snapshot: 
                        // se sì, $current_snapshot va tenuto:
                        $is_useful_for_calculating_consumption = true;
                        $reason_to_keep_the_snapshot = 'it is bound to a subscription, while $second_previous_snapshot_for_berth is not (or vice-versa)';
                    } else if($subscription_for_this_snapshot->get_id() !== $subscription_for_second_previous_snapshot_for_berth->get_id()) {
                    // altrimenti, se entrambi i contratti esistono, verifico se si tratta di contratti diversi (quindi con id diversi):
                        // se sì, $current_snapshot va tenuto:
                        $is_useful_for_calculating_consumption = true;
                        $reason_to_keep_the_snapshot = 'the bound subscription is different than the one bound to $second_previous_snapshot_for_berth';
                    } else if($subscription_for_this_snapshot->subscription_costs_have_changed_between_timestamps($this->get_timestamp(), $previous_snapshot_for_berth->get_timestamp())) {
                    // se la subscription è rimasta la stessa tra i due timestamp, verifico se i suoi costi sono cambiati dal timestamp di $previous_snapshot_for_berth:
                        // se sono cambiati, non posso eliminare eliminare $this
                        $is_useful_for_calculating_consumption = true;
                        $reason_to_keep_the_snapshot = 'the cost of energy and/or water for the bound subscription has changed since the last snapshot for the same berth';
                    } else if($subscription_for_this_snapshot->subscription_alarm_thresholds_have_changed_between_timestamps($this->get_timestamp(), $previous_snapshot_for_berth->get_timestamp())) {
                    // in caso contrario, verifico se le soglie allarmi della subscription sono cambiate dal timestamp di $previous_snapshot_for_berth:
                        // se sono cambiate, non posso eliminare eliminare $this:
                        $is_useful_for_calculating_consumption = true;
                        $reason_to_keep_the_snapshot = 'the alarm thresholds for the bound subscription have changed since the last snapshot for the same berth';
                    }
                }
            }
            if((!$is_useful_for_calculating_consumption) && ($need_to_keep_the_snapshot_if_the_minutes_elapsed_since_last_snapshot_for_the_same_berth_are_greater_than_the_configuration_parameter === true)) {
            // se - arrivato fino a qui - lo snapshot corrente risulta sacrificabile, e se devo controllare quanto tempo è passato dall'ultimo snapshot per la stessa presa:
                // calcolo i minuti trascorsi da $previous_snapshot_for_berth:
                $minutes_elapsed_between_socket_snapshot_record_and_last_socket_snapshot_record = ($this->get_timestamp()->diffInMinutes($previous_snapshot_for_berth->get_timestamp()));
                if($minutes_elapsed_between_socket_snapshot_record_and_last_socket_snapshot_record >= config('marina.interval_between_two_identical_snapshot_for_same_socket_minutes')) {
                // se è passato un numero di minuti maggiore o uguale al valore settato in config come intervallo massimo tra due snapshot identiche per la stessa presa:
                    // lo snapshot va scritto sul db:
                    $is_useful_for_calculating_consumption = true;
                    $reason_to_keep_the_snapshot = 'at least '.config('marina.interval_between_two_identical_snapshot_for_same_socket_minutes').' have passed since the last snapshot for the same berth (this is the max time between two snapshots for the same berth, according to the '.config('app.name').' configuration)';
                }
            }
        } else {
        // se l'anagrafica del posto barca collegato allo snapshot non esiste ancora sul db, non è possibile fare alcuni dei calcoli necessari a determinarne l'utilità, perciò lo snapshot va tenuto a prescindere:
            $is_useful_for_calculating_consumption = true;
            $reason_to_keep_the_snapshot = 'the database doesn\'t contain a record for the connected berth';
        }
        return $is_useful_for_calculating_consumption;
    }

    public static function compose_socket_snapshot_url(int $socket_id,
                                                       int $epoch_timestamp,
                                                       string $link_status,
                                                       string $on_off_status,
                                                       string $transponder_id,
                                                       float $total_power_cnt,
                                                       float $postpaid_power_cnt,
                                                       float $total_water_cnt,
                                                       float $postpaid_water_cnt,
                                                       float $residual_credit,
                                                       int $epower_id,
                                                       float $instant_power_consumption,
                                                       string $alarms = null) {
    // dati i dati dello snapshot (nel formato utilizzato da SmartView per fare la chiamata http), restituisce l'url da usare per la chiamata 
        $socket_snapshot_url = config('app.url');
        $socket_snapshot_url .= '/ws/insert?';
        $socket_snapshot_url .= 'mode=entity_tables&';
        $socket_snapshot_url .= 'table=snapshotpresa&';
        $socket_snapshot_url .= 'timestamp='.$epoch_timestamp.'&';
        $socket_snapshot_url .= 'indLogicoPresa='.$socket_id.'&';
        $socket_snapshot_url .= 'statoComunicazione='.$link_status.'&';
        $socket_snapshot_url .= 'statoOnOffPresa='.$on_off_status.'&';
        $socket_snapshot_url .= 'transponderID='.$transponder_id.'&';
        $socket_snapshot_url .= 'creditoResiduo='.number_format($residual_credit, 2, ",", "").'&';
        $socket_snapshot_url .= 'contEnergia='.number_format($total_power_cnt, 1, ",", "").'&';
        $socket_snapshot_url .= 'contEnergiaAbb='.number_format($postpaid_power_cnt, 1, ",", "").'&';
        $socket_snapshot_url .= 'contAcqua='.number_format($total_water_cnt, 3, ",", "").'&';
        $socket_snapshot_url .= 'contAcquaAbb='.number_format($postpaid_water_cnt, 3, ",", "").'&';
        $socket_snapshot_url .= 'codiceEPower='.$epower_id.'&';
        $socket_snapshot_url .= 'potenza='.$instant_power_consumption;
        if(!is_null($alarms) && (strlen(strval($alarms)) > 0)) {
            $socket_snapshot_url .= 'allarmi='.$alarms.'&';
        }
        return $socket_snapshot_url;
    }

    public function scopeBeforeTimestamp($query, $timestamp, bool $strict_before = false) {
        if($strict_before) {
            $compare_symbol = '<';
        } else {
            $compare_symbol = '<=';
        }
        return $query->where('timestamp', $compare_symbol, $timestamp);
    }

    public function scopeAfterTimestamp($query, $timestamp, bool $strict_after = false) {
        if($strict_after) {
            $compare_symbol = '>';
        } else {
            $compare_symbol = '>=';
        }
        return $query->where('timestamp', $compare_symbol, $timestamp);
    }

    public function scopeOlderFirst($query) {
        return $query->orderBy('timestamp', 'asc');
    }

    public function scopeNewerFirst($query) {
        return $query->orderBy('timestamp', 'desc');
    }
}
