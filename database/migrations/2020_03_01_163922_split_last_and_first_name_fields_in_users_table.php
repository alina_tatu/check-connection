<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SplitLastAndFirstNameFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('name', 'last_name');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->nullable()->after('last_name');
        });

        Schema::table('users', function (Blueprint $table) {
            $results = DB::table('users')->select('id', 'last_name', 'first_name')->get();
            foreach($results as $user_record) {
                $names_array = explode(' ', $user_record->last_name, 2);
                $first_name = $names_array[0];
                $last_name = $names_array[1];
                DB::table('users')
                    ->where('id', $user_record->id)
                    ->update([
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                ]);
            }         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $results = DB::table('users')->select('id', 'last_name', 'first_name')->get();
            foreach($results as $user_record) {
                $full_name = $user_record->last_name.' '.$user_record->first_name;
                DB::table('users')
                    ->where('id', $user_record->id)
                    ->update([
                        'last_name' => $full_name,
                        'first_name' => '',
                ]);
            }         
        });

        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('last_name', 'name');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
        });
    }
}
