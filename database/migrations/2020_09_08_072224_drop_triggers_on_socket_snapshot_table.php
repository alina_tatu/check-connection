<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropTriggersOnSocketSnapshotTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::unprepared('DROP TRIGGER trigger_copy_snapshot_row_on_inserts_on_last_snapshot_table');
        DB::unprepared('DROP TRIGGER trigger_copy_snapshot_row_on_updates_on_last_snapshot_table');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::unprepared('CREATE TRIGGER `trigger_copy_snapshot_row_on_inserts_on_last_snapshot_table` AFTER INSERT ON `socket_snapshots` FOR EACH ROW 
                            BEGIN
                                DELETE FROM socket_last_snapshots
                                WHERE socket_id = new.socket_id;

                                INSERT INTO socket_last_snapshots(id, socket_id, timestamp, link_status, on_off_status, transponder_id, total_power_cnt, total_water_cnt, postpaid_power_cnt, postpaid_water_cnt, residual_credit, alarms, instant_power_consumption, epower_id) VALUES(new.id, new.socket_id, new.timestamp, new.link_status, new.on_off_status, new.transponder_id, new.total_power_cnt, new.total_water_cnt, new.postpaid_power_cnt, new.postpaid_water_cnt, new.residual_credit, new.alarms, new.instant_power_consumption, new.epower_id);
                            END');
        DB::unprepared('CREATE TRIGGER `trigger_copy_snapshot_row_on_updates_on_last_snapshot_table` AFTER UPDATE ON `socket_snapshots` FOR EACH ROW 
                        BEGIN
                            DELETE FROM socket_last_snapshots
                            WHERE socket_id = new.socket_id;

                            INSERT INTO socket_last_snapshots(id, socket_id, timestamp, link_status, on_off_status, transponder_id, total_power_cnt, total_water_cnt, postpaid_power_cnt, postpaid_water_cnt, residual_credit, alarms, instant_power_consumption, epower_id) VALUES(new.id, new.socket_id, new.timestamp, new.link_status, new.on_off_status, new.transponder_id, new.total_power_cnt, new.total_water_cnt, new.postpaid_power_cnt, new.postpaid_water_cnt, new.residual_credit, new.alarms, new.instant_power_consumption, new.epower_id);
                        END');
    }
}
