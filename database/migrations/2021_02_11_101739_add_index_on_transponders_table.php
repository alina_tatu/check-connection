<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexOnTranspondersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        
        Schema::table('transponders', function (Blueprint $table) {
        // TODO: se questa migration va in errore, vedere qui https://stackoverflow.com/questions/56766511/laravel-sqlstatehy000-general-error-1364-field-id-doesnt-have-a-default-v/56766699#56766699
            $table->unique(['code', 'used_for_socket_subscriptions']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('transponders', function (Blueprint $table) {
            $table->dropUnique(['code', 'used_for_socket_subscriptions']);
        });
    }
}
