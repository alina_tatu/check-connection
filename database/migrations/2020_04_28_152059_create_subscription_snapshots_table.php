<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionSnapshotsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('subscription_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id');
            $table->timestamp('timestamp');
            $table->unsignedInteger('socket_id');
            $table->decimal('subscription_total_power_consumption', 14, 1);
            $table->decimal('subscription_total_water_consumption', 16, 3);
            $table->decimal('subscription_power_consumption_on_socket', 14, 1);
            $table->decimal('subscription_water_consumption_on_socket', 16, 3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('subscription_snapshots');
    }
}
