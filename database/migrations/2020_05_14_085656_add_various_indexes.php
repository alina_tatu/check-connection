<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVariousIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('subscription_costs', function (Blueprint $table) {
            $table->index(['subscription_id', 'begin_dt', 'end_dt']);
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->index(['notifiable_id', 'read_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('subscription_costs', function (Blueprint $table) {
            $table->dropIndex(['subscription_id', 'begin_dt', 'end_dt']);
        });
        Schema::table('notifications', function (Blueprint $table) {
            $table->dropIndex(['notifiable_id', 'read_at']);
        });
    }
}
