<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;

class AddLocalSoftwareIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('local_software_id')->after('id')->nullable();
        });
        User::where('id', '<', '100000')                                // fino ad ora, solitamente ho usato id >= 100000 per utenti esistenti solamente su SmartCloud
            ->update(['local_software_id' => DB::raw("`id`")]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('local_software_id');
        });
    }
}
