<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Berth;

class AddPedestalIdFieldToBerthsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('berths', function (Blueprint $table) {
            $table->bigInteger('pedestal_id')->nullable(false)->default(0);
        });
        Schema::table('berths', function (Blueprint $table) {
            $all_berths_in_plant = Berth::all();
            foreach($all_berths_in_plant as $berth) {
                $berth->calculate_and_set_pedestal_id();
                $berth->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('berths', function (Blueprint $table) {
            $table->dropColumn('pedestal_id');
        });
    }
}
