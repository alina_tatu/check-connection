<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyToSubscriptionsTableOnSocketSnapshotsAndSocketLastSnapshotsTables extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->foreign('subscription_id')
                  ->references('id')
                  ->on('subscriptions');
        });

        Schema::table('socket_last_snapshots', function (Blueprint $table) {
            $table->foreign('subscription_id')
                  ->references('id')
                  ->on('subscriptions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
        });

        Schema::table('socket_last_snapshots', function (Blueprint $table) {
            $table->dropForeign(['subscription_id']);
        });
    }
}
