<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TurnSocketLastSnapshotViewIntoATable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('DROP VIEW IF EXISTS socket_last_snapshots');
        Schema::create('socket_last_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('socket_id');
            $table->timestamp('timestamp');
            $table->string('link_status', 35);
            $table->string('on_off_status', 6);
            $table->decimal('total_power_cnt', 14, 1);
            $table->decimal('postpaid_power_cnt', 14, 1);
            $table->decimal('total_water_cnt', 16, 3);
            $table->decimal('postpaid_water_cnt', 16, 3);
            $table->string('transponder_id', 8);
            $table->decimal('residual_credit', 10, 2);
            $table->unsignedInteger('epower_id');
            $table->unsignedDecimal('instant_power_consumption', 8, 3);
            $table->string('alarms', 10)->nullable();
            $table->index('socket_id');
            $table->index('transponder_id');
            $table->index('timestamp');
            $table->index(['timestamp', 'socket_id']);
            $table->index(['timestamp', 'socket_id', 'transponder_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('socket_last_snapshots');
        DB::statement('CREATE VIEW socket_last_snapshots AS
                            SELECT *
                            FROM socket_snapshots
                            WHERE id IN (
                                SELECT MAX(id)
                                FROM socket_snapshots 
                                GROUP BY socket_id
                            )  
                            ORDER BY `socket_snapshots`.`timestamp` DESC
        ');
    }
}
