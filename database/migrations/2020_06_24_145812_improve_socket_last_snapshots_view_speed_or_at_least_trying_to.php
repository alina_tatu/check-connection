<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImproveSocketLastSnapshotsViewSpeedOrAtLeastTryingTo extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('DROP VIEW IF EXISTS socket_last_snapshots');
        DB::statement('CREATE VIEW socket_last_snapshots AS 
                            SELECT ss1.* 
                            FROM socket_snapshots ss1
                            WHERE ss1.id IN (SELECT MAX(ss2.id) AS id
                            FROM socket_snapshots ss2
                            GROUP BY ss2.socket_id)');
        // TODO: questa query non funziona se uso artisan migrate, non so per quale motivo. Però se la lancio direttamente sul db, la view viene creata.
        // TODO: non sono convinto che questa migration migliori effettivamente le cose
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement('DROP VIEW IF EXISTS socket_last_snapshots');
        DB::statement('CREATE VIEW socket_last_snapshots AS
                            SELECT ss1.* 
                            FROM socket_snapshots ss1
                            WHERE id = (SELECT ss2.id
                            FROM socket_snapshots ss2
                            WHERE ss2.socket_id = ss1.socket_id
                            ORDER BY ss2.id DESC
                            LIMIT 1)');
    }
}
