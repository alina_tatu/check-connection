<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKeepSnapshotFlagsToSocketSnapshotTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->boolean('keep_snapshot_flag')->default(false)->after('alarms');
        });
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->boolean('keep_next_snapshot_flag')->default(false)->after('keep_snapshot_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->dropColumn('keep_snapshot_flag');
        });
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->dropColumn('keep_next_snapshot_flag');
        });
    }
}