<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use App\User;

class AddTokenForActionsOnSocketsOnUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->string('token_for_actions_on_sockets', '100')
                  ->nullable(true)
                  ->after('tresholds_in_ampere');
        });
        $existing_users = User::all();
        foreach($existing_users as $user) {
            $token = User::generate_token_for_actions_on_sockets();
            $user->set_token_for_actions_on_sockets($token);
            $user->save();
        }
        Schema::table('users', function (Blueprint $table) {
            $table->string('token_for_actions_on_sockets', '100')
                  ->nullable(false)
                  ->unique()
                  ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('token_for_actions_on_sockets');
        });
    }
}
