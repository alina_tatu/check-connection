<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateSubscriptionConsumptionPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('subscription_consumption_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id')->nullable(false);
            $table->decimal('paid_power_amount_kwh', 14, 1);
            $table->decimal('paid_power_amount_currency', 10, 2);
            $table->decimal('paid_water_amount_cm', 16, 3);
            $table->decimal('paid_water_amount_currency', 10, 2);
            $table->bigInteger('operator_id')->nullable(false);
            $table->dateTimeTz('created_at', 0)->useCurrent()->nullable(false);     // TODO: useCurrent non va perchè non setta correttamente il formato. Come fare? O forse è il formato definito nel model che non combacia?
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('subscription_consumption_payments');
    }
}
