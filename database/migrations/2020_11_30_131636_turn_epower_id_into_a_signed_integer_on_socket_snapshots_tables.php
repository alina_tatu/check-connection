<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TurnEpowerIdIntoASignedIntegerOnSocketSnapshotsTables extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->integer('epower_id')->change();
        });
        Schema::table('socket_last_snapshots', function (Blueprint $table) {
            $table->integer('epower_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->unsignedInteger('epower_id');
        });
        Schema::table('socket_last_snapshots', function (Blueprint $table) {
            $table->unsignedInteger('epower_id');
        });
    }
}
