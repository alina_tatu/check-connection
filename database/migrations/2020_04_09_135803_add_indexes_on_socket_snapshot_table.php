<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesOnSocketSnapshotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->index('socket_id');
            $table->index('transponder_id');
            $table->index('timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->dropIndex('socket_id');
            $table->dropIndex('transponder_id');
            $table->dropIndex('timestamp');
        });
    }
}
