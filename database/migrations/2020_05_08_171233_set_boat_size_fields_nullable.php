<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetBoatSizeFieldsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boats', function (Blueprint $table) {
            $table->unsignedDecimal('length')->nullable()->change();
            $table->unsignedDecimal('width')->nullable()->change();
            $table->unsignedDecimal('depth')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boats', function (Blueprint $table) {
            $table->unsignedDecimal('length')->nullable(false)->change();
            $table->unsignedDecimal('width')->nullable(false)->change();
            $table->unsignedDecimal('depth')->nullable(false)->change();
        });
    }
}
