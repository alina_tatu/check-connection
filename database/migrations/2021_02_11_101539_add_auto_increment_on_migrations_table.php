<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddAutoIncrementOnMigrationsTable extends Migration {
// TODO: forse queesta migration non mi serve. L'avevo aggiunta perchè improvvisamente quando lanciavo una migration ottenevo un errore che diceva che il campo di della tabella migrations 

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        /*
        DB::statement('ALTER TABLE `migrations` ADD PRIMARY KEY (`id`)');
        DB::statement('ALTER TABLE `migrations` MODIFY `id` INTEGER NOT NULL AUTO_INCREMENT');
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        /*
        DB::statement('ALTER TABLE `migrations` MODIFY `id` INTEGER NOT NULL');
        DB::statement('ALTER TABLE `migrations` DROP PRIMARY KEY');
        */
    }
}
