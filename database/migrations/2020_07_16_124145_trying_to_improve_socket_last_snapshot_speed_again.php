<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TryingToImproveSocketLastSnapshotSpeedAgain extends Migration {
    public function up() {
        DB::statement('DROP VIEW IF EXISTS socket_last_snapshots');
        DB::statement('CREATE VIEW socket_last_snapshots AS
                            SELECT *
                            FROM socket_snapshots
                            WHERE id IN (
                                SELECT MAX(id)
                                FROM socket_snapshots 
                                GROUP BY socket_id
                            )  
                            ORDER BY `socket_snapshots`.`timestamp` DESC
        ');
    }

    public function down() {
        DB::statement('DROP VIEW IF EXISTS socket_last_snapshots');
        DB::statement('CREATE VIEW socket_last_snapshots AS
                            SELECT ss1.* 
                            FROM socket_snapshots ss1
                            WHERE id = (SELECT ss2.id
                            FROM socket_snapshots ss2
                            WHERE ss2.socket_id = ss1.socket_id
                            ORDER BY ss2.id DESC
                            LIMIT 1)
        ');
    }
}
