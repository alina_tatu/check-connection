<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexOnSubscriptionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->index(['transponder_id', 'begin_dt', 'end_dt']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropIndex(['transponder_id', 'begin_dt', 'end_dt']);
        });
    }
}
