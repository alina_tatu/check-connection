<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLostMoneyForResettedSocketsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('lost_money_for_resetted_sockets', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('action_id')->nullable(false);
            $table->bigInteger('socket_snapshot_id')->nullable(false);
            $table->decimal('money_on_the_socket_when_resetted', 10, 2);            // TODO: questa è una info ridondante? è meglio toglierla e prendere il credito direttamente dal socket_snapshot collegato?

            // TODO: quando chiamo un'azione, ricordare che devo settare a 1 il keep_snapshot_flag dello snapshot

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('lost_money_for_resetted_sockets');
    }
}
