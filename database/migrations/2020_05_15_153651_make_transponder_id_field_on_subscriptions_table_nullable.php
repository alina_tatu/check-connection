<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeTransponderIdFieldOnSubscriptionsTableNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('transponder_id', 8)->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('transponder_id', 8)->nullable(false)->change();
        });
    }
}
