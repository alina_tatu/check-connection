<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;

class UpdateDateformatInAllTables extends Migration {
    public function up() {
        $results = DB::table('actions')->get()->toArray();
        Schema::dropIfExists('actions');
        foreach($results as $result) {
            $result->request_timestamp = Carbon::parse($result->request_timestamp, config('app.timezone'))->format('Y-m-d H:i:s');
            if(!is_null($result->completion_timestamp)) {
                $result->completion_timestamp = Carbon::parse($result->completion_timestamp, config('app.timezone'))->format('Y-m-d H:i:s');
            }
        }
        Schema::create('actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 25);
            $table->string('object_type', 25);
            $table->bigInteger('object_id');
            $table->timestamp('request_timestamp')->useCurrent();
            $table->bigInteger('requesting_user_id');
            $table->timestamp('completion_timestamp')->nullable();
            $table->string('completing_terminal_name')->nullable();
        });
        foreach($results as$row) {
            DB::table('actions')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('notifications')->get()->toArray();
        Schema::dropIfExists('notifications');
        foreach($results as $result) {
            $result->created_at = Carbon::parse($result->created_at, config('app.timezone'))->format('Y-m-d H:i:s');
            $result->updated_at = Carbon::parse($result->updated_at, config('app.timezone'))->format('Y-m-d H:i:s');
            if(!is_null($result->read_at)) {
                $result->read_at = Carbon::parse($result->read_at, config('app.timezone'))->format('Y-m-d H:i:s');
            }
        }
        Schema::create('notifications', function (Blueprint $table) {
            Schema::dropIfExists('notifications');
            $table->uuid('id')->primary();
            $table->string('type');
            $table->morphs('notifiable');
            $table->text('data');
            $table->timestamp('read_at')->nullable();
            $table->timestamps();
            $table->index(['notifiable_id', 'read_at']);
        });
        foreach($results as$row) {
            DB::table('notifications')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('socket_snapshots')->get()->toArray();
        Schema::dropIfExists('socket_snapshots');
        foreach($results as $result) {
            $result->timestamp = Carbon::parse($result->timestamp, config('app.timezone'))->format('Y-m-d H:i:s');
        }
        Schema::create('socket_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('socket_id');
            $table->timestamp('timestamp');
            $table->string('link_status', 35);
            $table->string('on_off_status', 6);
            $table->decimal('total_power_cnt', 14, 1);
            $table->decimal('postpaid_power_cnt', 14, 1);
            $table->decimal('total_water_cnt', 16, 3);
            $table->decimal('postpaid_water_cnt', 16, 3);
            $table->string('transponder_id', 8);
            $table->decimal('residual_credit', 10, 2);
            $table->unsignedInteger('epower_id');
            $table->unsignedDecimal('instant_power_consumption', 8, 3);
            $table->string('alarms', 10)->nullable();
            $table->boolean('keep_snapshot_flag')->default(false);
            $table->boolean('keep_next_snapshot_flag')->default(false);
            $table->index('socket_id');
            $table->index('transponder_id');
            $table->index('timestamp');
            $table->index(['timestamp', 'socket_id']);
            $table->index(['timestamp', 'socket_id', 'transponder_id']);
        });
        foreach($results as$row) {
            DB::table('socket_snapshots')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('subscription_alarm_thresholds')->get()->toArray();
        Schema::dropIfExists('subscription_alarm_thresholds');
        foreach($results as $result) {
            $result->begin_dt = Carbon::parse($result->begin_dt, config('app.timezone'))->format('Y-m-d H:i:s');
            if(!is_null($result->end_dt)) {
                $result->end_dt = Carbon::parse($result->end_dt, config('app.timezone'))->format('Y-m-d H:i:s');
            }            
        }
        Schema::create('subscription_alarm_thresholds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id');
            $table->unsignedDecimal('min_power_threshold_kwh', 9, 3);
            $table->unsignedDecimal('max_power_threshold_kwh', 9, 3);
            $table->unsignedSmallInteger('max_water_tap_time_threshold_minutes');
            $table->decimal('min_credit_threshold', 10, 2);
            $table->timestamp('begin_dt');
            $table->timestamp('end_dt')->nullable();
            $table->index('subscription_id');
            $table->index('begin_dt');
            $table->index('end_dt');
            $table->index(['subscription_id', 'begin_dt', 'end_dt'],  'subscription_id_begin_dt_end_dt_index');
        });
        foreach($results as$row) {
            DB::table('subscription_alarm_thresholds')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('subscription_consumption_payments')->get()->toArray();
        Schema::dropIfExists('subscription_consumption_payments');
        foreach($results as $result) {
            $result->created_at = Carbon::parse($result->begin_dt, config('app.timezone'))->format('Y-m-d H:i:s');
            if(!is_null($result->deleted_at)) {
                $result->deleted_at = Carbon::parse($result->end_dt, config('app.timezone'))->format('Y-m-d H:i:s');
            }
        }
        Schema::create('subscription_consumption_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id')->nullable(false);
            $table->decimal('paid_power_amount_kwh', 14, 1);
            $table->decimal('paid_power_amount_currency', 10, 2);
            $table->decimal('paid_water_amount_cm', 16, 3);
            $table->decimal('paid_water_amount_currency', 10, 2);
            $table->bigInteger('operator_id')->nullable(false);
            $table->dateTimeTz('created_at', 0)->useCurrent()->nullable(false);     // TODO: useCurrent non va perchè non setta correttamente il formato. Come fare? O forse è il formato definito nel model che non combacia?
            $table->softDeletes();
        });
        foreach($results as$row) {
            DB::table('subscription_consumption_payments')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('subscription_costs')->get()->toArray();
        Schema::dropIfExists('subscription_costs');
        foreach($results as $result) {
            $result->begin_dt = Carbon::parse($result->begin_dt, config('app.timezone'))->format('Y-m-d H:i:s');
            if(!is_null($result->end_dt)) {
                $result->end_dt = Carbon::parse($result->end_dt, config('app.timezone'))->format('Y-m-d H:i:s');
            }
        }
        Schema::create('subscription_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id');
            $table->decimal('power_cost_per_kwh', 6, 2);
            $table->decimal('water_cost_per_m3', 6, 2);
            $table->timestamp('begin_dt');
            $table->timestamp('end_dt')->nullable();
            $table->index('subscription_id');
            $table->index('begin_dt');
            $table->index('end_dt');
            $table->index(['subscription_id', 'begin_dt', 'end_dt']);
        });
        foreach($results as$row) {
            DB::table('subscription_costs')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('subscription_snapshots')->get()->toArray();
        Schema::dropIfExists('subscription_snapshots');
        foreach($results as $result) {
            $result->timestamp = Carbon::parse($result->timestamp, config('app.timezone'))->format('Y-m-d H:i:s');
        }
        Schema::create('subscription_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id');
            $table->timestamp('timestamp');
            $table->unsignedInteger('socket_id');
            $table->decimal('subscription_total_power_consumption', 14, 1);
            $table->decimal('subscription_total_water_consumption', 16, 3);
            $table->decimal('subscription_total_credit_consumption', 10, 2)->nullable();
            $table->decimal('subscription_power_consumption_on_socket', 14, 1);
            $table->decimal('subscription_water_consumption_on_socket', 16, 3);
            $table->decimal('subscription_credit_consumption_on_socket', 10, 2)->nullable();
        });       
        foreach($results as$row) {
            DB::table('subscription_snapshots')->insert(json_decode(json_encode($row), true));
        }
        
        $results = DB::table('subscriptions')->get()->toArray();
        Schema::dropIfExists('subscriptions');
        foreach($results as $result) {
            $result->begin_dt = Carbon::parse($result->begin_dt, config('app.timezone'))->format('Y-m-d H:i:s');
            if(!is_null($result->end_dt)) {
                $result->end_dt = Carbon::parse($result->end_dt, config('app.timezone'))->format('Y-m-d H:i:s');
            }
        }
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('transponder_id', 8)->nullable();
            $table->bigInteger('socket_id')->nullable();
            $table->timestamp('begin_dt');
            $table->timestamp('end_dt')->nullable();
            $table->text('notes')->nullable();
        });
        foreach($results as$row) {
            DB::table('subscriptions')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('users')->get()->toArray();
        Schema::dropIfExists('users');
        foreach($results as $result) {
            if(!is_null($result->email_verified_at)) {
                $result->email_verified_at = Carbon::parse($result->email_verified_at, config('app.timezone'))->format('Y-m-d H:i:s');
            }            
            $result->created_at = Carbon::parse($result->created_at, config('app.timezone'))->format('Y-m-d H:i:s');
            $result->updated_at = Carbon::parse($result->updated_at, config('app.timezone'))->format('Y-m-d H:i:s');
        }
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('last_name');
            $table->string('first_name')->nullable();
            $table->string('username')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('phone')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->text('notes')->nullable();
            $table->string('language', 3)->nullable();
            $table->boolean('tresholds_in_ampere')->default(0);
            $table->tinyInteger('access_level')
                  ->default(0);
            $table->boolean('is_disabled')
                  ->nullable(false)
                  ->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
        foreach($results as$row) {
            DB::table('users')->insert(json_decode(json_encode($row), true));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $results = DB::table('actions')->get()->toArray();
        Schema::dropIfExists('actions');
        foreach($results as $result) {
            $result->request_timestamp = Carbon::parse($result->request_timestamp, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            if(!is_null($result->completion_timestamp)) {
                $result->completion_timestamp = Carbon::parse($result->completion_timestamp, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            }
        }
        Schema::create('actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 25);
            $table->string('object_type', 25);
            $table->bigInteger('object_id');
            $table->timestamp('request_timestamp')->useCurrent();
            $table->bigInteger('requesting_user_id');
            $table->timestamp('completion_timestamp')->nullable();
            $table->string('completing_terminal_name')->nullable();
        });
        foreach($results as$row) {
            DB::table('actions')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('notifications')->get()->toArray();
        Schema::dropIfExists('notifications');
        foreach($results as $result) {
            $result->created_at = Carbon::parse($result->created_at, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            $result->updated_at = Carbon::parse($result->updated_at, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            if(!is_null($result->read_at)) {
                $result->read_at = Carbon::parse($result->read_at, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            }
        }
        Schema::create('notifications', function (Blueprint $table) {
            Schema::dropIfExists('notifications');
            $table->uuid('id')->primary();
            $table->string('type');
            $table->morphs('notifiable');
            $table->text('data');
            $table->timestamp('read_at')->nullable();
            $table->timestamps();
            $table->index(['notifiable_id', 'read_at']);
        });
        foreach($results as$row) {
            DB::table('notifications')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('socket_snapshots')->get()->toArray();
        Schema::dropIfExists('socket_snapshots');
        foreach($results as $result) {
            $result->timestamp = Carbon::parse($result->timestamp, config('app.timezone'))->format('Y-m-d\TH:i:sO');
        }
        Schema::create('socket_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('socket_id');
            $table->timestamp('timestamp');
            $table->string('link_status', 35);
            $table->string('on_off_status', 6);
            $table->decimal('total_power_cnt', 14, 1);
            $table->decimal('postpaid_power_cnt', 14, 1);
            $table->decimal('total_water_cnt', 16, 3);
            $table->decimal('postpaid_water_cnt', 16, 3);
            $table->string('transponder_id', 8);
            $table->decimal('residual_credit', 10, 2);
            $table->unsignedInteger('epower_id');
            $table->unsignedDecimal('instant_power_consumption', 8, 3);
            $table->string('alarms', 10)->nullable();
            $table->boolean('keep_snapshot_flag')->default(false);
            $table->boolean('keep_next_snapshot_flag')->default(false);
            $table->index('socket_id');
            $table->index('transponder_id');
            $table->index('timestamp');
            $table->index(['timestamp', 'socket_id']);
            $table->index(['timestamp', 'socket_id', 'transponder_id']);
        });
        foreach($results as$row) {
            DB::table('socket_snapshots')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('subscription_alarm_thresholds')->get()->toArray();
        Schema::dropIfExists('subscription_alarm_thresholds');
        foreach($results as $result) {
            $result->begin_dt = Carbon::parse($result->begin_dt, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            if(!is_null($result->end_dt)) {
                $result->end_dt = Carbon::parse($result->end_dt, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            }            
        }
        Schema::create('subscription_alarm_thresholds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id');
            $table->unsignedDecimal('min_power_threshold_kwh', 9, 3);
            $table->unsignedDecimal('max_power_threshold_kwh', 9, 3);
            $table->unsignedSmallInteger('max_water_tap_time_threshold_minutes');
            $table->decimal('min_credit_threshold', 10, 2);
            $table->timestamp('begin_dt');
            $table->timestamp('end_dt')->nullable();
            $table->index('subscription_id');
            $table->index('begin_dt');
            $table->index('end_dt');
            $table->index(['subscription_id', 'begin_dt', 'end_dt'], 'subscription_id_begin_dt_end_dt_index');
        });
        foreach($results as$row) {
            DB::table('subscription_alarm_thresholds')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('subscription_consumption_payments')->get()->toArray();
        Schema::dropIfExists('subscription_consumption_payments');
        foreach($results as $result) {
            $result->created_at = Carbon::parse($result->begin_dt, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            if(!is_null($result->deleted_at)) {
                $result->deleted_at = Carbon::parse($result->end_dt, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            }
        }
        Schema::create('subscription_consumption_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id')->nullable(false);
            $table->decimal('paid_power_amount_kwh', 14, 1);
            $table->decimal('paid_power_amount_currency', 10, 2);
            $table->decimal('paid_water_amount_cm', 16, 3);
            $table->decimal('paid_water_amount_currency', 10, 2);
            $table->bigInteger('operator_id')->nullable(false);
            $table->dateTimeTz('created_at', 0)->useCurrent()->nullable(false);     // TODO: useCurrent non va perchè non setta correttamente il formato. Come fare? O forse è il formato definito nel model che non combacia?
            $table->softDeletes();
        });
        foreach($results as$row) {
            DB::table('subscription_consumption_payments')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('subscription_costs')->get()->toArray();
        Schema::dropIfExists('subscription_costs');
        foreach($results as $result) {
            $result->begin_dt = Carbon::parse($result->begin_dt, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            if(!is_null($result->end_dt)) {
                $result->end_dt = Carbon::parse($result->end_dt, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            }
        }
        Schema::create('subscription_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id');
            $table->decimal('power_cost_per_kwh', 6, 2);
            $table->decimal('water_cost_per_m3', 6, 2);
            $table->timestamp('begin_dt');
            $table->timestamp('end_dt')->nullable();
            $table->index('subscription_id');
            $table->index('begin_dt');
            $table->index('end_dt');
            $table->index(['subscription_id', 'begin_dt', 'end_dt']);
        });
        foreach($results as$row) {
            DB::table('subscription_costs')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('subscription_snapshots')->get()->toArray();
        Schema::dropIfExists('subscription_snapshots');
        foreach($results as $result) {
            $result->timestamp = Carbon::parse($result->timestamp, config('app.timezone'))->format('Y-m-d\TH:i:sO');
        }
        Schema::create('subscription_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id');
            $table->timestamp('timestamp');
            $table->unsignedInteger('socket_id');
            $table->decimal('subscription_total_power_consumption', 14, 1);
            $table->decimal('subscription_total_water_consumption', 16, 3);
            $table->decimal('subscription_total_credit_consumption', 10, 2)->nullable();
            $table->decimal('subscription_power_consumption_on_socket', 14, 1);
            $table->decimal('subscription_water_consumption_on_socket', 16, 3);
            $table->decimal('subscription_credit_consumption_on_socket', 10, 2)->nullable();
        });       
        foreach($results as$row) {
            DB::table('subscription_snapshots')->insert(json_decode(json_encode($row), true));
        }
        
        $results = DB::table('subscriptions')->get()->toArray();
        Schema::dropIfExists('subscriptions');
        foreach($results as $result) {
            $result->begin_dt = Carbon::parse($result->begin_dt, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            if(!is_null($result->end_dt)) {
                $result->end_dt = Carbon::parse($result->end_dt, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            }
        }
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('transponder_id', 8)->nullable();
            $table->bigInteger('socket_id')->nullable();
            $table->timestamp('begin_dt');
            $table->timestamp('end_dt')->nullable();
            $table->text('notes')->nullable();
        });
        foreach($results as$row) {
            DB::table('subscriptions')->insert(json_decode(json_encode($row), true));
        }

        $results = DB::table('users')->get()->toArray();
        Schema::dropIfExists('users');
        foreach($results as $result) {
            if(!is_null($result->email_verified_at)) {
                $result->email_verified_at = Carbon::parse($result->email_verified_at, config('app.timezone'))->format('Y-m-d H:i:s');
            }            
            $result->created_at = Carbon::parse($result->created_at, config('app.timezone'))->format('Y-m-d\TH:i:sO');
            $result->updated_at = Carbon::parse($result->updated_at, config('app.timezone'))->format('Y-m-d\TH:i:sO');
        }
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('last_name');
            $table->string('first_name')->nullable();
            $table->string('username')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('phone')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->text('notes')->nullable();
            $table->string('language', 3)->nullable();
            $table->boolean('tresholds_in_ampere')->default(0);
            $table->tinyInteger('access_level')
                  ->default(0);
            $table->boolean('is_disabled')
                  ->nullable(false)
                  ->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
        foreach($results as$row) {
            DB::table('users')->insert(json_decode(json_encode($row), true));
        }
    }
}