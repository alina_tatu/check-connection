<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPasswordChangeFieldsOnUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('needs_password_change')
                  ->after('updated_at')
                  ->nullable(false)
                  ->default(false);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('password_changed_at')
                  ->after('needs_password_change')
                  ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('needs_password_change');
            $table->dropColumn('password_changed_at');
        });
    }
}
