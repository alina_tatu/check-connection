<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeInstantPowerConsumptionFieldToDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->unsignedDecimal('instant_power_consumption', 8, 3)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->unsignedInteger('instant_power_consumption')->change();
        });
    }
}
