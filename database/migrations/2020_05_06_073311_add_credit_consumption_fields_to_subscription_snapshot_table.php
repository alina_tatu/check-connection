<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreditConsumptionFieldsToSubscriptionSnapshotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('subscription_snapshots', function (Blueprint $table) {
            $table->decimal('subscription_total_credit_consumption', 10, 2)->nullable();
            $table->decimal('subscription_credit_consumption_on_socket', 10, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('subscription_snapshots', function (Blueprint $table) {
            $table->dropColumn('subscription_total_credit_consumption');
        });
        Schema::table('subscription_snapshots', function (Blueprint $table) {
            $table->dropColumn('subscription_credit_consumption_on_socket');
        });
    }
}
