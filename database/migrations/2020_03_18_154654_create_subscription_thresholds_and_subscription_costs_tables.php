<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionThresholdsAndSubscriptionCostsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_alarm_thresholds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id');
            $table->unsignedDecimal('min_power_threshold_kwh', 9, 3);
            $table->unsignedDecimal('max_power_threshold_kwh', 9, 3);
            $table->unsignedSmallInteger('max_water_tap_time_threshold_minutes');
            $table->decimal('min_credit_threshold', 10, 2);
            $table->timestamp('begin_dt');
            $table->timestamp('end_dt')->nullable();
        });

        Schema::create('subscription_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscription_id');
            $table->decimal('power_cost_per_kwh', 6, 2);
            $table->decimal('water_cost_per_m3', 6, 2);
            $table->timestamp('begin_dt');
            $table->timestamp('end_dt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_alarm_thresholds');
        Schema::dropIfExists('subscription_costs');
    }
}
