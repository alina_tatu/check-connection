<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeSocketsnapshotTableTimestampIndexToUnique extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->dropIndex('socket_snapshots_timestamp_index');
            $table->index(['timestamp', 'socket_id']);
            $table->index(['timestamp', 'socket_id', 'transponder_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->dropIndex('socket_snapshots_timestamp_socket_id_index');
            $table->dropIndex('socket_snapshots_timestamp_socket_id_transponder_id_index');
            $table->index('timestamp');
        });
    }
}
