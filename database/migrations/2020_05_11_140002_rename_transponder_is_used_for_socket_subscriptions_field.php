<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTransponderIsUsedForSocketSubscriptionsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('transponders', function (Blueprint $table) {
            $table->renameColumn('is_used_for_socket_subscriptions', 'used_for_socket_subscriptions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('transponders', function (Blueprint $table) {
            $table->renameColumn('used_for_socket_subscriptions', 'is_used_for_socket_subscriptions');
        });
    }
}
