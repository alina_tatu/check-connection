<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocketSnapshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socket_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('socket_id');
            $table->timestamp('timestamp');
            $table->string('link_status', 35);
            $table->string('on_off_status', 6);
            $table->decimal('total_power_cnt', 14, 1);
            $table->decimal('postpaid_power_cnt', 14, 1);
            $table->decimal('total_water_cnt', 16, 3);
            $table->decimal('postpaid_water_cnt', 16, 3);
            $table->string('transponder_id', 8);
            $table->decimal('residual_credit', 10, 2);
            $table->unsignedInteger('epower_id');
            $table->unsignedInteger('instant_power_consumption');
            $table->string('alarms', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socket_snapshots');
    }
}
