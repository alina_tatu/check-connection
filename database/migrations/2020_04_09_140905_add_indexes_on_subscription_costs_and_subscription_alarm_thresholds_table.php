<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesOnSubscriptionCostsAndSubscriptionAlarmThresholdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscription_costs', function (Blueprint $table) {
            $table->index('subscription_id');    
            $table->index('begin_dt');
            $table->index('end_dt');
        });
        Schema::table('subscription_alarm_thresholds', function (Blueprint $table) {
            $table->index('subscription_id');    
            $table->index('begin_dt');
            $table->index('end_dt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_costs', function (Blueprint $table) {
            $table->dropIndex('subscription_id');    
            $table->dropIndex('begin_dt');
            $table->dropIndex('end_dt');
        });
        Schema::table('subscription_alarm_thresholds', function (Blueprint $table) {
            $table->dropIndex('subscription_id');    
            $table->dropIndex('begin_dt');
            $table->dropIndex('end_dt');
        });
    }
}
