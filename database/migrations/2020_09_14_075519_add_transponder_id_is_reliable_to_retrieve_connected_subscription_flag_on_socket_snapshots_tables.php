<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransponderIdIsReliableToRetrieveConnectedSubscriptionFlagOnSocketSnapshotsTables extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->boolean('transponder_id_field_is_reliable')
                  ->after('transponder_id')
                  ->nullable(false)
                  ->default(true);
        });
        Schema::table('socket_last_snapshots', function (Blueprint $table) {
            $table->boolean('transponder_id_field_is_reliable')
                  ->after('transponder_id')
                  ->nullable(false)
                  ->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->dropColumn('transponder_id_field_is_reliable');
        });
        Schema::table('socket_last_snapshots', function (Blueprint $table) {
            $table->dropColumn('transponder_id_field_is_reliable');
        });
    }
}
