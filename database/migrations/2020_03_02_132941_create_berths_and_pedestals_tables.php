<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBerthsAndPedestalsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berths', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pontoon_name', 50);
            $table->string('berth_name', 50);
            $table->string('description', 150)->nullable();
            $table->string('socket_type', 30)->default('other');
        });

        Schema::create('pedestals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50);
            $table->unsignedMediumInteger('x_coord')->default(0);
            $table->unsignedMediumInteger('y_coord')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berths');
        Schema::dropIfExists('pedestals');
    }
}
