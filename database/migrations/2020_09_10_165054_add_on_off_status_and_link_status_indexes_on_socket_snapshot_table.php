<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOnOffStatusAndLinkStatusIndexesOnSocketSnapshotTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->index('on_off_status');
            $table->index('link_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->dropIndex('on_off_status');
            $table->dropIndex('link_status');
        });
    }
}
