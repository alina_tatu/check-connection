<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\SocketLastSnapshot;

class AddSubscriptionIdFieldsOnSocketSnapshotsAndSocketLastSnapshotsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        /* 
           in un mondo ideale, nel metodo up() dovrei recuperare tutte le socket_snapshots e le socket_last_snapshots già esistenti sul db, 
           recuperarne il contratto collegato con il metodo retrieve_connected_subscription() e settare il campo subscription_id al valore 
           dell'id del contratto trovato, ma gli impianti in funzione hanno quasi 2 milioni di record di socket_snapshots, quindi la cosa non 
           è fattibile (la migration dovrebbe durare girare per svariate ore (giorni?) ed andrebbe in timeout). Di conseguenza introduco un 
           campo connected_subscription_can_be_retrieved_using_subscription_id_field: se è true (1), significa che lo snapshot è stato scritto
           dopo l'introduzione del campo subscription_id, e quindi è possibile usare la relationship subscription(). Altrimenti, il contratto
           collegato va recuperato usando il metodo retrieve_connected_suscription(). 
           Dato che retrieve_connected_suscription() non è una relationship, non può essere serializzata nel json. Di conseguenza, la view
           plant_status in Vue non potrà mostrare correttamente gli utilizzatori delle prese fino a quando tutte i record nella tabella 
           socket_last_snapshots senza subscription_id non saranno stati rimpiazzati da nuovi record con il subscription_id settato.
        */
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->unsignedBigInteger('subscription_id')
                  ->after('transponder_id_field_is_reliable')
                  ->nullable(true)
                  ->default(null);
            $table->boolean('can_retrieve_subscription_via_subscription_id')
                  ->after('subscription_id')
                  ->nullable(false)
                  ->default(false);
        });

        Schema::table('socket_last_snapshots', function (Blueprint $table) {
            $table->unsignedBigInteger('subscription_id')
                  ->after('transponder_id_field_is_reliable')
                  ->nullable(true)
                  ->default(null);
            $table->boolean('can_retrieve_subscription_via_subscription_id')
                  ->after('subscription_id')
                  ->nullable(false)
                  ->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('socket_snapshots', function (Blueprint $table) {
            $table->dropColumn('subscription_id');
            $table->dropColumn('can_retrieve_subscription_via_subscription_id');
        });
        Schema::table('socket_last_snapshots', function (Blueprint $table) {
            $table->dropColumn('subscription_id');
            $table->dropColumn('can_retrieve_subscription_via_subscription_id');
        });
    }
}
