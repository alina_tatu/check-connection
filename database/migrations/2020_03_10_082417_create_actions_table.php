<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 25);
            $table->string('object_type', 25);
            // per il momento non metto l'object_name, non dovrebbe servirmi dato che posso risalire al nome dell'oggetto con l'object_id
            $table->bigInteger('object_id');
            $table->timestamp('request_timestamp')->useCurrent();
            $table->bigInteger('requesting_user_id');
            $table->timestamp('completion_timestamp')->nullable();
            $table->string('completing_terminal_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
