<?php

use App\SocketLastSnapshot;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSoftDeletedRecordsFromSocketLastSnapshotsTable extends Migration {
// ho troppi record in socket_last_snapshots, con questa li elimino mentre correggo il metodo SocketSnapshot::create_instance_for_socket_last_snapshot_table_and_delete_old_socket_last_snapshots_for_same_sockets() affinchè faccia il forceDelete() del record precedente ad ogni nuovo insert

// TODO: lanciare la migration a webapp offline (= dopo un php artisan down) per evitare che continuino ad arrivare record (e quindi che altre snapshot vengano softdeletate nel frattempo). Se la migration crasha: provare a rollbackare fino a 2021_01_18_104406_add_soft_deletes_to_socket_last_snapshots_table.php compresa, poi rilanciare le migrations
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        SocketLastSnapshot::whereNotNull('deleted_at')->forceDelete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

    }
}
