function show_no_entries_message_if_there_are_no_entries_or_no_entry_matches_the_filters() {
// mostra/nasconde il messaggio di no entries a seconda che ci siano o meno utenti visualizzati (potrebbero non esserci perchè nessun utente corrisponde ai filtri settati)
    var no_entries_message_div = document.getElementById("no_entries_message");
    var shown_user_rows = document.querySelectorAll('.user_row:not(.d-none)');
    if(shown_user_rows.length > 0) {
        if(!(no_entries_message_div.classList.contains("d-none"))) {
            no_entries_message_div.classList.add("d-none");
        }
    } else {
        if(no_entries_message_div.classList.contains("d-none")) {
            no_entries_message_div.classList.remove("d-none");
        }
    }
}

function apply_fulltext_filter() {
    var user_rows = document.getElementsByClassName("user_row");
    var filter_from_input = $("#fulltext_filter_input").val().toUpperCase();
    var current_user_last_name, current_user_first_name, current_user_email_address, current_user_username;
    for(i = 0; i < user_rows.length; i++) {
        current_user_last_name = user_rows[i].querySelectorAll('.user_last_name')[0].innerHTML.trim();
        current_user_first_name = user_rows[i].querySelectorAll('.user_first_name')[0].innerHTML.trim();
        current_user_email_address = user_rows[i].querySelectorAll('.user_email_address')[0].innerHTML.trim();
        current_user_username = user_rows[i].querySelectorAll('.user_username')[0].innerHTML.trim();
        current_user_boat_name = user_rows[i].querySelectorAll('.boat_name')[0].innerHTML.trim();
        if((current_user_last_name.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_user_first_name.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_user_email_address.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_user_username.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_user_boat_name.toUpperCase().indexOf(filter_from_input) > -1)) {
        } else {
            user_rows[i].classList.add('d-none');
        }

    }
}

function apply_combo_filter() {
    var user_rows = document.getElementsByClassName("user_row");
    var filter_from_input = $("#combo_filter_input").val();
    var current_user_username;
    for(i = 0; i < user_rows.length; i++) {
        current_user_username = user_rows[i].querySelectorAll('.user_username')[0].innerHTML;
        current_user_enable_disable_btn = user_rows[i].querySelectorAll('.enable-disable-user-btn')[0];
        current_user_access_level_icon = user_rows[i].querySelectorAll('.user-access-level-icon')[0];
        switch(filter_from_input) {
            case 'all_users':
                // qui non faccio nulla: quando entro qui, tutti gli utenti sono già visualizzati
                break;
            case 'cloud_users_only':
                if(current_user_username.trim() === '-') {
                    user_rows[i].classList.add('d-none');
                }
                break;
            case 'not_cloud_users_only':
                if(current_user_username.trim() !== '-') {
                    user_rows[i].classList.add('d-none');
                }
                break;
            case 'enabled_users_only':
                if(!(current_user_enable_disable_btn.classList.contains('disable-user-btn')) || (current_user_username.trim() === '-')) {
                    user_rows[i].classList.add('d-none');
                }
                break;
            case 'disabled_users_only':
                if(!(current_user_enable_disable_btn.classList.contains('enable-user-btn')) || (current_user_username.trim() === '-')) {
                    user_rows[i].classList.add('d-none');
                }
                break;
            case 'supervisor_users_only':
                if(!(current_user_access_level_icon.classList.contains('supervisor-user-icon'))) {
                    user_rows[i].classList.add('d-none');
                }
                break;
            case 'full_admin_users_only':
                if(!(current_user_access_level_icon.classList.contains('full-admin-user-icon'))) {
                    user_rows[i].classList.add('d-none');
                }
                break;
            case 'viewer_admin_users_only':
                if(!(current_user_access_level_icon.classList.contains('viewer-admin-user-icon'))) {
                    user_rows[i].classList.add('d-none');
                }
                break;
            case 'standard_users_only':
                if(!(current_user_access_level_icon.classList.contains('standard-user-icon'))) {
                    user_rows[i].classList.add('d-none');
                }
        }
    }
}

function reset_filters() {
    document.getElementById('combo_filter_input').value = 'all_users';
    document.getElementById('fulltext_filter_input').value = '';
}

function apply_filters() {
    var user_rows = document.getElementsByClassName("user_row");
    for(i = 0; i < user_rows.length; i++) {
        user_rows[i].classList.remove('d-none');
    }
    apply_combo_filter();
    apply_fulltext_filter();
    show_no_entries_message_if_there_are_no_entries_or_no_entry_matches_the_filters();
}
