window.addEventListener('load', function() {
    if(!window.matchMedia || (window.matchMedia("(min-width: 768px)").matches)) {
    // a meno che non si stia visualizzando il sito da mobile:
        // abilito i tooltip:
        $('[data-toggle="tooltip"]').tooltip();
    }
    // al caricamento della pagina, faccio in modo che ciascuna tabella snapshot mostri le info riguardanti il posto barca selezionato sulla propria select (se la select esiste, ovvero se il contratto è stato usato su più di un posto barca):
    subscription_berth_select_elements = document.querySelectorAll(`[data-element-purpose="subscription_berth_select"]`);
    for(var i = 0; i < subscription_berth_select_elements.length; i++) {
        change_shown_berth_info_according_to_selected_berth(subscription_berth_select_elements[i]);
    }
});

function change_chart_form_action_according_to_selected_berth(form_id, new_berth_id) {
    var subscription_id = form_id.substring(form_id.lastIndexOf('[') + 1, form_id.lastIndexOf(']'));
    var form = document.getElementById(form_id);
    var old_form_action, new_form_action;
    old_form_action = form.action;
    var form_action_part_before_berth_id, form_action_part_after_berth_id_plus_old_berth, first_slash_occurrence_position_in_form_action_part_after_berth_id_without_old_berth;
    form_action_part_before_berth_id = old_form_action.substring(0, (old_form_action.lastIndexOf("subscriptions/" + subscription_id + "/") + ("subscriptions/" + subscription_id + "/").length));
    form_action_part_after_berth_id_plus_old_berth = old_form_action.replace(form_action_part_before_berth_id, "");
    var first_slash_occurrence_position_in_form_action_part_after_berth_id_without_old_berth = form_action_part_after_berth_id_plus_old_berth.indexOf('/');
    form_action_part_after_berth_id_without_old_berth = form_action_part_after_berth_id_plus_old_berth.substring(first_slash_occurrence_position_in_form_action_part_after_berth_id_without_old_berth, form_action_part_after_berth_id_plus_old_berth.length);
    new_form_action = form_action_part_before_berth_id + new_berth_id + form_action_part_after_berth_id_without_old_berth;
    form.action = new_form_action;
}

function change_shown_berth_info_according_to_selected_berth(berth_select_element) {
    // salvo nella variabile snapshot_info_div il div che contiene lo stato della subscription:
    var snapshot_info_div = $(berth_select_element).closest(".socket_status")[0];
    // recupero l'id del posto barca selezionato:
    var selected_berth_id = berth_select_element.options[berth_select_element.selectedIndex].value;
    // recupero l'id del contratto:
    var subscription_id = snapshot_info_div.getAttribute('data-subscription_id');
    var subscription = subscriptions.filter(obj => {
    // recupero il contratto all'interno dell'array subscriptions:
        return obj.id == subscription_id
    })[0];
    // recupero il pulsante di on/off della presa:
    var on_off_btt = snapshot_info_div.previousElementSibling.querySelectorAll(`[data-snapshot-attribute=on_off_btt]`)[0];
    if(subscription !== null) {
    // se il contratto ESISTE nell'array subscriptions:
        // ne recupero l'ultimo snapshot sul posto barca che ha id selected_berth_id:
        var subscription_berth = subscription.berths.filter(obj => {
            return obj.id == selected_berth_id
        })[0];
        if(subscription_berth === null) {
        // se il contratto subscription NON HA MAI EFFETTUATO CONSUMI sul posto barca selected_berth_id:
            // TODO: forse questo non succede mai, perchè in questo caso il berth non sarebbe nei subscription_berths (ovvero: non ci sarebbero record in subscription_snapshots relativi al contratto subscription e al posto barca selected_berth_id). Verificare.
            // nascondo tutte le righe relative allo stato e tolgo eventuali allarmi visualizzati per lo snapshot precedente:
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="socket_type"]`)[0].classList.add('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="link_status"]`)[0].classList.add('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="link_status"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_data_part"]`)[0].classList.add('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_data_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.add('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.add('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.remove('alarm_under_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.add('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.remove('alarm_under_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_data_part"]`)[0].classList.add('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_data_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_chart_controls_part"]`)[0].classList.add('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_chart_controls_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="instant_power_consumption"]`)[0].classList.add('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="last_update"]`)[0].classList.add('d-none');
            // nascondo il pulsante di on/off:
            on_off_btt.classList.add('d-none');
            // e mostro il messaggio di stato non disponibile:
            snapshot_info_div.querySelectorAll(`[data-snapshot-purpose="no_status_info_available"]`)[0].classList.remove('d-none');
        } else {
        // se il contratto subscription HA EFFETTUATO CONSUMI sul posto barca selected_berth_id:
            // mostro tutte le righe relative allo stato (le ri-nasconderò all'occorrenza nelle righe successive, se le circostanze lo richiedono) e tolgo eventuali allarmi visualizzati per lo snapshot precedente:
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="socket_type"]`)[0].classList.remove('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="link_status"]`)[0].classList.remove('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="link_status"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_data_part"]`)[0].classList.remove('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_data_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.remove('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.remove('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.remove('alarm_under_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.remove('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.remove('alarm_under_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_data_part"]`)[0].classList.remove('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_data_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_chart_controls_part"]`)[0].classList.remove('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_chart_controls_part"]`)[0].classList.remove('alarm_over_threshold_active');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="instant_power_consumption"]`)[0].classList.remove('d-none');
            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="last_update"]`)[0].classList.remove('d-none');
            // mostro il pulsante di on/off:
            on_off_btt.classList.remove('d-none');
            // rimuovo dal pulsante di on/off eventuali classi relative al colore (in seguito lo aggiungerò di nuovo in base allo stato della presa relativa al posto barca selezionato):
            on_off_btt.classList.remove('btn-success', 'btn-danger', 'btn-warning');
            // rimuovo dal pulsante di on/off l'attributo disabled (lo aggiungerò di nuovo in seguito se necessario):
            on_off_btt.disabled = false;
            // e nascondo il messaggio di stato non disponibile:
            snapshot_info_div.querySelectorAll(`[data-snapshot-purpose="no_status_info_available"]`)[0].classList.remove('d-none');
            if((snapshot_info_div.querySelector(`[data-snapshot-attribute="socket_type"]`) !== null) && (subscription_berth.socket_type !== socket_type_value_for_not_defined)) {
            // se per il posto barca selezionato viene specificato il tipo di presa, e se sulla pagina esiste lo span che deve contenere il socket_type (TODO: cosa che in teoria ora è sempre true perchè dovrei outputtarlo sempre, perciò credo di poter togliere quella condizione):
                // aggiorno l'informazione del socket_type:
                snapshot_info_div.querySelector(`[data-snapshot-attribute="socket_type"]`).innerHTML = subscription_berth.socket_type;
            } else {
            // se per il posto barca selezionato NON viene specificato il tipo di presa oppure se sulla pagina NON esiste lo span che deve contenere il socket_type (TODO: cosa che in teoria ora non succede più perchè dovrei outputtarlo sempre, perciò credo di poter togliere quella condizione):
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="socket_type"]`)[0].classList.add('d-none');
            }
            var subscription_last_snapshot_on_selected_berth = subscription_berth.last_snapshot_for_subscription;
            if(subscription_last_snapshot_on_selected_berth === null) {
            // se NON ESISTE nessuno snapshot per la subscription sul posto barca selezionato:
                // nascondo tutte le righe relative a credito residuo, contatori energia/acqua, form di richiesta grafici e consumo istantaneo:
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="link_status"]`)[0].classList.add('d-none');
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_data_part"]`)[0].classList.add('d-none');
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.add('d-none');
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.add('d-none');
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.add('d-none');
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_data_part"]`)[0].classList.add('d-none');
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_chart_controls_part"]`)[0].classList.add('d-none');
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="instant_power_consumption"]`)[0].classList.add('d-none');
                snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="last_update"]`)[0].classList.add('d-none');
                // nascondo il pulsante di on/off:
                on_off_btt.classList.add('d-none');
                // e mostro il messaggio di stato non disponibile:
                snapshot_info_div.querySelectorAll(`[data-snapshot-purpose="no_status_info_available"]`)[0].classList.remove('d-none');
            } else {
            // se ESISTE uno snapshot per la subscription sul posto barca selezionato:
                // nascondo il messaggio di stato non disponibile:
                snapshot_info_div.querySelectorAll(`[data-snapshot-purpose="no_status_info_available"]`)[0].classList.add('d-none');
                // popolo snapshot_info_div con i dati del posto barca e dello snapshot appena trovati:
                switch(subscription_last_snapshot_on_selected_berth.link_status) {
                    case socket_link_status_values.connected:
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="link_status"]`)[0].classList.remove('alarm_over_threshold_active');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="link_status"]`)[0].classList.remove('text-danger');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="link_status"]`)[0].classList.add('text-success');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="link_status"]`)[0].innerHTML = translations.socket_link_status_connected.toUpperCase();
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_data_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_data_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_chart_controls_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="instant_power_consumption"]`)[0].classList.remove('d-none');
                        break;
                    case socket_link_status_values.theft_alarm:
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="link_status"]`)[0].classList.add('alarm_over_threshold_active');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="link_status"]`)[0].classList.remove('text-success');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="link_status"]`)[0].classList.add('text-danger');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="link_status"]`)[0].innerHTML = translations.socket_link_status_theft_alarm.toUpperCase();
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_data_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_data_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_chart_controls_part"]`)[0].classList.remove('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="instant_power_consumption"]`)[0].classList.remove('d-none');
                        break;
                    case socket_link_status_values.not_connected:
                    default:
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="link_status"]`)[0].classList.add('alarm_over_threshold_active');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="link_status"]`)[0].classList.remove('text-success');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="link_status"]`)[0].classList.add('text-danger');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="link_status"]`)[0].innerHTML = translations.socket_link_status_not_connected.toUpperCase();
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_data_part"]`)[0].classList.add('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.add('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.add('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.add('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_data_part"]`)[0].classList.add('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_chart_controls_part"]`)[0].classList.add('d-none');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="instant_power_consumption"]`)[0].classList.add('d-none');
                }
                var partial_formaction = on_off_btt.formAction.substr(0, on_off_btt.formAction.indexOf('/turn'));
                switch(subscription_last_snapshot_on_selected_berth.on_off_status) {
                    case socket_on_off_status_values.off:
                        on_off_btt.classList.remove('btn-success', 'btn-warning');
                        on_off_btt.classList.add('btn-danger');
                        on_off_btt.disabled = true;
                        on_off_btt.formAction = partial_formaction + '/' + subscription_berth.id + '/turn_on';        // posso settare il formAction a turn_on anche se la funzionalità lato SmartView non è ancora pronta, tanto il button è disabled
                        on_off_btt_confirmation_message = ucfirst(translations.onoff_btt_alert_switchon_confirmation_request).replace(":berth", subscription_berth.description);        // TODO: per ora setto la richiesta di conferma anche se non posso accendere le prese da remoto. La richiesta non verrà fatta, dato che il button è disabled
                        on_off_btt.setAttribute('onclick', 'return confirm("' + on_off_btt_confirmation_message + '");');
                        on_off_btt.innerText = translations.onoff_btt_caption_socket_is_off.toUpperCase();
                        on_off_btt.setAttribute("data-original-title", ucfirst(translations.onoff_btt_tooltip_socket_switchon_is_disabled_for_security_reasons));
                        break;
                    case socket_on_off_status_values.on:
                        on_off_btt.classList.remove('btn-danger', 'btn-warning');
                        on_off_btt.classList.add('btn-success');
                        on_off_btt.disabled = false;
                        on_off_btt.formAction = partial_formaction + '/' + subscription_berth.id + '/turn_off'
                        on_off_btt_confirmation_message = ucfirst(translations.onoff_btt_alert_switchoff_confirmation_request).replace(":berth", subscription_berth.description);
                        on_off_btt.setAttribute('onclick', 'return confirm("' + on_off_btt_confirmation_message + '");');
                        on_off_btt.innerText = translations.onoff_btt_caption_socket_is_on.toUpperCase();
                        on_off_btt.setAttribute('data-original-title', ucfirst(translations.onoff_btt_tooltip_click_to_switch_off_socket));
                        break;
                    case socket_on_off_status_values.disabled:
                    case socket_on_off_status_values.forced:
                    case socket_on_off_status_values.off_transaction_not_completed:
                    case socket_on_off_status_values.on_transaction_not_completed:
                    // TODO: in realtà dovrei mostrare NOT CONNECTED solo se il valore è "1ERROR", e mostrare caption più precise e permettere lo spegnimento della presa in molti degli altri casi, ma al momento faccio così anche quando genero la pagina via php. Sistemare
                        on_off_btt.classList.remove('btn-danger', 'btn-success');
                        on_off_btt.classList.add('btn-warning');
                        on_off_btt.disabled = true;
                        // qui non cambio il formAction, tanto il button è già disabled
                        on_off_btt.innerText = translations.onoff_btt_caption_socket_is_not_connected.toUpperCase();
                        on_off_btt.setAttribute("data-original-title", ucfirst(translations.onoff_btt_tooltip_socket_is_not_responding_unable_to_control_it));
                        break;
                    case socket_on_off_status_values.not_connected:
                    default:
                        on_off_btt.classList.remove('btn-danger', 'btn-success');
                        on_off_btt.classList.add('btn-warning');
                        on_off_btt.disabled = true;
                        // qui non disabilito il formAction (altrimenti ne perdo il riferimento per quando devo riabilitarlo), tanto il button è già disabled
                        on_off_btt.innerText = translations.onoff_btt_caption_socket_is_not_connected.toUpperCase();
                        on_off_btt.setAttribute("data-original-title", ucfirst(translations.onoff_btt_tooltip_socket_is_not_responding_unable_to_control_it));
                }
                residual_credit_span = snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="socket_residual_credit"]`)[0];
                if((subscription_last_snapshot_on_selected_berth.on_off_status === socket_on_off_status_values.off) || ((subscription.is_prepaid) && (subscription_last_snapshot_on_selected_berth.residual_credit === 0))) {
                // se la presa è spenta o il contratto è prepagato ma il credito è arrivato a zero (TODO: le due eventualità sono in realtà la stessa eventualità?):
                    // mostro "-" come credito residuo:
                    residual_credit_span.classList.remove("text-light", "bg-info", "p-2", "rounded", "d-inline", "text-center");
                    residual_credit_span.innerText = "-";
                } else {
                // se la presa è accesa o il contratto è postpagato o il credito è !== 0
                    switch(subscription.is_prepaid) {
                        case true:
                        // se il contratto è prepagato, mostro il credito disponibile sulla presa selezionata:
                            // (eventualmente togliendo dal residual_credit_span le classi usate dall'indicatore di subscription prepagata):     // TODO: in teoria questa cosa non serve, lo faccio per sicurezza
                            residual_credit_span.classList.remove("text-light", "bg-info", "p-2", "rounded", "d-inline", "text-center");
                            residual_credit_span.innerText = subscription_last_snapshot_on_selected_berth.residual_credit + '\xa0' + app_settings.currency_character;   // TODO: il credito residuo andrà approssimato secondo l'apposita configurazione in dates_currency_and_locale, sempre che non sia già approssimato all'interno della variabile subscription_last_snapshot_on_selected_berth
                            // e mostro i controlli per il grafico credito:
                            //snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.remove('d-none');     // TODO: in teoria quest'istruzione è inutile, la riga l'ho già mostrata sopra
                            break;
                        case false:
                        // se il contratto è postpagato
                            // mostro l'apposito indicatore:
                            residual_credit_span.add("text-light", "bg-info", "p-2", "rounded", "d-inline", "text-center");
                            residual_credit_span.innerText = translations.postpaid_user_span_caption.toUpperCase();
                            // e nascondo i controlli per il grafico credito:
                            snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_chart_controls_part"]`)[0].classList.add('d-none');
                    }
                }
                snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="socket_power_cnt"]`)[0].innerHTML = subscription_last_snapshot_on_selected_berth.total_power_cnt;
                snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="socket_water_cnt"]`)[0].innerHTML = subscription_last_snapshot_on_selected_berth.total_water_cnt;
                var socket_instant_power_consumption_container_element = snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="socket_instant_power_consumption"]`)[0];
                var socket_last_update_timestamp_container_element = snapshot_info_div.querySelectorAll(`[data-snapshot-attribute="socket_last_update_timestamp"]`)[0];
                socket_instant_power_consumption_container_element.innerHTML = subscription_last_snapshot_on_selected_berth.instant_power_consumption;
                var last_update_timestamp_utc = moment.tz(subscription_last_snapshot_on_selected_berth.timestamp, app_settings.server_timezone);
                var last_update_timestamp_local = last_update_timestamp_utc.clone().tz(app_settings.local_timezone);
                var last_update_timestamp_local_formatted = last_update_timestamp_local.format(app_settings.datetime_format_full_momentjs);
                socket_last_update_timestamp_container_element.innerHTML = last_update_timestamp_local_formatted;
                if(subscription_last_snapshot_on_selected_berth.alarms !== null) {
                    if(subscription_last_snapshot_on_selected_berth.alarms.includes(socket_snapshot_active_alarms_flags.min_power_consumption_alarm_flag)) {
                    // se l'allarme di consumo energia < della soglia minima impostata è attivo:
                        // lo segnalo:
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.add('alarm_under_threshold_active');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.add('alarm_under_threshold_active');
                    }
                    if(subscription_last_snapshot_on_selected_berth.alarms.includes(socket_snapshot_active_alarms_flags.max_power_consumption_alarm_flag)) {
                    // se l'allarme di consumo energia > della soglia massima impostata è attivo:
                        // lo segnalo:
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_data_part"]`)[0].classList.add('alarm_over_threshold_active');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="power_chart_controls_part"]`)[0].classList.add('alarm_over_threshold_active');
                    }
                    if(subscription_last_snapshot_on_selected_berth.alarms.includes(socket_snapshot_active_alarms_flags.max_water_time_alarm_flag)) {
                    // se l'allarme di rubinetto acqua aperto da un tempo > della soglia massima impostata è attivo:
                        // lo segnalo:
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_data_part"]`)[0].classList.add('alarm_over_threshold_active');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="water_chart_controls_part"]`)[0].classList.add('alarm_over_threshold_active');
                    }
                    if(subscription_last_snapshot_on_selected_berth.alarms.includes(socket_snapshot_active_alarms_flags.min_credit_alarm_flag)) {
                    // se l'allarme di credito sulla presa < della soglia minima impostata è attivo:
                        // lo segnalo:
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_credit_data_part"]`)[0].classList.add('alarm_over_threshold_active');
                        snapshot_info_div.querySelectorAll(`[data-snapshot-row-purpose="residual_chart_controls_part"]`)[0].classList.add('alarm_over_threshold_active');
                    }
                }
                change_chart_form_action_according_to_selected_berth('subscription_chart_power_consumption_frm[' + subscription_id + ']', selected_berth_id);
                change_chart_form_action_according_to_selected_berth('subscription_chart_water_consumption_frm[' + subscription_id + ']', selected_berth_id);
                change_chart_form_action_according_to_selected_berth('subscription_chart_credit_consumption_frm[' + subscription_id + ']', selected_berth_id);
            }
        }
    }
    // prima di terminare, aggiorno anche i timestamp di inizio e fine sugli attributi action dei tre form di richiesta del grafico, in modo da portarli avanti all'istante attuale:
    change_chart_form_action_according_to_selected_period('subscription_chart_power_consumption_frm[' + subscription_id + ']');
    change_chart_form_action_according_to_selected_period('subscription_chart_water_consumption_frm[' + subscription_id + ']');
    change_chart_form_action_according_to_selected_period('subscription_chart_credit_consumption_frm[' + subscription_id + ']');
}
