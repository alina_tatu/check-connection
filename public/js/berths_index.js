$(document).ready(function() {
// appena la pagina viene caricata:
     if(!window.matchMedia || (window.matchMedia("(min-width: 768px)").matches)) {
    // a meno che non si stia visualizzando il sito da mobile:
        // abilito i tooltip:
        $('[data-toggle="tooltip"]').tooltip();
    }
});

function show_no_entries_message_if_there_are_no_entries_or_no_entry_matches_the_filters() {
// mostra/nasconde il messaggio di no entries a seconda che ci siano o meno posti barca visualizzati (potrebbero non esserci perchè nessun posto barca corrisponde ai filtri settati)
    var no_entries_message_div = document.getElementById("no_entries_message");
    var shown_berth_rows = document.querySelectorAll('.berth_row:not(.d-none)');
    if(shown_berth_rows.length > 0) {
        if(!(no_entries_message_div.classList.contains("d-none"))) {
            no_entries_message_div.classList.add("d-none");
        }
    } else {
        if(no_entries_message_div.classList.contains("d-none")) {
            no_entries_message_div.classList.remove("d-none");
        }
    }
}

function apply_fulltext_filter() {
    var berth_rows = document.getElementsByClassName("berth_row");
    var filter_from_input = $("#fulltext_filter_input").val().toUpperCase();
    var current_berth_name, current_berth_socket_type, current_user_full_name, current_user_username, current_user_boat_name;
    for(i = 0; i < berth_rows.length; i++) {
        current_berth_name = berth_rows[i].querySelectorAll('.berth_name_inner')[0].innerHTML.trim();
        current_berth_socket_type = berth_rows[i].querySelectorAll('.socket_type')[0].innerHTML.trim();
        current_user_full_name_element = berth_rows[i].querySelectorAll('.user_full_name')[0];
        if(current_user_full_name_element.innerHTML.trim() !== '-') {
            current_user_full_name = current_user_full_name_element.innerHTML;
        } else {
            current_user_full_name = '';
        }
        current_user_username_element = berth_rows[i].querySelectorAll('.user_username')[0];
        if((current_user_username_element !== undefined) && (current_user_username_element.innerHTML.trim().length > 0)) {
            current_user_username = current_user_username_element.innerHTML.trim();
        } else {
            current_user_username = '';
        }
        current_user_boat_name_element = berth_rows[i].querySelectorAll('.user_boat_name')[0];
        if((current_user_boat_name_element !== undefined) && (current_user_boat_name_element.innerHTML.trim() !== '-')) {
            current_user_boat_name = current_user_boat_name_element.innerHTML.trim();
        } else {
            current_user_boat_name = '';
        }
        if((current_berth_name.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_berth_socket_type.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_user_full_name.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_user_boat_name.toUpperCase().indexOf(filter_from_input) > -1)) {
        } else {
            berth_rows[i].classList.add('d-none');
        }
    }
}

function apply_combo_filter() {
    var berth_rows = document.getElementsByClassName("berth_row");
    var filter_from_input = $("#combo_filter_input").val();
    for(i = 0; i < berth_rows.length; i++) {
        current_subscription_begin_timestamp = berth_rows[i].querySelectorAll('.subscription_begin_timestamp')[0].innerHTML.trim();
        current_subscription_end_timestamp = berth_rows[i].querySelectorAll('.subscription_end_timestamp')[0].innerHTML.trim();
        switch(filter_from_input) {
            case 'available_berths_only':
                if(current_subscription_begin_timestamp !== '-') {
                    berth_rows[i].classList.add('d-none');
                }
                break;
            case 'assigned_berths_only':
                if(((current_subscription_begin_timestamp !== '-') && (current_subscription_end_timestamp !== '-')) ||
                   ((current_subscription_begin_timestamp === '-') && (current_subscription_end_timestamp === '-'))) {
                    berth_rows[i].classList.add('d-none');
                }
            case 'all_berths':
                // qui non faccio nulla, non devo nascondere nessuna riga e in teoria - prima di entrare qui - nessuna riga risulta nascosta
        }
    }
}

function reset_filters() {
    document.getElementById('combo_filter_input').value = 'available_berths_only';
    document.getElementById('fulltext_filter_input').value = '';
}


function apply_filters() {
    var berth_rows = document.getElementsByClassName("berth_row");
    for(i = 0; i < berth_rows.length; i++) {
        berth_rows[i].classList.remove('d-none');
    }
    apply_combo_filter();
    apply_fulltext_filter();
    show_no_entries_message_if_there_are_no_entries_or_no_entry_matches_the_filters();
}
