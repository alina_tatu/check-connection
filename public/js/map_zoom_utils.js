$(document).keypress(function(e) {
    if((e.key == '-')) {
        zoom_map_out(0.05);
    }
    if((e.key == '+')) {
        zoom_map_in(0.05);
    }
});

$('#map_zoom_out_btn').click(function() {
    zoom_map_out(0.05);
});

$('#map_zoom_in_btn').click(function() {
    zoom_map_in(0.05);
});

function zoom_map_in(factor) {
    if(zoomCompleted == true) {
        zoomCompleted = false;
        if(document.getElementById('map_img').width < (1.2 * document.getElementById('map_img').naturalWidth)) {
            $('#map_img').width($('#map_img').width() * (1 + factor));
            $('.pedestal_marker').each(function() {
                current_marker_top_position = $(this).position().top;
                current_marker_left_position = $(this).position().left;
                current_marker_width = $(this).width();
                $(this).css('top', (current_marker_top_position * (1 + factor)));
                $(this).css('left', (current_marker_left_position * (1 + factor)));
                $(this).css('width', (current_marker_width * (1 + factor)));
            })
        }
    }
    zoomCompleted = true;
}

function zoom_map_out(factor) {
    if(zoomCompleted == true) {
        zoomCompleted = false;
        if(document.getElementById('map_img').width > (document.getElementById('map_img').naturalWidth / 2)) {
            $('#map_img').width($('#map_img').width() / (1 + factor));
            $('.pedestal_marker').each(function() {
                current_marker_top_position = $(this).position().top;
                current_marker_left_position = $(this).position().left;
                current_marker_width = $(this).width();
                $(this).css('top', (current_marker_top_position / (1 + factor)));
                $(this).css('left', (current_marker_left_position / (1 + factor)));
                $(this).css('width', (current_marker_width / (1 + factor)));
            })
        }
    }
    zoomCompleted = true;
}
