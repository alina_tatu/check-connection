$(document).ready(function() {
// appena la pagina viene caricata:
    if(!window.matchMedia || (window.matchMedia("(min-width: 768px)").matches)) {
    // a meno che non si stia visualizzando il sito da mobile:
        // abilito i tooltip:
        $('[data-toggle="tooltip"]').tooltip();
    }
});

function change_payments_div_visibility_payments_button_caption(button_caption_for_showing_the_payments_table, button_caption_for_hiding_the_payments_table, button_caption_for_showing_the_new_payment_div) {
    var payments_table =  document.querySelector('table#payments-history');
    var show_hide_payments_button =  document.getElementById('show-payments-btt');
    if(payments_table.classList.contains('d-none')) {
        payments_table.classList.remove('d-none');
        show_hide_payments_button.innerHTML = button_caption_for_hiding_the_payments_table;
    } else {
        payments_table.classList.add('d-none');
        show_hide_payments_button.innerHTML = button_caption_for_showing_the_payments_table;
    }
    // se il form di nuovo pagamento è visibile, lo nascondo:
    var new_payment_div = document.querySelector('div#new-payment');
    if(!(new_payment_div.classList.contains('d-none'))) {
        new_payment_div.classList.add('d-none');
        var new_payment_btt = document.getElementById('show-new-payment-div-btt');
        new_payment_btt.innerHTML = button_caption_for_showing_the_new_payment_div;
    }
}

function change_new_payment_div_visibility_new_payment_button_caption(button_caption_for_showing_the_new_payment_div, button_caption_for_hiding_the_new_payment_div, button_caption_for_showing_the_payments_table) {
    var new_payment_div = document.querySelector('div#new-payment');
    var show_hide_new_payment_div_button =  document.getElementById('show-new-payment-div-btt');
    if(new_payment_div.classList.contains('d-none')) {
        new_payment_div.classList.remove('d-none');
        show_hide_new_payment_div_button.innerHTML = button_caption_for_hiding_the_new_payment_div;
    } else {
        new_payment_div.classList.add('d-none');
        show_hide_new_payment_div_button.innerHTML = button_caption_for_showing_the_new_payment_div;
    }
    // se la tabella del riepilogo pagamenti è visibile, la nascondo:
    var payments_table =  document.querySelector('table#payments-history');
    if(!(payments_table.classList.contains('d-none'))) {
        payments_table.classList.add('d-none');
        var show_hide_payments_button = document.getElementById('show-payments-btt');
        show_hide_payments_button.innerHTML = button_caption_for_showing_the_payments_table;
    }
}

function paid_amount_is_greater_than_zero() {
    if((document.getElementById('paid_power_consumption_amount').value > 0) || (document.getElementById('paid_water_consumption_amount').value > 0)) {
        return true;
    } 
    return false;
}

function enable_add_payment_button_and_disable_tooltip() {
    document.getElementById("add_payment_btn").removeAttribute("title");
    document.getElementById("add_payment_btn").disabled = false;
}

function disable_add_payment_button_and_enable_tooltip(tooltip_text) {
    document.getElementById("add_payment_btn").title = tooltip_text;
    document.getElementById("add_payment_btn").disabled = true;
}
