function validate_selected_user(user_not_found_alert_msg) {
    selected_user_full_name = document.querySelector('input#user').value;
    found = false;
    users.forEach(function(current_user) {
        current_user_full_name = current_user.last_name + ' ' + current_user.first_name;
        if(current_user_full_name === selected_user_full_name) {
            found = true;
        }
    });
    if(!found) {
        alert(user_not_found_alert_msg);
    }
    return found;
}
