$(document).ready(function() {
// appena la pagina viene caricata:
    if(!window.matchMedia || (window.matchMedia("(min-width: 768px)").matches)) {
    // a meno che non si stia visualizzando il sito da mobile:
        // abilito i tooltip:
        $('[data-toggle="tooltip"]').tooltip();
    }
});

function show_no_entries_message_if_there_are_no_entries_or_no_entry_matches_the_filters() {
// mostra/nasconde il messaggio di no entries a seconda che ci siano o meno contratti visualizzati (potrebbero non esserci perchè nessun contratto corrisponde ai filtri settati)
    var no_entries_message_div = document.getElementById("no_entries_message");
    var shown_subscription_rows = document.querySelectorAll('.subscription_row:not(.d-none)');
    if(shown_subscription_rows.length > 0) {
        if(!(no_entries_message_div.classList.contains("d-none"))) {
            no_entries_message_div.classList.add("d-none");
        }
    } else {
        if(no_entries_message_div.classList.contains("d-none")) {
            no_entries_message_div.classList.remove("d-none");
        }
    }
}

function apply_fulltext_filter() {
    var subscription_rows = document.getElementsByClassName("subscription_row");
    var filter_from_input = $("#fulltext_filter_input").val().toUpperCase();
    var current_berth_name, current_user_full_name, current_user_username, current_user_boat_name, current_transponder_code;
    for(i = 0; i < subscription_rows.length; i++) {
        current_berth_name = subscription_rows[i].querySelectorAll('.berth_name_inner')[0].innerHTML.trim();
        current_transponder_code = subscription_rows[i].querySelectorAll('.transponder_code_inner')[0].innerHTML.trim();
        current_user_full_name_element = subscription_rows[i].querySelectorAll('.user_full_name')[0];
        if(current_user_full_name_element.innerHTML.trim() !== '-') {
            current_user_full_name = current_user_full_name_element.innerHTML;
        } else {
            current_user_full_name = '';
        }
        current_user_username_element = subscription_rows[i].querySelectorAll('.user_username')[0];
        if((current_user_username_element !== undefined) && (current_user_username_element.innerHTML.trim().length > 0)) {
            current_user_username = current_user_username_element.innerHTML.trim();
        } else {
            current_user_username = '';
        }
        current_user_boat_name_element = subscription_rows[i].querySelectorAll('.user_boat_name')[0];
        if((current_user_boat_name_element !== undefined) && (current_user_boat_name_element.innerHTML.trim() !== '-')) {
            current_user_boat_name = current_user_boat_name_element.innerHTML.trim();
        } else {
            current_user_boat_name = '';
        }        
        if((current_berth_name.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_user_full_name.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_user_boat_name.toUpperCase().indexOf(filter_from_input) > -1) ||
           (current_transponder_code.toUpperCase().indexOf(filter_from_input) > -1)) {
               // qui non faccio nulla, la riga è già visibile
        } else {
            subscription_rows[i].classList.add('d-none');
        }
    }
}

function apply_combo_filter() {
    var subscription_rows = document.getElementsByClassName("subscription_row");
    var filter_from_input = $("#combo_filter_input").val();
    for(i = 0; i < subscription_rows.length; i++) {
        current_subscription_begin_timestamp = subscription_rows[i].querySelectorAll('.subscription_begin_timestamp')[0].innerHTML.trim();
        current_subscription_end_timestamp = subscription_rows[i].querySelectorAll('.subscription_end_timestamp')[0].innerHTML.trim();
        switch(filter_from_input) {
            case 'active_subscriptions_only':
                if(current_subscription_end_timestamp !== '-') {
                    subscription_rows[i].classList.add('d-none');
                }
                break;
            case 'ended_subscriptions_only':
                if((current_subscription_begin_timestamp !== '-') && (current_subscription_end_timestamp === '-')) {
                    subscription_rows[i].classList.add('d-none');
                }
            case 'all_subscriptions':
                // qui non faccio nulla, non devo nascondere nessuna riga e in teoria - prima di entrare qui - nessuna riga risulta nascosta
        }
    }
}

function reset_filters() {
    document.getElementById('combo_filter_input').value = 'active_subscriptions_only';
    document.getElementById('fulltext_filter_input').value = '';
}

function apply_filters() {
    var subscription_rows = document.getElementsByClassName("subscription_row");
    for(i = 0; i < subscription_rows.length; i++) {
        subscription_rows[i].classList.remove('d-none');
    }
    apply_combo_filter();
    apply_fulltext_filter();
    show_no_entries_message_if_there_are_no_entries_or_no_entry_matches_the_filters();
}
