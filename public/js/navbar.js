function rearrange_navbar_entries_according_to_viewport() {
// viene chiamata al caricamento della pagina e al resize della finestra del browser. Sposta il dropdown dell'utente ("Welcome, utente") in cima alla navbar se la larghezza del viewport è < medium_screen_min_width.
    const medium_screen_min_width = 768;    
    const viewport_width = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    if(viewport_width <= medium_screen_min_width) {
        navbar_left_side = document.querySelector('#navbar_left_container');
        navbar_right_side = document.querySelector('#navbar_right_container');
        navbar_dropdown_parent_element = document.querySelector("#navbarDropdownParent");
        navbar_dropdown_content_element = document.querySelector("#navbarDropdownContent");
        navbar_left_side.insertBefore(navbar_dropdown_content_element, navbar_left_side.firstChild);
        navbar_left_side.insertBefore(navbar_dropdown_parent_element, navbar_dropdown_content_element);
    }
}