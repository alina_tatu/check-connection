#!/bin/bash

# lancio come utente www-data il pull da origin delle ultime modifiche al repository, in modo da preservare i permessi sulle directory:
sudo -u www-data git pull
# svuoto la cache e la modelCache: 
php artisan cache:clear
php artisan modelCache:clear
# lancio il comando optimize di artisan, che fa il caching di config, delle routes e dei files:
php artisan optimize
# faccio il caching delle views:
php artisan view:cache
# faccio il caching dei model (credo): 
php artisan modelCache:publish